enum Flavor { develop, qa, staging, production }

enum DeviceType { mobile, tablet }

enum LeadingIcon {
  back,
  close,
  hambuger,
  none,
  appLogo,
}

enum AppBarTitle {
  logo,
  text,
  none,
}
