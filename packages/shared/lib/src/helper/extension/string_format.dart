extension StringFormat on String {
  String removeComma() {
    return replaceAll(',','');
  }
  String removeDots() {
    return replaceAll('.','');
  }
  String removePercent() {
    return replaceAll('%','');
  }
}