import 'package:intl/intl.dart';

extension CurrencyFormat on double {
  String toEuro() {
    return NumberFormat.simpleCurrency(
      name: 'EUR',
    ).format(this / 100);
  }

  String toUsd() {
    return NumberFormat.simpleCurrency(
      name: 'USD',
    ).format(this / 1);
  }

  String toVnd() {
    return NumberFormat.simpleCurrency(
      name: 'VND',
    ).format(this);
  }
}