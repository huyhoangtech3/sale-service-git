import 'package:flutter/services.dart';

class SymbolFormatter extends TextInputFormatter {
  final String? symbolStart;
  final String? symbolEnd;

  SymbolFormatter({
    this.symbolStart,
    this.symbolEnd,
  });

  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.text.isEmpty) {
      return newValue.copyWith(text: '');
    }

    // Remove all non-numeric characters
    String numberText = newValue.text.replaceAll(RegExp('[^0-9]'), '');

    // Format with symbol and digits
    String formattedValue = '$symbolStart$numberText$symbolEnd';

    return newValue.copyWith(
      text: formattedValue,
      selection: TextSelection.collapsed(offset: formattedValue.length - 1),
    );
  }
}
