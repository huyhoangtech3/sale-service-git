import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

class CurrencyFormatter extends TextInputFormatter {
  final String separator;

  CurrencyFormatter({
    required this.separator,
  }) {
    assert(separator != null);
  }

  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue){
    if (newValue.text.isEmpty) {
      return newValue.copyWith(text: '');
    }

    // Remove all non-numeric characters
    String numberText = newValue.text.replaceAll(RegExp(r'[^0-9]'), '');

    // Add commas to separate thousands
    final formatter = NumberFormat(separator);
    String formattedValue = formatter.format(int.parse(numberText));

    return newValue.copyWith(
      text: formattedValue,
      selection: TextSelection.collapsed(offset: formattedValue.length),
    );
  }
}