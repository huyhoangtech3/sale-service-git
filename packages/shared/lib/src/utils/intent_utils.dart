import 'package:url_launcher/url_launcher.dart' as url_launcher;

class IntentUtils {
  static Future<bool> openBrowserURL({
    required String uri,
  }) async {
    final web = Uri.parse(uri);
    return await url_launcher.canLaunchUrl(web)
        ? await url_launcher.launchUrl(web)
        : throw 'Could not launch $web';
  }

  static Future<bool> call({
    required String uri,
  }) async {
    final phone = Uri.parse(uri);
    return await url_launcher.canLaunchUrl(phone)
        ? await url_launcher.launchUrl(phone)
        : throw 'Could not launch $phone';
  }

  static Future<bool> sms({
    required String uri,
  }) async {
    final sms = Uri.parse(uri);
    return await url_launcher.canLaunchUrl(sms)
        ? await url_launcher.launchUrl(sms)
        : throw 'Could not launch $sms';
  }

  static Future<bool> email({
    required String to,
    required String subject,
    required String body,
  }) async {
    final email = Uri(
      scheme: 'mailto',
      path: to,
      query: 'subject=$subject&body=$body',
    );
    return await url_launcher.canLaunchUrl(email)
        ? await url_launcher.launchUrl(email)
        : throw 'Could not launch $email';
  }
}
