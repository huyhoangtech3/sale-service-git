import 'package:intl/intl.dart';

import 'package:shared/shared.dart';

class NumberFormatUtils {
  NumberFormatUtils._();

  static String formatYen(double price) {
    return NumberFormat.currency(symbol: SymbolConstants.yen, decimalDigits: 0).format(price);
  }

  static String formatNumber(int number) {
    return NumberFormat(NumberFormatConstants.defaultFormat).format(number);
  }

  static String formatDotsNumber(int number) {
    return NumberFormat(NumberFormatConstants.dotsFormat).format(number);
  }
}
