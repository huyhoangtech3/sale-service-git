import 'package:shared/shared.dart';

class Constants {
  const Constants._();

  /// Character special
  static const space = ' ';
  static const colon = ':';
  static const percent = '%';
  static const atSign = '@';
  static const caret = '^';
  static const asterisk = '*';
  static const hash = '#';
  static const slash = '/';
  static const backslash = '\\';
  static const hyphen = '-';
  static const questionMark = '?';
  static const fullStop = '.';
  static const amp = '&';
  static const lt = '<';
  static const gt = '>';
  static const quot = '"';
  static const plus = '+';
  static const divine = '÷';
  /// Gender
  static const man = 'Nam';
  static const woman = 'Nữ';
  static const male = 'MALE';
  static const female = 'FEMALE';
  /// Customer
  static const view = 'VIEW';
  static const indiv = 'INDIV';
  /// Utilities
  static const usd = 'USD';
  static const vnd = 'VND';
  static const usdVnd = 'USD/VND';
}
