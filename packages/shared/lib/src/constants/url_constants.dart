import 'package:shared/shared.dart';

class UrlConstants {
  const UrlConstants._();

  /// Url
  static const termUrl = 'https://www.chatwork.com/';
  static const lineApiBaseUrl = 'https://api.line.me/';
  static const twitterApiBaseUrl = 'https://api.twitter.com/';
  static const goongApiBaseUrl = 'https://rsapi.goong.io/';
  static const firebaseStorageBaseUrl = 'https://firebasestorage.googleapis.com/';
  static const randomUserBaseUrl = 'https://randomuser.me/api/';
  static const mockApiBaseUrl = 'https://api.jsonbin.io/';

  /// Path
  static const remoteConfigPath = '/config/RemoteConfig.json';
  static const settingsPath = '/mypage/settings';

  static const auth = 'realms/mbcrm/protocol/openid-connect';
  static const admin = 'crm-admin';
  static const category = 'crm-category';
  static const rmProfile = 'crm-rm';
  static const customer = 'crm-customer';
  static const sale = 'crm-sale';
  static const activity = 'crm-activity';
  static const workFlow = 'crm-workflow';
  static const report = 'crm-report';

  static String get keyCloakApiBaseUrl {
    switch (EnvConstants.flavor) {
      case Flavor.develop:
        return 'https://dev-keycloak21.lptech.vn/';
      case Flavor.qa:
        return 'https://dev-keycloak21.lptech.vn/';
      case Flavor.staging:
        return 'https://dev-keycloak21.lptech.vn/';
      case Flavor.production:
        return 'https://dev-keycloak21.lptech.vn/';
      default:
        return 'https://dev-keycloak21.lptech.vn/';
    }
  }

  static String get appApiBaseUrl {
    switch (EnvConstants.flavor) {
      case Flavor.develop:
        return 'https://dev-crm2-api.lptech.vn/';
      case Flavor.qa:
        return 'https://dev-crm2-api.lptech.vn/';
      case Flavor.staging:
        return 'https://dev-crm2-api.lptech.vn/';
      case Flavor.production:
        return 'https://dev-crm2-api.lptech.vn/';
      default:
        return 'https://dev-crm2-api.lptech.vn/';
    }
  }
}
