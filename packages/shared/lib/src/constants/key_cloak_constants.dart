class KeyCloakConstants {
  const KeyCloakConstants._();

  static const audience = 'sale-service';
  static const clientId = 'frontend';
  static const grantType = 'password';
  static const clientSecret = '2e285954-0393-4f8c-908a-fb3b7990fbe6';
  static const refreshToken = 'refresh_token';
}