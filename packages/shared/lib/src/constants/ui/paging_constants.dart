class PagingConstants {
  const PagingConstants._();

  static const initialPage = 0;
  static const itemsPerPage = 10;
  static const defaultInvisibleItemsThreshold = 3;
}
