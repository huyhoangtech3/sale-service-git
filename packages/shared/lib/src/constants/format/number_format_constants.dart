class NumberFormatConstants {
  NumberFormatConstants._();

  static const String defaultFormat = '#,###';
  static const String dotsFormat = '#.###';
}
