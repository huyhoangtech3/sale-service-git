// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class S {
  S();

  static S? _current;

  static S get current {
    assert(_current != null,
        'No instance of S was loaded. Try to initialize the S delegate before accessing S.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = S();
      S._current = instance;

      return instance;
    });
  }

  static S of(BuildContext context) {
    final instance = S.maybeOf(context);
    assert(instance != null,
        'No instance of S present in the widget tree. Did you add S.delegate in localizationsDelegates?');
    return instance!;
  }

  static S? maybeOf(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Smart SME`
  String get appBarTitle {
    return Intl.message(
      'Smart SME',
      name: 'appBarTitle',
      desc: '',
      args: [],
    );
  }

  /// `Login`
  String get login {
    return Intl.message(
      'Login',
      name: 'login',
      desc: '',
      args: [],
    );
  }

  /// `Fake Login`
  String get fakeLogin {
    return Intl.message(
      'Fake Login',
      name: 'fakeLogin',
      desc: '',
      args: [],
    );
  }

  /// `Logout`
  String get logout {
    return Intl.message(
      'Logout',
      name: 'logout',
      desc: '',
      args: [],
    );
  }

  /// `Email`
  String get email {
    return Intl.message(
      'Email',
      name: 'email',
      desc: '',
      args: [],
    );
  }

  /// `Password`
  String get password {
    return Intl.message(
      'Password',
      name: 'password',
      desc: '',
      args: [],
    );
  }

  /// `unknownException ({errorCode})`
  String unknownException(Object errorCode) {
    return Intl.message(
      'unknownException ($errorCode)',
      name: 'unknownException',
      desc: '',
      args: [errorCode],
    );
  }

  /// `parseException`
  String get parseException {
    return Intl.message(
      'parseException',
      name: 'parseException',
      desc: '',
      args: [],
    );
  }

  /// `cancellationException`
  String get cancellationException {
    return Intl.message(
      'cancellationException',
      name: 'cancellationException',
      desc: '',
      args: [],
    );
  }

  /// `noInternetException`
  String get noInternetException {
    return Intl.message(
      'noInternetException',
      name: 'noInternetException',
      desc: '',
      args: [],
    );
  }

  /// `timeoutException`
  String get timeoutException {
    return Intl.message(
      'timeoutException',
      name: 'timeoutException',
      desc: '',
      args: [],
    );
  }

  /// `badCertificateException`
  String get badCertificateException {
    return Intl.message(
      'badCertificateException',
      name: 'badCertificateException',
      desc: '',
      args: [],
    );
  }

  /// `Can not connect to this host`
  String get canNotConnectToHost {
    return Intl.message(
      'Can not connect to this host',
      name: 'canNotConnectToHost',
      desc: '',
      args: [],
    );
  }

  /// `tokenExpired`
  String get tokenExpired {
    return Intl.message(
      'tokenExpired',
      name: 'tokenExpired',
      desc: '',
      args: [],
    );
  }

  /// `emptyEmail`
  String get emptyEmail {
    return Intl.message(
      'emptyEmail',
      name: 'emptyEmail',
      desc: '',
      args: [],
    );
  }

  /// `invalidEmail`
  String get invalidEmail {
    return Intl.message(
      'invalidEmail',
      name: 'invalidEmail',
      desc: '',
      args: [],
    );
  }

  /// `invalidPassword`
  String get invalidPassword {
    return Intl.message(
      'invalidPassword',
      name: 'invalidPassword',
      desc: '',
      args: [],
    );
  }

  /// `invalidUserName`
  String get invalidUserName {
    return Intl.message(
      'invalidUserName',
      name: 'invalidUserName',
      desc: '',
      args: [],
    );
  }

  /// `invalidPhoneNumber`
  String get invalidPhoneNumber {
    return Intl.message(
      'invalidPhoneNumber',
      name: 'invalidPhoneNumber',
      desc: '',
      args: [],
    );
  }

  /// `invalidDateTime`
  String get invalidDateTime {
    return Intl.message(
      'invalidDateTime',
      name: 'invalidDateTime',
      desc: '',
      args: [],
    );
  }

  /// `passwordsAreNotMatch`
  String get passwordsAreNotMatch {
    return Intl.message(
      'passwordsAreNotMatch',
      name: 'passwordsAreNotMatch',
      desc: '',
      args: [],
    );
  }

  /// `OK`
  String get ok {
    return Intl.message(
      'OK',
      name: 'ok',
      desc: '',
      args: [],
    );
  }

  /// `Cancel`
  String get cancel {
    return Intl.message(
      'Cancel',
      name: 'cancel',
      desc: '',
      args: [],
    );
  }

  /// `Retry`
  String get retry {
    return Intl.message(
      'Retry',
      name: 'retry',
      desc: '',
      args: [],
    );
  }

  /// `Close`
  String get close {
    return Intl.message(
      'Close',
      name: 'close',
      desc: '',
      args: [],
    );
  }

  /// `Search`
  String get search {
    return Intl.message(
      'Search',
      name: 'search',
      desc: '',
      args: [],
    );
  }

  /// `My Page`
  String get myPage {
    return Intl.message(
      'My Page',
      name: 'myPage',
      desc: '',
      args: [],
    );
  }

  /// `Home`
  String get home {
    return Intl.message(
      'Home',
      name: 'home',
      desc: '',
      args: [],
    );
  }

  /// `Dark Theme`
  String get darkTheme {
    return Intl.message(
      'Dark Theme',
      name: 'darkTheme',
      desc: '',
      args: [],
    );
  }

  /// `Vietnamese`
  String get vietnamese {
    return Intl.message(
      'Vietnamese',
      name: 'vietnamese',
      desc: '',
      args: [],
    );
  }

  /// `User name`
  String get userName {
    return Intl.message(
      'User name',
      name: 'userName',
      desc: '',
      args: [],
    );
  }

  /// `Welcome`
  String get welcomeLoginPage {
    return Intl.message(
      'Welcome',
      name: 'welcomeLoginPage',
      desc: '',
      args: [],
    );
  }

  /// `Enter user name`
  String get hintUserName {
    return Intl.message(
      'Enter user name',
      name: 'hintUserName',
      desc: '',
      args: [],
    );
  }

  /// `Enter password`
  String get hintPassword {
    return Intl.message(
      'Enter password',
      name: 'hintPassword',
      desc: '',
      args: [],
    );
  }

  /// `Phiên bản CRM Nextgen 1.0`
  String get version {
    return Intl.message(
      'Phiên bản CRM Nextgen 1.0',
      name: 'version',
      desc: '',
      args: [],
    );
  }

  /// `CRM NextGen version`
  String get crmNextGenVersion {
    return Intl.message(
      'CRM NextGen version',
      name: 'crmNextGenVersion',
      desc: '',
      args: [],
    );
  }

  /// `CRM NextGen`
  String get crmNextGen {
    return Intl.message(
      'CRM NextGen',
      name: 'crmNextGen',
      desc: '',
      args: [],
    );
  }

  /// `Guideline`
  String get guideLine {
    return Intl.message(
      'Guideline',
      name: 'guideLine',
      desc: '',
      args: [],
    );
  }

  /// `Support`
  String get support {
    return Intl.message(
      'Support',
      name: 'support',
      desc: '',
      args: [],
    );
  }

  /// `Nam`
  String get Man {
    return Intl.message(
      'Nam',
      name: 'Man',
      desc: '',
      args: [],
    );
  }

  /// `Nữ`
  String get Woman {
    return Intl.message(
      'Nữ',
      name: 'Woman',
      desc: '',
      args: [],
    );
  }

  /// `Thông tin người dùng`
  String get userInfo {
    return Intl.message(
      'Thông tin người dùng',
      name: 'userInfo',
      desc: '',
      args: [],
    );
  }

  /// `Mã NV`
  String get empCode {
    return Intl.message(
      'Mã NV',
      name: 'empCode',
      desc: '',
      args: [],
    );
  }

  /// `Mã RM`
  String get rmCode {
    return Intl.message(
      'Mã RM',
      name: 'rmCode',
      desc: '',
      args: [],
    );
  }

  /// `Branch code`
  String get branchCode {
    return Intl.message(
      'Branch code',
      name: 'branchCode',
      desc: '',
      args: [],
    );
  }

  /// `Tên Chi Nhánh`
  String get branchName {
    return Intl.message(
      'Tên Chi Nhánh',
      name: 'branchName',
      desc: '',
      args: [],
    );
  }

  /// `Công cụ & Tiện ích`
  String get toolsAndUtils {
    return Intl.message(
      'Công cụ & Tiện ích',
      name: 'toolsAndUtils',
      desc: '',
      args: [],
    );
  }

  /// `Tính toán tiết kiệm`
  String get calculateSaving {
    return Intl.message(
      'Tính toán tiết kiệm',
      name: 'calculateSaving',
      desc: '',
      args: [],
    );
  }

  /// `Số tiền gửi (đ)`
  String get depositAmount {
    return Intl.message(
      'Số tiền gửi (đ)',
      name: 'depositAmount',
      desc: '',
      args: [],
    );
  }

  /// `Kỳ hạn gửi`
  String get depositTerm {
    return Intl.message(
      'Kỳ hạn gửi',
      name: 'depositTerm',
      desc: '',
      args: [],
    );
  }

  /// `Lãi suất (%) năm`
  String get interestRatePerYear {
    return Intl.message(
      'Lãi suất (%) năm',
      name: 'interestRatePerYear',
      desc: '',
      args: [],
    );
  }

  /// `Nhập lại`
  String get retype {
    return Intl.message(
      'Nhập lại',
      name: 'retype',
      desc: '',
      args: [],
    );
  }

  /// `Tính toán`
  String get calculate {
    return Intl.message(
      'Tính toán',
      name: 'calculate',
      desc: '',
      args: [],
    );
  }

  /// `KẾT QUẢ DỰ TÍNH`
  String get estimatedResults {
    return Intl.message(
      'KẾT QUẢ DỰ TÍNH',
      name: 'estimatedResults',
      desc: '',
      args: [],
    );
  }

  /// `Tổng tiền lãi cuối kỳ:`
  String get totalInterestEndPeriod {
    return Intl.message(
      'Tổng tiền lãi cuối kỳ:',
      name: 'totalInterestEndPeriod',
      desc: '',
      args: [],
    );
  }

  /// `Tổng tiền lãi và gốc cuối kỳ:`
  String get totalInterestPrincipalEndPeriod {
    return Intl.message(
      'Tổng tiền lãi và gốc cuối kỳ:',
      name: 'totalInterestPrincipalEndPeriod',
      desc: '',
      args: [],
    );
  }

  /// `Ngân hàng`
  String get bank {
    return Intl.message(
      'Ngân hàng',
      name: 'bank',
      desc: '',
      args: [],
    );
  }

  /// `Lãi suất`
  String get interestRate {
    return Intl.message(
      'Lãi suất',
      name: 'interestRate',
      desc: '',
      args: [],
    );
  }

  /// `Tỉ giá niêm yết`
  String get exchangeRate {
    return Intl.message(
      'Tỉ giá niêm yết',
      name: 'exchangeRate',
      desc: '',
      args: [],
    );
  }

  /// `Tỉ giá liên ngân hàng (Interbank)`
  String get interbankRate {
    return Intl.message(
      'Tỉ giá liên ngân hàng (Interbank)',
      name: 'interbankRate',
      desc: '',
      args: [],
    );
  }

  /// `Ngày`
  String get day {
    return Intl.message(
      'Ngày',
      name: 'day',
      desc: '',
      args: [],
    );
  }

  /// `Tháng`
  String get month {
    return Intl.message(
      'Tháng',
      name: 'month',
      desc: '',
      args: [],
    );
  }

  /// `Năm`
  String get year {
    return Intl.message(
      'Năm',
      name: 'year',
      desc: '',
      args: [],
    );
  }

  /// `Khách hàng`
  String get customer {
    return Intl.message(
      'Khách hàng',
      name: 'customer',
      desc: '',
      args: [],
    );
  }

  /// `KH 360`
  String get customer360 {
    return Intl.message(
      'KH 360',
      name: 'customer360',
      desc: '',
      args: [],
    );
  }

  /// `Dashboard`
  String get dashboard {
    return Intl.message(
      'Dashboard',
      name: 'dashboard',
      desc: '',
      args: [],
    );
  }

  /// `RM 360`
  String get rm360 {
    return Intl.message(
      'RM 360',
      name: 'rm360',
      desc: '',
      args: [],
    );
  }

  /// `Campaign`
  String get campaign {
    return Intl.message(
      'Campaign',
      name: 'campaign',
      desc: '',
      args: [],
    );
  }

  /// `Sales`
  String get sales {
    return Intl.message(
      'Sales',
      name: 'sales',
      desc: '',
      args: [],
    );
  }

  /// `SaleKit`
  String get saleKit {
    return Intl.message(
      'SaleKit',
      name: 'saleKit',
      desc: '',
      args: [],
    );
  }

  /// `Warning`
  String get warning {
    return Intl.message(
      'Warning',
      name: 'warning',
      desc: '',
      args: [],
    );
  }

  /// `Setting`
  String get setting {
    return Intl.message(
      'Setting',
      name: 'setting',
      desc: '',
      args: [],
    );
  }

  /// `Private`
  String get private {
    return Intl.message(
      'Private',
      name: 'private',
      desc: '',
      args: [],
    );
  }

  /// `Public`
  String get public {
    return Intl.message(
      'Public',
      name: 'public',
      desc: '',
      args: [],
    );
  }

  /// `Active`
  String get active {
    return Intl.message(
      'Active',
      name: 'active',
      desc: '',
      args: [],
    );
  }

  /// `Inactive`
  String get inactive {
    return Intl.message(
      'Inactive',
      name: 'inactive',
      desc: '',
      args: [],
    );
  }

  /// `Sort`
  String get sort {
    return Intl.message(
      'Sort',
      name: 'sort',
      desc: '',
      args: [],
    );
  }

  /// `record`
  String get record {
    return Intl.message(
      'record',
      name: 'record',
      desc: '',
      args: [],
    );
  }

  /// `Basic search`
  String get basicSearch {
    return Intl.message(
      'Basic search',
      name: 'basicSearch',
      desc: '',
      args: [],
    );
  }

  /// `Advance search`
  String get advanceSearch {
    return Intl.message(
      'Advance search',
      name: 'advanceSearch',
      desc: '',
      args: [],
    );
  }

  /// `ID card`
  String get idCard {
    return Intl.message(
      'ID card',
      name: 'idCard',
      desc: '',
      args: [],
    );
  }

  /// `Customer code`
  String get customerCode {
    return Intl.message(
      'Customer code',
      name: 'customerCode',
      desc: '',
      args: [],
    );
  }

  /// `Phone`
  String get phone {
    return Intl.message(
      'Phone',
      name: 'phone',
      desc: '',
      args: [],
    );
  }

  /// `Dob`
  String get dateOfBirth {
    return Intl.message(
      'Dob',
      name: 'dateOfBirth',
      desc: '',
      args: [],
    );
  }

  /// `Group`
  String get group {
    return Intl.message(
      'Group',
      name: 'group',
      desc: '',
      args: [],
    );
  }

  /// `Customer 360/Details information`
  String get customerDetailTitle {
    return Intl.message(
      'Customer 360/Details information',
      name: 'customerDetailTitle',
      desc: '',
      args: [],
    );
  }

  /// `General information`
  String get generalInformation {
    return Intl.message(
      'General information',
      name: 'generalInformation',
      desc: '',
      args: [],
    );
  }

  /// `Service product`
  String get serviceProduct {
    return Intl.message(
      'Service product',
      name: 'serviceProduct',
      desc: '',
      args: [],
    );
  }

  /// `Transaction history`
  String get transactionHistory {
    return Intl.message(
      'Transaction history',
      name: 'transactionHistory',
      desc: '',
      args: [],
    );
  }

  /// `Other information`
  String get otherInformation {
    return Intl.message(
      'Other information',
      name: 'otherInformation',
      desc: '',
      args: [],
    );
  }

  /// `Customer information`
  String get customerInformation {
    return Intl.message(
      'Customer information',
      name: 'customerInformation',
      desc: '',
      args: [],
    );
  }

  /// `Customer name`
  String get customerName {
    return Intl.message(
      'Customer name',
      name: 'customerName',
      desc: '',
      args: [],
    );
  }

  /// `Identification`
  String get identification {
    return Intl.message(
      'Identification',
      name: 'identification',
      desc: '',
      args: [],
    );
  }

  /// `Block`
  String get block {
    return Intl.message(
      'Block',
      name: 'block',
      desc: '',
      args: [],
    );
  }

  /// `Sector`
  String get sector {
    return Intl.message(
      'Sector',
      name: 'sector',
      desc: '',
      args: [],
    );
  }

  /// `Status`
  String get status {
    return Intl.message(
      'Status',
      name: 'status',
      desc: '',
      args: [],
    );
  }

  /// `Sex`
  String get sex {
    return Intl.message(
      'Sex',
      name: 'sex',
      desc: '',
      args: [],
    );
  }

  /// `Marital status`
  String get maritalStatus {
    return Intl.message(
      'Marital status',
      name: 'maritalStatus',
      desc: '',
      args: [],
    );
  }

  /// `Age group`
  String get ageGroup {
    return Intl.message(
      'Age group',
      name: 'ageGroup',
      desc: '',
      args: [],
    );
  }

  /// `Address`
  String get address {
    return Intl.message(
      'Address',
      name: 'address',
      desc: '',
      args: [],
    );
  }

  /// `Nationality`
  String get nationality {
    return Intl.message(
      'Nationality',
      name: 'nationality',
      desc: '',
      args: [],
    );
  }

  /// `Branch management`
  String get branchManagement {
    return Intl.message(
      'Branch management',
      name: 'branchManagement',
      desc: '',
      args: [],
    );
  }

  /// `Priority`
  String get priority {
    return Intl.message(
      'Priority',
      name: 'priority',
      desc: '',
      args: [],
    );
  }

  /// `Income and Job`
  String get incomeAndJob {
    return Intl.message(
      'Income and Job',
      name: 'incomeAndJob',
      desc: '',
      args: [],
    );
  }

  /// `Job`
  String get job {
    return Intl.message(
      'Job',
      name: 'job',
      desc: '',
      args: [],
    );
  }

  /// `Income vnd`
  String get incomeVnd {
    return Intl.message(
      'Income vnd',
      name: 'incomeVnd',
      desc: '',
      args: [],
    );
  }

  /// `Company pays salary`
  String get companyPaysSalary {
    return Intl.message(
      'Company pays salary',
      name: 'companyPaysSalary',
      desc: '',
      args: [],
    );
  }

  /// `Formality`
  String get formality {
    return Intl.message(
      'Formality',
      name: 'formality',
      desc: '',
      args: [],
    );
  }

  /// `Average salary 3 months vnd`
  String get averageSalaryThreeMonths {
    return Intl.message(
      'Average salary 3 months vnd',
      name: 'averageSalaryThreeMonths',
      desc: '',
      args: [],
    );
  }

  /// `Others information`
  String get othersInformation {
    return Intl.message(
      'Others information',
      name: 'othersInformation',
      desc: '',
      args: [],
    );
  }

  /// `Code open date`
  String get codeOpenDate {
    return Intl.message(
      'Code open date',
      name: 'codeOpenDate',
      desc: '',
      args: [],
    );
  }

  /// `Time relationship MB`
  String get timeRelationshipMB {
    return Intl.message(
      'Time relationship MB',
      name: 'timeRelationshipMB',
      desc: '',
      args: [],
    );
  }

  /// `Current dept MB`
  String get currentDeptMB {
    return Intl.message(
      'Current dept MB',
      name: 'currentDeptMB',
      desc: '',
      args: [],
    );
  }

  /// `Dept MB`
  String get deptMB {
    return Intl.message(
      'Dept MB',
      name: 'deptMB',
      desc: '',
      args: [],
    );
  }

  /// `Payroll`
  String get payroll {
    return Intl.message(
      'Payroll',
      name: 'payroll',
      desc: '',
      args: [],
    );
  }

  /// `kh`
  String get customer_short {
    return Intl.message(
      'kh',
      name: 'customer_short',
      desc: '',
      args: [],
    );
  }

  /// `segment`
  String get segment {
    return Intl.message(
      'segment',
      name: 'segment',
      desc: '',
      args: [],
    );
  }

  /// `private/Priority`
  String get privatePriority {
    return Intl.message(
      'private/Priority',
      name: 'privatePriority',
      desc: '',
      args: [],
    );
  }

  /// `Number of used products and services`
  String get numberUsedProductsServices {
    return Intl.message(
      'Number of used products and services',
      name: 'numberUsedProductsServices',
      desc: '',
      args: [],
    );
  }

  /// `The masses are well-off`
  String get theMassesAreWellOff {
    return Intl.message(
      'The masses are well-off',
      name: 'theMassesAreWellOff',
      desc: '',
      args: [],
    );
  }

  /// `Mass - Individual customers`
  String get massIndividualCustomers {
    return Intl.message(
      'Mass - Individual customers',
      name: 'massIndividualCustomers',
      desc: '',
      args: [],
    );
  }

  /// `Mass - Digital banking`
  String get massDigitalBanking {
    return Intl.message(
      'Mass - Digital banking',
      name: 'massDigitalBanking',
      desc: '',
      args: [],
    );
  }

  /// `Not rate yet`
  String get notRatedYet {
    return Intl.message(
      'Not rate yet',
      name: 'notRatedYet',
      desc: '',
      args: [],
    );
  }

  /// `Non-VIP`
  String get nonVip {
    return Intl.message(
      'Non-VIP',
      name: 'nonVip',
      desc: '',
      args: [],
    );
  }

  /// `SP`
  String get shortProduct {
    return Intl.message(
      'SP',
      name: 'shortProduct',
      desc: '',
      args: [],
    );
  }

  /// `Things to do`
  String get thingsToDo {
    return Intl.message(
      'Things to do',
      name: 'thingsToDo',
      desc: '',
      args: [],
    );
  }

  /// `Work schedule`
  String get workSchedule {
    return Intl.message(
      'Work schedule',
      name: 'workSchedule',
      desc: '',
      args: [],
    );
  }

  /// `High priority`
  String get highPriority {
    return Intl.message(
      'High priority',
      name: 'highPriority',
      desc: '',
      args: [],
    );
  }

  /// `Medium priority`
  String get mediumPriority {
    return Intl.message(
      'Medium priority',
      name: 'mediumPriority',
      desc: '',
      args: [],
    );
  }

  /// `Low priority`
  String get lowPriority {
    return Intl.message(
      'Low priority',
      name: 'lowPriority',
      desc: '',
      args: [],
    );
  }

  /// `Sunday`
  String get sunday {
    return Intl.message(
      'Sunday',
      name: 'sunday',
      desc: '',
      args: [],
    );
  }

  /// `Sun`
  String get shortSunday {
    return Intl.message(
      'Sun',
      name: 'shortSunday',
      desc: '',
      args: [],
    );
  }

  /// `Monday`
  String get monday {
    return Intl.message(
      'Monday',
      name: 'monday',
      desc: '',
      args: [],
    );
  }

  /// `Mon`
  String get shortMonday {
    return Intl.message(
      'Mon',
      name: 'shortMonday',
      desc: '',
      args: [],
    );
  }

  /// `Tuesday`
  String get tuesday {
    return Intl.message(
      'Tuesday',
      name: 'tuesday',
      desc: '',
      args: [],
    );
  }

  /// `Tue`
  String get shortTuesday {
    return Intl.message(
      'Tue',
      name: 'shortTuesday',
      desc: '',
      args: [],
    );
  }

  /// `Wednesday`
  String get wednesday {
    return Intl.message(
      'Wednesday',
      name: 'wednesday',
      desc: '',
      args: [],
    );
  }

  /// `Wed`
  String get shortWednesday {
    return Intl.message(
      'Wed',
      name: 'shortWednesday',
      desc: '',
      args: [],
    );
  }

  /// `Thursday`
  String get Thursday {
    return Intl.message(
      'Thursday',
      name: 'Thursday',
      desc: '',
      args: [],
    );
  }

  /// `Thu`
  String get shortThursday {
    return Intl.message(
      'Thu',
      name: 'shortThursday',
      desc: '',
      args: [],
    );
  }

  /// `Friday`
  String get friday {
    return Intl.message(
      'Friday',
      name: 'friday',
      desc: '',
      args: [],
    );
  }

  /// `Fri`
  String get shortFriday {
    return Intl.message(
      'Fri',
      name: 'shortFriday',
      desc: '',
      args: [],
    );
  }

  /// `Saturday`
  String get saturday {
    return Intl.message(
      'Saturday',
      name: 'saturday',
      desc: '',
      args: [],
    );
  }

  /// `Sat`
  String get shortSaturday {
    return Intl.message(
      'Sat',
      name: 'shortSaturday',
      desc: '',
      args: [],
    );
  }

  /// `Create job`
  String get createJob {
    return Intl.message(
      'Create job',
      name: 'createJob',
      desc: '',
      args: [],
    );
  }

  /// `Job title`
  String get jobTitle {
    return Intl.message(
      'Job title',
      name: 'jobTitle',
      desc: '',
      args: [],
    );
  }

  /// `To date`
  String get toDate {
    return Intl.message(
      'To date',
      name: 'toDate',
      desc: '',
      args: [],
    );
  }

  /// `High`
  String get high {
    return Intl.message(
      'High',
      name: 'high',
      desc: '',
      args: [],
    );
  }

  /// `Medium`
  String get medium {
    return Intl.message(
      'Medium',
      name: 'medium',
      desc: '',
      args: [],
    );
  }

  /// `Low`
  String get low {
    return Intl.message(
      'Low',
      name: 'low',
      desc: '',
      args: [],
    );
  }

  /// `Job content`
  String get jobContent {
    return Intl.message(
      'Job content',
      name: 'jobContent',
      desc: '',
      args: [],
    );
  }

  /// `Job location`
  String get jobLocation {
    return Intl.message(
      'Job location',
      name: 'jobLocation',
      desc: '',
      args: [],
    );
  }

  /// `Create to do`
  String get createTodo {
    return Intl.message(
      'Create to do',
      name: 'createTodo',
      desc: '',
      args: [],
    );
  }

  /// `To do title`
  String get toDoTitle {
    return Intl.message(
      'To do title',
      name: 'toDoTitle',
      desc: '',
      args: [],
    );
  }

  /// `To do content`
  String get toDoContent {
    return Intl.message(
      'To do content',
      name: 'toDoContent',
      desc: '',
      args: [],
    );
  }

  /// `To do details`
  String get toDoDetails {
    return Intl.message(
      'To do details',
      name: 'toDoDetails',
      desc: '',
      args: [],
    );
  }

  /// `Complete this to do?`
  String get completeToDo {
    return Intl.message(
      'Complete this to do?',
      name: 'completeToDo',
      desc: '',
      args: [],
    );
  }

  /// `Message`
  String get message {
    return Intl.message(
      'Message',
      name: 'message',
      desc: '',
      args: [],
    );
  }

  /// `Confirm`
  String get confirm {
    return Intl.message(
      'Confirm',
      name: 'confirm',
      desc: '',
      args: [],
    );
  }

  /// `Delete`
  String get delete {
    return Intl.message(
      'Delete',
      name: 'delete',
      desc: '',
      args: [],
    );
  }

  /// `No data`
  String get noDate {
    return Intl.message(
      'No data',
      name: 'noDate',
      desc: '',
      args: [],
    );
  }

  /// `Đã xảy ra lỗi`
  String get errorOccurred {
    return Intl.message(
      'Đã xảy ra lỗi',
      name: 'errorOccurred',
      desc: '',
      args: [],
    );
  }

  /// `Error`
  String get error {
    return Intl.message(
      'Error',
      name: 'error',
      desc: '',
      args: [],
    );
  }

  /// `Cập nhật lần cuối lúc:`
  String get lastUpdateAt {
    return Intl.message(
      'Cập nhật lần cuối lúc:',
      name: 'lastUpdateAt',
      desc: '',
      args: [],
    );
  }

  /// `Loại tiền tệ`
  String get currency {
    return Intl.message(
      'Loại tiền tệ',
      name: 'currency',
      desc: '',
      args: [],
    );
  }

  /// `Giá ITB mua`
  String get itbPurchasePrice {
    return Intl.message(
      'Giá ITB mua',
      name: 'itbPurchasePrice',
      desc: '',
      args: [],
    );
  }

  /// `Giá ITB bán`
  String get itpSellingPrice {
    return Intl.message(
      'Giá ITB bán',
      name: 'itpSellingPrice',
      desc: '',
      args: [],
    );
  }

  /// `Khu vực`
  String get area {
    return Intl.message(
      'Khu vực',
      name: 'area',
      desc: '',
      args: [],
    );
  }

  /// `Khách hàng doanh nghiệp`
  String get sme {
    return Intl.message(
      'Khách hàng doanh nghiệp',
      name: 'sme',
      desc: '',
      args: [],
    );
  }

  /// `Khách hàng cá nhân`
  String get indiv {
    return Intl.message(
      'Khách hàng cá nhân',
      name: 'indiv',
      desc: '',
      args: [],
    );
  }

  /// `VÍ DỤ MINH HỌA`
  String get illustrativeExampleUpper {
    return Intl.message(
      'VÍ DỤ MINH HỌA',
      name: 'illustrativeExampleUpper',
      desc: '',
      args: [],
    );
  }

  /// `Hôm nay`
  String get toDay {
    return Intl.message(
      'Hôm nay',
      name: 'toDay',
      desc: '',
      args: [],
    );
  }

  /// `Ngày mai`
  String get tomorrow {
    return Intl.message(
      'Ngày mai',
      name: 'tomorrow',
      desc: '',
      args: [],
    );
  }

  /// `Tuần này`
  String get thisWeek {
    return Intl.message(
      'Tuần này',
      name: 'thisWeek',
      desc: '',
      args: [],
    );
  }

  /// `Week`
  String get week {
    return Intl.message(
      'Week',
      name: 'week',
      desc: '',
      args: [],
    );
  }

  /// `All`
  String get all {
    return Intl.message(
      'All',
      name: 'all',
      desc: '',
      args: [],
    );
  }

  /// `Daily exchange rate: `
  String get dailyExchangeRate {
    return Intl.message(
      'Daily exchange rate: ',
      name: 'dailyExchangeRate',
      desc: '',
      args: [],
    );
  }

  /// `Foreign currency`
  String get foreignCurrency {
    return Intl.message(
      'Foreign currency',
      name: 'foreignCurrency',
      desc: '',
      args: [],
    );
  }

  /// `Transfer buy`
  String get transferBuy {
    return Intl.message(
      'Transfer buy',
      name: 'transferBuy',
      desc: '',
      args: [],
    );
  }

  /// `Transfer sale`
  String get transferSale {
    return Intl.message(
      'Transfer sale',
      name: 'transferSale',
      desc: '',
      args: [],
    );
  }

  /// `Cash buy`
  String get cashBuy {
    return Intl.message(
      'Cash buy',
      name: 'cashBuy',
      desc: '',
      args: [],
    );
  }

  /// `Tín dụng`
  String get credit {
    return Intl.message(
      'Tín dụng',
      name: 'credit',
      desc: '',
      args: [],
    );
  }

  /// `Tiết kiệm`
  String get savings {
    return Intl.message(
      'Tiết kiệm',
      name: 'savings',
      desc: '',
      args: [],
    );
  }

  /// `Bscore`
  String get bscore {
    return Intl.message(
      'Bscore',
      name: 'bscore',
      desc: '',
      args: [],
    );
  }

  /// `Cảnh báo cao`
  String get highWarning {
    return Intl.message(
      'Cảnh báo cao',
      name: 'highWarning',
      desc: '',
      args: [],
    );
  }

  /// `Nhóm theo KH`
  String get customerGroup {
    return Intl.message(
      'Nhóm theo KH',
      name: 'customerGroup',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'vi'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
