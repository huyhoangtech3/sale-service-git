// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a vi locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'vi';

  static String m0(errorCode) => "unknownException (${errorCode})";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "Man": MessageLookupByLibrary.simpleMessage("Nam"),
        "Thursday": MessageLookupByLibrary.simpleMessage("Thứ năm"),
        "Woman": MessageLookupByLibrary.simpleMessage("Nữ"),
        "active": MessageLookupByLibrary.simpleMessage("Hoạt động"),
        "address": MessageLookupByLibrary.simpleMessage("Địa chỉ"),
        "advanceSearch":
            MessageLookupByLibrary.simpleMessage("Tìm kiếm nâng cao"),
        "ageGroup": MessageLookupByLibrary.simpleMessage("Nhớm tuổi"),
        "all": MessageLookupByLibrary.simpleMessage("Tất cả"),
        "appBarTitle": MessageLookupByLibrary.simpleMessage("Smart SME"),
        "area": MessageLookupByLibrary.simpleMessage("Khu vực"),
        "averageSalaryThreeMonths": MessageLookupByLibrary.simpleMessage(
            "Lương trung bình 3 tháng gần nhất đ"),
        "badCertificateException":
            MessageLookupByLibrary.simpleMessage("badCertificateException"),
        "bank": MessageLookupByLibrary.simpleMessage("Ngân hàng"),
        "basicSearch": MessageLookupByLibrary.simpleMessage("Tìm kiếm cơ bản"),
        "block": MessageLookupByLibrary.simpleMessage("Khối"),
        "branchCode": MessageLookupByLibrary.simpleMessage("Mã CN"),
        "branchManagement":
            MessageLookupByLibrary.simpleMessage("Chi nhánh quản lý"),
        "branchName": MessageLookupByLibrary.simpleMessage("Tên Chi Nhánh"),
        "bscore": MessageLookupByLibrary.simpleMessage("Bscore"),
        "calculate": MessageLookupByLibrary.simpleMessage("Tính toán"),
        "calculateSaving":
            MessageLookupByLibrary.simpleMessage("Tính toán tiết kiệm"),
        "campaign": MessageLookupByLibrary.simpleMessage("Chiến dịch"),
        "canNotConnectToHost": MessageLookupByLibrary.simpleMessage(
            "Can not connect to this host"),
        "cancel": MessageLookupByLibrary.simpleMessage("Hủy"),
        "cancellationException":
            MessageLookupByLibrary.simpleMessage("cancellationException"),
        "cashBuy": MessageLookupByLibrary.simpleMessage("Mua tiền mặt"),
        "close": MessageLookupByLibrary.simpleMessage("Đóng"),
        "codeOpenDate": MessageLookupByLibrary.simpleMessage("Ngày mở code"),
        "companyPaysSalary":
            MessageLookupByLibrary.simpleMessage("Công ty trả lương"),
        "completeToDo": MessageLookupByLibrary.simpleMessage(
            "Hoàn thành việc cần làm này?"),
        "confirm": MessageLookupByLibrary.simpleMessage("Xác nhận"),
        "createJob": MessageLookupByLibrary.simpleMessage("Tạo công việc"),
        "createTodo": MessageLookupByLibrary.simpleMessage("Tạo việc cần làm"),
        "credit": MessageLookupByLibrary.simpleMessage("Tín dụng"),
        "crmNextGen": MessageLookupByLibrary.simpleMessage("CRM NextGen"),
        "crmNextGenVersion":
            MessageLookupByLibrary.simpleMessage("CRM NextGen phiên bẩn"),
        "currency": MessageLookupByLibrary.simpleMessage("Loại tiền tệ"),
        "currentDeptMB":
            MessageLookupByLibrary.simpleMessage("Nhóm nợ hiện tại CIC"),
        "customer": MessageLookupByLibrary.simpleMessage("Khách hàng"),
        "customer360": MessageLookupByLibrary.simpleMessage("KH 360"),
        "customerCode": MessageLookupByLibrary.simpleMessage("Mã KH"),
        "customerDetailTitle": MessageLookupByLibrary.simpleMessage(
            "Khách hàng 360/Thông tin chi tiết"),
        "customerGroup": MessageLookupByLibrary.simpleMessage("Nhóm theo KH"),
        "customerInformation":
            MessageLookupByLibrary.simpleMessage("Thông tin khách hàng"),
        "customerName": MessageLookupByLibrary.simpleMessage("Tên khách hàng"),
        "customer_short": MessageLookupByLibrary.simpleMessage("kh"),
        "dailyExchangeRate":
            MessageLookupByLibrary.simpleMessage("Tỷ giá hối đoái ngày: "),
        "darkTheme": MessageLookupByLibrary.simpleMessage("Dark Theme"),
        "dashboard": MessageLookupByLibrary.simpleMessage("Dashboard"),
        "dateOfBirth": MessageLookupByLibrary.simpleMessage("Ngày sinh"),
        "day": MessageLookupByLibrary.simpleMessage("Ngày"),
        "delete": MessageLookupByLibrary.simpleMessage("Xóa"),
        "depositAmount":
            MessageLookupByLibrary.simpleMessage("Số tiền gửi (đ)"),
        "depositTerm": MessageLookupByLibrary.simpleMessage("Kỳ hạn gửi"),
        "deptMB": MessageLookupByLibrary.simpleMessage("Nhớm nợ tại MB"),
        "email": MessageLookupByLibrary.simpleMessage("Thư điện tử"),
        "empCode": MessageLookupByLibrary.simpleMessage("Mã NV"),
        "emptyEmail": MessageLookupByLibrary.simpleMessage("emptyEmail"),
        "error": MessageLookupByLibrary.simpleMessage("Lỗi"),
        "errorOccurred": MessageLookupByLibrary.simpleMessage("Đã xảy ra lỗi"),
        "estimatedResults":
            MessageLookupByLibrary.simpleMessage("KẾT QUẢ DỰ TÍNH"),
        "exchangeRate": MessageLookupByLibrary.simpleMessage("Tỉ giá niêm yết"),
        "fakeLogin": MessageLookupByLibrary.simpleMessage("Fake Login"),
        "foreignCurrency": MessageLookupByLibrary.simpleMessage("Ngoại tệ"),
        "formality": MessageLookupByLibrary.simpleMessage("Hình thức"),
        "friday": MessageLookupByLibrary.simpleMessage("Thứ 6"),
        "generalInformation":
            MessageLookupByLibrary.simpleMessage("Thông tin chung"),
        "group": MessageLookupByLibrary.simpleMessage("Nhóm"),
        "guideLine": MessageLookupByLibrary.simpleMessage("Hướng dẫn"),
        "high": MessageLookupByLibrary.simpleMessage("Cao"),
        "highPriority": MessageLookupByLibrary.simpleMessage("Ưu tiên cao"),
        "highWarning": MessageLookupByLibrary.simpleMessage("Cảnh báo cao"),
        "hintPassword":
            MessageLookupByLibrary.simpleMessage("Nhập mật khẩu của bạn"),
        "hintUserName":
            MessageLookupByLibrary.simpleMessage("Nhập thư điện tử của bạn"),
        "home": MessageLookupByLibrary.simpleMessage("Home"),
        "idCard": MessageLookupByLibrary.simpleMessage("CCCD"),
        "identification":
            MessageLookupByLibrary.simpleMessage("Giấy tờ tùy thân"),
        "illustrativeExampleUpper":
            MessageLookupByLibrary.simpleMessage("VÍ DỤ MINH HỌA"),
        "inactive": MessageLookupByLibrary.simpleMessage("Không hoạt động"),
        "incomeAndJob":
            MessageLookupByLibrary.simpleMessage("Nghề nghiệp và thu nhập"),
        "incomeVnd": MessageLookupByLibrary.simpleMessage("Thu nhập đ"),
        "indiv": MessageLookupByLibrary.simpleMessage("Khách hàng cá nhân"),
        "interbankRate": MessageLookupByLibrary.simpleMessage(
            "Tỉ giá liên ngân hàng (Interbank)"),
        "interestRate": MessageLookupByLibrary.simpleMessage("Lãi suất"),
        "interestRatePerYear":
            MessageLookupByLibrary.simpleMessage("Lãi suất (%) năm"),
        "invalidDateTime":
            MessageLookupByLibrary.simpleMessage("invalidDateTime"),
        "invalidEmail": MessageLookupByLibrary.simpleMessage("invalidEmail"),
        "invalidPassword":
            MessageLookupByLibrary.simpleMessage("invalidPassword"),
        "invalidPhoneNumber":
            MessageLookupByLibrary.simpleMessage("invalidPhoneNumber"),
        "invalidUserName":
            MessageLookupByLibrary.simpleMessage("invalidUserName"),
        "itbPurchasePrice": MessageLookupByLibrary.simpleMessage("Giá ITB mua"),
        "itpSellingPrice": MessageLookupByLibrary.simpleMessage("Giá ITB bán"),
        "job": MessageLookupByLibrary.simpleMessage("Nghề nghiệp"),
        "jobContent":
            MessageLookupByLibrary.simpleMessage("Nội dung công việc"),
        "jobLocation":
            MessageLookupByLibrary.simpleMessage("Địa điểm công việc"),
        "jobTitle": MessageLookupByLibrary.simpleMessage("Tiêu đề công việc"),
        "lastUpdateAt":
            MessageLookupByLibrary.simpleMessage("Cập nhật lần cuối lúc:"),
        "login": MessageLookupByLibrary.simpleMessage("Đăng nhập"),
        "logout": MessageLookupByLibrary.simpleMessage("Đăng xuất"),
        "low": MessageLookupByLibrary.simpleMessage("Thấp"),
        "lowPriority": MessageLookupByLibrary.simpleMessage("Ưu tiên thấp"),
        "maritalStatus":
            MessageLookupByLibrary.simpleMessage("Tình trạng hôn nhân"),
        "massDigitalBanking":
            MessageLookupByLibrary.simpleMessage("Đại chúng - NHS"),
        "massIndividualCustomers":
            MessageLookupByLibrary.simpleMessage("Đại chúng - KHCN"),
        "medium": MessageLookupByLibrary.simpleMessage("Vừa"),
        "mediumPriority": MessageLookupByLibrary.simpleMessage("Ưu tiên vừa"),
        "message": MessageLookupByLibrary.simpleMessage("Thông báo"),
        "monday": MessageLookupByLibrary.simpleMessage("Thứ hai"),
        "month": MessageLookupByLibrary.simpleMessage("Tháng"),
        "myPage": MessageLookupByLibrary.simpleMessage("My Page"),
        "nationality": MessageLookupByLibrary.simpleMessage("Quốc tịch"),
        "noInternetException":
            MessageLookupByLibrary.simpleMessage("noInternetException"),
        "nonVip": MessageLookupByLibrary.simpleMessage("Không phải là VIP"),
        "notRatedYet": MessageLookupByLibrary.simpleMessage("Chưa đánh giá"),
        "numberUsedProductsServices":
            MessageLookupByLibrary.simpleMessage("Số lượng SPDV đã sử dụng"),
        "ok": MessageLookupByLibrary.simpleMessage("Đồng ý"),
        "otherInformation":
            MessageLookupByLibrary.simpleMessage("Thông tin khác"),
        "othersInformation":
            MessageLookupByLibrary.simpleMessage("Các thông tin khác"),
        "parseException":
            MessageLookupByLibrary.simpleMessage("parseException"),
        "password": MessageLookupByLibrary.simpleMessage("Mật khẩu"),
        "passwordsAreNotMatch":
            MessageLookupByLibrary.simpleMessage("passwordsAreNotMatch"),
        "payroll": MessageLookupByLibrary.simpleMessage("Lương thưởng"),
        "phone": MessageLookupByLibrary.simpleMessage("Điện thoại"),
        "priority": MessageLookupByLibrary.simpleMessage("Ưu tiên"),
        "private": MessageLookupByLibrary.simpleMessage("Cá nhân"),
        "privatePriority":
            MessageLookupByLibrary.simpleMessage("Cá nhân/Ưu tiên"),
        "public": MessageLookupByLibrary.simpleMessage("Public"),
        "record": MessageLookupByLibrary.simpleMessage("bản ghi"),
        "retry": MessageLookupByLibrary.simpleMessage("Thử lại"),
        "retype": MessageLookupByLibrary.simpleMessage("Nhập lại"),
        "rm360": MessageLookupByLibrary.simpleMessage("RM 360"),
        "rmCode": MessageLookupByLibrary.simpleMessage("Mã RM"),
        "saleKit": MessageLookupByLibrary.simpleMessage("SaleKit"),
        "sales": MessageLookupByLibrary.simpleMessage("Bán hàng"),
        "saturday": MessageLookupByLibrary.simpleMessage("Thứ bảy"),
        "savings": MessageLookupByLibrary.simpleMessage("Tiết kiệm"),
        "search": MessageLookupByLibrary.simpleMessage("Tìm kiếm"),
        "sector": MessageLookupByLibrary.simpleMessage("Khu vực"),
        "segment": MessageLookupByLibrary.simpleMessage("Phân khúc"),
        "serviceProduct":
            MessageLookupByLibrary.simpleMessage("Sản phẩm dịch vụ"),
        "setting": MessageLookupByLibrary.simpleMessage("Cài đặt"),
        "sex": MessageLookupByLibrary.simpleMessage("Giới tính"),
        "shortFriday": MessageLookupByLibrary.simpleMessage("T6"),
        "shortMonday": MessageLookupByLibrary.simpleMessage("T2"),
        "shortProduct": MessageLookupByLibrary.simpleMessage("SP"),
        "shortSaturday": MessageLookupByLibrary.simpleMessage("T7"),
        "shortSunday": MessageLookupByLibrary.simpleMessage("CN"),
        "shortThursday": MessageLookupByLibrary.simpleMessage("T5"),
        "shortTuesday": MessageLookupByLibrary.simpleMessage("T3"),
        "shortWednesday": MessageLookupByLibrary.simpleMessage("T4"),
        "sme": MessageLookupByLibrary.simpleMessage("Khách hàng doanh nghiệp"),
        "sort": MessageLookupByLibrary.simpleMessage("Sắp xếp"),
        "status": MessageLookupByLibrary.simpleMessage("Trạng thái"),
        "sunday": MessageLookupByLibrary.simpleMessage("Chủ nhật"),
        "support": MessageLookupByLibrary.simpleMessage("Hỗ trợ tài khoản"),
        "theMassesAreWellOff":
            MessageLookupByLibrary.simpleMessage("Đại chúng khá giả"),
        "thingsToDo": MessageLookupByLibrary.simpleMessage("Việc cần làm"),
        "thisWeek": MessageLookupByLibrary.simpleMessage("Tuần này"),
        "timeRelationshipMB":
            MessageLookupByLibrary.simpleMessage("Thời gian quan hệ với MB"),
        "timeoutException":
            MessageLookupByLibrary.simpleMessage("timeoutException"),
        "toDate": MessageLookupByLibrary.simpleMessage("Đến ngày"),
        "toDay": MessageLookupByLibrary.simpleMessage("Hôm nay"),
        "toDoContent":
            MessageLookupByLibrary.simpleMessage("Nội dung việc cần làm"),
        "toDoDetails":
            MessageLookupByLibrary.simpleMessage("Chi tiết việc cần làm"),
        "toDoTitle":
            MessageLookupByLibrary.simpleMessage("Tiêu đề việc cần làm"),
        "tokenExpired": MessageLookupByLibrary.simpleMessage("tokenExpired"),
        "tomorrow": MessageLookupByLibrary.simpleMessage("Ngày mai"),
        "toolsAndUtils":
            MessageLookupByLibrary.simpleMessage("Công cụ & Tiện ích"),
        "totalInterestEndPeriod":
            MessageLookupByLibrary.simpleMessage("Tổng tiền lãi cuối kỳ:"),
        "totalInterestPrincipalEndPeriod": MessageLookupByLibrary.simpleMessage(
            "Tổng tiền lãi và gốc cuối kỳ:"),
        "transactionHistory":
            MessageLookupByLibrary.simpleMessage("Lịch sử giao dịch"),
        "transferBuy": MessageLookupByLibrary.simpleMessage("Mua chuyển khoản"),
        "transferSale":
            MessageLookupByLibrary.simpleMessage("Bán chuyển khoản"),
        "tuesday": MessageLookupByLibrary.simpleMessage("Thứ ba"),
        "unknownException": m0,
        "userInfo":
            MessageLookupByLibrary.simpleMessage("Thông tin người dùng"),
        "userName": MessageLookupByLibrary.simpleMessage("Tên đăng nhập"),
        "version":
            MessageLookupByLibrary.simpleMessage("CRM Nextgen version 1.0"),
        "vietnamese": MessageLookupByLibrary.simpleMessage("Vietnamese"),
        "warning": MessageLookupByLibrary.simpleMessage("Cảnh báo"),
        "wednesday": MessageLookupByLibrary.simpleMessage("Thứ tư"),
        "week": MessageLookupByLibrary.simpleMessage("Tuần"),
        "welcomeLoginPage":
            MessageLookupByLibrary.simpleMessage("Xin chào bạn đã đến với"),
        "workSchedule": MessageLookupByLibrary.simpleMessage("Lịch làm việc"),
        "year": MessageLookupByLibrary.simpleMessage("Năm")
      };
}
