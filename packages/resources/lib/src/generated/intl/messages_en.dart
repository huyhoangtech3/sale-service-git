// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  static String m0(errorCode) => "unknownException (${errorCode})";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "Man": MessageLookupByLibrary.simpleMessage("Nam"),
        "Thursday": MessageLookupByLibrary.simpleMessage("Thursday"),
        "Woman": MessageLookupByLibrary.simpleMessage("Nữ"),
        "active": MessageLookupByLibrary.simpleMessage("Active"),
        "address": MessageLookupByLibrary.simpleMessage("Address"),
        "advanceSearch": MessageLookupByLibrary.simpleMessage("Advance search"),
        "ageGroup": MessageLookupByLibrary.simpleMessage("Age group"),
        "all": MessageLookupByLibrary.simpleMessage("All"),
        "appBarTitle": MessageLookupByLibrary.simpleMessage("Smart SME"),
        "area": MessageLookupByLibrary.simpleMessage("Khu vực"),
        "averageSalaryThreeMonths":
            MessageLookupByLibrary.simpleMessage("Average salary 3 months vnd"),
        "badCertificateException":
            MessageLookupByLibrary.simpleMessage("badCertificateException"),
        "bank": MessageLookupByLibrary.simpleMessage("Ngân hàng"),
        "basicSearch": MessageLookupByLibrary.simpleMessage("Basic search"),
        "block": MessageLookupByLibrary.simpleMessage("Block"),
        "branchCode": MessageLookupByLibrary.simpleMessage("Branch code"),
        "branchManagement":
            MessageLookupByLibrary.simpleMessage("Branch management"),
        "branchName": MessageLookupByLibrary.simpleMessage("Tên Chi Nhánh"),
        "bscore": MessageLookupByLibrary.simpleMessage("Bscore"),
        "calculate": MessageLookupByLibrary.simpleMessage("Tính toán"),
        "calculateSaving":
            MessageLookupByLibrary.simpleMessage("Tính toán tiết kiệm"),
        "campaign": MessageLookupByLibrary.simpleMessage("Campaign"),
        "canNotConnectToHost": MessageLookupByLibrary.simpleMessage(
            "Can not connect to this host"),
        "cancel": MessageLookupByLibrary.simpleMessage("Cancel"),
        "cancellationException":
            MessageLookupByLibrary.simpleMessage("cancellationException"),
        "cashBuy": MessageLookupByLibrary.simpleMessage("Cash buy"),
        "close": MessageLookupByLibrary.simpleMessage("Close"),
        "codeOpenDate": MessageLookupByLibrary.simpleMessage("Code open date"),
        "companyPaysSalary":
            MessageLookupByLibrary.simpleMessage("Company pays salary"),
        "completeToDo":
            MessageLookupByLibrary.simpleMessage("Complete this to do?"),
        "confirm": MessageLookupByLibrary.simpleMessage("Confirm"),
        "createJob": MessageLookupByLibrary.simpleMessage("Create job"),
        "createTodo": MessageLookupByLibrary.simpleMessage("Create to do"),
        "credit": MessageLookupByLibrary.simpleMessage("Tín dụng"),
        "crmNextGen": MessageLookupByLibrary.simpleMessage("CRM NextGen"),
        "crmNextGenVersion":
            MessageLookupByLibrary.simpleMessage("CRM NextGen version"),
        "currency": MessageLookupByLibrary.simpleMessage("Loại tiền tệ"),
        "currentDeptMB":
            MessageLookupByLibrary.simpleMessage("Current dept MB"),
        "customer": MessageLookupByLibrary.simpleMessage("Khách hàng"),
        "customer360": MessageLookupByLibrary.simpleMessage("KH 360"),
        "customerCode": MessageLookupByLibrary.simpleMessage("Customer code"),
        "customerDetailTitle": MessageLookupByLibrary.simpleMessage(
            "Customer 360/Details information"),
        "customerGroup": MessageLookupByLibrary.simpleMessage("Nhóm theo KH"),
        "customerInformation":
            MessageLookupByLibrary.simpleMessage("Customer information"),
        "customerName": MessageLookupByLibrary.simpleMessage("Customer name"),
        "customer_short": MessageLookupByLibrary.simpleMessage("kh"),
        "dailyExchangeRate":
            MessageLookupByLibrary.simpleMessage("Daily exchange rate: "),
        "darkTheme": MessageLookupByLibrary.simpleMessage("Dark Theme"),
        "dashboard": MessageLookupByLibrary.simpleMessage("Dashboard"),
        "dateOfBirth": MessageLookupByLibrary.simpleMessage("Dob"),
        "day": MessageLookupByLibrary.simpleMessage("Ngày"),
        "delete": MessageLookupByLibrary.simpleMessage("Delete"),
        "depositAmount":
            MessageLookupByLibrary.simpleMessage("Số tiền gửi (đ)"),
        "depositTerm": MessageLookupByLibrary.simpleMessage("Kỳ hạn gửi"),
        "deptMB": MessageLookupByLibrary.simpleMessage("Dept MB"),
        "email": MessageLookupByLibrary.simpleMessage("Email"),
        "empCode": MessageLookupByLibrary.simpleMessage("Mã NV"),
        "emptyEmail": MessageLookupByLibrary.simpleMessage("emptyEmail"),
        "error": MessageLookupByLibrary.simpleMessage("Error"),
        "errorOccurred": MessageLookupByLibrary.simpleMessage("Đã xảy ra lỗi"),
        "estimatedResults":
            MessageLookupByLibrary.simpleMessage("KẾT QUẢ DỰ TÍNH"),
        "exchangeRate": MessageLookupByLibrary.simpleMessage("Tỉ giá niêm yết"),
        "fakeLogin": MessageLookupByLibrary.simpleMessage("Fake Login"),
        "foreignCurrency":
            MessageLookupByLibrary.simpleMessage("Foreign currency"),
        "formality": MessageLookupByLibrary.simpleMessage("Formality"),
        "friday": MessageLookupByLibrary.simpleMessage("Friday"),
        "generalInformation":
            MessageLookupByLibrary.simpleMessage("General information"),
        "group": MessageLookupByLibrary.simpleMessage("Group"),
        "guideLine": MessageLookupByLibrary.simpleMessage("Guideline"),
        "high": MessageLookupByLibrary.simpleMessage("High"),
        "highPriority": MessageLookupByLibrary.simpleMessage("High priority"),
        "highWarning": MessageLookupByLibrary.simpleMessage("Cảnh báo cao"),
        "hintPassword": MessageLookupByLibrary.simpleMessage("Enter password"),
        "hintUserName": MessageLookupByLibrary.simpleMessage("Enter user name"),
        "home": MessageLookupByLibrary.simpleMessage("Home"),
        "idCard": MessageLookupByLibrary.simpleMessage("ID card"),
        "identification":
            MessageLookupByLibrary.simpleMessage("Identification"),
        "illustrativeExampleUpper":
            MessageLookupByLibrary.simpleMessage("VÍ DỤ MINH HỌA"),
        "inactive": MessageLookupByLibrary.simpleMessage("Inactive"),
        "incomeAndJob": MessageLookupByLibrary.simpleMessage("Income and Job"),
        "incomeVnd": MessageLookupByLibrary.simpleMessage("Income vnd"),
        "indiv": MessageLookupByLibrary.simpleMessage("Khách hàng cá nhân"),
        "interbankRate": MessageLookupByLibrary.simpleMessage(
            "Tỉ giá liên ngân hàng (Interbank)"),
        "interestRate": MessageLookupByLibrary.simpleMessage("Lãi suất"),
        "interestRatePerYear":
            MessageLookupByLibrary.simpleMessage("Lãi suất (%) năm"),
        "invalidDateTime":
            MessageLookupByLibrary.simpleMessage("invalidDateTime"),
        "invalidEmail": MessageLookupByLibrary.simpleMessage("invalidEmail"),
        "invalidPassword":
            MessageLookupByLibrary.simpleMessage("invalidPassword"),
        "invalidPhoneNumber":
            MessageLookupByLibrary.simpleMessage("invalidPhoneNumber"),
        "invalidUserName":
            MessageLookupByLibrary.simpleMessage("invalidUserName"),
        "itbPurchasePrice": MessageLookupByLibrary.simpleMessage("Giá ITB mua"),
        "itpSellingPrice": MessageLookupByLibrary.simpleMessage("Giá ITB bán"),
        "job": MessageLookupByLibrary.simpleMessage("Job"),
        "jobContent": MessageLookupByLibrary.simpleMessage("Job content"),
        "jobLocation": MessageLookupByLibrary.simpleMessage("Job location"),
        "jobTitle": MessageLookupByLibrary.simpleMessage("Job title"),
        "lastUpdateAt":
            MessageLookupByLibrary.simpleMessage("Cập nhật lần cuối lúc:"),
        "login": MessageLookupByLibrary.simpleMessage("Login"),
        "logout": MessageLookupByLibrary.simpleMessage("Logout"),
        "low": MessageLookupByLibrary.simpleMessage("Low"),
        "lowPriority": MessageLookupByLibrary.simpleMessage("Low priority"),
        "maritalStatus": MessageLookupByLibrary.simpleMessage("Marital status"),
        "massDigitalBanking":
            MessageLookupByLibrary.simpleMessage("Mass - Digital banking"),
        "massIndividualCustomers":
            MessageLookupByLibrary.simpleMessage("Mass - Individual customers"),
        "medium": MessageLookupByLibrary.simpleMessage("Medium"),
        "mediumPriority":
            MessageLookupByLibrary.simpleMessage("Medium priority"),
        "message": MessageLookupByLibrary.simpleMessage("Message"),
        "monday": MessageLookupByLibrary.simpleMessage("Monday"),
        "month": MessageLookupByLibrary.simpleMessage("Tháng"),
        "myPage": MessageLookupByLibrary.simpleMessage("My Page"),
        "nationality": MessageLookupByLibrary.simpleMessage("Nationality"),
        "noDate": MessageLookupByLibrary.simpleMessage("No data"),
        "noInternetException":
            MessageLookupByLibrary.simpleMessage("noInternetException"),
        "nonVip": MessageLookupByLibrary.simpleMessage("Non-VIP"),
        "notRatedYet": MessageLookupByLibrary.simpleMessage("Not rate yet"),
        "numberUsedProductsServices": MessageLookupByLibrary.simpleMessage(
            "Number of used products and services"),
        "ok": MessageLookupByLibrary.simpleMessage("OK"),
        "otherInformation":
            MessageLookupByLibrary.simpleMessage("Other information"),
        "othersInformation":
            MessageLookupByLibrary.simpleMessage("Others information"),
        "parseException":
            MessageLookupByLibrary.simpleMessage("parseException"),
        "password": MessageLookupByLibrary.simpleMessage("Password"),
        "passwordsAreNotMatch":
            MessageLookupByLibrary.simpleMessage("passwordsAreNotMatch"),
        "payroll": MessageLookupByLibrary.simpleMessage("Payroll"),
        "phone": MessageLookupByLibrary.simpleMessage("Phone"),
        "priority": MessageLookupByLibrary.simpleMessage("Priority"),
        "private": MessageLookupByLibrary.simpleMessage("Private"),
        "privatePriority":
            MessageLookupByLibrary.simpleMessage("private/Priority"),
        "public": MessageLookupByLibrary.simpleMessage("Public"),
        "record": MessageLookupByLibrary.simpleMessage("record"),
        "retry": MessageLookupByLibrary.simpleMessage("Retry"),
        "retype": MessageLookupByLibrary.simpleMessage("Nhập lại"),
        "rm360": MessageLookupByLibrary.simpleMessage("RM 360"),
        "rmCode": MessageLookupByLibrary.simpleMessage("Mã RM"),
        "saleKit": MessageLookupByLibrary.simpleMessage("SaleKit"),
        "sales": MessageLookupByLibrary.simpleMessage("Sales"),
        "saturday": MessageLookupByLibrary.simpleMessage("Saturday"),
        "savings": MessageLookupByLibrary.simpleMessage("Tiết kiệm"),
        "search": MessageLookupByLibrary.simpleMessage("Search"),
        "sector": MessageLookupByLibrary.simpleMessage("Sector"),
        "segment": MessageLookupByLibrary.simpleMessage("segment"),
        "serviceProduct":
            MessageLookupByLibrary.simpleMessage("Service product"),
        "setting": MessageLookupByLibrary.simpleMessage("Setting"),
        "sex": MessageLookupByLibrary.simpleMessage("Sex"),
        "shortFriday": MessageLookupByLibrary.simpleMessage("Fri"),
        "shortMonday": MessageLookupByLibrary.simpleMessage("Mon"),
        "shortProduct": MessageLookupByLibrary.simpleMessage("SP"),
        "shortSaturday": MessageLookupByLibrary.simpleMessage("Sat"),
        "shortSunday": MessageLookupByLibrary.simpleMessage("Sun"),
        "shortThursday": MessageLookupByLibrary.simpleMessage("Thu"),
        "shortTuesday": MessageLookupByLibrary.simpleMessage("Tue"),
        "shortWednesday": MessageLookupByLibrary.simpleMessage("Wed"),
        "sme": MessageLookupByLibrary.simpleMessage("Khách hàng doanh nghiệp"),
        "sort": MessageLookupByLibrary.simpleMessage("Sort"),
        "status": MessageLookupByLibrary.simpleMessage("Status"),
        "sunday": MessageLookupByLibrary.simpleMessage("Sunday"),
        "support": MessageLookupByLibrary.simpleMessage("Support"),
        "theMassesAreWellOff":
            MessageLookupByLibrary.simpleMessage("The masses are well-off"),
        "thingsToDo": MessageLookupByLibrary.simpleMessage("Things to do"),
        "thisWeek": MessageLookupByLibrary.simpleMessage("Tuần này"),
        "timeRelationshipMB":
            MessageLookupByLibrary.simpleMessage("Time relationship MB"),
        "timeoutException":
            MessageLookupByLibrary.simpleMessage("timeoutException"),
        "toDate": MessageLookupByLibrary.simpleMessage("To date"),
        "toDay": MessageLookupByLibrary.simpleMessage("Hôm nay"),
        "toDoContent": MessageLookupByLibrary.simpleMessage("To do content"),
        "toDoDetails": MessageLookupByLibrary.simpleMessage("To do details"),
        "toDoTitle": MessageLookupByLibrary.simpleMessage("To do title"),
        "tokenExpired": MessageLookupByLibrary.simpleMessage("tokenExpired"),
        "tomorrow": MessageLookupByLibrary.simpleMessage("Ngày mai"),
        "toolsAndUtils":
            MessageLookupByLibrary.simpleMessage("Công cụ & Tiện ích"),
        "totalInterestEndPeriod":
            MessageLookupByLibrary.simpleMessage("Tổng tiền lãi cuối kỳ:"),
        "totalInterestPrincipalEndPeriod": MessageLookupByLibrary.simpleMessage(
            "Tổng tiền lãi và gốc cuối kỳ:"),
        "transactionHistory":
            MessageLookupByLibrary.simpleMessage("Transaction history"),
        "transferBuy": MessageLookupByLibrary.simpleMessage("Transfer buy"),
        "transferSale": MessageLookupByLibrary.simpleMessage("Transfer sale"),
        "tuesday": MessageLookupByLibrary.simpleMessage("Tuesday"),
        "unknownException": m0,
        "userInfo":
            MessageLookupByLibrary.simpleMessage("Thông tin người dùng"),
        "userName": MessageLookupByLibrary.simpleMessage("User name"),
        "version":
            MessageLookupByLibrary.simpleMessage("Phiên bản CRM Nextgen 1.0"),
        "vietnamese": MessageLookupByLibrary.simpleMessage("Vietnamese"),
        "warning": MessageLookupByLibrary.simpleMessage("Warning"),
        "wednesday": MessageLookupByLibrary.simpleMessage("Wednesday"),
        "week": MessageLookupByLibrary.simpleMessage("Week"),
        "welcomeLoginPage": MessageLookupByLibrary.simpleMessage("Welcome"),
        "workSchedule": MessageLookupByLibrary.simpleMessage("Work schedule"),
        "year": MessageLookupByLibrary.simpleMessage("Năm")
      };
}
