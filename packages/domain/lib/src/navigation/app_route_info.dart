import 'package:freezed_annotation/freezed_annotation.dart';

import 'package:domain/domain.dart';

part 'app_route_info.freezed.dart';

/// page
@freezed
class AppRouteInfo with _$AppRouteInfo {
  const factory AppRouteInfo.login() = _Login;
  const factory AppRouteInfo.main() = _Main;
  const factory AppRouteInfo.customerDetail(Customer customer) = _CustomerDetail;

  const factory AppRouteInfo.itemDetail(User user) = _UserDetail;
}
