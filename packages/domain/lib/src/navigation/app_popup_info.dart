import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shared/shared.dart';

part 'app_popup_info.freezed.dart';

/// dialog, bottomSheet
@freezed
class AppPopupInfo with _$AppPopupInfo {
  const factory AppPopupInfo.confirmDialog({
    @Default('') String message,
    Func0<void>? onPressed,
  }) = _ConfirmDialog;

  const factory AppPopupInfo.errorWithRetryDialog({
    @Default('') String message,
    Func0<void>? onRetryPressed,
  }) = _ErrorWithRetryDialog;

  const factory AppPopupInfo.requiredLoginDialog() = _RequiredLoginDialog;

  const factory AppPopupInfo.birthDayDialog() = _BirthDayDialog;

  const factory AppPopupInfo.notificationDialog() = _NotificationDialog;

  const factory AppPopupInfo.toolsDialog() = _ToolsDialog;

  const factory AppPopupInfo.calculateSavingDialog() = _CalculateSavingDialog;

  const factory AppPopupInfo.exchangeRateDialog() = _ExchangeRateDialog;

  const factory AppPopupInfo.interbankDialog() = _InterbankDialog;

  const factory AppPopupInfo.createJobDialog() = _CreateJobDialog;

  const factory AppPopupInfo.pickerDateRangeDialog() = _PickerDateRangeDialog;

  const factory AppPopupInfo.createTodoDialog() = _CreateTodoDialog;
}
