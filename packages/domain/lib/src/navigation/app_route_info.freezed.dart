// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'app_route_info.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$AppRouteInfo {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() login,
    required TResult Function() main,
    required TResult Function(Customer customer) customerDetail,
    required TResult Function(User user) itemDetail,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? login,
    TResult? Function()? main,
    TResult? Function(Customer customer)? customerDetail,
    TResult? Function(User user)? itemDetail,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? login,
    TResult Function()? main,
    TResult Function(Customer customer)? customerDetail,
    TResult Function(User user)? itemDetail,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Login value) login,
    required TResult Function(_Main value) main,
    required TResult Function(_CustomerDetail value) customerDetail,
    required TResult Function(_UserDetail value) itemDetail,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Login value)? login,
    TResult? Function(_Main value)? main,
    TResult? Function(_CustomerDetail value)? customerDetail,
    TResult? Function(_UserDetail value)? itemDetail,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Login value)? login,
    TResult Function(_Main value)? main,
    TResult Function(_CustomerDetail value)? customerDetail,
    TResult Function(_UserDetail value)? itemDetail,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AppRouteInfoCopyWith<$Res> {
  factory $AppRouteInfoCopyWith(
          AppRouteInfo value, $Res Function(AppRouteInfo) then) =
      _$AppRouteInfoCopyWithImpl<$Res, AppRouteInfo>;
}

/// @nodoc
class _$AppRouteInfoCopyWithImpl<$Res, $Val extends AppRouteInfo>
    implements $AppRouteInfoCopyWith<$Res> {
  _$AppRouteInfoCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$LoginImplCopyWith<$Res> {
  factory _$$LoginImplCopyWith(
          _$LoginImpl value, $Res Function(_$LoginImpl) then) =
      __$$LoginImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$LoginImplCopyWithImpl<$Res>
    extends _$AppRouteInfoCopyWithImpl<$Res, _$LoginImpl>
    implements _$$LoginImplCopyWith<$Res> {
  __$$LoginImplCopyWithImpl(
      _$LoginImpl _value, $Res Function(_$LoginImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$LoginImpl implements _Login {
  const _$LoginImpl();

  @override
  String toString() {
    return 'AppRouteInfo.login()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$LoginImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() login,
    required TResult Function() main,
    required TResult Function(Customer customer) customerDetail,
    required TResult Function(User user) itemDetail,
  }) {
    return login();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? login,
    TResult? Function()? main,
    TResult? Function(Customer customer)? customerDetail,
    TResult? Function(User user)? itemDetail,
  }) {
    return login?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? login,
    TResult Function()? main,
    TResult Function(Customer customer)? customerDetail,
    TResult Function(User user)? itemDetail,
    required TResult orElse(),
  }) {
    if (login != null) {
      return login();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Login value) login,
    required TResult Function(_Main value) main,
    required TResult Function(_CustomerDetail value) customerDetail,
    required TResult Function(_UserDetail value) itemDetail,
  }) {
    return login(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Login value)? login,
    TResult? Function(_Main value)? main,
    TResult? Function(_CustomerDetail value)? customerDetail,
    TResult? Function(_UserDetail value)? itemDetail,
  }) {
    return login?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Login value)? login,
    TResult Function(_Main value)? main,
    TResult Function(_CustomerDetail value)? customerDetail,
    TResult Function(_UserDetail value)? itemDetail,
    required TResult orElse(),
  }) {
    if (login != null) {
      return login(this);
    }
    return orElse();
  }
}

abstract class _Login implements AppRouteInfo {
  const factory _Login() = _$LoginImpl;
}

/// @nodoc
abstract class _$$MainImplCopyWith<$Res> {
  factory _$$MainImplCopyWith(
          _$MainImpl value, $Res Function(_$MainImpl) then) =
      __$$MainImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$MainImplCopyWithImpl<$Res>
    extends _$AppRouteInfoCopyWithImpl<$Res, _$MainImpl>
    implements _$$MainImplCopyWith<$Res> {
  __$$MainImplCopyWithImpl(_$MainImpl _value, $Res Function(_$MainImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$MainImpl implements _Main {
  const _$MainImpl();

  @override
  String toString() {
    return 'AppRouteInfo.main()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$MainImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() login,
    required TResult Function() main,
    required TResult Function(Customer customer) customerDetail,
    required TResult Function(User user) itemDetail,
  }) {
    return main();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? login,
    TResult? Function()? main,
    TResult? Function(Customer customer)? customerDetail,
    TResult? Function(User user)? itemDetail,
  }) {
    return main?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? login,
    TResult Function()? main,
    TResult Function(Customer customer)? customerDetail,
    TResult Function(User user)? itemDetail,
    required TResult orElse(),
  }) {
    if (main != null) {
      return main();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Login value) login,
    required TResult Function(_Main value) main,
    required TResult Function(_CustomerDetail value) customerDetail,
    required TResult Function(_UserDetail value) itemDetail,
  }) {
    return main(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Login value)? login,
    TResult? Function(_Main value)? main,
    TResult? Function(_CustomerDetail value)? customerDetail,
    TResult? Function(_UserDetail value)? itemDetail,
  }) {
    return main?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Login value)? login,
    TResult Function(_Main value)? main,
    TResult Function(_CustomerDetail value)? customerDetail,
    TResult Function(_UserDetail value)? itemDetail,
    required TResult orElse(),
  }) {
    if (main != null) {
      return main(this);
    }
    return orElse();
  }
}

abstract class _Main implements AppRouteInfo {
  const factory _Main() = _$MainImpl;
}

/// @nodoc
abstract class _$$CustomerDetailImplCopyWith<$Res> {
  factory _$$CustomerDetailImplCopyWith(_$CustomerDetailImpl value,
          $Res Function(_$CustomerDetailImpl) then) =
      __$$CustomerDetailImplCopyWithImpl<$Res>;
  @useResult
  $Res call({Customer customer});

  $CustomerCopyWith<$Res> get customer;
}

/// @nodoc
class __$$CustomerDetailImplCopyWithImpl<$Res>
    extends _$AppRouteInfoCopyWithImpl<$Res, _$CustomerDetailImpl>
    implements _$$CustomerDetailImplCopyWith<$Res> {
  __$$CustomerDetailImplCopyWithImpl(
      _$CustomerDetailImpl _value, $Res Function(_$CustomerDetailImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? customer = null,
  }) {
    return _then(_$CustomerDetailImpl(
      null == customer
          ? _value.customer
          : customer // ignore: cast_nullable_to_non_nullable
              as Customer,
    ));
  }

  @override
  @pragma('vm:prefer-inline')
  $CustomerCopyWith<$Res> get customer {
    return $CustomerCopyWith<$Res>(_value.customer, (value) {
      return _then(_value.copyWith(customer: value));
    });
  }
}

/// @nodoc

class _$CustomerDetailImpl implements _CustomerDetail {
  const _$CustomerDetailImpl(this.customer);

  @override
  final Customer customer;

  @override
  String toString() {
    return 'AppRouteInfo.customerDetail(customer: $customer)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CustomerDetailImpl &&
            (identical(other.customer, customer) ||
                other.customer == customer));
  }

  @override
  int get hashCode => Object.hash(runtimeType, customer);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CustomerDetailImplCopyWith<_$CustomerDetailImpl> get copyWith =>
      __$$CustomerDetailImplCopyWithImpl<_$CustomerDetailImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() login,
    required TResult Function() main,
    required TResult Function(Customer customer) customerDetail,
    required TResult Function(User user) itemDetail,
  }) {
    return customerDetail(customer);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? login,
    TResult? Function()? main,
    TResult? Function(Customer customer)? customerDetail,
    TResult? Function(User user)? itemDetail,
  }) {
    return customerDetail?.call(customer);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? login,
    TResult Function()? main,
    TResult Function(Customer customer)? customerDetail,
    TResult Function(User user)? itemDetail,
    required TResult orElse(),
  }) {
    if (customerDetail != null) {
      return customerDetail(customer);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Login value) login,
    required TResult Function(_Main value) main,
    required TResult Function(_CustomerDetail value) customerDetail,
    required TResult Function(_UserDetail value) itemDetail,
  }) {
    return customerDetail(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Login value)? login,
    TResult? Function(_Main value)? main,
    TResult? Function(_CustomerDetail value)? customerDetail,
    TResult? Function(_UserDetail value)? itemDetail,
  }) {
    return customerDetail?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Login value)? login,
    TResult Function(_Main value)? main,
    TResult Function(_CustomerDetail value)? customerDetail,
    TResult Function(_UserDetail value)? itemDetail,
    required TResult orElse(),
  }) {
    if (customerDetail != null) {
      return customerDetail(this);
    }
    return orElse();
  }
}

abstract class _CustomerDetail implements AppRouteInfo {
  const factory _CustomerDetail(final Customer customer) = _$CustomerDetailImpl;

  Customer get customer;
  @JsonKey(ignore: true)
  _$$CustomerDetailImplCopyWith<_$CustomerDetailImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$UserDetailImplCopyWith<$Res> {
  factory _$$UserDetailImplCopyWith(
          _$UserDetailImpl value, $Res Function(_$UserDetailImpl) then) =
      __$$UserDetailImplCopyWithImpl<$Res>;
  @useResult
  $Res call({User user});

  $UserCopyWith<$Res> get user;
}

/// @nodoc
class __$$UserDetailImplCopyWithImpl<$Res>
    extends _$AppRouteInfoCopyWithImpl<$Res, _$UserDetailImpl>
    implements _$$UserDetailImplCopyWith<$Res> {
  __$$UserDetailImplCopyWithImpl(
      _$UserDetailImpl _value, $Res Function(_$UserDetailImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? user = null,
  }) {
    return _then(_$UserDetailImpl(
      null == user
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as User,
    ));
  }

  @override
  @pragma('vm:prefer-inline')
  $UserCopyWith<$Res> get user {
    return $UserCopyWith<$Res>(_value.user, (value) {
      return _then(_value.copyWith(user: value));
    });
  }
}

/// @nodoc

class _$UserDetailImpl implements _UserDetail {
  const _$UserDetailImpl(this.user);

  @override
  final User user;

  @override
  String toString() {
    return 'AppRouteInfo.itemDetail(user: $user)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UserDetailImpl &&
            (identical(other.user, user) || other.user == user));
  }

  @override
  int get hashCode => Object.hash(runtimeType, user);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$UserDetailImplCopyWith<_$UserDetailImpl> get copyWith =>
      __$$UserDetailImplCopyWithImpl<_$UserDetailImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() login,
    required TResult Function() main,
    required TResult Function(Customer customer) customerDetail,
    required TResult Function(User user) itemDetail,
  }) {
    return itemDetail(user);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? login,
    TResult? Function()? main,
    TResult? Function(Customer customer)? customerDetail,
    TResult? Function(User user)? itemDetail,
  }) {
    return itemDetail?.call(user);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? login,
    TResult Function()? main,
    TResult Function(Customer customer)? customerDetail,
    TResult Function(User user)? itemDetail,
    required TResult orElse(),
  }) {
    if (itemDetail != null) {
      return itemDetail(user);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Login value) login,
    required TResult Function(_Main value) main,
    required TResult Function(_CustomerDetail value) customerDetail,
    required TResult Function(_UserDetail value) itemDetail,
  }) {
    return itemDetail(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Login value)? login,
    TResult? Function(_Main value)? main,
    TResult? Function(_CustomerDetail value)? customerDetail,
    TResult? Function(_UserDetail value)? itemDetail,
  }) {
    return itemDetail?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Login value)? login,
    TResult Function(_Main value)? main,
    TResult Function(_CustomerDetail value)? customerDetail,
    TResult Function(_UserDetail value)? itemDetail,
    required TResult orElse(),
  }) {
    if (itemDetail != null) {
      return itemDetail(this);
    }
    return orElse();
  }
}

abstract class _UserDetail implements AppRouteInfo {
  const factory _UserDetail(final User user) = _$UserDetailImpl;

  User get user;
  @JsonKey(ignore: true)
  _$$UserDetailImplCopyWith<_$UserDetailImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
