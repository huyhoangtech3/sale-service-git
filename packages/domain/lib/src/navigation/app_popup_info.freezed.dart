// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'app_popup_info.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$AppPopupInfo {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String message, Func0<void>? onPressed)
        confirmDialog,
    required TResult Function(String message, Func0<void>? onRetryPressed)
        errorWithRetryDialog,
    required TResult Function() requiredLoginDialog,
    required TResult Function() birthDayDialog,
    required TResult Function() notificationDialog,
    required TResult Function() toolsDialog,
    required TResult Function() calculateSavingDialog,
    required TResult Function() exchangeRateDialog,
    required TResult Function() interbankDialog,
    required TResult Function() createJobDialog,
    required TResult Function() pickerDateRangeDialog,
    required TResult Function() createTodoDialog,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String message, Func0<void>? onPressed)? confirmDialog,
    TResult? Function(String message, Func0<void>? onRetryPressed)?
        errorWithRetryDialog,
    TResult? Function()? requiredLoginDialog,
    TResult? Function()? birthDayDialog,
    TResult? Function()? notificationDialog,
    TResult? Function()? toolsDialog,
    TResult? Function()? calculateSavingDialog,
    TResult? Function()? exchangeRateDialog,
    TResult? Function()? interbankDialog,
    TResult? Function()? createJobDialog,
    TResult? Function()? pickerDateRangeDialog,
    TResult? Function()? createTodoDialog,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String message, Func0<void>? onPressed)? confirmDialog,
    TResult Function(String message, Func0<void>? onRetryPressed)?
        errorWithRetryDialog,
    TResult Function()? requiredLoginDialog,
    TResult Function()? birthDayDialog,
    TResult Function()? notificationDialog,
    TResult Function()? toolsDialog,
    TResult Function()? calculateSavingDialog,
    TResult Function()? exchangeRateDialog,
    TResult Function()? interbankDialog,
    TResult Function()? createJobDialog,
    TResult Function()? pickerDateRangeDialog,
    TResult Function()? createTodoDialog,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ConfirmDialog value) confirmDialog,
    required TResult Function(_ErrorWithRetryDialog value) errorWithRetryDialog,
    required TResult Function(_RequiredLoginDialog value) requiredLoginDialog,
    required TResult Function(_BirthDayDialog value) birthDayDialog,
    required TResult Function(_NotificationDialog value) notificationDialog,
    required TResult Function(_ToolsDialog value) toolsDialog,
    required TResult Function(_CalculateSavingDialog value)
        calculateSavingDialog,
    required TResult Function(_ExchangeRateDialog value) exchangeRateDialog,
    required TResult Function(_InterbankDialog value) interbankDialog,
    required TResult Function(_CreateJobDialog value) createJobDialog,
    required TResult Function(_PickerDateRangeDialog value)
        pickerDateRangeDialog,
    required TResult Function(_CreateTodoDialog value) createTodoDialog,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_ConfirmDialog value)? confirmDialog,
    TResult? Function(_ErrorWithRetryDialog value)? errorWithRetryDialog,
    TResult? Function(_RequiredLoginDialog value)? requiredLoginDialog,
    TResult? Function(_BirthDayDialog value)? birthDayDialog,
    TResult? Function(_NotificationDialog value)? notificationDialog,
    TResult? Function(_ToolsDialog value)? toolsDialog,
    TResult? Function(_CalculateSavingDialog value)? calculateSavingDialog,
    TResult? Function(_ExchangeRateDialog value)? exchangeRateDialog,
    TResult? Function(_InterbankDialog value)? interbankDialog,
    TResult? Function(_CreateJobDialog value)? createJobDialog,
    TResult? Function(_PickerDateRangeDialog value)? pickerDateRangeDialog,
    TResult? Function(_CreateTodoDialog value)? createTodoDialog,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ConfirmDialog value)? confirmDialog,
    TResult Function(_ErrorWithRetryDialog value)? errorWithRetryDialog,
    TResult Function(_RequiredLoginDialog value)? requiredLoginDialog,
    TResult Function(_BirthDayDialog value)? birthDayDialog,
    TResult Function(_NotificationDialog value)? notificationDialog,
    TResult Function(_ToolsDialog value)? toolsDialog,
    TResult Function(_CalculateSavingDialog value)? calculateSavingDialog,
    TResult Function(_ExchangeRateDialog value)? exchangeRateDialog,
    TResult Function(_InterbankDialog value)? interbankDialog,
    TResult Function(_CreateJobDialog value)? createJobDialog,
    TResult Function(_PickerDateRangeDialog value)? pickerDateRangeDialog,
    TResult Function(_CreateTodoDialog value)? createTodoDialog,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AppPopupInfoCopyWith<$Res> {
  factory $AppPopupInfoCopyWith(
          AppPopupInfo value, $Res Function(AppPopupInfo) then) =
      _$AppPopupInfoCopyWithImpl<$Res, AppPopupInfo>;
}

/// @nodoc
class _$AppPopupInfoCopyWithImpl<$Res, $Val extends AppPopupInfo>
    implements $AppPopupInfoCopyWith<$Res> {
  _$AppPopupInfoCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$ConfirmDialogImplCopyWith<$Res> {
  factory _$$ConfirmDialogImplCopyWith(
          _$ConfirmDialogImpl value, $Res Function(_$ConfirmDialogImpl) then) =
      __$$ConfirmDialogImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String message, Func0<void>? onPressed});
}

/// @nodoc
class __$$ConfirmDialogImplCopyWithImpl<$Res>
    extends _$AppPopupInfoCopyWithImpl<$Res, _$ConfirmDialogImpl>
    implements _$$ConfirmDialogImplCopyWith<$Res> {
  __$$ConfirmDialogImplCopyWithImpl(
      _$ConfirmDialogImpl _value, $Res Function(_$ConfirmDialogImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? message = null,
    Object? onPressed = freezed,
  }) {
    return _then(_$ConfirmDialogImpl(
      message: null == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
      onPressed: freezed == onPressed
          ? _value.onPressed
          : onPressed // ignore: cast_nullable_to_non_nullable
              as Func0<void>?,
    ));
  }
}

/// @nodoc

class _$ConfirmDialogImpl implements _ConfirmDialog {
  const _$ConfirmDialogImpl({this.message = '', this.onPressed});

  @override
  @JsonKey()
  final String message;
  @override
  final Func0<void>? onPressed;

  @override
  String toString() {
    return 'AppPopupInfo.confirmDialog(message: $message, onPressed: $onPressed)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ConfirmDialogImpl &&
            (identical(other.message, message) || other.message == message) &&
            (identical(other.onPressed, onPressed) ||
                other.onPressed == onPressed));
  }

  @override
  int get hashCode => Object.hash(runtimeType, message, onPressed);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ConfirmDialogImplCopyWith<_$ConfirmDialogImpl> get copyWith =>
      __$$ConfirmDialogImplCopyWithImpl<_$ConfirmDialogImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String message, Func0<void>? onPressed)
        confirmDialog,
    required TResult Function(String message, Func0<void>? onRetryPressed)
        errorWithRetryDialog,
    required TResult Function() requiredLoginDialog,
    required TResult Function() birthDayDialog,
    required TResult Function() notificationDialog,
    required TResult Function() toolsDialog,
    required TResult Function() calculateSavingDialog,
    required TResult Function() exchangeRateDialog,
    required TResult Function() interbankDialog,
    required TResult Function() createJobDialog,
    required TResult Function() pickerDateRangeDialog,
    required TResult Function() createTodoDialog,
  }) {
    return confirmDialog(message, onPressed);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String message, Func0<void>? onPressed)? confirmDialog,
    TResult? Function(String message, Func0<void>? onRetryPressed)?
        errorWithRetryDialog,
    TResult? Function()? requiredLoginDialog,
    TResult? Function()? birthDayDialog,
    TResult? Function()? notificationDialog,
    TResult? Function()? toolsDialog,
    TResult? Function()? calculateSavingDialog,
    TResult? Function()? exchangeRateDialog,
    TResult? Function()? interbankDialog,
    TResult? Function()? createJobDialog,
    TResult? Function()? pickerDateRangeDialog,
    TResult? Function()? createTodoDialog,
  }) {
    return confirmDialog?.call(message, onPressed);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String message, Func0<void>? onPressed)? confirmDialog,
    TResult Function(String message, Func0<void>? onRetryPressed)?
        errorWithRetryDialog,
    TResult Function()? requiredLoginDialog,
    TResult Function()? birthDayDialog,
    TResult Function()? notificationDialog,
    TResult Function()? toolsDialog,
    TResult Function()? calculateSavingDialog,
    TResult Function()? exchangeRateDialog,
    TResult Function()? interbankDialog,
    TResult Function()? createJobDialog,
    TResult Function()? pickerDateRangeDialog,
    TResult Function()? createTodoDialog,
    required TResult orElse(),
  }) {
    if (confirmDialog != null) {
      return confirmDialog(message, onPressed);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ConfirmDialog value) confirmDialog,
    required TResult Function(_ErrorWithRetryDialog value) errorWithRetryDialog,
    required TResult Function(_RequiredLoginDialog value) requiredLoginDialog,
    required TResult Function(_BirthDayDialog value) birthDayDialog,
    required TResult Function(_NotificationDialog value) notificationDialog,
    required TResult Function(_ToolsDialog value) toolsDialog,
    required TResult Function(_CalculateSavingDialog value)
        calculateSavingDialog,
    required TResult Function(_ExchangeRateDialog value) exchangeRateDialog,
    required TResult Function(_InterbankDialog value) interbankDialog,
    required TResult Function(_CreateJobDialog value) createJobDialog,
    required TResult Function(_PickerDateRangeDialog value)
        pickerDateRangeDialog,
    required TResult Function(_CreateTodoDialog value) createTodoDialog,
  }) {
    return confirmDialog(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_ConfirmDialog value)? confirmDialog,
    TResult? Function(_ErrorWithRetryDialog value)? errorWithRetryDialog,
    TResult? Function(_RequiredLoginDialog value)? requiredLoginDialog,
    TResult? Function(_BirthDayDialog value)? birthDayDialog,
    TResult? Function(_NotificationDialog value)? notificationDialog,
    TResult? Function(_ToolsDialog value)? toolsDialog,
    TResult? Function(_CalculateSavingDialog value)? calculateSavingDialog,
    TResult? Function(_ExchangeRateDialog value)? exchangeRateDialog,
    TResult? Function(_InterbankDialog value)? interbankDialog,
    TResult? Function(_CreateJobDialog value)? createJobDialog,
    TResult? Function(_PickerDateRangeDialog value)? pickerDateRangeDialog,
    TResult? Function(_CreateTodoDialog value)? createTodoDialog,
  }) {
    return confirmDialog?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ConfirmDialog value)? confirmDialog,
    TResult Function(_ErrorWithRetryDialog value)? errorWithRetryDialog,
    TResult Function(_RequiredLoginDialog value)? requiredLoginDialog,
    TResult Function(_BirthDayDialog value)? birthDayDialog,
    TResult Function(_NotificationDialog value)? notificationDialog,
    TResult Function(_ToolsDialog value)? toolsDialog,
    TResult Function(_CalculateSavingDialog value)? calculateSavingDialog,
    TResult Function(_ExchangeRateDialog value)? exchangeRateDialog,
    TResult Function(_InterbankDialog value)? interbankDialog,
    TResult Function(_CreateJobDialog value)? createJobDialog,
    TResult Function(_PickerDateRangeDialog value)? pickerDateRangeDialog,
    TResult Function(_CreateTodoDialog value)? createTodoDialog,
    required TResult orElse(),
  }) {
    if (confirmDialog != null) {
      return confirmDialog(this);
    }
    return orElse();
  }
}

abstract class _ConfirmDialog implements AppPopupInfo {
  const factory _ConfirmDialog(
      {final String message,
      final Func0<void>? onPressed}) = _$ConfirmDialogImpl;

  String get message;
  Func0<void>? get onPressed;
  @JsonKey(ignore: true)
  _$$ConfirmDialogImplCopyWith<_$ConfirmDialogImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ErrorWithRetryDialogImplCopyWith<$Res> {
  factory _$$ErrorWithRetryDialogImplCopyWith(_$ErrorWithRetryDialogImpl value,
          $Res Function(_$ErrorWithRetryDialogImpl) then) =
      __$$ErrorWithRetryDialogImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String message, Func0<void>? onRetryPressed});
}

/// @nodoc
class __$$ErrorWithRetryDialogImplCopyWithImpl<$Res>
    extends _$AppPopupInfoCopyWithImpl<$Res, _$ErrorWithRetryDialogImpl>
    implements _$$ErrorWithRetryDialogImplCopyWith<$Res> {
  __$$ErrorWithRetryDialogImplCopyWithImpl(_$ErrorWithRetryDialogImpl _value,
      $Res Function(_$ErrorWithRetryDialogImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? message = null,
    Object? onRetryPressed = freezed,
  }) {
    return _then(_$ErrorWithRetryDialogImpl(
      message: null == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
      onRetryPressed: freezed == onRetryPressed
          ? _value.onRetryPressed
          : onRetryPressed // ignore: cast_nullable_to_non_nullable
              as Func0<void>?,
    ));
  }
}

/// @nodoc

class _$ErrorWithRetryDialogImpl implements _ErrorWithRetryDialog {
  const _$ErrorWithRetryDialogImpl({this.message = '', this.onRetryPressed});

  @override
  @JsonKey()
  final String message;
  @override
  final Func0<void>? onRetryPressed;

  @override
  String toString() {
    return 'AppPopupInfo.errorWithRetryDialog(message: $message, onRetryPressed: $onRetryPressed)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ErrorWithRetryDialogImpl &&
            (identical(other.message, message) || other.message == message) &&
            (identical(other.onRetryPressed, onRetryPressed) ||
                other.onRetryPressed == onRetryPressed));
  }

  @override
  int get hashCode => Object.hash(runtimeType, message, onRetryPressed);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ErrorWithRetryDialogImplCopyWith<_$ErrorWithRetryDialogImpl>
      get copyWith =>
          __$$ErrorWithRetryDialogImplCopyWithImpl<_$ErrorWithRetryDialogImpl>(
              this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String message, Func0<void>? onPressed)
        confirmDialog,
    required TResult Function(String message, Func0<void>? onRetryPressed)
        errorWithRetryDialog,
    required TResult Function() requiredLoginDialog,
    required TResult Function() birthDayDialog,
    required TResult Function() notificationDialog,
    required TResult Function() toolsDialog,
    required TResult Function() calculateSavingDialog,
    required TResult Function() exchangeRateDialog,
    required TResult Function() interbankDialog,
    required TResult Function() createJobDialog,
    required TResult Function() pickerDateRangeDialog,
    required TResult Function() createTodoDialog,
  }) {
    return errorWithRetryDialog(message, onRetryPressed);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String message, Func0<void>? onPressed)? confirmDialog,
    TResult? Function(String message, Func0<void>? onRetryPressed)?
        errorWithRetryDialog,
    TResult? Function()? requiredLoginDialog,
    TResult? Function()? birthDayDialog,
    TResult? Function()? notificationDialog,
    TResult? Function()? toolsDialog,
    TResult? Function()? calculateSavingDialog,
    TResult? Function()? exchangeRateDialog,
    TResult? Function()? interbankDialog,
    TResult? Function()? createJobDialog,
    TResult? Function()? pickerDateRangeDialog,
    TResult? Function()? createTodoDialog,
  }) {
    return errorWithRetryDialog?.call(message, onRetryPressed);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String message, Func0<void>? onPressed)? confirmDialog,
    TResult Function(String message, Func0<void>? onRetryPressed)?
        errorWithRetryDialog,
    TResult Function()? requiredLoginDialog,
    TResult Function()? birthDayDialog,
    TResult Function()? notificationDialog,
    TResult Function()? toolsDialog,
    TResult Function()? calculateSavingDialog,
    TResult Function()? exchangeRateDialog,
    TResult Function()? interbankDialog,
    TResult Function()? createJobDialog,
    TResult Function()? pickerDateRangeDialog,
    TResult Function()? createTodoDialog,
    required TResult orElse(),
  }) {
    if (errorWithRetryDialog != null) {
      return errorWithRetryDialog(message, onRetryPressed);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ConfirmDialog value) confirmDialog,
    required TResult Function(_ErrorWithRetryDialog value) errorWithRetryDialog,
    required TResult Function(_RequiredLoginDialog value) requiredLoginDialog,
    required TResult Function(_BirthDayDialog value) birthDayDialog,
    required TResult Function(_NotificationDialog value) notificationDialog,
    required TResult Function(_ToolsDialog value) toolsDialog,
    required TResult Function(_CalculateSavingDialog value)
        calculateSavingDialog,
    required TResult Function(_ExchangeRateDialog value) exchangeRateDialog,
    required TResult Function(_InterbankDialog value) interbankDialog,
    required TResult Function(_CreateJobDialog value) createJobDialog,
    required TResult Function(_PickerDateRangeDialog value)
        pickerDateRangeDialog,
    required TResult Function(_CreateTodoDialog value) createTodoDialog,
  }) {
    return errorWithRetryDialog(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_ConfirmDialog value)? confirmDialog,
    TResult? Function(_ErrorWithRetryDialog value)? errorWithRetryDialog,
    TResult? Function(_RequiredLoginDialog value)? requiredLoginDialog,
    TResult? Function(_BirthDayDialog value)? birthDayDialog,
    TResult? Function(_NotificationDialog value)? notificationDialog,
    TResult? Function(_ToolsDialog value)? toolsDialog,
    TResult? Function(_CalculateSavingDialog value)? calculateSavingDialog,
    TResult? Function(_ExchangeRateDialog value)? exchangeRateDialog,
    TResult? Function(_InterbankDialog value)? interbankDialog,
    TResult? Function(_CreateJobDialog value)? createJobDialog,
    TResult? Function(_PickerDateRangeDialog value)? pickerDateRangeDialog,
    TResult? Function(_CreateTodoDialog value)? createTodoDialog,
  }) {
    return errorWithRetryDialog?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ConfirmDialog value)? confirmDialog,
    TResult Function(_ErrorWithRetryDialog value)? errorWithRetryDialog,
    TResult Function(_RequiredLoginDialog value)? requiredLoginDialog,
    TResult Function(_BirthDayDialog value)? birthDayDialog,
    TResult Function(_NotificationDialog value)? notificationDialog,
    TResult Function(_ToolsDialog value)? toolsDialog,
    TResult Function(_CalculateSavingDialog value)? calculateSavingDialog,
    TResult Function(_ExchangeRateDialog value)? exchangeRateDialog,
    TResult Function(_InterbankDialog value)? interbankDialog,
    TResult Function(_CreateJobDialog value)? createJobDialog,
    TResult Function(_PickerDateRangeDialog value)? pickerDateRangeDialog,
    TResult Function(_CreateTodoDialog value)? createTodoDialog,
    required TResult orElse(),
  }) {
    if (errorWithRetryDialog != null) {
      return errorWithRetryDialog(this);
    }
    return orElse();
  }
}

abstract class _ErrorWithRetryDialog implements AppPopupInfo {
  const factory _ErrorWithRetryDialog(
      {final String message,
      final Func0<void>? onRetryPressed}) = _$ErrorWithRetryDialogImpl;

  String get message;
  Func0<void>? get onRetryPressed;
  @JsonKey(ignore: true)
  _$$ErrorWithRetryDialogImplCopyWith<_$ErrorWithRetryDialogImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$RequiredLoginDialogImplCopyWith<$Res> {
  factory _$$RequiredLoginDialogImplCopyWith(_$RequiredLoginDialogImpl value,
          $Res Function(_$RequiredLoginDialogImpl) then) =
      __$$RequiredLoginDialogImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$RequiredLoginDialogImplCopyWithImpl<$Res>
    extends _$AppPopupInfoCopyWithImpl<$Res, _$RequiredLoginDialogImpl>
    implements _$$RequiredLoginDialogImplCopyWith<$Res> {
  __$$RequiredLoginDialogImplCopyWithImpl(_$RequiredLoginDialogImpl _value,
      $Res Function(_$RequiredLoginDialogImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$RequiredLoginDialogImpl implements _RequiredLoginDialog {
  const _$RequiredLoginDialogImpl();

  @override
  String toString() {
    return 'AppPopupInfo.requiredLoginDialog()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RequiredLoginDialogImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String message, Func0<void>? onPressed)
        confirmDialog,
    required TResult Function(String message, Func0<void>? onRetryPressed)
        errorWithRetryDialog,
    required TResult Function() requiredLoginDialog,
    required TResult Function() birthDayDialog,
    required TResult Function() notificationDialog,
    required TResult Function() toolsDialog,
    required TResult Function() calculateSavingDialog,
    required TResult Function() exchangeRateDialog,
    required TResult Function() interbankDialog,
    required TResult Function() createJobDialog,
    required TResult Function() pickerDateRangeDialog,
    required TResult Function() createTodoDialog,
  }) {
    return requiredLoginDialog();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String message, Func0<void>? onPressed)? confirmDialog,
    TResult? Function(String message, Func0<void>? onRetryPressed)?
        errorWithRetryDialog,
    TResult? Function()? requiredLoginDialog,
    TResult? Function()? birthDayDialog,
    TResult? Function()? notificationDialog,
    TResult? Function()? toolsDialog,
    TResult? Function()? calculateSavingDialog,
    TResult? Function()? exchangeRateDialog,
    TResult? Function()? interbankDialog,
    TResult? Function()? createJobDialog,
    TResult? Function()? pickerDateRangeDialog,
    TResult? Function()? createTodoDialog,
  }) {
    return requiredLoginDialog?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String message, Func0<void>? onPressed)? confirmDialog,
    TResult Function(String message, Func0<void>? onRetryPressed)?
        errorWithRetryDialog,
    TResult Function()? requiredLoginDialog,
    TResult Function()? birthDayDialog,
    TResult Function()? notificationDialog,
    TResult Function()? toolsDialog,
    TResult Function()? calculateSavingDialog,
    TResult Function()? exchangeRateDialog,
    TResult Function()? interbankDialog,
    TResult Function()? createJobDialog,
    TResult Function()? pickerDateRangeDialog,
    TResult Function()? createTodoDialog,
    required TResult orElse(),
  }) {
    if (requiredLoginDialog != null) {
      return requiredLoginDialog();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ConfirmDialog value) confirmDialog,
    required TResult Function(_ErrorWithRetryDialog value) errorWithRetryDialog,
    required TResult Function(_RequiredLoginDialog value) requiredLoginDialog,
    required TResult Function(_BirthDayDialog value) birthDayDialog,
    required TResult Function(_NotificationDialog value) notificationDialog,
    required TResult Function(_ToolsDialog value) toolsDialog,
    required TResult Function(_CalculateSavingDialog value)
        calculateSavingDialog,
    required TResult Function(_ExchangeRateDialog value) exchangeRateDialog,
    required TResult Function(_InterbankDialog value) interbankDialog,
    required TResult Function(_CreateJobDialog value) createJobDialog,
    required TResult Function(_PickerDateRangeDialog value)
        pickerDateRangeDialog,
    required TResult Function(_CreateTodoDialog value) createTodoDialog,
  }) {
    return requiredLoginDialog(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_ConfirmDialog value)? confirmDialog,
    TResult? Function(_ErrorWithRetryDialog value)? errorWithRetryDialog,
    TResult? Function(_RequiredLoginDialog value)? requiredLoginDialog,
    TResult? Function(_BirthDayDialog value)? birthDayDialog,
    TResult? Function(_NotificationDialog value)? notificationDialog,
    TResult? Function(_ToolsDialog value)? toolsDialog,
    TResult? Function(_CalculateSavingDialog value)? calculateSavingDialog,
    TResult? Function(_ExchangeRateDialog value)? exchangeRateDialog,
    TResult? Function(_InterbankDialog value)? interbankDialog,
    TResult? Function(_CreateJobDialog value)? createJobDialog,
    TResult? Function(_PickerDateRangeDialog value)? pickerDateRangeDialog,
    TResult? Function(_CreateTodoDialog value)? createTodoDialog,
  }) {
    return requiredLoginDialog?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ConfirmDialog value)? confirmDialog,
    TResult Function(_ErrorWithRetryDialog value)? errorWithRetryDialog,
    TResult Function(_RequiredLoginDialog value)? requiredLoginDialog,
    TResult Function(_BirthDayDialog value)? birthDayDialog,
    TResult Function(_NotificationDialog value)? notificationDialog,
    TResult Function(_ToolsDialog value)? toolsDialog,
    TResult Function(_CalculateSavingDialog value)? calculateSavingDialog,
    TResult Function(_ExchangeRateDialog value)? exchangeRateDialog,
    TResult Function(_InterbankDialog value)? interbankDialog,
    TResult Function(_CreateJobDialog value)? createJobDialog,
    TResult Function(_PickerDateRangeDialog value)? pickerDateRangeDialog,
    TResult Function(_CreateTodoDialog value)? createTodoDialog,
    required TResult orElse(),
  }) {
    if (requiredLoginDialog != null) {
      return requiredLoginDialog(this);
    }
    return orElse();
  }
}

abstract class _RequiredLoginDialog implements AppPopupInfo {
  const factory _RequiredLoginDialog() = _$RequiredLoginDialogImpl;
}

/// @nodoc
abstract class _$$BirthDayDialogImplCopyWith<$Res> {
  factory _$$BirthDayDialogImplCopyWith(_$BirthDayDialogImpl value,
          $Res Function(_$BirthDayDialogImpl) then) =
      __$$BirthDayDialogImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$BirthDayDialogImplCopyWithImpl<$Res>
    extends _$AppPopupInfoCopyWithImpl<$Res, _$BirthDayDialogImpl>
    implements _$$BirthDayDialogImplCopyWith<$Res> {
  __$$BirthDayDialogImplCopyWithImpl(
      _$BirthDayDialogImpl _value, $Res Function(_$BirthDayDialogImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$BirthDayDialogImpl implements _BirthDayDialog {
  const _$BirthDayDialogImpl();

  @override
  String toString() {
    return 'AppPopupInfo.birthDayDialog()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$BirthDayDialogImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String message, Func0<void>? onPressed)
        confirmDialog,
    required TResult Function(String message, Func0<void>? onRetryPressed)
        errorWithRetryDialog,
    required TResult Function() requiredLoginDialog,
    required TResult Function() birthDayDialog,
    required TResult Function() notificationDialog,
    required TResult Function() toolsDialog,
    required TResult Function() calculateSavingDialog,
    required TResult Function() exchangeRateDialog,
    required TResult Function() interbankDialog,
    required TResult Function() createJobDialog,
    required TResult Function() pickerDateRangeDialog,
    required TResult Function() createTodoDialog,
  }) {
    return birthDayDialog();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String message, Func0<void>? onPressed)? confirmDialog,
    TResult? Function(String message, Func0<void>? onRetryPressed)?
        errorWithRetryDialog,
    TResult? Function()? requiredLoginDialog,
    TResult? Function()? birthDayDialog,
    TResult? Function()? notificationDialog,
    TResult? Function()? toolsDialog,
    TResult? Function()? calculateSavingDialog,
    TResult? Function()? exchangeRateDialog,
    TResult? Function()? interbankDialog,
    TResult? Function()? createJobDialog,
    TResult? Function()? pickerDateRangeDialog,
    TResult? Function()? createTodoDialog,
  }) {
    return birthDayDialog?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String message, Func0<void>? onPressed)? confirmDialog,
    TResult Function(String message, Func0<void>? onRetryPressed)?
        errorWithRetryDialog,
    TResult Function()? requiredLoginDialog,
    TResult Function()? birthDayDialog,
    TResult Function()? notificationDialog,
    TResult Function()? toolsDialog,
    TResult Function()? calculateSavingDialog,
    TResult Function()? exchangeRateDialog,
    TResult Function()? interbankDialog,
    TResult Function()? createJobDialog,
    TResult Function()? pickerDateRangeDialog,
    TResult Function()? createTodoDialog,
    required TResult orElse(),
  }) {
    if (birthDayDialog != null) {
      return birthDayDialog();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ConfirmDialog value) confirmDialog,
    required TResult Function(_ErrorWithRetryDialog value) errorWithRetryDialog,
    required TResult Function(_RequiredLoginDialog value) requiredLoginDialog,
    required TResult Function(_BirthDayDialog value) birthDayDialog,
    required TResult Function(_NotificationDialog value) notificationDialog,
    required TResult Function(_ToolsDialog value) toolsDialog,
    required TResult Function(_CalculateSavingDialog value)
        calculateSavingDialog,
    required TResult Function(_ExchangeRateDialog value) exchangeRateDialog,
    required TResult Function(_InterbankDialog value) interbankDialog,
    required TResult Function(_CreateJobDialog value) createJobDialog,
    required TResult Function(_PickerDateRangeDialog value)
        pickerDateRangeDialog,
    required TResult Function(_CreateTodoDialog value) createTodoDialog,
  }) {
    return birthDayDialog(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_ConfirmDialog value)? confirmDialog,
    TResult? Function(_ErrorWithRetryDialog value)? errorWithRetryDialog,
    TResult? Function(_RequiredLoginDialog value)? requiredLoginDialog,
    TResult? Function(_BirthDayDialog value)? birthDayDialog,
    TResult? Function(_NotificationDialog value)? notificationDialog,
    TResult? Function(_ToolsDialog value)? toolsDialog,
    TResult? Function(_CalculateSavingDialog value)? calculateSavingDialog,
    TResult? Function(_ExchangeRateDialog value)? exchangeRateDialog,
    TResult? Function(_InterbankDialog value)? interbankDialog,
    TResult? Function(_CreateJobDialog value)? createJobDialog,
    TResult? Function(_PickerDateRangeDialog value)? pickerDateRangeDialog,
    TResult? Function(_CreateTodoDialog value)? createTodoDialog,
  }) {
    return birthDayDialog?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ConfirmDialog value)? confirmDialog,
    TResult Function(_ErrorWithRetryDialog value)? errorWithRetryDialog,
    TResult Function(_RequiredLoginDialog value)? requiredLoginDialog,
    TResult Function(_BirthDayDialog value)? birthDayDialog,
    TResult Function(_NotificationDialog value)? notificationDialog,
    TResult Function(_ToolsDialog value)? toolsDialog,
    TResult Function(_CalculateSavingDialog value)? calculateSavingDialog,
    TResult Function(_ExchangeRateDialog value)? exchangeRateDialog,
    TResult Function(_InterbankDialog value)? interbankDialog,
    TResult Function(_CreateJobDialog value)? createJobDialog,
    TResult Function(_PickerDateRangeDialog value)? pickerDateRangeDialog,
    TResult Function(_CreateTodoDialog value)? createTodoDialog,
    required TResult orElse(),
  }) {
    if (birthDayDialog != null) {
      return birthDayDialog(this);
    }
    return orElse();
  }
}

abstract class _BirthDayDialog implements AppPopupInfo {
  const factory _BirthDayDialog() = _$BirthDayDialogImpl;
}

/// @nodoc
abstract class _$$NotificationDialogImplCopyWith<$Res> {
  factory _$$NotificationDialogImplCopyWith(_$NotificationDialogImpl value,
          $Res Function(_$NotificationDialogImpl) then) =
      __$$NotificationDialogImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$NotificationDialogImplCopyWithImpl<$Res>
    extends _$AppPopupInfoCopyWithImpl<$Res, _$NotificationDialogImpl>
    implements _$$NotificationDialogImplCopyWith<$Res> {
  __$$NotificationDialogImplCopyWithImpl(_$NotificationDialogImpl _value,
      $Res Function(_$NotificationDialogImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$NotificationDialogImpl implements _NotificationDialog {
  const _$NotificationDialogImpl();

  @override
  String toString() {
    return 'AppPopupInfo.notificationDialog()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$NotificationDialogImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String message, Func0<void>? onPressed)
        confirmDialog,
    required TResult Function(String message, Func0<void>? onRetryPressed)
        errorWithRetryDialog,
    required TResult Function() requiredLoginDialog,
    required TResult Function() birthDayDialog,
    required TResult Function() notificationDialog,
    required TResult Function() toolsDialog,
    required TResult Function() calculateSavingDialog,
    required TResult Function() exchangeRateDialog,
    required TResult Function() interbankDialog,
    required TResult Function() createJobDialog,
    required TResult Function() pickerDateRangeDialog,
    required TResult Function() createTodoDialog,
  }) {
    return notificationDialog();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String message, Func0<void>? onPressed)? confirmDialog,
    TResult? Function(String message, Func0<void>? onRetryPressed)?
        errorWithRetryDialog,
    TResult? Function()? requiredLoginDialog,
    TResult? Function()? birthDayDialog,
    TResult? Function()? notificationDialog,
    TResult? Function()? toolsDialog,
    TResult? Function()? calculateSavingDialog,
    TResult? Function()? exchangeRateDialog,
    TResult? Function()? interbankDialog,
    TResult? Function()? createJobDialog,
    TResult? Function()? pickerDateRangeDialog,
    TResult? Function()? createTodoDialog,
  }) {
    return notificationDialog?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String message, Func0<void>? onPressed)? confirmDialog,
    TResult Function(String message, Func0<void>? onRetryPressed)?
        errorWithRetryDialog,
    TResult Function()? requiredLoginDialog,
    TResult Function()? birthDayDialog,
    TResult Function()? notificationDialog,
    TResult Function()? toolsDialog,
    TResult Function()? calculateSavingDialog,
    TResult Function()? exchangeRateDialog,
    TResult Function()? interbankDialog,
    TResult Function()? createJobDialog,
    TResult Function()? pickerDateRangeDialog,
    TResult Function()? createTodoDialog,
    required TResult orElse(),
  }) {
    if (notificationDialog != null) {
      return notificationDialog();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ConfirmDialog value) confirmDialog,
    required TResult Function(_ErrorWithRetryDialog value) errorWithRetryDialog,
    required TResult Function(_RequiredLoginDialog value) requiredLoginDialog,
    required TResult Function(_BirthDayDialog value) birthDayDialog,
    required TResult Function(_NotificationDialog value) notificationDialog,
    required TResult Function(_ToolsDialog value) toolsDialog,
    required TResult Function(_CalculateSavingDialog value)
        calculateSavingDialog,
    required TResult Function(_ExchangeRateDialog value) exchangeRateDialog,
    required TResult Function(_InterbankDialog value) interbankDialog,
    required TResult Function(_CreateJobDialog value) createJobDialog,
    required TResult Function(_PickerDateRangeDialog value)
        pickerDateRangeDialog,
    required TResult Function(_CreateTodoDialog value) createTodoDialog,
  }) {
    return notificationDialog(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_ConfirmDialog value)? confirmDialog,
    TResult? Function(_ErrorWithRetryDialog value)? errorWithRetryDialog,
    TResult? Function(_RequiredLoginDialog value)? requiredLoginDialog,
    TResult? Function(_BirthDayDialog value)? birthDayDialog,
    TResult? Function(_NotificationDialog value)? notificationDialog,
    TResult? Function(_ToolsDialog value)? toolsDialog,
    TResult? Function(_CalculateSavingDialog value)? calculateSavingDialog,
    TResult? Function(_ExchangeRateDialog value)? exchangeRateDialog,
    TResult? Function(_InterbankDialog value)? interbankDialog,
    TResult? Function(_CreateJobDialog value)? createJobDialog,
    TResult? Function(_PickerDateRangeDialog value)? pickerDateRangeDialog,
    TResult? Function(_CreateTodoDialog value)? createTodoDialog,
  }) {
    return notificationDialog?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ConfirmDialog value)? confirmDialog,
    TResult Function(_ErrorWithRetryDialog value)? errorWithRetryDialog,
    TResult Function(_RequiredLoginDialog value)? requiredLoginDialog,
    TResult Function(_BirthDayDialog value)? birthDayDialog,
    TResult Function(_NotificationDialog value)? notificationDialog,
    TResult Function(_ToolsDialog value)? toolsDialog,
    TResult Function(_CalculateSavingDialog value)? calculateSavingDialog,
    TResult Function(_ExchangeRateDialog value)? exchangeRateDialog,
    TResult Function(_InterbankDialog value)? interbankDialog,
    TResult Function(_CreateJobDialog value)? createJobDialog,
    TResult Function(_PickerDateRangeDialog value)? pickerDateRangeDialog,
    TResult Function(_CreateTodoDialog value)? createTodoDialog,
    required TResult orElse(),
  }) {
    if (notificationDialog != null) {
      return notificationDialog(this);
    }
    return orElse();
  }
}

abstract class _NotificationDialog implements AppPopupInfo {
  const factory _NotificationDialog() = _$NotificationDialogImpl;
}

/// @nodoc
abstract class _$$ToolsDialogImplCopyWith<$Res> {
  factory _$$ToolsDialogImplCopyWith(
          _$ToolsDialogImpl value, $Res Function(_$ToolsDialogImpl) then) =
      __$$ToolsDialogImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ToolsDialogImplCopyWithImpl<$Res>
    extends _$AppPopupInfoCopyWithImpl<$Res, _$ToolsDialogImpl>
    implements _$$ToolsDialogImplCopyWith<$Res> {
  __$$ToolsDialogImplCopyWithImpl(
      _$ToolsDialogImpl _value, $Res Function(_$ToolsDialogImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ToolsDialogImpl implements _ToolsDialog {
  const _$ToolsDialogImpl();

  @override
  String toString() {
    return 'AppPopupInfo.toolsDialog()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ToolsDialogImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String message, Func0<void>? onPressed)
        confirmDialog,
    required TResult Function(String message, Func0<void>? onRetryPressed)
        errorWithRetryDialog,
    required TResult Function() requiredLoginDialog,
    required TResult Function() birthDayDialog,
    required TResult Function() notificationDialog,
    required TResult Function() toolsDialog,
    required TResult Function() calculateSavingDialog,
    required TResult Function() exchangeRateDialog,
    required TResult Function() interbankDialog,
    required TResult Function() createJobDialog,
    required TResult Function() pickerDateRangeDialog,
    required TResult Function() createTodoDialog,
  }) {
    return toolsDialog();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String message, Func0<void>? onPressed)? confirmDialog,
    TResult? Function(String message, Func0<void>? onRetryPressed)?
        errorWithRetryDialog,
    TResult? Function()? requiredLoginDialog,
    TResult? Function()? birthDayDialog,
    TResult? Function()? notificationDialog,
    TResult? Function()? toolsDialog,
    TResult? Function()? calculateSavingDialog,
    TResult? Function()? exchangeRateDialog,
    TResult? Function()? interbankDialog,
    TResult? Function()? createJobDialog,
    TResult? Function()? pickerDateRangeDialog,
    TResult? Function()? createTodoDialog,
  }) {
    return toolsDialog?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String message, Func0<void>? onPressed)? confirmDialog,
    TResult Function(String message, Func0<void>? onRetryPressed)?
        errorWithRetryDialog,
    TResult Function()? requiredLoginDialog,
    TResult Function()? birthDayDialog,
    TResult Function()? notificationDialog,
    TResult Function()? toolsDialog,
    TResult Function()? calculateSavingDialog,
    TResult Function()? exchangeRateDialog,
    TResult Function()? interbankDialog,
    TResult Function()? createJobDialog,
    TResult Function()? pickerDateRangeDialog,
    TResult Function()? createTodoDialog,
    required TResult orElse(),
  }) {
    if (toolsDialog != null) {
      return toolsDialog();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ConfirmDialog value) confirmDialog,
    required TResult Function(_ErrorWithRetryDialog value) errorWithRetryDialog,
    required TResult Function(_RequiredLoginDialog value) requiredLoginDialog,
    required TResult Function(_BirthDayDialog value) birthDayDialog,
    required TResult Function(_NotificationDialog value) notificationDialog,
    required TResult Function(_ToolsDialog value) toolsDialog,
    required TResult Function(_CalculateSavingDialog value)
        calculateSavingDialog,
    required TResult Function(_ExchangeRateDialog value) exchangeRateDialog,
    required TResult Function(_InterbankDialog value) interbankDialog,
    required TResult Function(_CreateJobDialog value) createJobDialog,
    required TResult Function(_PickerDateRangeDialog value)
        pickerDateRangeDialog,
    required TResult Function(_CreateTodoDialog value) createTodoDialog,
  }) {
    return toolsDialog(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_ConfirmDialog value)? confirmDialog,
    TResult? Function(_ErrorWithRetryDialog value)? errorWithRetryDialog,
    TResult? Function(_RequiredLoginDialog value)? requiredLoginDialog,
    TResult? Function(_BirthDayDialog value)? birthDayDialog,
    TResult? Function(_NotificationDialog value)? notificationDialog,
    TResult? Function(_ToolsDialog value)? toolsDialog,
    TResult? Function(_CalculateSavingDialog value)? calculateSavingDialog,
    TResult? Function(_ExchangeRateDialog value)? exchangeRateDialog,
    TResult? Function(_InterbankDialog value)? interbankDialog,
    TResult? Function(_CreateJobDialog value)? createJobDialog,
    TResult? Function(_PickerDateRangeDialog value)? pickerDateRangeDialog,
    TResult? Function(_CreateTodoDialog value)? createTodoDialog,
  }) {
    return toolsDialog?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ConfirmDialog value)? confirmDialog,
    TResult Function(_ErrorWithRetryDialog value)? errorWithRetryDialog,
    TResult Function(_RequiredLoginDialog value)? requiredLoginDialog,
    TResult Function(_BirthDayDialog value)? birthDayDialog,
    TResult Function(_NotificationDialog value)? notificationDialog,
    TResult Function(_ToolsDialog value)? toolsDialog,
    TResult Function(_CalculateSavingDialog value)? calculateSavingDialog,
    TResult Function(_ExchangeRateDialog value)? exchangeRateDialog,
    TResult Function(_InterbankDialog value)? interbankDialog,
    TResult Function(_CreateJobDialog value)? createJobDialog,
    TResult Function(_PickerDateRangeDialog value)? pickerDateRangeDialog,
    TResult Function(_CreateTodoDialog value)? createTodoDialog,
    required TResult orElse(),
  }) {
    if (toolsDialog != null) {
      return toolsDialog(this);
    }
    return orElse();
  }
}

abstract class _ToolsDialog implements AppPopupInfo {
  const factory _ToolsDialog() = _$ToolsDialogImpl;
}

/// @nodoc
abstract class _$$CalculateSavingDialogImplCopyWith<$Res> {
  factory _$$CalculateSavingDialogImplCopyWith(
          _$CalculateSavingDialogImpl value,
          $Res Function(_$CalculateSavingDialogImpl) then) =
      __$$CalculateSavingDialogImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$CalculateSavingDialogImplCopyWithImpl<$Res>
    extends _$AppPopupInfoCopyWithImpl<$Res, _$CalculateSavingDialogImpl>
    implements _$$CalculateSavingDialogImplCopyWith<$Res> {
  __$$CalculateSavingDialogImplCopyWithImpl(_$CalculateSavingDialogImpl _value,
      $Res Function(_$CalculateSavingDialogImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$CalculateSavingDialogImpl implements _CalculateSavingDialog {
  const _$CalculateSavingDialogImpl();

  @override
  String toString() {
    return 'AppPopupInfo.calculateSavingDialog()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CalculateSavingDialogImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String message, Func0<void>? onPressed)
        confirmDialog,
    required TResult Function(String message, Func0<void>? onRetryPressed)
        errorWithRetryDialog,
    required TResult Function() requiredLoginDialog,
    required TResult Function() birthDayDialog,
    required TResult Function() notificationDialog,
    required TResult Function() toolsDialog,
    required TResult Function() calculateSavingDialog,
    required TResult Function() exchangeRateDialog,
    required TResult Function() interbankDialog,
    required TResult Function() createJobDialog,
    required TResult Function() pickerDateRangeDialog,
    required TResult Function() createTodoDialog,
  }) {
    return calculateSavingDialog();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String message, Func0<void>? onPressed)? confirmDialog,
    TResult? Function(String message, Func0<void>? onRetryPressed)?
        errorWithRetryDialog,
    TResult? Function()? requiredLoginDialog,
    TResult? Function()? birthDayDialog,
    TResult? Function()? notificationDialog,
    TResult? Function()? toolsDialog,
    TResult? Function()? calculateSavingDialog,
    TResult? Function()? exchangeRateDialog,
    TResult? Function()? interbankDialog,
    TResult? Function()? createJobDialog,
    TResult? Function()? pickerDateRangeDialog,
    TResult? Function()? createTodoDialog,
  }) {
    return calculateSavingDialog?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String message, Func0<void>? onPressed)? confirmDialog,
    TResult Function(String message, Func0<void>? onRetryPressed)?
        errorWithRetryDialog,
    TResult Function()? requiredLoginDialog,
    TResult Function()? birthDayDialog,
    TResult Function()? notificationDialog,
    TResult Function()? toolsDialog,
    TResult Function()? calculateSavingDialog,
    TResult Function()? exchangeRateDialog,
    TResult Function()? interbankDialog,
    TResult Function()? createJobDialog,
    TResult Function()? pickerDateRangeDialog,
    TResult Function()? createTodoDialog,
    required TResult orElse(),
  }) {
    if (calculateSavingDialog != null) {
      return calculateSavingDialog();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ConfirmDialog value) confirmDialog,
    required TResult Function(_ErrorWithRetryDialog value) errorWithRetryDialog,
    required TResult Function(_RequiredLoginDialog value) requiredLoginDialog,
    required TResult Function(_BirthDayDialog value) birthDayDialog,
    required TResult Function(_NotificationDialog value) notificationDialog,
    required TResult Function(_ToolsDialog value) toolsDialog,
    required TResult Function(_CalculateSavingDialog value)
        calculateSavingDialog,
    required TResult Function(_ExchangeRateDialog value) exchangeRateDialog,
    required TResult Function(_InterbankDialog value) interbankDialog,
    required TResult Function(_CreateJobDialog value) createJobDialog,
    required TResult Function(_PickerDateRangeDialog value)
        pickerDateRangeDialog,
    required TResult Function(_CreateTodoDialog value) createTodoDialog,
  }) {
    return calculateSavingDialog(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_ConfirmDialog value)? confirmDialog,
    TResult? Function(_ErrorWithRetryDialog value)? errorWithRetryDialog,
    TResult? Function(_RequiredLoginDialog value)? requiredLoginDialog,
    TResult? Function(_BirthDayDialog value)? birthDayDialog,
    TResult? Function(_NotificationDialog value)? notificationDialog,
    TResult? Function(_ToolsDialog value)? toolsDialog,
    TResult? Function(_CalculateSavingDialog value)? calculateSavingDialog,
    TResult? Function(_ExchangeRateDialog value)? exchangeRateDialog,
    TResult? Function(_InterbankDialog value)? interbankDialog,
    TResult? Function(_CreateJobDialog value)? createJobDialog,
    TResult? Function(_PickerDateRangeDialog value)? pickerDateRangeDialog,
    TResult? Function(_CreateTodoDialog value)? createTodoDialog,
  }) {
    return calculateSavingDialog?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ConfirmDialog value)? confirmDialog,
    TResult Function(_ErrorWithRetryDialog value)? errorWithRetryDialog,
    TResult Function(_RequiredLoginDialog value)? requiredLoginDialog,
    TResult Function(_BirthDayDialog value)? birthDayDialog,
    TResult Function(_NotificationDialog value)? notificationDialog,
    TResult Function(_ToolsDialog value)? toolsDialog,
    TResult Function(_CalculateSavingDialog value)? calculateSavingDialog,
    TResult Function(_ExchangeRateDialog value)? exchangeRateDialog,
    TResult Function(_InterbankDialog value)? interbankDialog,
    TResult Function(_CreateJobDialog value)? createJobDialog,
    TResult Function(_PickerDateRangeDialog value)? pickerDateRangeDialog,
    TResult Function(_CreateTodoDialog value)? createTodoDialog,
    required TResult orElse(),
  }) {
    if (calculateSavingDialog != null) {
      return calculateSavingDialog(this);
    }
    return orElse();
  }
}

abstract class _CalculateSavingDialog implements AppPopupInfo {
  const factory _CalculateSavingDialog() = _$CalculateSavingDialogImpl;
}

/// @nodoc
abstract class _$$ExchangeRateDialogImplCopyWith<$Res> {
  factory _$$ExchangeRateDialogImplCopyWith(_$ExchangeRateDialogImpl value,
          $Res Function(_$ExchangeRateDialogImpl) then) =
      __$$ExchangeRateDialogImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ExchangeRateDialogImplCopyWithImpl<$Res>
    extends _$AppPopupInfoCopyWithImpl<$Res, _$ExchangeRateDialogImpl>
    implements _$$ExchangeRateDialogImplCopyWith<$Res> {
  __$$ExchangeRateDialogImplCopyWithImpl(_$ExchangeRateDialogImpl _value,
      $Res Function(_$ExchangeRateDialogImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ExchangeRateDialogImpl implements _ExchangeRateDialog {
  const _$ExchangeRateDialogImpl();

  @override
  String toString() {
    return 'AppPopupInfo.exchangeRateDialog()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ExchangeRateDialogImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String message, Func0<void>? onPressed)
        confirmDialog,
    required TResult Function(String message, Func0<void>? onRetryPressed)
        errorWithRetryDialog,
    required TResult Function() requiredLoginDialog,
    required TResult Function() birthDayDialog,
    required TResult Function() notificationDialog,
    required TResult Function() toolsDialog,
    required TResult Function() calculateSavingDialog,
    required TResult Function() exchangeRateDialog,
    required TResult Function() interbankDialog,
    required TResult Function() createJobDialog,
    required TResult Function() pickerDateRangeDialog,
    required TResult Function() createTodoDialog,
  }) {
    return exchangeRateDialog();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String message, Func0<void>? onPressed)? confirmDialog,
    TResult? Function(String message, Func0<void>? onRetryPressed)?
        errorWithRetryDialog,
    TResult? Function()? requiredLoginDialog,
    TResult? Function()? birthDayDialog,
    TResult? Function()? notificationDialog,
    TResult? Function()? toolsDialog,
    TResult? Function()? calculateSavingDialog,
    TResult? Function()? exchangeRateDialog,
    TResult? Function()? interbankDialog,
    TResult? Function()? createJobDialog,
    TResult? Function()? pickerDateRangeDialog,
    TResult? Function()? createTodoDialog,
  }) {
    return exchangeRateDialog?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String message, Func0<void>? onPressed)? confirmDialog,
    TResult Function(String message, Func0<void>? onRetryPressed)?
        errorWithRetryDialog,
    TResult Function()? requiredLoginDialog,
    TResult Function()? birthDayDialog,
    TResult Function()? notificationDialog,
    TResult Function()? toolsDialog,
    TResult Function()? calculateSavingDialog,
    TResult Function()? exchangeRateDialog,
    TResult Function()? interbankDialog,
    TResult Function()? createJobDialog,
    TResult Function()? pickerDateRangeDialog,
    TResult Function()? createTodoDialog,
    required TResult orElse(),
  }) {
    if (exchangeRateDialog != null) {
      return exchangeRateDialog();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ConfirmDialog value) confirmDialog,
    required TResult Function(_ErrorWithRetryDialog value) errorWithRetryDialog,
    required TResult Function(_RequiredLoginDialog value) requiredLoginDialog,
    required TResult Function(_BirthDayDialog value) birthDayDialog,
    required TResult Function(_NotificationDialog value) notificationDialog,
    required TResult Function(_ToolsDialog value) toolsDialog,
    required TResult Function(_CalculateSavingDialog value)
        calculateSavingDialog,
    required TResult Function(_ExchangeRateDialog value) exchangeRateDialog,
    required TResult Function(_InterbankDialog value) interbankDialog,
    required TResult Function(_CreateJobDialog value) createJobDialog,
    required TResult Function(_PickerDateRangeDialog value)
        pickerDateRangeDialog,
    required TResult Function(_CreateTodoDialog value) createTodoDialog,
  }) {
    return exchangeRateDialog(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_ConfirmDialog value)? confirmDialog,
    TResult? Function(_ErrorWithRetryDialog value)? errorWithRetryDialog,
    TResult? Function(_RequiredLoginDialog value)? requiredLoginDialog,
    TResult? Function(_BirthDayDialog value)? birthDayDialog,
    TResult? Function(_NotificationDialog value)? notificationDialog,
    TResult? Function(_ToolsDialog value)? toolsDialog,
    TResult? Function(_CalculateSavingDialog value)? calculateSavingDialog,
    TResult? Function(_ExchangeRateDialog value)? exchangeRateDialog,
    TResult? Function(_InterbankDialog value)? interbankDialog,
    TResult? Function(_CreateJobDialog value)? createJobDialog,
    TResult? Function(_PickerDateRangeDialog value)? pickerDateRangeDialog,
    TResult? Function(_CreateTodoDialog value)? createTodoDialog,
  }) {
    return exchangeRateDialog?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ConfirmDialog value)? confirmDialog,
    TResult Function(_ErrorWithRetryDialog value)? errorWithRetryDialog,
    TResult Function(_RequiredLoginDialog value)? requiredLoginDialog,
    TResult Function(_BirthDayDialog value)? birthDayDialog,
    TResult Function(_NotificationDialog value)? notificationDialog,
    TResult Function(_ToolsDialog value)? toolsDialog,
    TResult Function(_CalculateSavingDialog value)? calculateSavingDialog,
    TResult Function(_ExchangeRateDialog value)? exchangeRateDialog,
    TResult Function(_InterbankDialog value)? interbankDialog,
    TResult Function(_CreateJobDialog value)? createJobDialog,
    TResult Function(_PickerDateRangeDialog value)? pickerDateRangeDialog,
    TResult Function(_CreateTodoDialog value)? createTodoDialog,
    required TResult orElse(),
  }) {
    if (exchangeRateDialog != null) {
      return exchangeRateDialog(this);
    }
    return orElse();
  }
}

abstract class _ExchangeRateDialog implements AppPopupInfo {
  const factory _ExchangeRateDialog() = _$ExchangeRateDialogImpl;
}

/// @nodoc
abstract class _$$InterbankDialogImplCopyWith<$Res> {
  factory _$$InterbankDialogImplCopyWith(_$InterbankDialogImpl value,
          $Res Function(_$InterbankDialogImpl) then) =
      __$$InterbankDialogImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InterbankDialogImplCopyWithImpl<$Res>
    extends _$AppPopupInfoCopyWithImpl<$Res, _$InterbankDialogImpl>
    implements _$$InterbankDialogImplCopyWith<$Res> {
  __$$InterbankDialogImplCopyWithImpl(
      _$InterbankDialogImpl _value, $Res Function(_$InterbankDialogImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$InterbankDialogImpl implements _InterbankDialog {
  const _$InterbankDialogImpl();

  @override
  String toString() {
    return 'AppPopupInfo.interbankDialog()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$InterbankDialogImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String message, Func0<void>? onPressed)
        confirmDialog,
    required TResult Function(String message, Func0<void>? onRetryPressed)
        errorWithRetryDialog,
    required TResult Function() requiredLoginDialog,
    required TResult Function() birthDayDialog,
    required TResult Function() notificationDialog,
    required TResult Function() toolsDialog,
    required TResult Function() calculateSavingDialog,
    required TResult Function() exchangeRateDialog,
    required TResult Function() interbankDialog,
    required TResult Function() createJobDialog,
    required TResult Function() pickerDateRangeDialog,
    required TResult Function() createTodoDialog,
  }) {
    return interbankDialog();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String message, Func0<void>? onPressed)? confirmDialog,
    TResult? Function(String message, Func0<void>? onRetryPressed)?
        errorWithRetryDialog,
    TResult? Function()? requiredLoginDialog,
    TResult? Function()? birthDayDialog,
    TResult? Function()? notificationDialog,
    TResult? Function()? toolsDialog,
    TResult? Function()? calculateSavingDialog,
    TResult? Function()? exchangeRateDialog,
    TResult? Function()? interbankDialog,
    TResult? Function()? createJobDialog,
    TResult? Function()? pickerDateRangeDialog,
    TResult? Function()? createTodoDialog,
  }) {
    return interbankDialog?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String message, Func0<void>? onPressed)? confirmDialog,
    TResult Function(String message, Func0<void>? onRetryPressed)?
        errorWithRetryDialog,
    TResult Function()? requiredLoginDialog,
    TResult Function()? birthDayDialog,
    TResult Function()? notificationDialog,
    TResult Function()? toolsDialog,
    TResult Function()? calculateSavingDialog,
    TResult Function()? exchangeRateDialog,
    TResult Function()? interbankDialog,
    TResult Function()? createJobDialog,
    TResult Function()? pickerDateRangeDialog,
    TResult Function()? createTodoDialog,
    required TResult orElse(),
  }) {
    if (interbankDialog != null) {
      return interbankDialog();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ConfirmDialog value) confirmDialog,
    required TResult Function(_ErrorWithRetryDialog value) errorWithRetryDialog,
    required TResult Function(_RequiredLoginDialog value) requiredLoginDialog,
    required TResult Function(_BirthDayDialog value) birthDayDialog,
    required TResult Function(_NotificationDialog value) notificationDialog,
    required TResult Function(_ToolsDialog value) toolsDialog,
    required TResult Function(_CalculateSavingDialog value)
        calculateSavingDialog,
    required TResult Function(_ExchangeRateDialog value) exchangeRateDialog,
    required TResult Function(_InterbankDialog value) interbankDialog,
    required TResult Function(_CreateJobDialog value) createJobDialog,
    required TResult Function(_PickerDateRangeDialog value)
        pickerDateRangeDialog,
    required TResult Function(_CreateTodoDialog value) createTodoDialog,
  }) {
    return interbankDialog(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_ConfirmDialog value)? confirmDialog,
    TResult? Function(_ErrorWithRetryDialog value)? errorWithRetryDialog,
    TResult? Function(_RequiredLoginDialog value)? requiredLoginDialog,
    TResult? Function(_BirthDayDialog value)? birthDayDialog,
    TResult? Function(_NotificationDialog value)? notificationDialog,
    TResult? Function(_ToolsDialog value)? toolsDialog,
    TResult? Function(_CalculateSavingDialog value)? calculateSavingDialog,
    TResult? Function(_ExchangeRateDialog value)? exchangeRateDialog,
    TResult? Function(_InterbankDialog value)? interbankDialog,
    TResult? Function(_CreateJobDialog value)? createJobDialog,
    TResult? Function(_PickerDateRangeDialog value)? pickerDateRangeDialog,
    TResult? Function(_CreateTodoDialog value)? createTodoDialog,
  }) {
    return interbankDialog?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ConfirmDialog value)? confirmDialog,
    TResult Function(_ErrorWithRetryDialog value)? errorWithRetryDialog,
    TResult Function(_RequiredLoginDialog value)? requiredLoginDialog,
    TResult Function(_BirthDayDialog value)? birthDayDialog,
    TResult Function(_NotificationDialog value)? notificationDialog,
    TResult Function(_ToolsDialog value)? toolsDialog,
    TResult Function(_CalculateSavingDialog value)? calculateSavingDialog,
    TResult Function(_ExchangeRateDialog value)? exchangeRateDialog,
    TResult Function(_InterbankDialog value)? interbankDialog,
    TResult Function(_CreateJobDialog value)? createJobDialog,
    TResult Function(_PickerDateRangeDialog value)? pickerDateRangeDialog,
    TResult Function(_CreateTodoDialog value)? createTodoDialog,
    required TResult orElse(),
  }) {
    if (interbankDialog != null) {
      return interbankDialog(this);
    }
    return orElse();
  }
}

abstract class _InterbankDialog implements AppPopupInfo {
  const factory _InterbankDialog() = _$InterbankDialogImpl;
}

/// @nodoc
abstract class _$$CreateJobDialogImplCopyWith<$Res> {
  factory _$$CreateJobDialogImplCopyWith(_$CreateJobDialogImpl value,
          $Res Function(_$CreateJobDialogImpl) then) =
      __$$CreateJobDialogImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$CreateJobDialogImplCopyWithImpl<$Res>
    extends _$AppPopupInfoCopyWithImpl<$Res, _$CreateJobDialogImpl>
    implements _$$CreateJobDialogImplCopyWith<$Res> {
  __$$CreateJobDialogImplCopyWithImpl(
      _$CreateJobDialogImpl _value, $Res Function(_$CreateJobDialogImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$CreateJobDialogImpl implements _CreateJobDialog {
  const _$CreateJobDialogImpl();

  @override
  String toString() {
    return 'AppPopupInfo.createJobDialog()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$CreateJobDialogImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String message, Func0<void>? onPressed)
        confirmDialog,
    required TResult Function(String message, Func0<void>? onRetryPressed)
        errorWithRetryDialog,
    required TResult Function() requiredLoginDialog,
    required TResult Function() birthDayDialog,
    required TResult Function() notificationDialog,
    required TResult Function() toolsDialog,
    required TResult Function() calculateSavingDialog,
    required TResult Function() exchangeRateDialog,
    required TResult Function() interbankDialog,
    required TResult Function() createJobDialog,
    required TResult Function() pickerDateRangeDialog,
    required TResult Function() createTodoDialog,
  }) {
    return createJobDialog();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String message, Func0<void>? onPressed)? confirmDialog,
    TResult? Function(String message, Func0<void>? onRetryPressed)?
        errorWithRetryDialog,
    TResult? Function()? requiredLoginDialog,
    TResult? Function()? birthDayDialog,
    TResult? Function()? notificationDialog,
    TResult? Function()? toolsDialog,
    TResult? Function()? calculateSavingDialog,
    TResult? Function()? exchangeRateDialog,
    TResult? Function()? interbankDialog,
    TResult? Function()? createJobDialog,
    TResult? Function()? pickerDateRangeDialog,
    TResult? Function()? createTodoDialog,
  }) {
    return createJobDialog?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String message, Func0<void>? onPressed)? confirmDialog,
    TResult Function(String message, Func0<void>? onRetryPressed)?
        errorWithRetryDialog,
    TResult Function()? requiredLoginDialog,
    TResult Function()? birthDayDialog,
    TResult Function()? notificationDialog,
    TResult Function()? toolsDialog,
    TResult Function()? calculateSavingDialog,
    TResult Function()? exchangeRateDialog,
    TResult Function()? interbankDialog,
    TResult Function()? createJobDialog,
    TResult Function()? pickerDateRangeDialog,
    TResult Function()? createTodoDialog,
    required TResult orElse(),
  }) {
    if (createJobDialog != null) {
      return createJobDialog();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ConfirmDialog value) confirmDialog,
    required TResult Function(_ErrorWithRetryDialog value) errorWithRetryDialog,
    required TResult Function(_RequiredLoginDialog value) requiredLoginDialog,
    required TResult Function(_BirthDayDialog value) birthDayDialog,
    required TResult Function(_NotificationDialog value) notificationDialog,
    required TResult Function(_ToolsDialog value) toolsDialog,
    required TResult Function(_CalculateSavingDialog value)
        calculateSavingDialog,
    required TResult Function(_ExchangeRateDialog value) exchangeRateDialog,
    required TResult Function(_InterbankDialog value) interbankDialog,
    required TResult Function(_CreateJobDialog value) createJobDialog,
    required TResult Function(_PickerDateRangeDialog value)
        pickerDateRangeDialog,
    required TResult Function(_CreateTodoDialog value) createTodoDialog,
  }) {
    return createJobDialog(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_ConfirmDialog value)? confirmDialog,
    TResult? Function(_ErrorWithRetryDialog value)? errorWithRetryDialog,
    TResult? Function(_RequiredLoginDialog value)? requiredLoginDialog,
    TResult? Function(_BirthDayDialog value)? birthDayDialog,
    TResult? Function(_NotificationDialog value)? notificationDialog,
    TResult? Function(_ToolsDialog value)? toolsDialog,
    TResult? Function(_CalculateSavingDialog value)? calculateSavingDialog,
    TResult? Function(_ExchangeRateDialog value)? exchangeRateDialog,
    TResult? Function(_InterbankDialog value)? interbankDialog,
    TResult? Function(_CreateJobDialog value)? createJobDialog,
    TResult? Function(_PickerDateRangeDialog value)? pickerDateRangeDialog,
    TResult? Function(_CreateTodoDialog value)? createTodoDialog,
  }) {
    return createJobDialog?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ConfirmDialog value)? confirmDialog,
    TResult Function(_ErrorWithRetryDialog value)? errorWithRetryDialog,
    TResult Function(_RequiredLoginDialog value)? requiredLoginDialog,
    TResult Function(_BirthDayDialog value)? birthDayDialog,
    TResult Function(_NotificationDialog value)? notificationDialog,
    TResult Function(_ToolsDialog value)? toolsDialog,
    TResult Function(_CalculateSavingDialog value)? calculateSavingDialog,
    TResult Function(_ExchangeRateDialog value)? exchangeRateDialog,
    TResult Function(_InterbankDialog value)? interbankDialog,
    TResult Function(_CreateJobDialog value)? createJobDialog,
    TResult Function(_PickerDateRangeDialog value)? pickerDateRangeDialog,
    TResult Function(_CreateTodoDialog value)? createTodoDialog,
    required TResult orElse(),
  }) {
    if (createJobDialog != null) {
      return createJobDialog(this);
    }
    return orElse();
  }
}

abstract class _CreateJobDialog implements AppPopupInfo {
  const factory _CreateJobDialog() = _$CreateJobDialogImpl;
}

/// @nodoc
abstract class _$$PickerDateRangeDialogImplCopyWith<$Res> {
  factory _$$PickerDateRangeDialogImplCopyWith(
          _$PickerDateRangeDialogImpl value,
          $Res Function(_$PickerDateRangeDialogImpl) then) =
      __$$PickerDateRangeDialogImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$PickerDateRangeDialogImplCopyWithImpl<$Res>
    extends _$AppPopupInfoCopyWithImpl<$Res, _$PickerDateRangeDialogImpl>
    implements _$$PickerDateRangeDialogImplCopyWith<$Res> {
  __$$PickerDateRangeDialogImplCopyWithImpl(_$PickerDateRangeDialogImpl _value,
      $Res Function(_$PickerDateRangeDialogImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$PickerDateRangeDialogImpl implements _PickerDateRangeDialog {
  const _$PickerDateRangeDialogImpl();

  @override
  String toString() {
    return 'AppPopupInfo.pickerDateRangeDialog()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$PickerDateRangeDialogImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String message, Func0<void>? onPressed)
        confirmDialog,
    required TResult Function(String message, Func0<void>? onRetryPressed)
        errorWithRetryDialog,
    required TResult Function() requiredLoginDialog,
    required TResult Function() birthDayDialog,
    required TResult Function() notificationDialog,
    required TResult Function() toolsDialog,
    required TResult Function() calculateSavingDialog,
    required TResult Function() exchangeRateDialog,
    required TResult Function() interbankDialog,
    required TResult Function() createJobDialog,
    required TResult Function() pickerDateRangeDialog,
    required TResult Function() createTodoDialog,
  }) {
    return pickerDateRangeDialog();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String message, Func0<void>? onPressed)? confirmDialog,
    TResult? Function(String message, Func0<void>? onRetryPressed)?
        errorWithRetryDialog,
    TResult? Function()? requiredLoginDialog,
    TResult? Function()? birthDayDialog,
    TResult? Function()? notificationDialog,
    TResult? Function()? toolsDialog,
    TResult? Function()? calculateSavingDialog,
    TResult? Function()? exchangeRateDialog,
    TResult? Function()? interbankDialog,
    TResult? Function()? createJobDialog,
    TResult? Function()? pickerDateRangeDialog,
    TResult? Function()? createTodoDialog,
  }) {
    return pickerDateRangeDialog?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String message, Func0<void>? onPressed)? confirmDialog,
    TResult Function(String message, Func0<void>? onRetryPressed)?
        errorWithRetryDialog,
    TResult Function()? requiredLoginDialog,
    TResult Function()? birthDayDialog,
    TResult Function()? notificationDialog,
    TResult Function()? toolsDialog,
    TResult Function()? calculateSavingDialog,
    TResult Function()? exchangeRateDialog,
    TResult Function()? interbankDialog,
    TResult Function()? createJobDialog,
    TResult Function()? pickerDateRangeDialog,
    TResult Function()? createTodoDialog,
    required TResult orElse(),
  }) {
    if (pickerDateRangeDialog != null) {
      return pickerDateRangeDialog();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ConfirmDialog value) confirmDialog,
    required TResult Function(_ErrorWithRetryDialog value) errorWithRetryDialog,
    required TResult Function(_RequiredLoginDialog value) requiredLoginDialog,
    required TResult Function(_BirthDayDialog value) birthDayDialog,
    required TResult Function(_NotificationDialog value) notificationDialog,
    required TResult Function(_ToolsDialog value) toolsDialog,
    required TResult Function(_CalculateSavingDialog value)
        calculateSavingDialog,
    required TResult Function(_ExchangeRateDialog value) exchangeRateDialog,
    required TResult Function(_InterbankDialog value) interbankDialog,
    required TResult Function(_CreateJobDialog value) createJobDialog,
    required TResult Function(_PickerDateRangeDialog value)
        pickerDateRangeDialog,
    required TResult Function(_CreateTodoDialog value) createTodoDialog,
  }) {
    return pickerDateRangeDialog(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_ConfirmDialog value)? confirmDialog,
    TResult? Function(_ErrorWithRetryDialog value)? errorWithRetryDialog,
    TResult? Function(_RequiredLoginDialog value)? requiredLoginDialog,
    TResult? Function(_BirthDayDialog value)? birthDayDialog,
    TResult? Function(_NotificationDialog value)? notificationDialog,
    TResult? Function(_ToolsDialog value)? toolsDialog,
    TResult? Function(_CalculateSavingDialog value)? calculateSavingDialog,
    TResult? Function(_ExchangeRateDialog value)? exchangeRateDialog,
    TResult? Function(_InterbankDialog value)? interbankDialog,
    TResult? Function(_CreateJobDialog value)? createJobDialog,
    TResult? Function(_PickerDateRangeDialog value)? pickerDateRangeDialog,
    TResult? Function(_CreateTodoDialog value)? createTodoDialog,
  }) {
    return pickerDateRangeDialog?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ConfirmDialog value)? confirmDialog,
    TResult Function(_ErrorWithRetryDialog value)? errorWithRetryDialog,
    TResult Function(_RequiredLoginDialog value)? requiredLoginDialog,
    TResult Function(_BirthDayDialog value)? birthDayDialog,
    TResult Function(_NotificationDialog value)? notificationDialog,
    TResult Function(_ToolsDialog value)? toolsDialog,
    TResult Function(_CalculateSavingDialog value)? calculateSavingDialog,
    TResult Function(_ExchangeRateDialog value)? exchangeRateDialog,
    TResult Function(_InterbankDialog value)? interbankDialog,
    TResult Function(_CreateJobDialog value)? createJobDialog,
    TResult Function(_PickerDateRangeDialog value)? pickerDateRangeDialog,
    TResult Function(_CreateTodoDialog value)? createTodoDialog,
    required TResult orElse(),
  }) {
    if (pickerDateRangeDialog != null) {
      return pickerDateRangeDialog(this);
    }
    return orElse();
  }
}

abstract class _PickerDateRangeDialog implements AppPopupInfo {
  const factory _PickerDateRangeDialog() = _$PickerDateRangeDialogImpl;
}

/// @nodoc
abstract class _$$CreateTodoDialogImplCopyWith<$Res> {
  factory _$$CreateTodoDialogImplCopyWith(_$CreateTodoDialogImpl value,
          $Res Function(_$CreateTodoDialogImpl) then) =
      __$$CreateTodoDialogImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$CreateTodoDialogImplCopyWithImpl<$Res>
    extends _$AppPopupInfoCopyWithImpl<$Res, _$CreateTodoDialogImpl>
    implements _$$CreateTodoDialogImplCopyWith<$Res> {
  __$$CreateTodoDialogImplCopyWithImpl(_$CreateTodoDialogImpl _value,
      $Res Function(_$CreateTodoDialogImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$CreateTodoDialogImpl implements _CreateTodoDialog {
  const _$CreateTodoDialogImpl();

  @override
  String toString() {
    return 'AppPopupInfo.createTodoDialog()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$CreateTodoDialogImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String message, Func0<void>? onPressed)
        confirmDialog,
    required TResult Function(String message, Func0<void>? onRetryPressed)
        errorWithRetryDialog,
    required TResult Function() requiredLoginDialog,
    required TResult Function() birthDayDialog,
    required TResult Function() notificationDialog,
    required TResult Function() toolsDialog,
    required TResult Function() calculateSavingDialog,
    required TResult Function() exchangeRateDialog,
    required TResult Function() interbankDialog,
    required TResult Function() createJobDialog,
    required TResult Function() pickerDateRangeDialog,
    required TResult Function() createTodoDialog,
  }) {
    return createTodoDialog();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String message, Func0<void>? onPressed)? confirmDialog,
    TResult? Function(String message, Func0<void>? onRetryPressed)?
        errorWithRetryDialog,
    TResult? Function()? requiredLoginDialog,
    TResult? Function()? birthDayDialog,
    TResult? Function()? notificationDialog,
    TResult? Function()? toolsDialog,
    TResult? Function()? calculateSavingDialog,
    TResult? Function()? exchangeRateDialog,
    TResult? Function()? interbankDialog,
    TResult? Function()? createJobDialog,
    TResult? Function()? pickerDateRangeDialog,
    TResult? Function()? createTodoDialog,
  }) {
    return createTodoDialog?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String message, Func0<void>? onPressed)? confirmDialog,
    TResult Function(String message, Func0<void>? onRetryPressed)?
        errorWithRetryDialog,
    TResult Function()? requiredLoginDialog,
    TResult Function()? birthDayDialog,
    TResult Function()? notificationDialog,
    TResult Function()? toolsDialog,
    TResult Function()? calculateSavingDialog,
    TResult Function()? exchangeRateDialog,
    TResult Function()? interbankDialog,
    TResult Function()? createJobDialog,
    TResult Function()? pickerDateRangeDialog,
    TResult Function()? createTodoDialog,
    required TResult orElse(),
  }) {
    if (createTodoDialog != null) {
      return createTodoDialog();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ConfirmDialog value) confirmDialog,
    required TResult Function(_ErrorWithRetryDialog value) errorWithRetryDialog,
    required TResult Function(_RequiredLoginDialog value) requiredLoginDialog,
    required TResult Function(_BirthDayDialog value) birthDayDialog,
    required TResult Function(_NotificationDialog value) notificationDialog,
    required TResult Function(_ToolsDialog value) toolsDialog,
    required TResult Function(_CalculateSavingDialog value)
        calculateSavingDialog,
    required TResult Function(_ExchangeRateDialog value) exchangeRateDialog,
    required TResult Function(_InterbankDialog value) interbankDialog,
    required TResult Function(_CreateJobDialog value) createJobDialog,
    required TResult Function(_PickerDateRangeDialog value)
        pickerDateRangeDialog,
    required TResult Function(_CreateTodoDialog value) createTodoDialog,
  }) {
    return createTodoDialog(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_ConfirmDialog value)? confirmDialog,
    TResult? Function(_ErrorWithRetryDialog value)? errorWithRetryDialog,
    TResult? Function(_RequiredLoginDialog value)? requiredLoginDialog,
    TResult? Function(_BirthDayDialog value)? birthDayDialog,
    TResult? Function(_NotificationDialog value)? notificationDialog,
    TResult? Function(_ToolsDialog value)? toolsDialog,
    TResult? Function(_CalculateSavingDialog value)? calculateSavingDialog,
    TResult? Function(_ExchangeRateDialog value)? exchangeRateDialog,
    TResult? Function(_InterbankDialog value)? interbankDialog,
    TResult? Function(_CreateJobDialog value)? createJobDialog,
    TResult? Function(_PickerDateRangeDialog value)? pickerDateRangeDialog,
    TResult? Function(_CreateTodoDialog value)? createTodoDialog,
  }) {
    return createTodoDialog?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ConfirmDialog value)? confirmDialog,
    TResult Function(_ErrorWithRetryDialog value)? errorWithRetryDialog,
    TResult Function(_RequiredLoginDialog value)? requiredLoginDialog,
    TResult Function(_BirthDayDialog value)? birthDayDialog,
    TResult Function(_NotificationDialog value)? notificationDialog,
    TResult Function(_ToolsDialog value)? toolsDialog,
    TResult Function(_CalculateSavingDialog value)? calculateSavingDialog,
    TResult Function(_ExchangeRateDialog value)? exchangeRateDialog,
    TResult Function(_InterbankDialog value)? interbankDialog,
    TResult Function(_CreateJobDialog value)? createJobDialog,
    TResult Function(_PickerDateRangeDialog value)? pickerDateRangeDialog,
    TResult Function(_CreateTodoDialog value)? createTodoDialog,
    required TResult orElse(),
  }) {
    if (createTodoDialog != null) {
      return createTodoDialog(this);
    }
    return orElse();
  }
}

abstract class _CreateTodoDialog implements AppPopupInfo {
  const factory _CreateTodoDialog() = _$CreateTodoDialogImpl;
}
