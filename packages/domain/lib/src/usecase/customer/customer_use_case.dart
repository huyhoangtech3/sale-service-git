import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:shared/shared.dart';

import 'package:domain/domain.dart';

part 'customer_use_case.freezed.dart';

@Injectable()
class CustomerUseCase extends BaseLoadMoreUseCase<GetCustomerInput, Customer> {
  CustomerUseCase(this._repository) : super(initPage: PagingConstants.initialPage);

  final Repository _repository;

  @protected
  @override
  Future<PagedList<Customer>> buildUseCase(GetCustomerInput input) {
    return _repository.getCustomers(
      pageNumber: page,
      pageSize: PagingConstants.itemsPerPage,
      rsId: _repository.getUserInfoPreference().id,
      scope: Constants.view,
      search: input.search,
      searchFastType: input.searchFastType,
      customerType: Constants.indiv,
    );
  }
}

@freezed
class GetCustomerInput extends BaseInput with _$GetCustomerInput {
  const factory GetCustomerInput({
    required String search,
    required String searchFastType,
  }) = _GetCustomerInput;
}
