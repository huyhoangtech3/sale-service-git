// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'customer_use_case.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$GetCustomerInput {
  String get search => throw _privateConstructorUsedError;
  String get searchFastType => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $GetCustomerInputCopyWith<GetCustomerInput> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GetCustomerInputCopyWith<$Res> {
  factory $GetCustomerInputCopyWith(
          GetCustomerInput value, $Res Function(GetCustomerInput) then) =
      _$GetCustomerInputCopyWithImpl<$Res, GetCustomerInput>;
  @useResult
  $Res call({String search, String searchFastType});
}

/// @nodoc
class _$GetCustomerInputCopyWithImpl<$Res, $Val extends GetCustomerInput>
    implements $GetCustomerInputCopyWith<$Res> {
  _$GetCustomerInputCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? search = null,
    Object? searchFastType = null,
  }) {
    return _then(_value.copyWith(
      search: null == search
          ? _value.search
          : search // ignore: cast_nullable_to_non_nullable
              as String,
      searchFastType: null == searchFastType
          ? _value.searchFastType
          : searchFastType // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$GetCustomerInputImplCopyWith<$Res>
    implements $GetCustomerInputCopyWith<$Res> {
  factory _$$GetCustomerInputImplCopyWith(_$GetCustomerInputImpl value,
          $Res Function(_$GetCustomerInputImpl) then) =
      __$$GetCustomerInputImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String search, String searchFastType});
}

/// @nodoc
class __$$GetCustomerInputImplCopyWithImpl<$Res>
    extends _$GetCustomerInputCopyWithImpl<$Res, _$GetCustomerInputImpl>
    implements _$$GetCustomerInputImplCopyWith<$Res> {
  __$$GetCustomerInputImplCopyWithImpl(_$GetCustomerInputImpl _value,
      $Res Function(_$GetCustomerInputImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? search = null,
    Object? searchFastType = null,
  }) {
    return _then(_$GetCustomerInputImpl(
      search: null == search
          ? _value.search
          : search // ignore: cast_nullable_to_non_nullable
              as String,
      searchFastType: null == searchFastType
          ? _value.searchFastType
          : searchFastType // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$GetCustomerInputImpl implements _GetCustomerInput {
  const _$GetCustomerInputImpl(
      {required this.search, required this.searchFastType});

  @override
  final String search;
  @override
  final String searchFastType;

  @override
  String toString() {
    return 'GetCustomerInput(search: $search, searchFastType: $searchFastType)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GetCustomerInputImpl &&
            (identical(other.search, search) || other.search == search) &&
            (identical(other.searchFastType, searchFastType) ||
                other.searchFastType == searchFastType));
  }

  @override
  int get hashCode => Object.hash(runtimeType, search, searchFastType);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$GetCustomerInputImplCopyWith<_$GetCustomerInputImpl> get copyWith =>
      __$$GetCustomerInputImplCopyWithImpl<_$GetCustomerInputImpl>(
          this, _$identity);
}

abstract class _GetCustomerInput implements GetCustomerInput {
  const factory _GetCustomerInput(
      {required final String search,
      required final String searchFastType}) = _$GetCustomerInputImpl;

  @override
  String get search;
  @override
  String get searchFastType;
  @override
  @JsonKey(ignore: true)
  _$$GetCustomerInputImplCopyWith<_$GetCustomerInputImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
