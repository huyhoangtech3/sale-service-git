// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'user_info_use_case.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$UserInfoInput {
  String get username => throw _privateConstructorUsedError;
  int get externalData => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $UserInfoInputCopyWith<UserInfoInput> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserInfoInputCopyWith<$Res> {
  factory $UserInfoInputCopyWith(
          UserInfoInput value, $Res Function(UserInfoInput) then) =
      _$UserInfoInputCopyWithImpl<$Res, UserInfoInput>;
  @useResult
  $Res call({String username, int externalData});
}

/// @nodoc
class _$UserInfoInputCopyWithImpl<$Res, $Val extends UserInfoInput>
    implements $UserInfoInputCopyWith<$Res> {
  _$UserInfoInputCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? username = null,
    Object? externalData = null,
  }) {
    return _then(_value.copyWith(
      username: null == username
          ? _value.username
          : username // ignore: cast_nullable_to_non_nullable
              as String,
      externalData: null == externalData
          ? _value.externalData
          : externalData // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$UserInfoInputImplCopyWith<$Res>
    implements $UserInfoInputCopyWith<$Res> {
  factory _$$UserInfoInputImplCopyWith(
          _$UserInfoInputImpl value, $Res Function(_$UserInfoInputImpl) then) =
      __$$UserInfoInputImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String username, int externalData});
}

/// @nodoc
class __$$UserInfoInputImplCopyWithImpl<$Res>
    extends _$UserInfoInputCopyWithImpl<$Res, _$UserInfoInputImpl>
    implements _$$UserInfoInputImplCopyWith<$Res> {
  __$$UserInfoInputImplCopyWithImpl(
      _$UserInfoInputImpl _value, $Res Function(_$UserInfoInputImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? username = null,
    Object? externalData = null,
  }) {
    return _then(_$UserInfoInputImpl(
      username: null == username
          ? _value.username
          : username // ignore: cast_nullable_to_non_nullable
              as String,
      externalData: null == externalData
          ? _value.externalData
          : externalData // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$UserInfoInputImpl implements _UserInfoInput {
  const _$UserInfoInputImpl(
      {required this.username, required this.externalData});

  @override
  final String username;
  @override
  final int externalData;

  @override
  String toString() {
    return 'UserInfoInput(username: $username, externalData: $externalData)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UserInfoInputImpl &&
            (identical(other.username, username) ||
                other.username == username) &&
            (identical(other.externalData, externalData) ||
                other.externalData == externalData));
  }

  @override
  int get hashCode => Object.hash(runtimeType, username, externalData);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$UserInfoInputImplCopyWith<_$UserInfoInputImpl> get copyWith =>
      __$$UserInfoInputImplCopyWithImpl<_$UserInfoInputImpl>(this, _$identity);
}

abstract class _UserInfoInput implements UserInfoInput {
  const factory _UserInfoInput(
      {required final String username,
      required final int externalData}) = _$UserInfoInputImpl;

  @override
  String get username;
  @override
  int get externalData;
  @override
  @JsonKey(ignore: true)
  _$$UserInfoInputImplCopyWith<_$UserInfoInputImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$UserInfoOutput {}

/// @nodoc
abstract class $UserInfoOutputCopyWith<$Res> {
  factory $UserInfoOutputCopyWith(
          UserInfoOutput value, $Res Function(UserInfoOutput) then) =
      _$UserInfoOutputCopyWithImpl<$Res, UserInfoOutput>;
}

/// @nodoc
class _$UserInfoOutputCopyWithImpl<$Res, $Val extends UserInfoOutput>
    implements $UserInfoOutputCopyWith<$Res> {
  _$UserInfoOutputCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$UserInfoOutputImplCopyWith<$Res> {
  factory _$$UserInfoOutputImplCopyWith(_$UserInfoOutputImpl value,
          $Res Function(_$UserInfoOutputImpl) then) =
      __$$UserInfoOutputImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$UserInfoOutputImplCopyWithImpl<$Res>
    extends _$UserInfoOutputCopyWithImpl<$Res, _$UserInfoOutputImpl>
    implements _$$UserInfoOutputImplCopyWith<$Res> {
  __$$UserInfoOutputImplCopyWithImpl(
      _$UserInfoOutputImpl _value, $Res Function(_$UserInfoOutputImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$UserInfoOutputImpl extends _UserInfoOutput {
  const _$UserInfoOutputImpl() : super._();

  @override
  String toString() {
    return 'UserInfoOutput()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$UserInfoOutputImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;
}

abstract class _UserInfoOutput extends UserInfoOutput {
  const factory _UserInfoOutput() = _$UserInfoOutputImpl;
  const _UserInfoOutput._() : super._();
}
