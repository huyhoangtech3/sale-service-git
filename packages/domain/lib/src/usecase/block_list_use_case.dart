import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:shared/shared.dart';

import 'package:domain/domain.dart';

part 'block_list_use_case.freezed.dart';

@Injectable()
class BlockListUseCase extends BaseFutureUseCase<BlockListInput, BlockListOutput> {
  const BlockListUseCase(this._repository);

  final Repository _repository;

  @protected
  @override
  Future<BlockListOutput> buildUseCase(BlockListInput input) async {
    await _repository.getBlocks(page: input.page, size: input.size);
    return const BlockListOutput();
  }
}

@freezed
class BlockListInput extends BaseInput with _$BlockListInput {
  const factory BlockListInput({
    required int page,
    required int size,
  }) = _BlockListInput;
}

@freezed
class BlockListOutput extends BaseOutput with _$BlockListOutput {
  const BlockListOutput._();

  const factory BlockListOutput() = _BlockListOutput;
}
