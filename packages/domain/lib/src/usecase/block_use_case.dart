import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:shared/shared.dart';

import 'package:domain/domain.dart';

part 'block_use_case.freezed.dart';

@Injectable()
class BlockUseCase extends BaseFutureUseCase<BlockInput, BlockOutput> {
  const BlockUseCase(this._repository);

  final Repository _repository;

  @protected
  @override
  Future<BlockOutput> buildUseCase(BlockInput input) async {
    await _repository.findBlockByRM();
    return const BlockOutput();
  }
}

@freezed
class BlockInput extends BaseInput with _$BlockInput {
  const factory BlockInput() = _BlockInput;
}

@freezed
class BlockOutput extends BaseOutput with _$BlockOutput {
  const BlockOutput._();

  const factory BlockOutput() = _BlockOutput;
}
