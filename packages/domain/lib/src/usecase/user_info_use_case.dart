import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:shared/shared.dart';

import 'package:domain/domain.dart';

part 'user_info_use_case.freezed.dart';

@Injectable()
class UserInfoUseCase extends BaseFutureUseCase<UserInfoInput, UserInfoOutput> {
  const UserInfoUseCase(this._repository);

  final Repository _repository;

  @protected
  @override
  Future<UserInfoOutput> buildUseCase(UserInfoInput input) async {
    await _repository.getUserInfo(username: input.username, externalData: input.externalData);
    return const UserInfoOutput();
  }
}

@freezed
class UserInfoInput extends BaseInput with _$UserInfoInput {
  const factory UserInfoInput({
    required String username,
    required int externalData,
  }) = _UserInfoInput;
}

@freezed
class UserInfoOutput extends BaseOutput with _$UserInfoOutput {
  const UserInfoOutput._();

  const factory UserInfoOutput() = _UserInfoOutput;
}
