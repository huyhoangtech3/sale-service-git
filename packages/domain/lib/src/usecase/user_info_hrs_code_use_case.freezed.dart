// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'user_info_hrs_code_use_case.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$UserInfoHrsCodeInput {}

/// @nodoc
abstract class $UserInfoHrsCodeInputCopyWith<$Res> {
  factory $UserInfoHrsCodeInputCopyWith(UserInfoHrsCodeInput value,
          $Res Function(UserInfoHrsCodeInput) then) =
      _$UserInfoHrsCodeInputCopyWithImpl<$Res, UserInfoHrsCodeInput>;
}

/// @nodoc
class _$UserInfoHrsCodeInputCopyWithImpl<$Res,
        $Val extends UserInfoHrsCodeInput>
    implements $UserInfoHrsCodeInputCopyWith<$Res> {
  _$UserInfoHrsCodeInputCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$UserInfoHrsCodeInputImplCopyWith<$Res> {
  factory _$$UserInfoHrsCodeInputImplCopyWith(_$UserInfoHrsCodeInputImpl value,
          $Res Function(_$UserInfoHrsCodeInputImpl) then) =
      __$$UserInfoHrsCodeInputImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$UserInfoHrsCodeInputImplCopyWithImpl<$Res>
    extends _$UserInfoHrsCodeInputCopyWithImpl<$Res, _$UserInfoHrsCodeInputImpl>
    implements _$$UserInfoHrsCodeInputImplCopyWith<$Res> {
  __$$UserInfoHrsCodeInputImplCopyWithImpl(_$UserInfoHrsCodeInputImpl _value,
      $Res Function(_$UserInfoHrsCodeInputImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$UserInfoHrsCodeInputImpl implements _UserInfoHrsCodeInput {
  const _$UserInfoHrsCodeInputImpl();

  @override
  String toString() {
    return 'UserInfoHrsCodeInput()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UserInfoHrsCodeInputImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;
}

abstract class _UserInfoHrsCodeInput implements UserInfoHrsCodeInput {
  const factory _UserInfoHrsCodeInput() = _$UserInfoHrsCodeInputImpl;
}

/// @nodoc
mixin _$UserInfoHrsCodeOutput {}

/// @nodoc
abstract class $UserInfoHrsCodeOutputCopyWith<$Res> {
  factory $UserInfoHrsCodeOutputCopyWith(UserInfoHrsCodeOutput value,
          $Res Function(UserInfoHrsCodeOutput) then) =
      _$UserInfoHrsCodeOutputCopyWithImpl<$Res, UserInfoHrsCodeOutput>;
}

/// @nodoc
class _$UserInfoHrsCodeOutputCopyWithImpl<$Res,
        $Val extends UserInfoHrsCodeOutput>
    implements $UserInfoHrsCodeOutputCopyWith<$Res> {
  _$UserInfoHrsCodeOutputCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$UserInfoHrsCodeOutputImplCopyWith<$Res> {
  factory _$$UserInfoHrsCodeOutputImplCopyWith(
          _$UserInfoHrsCodeOutputImpl value,
          $Res Function(_$UserInfoHrsCodeOutputImpl) then) =
      __$$UserInfoHrsCodeOutputImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$UserInfoHrsCodeOutputImplCopyWithImpl<$Res>
    extends _$UserInfoHrsCodeOutputCopyWithImpl<$Res,
        _$UserInfoHrsCodeOutputImpl>
    implements _$$UserInfoHrsCodeOutputImplCopyWith<$Res> {
  __$$UserInfoHrsCodeOutputImplCopyWithImpl(_$UserInfoHrsCodeOutputImpl _value,
      $Res Function(_$UserInfoHrsCodeOutputImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$UserInfoHrsCodeOutputImpl extends _UserInfoHrsCodeOutput {
  const _$UserInfoHrsCodeOutputImpl() : super._();

  @override
  String toString() {
    return 'UserInfoHrsCodeOutput()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UserInfoHrsCodeOutputImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;
}

abstract class _UserInfoHrsCodeOutput extends UserInfoHrsCodeOutput {
  const factory _UserInfoHrsCodeOutput() = _$UserInfoHrsCodeOutputImpl;
  const _UserInfoHrsCodeOutput._() : super._();
}
