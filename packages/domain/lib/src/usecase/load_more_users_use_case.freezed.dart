// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'load_more_users_use_case.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$LoadMoreUsersInput {}

/// @nodoc
abstract class $LoadMoreUsersInputCopyWith<$Res> {
  factory $LoadMoreUsersInputCopyWith(
          LoadMoreUsersInput value, $Res Function(LoadMoreUsersInput) then) =
      _$LoadMoreUsersInputCopyWithImpl<$Res, LoadMoreUsersInput>;
}

/// @nodoc
class _$LoadMoreUsersInputCopyWithImpl<$Res, $Val extends LoadMoreUsersInput>
    implements $LoadMoreUsersInputCopyWith<$Res> {
  _$LoadMoreUsersInputCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$LoadMoreUsersInputImplCopyWith<$Res> {
  factory _$$LoadMoreUsersInputImplCopyWith(_$LoadMoreUsersInputImpl value,
          $Res Function(_$LoadMoreUsersInputImpl) then) =
      __$$LoadMoreUsersInputImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$LoadMoreUsersInputImplCopyWithImpl<$Res>
    extends _$LoadMoreUsersInputCopyWithImpl<$Res, _$LoadMoreUsersInputImpl>
    implements _$$LoadMoreUsersInputImplCopyWith<$Res> {
  __$$LoadMoreUsersInputImplCopyWithImpl(_$LoadMoreUsersInputImpl _value,
      $Res Function(_$LoadMoreUsersInputImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$LoadMoreUsersInputImpl implements _LoadMoreUsersInput {
  const _$LoadMoreUsersInputImpl();

  @override
  String toString() {
    return 'LoadMoreUsersInput()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$LoadMoreUsersInputImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;
}

abstract class _LoadMoreUsersInput implements LoadMoreUsersInput {
  const factory _LoadMoreUsersInput() = _$LoadMoreUsersInputImpl;
}
