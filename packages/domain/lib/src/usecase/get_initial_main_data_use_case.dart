import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

import 'package:domain/domain.dart';
import 'package:shared/shared.dart';

part 'get_initial_main_data_use_case.freezed.dart';

@Injectable()
class GetInitialMainDataUseCase extends BaseSyncUseCase<GetInitialMainDataInput, GetInitialMainDataOutput> {
  const GetInitialMainDataUseCase(this._repository);

  final Repository _repository;

  @protected
  @override
  GetInitialMainDataOutput buildUseCase(GetInitialMainDataInput input) {
    return GetInitialMainDataOutput(
      id: _repository.getUserInfoPreference().id,
      fullName: _repository.getUserInfoPreference().fullName,
      userName: _repository.getUserInfoPreference().username,
      gender: _repository.getUserInfoPreference().gender,
    );
  }
}

@freezed
class GetInitialMainDataInput extends BaseInput with _$GetInitialMainDataInput {
  const factory GetInitialMainDataInput() = _GetInitialMainDataInput;
}

@freezed
class GetInitialMainDataOutput extends BaseOutput with _$GetInitialMainDataOutput {
  const GetInitialMainDataOutput._();

  const factory GetInitialMainDataOutput({
    @Default('') String id,
    @Default('') String fullName,
    @Default('') String userName,
    @Default('') String gender,
  }) = _GetInitialMainDataOutput;
}
