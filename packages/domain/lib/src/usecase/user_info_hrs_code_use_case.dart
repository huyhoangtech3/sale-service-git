import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:shared/shared.dart';

import 'package:domain/domain.dart';

part 'user_info_hrs_code_use_case.freezed.dart';

@Injectable()
class UserInfoHrsCodeUseCase extends BaseFutureUseCase<UserInfoHrsCodeInput, UserInfoHrsCodeOutput> {
  const UserInfoHrsCodeUseCase(this._repository);

  final Repository _repository;

  @protected
  @override
  Future<UserInfoHrsCodeOutput> buildUseCase(UserInfoHrsCodeInput input) async {
    final hrsCode = _repository.getUserInfoPreference().hrsCode;
    await _repository.getDetailUserInfoByHrsCode(hrsCode);
    return const UserInfoHrsCodeOutput();
  }
}

@freezed
class UserInfoHrsCodeInput extends BaseInput with _$UserInfoHrsCodeInput {
  const factory UserInfoHrsCodeInput() = _UserInfoHrsCodeInput;
}

@freezed
class UserInfoHrsCodeOutput extends BaseOutput with _$UserInfoHrsCodeOutput {
  const UserInfoHrsCodeOutput._();

  const factory UserInfoHrsCodeOutput() = _UserInfoHrsCodeOutput;
}
