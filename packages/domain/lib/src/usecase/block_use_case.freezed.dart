// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'block_use_case.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$BlockInput {}

/// @nodoc
abstract class $BlockInputCopyWith<$Res> {
  factory $BlockInputCopyWith(
          BlockInput value, $Res Function(BlockInput) then) =
      _$BlockInputCopyWithImpl<$Res, BlockInput>;
}

/// @nodoc
class _$BlockInputCopyWithImpl<$Res, $Val extends BlockInput>
    implements $BlockInputCopyWith<$Res> {
  _$BlockInputCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$BlockInputImplCopyWith<$Res> {
  factory _$$BlockInputImplCopyWith(
          _$BlockInputImpl value, $Res Function(_$BlockInputImpl) then) =
      __$$BlockInputImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$BlockInputImplCopyWithImpl<$Res>
    extends _$BlockInputCopyWithImpl<$Res, _$BlockInputImpl>
    implements _$$BlockInputImplCopyWith<$Res> {
  __$$BlockInputImplCopyWithImpl(
      _$BlockInputImpl _value, $Res Function(_$BlockInputImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$BlockInputImpl implements _BlockInput {
  const _$BlockInputImpl();

  @override
  String toString() {
    return 'BlockInput()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$BlockInputImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;
}

abstract class _BlockInput implements BlockInput {
  const factory _BlockInput() = _$BlockInputImpl;
}

/// @nodoc
mixin _$BlockOutput {}

/// @nodoc
abstract class $BlockOutputCopyWith<$Res> {
  factory $BlockOutputCopyWith(
          BlockOutput value, $Res Function(BlockOutput) then) =
      _$BlockOutputCopyWithImpl<$Res, BlockOutput>;
}

/// @nodoc
class _$BlockOutputCopyWithImpl<$Res, $Val extends BlockOutput>
    implements $BlockOutputCopyWith<$Res> {
  _$BlockOutputCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$BlockOutputImplCopyWith<$Res> {
  factory _$$BlockOutputImplCopyWith(
          _$BlockOutputImpl value, $Res Function(_$BlockOutputImpl) then) =
      __$$BlockOutputImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$BlockOutputImplCopyWithImpl<$Res>
    extends _$BlockOutputCopyWithImpl<$Res, _$BlockOutputImpl>
    implements _$$BlockOutputImplCopyWith<$Res> {
  __$$BlockOutputImplCopyWithImpl(
      _$BlockOutputImpl _value, $Res Function(_$BlockOutputImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$BlockOutputImpl extends _BlockOutput {
  const _$BlockOutputImpl() : super._();

  @override
  String toString() {
    return 'BlockOutput()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$BlockOutputImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;
}

abstract class _BlockOutput extends BlockOutput {
  const factory _BlockOutput() = _$BlockOutputImpl;
  const _BlockOutput._() : super._();
}
