import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:shared/shared.dart';

import 'package:domain/domain.dart';

part 'branch_use_case.freezed.dart';

@Injectable()
class BranchUseCase extends BaseFutureUseCase<BranchInput, BranchOutput> {
  const BranchUseCase(this._repository);

  final Repository _repository;

  @protected
  @override
  Future<BranchOutput> buildUseCase(BranchInput input) async {
    final rsId = _repository.getUserInfoPreference().id;
    await _repository.getBranchByUser(rsId);
    return const BranchOutput();
  }
}

@freezed
class BranchInput extends BaseInput with _$BranchInput {
  const factory BranchInput() = _BranchInput;
}

@freezed
class BranchOutput extends BaseOutput with _$BranchOutput {
  const BranchOutput._();

  const factory BranchOutput() = _BranchOutput;
}
