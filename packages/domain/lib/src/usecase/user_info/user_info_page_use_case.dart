import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

import 'package:domain/domain.dart';
import 'package:shared/shared.dart';

part 'user_info_page_use_case.freezed.dart';

@Injectable()
class UserInfoPageUseCase extends BaseSyncUseCase<UserInfoPageDataInput, UserInfoPageDataOutput> {
  const UserInfoPageUseCase(this._repository);

  final Repository _repository;

  @protected
  @override
  UserInfoPageDataOutput buildUseCase(UserInfoPageDataInput input) {
    return UserInfoPageDataOutput(
      gender: _repository.getUserInfoPreference().gender,
      fullName: _repository.getUserInfoPreference().fullName,
      hrsCode: _repository.getUserInfoPreference().hrsCode,
      code: _repository.getUserInfoPreference().code,
      branch: _repository.getUserInfoPreference().branch,
      branchName: _repository.getUserInfoPreference().branchName,
    );
  }
}

@freezed
class UserInfoPageDataInput extends BaseInput with _$UserInfoPageDataInput {
  const factory UserInfoPageDataInput() = _UserInfoPageDataInput;
}

@freezed
class UserInfoPageDataOutput extends BaseOutput with _$UserInfoPageDataOutput {
  const UserInfoPageDataOutput._();

  const factory UserInfoPageDataOutput({
    @Default('') String gender,
    @Default('') String fullName,
    @Default('') String hrsCode,
    @Default('') String code,
    @Default('') String branch,
    @Default('') String branchName,
  }) = _UserInfoPageDataOutput;
}
