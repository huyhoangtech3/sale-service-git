// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'user_info_page_use_case.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$UserInfoPageDataInput {}

/// @nodoc
abstract class $UserInfoPageDataInputCopyWith<$Res> {
  factory $UserInfoPageDataInputCopyWith(UserInfoPageDataInput value,
          $Res Function(UserInfoPageDataInput) then) =
      _$UserInfoPageDataInputCopyWithImpl<$Res, UserInfoPageDataInput>;
}

/// @nodoc
class _$UserInfoPageDataInputCopyWithImpl<$Res,
        $Val extends UserInfoPageDataInput>
    implements $UserInfoPageDataInputCopyWith<$Res> {
  _$UserInfoPageDataInputCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$UserInfoPageDataInputImplCopyWith<$Res> {
  factory _$$UserInfoPageDataInputImplCopyWith(
          _$UserInfoPageDataInputImpl value,
          $Res Function(_$UserInfoPageDataInputImpl) then) =
      __$$UserInfoPageDataInputImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$UserInfoPageDataInputImplCopyWithImpl<$Res>
    extends _$UserInfoPageDataInputCopyWithImpl<$Res,
        _$UserInfoPageDataInputImpl>
    implements _$$UserInfoPageDataInputImplCopyWith<$Res> {
  __$$UserInfoPageDataInputImplCopyWithImpl(_$UserInfoPageDataInputImpl _value,
      $Res Function(_$UserInfoPageDataInputImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$UserInfoPageDataInputImpl implements _UserInfoPageDataInput {
  const _$UserInfoPageDataInputImpl();

  @override
  String toString() {
    return 'UserInfoPageDataInput()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UserInfoPageDataInputImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;
}

abstract class _UserInfoPageDataInput implements UserInfoPageDataInput {
  const factory _UserInfoPageDataInput() = _$UserInfoPageDataInputImpl;
}

/// @nodoc
mixin _$UserInfoPageDataOutput {
  String get gender => throw _privateConstructorUsedError;
  String get fullName => throw _privateConstructorUsedError;
  String get hrsCode => throw _privateConstructorUsedError;
  String get code => throw _privateConstructorUsedError;
  String get branch => throw _privateConstructorUsedError;
  String get branchName => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $UserInfoPageDataOutputCopyWith<UserInfoPageDataOutput> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserInfoPageDataOutputCopyWith<$Res> {
  factory $UserInfoPageDataOutputCopyWith(UserInfoPageDataOutput value,
          $Res Function(UserInfoPageDataOutput) then) =
      _$UserInfoPageDataOutputCopyWithImpl<$Res, UserInfoPageDataOutput>;
  @useResult
  $Res call(
      {String gender,
      String fullName,
      String hrsCode,
      String code,
      String branch,
      String branchName});
}

/// @nodoc
class _$UserInfoPageDataOutputCopyWithImpl<$Res,
        $Val extends UserInfoPageDataOutput>
    implements $UserInfoPageDataOutputCopyWith<$Res> {
  _$UserInfoPageDataOutputCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? gender = null,
    Object? fullName = null,
    Object? hrsCode = null,
    Object? code = null,
    Object? branch = null,
    Object? branchName = null,
  }) {
    return _then(_value.copyWith(
      gender: null == gender
          ? _value.gender
          : gender // ignore: cast_nullable_to_non_nullable
              as String,
      fullName: null == fullName
          ? _value.fullName
          : fullName // ignore: cast_nullable_to_non_nullable
              as String,
      hrsCode: null == hrsCode
          ? _value.hrsCode
          : hrsCode // ignore: cast_nullable_to_non_nullable
              as String,
      code: null == code
          ? _value.code
          : code // ignore: cast_nullable_to_non_nullable
              as String,
      branch: null == branch
          ? _value.branch
          : branch // ignore: cast_nullable_to_non_nullable
              as String,
      branchName: null == branchName
          ? _value.branchName
          : branchName // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$UserInfoPageDataOutputImplCopyWith<$Res>
    implements $UserInfoPageDataOutputCopyWith<$Res> {
  factory _$$UserInfoPageDataOutputImplCopyWith(
          _$UserInfoPageDataOutputImpl value,
          $Res Function(_$UserInfoPageDataOutputImpl) then) =
      __$$UserInfoPageDataOutputImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String gender,
      String fullName,
      String hrsCode,
      String code,
      String branch,
      String branchName});
}

/// @nodoc
class __$$UserInfoPageDataOutputImplCopyWithImpl<$Res>
    extends _$UserInfoPageDataOutputCopyWithImpl<$Res,
        _$UserInfoPageDataOutputImpl>
    implements _$$UserInfoPageDataOutputImplCopyWith<$Res> {
  __$$UserInfoPageDataOutputImplCopyWithImpl(
      _$UserInfoPageDataOutputImpl _value,
      $Res Function(_$UserInfoPageDataOutputImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? gender = null,
    Object? fullName = null,
    Object? hrsCode = null,
    Object? code = null,
    Object? branch = null,
    Object? branchName = null,
  }) {
    return _then(_$UserInfoPageDataOutputImpl(
      gender: null == gender
          ? _value.gender
          : gender // ignore: cast_nullable_to_non_nullable
              as String,
      fullName: null == fullName
          ? _value.fullName
          : fullName // ignore: cast_nullable_to_non_nullable
              as String,
      hrsCode: null == hrsCode
          ? _value.hrsCode
          : hrsCode // ignore: cast_nullable_to_non_nullable
              as String,
      code: null == code
          ? _value.code
          : code // ignore: cast_nullable_to_non_nullable
              as String,
      branch: null == branch
          ? _value.branch
          : branch // ignore: cast_nullable_to_non_nullable
              as String,
      branchName: null == branchName
          ? _value.branchName
          : branchName // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$UserInfoPageDataOutputImpl extends _UserInfoPageDataOutput {
  const _$UserInfoPageDataOutputImpl(
      {this.gender = '',
      this.fullName = '',
      this.hrsCode = '',
      this.code = '',
      this.branch = '',
      this.branchName = ''})
      : super._();

  @override
  @JsonKey()
  final String gender;
  @override
  @JsonKey()
  final String fullName;
  @override
  @JsonKey()
  final String hrsCode;
  @override
  @JsonKey()
  final String code;
  @override
  @JsonKey()
  final String branch;
  @override
  @JsonKey()
  final String branchName;

  @override
  String toString() {
    return 'UserInfoPageDataOutput(gender: $gender, fullName: $fullName, hrsCode: $hrsCode, code: $code, branch: $branch, branchName: $branchName)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UserInfoPageDataOutputImpl &&
            (identical(other.gender, gender) || other.gender == gender) &&
            (identical(other.fullName, fullName) ||
                other.fullName == fullName) &&
            (identical(other.hrsCode, hrsCode) || other.hrsCode == hrsCode) &&
            (identical(other.code, code) || other.code == code) &&
            (identical(other.branch, branch) || other.branch == branch) &&
            (identical(other.branchName, branchName) ||
                other.branchName == branchName));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, gender, fullName, hrsCode, code, branch, branchName);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$UserInfoPageDataOutputImplCopyWith<_$UserInfoPageDataOutputImpl>
      get copyWith => __$$UserInfoPageDataOutputImplCopyWithImpl<
          _$UserInfoPageDataOutputImpl>(this, _$identity);
}

abstract class _UserInfoPageDataOutput extends UserInfoPageDataOutput {
  const factory _UserInfoPageDataOutput(
      {final String gender,
      final String fullName,
      final String hrsCode,
      final String code,
      final String branch,
      final String branchName}) = _$UserInfoPageDataOutputImpl;
  const _UserInfoPageDataOutput._() : super._();

  @override
  String get gender;
  @override
  String get fullName;
  @override
  String get hrsCode;
  @override
  String get code;
  @override
  String get branch;
  @override
  String get branchName;
  @override
  @JsonKey(ignore: true)
  _$$UserInfoPageDataOutputImplCopyWith<_$UserInfoPageDataOutputImpl>
      get copyWith => throw _privateConstructorUsedError;
}
