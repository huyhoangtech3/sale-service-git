// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'create_todo_use_case.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$CreateTodoInput {
  String get todoTitle => throw _privateConstructorUsedError;
  String get fromDate => throw _privateConstructorUsedError;
  String get toDate => throw _privateConstructorUsedError;
  int get priority => throw _privateConstructorUsedError;
  String get todoContent => throw _privateConstructorUsedError;
  String get todoLocation => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CreateTodoInputCopyWith<CreateTodoInput> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CreateTodoInputCopyWith<$Res> {
  factory $CreateTodoInputCopyWith(
          CreateTodoInput value, $Res Function(CreateTodoInput) then) =
      _$CreateTodoInputCopyWithImpl<$Res, CreateTodoInput>;
  @useResult
  $Res call(
      {String todoTitle,
      String fromDate,
      String toDate,
      int priority,
      String todoContent,
      String todoLocation});
}

/// @nodoc
class _$CreateTodoInputCopyWithImpl<$Res, $Val extends CreateTodoInput>
    implements $CreateTodoInputCopyWith<$Res> {
  _$CreateTodoInputCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? todoTitle = null,
    Object? fromDate = null,
    Object? toDate = null,
    Object? priority = null,
    Object? todoContent = null,
    Object? todoLocation = null,
  }) {
    return _then(_value.copyWith(
      todoTitle: null == todoTitle
          ? _value.todoTitle
          : todoTitle // ignore: cast_nullable_to_non_nullable
              as String,
      fromDate: null == fromDate
          ? _value.fromDate
          : fromDate // ignore: cast_nullable_to_non_nullable
              as String,
      toDate: null == toDate
          ? _value.toDate
          : toDate // ignore: cast_nullable_to_non_nullable
              as String,
      priority: null == priority
          ? _value.priority
          : priority // ignore: cast_nullable_to_non_nullable
              as int,
      todoContent: null == todoContent
          ? _value.todoContent
          : todoContent // ignore: cast_nullable_to_non_nullable
              as String,
      todoLocation: null == todoLocation
          ? _value.todoLocation
          : todoLocation // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$CreateTodoInputImplCopyWith<$Res>
    implements $CreateTodoInputCopyWith<$Res> {
  factory _$$CreateTodoInputImplCopyWith(_$CreateTodoInputImpl value,
          $Res Function(_$CreateTodoInputImpl) then) =
      __$$CreateTodoInputImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String todoTitle,
      String fromDate,
      String toDate,
      int priority,
      String todoContent,
      String todoLocation});
}

/// @nodoc
class __$$CreateTodoInputImplCopyWithImpl<$Res>
    extends _$CreateTodoInputCopyWithImpl<$Res, _$CreateTodoInputImpl>
    implements _$$CreateTodoInputImplCopyWith<$Res> {
  __$$CreateTodoInputImplCopyWithImpl(
      _$CreateTodoInputImpl _value, $Res Function(_$CreateTodoInputImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? todoTitle = null,
    Object? fromDate = null,
    Object? toDate = null,
    Object? priority = null,
    Object? todoContent = null,
    Object? todoLocation = null,
  }) {
    return _then(_$CreateTodoInputImpl(
      todoTitle: null == todoTitle
          ? _value.todoTitle
          : todoTitle // ignore: cast_nullable_to_non_nullable
              as String,
      fromDate: null == fromDate
          ? _value.fromDate
          : fromDate // ignore: cast_nullable_to_non_nullable
              as String,
      toDate: null == toDate
          ? _value.toDate
          : toDate // ignore: cast_nullable_to_non_nullable
              as String,
      priority: null == priority
          ? _value.priority
          : priority // ignore: cast_nullable_to_non_nullable
              as int,
      todoContent: null == todoContent
          ? _value.todoContent
          : todoContent // ignore: cast_nullable_to_non_nullable
              as String,
      todoLocation: null == todoLocation
          ? _value.todoLocation
          : todoLocation // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$CreateTodoInputImpl implements _CreateTodoInput {
  const _$CreateTodoInputImpl(
      {required this.todoTitle,
      required this.fromDate,
      required this.toDate,
      required this.priority,
      required this.todoContent,
      required this.todoLocation});

  @override
  final String todoTitle;
  @override
  final String fromDate;
  @override
  final String toDate;
  @override
  final int priority;
  @override
  final String todoContent;
  @override
  final String todoLocation;

  @override
  String toString() {
    return 'CreateTodoInput(todoTitle: $todoTitle, fromDate: $fromDate, toDate: $toDate, priority: $priority, todoContent: $todoContent, todoLocation: $todoLocation)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CreateTodoInputImpl &&
            (identical(other.todoTitle, todoTitle) ||
                other.todoTitle == todoTitle) &&
            (identical(other.fromDate, fromDate) ||
                other.fromDate == fromDate) &&
            (identical(other.toDate, toDate) || other.toDate == toDate) &&
            (identical(other.priority, priority) ||
                other.priority == priority) &&
            (identical(other.todoContent, todoContent) ||
                other.todoContent == todoContent) &&
            (identical(other.todoLocation, todoLocation) ||
                other.todoLocation == todoLocation));
  }

  @override
  int get hashCode => Object.hash(runtimeType, todoTitle, fromDate, toDate,
      priority, todoContent, todoLocation);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CreateTodoInputImplCopyWith<_$CreateTodoInputImpl> get copyWith =>
      __$$CreateTodoInputImplCopyWithImpl<_$CreateTodoInputImpl>(
          this, _$identity);
}

abstract class _CreateTodoInput implements CreateTodoInput {
  const factory _CreateTodoInput(
      {required final String todoTitle,
      required final String fromDate,
      required final String toDate,
      required final int priority,
      required final String todoContent,
      required final String todoLocation}) = _$CreateTodoInputImpl;

  @override
  String get todoTitle;
  @override
  String get fromDate;
  @override
  String get toDate;
  @override
  int get priority;
  @override
  String get todoContent;
  @override
  String get todoLocation;
  @override
  @JsonKey(ignore: true)
  _$$CreateTodoInputImplCopyWith<_$CreateTodoInputImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$CreateTodoOutput {
  String get titleTodo => throw _privateConstructorUsedError;
  String get contentTodo => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CreateTodoOutputCopyWith<CreateTodoOutput> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CreateTodoOutputCopyWith<$Res> {
  factory $CreateTodoOutputCopyWith(
          CreateTodoOutput value, $Res Function(CreateTodoOutput) then) =
      _$CreateTodoOutputCopyWithImpl<$Res, CreateTodoOutput>;
  @useResult
  $Res call({String titleTodo, String contentTodo});
}

/// @nodoc
class _$CreateTodoOutputCopyWithImpl<$Res, $Val extends CreateTodoOutput>
    implements $CreateTodoOutputCopyWith<$Res> {
  _$CreateTodoOutputCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? titleTodo = null,
    Object? contentTodo = null,
  }) {
    return _then(_value.copyWith(
      titleTodo: null == titleTodo
          ? _value.titleTodo
          : titleTodo // ignore: cast_nullable_to_non_nullable
              as String,
      contentTodo: null == contentTodo
          ? _value.contentTodo
          : contentTodo // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$CreateTodoOutputImplCopyWith<$Res>
    implements $CreateTodoOutputCopyWith<$Res> {
  factory _$$CreateTodoOutputImplCopyWith(_$CreateTodoOutputImpl value,
          $Res Function(_$CreateTodoOutputImpl) then) =
      __$$CreateTodoOutputImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String titleTodo, String contentTodo});
}

/// @nodoc
class __$$CreateTodoOutputImplCopyWithImpl<$Res>
    extends _$CreateTodoOutputCopyWithImpl<$Res, _$CreateTodoOutputImpl>
    implements _$$CreateTodoOutputImplCopyWith<$Res> {
  __$$CreateTodoOutputImplCopyWithImpl(_$CreateTodoOutputImpl _value,
      $Res Function(_$CreateTodoOutputImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? titleTodo = null,
    Object? contentTodo = null,
  }) {
    return _then(_$CreateTodoOutputImpl(
      titleTodo: null == titleTodo
          ? _value.titleTodo
          : titleTodo // ignore: cast_nullable_to_non_nullable
              as String,
      contentTodo: null == contentTodo
          ? _value.contentTodo
          : contentTodo // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$CreateTodoOutputImpl extends _CreateTodoOutput {
  const _$CreateTodoOutputImpl({this.titleTodo = '', this.contentTodo = ''})
      : super._();

  @override
  @JsonKey()
  final String titleTodo;
  @override
  @JsonKey()
  final String contentTodo;

  @override
  String toString() {
    return 'CreateTodoOutput(titleTodo: $titleTodo, contentTodo: $contentTodo)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CreateTodoOutputImpl &&
            (identical(other.titleTodo, titleTodo) ||
                other.titleTodo == titleTodo) &&
            (identical(other.contentTodo, contentTodo) ||
                other.contentTodo == contentTodo));
  }

  @override
  int get hashCode => Object.hash(runtimeType, titleTodo, contentTodo);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CreateTodoOutputImplCopyWith<_$CreateTodoOutputImpl> get copyWith =>
      __$$CreateTodoOutputImplCopyWithImpl<_$CreateTodoOutputImpl>(
          this, _$identity);
}

abstract class _CreateTodoOutput extends CreateTodoOutput {
  const factory _CreateTodoOutput(
      {final String titleTodo,
      final String contentTodo}) = _$CreateTodoOutputImpl;
  const _CreateTodoOutput._() : super._();

  @override
  String get titleTodo;
  @override
  String get contentTodo;
  @override
  @JsonKey(ignore: true)
  _$$CreateTodoOutputImplCopyWith<_$CreateTodoOutputImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
