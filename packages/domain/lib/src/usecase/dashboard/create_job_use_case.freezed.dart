// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'create_job_use_case.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$CreateJobInput {
  String get jobTitle => throw _privateConstructorUsedError;
  String get fromDate => throw _privateConstructorUsedError;
  String get toDate => throw _privateConstructorUsedError;
  int get priority => throw _privateConstructorUsedError;
  String get jobContent => throw _privateConstructorUsedError;
  String get jobLocation => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CreateJobInputCopyWith<CreateJobInput> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CreateJobInputCopyWith<$Res> {
  factory $CreateJobInputCopyWith(
          CreateJobInput value, $Res Function(CreateJobInput) then) =
      _$CreateJobInputCopyWithImpl<$Res, CreateJobInput>;
  @useResult
  $Res call(
      {String jobTitle,
      String fromDate,
      String toDate,
      int priority,
      String jobContent,
      String jobLocation});
}

/// @nodoc
class _$CreateJobInputCopyWithImpl<$Res, $Val extends CreateJobInput>
    implements $CreateJobInputCopyWith<$Res> {
  _$CreateJobInputCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? jobTitle = null,
    Object? fromDate = null,
    Object? toDate = null,
    Object? priority = null,
    Object? jobContent = null,
    Object? jobLocation = null,
  }) {
    return _then(_value.copyWith(
      jobTitle: null == jobTitle
          ? _value.jobTitle
          : jobTitle // ignore: cast_nullable_to_non_nullable
              as String,
      fromDate: null == fromDate
          ? _value.fromDate
          : fromDate // ignore: cast_nullable_to_non_nullable
              as String,
      toDate: null == toDate
          ? _value.toDate
          : toDate // ignore: cast_nullable_to_non_nullable
              as String,
      priority: null == priority
          ? _value.priority
          : priority // ignore: cast_nullable_to_non_nullable
              as int,
      jobContent: null == jobContent
          ? _value.jobContent
          : jobContent // ignore: cast_nullable_to_non_nullable
              as String,
      jobLocation: null == jobLocation
          ? _value.jobLocation
          : jobLocation // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$CreateJobInputImplCopyWith<$Res>
    implements $CreateJobInputCopyWith<$Res> {
  factory _$$CreateJobInputImplCopyWith(_$CreateJobInputImpl value,
          $Res Function(_$CreateJobInputImpl) then) =
      __$$CreateJobInputImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String jobTitle,
      String fromDate,
      String toDate,
      int priority,
      String jobContent,
      String jobLocation});
}

/// @nodoc
class __$$CreateJobInputImplCopyWithImpl<$Res>
    extends _$CreateJobInputCopyWithImpl<$Res, _$CreateJobInputImpl>
    implements _$$CreateJobInputImplCopyWith<$Res> {
  __$$CreateJobInputImplCopyWithImpl(
      _$CreateJobInputImpl _value, $Res Function(_$CreateJobInputImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? jobTitle = null,
    Object? fromDate = null,
    Object? toDate = null,
    Object? priority = null,
    Object? jobContent = null,
    Object? jobLocation = null,
  }) {
    return _then(_$CreateJobInputImpl(
      jobTitle: null == jobTitle
          ? _value.jobTitle
          : jobTitle // ignore: cast_nullable_to_non_nullable
              as String,
      fromDate: null == fromDate
          ? _value.fromDate
          : fromDate // ignore: cast_nullable_to_non_nullable
              as String,
      toDate: null == toDate
          ? _value.toDate
          : toDate // ignore: cast_nullable_to_non_nullable
              as String,
      priority: null == priority
          ? _value.priority
          : priority // ignore: cast_nullable_to_non_nullable
              as int,
      jobContent: null == jobContent
          ? _value.jobContent
          : jobContent // ignore: cast_nullable_to_non_nullable
              as String,
      jobLocation: null == jobLocation
          ? _value.jobLocation
          : jobLocation // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$CreateJobInputImpl implements _CreateJobInput {
  const _$CreateJobInputImpl(
      {required this.jobTitle,
      required this.fromDate,
      required this.toDate,
      required this.priority,
      required this.jobContent,
      required this.jobLocation});

  @override
  final String jobTitle;
  @override
  final String fromDate;
  @override
  final String toDate;
  @override
  final int priority;
  @override
  final String jobContent;
  @override
  final String jobLocation;

  @override
  String toString() {
    return 'CreateJobInput(jobTitle: $jobTitle, fromDate: $fromDate, toDate: $toDate, priority: $priority, jobContent: $jobContent, jobLocation: $jobLocation)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CreateJobInputImpl &&
            (identical(other.jobTitle, jobTitle) ||
                other.jobTitle == jobTitle) &&
            (identical(other.fromDate, fromDate) ||
                other.fromDate == fromDate) &&
            (identical(other.toDate, toDate) || other.toDate == toDate) &&
            (identical(other.priority, priority) ||
                other.priority == priority) &&
            (identical(other.jobContent, jobContent) ||
                other.jobContent == jobContent) &&
            (identical(other.jobLocation, jobLocation) ||
                other.jobLocation == jobLocation));
  }

  @override
  int get hashCode => Object.hash(runtimeType, jobTitle, fromDate, toDate,
      priority, jobContent, jobLocation);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CreateJobInputImplCopyWith<_$CreateJobInputImpl> get copyWith =>
      __$$CreateJobInputImplCopyWithImpl<_$CreateJobInputImpl>(
          this, _$identity);
}

abstract class _CreateJobInput implements CreateJobInput {
  const factory _CreateJobInput(
      {required final String jobTitle,
      required final String fromDate,
      required final String toDate,
      required final int priority,
      required final String jobContent,
      required final String jobLocation}) = _$CreateJobInputImpl;

  @override
  String get jobTitle;
  @override
  String get fromDate;
  @override
  String get toDate;
  @override
  int get priority;
  @override
  String get jobContent;
  @override
  String get jobLocation;
  @override
  @JsonKey(ignore: true)
  _$$CreateJobInputImplCopyWith<_$CreateJobInputImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$CreateJobOutput {
  String get titleJob => throw _privateConstructorUsedError;
  String get contentJob => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CreateJobOutputCopyWith<CreateJobOutput> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CreateJobOutputCopyWith<$Res> {
  factory $CreateJobOutputCopyWith(
          CreateJobOutput value, $Res Function(CreateJobOutput) then) =
      _$CreateJobOutputCopyWithImpl<$Res, CreateJobOutput>;
  @useResult
  $Res call({String titleJob, String contentJob});
}

/// @nodoc
class _$CreateJobOutputCopyWithImpl<$Res, $Val extends CreateJobOutput>
    implements $CreateJobOutputCopyWith<$Res> {
  _$CreateJobOutputCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? titleJob = null,
    Object? contentJob = null,
  }) {
    return _then(_value.copyWith(
      titleJob: null == titleJob
          ? _value.titleJob
          : titleJob // ignore: cast_nullable_to_non_nullable
              as String,
      contentJob: null == contentJob
          ? _value.contentJob
          : contentJob // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$CreateJobOutputImplCopyWith<$Res>
    implements $CreateJobOutputCopyWith<$Res> {
  factory _$$CreateJobOutputImplCopyWith(_$CreateJobOutputImpl value,
          $Res Function(_$CreateJobOutputImpl) then) =
      __$$CreateJobOutputImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String titleJob, String contentJob});
}

/// @nodoc
class __$$CreateJobOutputImplCopyWithImpl<$Res>
    extends _$CreateJobOutputCopyWithImpl<$Res, _$CreateJobOutputImpl>
    implements _$$CreateJobOutputImplCopyWith<$Res> {
  __$$CreateJobOutputImplCopyWithImpl(
      _$CreateJobOutputImpl _value, $Res Function(_$CreateJobOutputImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? titleJob = null,
    Object? contentJob = null,
  }) {
    return _then(_$CreateJobOutputImpl(
      titleJob: null == titleJob
          ? _value.titleJob
          : titleJob // ignore: cast_nullable_to_non_nullable
              as String,
      contentJob: null == contentJob
          ? _value.contentJob
          : contentJob // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$CreateJobOutputImpl extends _CreateJobOutput {
  const _$CreateJobOutputImpl({this.titleJob = '', this.contentJob = ''})
      : super._();

  @override
  @JsonKey()
  final String titleJob;
  @override
  @JsonKey()
  final String contentJob;

  @override
  String toString() {
    return 'CreateJobOutput(titleJob: $titleJob, contentJob: $contentJob)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CreateJobOutputImpl &&
            (identical(other.titleJob, titleJob) ||
                other.titleJob == titleJob) &&
            (identical(other.contentJob, contentJob) ||
                other.contentJob == contentJob));
  }

  @override
  int get hashCode => Object.hash(runtimeType, titleJob, contentJob);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CreateJobOutputImplCopyWith<_$CreateJobOutputImpl> get copyWith =>
      __$$CreateJobOutputImplCopyWithImpl<_$CreateJobOutputImpl>(
          this, _$identity);
}

abstract class _CreateJobOutput extends CreateJobOutput {
  const factory _CreateJobOutput(
      {final String titleJob, final String contentJob}) = _$CreateJobOutputImpl;
  const _CreateJobOutput._() : super._();

  @override
  String get titleJob;
  @override
  String get contentJob;
  @override
  @JsonKey(ignore: true)
  _$$CreateJobOutputImplCopyWith<_$CreateJobOutputImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
