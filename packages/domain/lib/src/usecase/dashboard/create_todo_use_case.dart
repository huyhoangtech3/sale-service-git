import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:resources/resources.dart';
import 'package:shared/shared.dart';
import 'package:domain/domain.dart';
part 'create_todo_use_case.freezed.dart';

@Injectable()
class CreateTodoUseCase extends BaseSyncUseCase<CreateTodoInput, CreateTodoOutput> {
  const CreateTodoUseCase(this._repository);

  final Repository _repository;

  @protected
  @override
  CreateTodoOutput buildUseCase(CreateTodoInput input) {
    return CreateTodoOutput(
      titleTodo: 'totalFormatGrossValue',
      contentTodo: 'totalFormatNetWorthValue',
    );
  }
}

@freezed
class CreateTodoInput extends BaseInput with _$CreateTodoInput {
  const factory CreateTodoInput({
    required String todoTitle,
    required String fromDate,
    required String toDate,
    required int priority,
    required String todoContent,
    required String todoLocation,
  }) = _CreateTodoInput;
}

@freezed
class CreateTodoOutput extends BaseOutput with _$CreateTodoOutput {
  const CreateTodoOutput._();

  const factory CreateTodoOutput({
    @Default('') String titleTodo,
    @Default('') String contentTodo,
  }) = _CreateTodoOutput;
}
