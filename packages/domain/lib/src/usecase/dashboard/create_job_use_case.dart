import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:resources/resources.dart';
import 'package:shared/shared.dart';
import 'package:domain/domain.dart';
part 'create_job_use_case.freezed.dart';

@Injectable()
class CreateJobUseCase extends BaseSyncUseCase<CreateJobInput, CreateJobOutput> {
  const CreateJobUseCase(this._repository);

  final Repository _repository;

  @protected
  @override
  CreateJobOutput buildUseCase(CreateJobInput input) {
    return CreateJobOutput(
      titleJob: 'totalFormatGrossValue',
      contentJob: 'totalFormatNetWorthValue',
    );
  }
}

@freezed
class CreateJobInput extends BaseInput with _$CreateJobInput {
  const factory CreateJobInput({
    required String jobTitle,
    required String fromDate,
    required String toDate,
    required int priority,
    required String jobContent,
    required String jobLocation,
  }) = _CreateJobInput;
}

@freezed
class CreateJobOutput extends BaseOutput with _$CreateJobOutput {
  const CreateJobOutput._();

  const factory CreateJobOutput({
    @Default('') String titleJob,
    @Default('') String contentJob,
  }) = _CreateJobOutput;
}
