import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

import 'package:domain/domain.dart';

part 'save_username_use_case.freezed.dart';

@Injectable()
class SaveUserNameUseCase
    extends BaseFutureUseCase<SaveUserNameInput, SaveUserNameOutput> {
  const SaveUserNameUseCase(this._repository);

  final Repository _repository;

  @protected
  @override
  Future<SaveUserNameOutput> buildUseCase(SaveUserNameInput input) async {
    await _repository.saveUserName(input.userName);

    return const SaveUserNameOutput();
  }
}

@freezed
class SaveUserNameInput extends BaseInput with _$SaveUserNameInput {
  const factory SaveUserNameInput({
    required String userName,
  }) = _SaveUserNameUseCase;
}

@freezed
class SaveUserNameOutput extends BaseOutput with _$SaveUserNameOutput {
  const SaveUserNameOutput._();

  const factory SaveUserNameOutput() = _SaveUserNameOutput;
}
