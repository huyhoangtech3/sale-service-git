// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'get_initial_main_data_use_case.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$GetInitialMainDataInput {}

/// @nodoc
abstract class $GetInitialMainDataInputCopyWith<$Res> {
  factory $GetInitialMainDataInputCopyWith(GetInitialMainDataInput value,
          $Res Function(GetInitialMainDataInput) then) =
      _$GetInitialMainDataInputCopyWithImpl<$Res, GetInitialMainDataInput>;
}

/// @nodoc
class _$GetInitialMainDataInputCopyWithImpl<$Res,
        $Val extends GetInitialMainDataInput>
    implements $GetInitialMainDataInputCopyWith<$Res> {
  _$GetInitialMainDataInputCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$GetInitialMainDataInputImplCopyWith<$Res> {
  factory _$$GetInitialMainDataInputImplCopyWith(
          _$GetInitialMainDataInputImpl value,
          $Res Function(_$GetInitialMainDataInputImpl) then) =
      __$$GetInitialMainDataInputImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$GetInitialMainDataInputImplCopyWithImpl<$Res>
    extends _$GetInitialMainDataInputCopyWithImpl<$Res,
        _$GetInitialMainDataInputImpl>
    implements _$$GetInitialMainDataInputImplCopyWith<$Res> {
  __$$GetInitialMainDataInputImplCopyWithImpl(
      _$GetInitialMainDataInputImpl _value,
      $Res Function(_$GetInitialMainDataInputImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$GetInitialMainDataInputImpl implements _GetInitialMainDataInput {
  const _$GetInitialMainDataInputImpl();

  @override
  String toString() {
    return 'GetInitialMainDataInput()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GetInitialMainDataInputImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;
}

abstract class _GetInitialMainDataInput implements GetInitialMainDataInput {
  const factory _GetInitialMainDataInput() = _$GetInitialMainDataInputImpl;
}

/// @nodoc
mixin _$GetInitialMainDataOutput {
  String get id => throw _privateConstructorUsedError;
  String get fullName => throw _privateConstructorUsedError;
  String get userName => throw _privateConstructorUsedError;
  String get gender => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $GetInitialMainDataOutputCopyWith<GetInitialMainDataOutput> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GetInitialMainDataOutputCopyWith<$Res> {
  factory $GetInitialMainDataOutputCopyWith(GetInitialMainDataOutput value,
          $Res Function(GetInitialMainDataOutput) then) =
      _$GetInitialMainDataOutputCopyWithImpl<$Res, GetInitialMainDataOutput>;
  @useResult
  $Res call({String id, String fullName, String userName, String gender});
}

/// @nodoc
class _$GetInitialMainDataOutputCopyWithImpl<$Res,
        $Val extends GetInitialMainDataOutput>
    implements $GetInitialMainDataOutputCopyWith<$Res> {
  _$GetInitialMainDataOutputCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? fullName = null,
    Object? userName = null,
    Object? gender = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      fullName: null == fullName
          ? _value.fullName
          : fullName // ignore: cast_nullable_to_non_nullable
              as String,
      userName: null == userName
          ? _value.userName
          : userName // ignore: cast_nullable_to_non_nullable
              as String,
      gender: null == gender
          ? _value.gender
          : gender // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$GetInitialMainDataOutputImplCopyWith<$Res>
    implements $GetInitialMainDataOutputCopyWith<$Res> {
  factory _$$GetInitialMainDataOutputImplCopyWith(
          _$GetInitialMainDataOutputImpl value,
          $Res Function(_$GetInitialMainDataOutputImpl) then) =
      __$$GetInitialMainDataOutputImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String id, String fullName, String userName, String gender});
}

/// @nodoc
class __$$GetInitialMainDataOutputImplCopyWithImpl<$Res>
    extends _$GetInitialMainDataOutputCopyWithImpl<$Res,
        _$GetInitialMainDataOutputImpl>
    implements _$$GetInitialMainDataOutputImplCopyWith<$Res> {
  __$$GetInitialMainDataOutputImplCopyWithImpl(
      _$GetInitialMainDataOutputImpl _value,
      $Res Function(_$GetInitialMainDataOutputImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? fullName = null,
    Object? userName = null,
    Object? gender = null,
  }) {
    return _then(_$GetInitialMainDataOutputImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      fullName: null == fullName
          ? _value.fullName
          : fullName // ignore: cast_nullable_to_non_nullable
              as String,
      userName: null == userName
          ? _value.userName
          : userName // ignore: cast_nullable_to_non_nullable
              as String,
      gender: null == gender
          ? _value.gender
          : gender // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$GetInitialMainDataOutputImpl extends _GetInitialMainDataOutput {
  const _$GetInitialMainDataOutputImpl(
      {this.id = '', this.fullName = '', this.userName = '', this.gender = ''})
      : super._();

  @override
  @JsonKey()
  final String id;
  @override
  @JsonKey()
  final String fullName;
  @override
  @JsonKey()
  final String userName;
  @override
  @JsonKey()
  final String gender;

  @override
  String toString() {
    return 'GetInitialMainDataOutput(id: $id, fullName: $fullName, userName: $userName, gender: $gender)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GetInitialMainDataOutputImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.fullName, fullName) ||
                other.fullName == fullName) &&
            (identical(other.userName, userName) ||
                other.userName == userName) &&
            (identical(other.gender, gender) || other.gender == gender));
  }

  @override
  int get hashCode => Object.hash(runtimeType, id, fullName, userName, gender);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$GetInitialMainDataOutputImplCopyWith<_$GetInitialMainDataOutputImpl>
      get copyWith => __$$GetInitialMainDataOutputImplCopyWithImpl<
          _$GetInitialMainDataOutputImpl>(this, _$identity);
}

abstract class _GetInitialMainDataOutput extends GetInitialMainDataOutput {
  const factory _GetInitialMainDataOutput(
      {final String id,
      final String fullName,
      final String userName,
      final String gender}) = _$GetInitialMainDataOutputImpl;
  const _GetInitialMainDataOutput._() : super._();

  @override
  String get id;
  @override
  String get fullName;
  @override
  String get userName;
  @override
  String get gender;
  @override
  @JsonKey(ignore: true)
  _$$GetInitialMainDataOutputImplCopyWith<_$GetInitialMainDataOutputImpl>
      get copyWith => throw _privateConstructorUsedError;
}
