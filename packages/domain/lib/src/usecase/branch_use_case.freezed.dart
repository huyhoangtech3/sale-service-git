// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'branch_use_case.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$BranchInput {}

/// @nodoc
abstract class $BranchInputCopyWith<$Res> {
  factory $BranchInputCopyWith(
          BranchInput value, $Res Function(BranchInput) then) =
      _$BranchInputCopyWithImpl<$Res, BranchInput>;
}

/// @nodoc
class _$BranchInputCopyWithImpl<$Res, $Val extends BranchInput>
    implements $BranchInputCopyWith<$Res> {
  _$BranchInputCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$BranchInputImplCopyWith<$Res> {
  factory _$$BranchInputImplCopyWith(
          _$BranchInputImpl value, $Res Function(_$BranchInputImpl) then) =
      __$$BranchInputImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$BranchInputImplCopyWithImpl<$Res>
    extends _$BranchInputCopyWithImpl<$Res, _$BranchInputImpl>
    implements _$$BranchInputImplCopyWith<$Res> {
  __$$BranchInputImplCopyWithImpl(
      _$BranchInputImpl _value, $Res Function(_$BranchInputImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$BranchInputImpl implements _BranchInput {
  const _$BranchInputImpl();

  @override
  String toString() {
    return 'BranchInput()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$BranchInputImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;
}

abstract class _BranchInput implements BranchInput {
  const factory _BranchInput() = _$BranchInputImpl;
}

/// @nodoc
mixin _$BranchOutput {}

/// @nodoc
abstract class $BranchOutputCopyWith<$Res> {
  factory $BranchOutputCopyWith(
          BranchOutput value, $Res Function(BranchOutput) then) =
      _$BranchOutputCopyWithImpl<$Res, BranchOutput>;
}

/// @nodoc
class _$BranchOutputCopyWithImpl<$Res, $Val extends BranchOutput>
    implements $BranchOutputCopyWith<$Res> {
  _$BranchOutputCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$BranchOutputImplCopyWith<$Res> {
  factory _$$BranchOutputImplCopyWith(
          _$BranchOutputImpl value, $Res Function(_$BranchOutputImpl) then) =
      __$$BranchOutputImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$BranchOutputImplCopyWithImpl<$Res>
    extends _$BranchOutputCopyWithImpl<$Res, _$BranchOutputImpl>
    implements _$$BranchOutputImplCopyWith<$Res> {
  __$$BranchOutputImplCopyWithImpl(
      _$BranchOutputImpl _value, $Res Function(_$BranchOutputImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$BranchOutputImpl extends _BranchOutput {
  const _$BranchOutputImpl() : super._();

  @override
  String toString() {
    return 'BranchOutput()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$BranchOutputImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;
}

abstract class _BranchOutput extends BranchOutput {
  const factory _BranchOutput() = _$BranchOutputImpl;
  const _BranchOutput._() : super._();
}
