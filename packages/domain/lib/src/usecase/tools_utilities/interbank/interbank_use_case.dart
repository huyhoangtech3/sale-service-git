import 'package:dartx/dartx.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:resources/resources.dart';
import 'package:shared/shared.dart';
import 'package:domain/domain.dart';
part 'interbank_use_case.freezed.dart';

@Injectable()
class InterbankUseCase extends BaseSyncUseCase<InterbankInput, InterbankOutput> {
  const InterbankUseCase(this._repository);

  final Repository _repository;

  @protected
  @override
  InterbankOutput buildUseCase(InterbankInput input) {
    return InterbankOutput(
      interbankRate: 'interbankRate',
    );
  }
}

@freezed
class InterbankInput extends BaseInput with _$InterbankInput {
  const factory InterbankInput({
    required String interbankRate,
  }) = _InterbankInput;
}

@freezed
class InterbankOutput extends BaseOutput with _$InterbankOutput {
  const InterbankOutput._();

  const factory InterbankOutput({
    @Default('') String interbankRate,
  }) = _InterbankOutput;
}
