// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'interbank_use_case.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$InterbankInput {
  String get interbankRate => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $InterbankInputCopyWith<InterbankInput> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $InterbankInputCopyWith<$Res> {
  factory $InterbankInputCopyWith(
          InterbankInput value, $Res Function(InterbankInput) then) =
      _$InterbankInputCopyWithImpl<$Res, InterbankInput>;
  @useResult
  $Res call({String interbankRate});
}

/// @nodoc
class _$InterbankInputCopyWithImpl<$Res, $Val extends InterbankInput>
    implements $InterbankInputCopyWith<$Res> {
  _$InterbankInputCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? interbankRate = null,
  }) {
    return _then(_value.copyWith(
      interbankRate: null == interbankRate
          ? _value.interbankRate
          : interbankRate // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$InterbankInputImplCopyWith<$Res>
    implements $InterbankInputCopyWith<$Res> {
  factory _$$InterbankInputImplCopyWith(_$InterbankInputImpl value,
          $Res Function(_$InterbankInputImpl) then) =
      __$$InterbankInputImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String interbankRate});
}

/// @nodoc
class __$$InterbankInputImplCopyWithImpl<$Res>
    extends _$InterbankInputCopyWithImpl<$Res, _$InterbankInputImpl>
    implements _$$InterbankInputImplCopyWith<$Res> {
  __$$InterbankInputImplCopyWithImpl(
      _$InterbankInputImpl _value, $Res Function(_$InterbankInputImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? interbankRate = null,
  }) {
    return _then(_$InterbankInputImpl(
      interbankRate: null == interbankRate
          ? _value.interbankRate
          : interbankRate // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$InterbankInputImpl implements _InterbankInput {
  const _$InterbankInputImpl({required this.interbankRate});

  @override
  final String interbankRate;

  @override
  String toString() {
    return 'InterbankInput(interbankRate: $interbankRate)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$InterbankInputImpl &&
            (identical(other.interbankRate, interbankRate) ||
                other.interbankRate == interbankRate));
  }

  @override
  int get hashCode => Object.hash(runtimeType, interbankRate);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$InterbankInputImplCopyWith<_$InterbankInputImpl> get copyWith =>
      __$$InterbankInputImplCopyWithImpl<_$InterbankInputImpl>(
          this, _$identity);
}

abstract class _InterbankInput implements InterbankInput {
  const factory _InterbankInput({required final String interbankRate}) =
      _$InterbankInputImpl;

  @override
  String get interbankRate;
  @override
  @JsonKey(ignore: true)
  _$$InterbankInputImplCopyWith<_$InterbankInputImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$InterbankOutput {
  String get interbankRate => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $InterbankOutputCopyWith<InterbankOutput> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $InterbankOutputCopyWith<$Res> {
  factory $InterbankOutputCopyWith(
          InterbankOutput value, $Res Function(InterbankOutput) then) =
      _$InterbankOutputCopyWithImpl<$Res, InterbankOutput>;
  @useResult
  $Res call({String interbankRate});
}

/// @nodoc
class _$InterbankOutputCopyWithImpl<$Res, $Val extends InterbankOutput>
    implements $InterbankOutputCopyWith<$Res> {
  _$InterbankOutputCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? interbankRate = null,
  }) {
    return _then(_value.copyWith(
      interbankRate: null == interbankRate
          ? _value.interbankRate
          : interbankRate // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$InterbankOutputImplCopyWith<$Res>
    implements $InterbankOutputCopyWith<$Res> {
  factory _$$InterbankOutputImplCopyWith(_$InterbankOutputImpl value,
          $Res Function(_$InterbankOutputImpl) then) =
      __$$InterbankOutputImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String interbankRate});
}

/// @nodoc
class __$$InterbankOutputImplCopyWithImpl<$Res>
    extends _$InterbankOutputCopyWithImpl<$Res, _$InterbankOutputImpl>
    implements _$$InterbankOutputImplCopyWith<$Res> {
  __$$InterbankOutputImplCopyWithImpl(
      _$InterbankOutputImpl _value, $Res Function(_$InterbankOutputImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? interbankRate = null,
  }) {
    return _then(_$InterbankOutputImpl(
      interbankRate: null == interbankRate
          ? _value.interbankRate
          : interbankRate // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$InterbankOutputImpl extends _InterbankOutput {
  const _$InterbankOutputImpl({this.interbankRate = ''}) : super._();

  @override
  @JsonKey()
  final String interbankRate;

  @override
  String toString() {
    return 'InterbankOutput(interbankRate: $interbankRate)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$InterbankOutputImpl &&
            (identical(other.interbankRate, interbankRate) ||
                other.interbankRate == interbankRate));
  }

  @override
  int get hashCode => Object.hash(runtimeType, interbankRate);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$InterbankOutputImplCopyWith<_$InterbankOutputImpl> get copyWith =>
      __$$InterbankOutputImplCopyWithImpl<_$InterbankOutputImpl>(
          this, _$identity);
}

abstract class _InterbankOutput extends InterbankOutput {
  const factory _InterbankOutput({final String interbankRate}) =
      _$InterbankOutputImpl;
  const _InterbankOutput._() : super._();

  @override
  String get interbankRate;
  @override
  @JsonKey(ignore: true)
  _$$InterbankOutputImplCopyWith<_$InterbankOutputImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
