import 'package:dartx/dartx.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:resources/resources.dart';
import 'package:shared/shared.dart';
import 'package:domain/domain.dart';
part 'calculate_saving_use_case.freezed.dart';

@Injectable()
class CalculateSavingUseCase extends BaseSyncUseCase<CalculateSavingInput, CalculateSavingOutput> {
  const CalculateSavingUseCase(this._repository);

  final Repository _repository;

  @protected
  @override
  CalculateSavingOutput buildUseCase(CalculateSavingInput input) {
    final grossValue = (input.depositAmount.toInt() * input.depositTerm.toInt() * input.interestRatePerYear.toInt()) /
        ((input.dateUnit == S.current.day ? 365 : input.dateUnit == S.current.month ? 12 : 1) * 100);
    final netWorthValue = grossValue + input.depositAmount.toInt();
    final totalFormatGrossValue = grossValue.toVnd();
    final totalFormatNetWorthValue = netWorthValue.toVnd();
    return CalculateSavingOutput(
      totalInterestEndPeriod: ' $totalFormatGrossValue',
      totalInterestPrincipalEndPeriod: ' $totalFormatNetWorthValue',
    );
  }
}

@freezed
class CalculateSavingInput extends BaseInput with _$CalculateSavingInput {
  const factory CalculateSavingInput({
    required String depositAmount,
    required String depositTerm,
    required String interestRatePerYear,
    required String dateUnit,
  }) = _CalculateSavingInput;
}

@freezed
class CalculateSavingOutput extends BaseOutput with _$CalculateSavingOutput {
  const CalculateSavingOutput._();

  const factory CalculateSavingOutput({
    @Default('') String totalInterestEndPeriod,
    @Default('') String totalInterestPrincipalEndPeriod,
  }) = _CalculateSavingOutput;
}
