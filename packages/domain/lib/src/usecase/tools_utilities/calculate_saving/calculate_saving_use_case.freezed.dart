// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'calculate_saving_use_case.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$CalculateSavingInput {
  String get depositAmount => throw _privateConstructorUsedError;
  String get depositTerm => throw _privateConstructorUsedError;
  String get interestRatePerYear => throw _privateConstructorUsedError;
  String get dateUnit => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CalculateSavingInputCopyWith<CalculateSavingInput> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CalculateSavingInputCopyWith<$Res> {
  factory $CalculateSavingInputCopyWith(CalculateSavingInput value,
          $Res Function(CalculateSavingInput) then) =
      _$CalculateSavingInputCopyWithImpl<$Res, CalculateSavingInput>;
  @useResult
  $Res call(
      {String depositAmount,
      String depositTerm,
      String interestRatePerYear,
      String dateUnit});
}

/// @nodoc
class _$CalculateSavingInputCopyWithImpl<$Res,
        $Val extends CalculateSavingInput>
    implements $CalculateSavingInputCopyWith<$Res> {
  _$CalculateSavingInputCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? depositAmount = null,
    Object? depositTerm = null,
    Object? interestRatePerYear = null,
    Object? dateUnit = null,
  }) {
    return _then(_value.copyWith(
      depositAmount: null == depositAmount
          ? _value.depositAmount
          : depositAmount // ignore: cast_nullable_to_non_nullable
              as String,
      depositTerm: null == depositTerm
          ? _value.depositTerm
          : depositTerm // ignore: cast_nullable_to_non_nullable
              as String,
      interestRatePerYear: null == interestRatePerYear
          ? _value.interestRatePerYear
          : interestRatePerYear // ignore: cast_nullable_to_non_nullable
              as String,
      dateUnit: null == dateUnit
          ? _value.dateUnit
          : dateUnit // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$CalculateSavingInputImplCopyWith<$Res>
    implements $CalculateSavingInputCopyWith<$Res> {
  factory _$$CalculateSavingInputImplCopyWith(_$CalculateSavingInputImpl value,
          $Res Function(_$CalculateSavingInputImpl) then) =
      __$$CalculateSavingInputImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String depositAmount,
      String depositTerm,
      String interestRatePerYear,
      String dateUnit});
}

/// @nodoc
class __$$CalculateSavingInputImplCopyWithImpl<$Res>
    extends _$CalculateSavingInputCopyWithImpl<$Res, _$CalculateSavingInputImpl>
    implements _$$CalculateSavingInputImplCopyWith<$Res> {
  __$$CalculateSavingInputImplCopyWithImpl(_$CalculateSavingInputImpl _value,
      $Res Function(_$CalculateSavingInputImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? depositAmount = null,
    Object? depositTerm = null,
    Object? interestRatePerYear = null,
    Object? dateUnit = null,
  }) {
    return _then(_$CalculateSavingInputImpl(
      depositAmount: null == depositAmount
          ? _value.depositAmount
          : depositAmount // ignore: cast_nullable_to_non_nullable
              as String,
      depositTerm: null == depositTerm
          ? _value.depositTerm
          : depositTerm // ignore: cast_nullable_to_non_nullable
              as String,
      interestRatePerYear: null == interestRatePerYear
          ? _value.interestRatePerYear
          : interestRatePerYear // ignore: cast_nullable_to_non_nullable
              as String,
      dateUnit: null == dateUnit
          ? _value.dateUnit
          : dateUnit // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$CalculateSavingInputImpl implements _CalculateSavingInput {
  const _$CalculateSavingInputImpl(
      {required this.depositAmount,
      required this.depositTerm,
      required this.interestRatePerYear,
      required this.dateUnit});

  @override
  final String depositAmount;
  @override
  final String depositTerm;
  @override
  final String interestRatePerYear;
  @override
  final String dateUnit;

  @override
  String toString() {
    return 'CalculateSavingInput(depositAmount: $depositAmount, depositTerm: $depositTerm, interestRatePerYear: $interestRatePerYear, dateUnit: $dateUnit)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CalculateSavingInputImpl &&
            (identical(other.depositAmount, depositAmount) ||
                other.depositAmount == depositAmount) &&
            (identical(other.depositTerm, depositTerm) ||
                other.depositTerm == depositTerm) &&
            (identical(other.interestRatePerYear, interestRatePerYear) ||
                other.interestRatePerYear == interestRatePerYear) &&
            (identical(other.dateUnit, dateUnit) ||
                other.dateUnit == dateUnit));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, depositAmount, depositTerm, interestRatePerYear, dateUnit);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CalculateSavingInputImplCopyWith<_$CalculateSavingInputImpl>
      get copyWith =>
          __$$CalculateSavingInputImplCopyWithImpl<_$CalculateSavingInputImpl>(
              this, _$identity);
}

abstract class _CalculateSavingInput implements CalculateSavingInput {
  const factory _CalculateSavingInput(
      {required final String depositAmount,
      required final String depositTerm,
      required final String interestRatePerYear,
      required final String dateUnit}) = _$CalculateSavingInputImpl;

  @override
  String get depositAmount;
  @override
  String get depositTerm;
  @override
  String get interestRatePerYear;
  @override
  String get dateUnit;
  @override
  @JsonKey(ignore: true)
  _$$CalculateSavingInputImplCopyWith<_$CalculateSavingInputImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$CalculateSavingOutput {
  String get totalInterestEndPeriod => throw _privateConstructorUsedError;
  String get totalInterestPrincipalEndPeriod =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CalculateSavingOutputCopyWith<CalculateSavingOutput> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CalculateSavingOutputCopyWith<$Res> {
  factory $CalculateSavingOutputCopyWith(CalculateSavingOutput value,
          $Res Function(CalculateSavingOutput) then) =
      _$CalculateSavingOutputCopyWithImpl<$Res, CalculateSavingOutput>;
  @useResult
  $Res call(
      {String totalInterestEndPeriod, String totalInterestPrincipalEndPeriod});
}

/// @nodoc
class _$CalculateSavingOutputCopyWithImpl<$Res,
        $Val extends CalculateSavingOutput>
    implements $CalculateSavingOutputCopyWith<$Res> {
  _$CalculateSavingOutputCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? totalInterestEndPeriod = null,
    Object? totalInterestPrincipalEndPeriod = null,
  }) {
    return _then(_value.copyWith(
      totalInterestEndPeriod: null == totalInterestEndPeriod
          ? _value.totalInterestEndPeriod
          : totalInterestEndPeriod // ignore: cast_nullable_to_non_nullable
              as String,
      totalInterestPrincipalEndPeriod: null == totalInterestPrincipalEndPeriod
          ? _value.totalInterestPrincipalEndPeriod
          : totalInterestPrincipalEndPeriod // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$CalculateSavingOutputImplCopyWith<$Res>
    implements $CalculateSavingOutputCopyWith<$Res> {
  factory _$$CalculateSavingOutputImplCopyWith(
          _$CalculateSavingOutputImpl value,
          $Res Function(_$CalculateSavingOutputImpl) then) =
      __$$CalculateSavingOutputImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String totalInterestEndPeriod, String totalInterestPrincipalEndPeriod});
}

/// @nodoc
class __$$CalculateSavingOutputImplCopyWithImpl<$Res>
    extends _$CalculateSavingOutputCopyWithImpl<$Res,
        _$CalculateSavingOutputImpl>
    implements _$$CalculateSavingOutputImplCopyWith<$Res> {
  __$$CalculateSavingOutputImplCopyWithImpl(_$CalculateSavingOutputImpl _value,
      $Res Function(_$CalculateSavingOutputImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? totalInterestEndPeriod = null,
    Object? totalInterestPrincipalEndPeriod = null,
  }) {
    return _then(_$CalculateSavingOutputImpl(
      totalInterestEndPeriod: null == totalInterestEndPeriod
          ? _value.totalInterestEndPeriod
          : totalInterestEndPeriod // ignore: cast_nullable_to_non_nullable
              as String,
      totalInterestPrincipalEndPeriod: null == totalInterestPrincipalEndPeriod
          ? _value.totalInterestPrincipalEndPeriod
          : totalInterestPrincipalEndPeriod // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$CalculateSavingOutputImpl extends _CalculateSavingOutput {
  const _$CalculateSavingOutputImpl(
      {this.totalInterestEndPeriod = '',
      this.totalInterestPrincipalEndPeriod = ''})
      : super._();

  @override
  @JsonKey()
  final String totalInterestEndPeriod;
  @override
  @JsonKey()
  final String totalInterestPrincipalEndPeriod;

  @override
  String toString() {
    return 'CalculateSavingOutput(totalInterestEndPeriod: $totalInterestEndPeriod, totalInterestPrincipalEndPeriod: $totalInterestPrincipalEndPeriod)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CalculateSavingOutputImpl &&
            (identical(other.totalInterestEndPeriod, totalInterestEndPeriod) ||
                other.totalInterestEndPeriod == totalInterestEndPeriod) &&
            (identical(other.totalInterestPrincipalEndPeriod,
                    totalInterestPrincipalEndPeriod) ||
                other.totalInterestPrincipalEndPeriod ==
                    totalInterestPrincipalEndPeriod));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, totalInterestEndPeriod, totalInterestPrincipalEndPeriod);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CalculateSavingOutputImplCopyWith<_$CalculateSavingOutputImpl>
      get copyWith => __$$CalculateSavingOutputImplCopyWithImpl<
          _$CalculateSavingOutputImpl>(this, _$identity);
}

abstract class _CalculateSavingOutput extends CalculateSavingOutput {
  const factory _CalculateSavingOutput(
          {final String totalInterestEndPeriod,
          final String totalInterestPrincipalEndPeriod}) =
      _$CalculateSavingOutputImpl;
  const _CalculateSavingOutput._() : super._();

  @override
  String get totalInterestEndPeriod;
  @override
  String get totalInterestPrincipalEndPeriod;
  @override
  @JsonKey(ignore: true)
  _$$CalculateSavingOutputImplCopyWith<_$CalculateSavingOutputImpl>
      get copyWith => throw _privateConstructorUsedError;
}
