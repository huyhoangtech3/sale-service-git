import 'package:dartx/dartx.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:resources/resources.dart';
import 'package:shared/shared.dart';
import 'package:domain/domain.dart';
part 'exchange_rate_use_case.freezed.dart';

@Injectable()
class ExchangeRateUseCase extends BaseSyncUseCase<ExchangeRateInput, ExchangeRateOutput> {
  const ExchangeRateUseCase(this._repository);

  final Repository _repository;

  @protected
  @override
  ExchangeRateOutput buildUseCase(ExchangeRateInput input) {
    return ExchangeRateOutput(
      dailyExchangeRate: 'dailyExchangeRate',
    );
  }
}

@freezed
class ExchangeRateInput extends BaseInput with _$ExchangeRateInput {
  const factory ExchangeRateInput({
    required String dailyExchangeRate,
  }) = _ExchangeRateInput;
}

@freezed
class ExchangeRateOutput extends BaseOutput with _$ExchangeRateOutput {
  const ExchangeRateOutput._();

  const factory ExchangeRateOutput({
    @Default('') String dailyExchangeRate,
  }) = _ExchangeRateOutput;
}
