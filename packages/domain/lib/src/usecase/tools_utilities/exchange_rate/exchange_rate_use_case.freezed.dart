// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'exchange_rate_use_case.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$ExchangeRateInput {
  String get dailyExchangeRate => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ExchangeRateInputCopyWith<ExchangeRateInput> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ExchangeRateInputCopyWith<$Res> {
  factory $ExchangeRateInputCopyWith(
          ExchangeRateInput value, $Res Function(ExchangeRateInput) then) =
      _$ExchangeRateInputCopyWithImpl<$Res, ExchangeRateInput>;
  @useResult
  $Res call({String dailyExchangeRate});
}

/// @nodoc
class _$ExchangeRateInputCopyWithImpl<$Res, $Val extends ExchangeRateInput>
    implements $ExchangeRateInputCopyWith<$Res> {
  _$ExchangeRateInputCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? dailyExchangeRate = null,
  }) {
    return _then(_value.copyWith(
      dailyExchangeRate: null == dailyExchangeRate
          ? _value.dailyExchangeRate
          : dailyExchangeRate // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ExchangeRateInputImplCopyWith<$Res>
    implements $ExchangeRateInputCopyWith<$Res> {
  factory _$$ExchangeRateInputImplCopyWith(_$ExchangeRateInputImpl value,
          $Res Function(_$ExchangeRateInputImpl) then) =
      __$$ExchangeRateInputImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String dailyExchangeRate});
}

/// @nodoc
class __$$ExchangeRateInputImplCopyWithImpl<$Res>
    extends _$ExchangeRateInputCopyWithImpl<$Res, _$ExchangeRateInputImpl>
    implements _$$ExchangeRateInputImplCopyWith<$Res> {
  __$$ExchangeRateInputImplCopyWithImpl(_$ExchangeRateInputImpl _value,
      $Res Function(_$ExchangeRateInputImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? dailyExchangeRate = null,
  }) {
    return _then(_$ExchangeRateInputImpl(
      dailyExchangeRate: null == dailyExchangeRate
          ? _value.dailyExchangeRate
          : dailyExchangeRate // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$ExchangeRateInputImpl implements _ExchangeRateInput {
  const _$ExchangeRateInputImpl({required this.dailyExchangeRate});

  @override
  final String dailyExchangeRate;

  @override
  String toString() {
    return 'ExchangeRateInput(dailyExchangeRate: $dailyExchangeRate)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ExchangeRateInputImpl &&
            (identical(other.dailyExchangeRate, dailyExchangeRate) ||
                other.dailyExchangeRate == dailyExchangeRate));
  }

  @override
  int get hashCode => Object.hash(runtimeType, dailyExchangeRate);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ExchangeRateInputImplCopyWith<_$ExchangeRateInputImpl> get copyWith =>
      __$$ExchangeRateInputImplCopyWithImpl<_$ExchangeRateInputImpl>(
          this, _$identity);
}

abstract class _ExchangeRateInput implements ExchangeRateInput {
  const factory _ExchangeRateInput({required final String dailyExchangeRate}) =
      _$ExchangeRateInputImpl;

  @override
  String get dailyExchangeRate;
  @override
  @JsonKey(ignore: true)
  _$$ExchangeRateInputImplCopyWith<_$ExchangeRateInputImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$ExchangeRateOutput {
  String get dailyExchangeRate => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ExchangeRateOutputCopyWith<ExchangeRateOutput> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ExchangeRateOutputCopyWith<$Res> {
  factory $ExchangeRateOutputCopyWith(
          ExchangeRateOutput value, $Res Function(ExchangeRateOutput) then) =
      _$ExchangeRateOutputCopyWithImpl<$Res, ExchangeRateOutput>;
  @useResult
  $Res call({String dailyExchangeRate});
}

/// @nodoc
class _$ExchangeRateOutputCopyWithImpl<$Res, $Val extends ExchangeRateOutput>
    implements $ExchangeRateOutputCopyWith<$Res> {
  _$ExchangeRateOutputCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? dailyExchangeRate = null,
  }) {
    return _then(_value.copyWith(
      dailyExchangeRate: null == dailyExchangeRate
          ? _value.dailyExchangeRate
          : dailyExchangeRate // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ExchangeRateOutputImplCopyWith<$Res>
    implements $ExchangeRateOutputCopyWith<$Res> {
  factory _$$ExchangeRateOutputImplCopyWith(_$ExchangeRateOutputImpl value,
          $Res Function(_$ExchangeRateOutputImpl) then) =
      __$$ExchangeRateOutputImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String dailyExchangeRate});
}

/// @nodoc
class __$$ExchangeRateOutputImplCopyWithImpl<$Res>
    extends _$ExchangeRateOutputCopyWithImpl<$Res, _$ExchangeRateOutputImpl>
    implements _$$ExchangeRateOutputImplCopyWith<$Res> {
  __$$ExchangeRateOutputImplCopyWithImpl(_$ExchangeRateOutputImpl _value,
      $Res Function(_$ExchangeRateOutputImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? dailyExchangeRate = null,
  }) {
    return _then(_$ExchangeRateOutputImpl(
      dailyExchangeRate: null == dailyExchangeRate
          ? _value.dailyExchangeRate
          : dailyExchangeRate // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$ExchangeRateOutputImpl extends _ExchangeRateOutput {
  const _$ExchangeRateOutputImpl({this.dailyExchangeRate = ''}) : super._();

  @override
  @JsonKey()
  final String dailyExchangeRate;

  @override
  String toString() {
    return 'ExchangeRateOutput(dailyExchangeRate: $dailyExchangeRate)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ExchangeRateOutputImpl &&
            (identical(other.dailyExchangeRate, dailyExchangeRate) ||
                other.dailyExchangeRate == dailyExchangeRate));
  }

  @override
  int get hashCode => Object.hash(runtimeType, dailyExchangeRate);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ExchangeRateOutputImplCopyWith<_$ExchangeRateOutputImpl> get copyWith =>
      __$$ExchangeRateOutputImplCopyWithImpl<_$ExchangeRateOutputImpl>(
          this, _$identity);
}

abstract class _ExchangeRateOutput extends ExchangeRateOutput {
  const factory _ExchangeRateOutput({final String dailyExchangeRate}) =
      _$ExchangeRateOutputImpl;
  const _ExchangeRateOutput._() : super._();

  @override
  String get dailyExchangeRate;
  @override
  @JsonKey(ignore: true)
  _$$ExchangeRateOutputImplCopyWith<_$ExchangeRateOutputImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
