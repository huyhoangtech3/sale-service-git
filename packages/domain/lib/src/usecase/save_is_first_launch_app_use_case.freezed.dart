// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'save_is_first_launch_app_use_case.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$SaveIsFirstLaunchAppInput {
  bool get isFirstLaunchApp => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $SaveIsFirstLaunchAppInputCopyWith<SaveIsFirstLaunchAppInput> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SaveIsFirstLaunchAppInputCopyWith<$Res> {
  factory $SaveIsFirstLaunchAppInputCopyWith(SaveIsFirstLaunchAppInput value,
          $Res Function(SaveIsFirstLaunchAppInput) then) =
      _$SaveIsFirstLaunchAppInputCopyWithImpl<$Res, SaveIsFirstLaunchAppInput>;
  @useResult
  $Res call({bool isFirstLaunchApp});
}

/// @nodoc
class _$SaveIsFirstLaunchAppInputCopyWithImpl<$Res,
        $Val extends SaveIsFirstLaunchAppInput>
    implements $SaveIsFirstLaunchAppInputCopyWith<$Res> {
  _$SaveIsFirstLaunchAppInputCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isFirstLaunchApp = null,
  }) {
    return _then(_value.copyWith(
      isFirstLaunchApp: null == isFirstLaunchApp
          ? _value.isFirstLaunchApp
          : isFirstLaunchApp // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$SaveIsFirstLaunchAppInputImplCopyWith<$Res>
    implements $SaveIsFirstLaunchAppInputCopyWith<$Res> {
  factory _$$SaveIsFirstLaunchAppInputImplCopyWith(
          _$SaveIsFirstLaunchAppInputImpl value,
          $Res Function(_$SaveIsFirstLaunchAppInputImpl) then) =
      __$$SaveIsFirstLaunchAppInputImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({bool isFirstLaunchApp});
}

/// @nodoc
class __$$SaveIsFirstLaunchAppInputImplCopyWithImpl<$Res>
    extends _$SaveIsFirstLaunchAppInputCopyWithImpl<$Res,
        _$SaveIsFirstLaunchAppInputImpl>
    implements _$$SaveIsFirstLaunchAppInputImplCopyWith<$Res> {
  __$$SaveIsFirstLaunchAppInputImplCopyWithImpl(
      _$SaveIsFirstLaunchAppInputImpl _value,
      $Res Function(_$SaveIsFirstLaunchAppInputImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isFirstLaunchApp = null,
  }) {
    return _then(_$SaveIsFirstLaunchAppInputImpl(
      isFirstLaunchApp: null == isFirstLaunchApp
          ? _value.isFirstLaunchApp
          : isFirstLaunchApp // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$SaveIsFirstLaunchAppInputImpl implements _SaveIsFirstLaunchAppInput {
  const _$SaveIsFirstLaunchAppInputImpl({required this.isFirstLaunchApp});

  @override
  final bool isFirstLaunchApp;

  @override
  String toString() {
    return 'SaveIsFirstLaunchAppInput(isFirstLaunchApp: $isFirstLaunchApp)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SaveIsFirstLaunchAppInputImpl &&
            (identical(other.isFirstLaunchApp, isFirstLaunchApp) ||
                other.isFirstLaunchApp == isFirstLaunchApp));
  }

  @override
  int get hashCode => Object.hash(runtimeType, isFirstLaunchApp);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$SaveIsFirstLaunchAppInputImplCopyWith<_$SaveIsFirstLaunchAppInputImpl>
      get copyWith => __$$SaveIsFirstLaunchAppInputImplCopyWithImpl<
          _$SaveIsFirstLaunchAppInputImpl>(this, _$identity);
}

abstract class _SaveIsFirstLaunchAppInput implements SaveIsFirstLaunchAppInput {
  const factory _SaveIsFirstLaunchAppInput(
      {required final bool isFirstLaunchApp}) = _$SaveIsFirstLaunchAppInputImpl;

  @override
  bool get isFirstLaunchApp;
  @override
  @JsonKey(ignore: true)
  _$$SaveIsFirstLaunchAppInputImplCopyWith<_$SaveIsFirstLaunchAppInputImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$SaveIsFirstLaunchAppOutput {}

/// @nodoc
abstract class $SaveIsFirstLaunchAppOutputCopyWith<$Res> {
  factory $SaveIsFirstLaunchAppOutputCopyWith(SaveIsFirstLaunchAppOutput value,
          $Res Function(SaveIsFirstLaunchAppOutput) then) =
      _$SaveIsFirstLaunchAppOutputCopyWithImpl<$Res,
          SaveIsFirstLaunchAppOutput>;
}

/// @nodoc
class _$SaveIsFirstLaunchAppOutputCopyWithImpl<$Res,
        $Val extends SaveIsFirstLaunchAppOutput>
    implements $SaveIsFirstLaunchAppOutputCopyWith<$Res> {
  _$SaveIsFirstLaunchAppOutputCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$SaveIsFirstLaunchAppOutputImplCopyWith<$Res> {
  factory _$$SaveIsFirstLaunchAppOutputImplCopyWith(
          _$SaveIsFirstLaunchAppOutputImpl value,
          $Res Function(_$SaveIsFirstLaunchAppOutputImpl) then) =
      __$$SaveIsFirstLaunchAppOutputImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$SaveIsFirstLaunchAppOutputImplCopyWithImpl<$Res>
    extends _$SaveIsFirstLaunchAppOutputCopyWithImpl<$Res,
        _$SaveIsFirstLaunchAppOutputImpl>
    implements _$$SaveIsFirstLaunchAppOutputImplCopyWith<$Res> {
  __$$SaveIsFirstLaunchAppOutputImplCopyWithImpl(
      _$SaveIsFirstLaunchAppOutputImpl _value,
      $Res Function(_$SaveIsFirstLaunchAppOutputImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$SaveIsFirstLaunchAppOutputImpl extends _SaveIsFirstLaunchAppOutput {
  const _$SaveIsFirstLaunchAppOutputImpl() : super._();

  @override
  String toString() {
    return 'SaveIsFirstLaunchAppOutput()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SaveIsFirstLaunchAppOutputImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;
}

abstract class _SaveIsFirstLaunchAppOutput extends SaveIsFirstLaunchAppOutput {
  const factory _SaveIsFirstLaunchAppOutput() =
      _$SaveIsFirstLaunchAppOutputImpl;
  const _SaveIsFirstLaunchAppOutput._() : super._();
}
