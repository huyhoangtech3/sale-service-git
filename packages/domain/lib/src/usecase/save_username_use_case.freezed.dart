// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'save_username_use_case.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$SaveUserNameInput {
  String get userName => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $SaveUserNameInputCopyWith<SaveUserNameInput> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SaveUserNameInputCopyWith<$Res> {
  factory $SaveUserNameInputCopyWith(
          SaveUserNameInput value, $Res Function(SaveUserNameInput) then) =
      _$SaveUserNameInputCopyWithImpl<$Res, SaveUserNameInput>;
  @useResult
  $Res call({String userName});
}

/// @nodoc
class _$SaveUserNameInputCopyWithImpl<$Res, $Val extends SaveUserNameInput>
    implements $SaveUserNameInputCopyWith<$Res> {
  _$SaveUserNameInputCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? userName = null,
  }) {
    return _then(_value.copyWith(
      userName: null == userName
          ? _value.userName
          : userName // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$SaveUserNameUseCaseImplCopyWith<$Res>
    implements $SaveUserNameInputCopyWith<$Res> {
  factory _$$SaveUserNameUseCaseImplCopyWith(_$SaveUserNameUseCaseImpl value,
          $Res Function(_$SaveUserNameUseCaseImpl) then) =
      __$$SaveUserNameUseCaseImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String userName});
}

/// @nodoc
class __$$SaveUserNameUseCaseImplCopyWithImpl<$Res>
    extends _$SaveUserNameInputCopyWithImpl<$Res, _$SaveUserNameUseCaseImpl>
    implements _$$SaveUserNameUseCaseImplCopyWith<$Res> {
  __$$SaveUserNameUseCaseImplCopyWithImpl(_$SaveUserNameUseCaseImpl _value,
      $Res Function(_$SaveUserNameUseCaseImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? userName = null,
  }) {
    return _then(_$SaveUserNameUseCaseImpl(
      userName: null == userName
          ? _value.userName
          : userName // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$SaveUserNameUseCaseImpl implements _SaveUserNameUseCase {
  const _$SaveUserNameUseCaseImpl({required this.userName});

  @override
  final String userName;

  @override
  String toString() {
    return 'SaveUserNameInput(userName: $userName)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SaveUserNameUseCaseImpl &&
            (identical(other.userName, userName) ||
                other.userName == userName));
  }

  @override
  int get hashCode => Object.hash(runtimeType, userName);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$SaveUserNameUseCaseImplCopyWith<_$SaveUserNameUseCaseImpl> get copyWith =>
      __$$SaveUserNameUseCaseImplCopyWithImpl<_$SaveUserNameUseCaseImpl>(
          this, _$identity);
}

abstract class _SaveUserNameUseCase implements SaveUserNameInput {
  const factory _SaveUserNameUseCase({required final String userName}) =
      _$SaveUserNameUseCaseImpl;

  @override
  String get userName;
  @override
  @JsonKey(ignore: true)
  _$$SaveUserNameUseCaseImplCopyWith<_$SaveUserNameUseCaseImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$SaveUserNameOutput {}

/// @nodoc
abstract class $SaveUserNameOutputCopyWith<$Res> {
  factory $SaveUserNameOutputCopyWith(
          SaveUserNameOutput value, $Res Function(SaveUserNameOutput) then) =
      _$SaveUserNameOutputCopyWithImpl<$Res, SaveUserNameOutput>;
}

/// @nodoc
class _$SaveUserNameOutputCopyWithImpl<$Res, $Val extends SaveUserNameOutput>
    implements $SaveUserNameOutputCopyWith<$Res> {
  _$SaveUserNameOutputCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$SaveUserNameOutputImplCopyWith<$Res> {
  factory _$$SaveUserNameOutputImplCopyWith(_$SaveUserNameOutputImpl value,
          $Res Function(_$SaveUserNameOutputImpl) then) =
      __$$SaveUserNameOutputImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$SaveUserNameOutputImplCopyWithImpl<$Res>
    extends _$SaveUserNameOutputCopyWithImpl<$Res, _$SaveUserNameOutputImpl>
    implements _$$SaveUserNameOutputImplCopyWith<$Res> {
  __$$SaveUserNameOutputImplCopyWithImpl(_$SaveUserNameOutputImpl _value,
      $Res Function(_$SaveUserNameOutputImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$SaveUserNameOutputImpl extends _SaveUserNameOutput {
  const _$SaveUserNameOutputImpl() : super._();

  @override
  String toString() {
    return 'SaveUserNameOutput()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$SaveUserNameOutputImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;
}

abstract class _SaveUserNameOutput extends SaveUserNameOutput {
  const factory _SaveUserNameOutput() = _$SaveUserNameOutputImpl;
  const _SaveUserNameOutput._() : super._();
}
