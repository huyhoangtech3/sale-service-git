// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'logout_use_case.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$LogoutInput {}

/// @nodoc
abstract class $LogoutInputCopyWith<$Res> {
  factory $LogoutInputCopyWith(
          LogoutInput value, $Res Function(LogoutInput) then) =
      _$LogoutInputCopyWithImpl<$Res, LogoutInput>;
}

/// @nodoc
class _$LogoutInputCopyWithImpl<$Res, $Val extends LogoutInput>
    implements $LogoutInputCopyWith<$Res> {
  _$LogoutInputCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$LogoutUseCaseImplCopyWith<$Res> {
  factory _$$LogoutUseCaseImplCopyWith(
          _$LogoutUseCaseImpl value, $Res Function(_$LogoutUseCaseImpl) then) =
      __$$LogoutUseCaseImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$LogoutUseCaseImplCopyWithImpl<$Res>
    extends _$LogoutInputCopyWithImpl<$Res, _$LogoutUseCaseImpl>
    implements _$$LogoutUseCaseImplCopyWith<$Res> {
  __$$LogoutUseCaseImplCopyWithImpl(
      _$LogoutUseCaseImpl _value, $Res Function(_$LogoutUseCaseImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$LogoutUseCaseImpl implements _LogoutUseCase {
  const _$LogoutUseCaseImpl();

  @override
  String toString() {
    return 'LogoutInput()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$LogoutUseCaseImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;
}

abstract class _LogoutUseCase implements LogoutInput {
  const factory _LogoutUseCase() = _$LogoutUseCaseImpl;
}

/// @nodoc
mixin _$LogoutOutput {}

/// @nodoc
abstract class $LogoutOutputCopyWith<$Res> {
  factory $LogoutOutputCopyWith(
          LogoutOutput value, $Res Function(LogoutOutput) then) =
      _$LogoutOutputCopyWithImpl<$Res, LogoutOutput>;
}

/// @nodoc
class _$LogoutOutputCopyWithImpl<$Res, $Val extends LogoutOutput>
    implements $LogoutOutputCopyWith<$Res> {
  _$LogoutOutputCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$LogoutOutputImplCopyWith<$Res> {
  factory _$$LogoutOutputImplCopyWith(
          _$LogoutOutputImpl value, $Res Function(_$LogoutOutputImpl) then) =
      __$$LogoutOutputImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$LogoutOutputImplCopyWithImpl<$Res>
    extends _$LogoutOutputCopyWithImpl<$Res, _$LogoutOutputImpl>
    implements _$$LogoutOutputImplCopyWith<$Res> {
  __$$LogoutOutputImplCopyWithImpl(
      _$LogoutOutputImpl _value, $Res Function(_$LogoutOutputImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$LogoutOutputImpl extends _LogoutOutput {
  const _$LogoutOutputImpl() : super._();

  @override
  String toString() {
    return 'LogoutOutput()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$LogoutOutputImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;
}

abstract class _LogoutOutput extends LogoutOutput {
  const factory _LogoutOutput() = _$LogoutOutputImpl;
  const _LogoutOutput._() : super._();
}
