// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'block_list_use_case.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$BlockListInput {
  int get page => throw _privateConstructorUsedError;
  int get size => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $BlockListInputCopyWith<BlockListInput> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BlockListInputCopyWith<$Res> {
  factory $BlockListInputCopyWith(
          BlockListInput value, $Res Function(BlockListInput) then) =
      _$BlockListInputCopyWithImpl<$Res, BlockListInput>;
  @useResult
  $Res call({int page, int size});
}

/// @nodoc
class _$BlockListInputCopyWithImpl<$Res, $Val extends BlockListInput>
    implements $BlockListInputCopyWith<$Res> {
  _$BlockListInputCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? page = null,
    Object? size = null,
  }) {
    return _then(_value.copyWith(
      page: null == page
          ? _value.page
          : page // ignore: cast_nullable_to_non_nullable
              as int,
      size: null == size
          ? _value.size
          : size // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$BlockListInputImplCopyWith<$Res>
    implements $BlockListInputCopyWith<$Res> {
  factory _$$BlockListInputImplCopyWith(_$BlockListInputImpl value,
          $Res Function(_$BlockListInputImpl) then) =
      __$$BlockListInputImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int page, int size});
}

/// @nodoc
class __$$BlockListInputImplCopyWithImpl<$Res>
    extends _$BlockListInputCopyWithImpl<$Res, _$BlockListInputImpl>
    implements _$$BlockListInputImplCopyWith<$Res> {
  __$$BlockListInputImplCopyWithImpl(
      _$BlockListInputImpl _value, $Res Function(_$BlockListInputImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? page = null,
    Object? size = null,
  }) {
    return _then(_$BlockListInputImpl(
      page: null == page
          ? _value.page
          : page // ignore: cast_nullable_to_non_nullable
              as int,
      size: null == size
          ? _value.size
          : size // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$BlockListInputImpl implements _BlockListInput {
  const _$BlockListInputImpl({required this.page, required this.size});

  @override
  final int page;
  @override
  final int size;

  @override
  String toString() {
    return 'BlockListInput(page: $page, size: $size)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$BlockListInputImpl &&
            (identical(other.page, page) || other.page == page) &&
            (identical(other.size, size) || other.size == size));
  }

  @override
  int get hashCode => Object.hash(runtimeType, page, size);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$BlockListInputImplCopyWith<_$BlockListInputImpl> get copyWith =>
      __$$BlockListInputImplCopyWithImpl<_$BlockListInputImpl>(
          this, _$identity);
}

abstract class _BlockListInput implements BlockListInput {
  const factory _BlockListInput(
      {required final int page,
      required final int size}) = _$BlockListInputImpl;

  @override
  int get page;
  @override
  int get size;
  @override
  @JsonKey(ignore: true)
  _$$BlockListInputImplCopyWith<_$BlockListInputImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$BlockListOutput {}

/// @nodoc
abstract class $BlockListOutputCopyWith<$Res> {
  factory $BlockListOutputCopyWith(
          BlockListOutput value, $Res Function(BlockListOutput) then) =
      _$BlockListOutputCopyWithImpl<$Res, BlockListOutput>;
}

/// @nodoc
class _$BlockListOutputCopyWithImpl<$Res, $Val extends BlockListOutput>
    implements $BlockListOutputCopyWith<$Res> {
  _$BlockListOutputCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$BlockListOutputImplCopyWith<$Res> {
  factory _$$BlockListOutputImplCopyWith(_$BlockListOutputImpl value,
          $Res Function(_$BlockListOutputImpl) then) =
      __$$BlockListOutputImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$BlockListOutputImplCopyWithImpl<$Res>
    extends _$BlockListOutputCopyWithImpl<$Res, _$BlockListOutputImpl>
    implements _$$BlockListOutputImplCopyWith<$Res> {
  __$$BlockListOutputImplCopyWithImpl(
      _$BlockListOutputImpl _value, $Res Function(_$BlockListOutputImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$BlockListOutputImpl extends _BlockListOutput {
  const _$BlockListOutputImpl() : super._();

  @override
  String toString() {
    return 'BlockListOutput()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$BlockListOutputImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;
}

abstract class _BlockListOutput extends BlockListOutput {
  const factory _BlockListOutput() = _$BlockListOutputImpl;
  const _BlockListOutput._() : super._();
}
