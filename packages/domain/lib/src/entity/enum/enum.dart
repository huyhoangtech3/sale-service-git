// import 'package:base_architecture_flutter/resource/app_icons.dart';
import 'package:domain/domain.dart';
import 'package:flutter/material.dart';
import 'package:resources/resources.dart';
import 'package:shared/shared.dart';

enum InitialAppRoute {
  login,
  main,
}

enum Gender {
  male(ServerRequestResponseConstants.male),
  female(ServerRequestResponseConstants.female),
  other(ServerRequestResponseConstants.other),
  unknown(ServerRequestResponseConstants.unknown);

  const Gender(this.serverValue);
  final int serverValue;

  static const defaultValue = unknown;
}

enum LanguageCode {
  en(
    localeCode: LocaleConstants.en,
    serverValue: ServerRequestResponseConstants.en,
  ),
  vi(
    localeCode: LocaleConstants.vi,
    serverValue: ServerRequestResponseConstants.vi,
  );

  const LanguageCode({
    required this.localeCode,
    required this.serverValue,
  });
  final String localeCode;
  final String serverValue;

  static const defaultValue = vi;
}

enum NotificationType {
  unknown,
  newPost,
  liked;

  static const defaultValue = unknown;
}

// enum BottomTab {
//   dashboard(icon: Icon(Icons.dashboard_outlined), activeIcon: Icon(Icons.dashboard_outlined)),
//   customer(icon: Icon(AppIcons.user_1), activeIcon: Icon(AppIcons.user_1)),
//   rm(icon: Icon(AppIcons.rm360), activeIcon: Icon(AppIcons.rm360)),
//   campaign(icon: Icon(AppIcons.campaign), activeIcon: Icon(AppIcons.campaign)),
//   sales(icon: Icon(AppIcons.sales), activeIcon: Icon(AppIcons.sales)),
//   saleKit(icon: Icon(Icons.cases_outlined), activeIcon: Icon(Icons.cases_outlined)),
//   warning(icon: Icon(Icons.warning_amber), activeIcon: Icon(Icons.warning_amber)),
//   setting(icon: Icon(Icons.settings_outlined), activeIcon: Icon(Icons.settings_outlined)),
//   userInfo(icon: Icon(Icons.supervised_user_circle, size: 0), activeIcon: Icon(Icons.supervised_user_circle, size: 0));
//
//   const BottomTab({
//     required this.icon,
//     required this.activeIcon,
//   });
//   final Widget icon;
//   final Widget activeIcon;
//
//   String get title {
//     switch (this) {
//       case BottomTab.dashboard:
//         return S.current.dashboard;
//       case BottomTab.customer:
//         return S.current.customer360;
//       case BottomTab.rm:
//         return S.current.rm360;
//       case BottomTab.campaign:
//         return S.current.campaign;
//       case BottomTab.sales:
//         return S.current.sales;
//       case BottomTab.saleKit:
//         return S.current.saleKit;
//       case BottomTab.warning:
//         return S.current.warning;
//       case BottomTab.setting:
//         return S.current.setting;
//       case BottomTab.userInfo:
//         return Constants.space;
//     }
//   }
// }
