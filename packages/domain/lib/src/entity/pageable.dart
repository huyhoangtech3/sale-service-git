import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shared/shared.dart';

import 'package:domain/domain.dart';

part 'pageable.freezed.dart';

@freezed
class Pageable with _$Pageable {
  const factory Pageable({
    @Default(Pageable.defaultSort) Sort sort,
    @Default(Pageable.defaultPageNumber) int pageNumber,
    @Default(Pageable.defaultPageSize) int pageSize,
    @Default(Pageable.defaultOffset) int offset,
    @Default(Pageable.defaultPaged) bool paged,
    @Default(Pageable.defaultUnpaged) bool unpaged,
  }) = _Pageable;

  static const defaultSort = Sort();
  static const defaultPageNumber = 0;
  static const defaultPageSize = 0;
  static const defaultOffset = 0;
  static const defaultPaged = false;
  static const defaultUnpaged = false;
}
