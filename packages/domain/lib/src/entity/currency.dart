import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shared/shared.dart';

import 'package:domain/domain.dart';

part 'currency.freezed.dart';

@freezed
class Currency with _$Currency {
  const factory Currency({
    @Default(Currency.defaultCurrency) String currency,
    @Default(Currency.defaultItbBuy) int itbBuy,
    @Default(Currency.defaultItbSell) int itbSell,
  }) = _Currency;

  static const defaultCurrency = '';
  static const defaultItbBuy = 0;
  static const defaultItbSell = 0;
}
