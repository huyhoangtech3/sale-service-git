// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'paging.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$Paging<T> {
  List<T> get content => throw _privateConstructorUsedError;
  int get totalPages => throw _privateConstructorUsedError;
  bool get last => throw _privateConstructorUsedError;
  int get totalElements => throw _privateConstructorUsedError;
  bool get first => throw _privateConstructorUsedError;
  int get numberOfElements => throw _privateConstructorUsedError;
  int get number => throw _privateConstructorUsedError;
  int get size => throw _privateConstructorUsedError;
  bool get empty => throw _privateConstructorUsedError;
  Sort get sort => throw _privateConstructorUsedError;
  Pageable get pageable => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $PagingCopyWith<T, Paging<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PagingCopyWith<T, $Res> {
  factory $PagingCopyWith(Paging<T> value, $Res Function(Paging<T>) then) =
      _$PagingCopyWithImpl<T, $Res, Paging<T>>;
  @useResult
  $Res call(
      {List<T> content,
      int totalPages,
      bool last,
      int totalElements,
      bool first,
      int numberOfElements,
      int number,
      int size,
      bool empty,
      Sort sort,
      Pageable pageable});

  $SortCopyWith<$Res> get sort;
  $PageableCopyWith<$Res> get pageable;
}

/// @nodoc
class _$PagingCopyWithImpl<T, $Res, $Val extends Paging<T>>
    implements $PagingCopyWith<T, $Res> {
  _$PagingCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? content = null,
    Object? totalPages = null,
    Object? last = null,
    Object? totalElements = null,
    Object? first = null,
    Object? numberOfElements = null,
    Object? number = null,
    Object? size = null,
    Object? empty = null,
    Object? sort = null,
    Object? pageable = null,
  }) {
    return _then(_value.copyWith(
      content: null == content
          ? _value.content
          : content // ignore: cast_nullable_to_non_nullable
              as List<T>,
      totalPages: null == totalPages
          ? _value.totalPages
          : totalPages // ignore: cast_nullable_to_non_nullable
              as int,
      last: null == last
          ? _value.last
          : last // ignore: cast_nullable_to_non_nullable
              as bool,
      totalElements: null == totalElements
          ? _value.totalElements
          : totalElements // ignore: cast_nullable_to_non_nullable
              as int,
      first: null == first
          ? _value.first
          : first // ignore: cast_nullable_to_non_nullable
              as bool,
      numberOfElements: null == numberOfElements
          ? _value.numberOfElements
          : numberOfElements // ignore: cast_nullable_to_non_nullable
              as int,
      number: null == number
          ? _value.number
          : number // ignore: cast_nullable_to_non_nullable
              as int,
      size: null == size
          ? _value.size
          : size // ignore: cast_nullable_to_non_nullable
              as int,
      empty: null == empty
          ? _value.empty
          : empty // ignore: cast_nullable_to_non_nullable
              as bool,
      sort: null == sort
          ? _value.sort
          : sort // ignore: cast_nullable_to_non_nullable
              as Sort,
      pageable: null == pageable
          ? _value.pageable
          : pageable // ignore: cast_nullable_to_non_nullable
              as Pageable,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $SortCopyWith<$Res> get sort {
    return $SortCopyWith<$Res>(_value.sort, (value) {
      return _then(_value.copyWith(sort: value) as $Val);
    });
  }

  @override
  @pragma('vm:prefer-inline')
  $PageableCopyWith<$Res> get pageable {
    return $PageableCopyWith<$Res>(_value.pageable, (value) {
      return _then(_value.copyWith(pageable: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$PagingImplCopyWith<T, $Res>
    implements $PagingCopyWith<T, $Res> {
  factory _$$PagingImplCopyWith(
          _$PagingImpl<T> value, $Res Function(_$PagingImpl<T>) then) =
      __$$PagingImplCopyWithImpl<T, $Res>;
  @override
  @useResult
  $Res call(
      {List<T> content,
      int totalPages,
      bool last,
      int totalElements,
      bool first,
      int numberOfElements,
      int number,
      int size,
      bool empty,
      Sort sort,
      Pageable pageable});

  @override
  $SortCopyWith<$Res> get sort;
  @override
  $PageableCopyWith<$Res> get pageable;
}

/// @nodoc
class __$$PagingImplCopyWithImpl<T, $Res>
    extends _$PagingCopyWithImpl<T, $Res, _$PagingImpl<T>>
    implements _$$PagingImplCopyWith<T, $Res> {
  __$$PagingImplCopyWithImpl(
      _$PagingImpl<T> _value, $Res Function(_$PagingImpl<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? content = null,
    Object? totalPages = null,
    Object? last = null,
    Object? totalElements = null,
    Object? first = null,
    Object? numberOfElements = null,
    Object? number = null,
    Object? size = null,
    Object? empty = null,
    Object? sort = null,
    Object? pageable = null,
  }) {
    return _then(_$PagingImpl<T>(
      content: null == content
          ? _value._content
          : content // ignore: cast_nullable_to_non_nullable
              as List<T>,
      totalPages: null == totalPages
          ? _value.totalPages
          : totalPages // ignore: cast_nullable_to_non_nullable
              as int,
      last: null == last
          ? _value.last
          : last // ignore: cast_nullable_to_non_nullable
              as bool,
      totalElements: null == totalElements
          ? _value.totalElements
          : totalElements // ignore: cast_nullable_to_non_nullable
              as int,
      first: null == first
          ? _value.first
          : first // ignore: cast_nullable_to_non_nullable
              as bool,
      numberOfElements: null == numberOfElements
          ? _value.numberOfElements
          : numberOfElements // ignore: cast_nullable_to_non_nullable
              as int,
      number: null == number
          ? _value.number
          : number // ignore: cast_nullable_to_non_nullable
              as int,
      size: null == size
          ? _value.size
          : size // ignore: cast_nullable_to_non_nullable
              as int,
      empty: null == empty
          ? _value.empty
          : empty // ignore: cast_nullable_to_non_nullable
              as bool,
      sort: null == sort
          ? _value.sort
          : sort // ignore: cast_nullable_to_non_nullable
              as Sort,
      pageable: null == pageable
          ? _value.pageable
          : pageable // ignore: cast_nullable_to_non_nullable
              as Pageable,
    ));
  }
}

/// @nodoc

class _$PagingImpl<T> extends _Paging<T> {
  const _$PagingImpl(
      {required final List<T> content,
      this.totalPages = Paging.defaultTotalPages,
      this.last = Paging.defaultLast,
      this.totalElements = Paging.defaultTotalElements,
      this.first = Paging.defaultFirst,
      this.numberOfElements = Paging.defaultNumberOfElements,
      this.number = Paging.defaultNumber,
      this.size = Paging.defaultSize,
      this.empty = Paging.defaultEmpty,
      this.sort = Paging.defaultSort,
      this.pageable = Paging.defaultPageable})
      : _content = content,
        super._();

  final List<T> _content;
  @override
  List<T> get content {
    if (_content is EqualUnmodifiableListView) return _content;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_content);
  }

  @override
  @JsonKey()
  final int totalPages;
  @override
  @JsonKey()
  final bool last;
  @override
  @JsonKey()
  final int totalElements;
  @override
  @JsonKey()
  final bool first;
  @override
  @JsonKey()
  final int numberOfElements;
  @override
  @JsonKey()
  final int number;
  @override
  @JsonKey()
  final int size;
  @override
  @JsonKey()
  final bool empty;
  @override
  @JsonKey()
  final Sort sort;
  @override
  @JsonKey()
  final Pageable pageable;

  @override
  String toString() {
    return 'Paging<$T>(content: $content, totalPages: $totalPages, last: $last, totalElements: $totalElements, first: $first, numberOfElements: $numberOfElements, number: $number, size: $size, empty: $empty, sort: $sort, pageable: $pageable)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$PagingImpl<T> &&
            const DeepCollectionEquality().equals(other._content, _content) &&
            (identical(other.totalPages, totalPages) ||
                other.totalPages == totalPages) &&
            (identical(other.last, last) || other.last == last) &&
            (identical(other.totalElements, totalElements) ||
                other.totalElements == totalElements) &&
            (identical(other.first, first) || other.first == first) &&
            (identical(other.numberOfElements, numberOfElements) ||
                other.numberOfElements == numberOfElements) &&
            (identical(other.number, number) || other.number == number) &&
            (identical(other.size, size) || other.size == size) &&
            (identical(other.empty, empty) || other.empty == empty) &&
            (identical(other.sort, sort) || other.sort == sort) &&
            (identical(other.pageable, pageable) ||
                other.pageable == pageable));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_content),
      totalPages,
      last,
      totalElements,
      first,
      numberOfElements,
      number,
      size,
      empty,
      sort,
      pageable);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$PagingImplCopyWith<T, _$PagingImpl<T>> get copyWith =>
      __$$PagingImplCopyWithImpl<T, _$PagingImpl<T>>(this, _$identity);
}

abstract class _Paging<T> extends Paging<T> {
  const factory _Paging(
      {required final List<T> content,
      final int totalPages,
      final bool last,
      final int totalElements,
      final bool first,
      final int numberOfElements,
      final int number,
      final int size,
      final bool empty,
      final Sort sort,
      final Pageable pageable}) = _$PagingImpl<T>;
  const _Paging._() : super._();

  @override
  List<T> get content;
  @override
  int get totalPages;
  @override
  bool get last;
  @override
  int get totalElements;
  @override
  bool get first;
  @override
  int get numberOfElements;
  @override
  int get number;
  @override
  int get size;
  @override
  bool get empty;
  @override
  Sort get sort;
  @override
  Pageable get pageable;
  @override
  @JsonKey(ignore: true)
  _$$PagingImplCopyWith<T, _$PagingImpl<T>> get copyWith =>
      throw _privateConstructorUsedError;
}
