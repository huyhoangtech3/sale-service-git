import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shared/shared.dart';

import 'package:domain/domain.dart';

part 'paging.freezed.dart';

@freezed
class Paging<T> with _$Paging<T> {
  const Paging._();
  const factory Paging({
    required List<T> content,
    @Default(Paging.defaultTotalPages) int totalPages,
    @Default(Paging.defaultLast) bool last,
    @Default(Paging.defaultTotalElements) int totalElements,
    @Default(Paging.defaultFirst) bool first,
    @Default(Paging.defaultNumberOfElements) int numberOfElements,
    @Default(Paging.defaultNumber) int number,
    @Default(Paging.defaultSize) int size,
    @Default(Paging.defaultEmpty) bool empty,
    @Default(Paging.defaultSort) Sort sort,
    @Default(Paging.defaultPageable) Pageable pageable,
  }) = _Paging;

  static const defaultTotalPages = 0;
  static const defaultLast = false;
  static const defaultTotalElements = 0;
  static const defaultFirst = false;
  static const defaultNumberOfElements = 0;
  static const defaultNumber = 0;
  static const defaultSize = 0;
  static const defaultEmpty = false;
  static const defaultSort = Sort();
  static const defaultPageable = Pageable();
}
