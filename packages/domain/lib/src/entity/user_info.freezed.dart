// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'user_info.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$UserInfo {
  String get id => throw _privateConstructorUsedError;
  String get code => throw _privateConstructorUsedError;
  String get hrsCode => throw _privateConstructorUsedError;
  String get username => throw _privateConstructorUsedError;
  String get fullName => throw _privateConstructorUsedError;
  String get branch => throw _privateConstructorUsedError;
  String get phoneNumber => throw _privateConstructorUsedError;
  String get email => throw _privateConstructorUsedError;
  String get note => throw _privateConstructorUsedError;
  String get image => throw _privateConstructorUsedError;
  String get branchName => throw _privateConstructorUsedError;
  String get phoneDevice => throw _privateConstructorUsedError;
  String get reason => throw _privateConstructorUsedError;
  String get gender => throw _privateConstructorUsedError;
  bool get unLock => throw _privateConstructorUsedError;
  String get statusWork => throw _privateConstructorUsedError;
  List<String> get blockRmManager => throw _privateConstructorUsedError;
  bool get domainAll => throw _privateConstructorUsedError;
  bool get adminRole => throw _privateConstructorUsedError;
  bool get active => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $UserInfoCopyWith<UserInfo> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserInfoCopyWith<$Res> {
  factory $UserInfoCopyWith(UserInfo value, $Res Function(UserInfo) then) =
      _$UserInfoCopyWithImpl<$Res, UserInfo>;
  @useResult
  $Res call(
      {String id,
      String code,
      String hrsCode,
      String username,
      String fullName,
      String branch,
      String phoneNumber,
      String email,
      String note,
      String image,
      String branchName,
      String phoneDevice,
      String reason,
      String gender,
      bool unLock,
      String statusWork,
      List<String> blockRmManager,
      bool domainAll,
      bool adminRole,
      bool active});
}

/// @nodoc
class _$UserInfoCopyWithImpl<$Res, $Val extends UserInfo>
    implements $UserInfoCopyWith<$Res> {
  _$UserInfoCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? code = null,
    Object? hrsCode = null,
    Object? username = null,
    Object? fullName = null,
    Object? branch = null,
    Object? phoneNumber = null,
    Object? email = null,
    Object? note = null,
    Object? image = null,
    Object? branchName = null,
    Object? phoneDevice = null,
    Object? reason = null,
    Object? gender = null,
    Object? unLock = null,
    Object? statusWork = null,
    Object? blockRmManager = null,
    Object? domainAll = null,
    Object? adminRole = null,
    Object? active = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      code: null == code
          ? _value.code
          : code // ignore: cast_nullable_to_non_nullable
              as String,
      hrsCode: null == hrsCode
          ? _value.hrsCode
          : hrsCode // ignore: cast_nullable_to_non_nullable
              as String,
      username: null == username
          ? _value.username
          : username // ignore: cast_nullable_to_non_nullable
              as String,
      fullName: null == fullName
          ? _value.fullName
          : fullName // ignore: cast_nullable_to_non_nullable
              as String,
      branch: null == branch
          ? _value.branch
          : branch // ignore: cast_nullable_to_non_nullable
              as String,
      phoneNumber: null == phoneNumber
          ? _value.phoneNumber
          : phoneNumber // ignore: cast_nullable_to_non_nullable
              as String,
      email: null == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      note: null == note
          ? _value.note
          : note // ignore: cast_nullable_to_non_nullable
              as String,
      image: null == image
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as String,
      branchName: null == branchName
          ? _value.branchName
          : branchName // ignore: cast_nullable_to_non_nullable
              as String,
      phoneDevice: null == phoneDevice
          ? _value.phoneDevice
          : phoneDevice // ignore: cast_nullable_to_non_nullable
              as String,
      reason: null == reason
          ? _value.reason
          : reason // ignore: cast_nullable_to_non_nullable
              as String,
      gender: null == gender
          ? _value.gender
          : gender // ignore: cast_nullable_to_non_nullable
              as String,
      unLock: null == unLock
          ? _value.unLock
          : unLock // ignore: cast_nullable_to_non_nullable
              as bool,
      statusWork: null == statusWork
          ? _value.statusWork
          : statusWork // ignore: cast_nullable_to_non_nullable
              as String,
      blockRmManager: null == blockRmManager
          ? _value.blockRmManager
          : blockRmManager // ignore: cast_nullable_to_non_nullable
              as List<String>,
      domainAll: null == domainAll
          ? _value.domainAll
          : domainAll // ignore: cast_nullable_to_non_nullable
              as bool,
      adminRole: null == adminRole
          ? _value.adminRole
          : adminRole // ignore: cast_nullable_to_non_nullable
              as bool,
      active: null == active
          ? _value.active
          : active // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$UserInfoImplCopyWith<$Res>
    implements $UserInfoCopyWith<$Res> {
  factory _$$UserInfoImplCopyWith(
          _$UserInfoImpl value, $Res Function(_$UserInfoImpl) then) =
      __$$UserInfoImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String id,
      String code,
      String hrsCode,
      String username,
      String fullName,
      String branch,
      String phoneNumber,
      String email,
      String note,
      String image,
      String branchName,
      String phoneDevice,
      String reason,
      String gender,
      bool unLock,
      String statusWork,
      List<String> blockRmManager,
      bool domainAll,
      bool adminRole,
      bool active});
}

/// @nodoc
class __$$UserInfoImplCopyWithImpl<$Res>
    extends _$UserInfoCopyWithImpl<$Res, _$UserInfoImpl>
    implements _$$UserInfoImplCopyWith<$Res> {
  __$$UserInfoImplCopyWithImpl(
      _$UserInfoImpl _value, $Res Function(_$UserInfoImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? code = null,
    Object? hrsCode = null,
    Object? username = null,
    Object? fullName = null,
    Object? branch = null,
    Object? phoneNumber = null,
    Object? email = null,
    Object? note = null,
    Object? image = null,
    Object? branchName = null,
    Object? phoneDevice = null,
    Object? reason = null,
    Object? gender = null,
    Object? unLock = null,
    Object? statusWork = null,
    Object? blockRmManager = null,
    Object? domainAll = null,
    Object? adminRole = null,
    Object? active = null,
  }) {
    return _then(_$UserInfoImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      code: null == code
          ? _value.code
          : code // ignore: cast_nullable_to_non_nullable
              as String,
      hrsCode: null == hrsCode
          ? _value.hrsCode
          : hrsCode // ignore: cast_nullable_to_non_nullable
              as String,
      username: null == username
          ? _value.username
          : username // ignore: cast_nullable_to_non_nullable
              as String,
      fullName: null == fullName
          ? _value.fullName
          : fullName // ignore: cast_nullable_to_non_nullable
              as String,
      branch: null == branch
          ? _value.branch
          : branch // ignore: cast_nullable_to_non_nullable
              as String,
      phoneNumber: null == phoneNumber
          ? _value.phoneNumber
          : phoneNumber // ignore: cast_nullable_to_non_nullable
              as String,
      email: null == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      note: null == note
          ? _value.note
          : note // ignore: cast_nullable_to_non_nullable
              as String,
      image: null == image
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as String,
      branchName: null == branchName
          ? _value.branchName
          : branchName // ignore: cast_nullable_to_non_nullable
              as String,
      phoneDevice: null == phoneDevice
          ? _value.phoneDevice
          : phoneDevice // ignore: cast_nullable_to_non_nullable
              as String,
      reason: null == reason
          ? _value.reason
          : reason // ignore: cast_nullable_to_non_nullable
              as String,
      gender: null == gender
          ? _value.gender
          : gender // ignore: cast_nullable_to_non_nullable
              as String,
      unLock: null == unLock
          ? _value.unLock
          : unLock // ignore: cast_nullable_to_non_nullable
              as bool,
      statusWork: null == statusWork
          ? _value.statusWork
          : statusWork // ignore: cast_nullable_to_non_nullable
              as String,
      blockRmManager: null == blockRmManager
          ? _value._blockRmManager
          : blockRmManager // ignore: cast_nullable_to_non_nullable
              as List<String>,
      domainAll: null == domainAll
          ? _value.domainAll
          : domainAll // ignore: cast_nullable_to_non_nullable
              as bool,
      adminRole: null == adminRole
          ? _value.adminRole
          : adminRole // ignore: cast_nullable_to_non_nullable
              as bool,
      active: null == active
          ? _value.active
          : active // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$UserInfoImpl implements _UserInfo {
  const _$UserInfoImpl(
      {this.id = UserInfo.defaultId,
      this.code = UserInfo.defaultCode,
      this.hrsCode = UserInfo.defaultHrsCode,
      this.username = UserInfo.defaultUsername,
      this.fullName = UserInfo.defaultFullName,
      this.branch = UserInfo.defaultBranch,
      this.phoneNumber = UserInfo.defaultPhoneNumber,
      this.email = UserInfo.defaultEmail,
      this.note = UserInfo.defaultNote,
      this.image = UserInfo.defaultImage,
      this.branchName = UserInfo.defaultBranchName,
      this.phoneDevice = UserInfo.defaultPhoneDevice,
      this.reason = UserInfo.defaultReason,
      this.gender = UserInfo.defaultGender,
      this.unLock = UserInfo.defaultUnLock,
      this.statusWork = UserInfo.defaultStatusWork,
      final List<String> blockRmManager = UserInfo.defaultBlockRmManager,
      this.domainAll = UserInfo.defaultDomainAll,
      this.adminRole = UserInfo.defaultAdminRole,
      this.active = UserInfo.defaultActive})
      : _blockRmManager = blockRmManager;

  @override
  @JsonKey()
  final String id;
  @override
  @JsonKey()
  final String code;
  @override
  @JsonKey()
  final String hrsCode;
  @override
  @JsonKey()
  final String username;
  @override
  @JsonKey()
  final String fullName;
  @override
  @JsonKey()
  final String branch;
  @override
  @JsonKey()
  final String phoneNumber;
  @override
  @JsonKey()
  final String email;
  @override
  @JsonKey()
  final String note;
  @override
  @JsonKey()
  final String image;
  @override
  @JsonKey()
  final String branchName;
  @override
  @JsonKey()
  final String phoneDevice;
  @override
  @JsonKey()
  final String reason;
  @override
  @JsonKey()
  final String gender;
  @override
  @JsonKey()
  final bool unLock;
  @override
  @JsonKey()
  final String statusWork;
  final List<String> _blockRmManager;
  @override
  @JsonKey()
  List<String> get blockRmManager {
    if (_blockRmManager is EqualUnmodifiableListView) return _blockRmManager;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_blockRmManager);
  }

  @override
  @JsonKey()
  final bool domainAll;
  @override
  @JsonKey()
  final bool adminRole;
  @override
  @JsonKey()
  final bool active;

  @override
  String toString() {
    return 'UserInfo(id: $id, code: $code, hrsCode: $hrsCode, username: $username, fullName: $fullName, branch: $branch, phoneNumber: $phoneNumber, email: $email, note: $note, image: $image, branchName: $branchName, phoneDevice: $phoneDevice, reason: $reason, gender: $gender, unLock: $unLock, statusWork: $statusWork, blockRmManager: $blockRmManager, domainAll: $domainAll, adminRole: $adminRole, active: $active)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UserInfoImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.code, code) || other.code == code) &&
            (identical(other.hrsCode, hrsCode) || other.hrsCode == hrsCode) &&
            (identical(other.username, username) ||
                other.username == username) &&
            (identical(other.fullName, fullName) ||
                other.fullName == fullName) &&
            (identical(other.branch, branch) || other.branch == branch) &&
            (identical(other.phoneNumber, phoneNumber) ||
                other.phoneNumber == phoneNumber) &&
            (identical(other.email, email) || other.email == email) &&
            (identical(other.note, note) || other.note == note) &&
            (identical(other.image, image) || other.image == image) &&
            (identical(other.branchName, branchName) ||
                other.branchName == branchName) &&
            (identical(other.phoneDevice, phoneDevice) ||
                other.phoneDevice == phoneDevice) &&
            (identical(other.reason, reason) || other.reason == reason) &&
            (identical(other.gender, gender) || other.gender == gender) &&
            (identical(other.unLock, unLock) || other.unLock == unLock) &&
            (identical(other.statusWork, statusWork) ||
                other.statusWork == statusWork) &&
            const DeepCollectionEquality()
                .equals(other._blockRmManager, _blockRmManager) &&
            (identical(other.domainAll, domainAll) ||
                other.domainAll == domainAll) &&
            (identical(other.adminRole, adminRole) ||
                other.adminRole == adminRole) &&
            (identical(other.active, active) || other.active == active));
  }

  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        id,
        code,
        hrsCode,
        username,
        fullName,
        branch,
        phoneNumber,
        email,
        note,
        image,
        branchName,
        phoneDevice,
        reason,
        gender,
        unLock,
        statusWork,
        const DeepCollectionEquality().hash(_blockRmManager),
        domainAll,
        adminRole,
        active
      ]);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$UserInfoImplCopyWith<_$UserInfoImpl> get copyWith =>
      __$$UserInfoImplCopyWithImpl<_$UserInfoImpl>(this, _$identity);
}

abstract class _UserInfo implements UserInfo {
  const factory _UserInfo(
      {final String id,
      final String code,
      final String hrsCode,
      final String username,
      final String fullName,
      final String branch,
      final String phoneNumber,
      final String email,
      final String note,
      final String image,
      final String branchName,
      final String phoneDevice,
      final String reason,
      final String gender,
      final bool unLock,
      final String statusWork,
      final List<String> blockRmManager,
      final bool domainAll,
      final bool adminRole,
      final bool active}) = _$UserInfoImpl;

  @override
  String get id;
  @override
  String get code;
  @override
  String get hrsCode;
  @override
  String get username;
  @override
  String get fullName;
  @override
  String get branch;
  @override
  String get phoneNumber;
  @override
  String get email;
  @override
  String get note;
  @override
  String get image;
  @override
  String get branchName;
  @override
  String get phoneDevice;
  @override
  String get reason;
  @override
  String get gender;
  @override
  bool get unLock;
  @override
  String get statusWork;
  @override
  List<String> get blockRmManager;
  @override
  bool get domainAll;
  @override
  bool get adminRole;
  @override
  bool get active;
  @override
  @JsonKey(ignore: true)
  _$$UserInfoImplCopyWith<_$UserInfoImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
