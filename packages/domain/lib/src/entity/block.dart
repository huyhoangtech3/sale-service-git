import 'dart:ffi';

import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shared/shared.dart';

import 'package:domain/domain.dart';

part 'block.freezed.dart';

@freezed
class Block with _$Block {
  const factory Block({
    @Default(Block.defaultId) String id,
    @Default(Block.defaultCode) String code,
    @Default(Block.defaultName) String name,
    @Default(Block.defaultIsActive) bool isActive,
    @Default(Block.defaultCustomerType) String customerType,
    @Default(Block.defaultCreatedDate) String createdDate,
    @Default(Block.defaultCreatedBy) String createdBy,
    @Default(Block.defaultUpdatedDate) String updatedDate,
    @Default(Block.defaultUpdatedBy) String updatedBy,
  }) = _Block;

  static const defaultId = '';
  static const defaultCode = '';
  static const defaultName = '';
  static const defaultIsActive = false;
  static const defaultCustomerType = '';
  static const defaultCreatedDate = '';
  static const defaultCreatedBy = '';
  static const defaultUpdatedDate = '';
  static const defaultUpdatedBy = '';
}
