import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shared/shared.dart';

import 'package:domain/domain.dart';

part 'sort.freezed.dart';

@freezed
class Sort with _$Sort {
  const factory Sort({
    @Default(Sort.defaultUnsorted) bool unsorted,
    @Default(Sort.defaultSorted) bool sorted,
    @Default(Sort.defaultEmpty) bool empty,
  }) = _Sort;

  static const defaultUnsorted = false;
  static const defaultSorted = false;
  static const defaultEmpty = false;
}
