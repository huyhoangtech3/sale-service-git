import 'dart:ffi';

import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shared/shared.dart';

import 'package:domain/domain.dart';

part 'branch.freezed.dart';

@freezed
class Branch with _$Branch {
  const factory Branch({
    @Default(Branch.defaultCode) String code,
    @Default(Branch.defaultName) String name,
    @Default(Branch.defaultRegion) String region,
    @Default(Branch.defaultDescription) String description,
  }) = _Branch;

  static const defaultCode = '';
  static const defaultName = '';
  static const defaultRegion = '';
  static const defaultDescription = '';
}
