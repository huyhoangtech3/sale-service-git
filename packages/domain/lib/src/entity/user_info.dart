import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shared/shared.dart';

import 'package:domain/domain.dart';

part 'user_info.freezed.dart';

@freezed
class UserInfo with _$UserInfo {
  const factory UserInfo({
    @Default(UserInfo.defaultId) String id,
    @Default(UserInfo.defaultCode) String code,
    @Default(UserInfo.defaultHrsCode) String hrsCode,
    @Default(UserInfo.defaultUsername) String username,
    @Default(UserInfo.defaultFullName) String fullName,
    @Default(UserInfo.defaultBranch) String branch,
    @Default(UserInfo.defaultPhoneNumber) String phoneNumber,
    @Default(UserInfo.defaultEmail) String email,
    @Default(UserInfo.defaultNote) String note,
    @Default(UserInfo.defaultImage) String image,
    @Default(UserInfo.defaultBranchName) String branchName,
    @Default(UserInfo.defaultPhoneDevice) String phoneDevice,
    @Default(UserInfo.defaultReason) String reason,
    @Default(UserInfo.defaultGender) String gender,
    @Default(UserInfo.defaultUnLock) bool unLock,
    @Default(UserInfo.defaultStatusWork) String statusWork,
    @Default(UserInfo.defaultBlockRmManager) List<String> blockRmManager,
    @Default(UserInfo.defaultDomainAll) bool domainAll,
    @Default(UserInfo.defaultAdminRole) bool adminRole,
    @Default(UserInfo.defaultActive) bool active,
  }) = _UserInfo;

  static const defaultId = "";
  static const defaultCode = "";
  static const defaultHrsCode = "";
  static const defaultUsername = "";
  static const defaultFullName = "";
  static const defaultBranch = "";
  static const defaultPhoneNumber = "";
  static const defaultEmail = "";
  static const defaultNote = "";
  static const defaultImage = "";
  static const defaultBranchName = "";
  static const defaultPhoneDevice = "";
  static const defaultReason = "";
  static const defaultGender = "";
  static const defaultUnLock = false;
  static const defaultStatusWork = "";
  static const defaultBlockRmManager = <String>[];
  static const defaultDomainAll = false;
  static const defaultAdminRole = false;
  static const defaultActive = false;
}
