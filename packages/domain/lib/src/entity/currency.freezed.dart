// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'currency.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$Currency {
  String get currency => throw _privateConstructorUsedError;
  int get itbBuy => throw _privateConstructorUsedError;
  int get itbSell => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CurrencyCopyWith<Currency> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CurrencyCopyWith<$Res> {
  factory $CurrencyCopyWith(Currency value, $Res Function(Currency) then) =
      _$CurrencyCopyWithImpl<$Res, Currency>;
  @useResult
  $Res call({String currency, int itbBuy, int itbSell});
}

/// @nodoc
class _$CurrencyCopyWithImpl<$Res, $Val extends Currency>
    implements $CurrencyCopyWith<$Res> {
  _$CurrencyCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? currency = null,
    Object? itbBuy = null,
    Object? itbSell = null,
  }) {
    return _then(_value.copyWith(
      currency: null == currency
          ? _value.currency
          : currency // ignore: cast_nullable_to_non_nullable
              as String,
      itbBuy: null == itbBuy
          ? _value.itbBuy
          : itbBuy // ignore: cast_nullable_to_non_nullable
              as int,
      itbSell: null == itbSell
          ? _value.itbSell
          : itbSell // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$CurrencyImplCopyWith<$Res>
    implements $CurrencyCopyWith<$Res> {
  factory _$$CurrencyImplCopyWith(
          _$CurrencyImpl value, $Res Function(_$CurrencyImpl) then) =
      __$$CurrencyImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String currency, int itbBuy, int itbSell});
}

/// @nodoc
class __$$CurrencyImplCopyWithImpl<$Res>
    extends _$CurrencyCopyWithImpl<$Res, _$CurrencyImpl>
    implements _$$CurrencyImplCopyWith<$Res> {
  __$$CurrencyImplCopyWithImpl(
      _$CurrencyImpl _value, $Res Function(_$CurrencyImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? currency = null,
    Object? itbBuy = null,
    Object? itbSell = null,
  }) {
    return _then(_$CurrencyImpl(
      currency: null == currency
          ? _value.currency
          : currency // ignore: cast_nullable_to_non_nullable
              as String,
      itbBuy: null == itbBuy
          ? _value.itbBuy
          : itbBuy // ignore: cast_nullable_to_non_nullable
              as int,
      itbSell: null == itbSell
          ? _value.itbSell
          : itbSell // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$CurrencyImpl implements _Currency {
  const _$CurrencyImpl(
      {this.currency = Currency.defaultCurrency,
      this.itbBuy = Currency.defaultItbBuy,
      this.itbSell = Currency.defaultItbSell});

  @override
  @JsonKey()
  final String currency;
  @override
  @JsonKey()
  final int itbBuy;
  @override
  @JsonKey()
  final int itbSell;

  @override
  String toString() {
    return 'Currency(currency: $currency, itbBuy: $itbBuy, itbSell: $itbSell)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CurrencyImpl &&
            (identical(other.currency, currency) ||
                other.currency == currency) &&
            (identical(other.itbBuy, itbBuy) || other.itbBuy == itbBuy) &&
            (identical(other.itbSell, itbSell) || other.itbSell == itbSell));
  }

  @override
  int get hashCode => Object.hash(runtimeType, currency, itbBuy, itbSell);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CurrencyImplCopyWith<_$CurrencyImpl> get copyWith =>
      __$$CurrencyImplCopyWithImpl<_$CurrencyImpl>(this, _$identity);
}

abstract class _Currency implements Currency {
  const factory _Currency(
      {final String currency,
      final int itbBuy,
      final int itbSell}) = _$CurrencyImpl;

  @override
  String get currency;
  @override
  int get itbBuy;
  @override
  int get itbSell;
  @override
  @JsonKey(ignore: true)
  _$$CurrencyImplCopyWith<_$CurrencyImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
