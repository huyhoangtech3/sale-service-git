import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shared/shared.dart';

import 'package:domain/domain.dart';

part 'customer.freezed.dart';

@freezed
class Customer with _$Customer {
  const factory Customer({
    @Default(Customer.defaultCustomerCode) String customerCode,
    @Default(Customer.defaultAddress) String address,
    @Default(Customer.defaultBranchCode) String branchCode,
    @Default(Customer.defaultBusinessRegistrationNumber) String businessRegistrationNumber,
    @Default(Customer.defaultCustomerName) String customerName,
    @Default(Customer.defaultClassification) String classification,
    @Default(Customer.defaultCustomerType) String customerType,
    @Default(Customer.defaultDateOfBirth) String dateOfBirth,
    @Default(Customer.defaultDistrict) String district,
    @Default(Customer.defaultGender) String gender,
    @Default(Customer.defaultIdCard) String idCard,
    @Default(Customer.defaultIndustry) String industry,
    @Default(Customer.defaultNationCode) String nationCode,
    @Default(Customer.defaultProvince) String province,
    @Default(Customer.defaultSector) String sector,
    @Default(Customer.defaultSegment) String segment,
    @Default(Customer.defaultStatus) String status,
    @Default(Customer.defaultSwiftCode) String swiftCode,
    @Default(Customer.defaultTaxCode) String taxCode,
    @Default(Customer.defaultTransactionFrequency) String transactionFrequency,
    @Default(Customer.defaultCustomerTypeSector) String customerTypeSector,
    @Default(Customer.defaultCustomerTypeMerge) String customerTypeMerge,
    @Default(Customer.defaultPhoneT24) String phoneT24,
    @Default(Customer.defaultPhoneEmb) String phoneEmb,
    @Default(Customer.defaultPhoneSms) String phoneSms,
    @Default(Customer.defaultPhoneWay4) String phoneWay4,
    @Default(Customer.defaultEmailT24) String emailT24,
    @Default(Customer.defaultEmailEmb) String emailEmb,
    @Default(Customer.defaultEmailWay4) String emailWay4,
    @Default(Customer.defaultAccountOfficer) String accountOfficer,
    @Default(Customer.defaultCreatedBy) String createdBy,
    @Default(Customer.defaultUpdatedBy) String updatedBy,
  }) = _Customer;

  static const defaultCustomerCode = '';
  static const defaultAddress = '';
  static const defaultBranchCode = '';
  static const defaultBusinessRegistrationNumber = '';
  static const defaultCustomerName = '';
  static const defaultClassification = '';
  static const defaultCustomerType = '';
  static const defaultDateOfBirth = '';
  static const defaultDistrict = '';
  static const defaultGender = '';
  static const defaultIdCard = '';
  static const defaultIndustry = '';
  static const defaultNationCode = '';
  static const defaultProvince = '';
  static const defaultSector = '';
  static const defaultSegment = '';
  static const defaultStatus = '';
  static const defaultSwiftCode = '';
  static const defaultTaxCode = '';
  static const defaultTransactionFrequency = '';
  static const defaultCustomerTypeSector = '';
  static const defaultCustomerTypeMerge = '';
  static const defaultPhoneT24 = '';
  static const defaultPhoneEmb = '';
  static const defaultPhoneSms = '';
  static const defaultPhoneWay4 = '';
  static const defaultEmailT24 = '';
  static const defaultEmailEmb = '';
  static const defaultEmailWay4 = '';
  static const defaultAccountOfficer = '';
  static const defaultCreatedBy = '';
  static const defaultUpdatedBy = '';
}
