// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'customer.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$Customer {
  String get customerCode => throw _privateConstructorUsedError;
  String get address => throw _privateConstructorUsedError;
  String get branchCode => throw _privateConstructorUsedError;
  String get businessRegistrationNumber => throw _privateConstructorUsedError;
  String get customerName => throw _privateConstructorUsedError;
  String get classification => throw _privateConstructorUsedError;
  String get customerType => throw _privateConstructorUsedError;
  String get dateOfBirth => throw _privateConstructorUsedError;
  String get district => throw _privateConstructorUsedError;
  String get gender => throw _privateConstructorUsedError;
  String get idCard => throw _privateConstructorUsedError;
  String get industry => throw _privateConstructorUsedError;
  String get nationCode => throw _privateConstructorUsedError;
  String get province => throw _privateConstructorUsedError;
  String get sector => throw _privateConstructorUsedError;
  String get segment => throw _privateConstructorUsedError;
  String get status => throw _privateConstructorUsedError;
  String get swiftCode => throw _privateConstructorUsedError;
  String get taxCode => throw _privateConstructorUsedError;
  String get transactionFrequency => throw _privateConstructorUsedError;
  String get customerTypeSector => throw _privateConstructorUsedError;
  String get customerTypeMerge => throw _privateConstructorUsedError;
  String get phoneT24 => throw _privateConstructorUsedError;
  String get phoneEmb => throw _privateConstructorUsedError;
  String get phoneSms => throw _privateConstructorUsedError;
  String get phoneWay4 => throw _privateConstructorUsedError;
  String get emailT24 => throw _privateConstructorUsedError;
  String get emailEmb => throw _privateConstructorUsedError;
  String get emailWay4 => throw _privateConstructorUsedError;
  String get accountOfficer => throw _privateConstructorUsedError;
  String get createdBy => throw _privateConstructorUsedError;
  String get updatedBy => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CustomerCopyWith<Customer> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CustomerCopyWith<$Res> {
  factory $CustomerCopyWith(Customer value, $Res Function(Customer) then) =
      _$CustomerCopyWithImpl<$Res, Customer>;
  @useResult
  $Res call(
      {String customerCode,
      String address,
      String branchCode,
      String businessRegistrationNumber,
      String customerName,
      String classification,
      String customerType,
      String dateOfBirth,
      String district,
      String gender,
      String idCard,
      String industry,
      String nationCode,
      String province,
      String sector,
      String segment,
      String status,
      String swiftCode,
      String taxCode,
      String transactionFrequency,
      String customerTypeSector,
      String customerTypeMerge,
      String phoneT24,
      String phoneEmb,
      String phoneSms,
      String phoneWay4,
      String emailT24,
      String emailEmb,
      String emailWay4,
      String accountOfficer,
      String createdBy,
      String updatedBy});
}

/// @nodoc
class _$CustomerCopyWithImpl<$Res, $Val extends Customer>
    implements $CustomerCopyWith<$Res> {
  _$CustomerCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? customerCode = null,
    Object? address = null,
    Object? branchCode = null,
    Object? businessRegistrationNumber = null,
    Object? customerName = null,
    Object? classification = null,
    Object? customerType = null,
    Object? dateOfBirth = null,
    Object? district = null,
    Object? gender = null,
    Object? idCard = null,
    Object? industry = null,
    Object? nationCode = null,
    Object? province = null,
    Object? sector = null,
    Object? segment = null,
    Object? status = null,
    Object? swiftCode = null,
    Object? taxCode = null,
    Object? transactionFrequency = null,
    Object? customerTypeSector = null,
    Object? customerTypeMerge = null,
    Object? phoneT24 = null,
    Object? phoneEmb = null,
    Object? phoneSms = null,
    Object? phoneWay4 = null,
    Object? emailT24 = null,
    Object? emailEmb = null,
    Object? emailWay4 = null,
    Object? accountOfficer = null,
    Object? createdBy = null,
    Object? updatedBy = null,
  }) {
    return _then(_value.copyWith(
      customerCode: null == customerCode
          ? _value.customerCode
          : customerCode // ignore: cast_nullable_to_non_nullable
              as String,
      address: null == address
          ? _value.address
          : address // ignore: cast_nullable_to_non_nullable
              as String,
      branchCode: null == branchCode
          ? _value.branchCode
          : branchCode // ignore: cast_nullable_to_non_nullable
              as String,
      businessRegistrationNumber: null == businessRegistrationNumber
          ? _value.businessRegistrationNumber
          : businessRegistrationNumber // ignore: cast_nullable_to_non_nullable
              as String,
      customerName: null == customerName
          ? _value.customerName
          : customerName // ignore: cast_nullable_to_non_nullable
              as String,
      classification: null == classification
          ? _value.classification
          : classification // ignore: cast_nullable_to_non_nullable
              as String,
      customerType: null == customerType
          ? _value.customerType
          : customerType // ignore: cast_nullable_to_non_nullable
              as String,
      dateOfBirth: null == dateOfBirth
          ? _value.dateOfBirth
          : dateOfBirth // ignore: cast_nullable_to_non_nullable
              as String,
      district: null == district
          ? _value.district
          : district // ignore: cast_nullable_to_non_nullable
              as String,
      gender: null == gender
          ? _value.gender
          : gender // ignore: cast_nullable_to_non_nullable
              as String,
      idCard: null == idCard
          ? _value.idCard
          : idCard // ignore: cast_nullable_to_non_nullable
              as String,
      industry: null == industry
          ? _value.industry
          : industry // ignore: cast_nullable_to_non_nullable
              as String,
      nationCode: null == nationCode
          ? _value.nationCode
          : nationCode // ignore: cast_nullable_to_non_nullable
              as String,
      province: null == province
          ? _value.province
          : province // ignore: cast_nullable_to_non_nullable
              as String,
      sector: null == sector
          ? _value.sector
          : sector // ignore: cast_nullable_to_non_nullable
              as String,
      segment: null == segment
          ? _value.segment
          : segment // ignore: cast_nullable_to_non_nullable
              as String,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as String,
      swiftCode: null == swiftCode
          ? _value.swiftCode
          : swiftCode // ignore: cast_nullable_to_non_nullable
              as String,
      taxCode: null == taxCode
          ? _value.taxCode
          : taxCode // ignore: cast_nullable_to_non_nullable
              as String,
      transactionFrequency: null == transactionFrequency
          ? _value.transactionFrequency
          : transactionFrequency // ignore: cast_nullable_to_non_nullable
              as String,
      customerTypeSector: null == customerTypeSector
          ? _value.customerTypeSector
          : customerTypeSector // ignore: cast_nullable_to_non_nullable
              as String,
      customerTypeMerge: null == customerTypeMerge
          ? _value.customerTypeMerge
          : customerTypeMerge // ignore: cast_nullable_to_non_nullable
              as String,
      phoneT24: null == phoneT24
          ? _value.phoneT24
          : phoneT24 // ignore: cast_nullable_to_non_nullable
              as String,
      phoneEmb: null == phoneEmb
          ? _value.phoneEmb
          : phoneEmb // ignore: cast_nullable_to_non_nullable
              as String,
      phoneSms: null == phoneSms
          ? _value.phoneSms
          : phoneSms // ignore: cast_nullable_to_non_nullable
              as String,
      phoneWay4: null == phoneWay4
          ? _value.phoneWay4
          : phoneWay4 // ignore: cast_nullable_to_non_nullable
              as String,
      emailT24: null == emailT24
          ? _value.emailT24
          : emailT24 // ignore: cast_nullable_to_non_nullable
              as String,
      emailEmb: null == emailEmb
          ? _value.emailEmb
          : emailEmb // ignore: cast_nullable_to_non_nullable
              as String,
      emailWay4: null == emailWay4
          ? _value.emailWay4
          : emailWay4 // ignore: cast_nullable_to_non_nullable
              as String,
      accountOfficer: null == accountOfficer
          ? _value.accountOfficer
          : accountOfficer // ignore: cast_nullable_to_non_nullable
              as String,
      createdBy: null == createdBy
          ? _value.createdBy
          : createdBy // ignore: cast_nullable_to_non_nullable
              as String,
      updatedBy: null == updatedBy
          ? _value.updatedBy
          : updatedBy // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$CustomerImplCopyWith<$Res>
    implements $CustomerCopyWith<$Res> {
  factory _$$CustomerImplCopyWith(
          _$CustomerImpl value, $Res Function(_$CustomerImpl) then) =
      __$$CustomerImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String customerCode,
      String address,
      String branchCode,
      String businessRegistrationNumber,
      String customerName,
      String classification,
      String customerType,
      String dateOfBirth,
      String district,
      String gender,
      String idCard,
      String industry,
      String nationCode,
      String province,
      String sector,
      String segment,
      String status,
      String swiftCode,
      String taxCode,
      String transactionFrequency,
      String customerTypeSector,
      String customerTypeMerge,
      String phoneT24,
      String phoneEmb,
      String phoneSms,
      String phoneWay4,
      String emailT24,
      String emailEmb,
      String emailWay4,
      String accountOfficer,
      String createdBy,
      String updatedBy});
}

/// @nodoc
class __$$CustomerImplCopyWithImpl<$Res>
    extends _$CustomerCopyWithImpl<$Res, _$CustomerImpl>
    implements _$$CustomerImplCopyWith<$Res> {
  __$$CustomerImplCopyWithImpl(
      _$CustomerImpl _value, $Res Function(_$CustomerImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? customerCode = null,
    Object? address = null,
    Object? branchCode = null,
    Object? businessRegistrationNumber = null,
    Object? customerName = null,
    Object? classification = null,
    Object? customerType = null,
    Object? dateOfBirth = null,
    Object? district = null,
    Object? gender = null,
    Object? idCard = null,
    Object? industry = null,
    Object? nationCode = null,
    Object? province = null,
    Object? sector = null,
    Object? segment = null,
    Object? status = null,
    Object? swiftCode = null,
    Object? taxCode = null,
    Object? transactionFrequency = null,
    Object? customerTypeSector = null,
    Object? customerTypeMerge = null,
    Object? phoneT24 = null,
    Object? phoneEmb = null,
    Object? phoneSms = null,
    Object? phoneWay4 = null,
    Object? emailT24 = null,
    Object? emailEmb = null,
    Object? emailWay4 = null,
    Object? accountOfficer = null,
    Object? createdBy = null,
    Object? updatedBy = null,
  }) {
    return _then(_$CustomerImpl(
      customerCode: null == customerCode
          ? _value.customerCode
          : customerCode // ignore: cast_nullable_to_non_nullable
              as String,
      address: null == address
          ? _value.address
          : address // ignore: cast_nullable_to_non_nullable
              as String,
      branchCode: null == branchCode
          ? _value.branchCode
          : branchCode // ignore: cast_nullable_to_non_nullable
              as String,
      businessRegistrationNumber: null == businessRegistrationNumber
          ? _value.businessRegistrationNumber
          : businessRegistrationNumber // ignore: cast_nullable_to_non_nullable
              as String,
      customerName: null == customerName
          ? _value.customerName
          : customerName // ignore: cast_nullable_to_non_nullable
              as String,
      classification: null == classification
          ? _value.classification
          : classification // ignore: cast_nullable_to_non_nullable
              as String,
      customerType: null == customerType
          ? _value.customerType
          : customerType // ignore: cast_nullable_to_non_nullable
              as String,
      dateOfBirth: null == dateOfBirth
          ? _value.dateOfBirth
          : dateOfBirth // ignore: cast_nullable_to_non_nullable
              as String,
      district: null == district
          ? _value.district
          : district // ignore: cast_nullable_to_non_nullable
              as String,
      gender: null == gender
          ? _value.gender
          : gender // ignore: cast_nullable_to_non_nullable
              as String,
      idCard: null == idCard
          ? _value.idCard
          : idCard // ignore: cast_nullable_to_non_nullable
              as String,
      industry: null == industry
          ? _value.industry
          : industry // ignore: cast_nullable_to_non_nullable
              as String,
      nationCode: null == nationCode
          ? _value.nationCode
          : nationCode // ignore: cast_nullable_to_non_nullable
              as String,
      province: null == province
          ? _value.province
          : province // ignore: cast_nullable_to_non_nullable
              as String,
      sector: null == sector
          ? _value.sector
          : sector // ignore: cast_nullable_to_non_nullable
              as String,
      segment: null == segment
          ? _value.segment
          : segment // ignore: cast_nullable_to_non_nullable
              as String,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as String,
      swiftCode: null == swiftCode
          ? _value.swiftCode
          : swiftCode // ignore: cast_nullable_to_non_nullable
              as String,
      taxCode: null == taxCode
          ? _value.taxCode
          : taxCode // ignore: cast_nullable_to_non_nullable
              as String,
      transactionFrequency: null == transactionFrequency
          ? _value.transactionFrequency
          : transactionFrequency // ignore: cast_nullable_to_non_nullable
              as String,
      customerTypeSector: null == customerTypeSector
          ? _value.customerTypeSector
          : customerTypeSector // ignore: cast_nullable_to_non_nullable
              as String,
      customerTypeMerge: null == customerTypeMerge
          ? _value.customerTypeMerge
          : customerTypeMerge // ignore: cast_nullable_to_non_nullable
              as String,
      phoneT24: null == phoneT24
          ? _value.phoneT24
          : phoneT24 // ignore: cast_nullable_to_non_nullable
              as String,
      phoneEmb: null == phoneEmb
          ? _value.phoneEmb
          : phoneEmb // ignore: cast_nullable_to_non_nullable
              as String,
      phoneSms: null == phoneSms
          ? _value.phoneSms
          : phoneSms // ignore: cast_nullable_to_non_nullable
              as String,
      phoneWay4: null == phoneWay4
          ? _value.phoneWay4
          : phoneWay4 // ignore: cast_nullable_to_non_nullable
              as String,
      emailT24: null == emailT24
          ? _value.emailT24
          : emailT24 // ignore: cast_nullable_to_non_nullable
              as String,
      emailEmb: null == emailEmb
          ? _value.emailEmb
          : emailEmb // ignore: cast_nullable_to_non_nullable
              as String,
      emailWay4: null == emailWay4
          ? _value.emailWay4
          : emailWay4 // ignore: cast_nullable_to_non_nullable
              as String,
      accountOfficer: null == accountOfficer
          ? _value.accountOfficer
          : accountOfficer // ignore: cast_nullable_to_non_nullable
              as String,
      createdBy: null == createdBy
          ? _value.createdBy
          : createdBy // ignore: cast_nullable_to_non_nullable
              as String,
      updatedBy: null == updatedBy
          ? _value.updatedBy
          : updatedBy // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$CustomerImpl implements _Customer {
  const _$CustomerImpl(
      {this.customerCode = Customer.defaultCustomerCode,
      this.address = Customer.defaultAddress,
      this.branchCode = Customer.defaultBranchCode,
      this.businessRegistrationNumber =
          Customer.defaultBusinessRegistrationNumber,
      this.customerName = Customer.defaultCustomerName,
      this.classification = Customer.defaultClassification,
      this.customerType = Customer.defaultCustomerType,
      this.dateOfBirth = Customer.defaultDateOfBirth,
      this.district = Customer.defaultDistrict,
      this.gender = Customer.defaultGender,
      this.idCard = Customer.defaultIdCard,
      this.industry = Customer.defaultIndustry,
      this.nationCode = Customer.defaultNationCode,
      this.province = Customer.defaultProvince,
      this.sector = Customer.defaultSector,
      this.segment = Customer.defaultSegment,
      this.status = Customer.defaultStatus,
      this.swiftCode = Customer.defaultSwiftCode,
      this.taxCode = Customer.defaultTaxCode,
      this.transactionFrequency = Customer.defaultTransactionFrequency,
      this.customerTypeSector = Customer.defaultCustomerTypeSector,
      this.customerTypeMerge = Customer.defaultCustomerTypeMerge,
      this.phoneT24 = Customer.defaultPhoneT24,
      this.phoneEmb = Customer.defaultPhoneEmb,
      this.phoneSms = Customer.defaultPhoneSms,
      this.phoneWay4 = Customer.defaultPhoneWay4,
      this.emailT24 = Customer.defaultEmailT24,
      this.emailEmb = Customer.defaultEmailEmb,
      this.emailWay4 = Customer.defaultEmailWay4,
      this.accountOfficer = Customer.defaultAccountOfficer,
      this.createdBy = Customer.defaultCreatedBy,
      this.updatedBy = Customer.defaultUpdatedBy});

  @override
  @JsonKey()
  final String customerCode;
  @override
  @JsonKey()
  final String address;
  @override
  @JsonKey()
  final String branchCode;
  @override
  @JsonKey()
  final String businessRegistrationNumber;
  @override
  @JsonKey()
  final String customerName;
  @override
  @JsonKey()
  final String classification;
  @override
  @JsonKey()
  final String customerType;
  @override
  @JsonKey()
  final String dateOfBirth;
  @override
  @JsonKey()
  final String district;
  @override
  @JsonKey()
  final String gender;
  @override
  @JsonKey()
  final String idCard;
  @override
  @JsonKey()
  final String industry;
  @override
  @JsonKey()
  final String nationCode;
  @override
  @JsonKey()
  final String province;
  @override
  @JsonKey()
  final String sector;
  @override
  @JsonKey()
  final String segment;
  @override
  @JsonKey()
  final String status;
  @override
  @JsonKey()
  final String swiftCode;
  @override
  @JsonKey()
  final String taxCode;
  @override
  @JsonKey()
  final String transactionFrequency;
  @override
  @JsonKey()
  final String customerTypeSector;
  @override
  @JsonKey()
  final String customerTypeMerge;
  @override
  @JsonKey()
  final String phoneT24;
  @override
  @JsonKey()
  final String phoneEmb;
  @override
  @JsonKey()
  final String phoneSms;
  @override
  @JsonKey()
  final String phoneWay4;
  @override
  @JsonKey()
  final String emailT24;
  @override
  @JsonKey()
  final String emailEmb;
  @override
  @JsonKey()
  final String emailWay4;
  @override
  @JsonKey()
  final String accountOfficer;
  @override
  @JsonKey()
  final String createdBy;
  @override
  @JsonKey()
  final String updatedBy;

  @override
  String toString() {
    return 'Customer(customerCode: $customerCode, address: $address, branchCode: $branchCode, businessRegistrationNumber: $businessRegistrationNumber, customerName: $customerName, classification: $classification, customerType: $customerType, dateOfBirth: $dateOfBirth, district: $district, gender: $gender, idCard: $idCard, industry: $industry, nationCode: $nationCode, province: $province, sector: $sector, segment: $segment, status: $status, swiftCode: $swiftCode, taxCode: $taxCode, transactionFrequency: $transactionFrequency, customerTypeSector: $customerTypeSector, customerTypeMerge: $customerTypeMerge, phoneT24: $phoneT24, phoneEmb: $phoneEmb, phoneSms: $phoneSms, phoneWay4: $phoneWay4, emailT24: $emailT24, emailEmb: $emailEmb, emailWay4: $emailWay4, accountOfficer: $accountOfficer, createdBy: $createdBy, updatedBy: $updatedBy)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CustomerImpl &&
            (identical(other.customerCode, customerCode) ||
                other.customerCode == customerCode) &&
            (identical(other.address, address) || other.address == address) &&
            (identical(other.branchCode, branchCode) ||
                other.branchCode == branchCode) &&
            (identical(other.businessRegistrationNumber,
                    businessRegistrationNumber) ||
                other.businessRegistrationNumber ==
                    businessRegistrationNumber) &&
            (identical(other.customerName, customerName) ||
                other.customerName == customerName) &&
            (identical(other.classification, classification) ||
                other.classification == classification) &&
            (identical(other.customerType, customerType) ||
                other.customerType == customerType) &&
            (identical(other.dateOfBirth, dateOfBirth) ||
                other.dateOfBirth == dateOfBirth) &&
            (identical(other.district, district) ||
                other.district == district) &&
            (identical(other.gender, gender) || other.gender == gender) &&
            (identical(other.idCard, idCard) || other.idCard == idCard) &&
            (identical(other.industry, industry) ||
                other.industry == industry) &&
            (identical(other.nationCode, nationCode) ||
                other.nationCode == nationCode) &&
            (identical(other.province, province) ||
                other.province == province) &&
            (identical(other.sector, sector) || other.sector == sector) &&
            (identical(other.segment, segment) || other.segment == segment) &&
            (identical(other.status, status) || other.status == status) &&
            (identical(other.swiftCode, swiftCode) ||
                other.swiftCode == swiftCode) &&
            (identical(other.taxCode, taxCode) || other.taxCode == taxCode) &&
            (identical(other.transactionFrequency, transactionFrequency) ||
                other.transactionFrequency == transactionFrequency) &&
            (identical(other.customerTypeSector, customerTypeSector) ||
                other.customerTypeSector == customerTypeSector) &&
            (identical(other.customerTypeMerge, customerTypeMerge) ||
                other.customerTypeMerge == customerTypeMerge) &&
            (identical(other.phoneT24, phoneT24) ||
                other.phoneT24 == phoneT24) &&
            (identical(other.phoneEmb, phoneEmb) ||
                other.phoneEmb == phoneEmb) &&
            (identical(other.phoneSms, phoneSms) ||
                other.phoneSms == phoneSms) &&
            (identical(other.phoneWay4, phoneWay4) ||
                other.phoneWay4 == phoneWay4) &&
            (identical(other.emailT24, emailT24) ||
                other.emailT24 == emailT24) &&
            (identical(other.emailEmb, emailEmb) ||
                other.emailEmb == emailEmb) &&
            (identical(other.emailWay4, emailWay4) ||
                other.emailWay4 == emailWay4) &&
            (identical(other.accountOfficer, accountOfficer) ||
                other.accountOfficer == accountOfficer) &&
            (identical(other.createdBy, createdBy) ||
                other.createdBy == createdBy) &&
            (identical(other.updatedBy, updatedBy) ||
                other.updatedBy == updatedBy));
  }

  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        customerCode,
        address,
        branchCode,
        businessRegistrationNumber,
        customerName,
        classification,
        customerType,
        dateOfBirth,
        district,
        gender,
        idCard,
        industry,
        nationCode,
        province,
        sector,
        segment,
        status,
        swiftCode,
        taxCode,
        transactionFrequency,
        customerTypeSector,
        customerTypeMerge,
        phoneT24,
        phoneEmb,
        phoneSms,
        phoneWay4,
        emailT24,
        emailEmb,
        emailWay4,
        accountOfficer,
        createdBy,
        updatedBy
      ]);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CustomerImplCopyWith<_$CustomerImpl> get copyWith =>
      __$$CustomerImplCopyWithImpl<_$CustomerImpl>(this, _$identity);
}

abstract class _Customer implements Customer {
  const factory _Customer(
      {final String customerCode,
      final String address,
      final String branchCode,
      final String businessRegistrationNumber,
      final String customerName,
      final String classification,
      final String customerType,
      final String dateOfBirth,
      final String district,
      final String gender,
      final String idCard,
      final String industry,
      final String nationCode,
      final String province,
      final String sector,
      final String segment,
      final String status,
      final String swiftCode,
      final String taxCode,
      final String transactionFrequency,
      final String customerTypeSector,
      final String customerTypeMerge,
      final String phoneT24,
      final String phoneEmb,
      final String phoneSms,
      final String phoneWay4,
      final String emailT24,
      final String emailEmb,
      final String emailWay4,
      final String accountOfficer,
      final String createdBy,
      final String updatedBy}) = _$CustomerImpl;

  @override
  String get customerCode;
  @override
  String get address;
  @override
  String get branchCode;
  @override
  String get businessRegistrationNumber;
  @override
  String get customerName;
  @override
  String get classification;
  @override
  String get customerType;
  @override
  String get dateOfBirth;
  @override
  String get district;
  @override
  String get gender;
  @override
  String get idCard;
  @override
  String get industry;
  @override
  String get nationCode;
  @override
  String get province;
  @override
  String get sector;
  @override
  String get segment;
  @override
  String get status;
  @override
  String get swiftCode;
  @override
  String get taxCode;
  @override
  String get transactionFrequency;
  @override
  String get customerTypeSector;
  @override
  String get customerTypeMerge;
  @override
  String get phoneT24;
  @override
  String get phoneEmb;
  @override
  String get phoneSms;
  @override
  String get phoneWay4;
  @override
  String get emailT24;
  @override
  String get emailEmb;
  @override
  String get emailWay4;
  @override
  String get accountOfficer;
  @override
  String get createdBy;
  @override
  String get updatedBy;
  @override
  @JsonKey(ignore: true)
  _$$CustomerImplCopyWith<_$CustomerImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
