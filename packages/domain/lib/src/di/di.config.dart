// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:domain/domain.dart' as _i4;
import 'package:domain/src/usecase/block_list_use_case.dart' as _i5;
import 'package:domain/src/usecase/block_use_case.dart' as _i6;
import 'package:domain/src/usecase/branch_use_case.dart' as _i7;
import 'package:domain/src/usecase/clear_current_user_data_use_case.dart'
    as _i8;
import 'package:domain/src/usecase/customer/customer_use_case.dart' as _i9;
import 'package:domain/src/usecase/dashboard/create_job_use_case.dart' as _i10;
import 'package:domain/src/usecase/dashboard/create_todo_use_case.dart' as _i11;
import 'package:domain/src/usecase/get_initial_app_data_use_case.dart' as _i12;
import 'package:domain/src/usecase/get_initial_home_data_use_case.dart' as _i13;
import 'package:domain/src/usecase/get_initial_main_data_use_case.dart' as _i14;
import 'package:domain/src/usecase/get_users_use_case.dart' as _i15;
import 'package:domain/src/usecase/is_logged_in_use_case.dart' as _i16;
import 'package:domain/src/usecase/load_initial_resource_use_case.dart' as _i17;
import 'package:domain/src/usecase/load_more_users_use_case.dart' as _i18;
import 'package:domain/src/usecase/login_use_case.dart' as _i19;
import 'package:domain/src/usecase/logout_use_case.dart' as _i3;
import 'package:domain/src/usecase/save_is_dark_mode_use_case.dart' as _i20;
import 'package:domain/src/usecase/save_is_first_launch_app_use_case.dart'
    as _i21;
import 'package:domain/src/usecase/save_language_code_use_case.dart' as _i22;
import 'package:domain/src/usecase/save_username_use_case.dart' as _i23;
import 'package:domain/src/usecase/tools_utilities/calculate_saving/calculate_saving_use_case.dart'
    as _i24;
import 'package:domain/src/usecase/tools_utilities/exchange_rate/exchange_rate_use_case.dart'
    as _i28;
import 'package:domain/src/usecase/tools_utilities/interbank/interbank_use_case.dart'
    as _i29;
import 'package:domain/src/usecase/user_info/user_info_page_use_case.dart'
    as _i25;
import 'package:domain/src/usecase/user_info_hrs_code_use_case.dart' as _i26;
import 'package:domain/src/usecase/user_info_use_case.dart' as _i27;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

extension GetItInjectableX on _i1.GetIt {
// initializes the registration of main-scope dependencies inside of GetIt
  _i1.GetIt init({
    String? environment,
    _i2.EnvironmentFilter? environmentFilter,
  }) {
    final gh = _i2.GetItHelper(
      this,
      environment,
      environmentFilter,
    );
    gh.factory<_i3.LogoutUseCase>(() => _i3.LogoutUseCase(
          gh<_i4.Repository>(),
          gh<_i4.AppNavigator>(),
        ));
    gh.factory<_i5.BlockListUseCase>(
        () => _i5.BlockListUseCase(gh<_i4.Repository>()));
    gh.factory<_i6.BlockUseCase>(() => _i6.BlockUseCase(gh<_i4.Repository>()));
    gh.factory<_i7.BranchUseCase>(
        () => _i7.BranchUseCase(gh<_i4.Repository>()));
    gh.factory<_i8.ClearCurrentUserDataUseCase>(
        () => _i8.ClearCurrentUserDataUseCase(gh<_i4.Repository>()));
    gh.factory<_i9.CustomerUseCase>(
        () => _i9.CustomerUseCase(gh<_i4.Repository>()));
    gh.factory<_i10.CreateJobUseCase>(
        () => _i10.CreateJobUseCase(gh<_i4.Repository>()));
    gh.factory<_i11.CreateTodoUseCase>(
        () => _i11.CreateTodoUseCase(gh<_i4.Repository>()));
    gh.factory<_i12.GetInitialAppDataUseCase>(
        () => _i12.GetInitialAppDataUseCase(gh<_i4.Repository>()));
    gh.factory<_i13.GetInitialHomeDataUseCase>(
        () => _i13.GetInitialHomeDataUseCase(gh<_i4.Repository>()));
    gh.factory<_i14.GetInitialMainDataUseCase>(
        () => _i14.GetInitialMainDataUseCase(gh<_i4.Repository>()));
    gh.factory<_i15.GetUsersUseCase>(
        () => _i15.GetUsersUseCase(gh<_i4.Repository>()));
    gh.factory<_i16.IsLoggedInUseCase>(
        () => _i16.IsLoggedInUseCase(gh<_i4.Repository>()));
    gh.factory<_i17.LoadInitialResourceUseCase>(
        () => _i17.LoadInitialResourceUseCase(gh<_i4.Repository>()));
    gh.factory<_i18.LoadMoreUsersUseCase>(
        () => _i18.LoadMoreUsersUseCase(gh<_i4.Repository>()));
    gh.factory<_i19.LoginUseCase>(
        () => _i19.LoginUseCase(gh<_i4.Repository>()));
    gh.factory<_i20.SaveIsDarkModeUseCase>(
        () => _i20.SaveIsDarkModeUseCase(gh<_i4.Repository>()));
    gh.factory<_i21.SaveIsFirstLaunchAppUseCase>(
        () => _i21.SaveIsFirstLaunchAppUseCase(gh<_i4.Repository>()));
    gh.factory<_i22.SaveLanguageCodeUseCase>(
        () => _i22.SaveLanguageCodeUseCase(gh<_i4.Repository>()));
    gh.factory<_i23.SaveUserNameUseCase>(
        () => _i23.SaveUserNameUseCase(gh<_i4.Repository>()));
    gh.factory<_i24.CalculateSavingUseCase>(
        () => _i24.CalculateSavingUseCase(gh<_i4.Repository>()));
    gh.factory<_i25.UserInfoPageUseCase>(
        () => _i25.UserInfoPageUseCase(gh<_i4.Repository>()));
    gh.factory<_i26.UserInfoHrsCodeUseCase>(
        () => _i26.UserInfoHrsCodeUseCase(gh<_i4.Repository>()));
    gh.factory<_i27.UserInfoUseCase>(
        () => _i27.UserInfoUseCase(gh<_i4.Repository>()));
    gh.factory<_i28.ExchangeRateUseCase>(
        () => _i28.ExchangeRateUseCase(gh<_i4.Repository>()));
    gh.factory<_i29.InterbankUseCase>(
        () => _i29.InterbankUseCase(gh<_i4.Repository>()));
    return this;
  }
}
