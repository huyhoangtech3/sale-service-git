import 'package:domain/domain.dart';

abstract class Repository {
  // TODO: Call API
  Future<void> login({
    required String username,
    required String password,
  });
  Future<void> logout();
  Future<List<Block>> findBlockByRM();
  Future<void> getUserInfo({
    required String username,
    required int externalData,
  });
  Future<Paging<UserInfo>> getDetailUserInfoByHrsCode(String hrsCode);
  Future<List<Branch>> getBranchByUser(String rsId);
  Future<PagedList<Customer>> getCustomers({
    required int pageNumber,
    required int pageSize,
    required String rsId,
    required String scope,
    required String search,
    required String searchFastType,
    required String customerType,
  });

  // TODO: Save Data
  Future<bool> saveIsDarkMode(bool isDarkMode);
  Future<bool> saveLanguageCode(LanguageCode languageCode);
  Future<bool> saveIsFirstLogin(bool isFirstLogin);
  Future<bool> saveIsFirstLaunchApp(bool isFirstLaunchApp);
  Future<void> saveAccessToken(String accessToken);
  Future<void> saveRefreshToken(String refreshToken);
  Future<void> saveUserName(String userName);
  Future<void> saveBlockListPreference(List<Block> blockList);
  Future<void> saveUserInfoPreference(UserInfo user);
  List<int> putUserInfo(Paging<UserInfo> userInfo);
  List<int> putBranches(List<Branch> branches);

  Future<void> getBlocks({
    required int page,
    required int size,
  });
  User getUserPreference();
  Future<PagedList<User>> getUsers({
    required int page,
    required int? limit,
  });
  Future<bool> saveUserPreference(User user);
  Future<User> getMe();
  int putLocalUser(User user);
  Stream<List<User>> getLocalUsersStream();
  List<User> getLocalUsers();
  User? getLocalUser(int id);
  bool deleteImageUrl(int id);
  int deleteAllUsersAndImageUrls();

  // TODO: Get & Clear Data
  bool get isLoggedIn;
  bool get isFirstLaunchApp;
  bool get isFirstLogin;
  bool get isDarkMode;
  Future<String> get getRefreshToken;
  String get getUserName;
  LanguageCode get languageCode;
  Stream<bool> get onConnectivityChanged;
  List<Block> getBlockListPreference();
  UserInfo getUserInfoPreference();
  List<UserInfo> getUsersInfoDB();
  List<UserInfo> getUserInfoDB(String hrsCode);
  int deleteAllUserInfo();
  List<Branch> getBranches();
  List<Branch> getBranch(String code);
  int deleteAllUBranches();

  Future<void> clearCurrentUserData();

}
