import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:domain/domain.dart';
import 'package:injectable/injectable.dart';
import 'package:data/data.dart';
import 'package:shared/shared.dart';

@LazySingleton(as: Repository)
class RepositoryImpl implements Repository {
  RepositoryImpl(
    this._appApiService,
    this._appPreferences,
    this._appDatabase,
    this._preferenceUserDataMapper,
    this._userDataMapper,
    this._languageCodeDataMapper,
    this._genderDataMapper,
    this._localUserDataMapper,
    this._blockDataMapper,
    this._apiUserInfoDataMapper,
    this._pageableUserInfoDataMapper,
    this._userInfoDataMapper,
    this._apiBranchDataMapper,
    this._branchDataMapper,
    this._apiCustomerDataMapper,
  );

  final AppApiService _appApiService;
  final AppPreferences _appPreferences;
  final AppDatabase _appDatabase;
  final PreferenceUserDataMapper _preferenceUserDataMapper;
  final ApiUserDataMapper _userDataMapper;
  final LanguageCodeDataMapper _languageCodeDataMapper;
  final GenderDataMapper _genderDataMapper;
  final LocalUserDataMapper _localUserDataMapper;
  final ApiBlockDataMapper _blockDataMapper;
  final ApiUserInfoDataMapper _apiUserInfoDataMapper;
  final ApiPageableUserInfoDataMapper _pageableUserInfoDataMapper;
  final UserInfoDataMapper _userInfoDataMapper;
  final ApiBranchDataMapper _apiBranchDataMapper;
  final BranchDataMapper _branchDataMapper;
  final ApiCustomerDataMapper _apiCustomerDataMapper;

  @override
  bool get isLoggedIn => _appPreferences.isLoggedIn;

  @override
  bool get isFirstLogin => _appPreferences.isFirstLogin;

  @override
  bool get isFirstLaunchApp => _appPreferences.isFirstLaunchApp;

  @override
  Stream<bool> get onConnectivityChanged =>
      Connectivity().onConnectivityChanged.map((event) => event != ConnectivityResult.none);

  @override
  bool get isDarkMode => _appPreferences.isDarkMode;

  @override
  LanguageCode get languageCode =>
      _languageCodeDataMapper.mapToEntity(_appPreferences.languageCode);

  @override
  Future<String> get getRefreshToken => _appPreferences.refreshToken;

  @override
  Future<bool> saveIsFirstLogin(bool isFirstLogin) {
    return _appPreferences.saveIsFirstLogin(isFirstLogin);
  }

  @override
  Future<bool> saveIsFirstLaunchApp(bool isFirstLaunchApp) {
    return _appPreferences.saveIsFirsLaunchApp(isFirstLaunchApp);
  }

  @override
  Future<bool> saveLanguageCode(LanguageCode languageCode) {
    return _appPreferences.saveLanguageCode(_languageCodeDataMapper.mapToData(languageCode));
  }

  @override
  Future<bool> saveIsDarkMode(bool isDarkMode) => _appPreferences.saveIsDarkMode(isDarkMode);

  @override
  Future<void> saveAccessToken(String accessToken) => _appPreferences.saveAccessToken(accessToken);

  @override
  Future<void> saveRefreshToken(String refreshToken) => _appPreferences.saveRefreshToken(refreshToken) ;

  @override
  Future<void> login({required String username, required String password}) async {
    final response = await _appApiService.login(username: username, password: password);
    await Future.wait([
      saveAccessToken(response?.accessToken ?? ''),
      saveRefreshToken(response?.refreshToken ?? ''),
    ]);
  }

  @override
  Future<void> saveBlockListPreference(List<Block> blockList) =>
    _appPreferences.saveBlockListData(_blockDataMapper.mapToListData(blockList));

  @override
  List<Block> getBlockListPreference() =>
    _blockDataMapper.mapToListEntity(_appPreferences.getBlockList);

  @override
  Future<List<Block>> findBlockByRM() async {
    final response = await _appApiService.findBlockByRM();
    await Future.wait([saveBlockListPreference(_blockDataMapper.mapToListEntity(response))]);
    return _blockDataMapper.mapToListEntity(response);
  }

  @override
  Future<void> saveUserInfoPreference(UserInfo userInfo) =>
      _appPreferences.saveCurrentUserInfo(_apiUserInfoDataMapper.mapToData(userInfo));

  @override
  UserInfo getUserInfoPreference() =>
      _apiUserInfoDataMapper.mapToEntity(_appPreferences.getUserInfo);

  @override
  Future<UserInfo> getUserInfo({required String username, required int externalData}) async {
    final response = await _appApiService.getUserInfo(username: username, externalData: externalData);
    await Future.wait([saveUserInfoPreference(_apiUserInfoDataMapper.mapToEntity(response))]);
    return _apiUserInfoDataMapper.mapToEntity(response);
  }

  @override
  Future<Paging<UserInfo>> getDetailUserInfoByHrsCode(String hrsCode) async {
    final response = await _appApiService.getDetailUserInfoByHrsCode(hrsCode: hrsCode);
    final result = _pageableUserInfoDataMapper.mapToEntity(response);
    putUserInfo(result);
    return result;
  }

  @override
  Future<List<Branch>> getBranchByUser(String rsId) async {
    final response = await _appApiService.getBranchByUser(rsId: rsId);
    final result = _apiBranchDataMapper.mapToListEntity(response);
    putBranches(result);
    return result;
  }

  @override
  Future<void> logout() async {
    Log.e('Logout: ${_appPreferences.getUserInfo!.fullName}');
    final refreshToken = await _appPreferences.refreshToken;
    await _appApiService.logout(refreshToken: refreshToken);
    await _appPreferences.clearCurrentUserData();
    deleteAllUserInfo();
    deleteAllUBranches();
  }

  @override
  Future<void> getBlocks({required int page, required int size}) async {
    final response = await _appApiService.getBlocks(page: page, size: size);
  }

  @override
  Future<void> clearCurrentUserData() => _appPreferences.clearCurrentUserData();

  @override
  Future<bool> saveUserPreference(User user) => _appPreferences.saveCurrentUser(_preferenceUserDataMapper.mapToData(user));

  @override
  User getUserPreference() => _preferenceUserDataMapper.mapToEntity(_appPreferences.currentUser);

  @override
  Future<PagedList<User>> getUsers({
    required int page,
    required int? limit,
  }) async {
    final response = await _appApiService.getUsers(page: page, limit: limit);
    return PagedList(data: _userDataMapper.mapToListEntity(response?.results));
  }

  @override
  Future<User> getMe() async {
    final response = await _appApiService.getMe();
    return _userDataMapper.mapToEntity(response);
  }

  @override
  int deleteAllUsersAndImageUrls() {
    return _appDatabase.deleteAllUsersAndImageUrls();
  }

  @override
  bool deleteImageUrl(int id) {
    return _appDatabase.deleteImageUrl(id);
  }

  @override
  User? getLocalUser(int id) {
    return _localUserDataMapper.mapToEntity(_appDatabase.getUser(id));
  }

  @override
  List<User> getLocalUsers() {
    return _localUserDataMapper.mapToListEntity(_appDatabase.getUsers());
  }

  @override
  Stream<List<User>> getLocalUsersStream() {
    return _appDatabase
        .getUsersStream()
        .map((event) => _localUserDataMapper.mapToListEntity(event));
  }

  @override
  int putLocalUser(User user) {
    final userData = _localUserDataMapper.mapToData(user);
    return _appDatabase.putUser(userData);
  }

  @override
  String get getUserName => _appPreferences.userName;

  @override
  Future<void> saveUserName(String userName) async {
    _appPreferences.saveUserName(userName);
  }

  @override
  List<int> putUserInfo(Paging<UserInfo> userInfo) {
    final data = _userInfoDataMapper.mapToListData(userInfo.content);
    return _appDatabase.putUserInfo(data);
  }

  @override
  List<UserInfo> getUserInfoDB(String hrsCode) {
    return _userInfoDataMapper.mapToListEntity(_appDatabase.getUserInfo(hrsCode));
  }

  @override
  List<UserInfo> getUsersInfoDB() {
    return _userInfoDataMapper.mapToListEntity(_appDatabase.getUsersInfo());
  }

  @override
  int deleteAllUserInfo() {
    return _appDatabase.deleteAllUserInfo();
  }

  @override
  List<int> putBranches(List<Branch> branches) {
    final data = _branchDataMapper.mapToListData(branches);
    return _appDatabase.putBranch(data);
  }

  @override
  int deleteAllUBranches() {
    return _appDatabase.deleteAllBranches();
  }

  @override
  List<Branch> getBranch(String code) {
    return _branchDataMapper.mapToListEntity(_appDatabase.getBranch(code));
  }

  @override
  List<Branch> getBranches() {
    return _branchDataMapper.mapToListEntity(_appDatabase.getAllBranches());
  }

  @override
  Future<PagedList<Customer>> getCustomers({
    required int pageNumber,
    required int pageSize,
    required String rsId,
    required String scope,
    required String search,
    required String searchFastType,
    required String customerType
  }) async {
    final response = await _appApiService.getCustomers(
      pageNumber: pageNumber,
      pageSize: pageSize,
      rsId: rsId,
      scope: scope,
      search: search,
      searchFastType: searchFastType,
      customerType: customerType
    );
    return PagedList(data: _apiCustomerDataMapper.mapToListEntity(response));
  }
}
