// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'preference_user_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$PreferenceUserDataImpl _$$PreferenceUserDataImplFromJson(
        Map<String, dynamic> json) =>
    _$PreferenceUserDataImpl(
      id: (json['id'] as num).toInt(),
      email: json['email'] as String,
    );

Map<String, dynamic> _$$PreferenceUserDataImplToJson(
        _$PreferenceUserDataImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'email': instance.email,
    };
