import 'package:injectable/injectable.dart';
import 'package:objectbox/objectbox.dart';

import 'package:data/data.dart';
import 'package:shared/shared.dart';

import 'generated/objectbox.g.dart';

@LazySingleton()
class AppDatabase {
  AppDatabase(this.store);

  final Store store;

  //TODO: Table UserInfo
  List<int> putUserInfo(List<UserInfoData> userInfo) {
    return store.box<UserInfoData>().putMany(userInfo);
  }

  List<UserInfoData> getUsersInfo() {
    return store.box<UserInfoData>().getAll();
  }

  List<UserInfoData> getUserInfo(String hrsCode) {
    final query = store.box<UserInfoData>().query(UserInfoData_.hrsCode.equals(hrsCode)).build();
    final results = query.find();
    query.close();
    return results;
  }

  int deleteAllUserInfo() {
    return store.box<UserInfoData>().removeAll();
  }
  //TODO: Table Branch
  List<int> putBranch(List<BranchData> branches) {
    return store.box<BranchData>().putMany(branches);
  }

  List<BranchData> getAllBranches() {
    return store.box<BranchData>().getAll();
  }

  List<BranchData> getBranch(String code) {
    final query = store.box<BranchData>().query(BranchData_.code.equals(code)).build();
    final results = query.find();
    query.close();
    return results;
  }

  int deleteAllBranches() {
    return store.box<BranchData>().removeAll();
  }
  //TODO: Table User
  int putUser(LocalUserData user) {
    return store.box<LocalUserData>().put(user);
  }

  Stream<List<LocalUserData>> getUsersStream() {
    return store
        .box<LocalUserData>()
        .query()
        .watch(triggerImmediately: true)
        .map((query) => query.find());
  }

  List<LocalUserData> getUsers() {
    return store.box<LocalUserData>().getAll();
  }

  LocalUserData? getUser(int id) {
    return store.box<LocalUserData>().get(id);
  }

  bool deleteImageUrl(int id) {
    return store.box<LocalImageUrlData>().remove(id);
  }

  int deleteAllUsersAndImageUrls() {
    store.box<LocalImageUrlData>().removeAll();

    return store.box<LocalUserData>().removeAll();
  }
}
