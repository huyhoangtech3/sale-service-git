import 'package:objectbox/objectbox.dart';

@Entity()
class BranchData {
  BranchData({
    this.code,
    this.name,
    this.region,
    this.description,
  });

  @Id(assignable: true)
  int id = 0;
  String? code;
  String? name;
  String? region;
  String? description;

  @override
  int get hashCode {
    return id.hashCode ^ code.hashCode ^ name.hashCode ^ region.hashCode ^ description.hashCode;
  }

  @override
  String toString() {
    return 'BranchData(id: $id, code: $code, name: $name, region: $region, description: $description)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }

    return other is BranchData &&
        other.id == id &&
        other.code == code &&
        other.name == name &&
        other.region == region &&
        other.description == description;
  }
}
