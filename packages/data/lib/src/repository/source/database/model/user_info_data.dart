import 'package:objectbox/objectbox.dart';

@Entity()
class UserInfoData {
  UserInfoData({
    this.id,
    this.code,
    this.hrsCode,
    this.username,
    this.fullName,
    this.branch,
    this.phoneNumber,
    this.email,
    this.note,
    this.image,
    this.phoneDevice,
    this.reason,
    this.gender,
    this.unLock,
    this.statusWork,
    this.domainAll,
    this.adminRole,
    this.active,
  });

  @Id(assignable: true)
  int index = 0;
  String? id;
  String? code;
  String? hrsCode;
  String? username;
  String? fullName;
  String? branch;
  String? phoneNumber;
  String? email;
  String? note;
  String? image;
  String? phoneDevice;
  String? reason;
  String? gender;
  bool? unLock;
  String? statusWork;
  bool? domainAll;
  bool? adminRole;
  bool? active;

  @override
  int get hashCode {
    return index.hashCode ^ id.hashCode ^ code.hashCode ^ hrsCode.hashCode ^ username.hashCode ^ fullName.hashCode
            ^ branch.hashCode ^ phoneNumber.hashCode ^ email.hashCode ^ note.hashCode
            ^ image.hashCode ^ phoneDevice.hashCode ^ reason.hashCode
            ^ gender.hashCode ^ unLock.hashCode ^ statusWork.hashCode
            ^ domainAll.hashCode ^ adminRole.hashCode ^ active.hashCode;
  }

  @override
  String toString() {
    return 'UserInfoData(index: $index, id: $id, code: $code, hrsCode: $hrsCode, username: $username,'
        ' fullName: $fullName, branch: $branch, phoneNumber: $phoneNumber, email: $email,'
        ' note: $note, image: $image, phoneDevice: $phoneDevice,'
        ' reason: $reason, gender: $gender, unLock: $unLock, statusWork: $statusWork,'
        ' domainAll: $domainAll, adminRole: $adminRole, active: $active)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }

    return other is UserInfoData &&
        other.index == index &&
        other.id == id &&
        other.code == code &&
        other.hrsCode == hrsCode &&
        other.username == username &&
        other.fullName == fullName &&
        other.branch == branch &&
        other.phoneNumber == phoneNumber &&
        other.email == email &&
        other.note == note &&
        other.image == image &&
        other.phoneDevice == phoneDevice &&
        other.reason == reason &&
        other.gender == gender &&
        other.unLock == unLock &&
        other.statusWork == statusWork &&
        other.domainAll == domainAll &&
        other.adminRole == adminRole &&
        other.active == active;
  }
}
