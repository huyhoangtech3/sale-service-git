import 'package:domain/domain.dart';
import 'package:injectable/injectable.dart';
import 'package:shared/shared.dart';

import 'package:data/data.dart';

@Injectable()
class BranchDataMapper extends BaseDataMapper<BranchData, Branch> with DataMapperMixin {

  @override
  Branch mapToEntity(BranchData? data) {
    return Branch(
      code: data?.code ?? Branch.defaultCode,
      name: data?.name ?? Branch.defaultName,
      region: data?.region ?? Branch.defaultRegion,
      description: data?.description ?? Branch.defaultDescription,
    );
  }

  @override
  BranchData mapToData(Branch entity) {
    return BranchData(
      code: entity.code,
      name: entity.name,
      region: entity.region,
      description: entity.description,
    );
  }
}
