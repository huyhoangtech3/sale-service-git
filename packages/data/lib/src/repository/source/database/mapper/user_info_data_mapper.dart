import 'package:domain/domain.dart';
import 'package:injectable/injectable.dart';
import 'package:shared/shared.dart';

import 'package:data/data.dart';

@Injectable()
class UserInfoDataMapper extends BaseDataMapper<UserInfoData, UserInfo> with DataMapperMixin {

  @override
  UserInfo mapToEntity(UserInfoData? data) {
    return UserInfo(
      id: data?.id ?? UserInfo.defaultId,
      code: data?.code ?? UserInfo.defaultCode,
      hrsCode: data?.hrsCode ?? UserInfo.defaultHrsCode,
      username: data?.username ?? UserInfo.defaultUsername,
      fullName: data?.fullName ?? UserInfo.defaultFullName,
      branch: data?.branch ?? UserInfo.defaultBranch,
      phoneNumber: data?.phoneNumber ?? UserInfo.defaultPhoneNumber,
      email: data?.email ?? UserInfo.defaultEmail,
      note: data?.note ?? UserInfo.defaultNote,
      image: data?.image ?? UserInfo.defaultImage,
      phoneDevice: data?.phoneDevice ?? UserInfo.defaultPhoneDevice,
      reason: data?.reason ?? UserInfo.defaultReason,
      gender: data?.gender ?? UserInfo.defaultGender,
      unLock: data?.unLock ?? UserInfo.defaultUnLock,
      statusWork: data?.statusWork ?? UserInfo.defaultStatusWork,
      domainAll: data?.domainAll ?? UserInfo.defaultDomainAll,
      adminRole: data?.adminRole ?? UserInfo.defaultAdminRole,
      active: data?.active ?? UserInfo.defaultActive,
    );
  }

  @override
  UserInfoData mapToData(UserInfo entity) {
    return UserInfoData(
      id: entity.id,
      code: entity.code,
      hrsCode: entity.hrsCode,
      username: entity.username,
      fullName: entity.fullName,
      branch: entity.branch,
      phoneNumber: entity.phoneNumber,
      email: entity.email,
      note: entity.note,
      image: entity.image,
      phoneDevice: entity.phoneDevice,
      gender: entity.gender,
      unLock: entity.unLock,
      statusWork: entity.statusWork,
      domainAll: entity.domainAll,
      adminRole: entity.adminRole,
      active: entity.active,
    );
  }
}
