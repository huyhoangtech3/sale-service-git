import 'package:injectable/injectable.dart';
import 'package:shared/shared.dart';
import 'package:dio/dio.dart';
import 'package:data/data.dart';

@LazySingleton()
class RefreshTokenApiService {
  RefreshTokenApiService(this._refreshTokenApiClient);

  final RefreshTokenApiClient _refreshTokenApiClient;

  Future<ApiRefreshTokenData?> refreshToken(String refreshToken) async {
    try {
      return _refreshTokenApiClient.request(
        method: RestMethod.post,
        path: '${UrlConstants.auth}/token',
        body: {
          'client_id': KeyCloakConstants.clientId,
          'refresh_token': refreshToken,
          'grant_type': KeyCloakConstants.refreshToken,
        },
        options: Options(
          headers: {
            Headers.contentTypeHeader: Headers.formUrlEncodedContentType,
          },
        ),
        successResponseMapperType: SuccessResponseMapperType.jsonObject,
        errorResponseMapperType: ErrorResponseMapperType.jsonObject,
        decoder: (json) => ApiRefreshTokenData.fromJson(json as Map<String, dynamic>),
      );
    } catch (e) {
      if (e is RemoteException &&
          (e.kind == RemoteExceptionKind.serverDefined ||
              e.kind == RemoteExceptionKind.serverUndefined)) {
        throw const RemoteException(kind: RemoteExceptionKind.refreshTokenFailed);
      }

      rethrow;
    }
  }
}
