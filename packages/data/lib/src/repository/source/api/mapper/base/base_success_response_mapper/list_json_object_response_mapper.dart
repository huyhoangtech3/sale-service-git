import 'package:shared/shared.dart';

import 'package:data/data.dart';

class ListJsonObjectResponseMapper<T extends Object>
    extends BaseSuccessResponseMapper<T, ListResponse<T>> {
  @override
  // ignore: avoid-dynamic
  ListResponse<T>? mapToDataModel({
    required dynamic response,
    Decoder<T>? decoder,
  }) {
    return decoder != null && response is Map<String, dynamic>
        ? ListResponse.fromJson(response, (json) => decoder(json))
        : null;
  }
}
