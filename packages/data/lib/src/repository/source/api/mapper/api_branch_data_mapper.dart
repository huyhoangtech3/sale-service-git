import 'dart:convert';

import 'package:domain/domain.dart';
import 'package:injectable/injectable.dart';
import 'package:shared/shared.dart';

import 'package:data/data.dart';

@Injectable()
class ApiBranchDataMapper extends BaseDataMapper<ApiBranchData, Branch> {

  @override
  Branch mapToEntity(ApiBranchData? data) {
    return Branch(
      code: data?.code ?? Branch.defaultCode,
      name: data?.name ?? Branch.defaultName,
      region: data?.region ?? Branch.defaultRegion,
      description: data?.description ?? Branch.defaultDescription,
    );
  }

  @override
  ApiBranchData mapToData(Branch entity){
    return ApiBranchData(
      code: entity.code,
      name: entity.name,
      region: entity.region,
      description: entity.description,
    );
  }
}
