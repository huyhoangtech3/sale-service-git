import 'dart:convert';

import 'package:domain/domain.dart';
import 'package:injectable/injectable.dart';
import 'package:shared/shared.dart';

import 'package:data/data.dart';

@Injectable()
class ApiBlockDataMapper extends BaseDataMapper<ApiBlockData, Block> {

  @override
  Block mapToEntity(ApiBlockData? data) {
    return Block(
      id: data?.id ?? Block.defaultId,
      code: data?.code ?? Block.defaultCode,
      name: data?.name ?? Block.defaultName,
      isActive: data?.isActive ?? Block.defaultIsActive,
      customerType: data?.customerType ?? Block.defaultCustomerType,
      createdDate: data?.createdDate ?? Block.defaultCreatedDate,
      createdBy: data?.createdBy ?? Block.defaultCreatedBy,
      updatedDate: data?.updatedDate ?? Block.defaultUpdatedDate,
      updatedBy: data?.updatedBy ?? Block.defaultUpdatedBy,
    );
  }

  @override
  ApiBlockData mapToData(Block entity){
    return ApiBlockData(
      id: entity.id,
      code: entity.code,
      name: entity.name,
      isActive: entity.isActive,
      customerType: entity.customerType,
      createdDate: entity.createdDate,
      createdBy: entity.createdBy,
      updatedDate: entity.updatedDate,
      updatedBy: entity.updatedBy,
    );
  }

  @override
  List<ApiBlockData> mapToListData(List<Block> entity){
    return entity.map(mapToData).toList();
  }
}
