import 'dart:convert';

import 'package:domain/domain.dart';
import 'package:injectable/injectable.dart';
import 'package:shared/shared.dart';

import 'package:data/data.dart';

@Injectable()
class ApiPageableUserInfoDataMapper extends BaseDataMapper<ListResponse<ApiBasicUserInfoData>, Paging<UserInfo>> {

  @override
  Paging<UserInfo> mapToEntity(ListResponse<ApiBasicUserInfoData>? data) {
    return Paging<UserInfo>(
      content: contentMapToListEntity(data?.content) ?? [],
      totalPages: data?.totalPages ?? Paging.defaultTotalPages,
      last: data?.last ?? Paging.defaultLast,
      totalElements: data?.totalElements ?? Paging.defaultTotalElements,
      first: data?.first ?? Paging.defaultFirst,
      numberOfElements: data?.numberOfElements ?? Paging.defaultNumberOfElements,
      number: data?.number ?? Paging.defaultNumber,
      size: data?.size ?? Paging.defaultSize,
      empty: data?.empty ?? Paging.defaultEmpty,
      sort: sortMapToEntity(data?.sort),
      pageable: pageableMapToEntity(data?.pageable),
    );
  }

  @override
  ApiBasicUserInfoData mapToData(UserInfo entity){
    return ApiBasicUserInfoData(
      id: entity.id,
      code: entity.code,
      hrsCode: entity.hrsCode,
      username: entity.username,
      fullName: entity.fullName,
      branch: entity.branch,
      phoneNumber: entity.phoneNumber,
      email: entity.email,
      note: entity.note,
      image: entity.image,
      branchName: entity.branchName,
      phoneDevice: entity.phoneDevice,
      gender: entity.gender,
      unLock: entity.unLock,
      statusWork: entity.statusWork,
      blockRmManager: entity.blockRmManager,
      domainAll: entity.domainAll,
      adminRole: entity.adminRole,
      active: entity.active,
    );
  }

  Sort sortMapToEntity(SortResponse? data) {
    return Sort(
      unsorted: data?.unsorted ?? Sort.defaultUnsorted,
      sorted: data?.sorted ?? Sort.defaultSorted,
      empty: data?.empty ?? Sort.defaultEmpty,
    );
  }

  Pageable pageableMapToEntity(PageableResponse? data) {
    return Pageable(
      sort: sortMapToEntity(data?.sort),
      pageNumber: data?.pageNumber ?? Pageable.defaultPageNumber,
      pageSize: data?.pageSize ?? Pageable.defaultPageSize,
      offset: data?.offset ?? Pageable.defaultOffset,
      paged: data?.paged ?? Pageable.defaultPaged,
      unpaged: data?.unpaged ?? Pageable.defaultUnpaged,
    );
  }

  UserInfo contentMapToEntity(ApiBasicUserInfoData? data){
    return UserInfo(
      id: data?.id ?? UserInfo.defaultId,
      code: data?.code ?? UserInfo.defaultCode,
      hrsCode: data?.hrsCode ?? UserInfo.defaultHrsCode,
      username: data?.username ?? UserInfo.defaultUsername,
      fullName: data?.fullName ?? UserInfo.defaultFullName,
      branch: data?.branch ?? UserInfo.defaultBranch,
      phoneNumber: data?.phoneNumber ?? UserInfo.defaultPhoneNumber,
      email: data?.email ?? UserInfo.defaultEmail,
      note: data?.note ?? UserInfo.defaultNote,
      image: data?.image ?? UserInfo.defaultImage,
      branchName: data?.branchName ?? UserInfo.defaultBranchName,
      phoneDevice: data?.phoneDevice ?? UserInfo.defaultPhoneDevice,
      reason: data?.reason ?? UserInfo.defaultReason,
      gender: data?.gender ?? UserInfo.defaultGender,
      unLock: data?.unLock ?? UserInfo.defaultUnLock,
      statusWork: data?.statusWork ?? UserInfo.defaultStatusWork,
      blockRmManager: data?.blockRmManager ?? UserInfo.defaultBlockRmManager,
      domainAll: data?.domainAll ?? UserInfo.defaultDomainAll,
      adminRole: data?.adminRole ?? UserInfo.defaultAdminRole,
      active: data?.active ?? UserInfo.defaultActive,
    );
  }

  List<UserInfo>? contentMapToListEntity(List<ApiBasicUserInfoData>? data){
    return data?.map(contentMapToEntity).toList();
  }
}
