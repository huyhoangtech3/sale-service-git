import 'package:domain/domain.dart';
import 'package:injectable/injectable.dart';

import 'package:data/data.dart';

@Injectable()
class ApiCustomerDataMapper extends BaseDataMapper<ApiCustomerData, Customer> {

  @override
  Customer mapToEntity(ApiCustomerData? data) {
    return Customer(
      customerCode: data?.customerCode ?? Customer.defaultCustomerCode,
      address: data?.address ?? Customer.defaultAddress,
      branchCode: data?.branchCode ?? Customer.defaultBranchCode,
      businessRegistrationNumber: data?.businessRegistrationNumber ?? Customer.defaultBusinessRegistrationNumber,
      customerName: data?.customerName ?? Customer.defaultCustomerName,
      classification: data?.classification ?? Customer.defaultClassification,
      customerType: data?.customerType ?? Customer.defaultCustomerType,
      dateOfBirth: data?.dateOfBirth ?? Customer.defaultDateOfBirth,
      district: data?.district ?? Customer.defaultDistrict,
      gender: data?.gender ?? Customer.defaultGender,
      idCard: data?.idCard ?? Customer.defaultIdCard,
      industry: data?.industry ?? Customer.defaultIndustry,
      nationCode: data?.nationCode ?? Customer.defaultNationCode,
      province: data?.province ?? Customer.defaultProvince,
      sector: data?.sector ?? Customer.defaultSector,
      segment: data?.segment ?? Customer.defaultSegment,
      status: data?.status ?? Customer.defaultStatus,
      swiftCode: data?.swiftCode ?? Customer.defaultSwiftCode,
      taxCode: data?.taxCode ?? Customer.defaultTaxCode,
      transactionFrequency: data?.transactionFrequency ?? Customer.defaultTransactionFrequency,
      customerTypeSector: data?.customerTypeSector ?? Customer.defaultCustomerTypeSector,
      customerTypeMerge: data?.customerTypeMerge ?? Customer.defaultCustomerTypeMerge,
      phoneT24: data?.phoneT24 ?? Customer.defaultPhoneT24,
      phoneEmb: data?.phoneEmb ?? Customer.defaultPhoneEmb,
      phoneSms: data?.phoneSms ?? Customer.defaultPhoneSms,
      phoneWay4: data?.phoneWay4 ?? Customer.defaultPhoneWay4,
      emailT24: data?.emailT24 ?? Customer.defaultEmailT24,
      emailEmb: data?.emailEmb ?? Customer.defaultEmailEmb,
      emailWay4: data?.emailWay4 ?? Customer.defaultEmailWay4,
      accountOfficer: data?.accountOfficer ?? Customer.defaultAccountOfficer,
      createdBy: data?.createdBy ?? Customer.defaultCreatedBy,
      updatedBy: data?.updatedBy ?? Customer.defaultUpdatedBy,
    );
  }

  @override
  ApiCustomerData mapToData(Customer entity){
    return ApiCustomerData(
      customerCode: entity.customerCode,
      address: entity.address,
      branchCode: entity.branchCode,
      businessRegistrationNumber: entity.businessRegistrationNumber,
      customerName: entity.customerName,
      classification: entity.classification,
      customerType: entity.customerType,
      dateOfBirth: entity.dateOfBirth,
      district: entity.district,
      gender: entity.gender,
      idCard: entity.idCard,
      industry: entity.industry,
      nationCode: entity.nationCode,
      province: entity.province,
      sector: entity.sector,
      segment: entity.segment,
      status: entity.status,
      swiftCode: entity.swiftCode,
      taxCode: entity.taxCode,
      transactionFrequency: entity.transactionFrequency,
      customerTypeSector: entity.customerTypeSector,
      customerTypeMerge: entity.customerTypeMerge,
      phoneT24: entity.phoneT24,
      phoneEmb: entity.phoneEmb,
      phoneSms: entity.phoneSms,
      phoneWay4: entity.phoneWay4,
      emailT24: entity.emailT24,
      emailEmb: entity.emailEmb,
      emailWay4: entity.emailWay4,
      accountOfficer: entity.accountOfficer,
      createdBy: entity.createdBy,
      updatedBy: entity.updatedBy,
    );
  }
}
