import 'package:freezed_annotation/freezed_annotation.dart';

part 'api_block_data.freezed.dart';
part 'api_block_data.g.dart';

@freezed
class ApiBlockData with _$ApiBlockData {
  const ApiBlockData._();

  const factory ApiBlockData({
    @JsonKey(name: 'id') String? id,
    @JsonKey(name: 'code') String? code,
    @JsonKey(name: 'name') String? name,
    @JsonKey(name: 'isActive') bool? isActive,
    @JsonKey(name: 'customerType') String? customerType,
    @JsonKey(name: 'createdDate') String? createdDate,
    @JsonKey(name: 'createdBy') String? createdBy,
    @JsonKey(name: 'updatedDate') String? updatedDate,
    @JsonKey(name: 'updatedBy') String? updatedBy,
  }) = _ApiBlockData;

  factory ApiBlockData.fromJson(Map<String, dynamic> json) => _$ApiBlockDataFromJson(json);
}