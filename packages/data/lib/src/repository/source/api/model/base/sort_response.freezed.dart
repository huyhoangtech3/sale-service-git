// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'sort_response.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

SortResponse _$SortResponseFromJson(Map<String, dynamic> json) {
  return _SortResponse.fromJson(json);
}

/// @nodoc
mixin _$SortResponse {
  @JsonKey(name: 'unsorted')
  bool? get unsorted => throw _privateConstructorUsedError;
  @JsonKey(name: 'sorted')
  bool? get sorted => throw _privateConstructorUsedError;
  @JsonKey(name: 'empty')
  bool? get empty => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $SortResponseCopyWith<SortResponse> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SortResponseCopyWith<$Res> {
  factory $SortResponseCopyWith(
          SortResponse value, $Res Function(SortResponse) then) =
      _$SortResponseCopyWithImpl<$Res, SortResponse>;
  @useResult
  $Res call(
      {@JsonKey(name: 'unsorted') bool? unsorted,
      @JsonKey(name: 'sorted') bool? sorted,
      @JsonKey(name: 'empty') bool? empty});
}

/// @nodoc
class _$SortResponseCopyWithImpl<$Res, $Val extends SortResponse>
    implements $SortResponseCopyWith<$Res> {
  _$SortResponseCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? unsorted = freezed,
    Object? sorted = freezed,
    Object? empty = freezed,
  }) {
    return _then(_value.copyWith(
      unsorted: freezed == unsorted
          ? _value.unsorted
          : unsorted // ignore: cast_nullable_to_non_nullable
              as bool?,
      sorted: freezed == sorted
          ? _value.sorted
          : sorted // ignore: cast_nullable_to_non_nullable
              as bool?,
      empty: freezed == empty
          ? _value.empty
          : empty // ignore: cast_nullable_to_non_nullable
              as bool?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$SortResponseImplCopyWith<$Res>
    implements $SortResponseCopyWith<$Res> {
  factory _$$SortResponseImplCopyWith(
          _$SortResponseImpl value, $Res Function(_$SortResponseImpl) then) =
      __$$SortResponseImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@JsonKey(name: 'unsorted') bool? unsorted,
      @JsonKey(name: 'sorted') bool? sorted,
      @JsonKey(name: 'empty') bool? empty});
}

/// @nodoc
class __$$SortResponseImplCopyWithImpl<$Res>
    extends _$SortResponseCopyWithImpl<$Res, _$SortResponseImpl>
    implements _$$SortResponseImplCopyWith<$Res> {
  __$$SortResponseImplCopyWithImpl(
      _$SortResponseImpl _value, $Res Function(_$SortResponseImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? unsorted = freezed,
    Object? sorted = freezed,
    Object? empty = freezed,
  }) {
    return _then(_$SortResponseImpl(
      unsorted: freezed == unsorted
          ? _value.unsorted
          : unsorted // ignore: cast_nullable_to_non_nullable
              as bool?,
      sorted: freezed == sorted
          ? _value.sorted
          : sorted // ignore: cast_nullable_to_non_nullable
              as bool?,
      empty: freezed == empty
          ? _value.empty
          : empty // ignore: cast_nullable_to_non_nullable
              as bool?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$SortResponseImpl extends _SortResponse {
  const _$SortResponseImpl(
      {@JsonKey(name: 'unsorted') this.unsorted,
      @JsonKey(name: 'sorted') this.sorted,
      @JsonKey(name: 'empty') this.empty})
      : super._();

  factory _$SortResponseImpl.fromJson(Map<String, dynamic> json) =>
      _$$SortResponseImplFromJson(json);

  @override
  @JsonKey(name: 'unsorted')
  final bool? unsorted;
  @override
  @JsonKey(name: 'sorted')
  final bool? sorted;
  @override
  @JsonKey(name: 'empty')
  final bool? empty;

  @override
  String toString() {
    return 'SortResponse(unsorted: $unsorted, sorted: $sorted, empty: $empty)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SortResponseImpl &&
            (identical(other.unsorted, unsorted) ||
                other.unsorted == unsorted) &&
            (identical(other.sorted, sorted) || other.sorted == sorted) &&
            (identical(other.empty, empty) || other.empty == empty));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, unsorted, sorted, empty);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$SortResponseImplCopyWith<_$SortResponseImpl> get copyWith =>
      __$$SortResponseImplCopyWithImpl<_$SortResponseImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$SortResponseImplToJson(
      this,
    );
  }
}

abstract class _SortResponse extends SortResponse {
  const factory _SortResponse(
      {@JsonKey(name: 'unsorted') final bool? unsorted,
      @JsonKey(name: 'sorted') final bool? sorted,
      @JsonKey(name: 'empty') final bool? empty}) = _$SortResponseImpl;
  const _SortResponse._() : super._();

  factory _SortResponse.fromJson(Map<String, dynamic> json) =
      _$SortResponseImpl.fromJson;

  @override
  @JsonKey(name: 'unsorted')
  bool? get unsorted;
  @override
  @JsonKey(name: 'sorted')
  bool? get sorted;
  @override
  @JsonKey(name: 'empty')
  bool? get empty;
  @override
  @JsonKey(ignore: true)
  _$$SortResponseImplCopyWith<_$SortResponseImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
