import 'package:freezed_annotation/freezed_annotation.dart';

import 'package:data/data.dart';

part 'list_response.freezed.dart';
part 'list_response.g.dart';

@Freezed(genericArgumentFactories: true)
class ListResponse<T> with _$ListResponse<T> {
  const ListResponse._();

  const factory ListResponse({
    @JsonKey(name: 'content') List<T>? content,
    @JsonKey(name: 'totalPages') int? totalPages,
    @JsonKey(name: 'last') bool? last,
    @JsonKey(name: 'totalElements') int? totalElements,
    @JsonKey(name: 'first') bool? first,
    @JsonKey(name: 'numberOfElements') int? numberOfElements,
    @JsonKey(name: 'number') int? number,
    @JsonKey(name: 'size') int? size,
    @JsonKey(name: 'empty') bool? empty,
    @JsonKey(name: 'sort') SortResponse? sort,
    @JsonKey(name: 'pageable') PageableResponse? pageable,
  }) = _ListResponse;

  factory ListResponse.fromJson(Map<String, dynamic> json, T Function(Object?) fromJsonT) =>
      _$ListResponseFromJson(json, fromJsonT);
}
