// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'list_response.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

ListResponse<T> _$ListResponseFromJson<T>(
    Map<String, dynamic> json, T Function(Object?) fromJsonT) {
  return _ListResponse<T>.fromJson(json, fromJsonT);
}

/// @nodoc
mixin _$ListResponse<T> {
  @JsonKey(name: 'content')
  List<T>? get content => throw _privateConstructorUsedError;
  @JsonKey(name: 'totalPages')
  int? get totalPages => throw _privateConstructorUsedError;
  @JsonKey(name: 'last')
  bool? get last => throw _privateConstructorUsedError;
  @JsonKey(name: 'totalElements')
  int? get totalElements => throw _privateConstructorUsedError;
  @JsonKey(name: 'first')
  bool? get first => throw _privateConstructorUsedError;
  @JsonKey(name: 'numberOfElements')
  int? get numberOfElements => throw _privateConstructorUsedError;
  @JsonKey(name: 'number')
  int? get number => throw _privateConstructorUsedError;
  @JsonKey(name: 'size')
  int? get size => throw _privateConstructorUsedError;
  @JsonKey(name: 'empty')
  bool? get empty => throw _privateConstructorUsedError;
  @JsonKey(name: 'sort')
  SortResponse? get sort => throw _privateConstructorUsedError;
  @JsonKey(name: 'pageable')
  PageableResponse? get pageable => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson(Object? Function(T) toJsonT) =>
      throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ListResponseCopyWith<T, ListResponse<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ListResponseCopyWith<T, $Res> {
  factory $ListResponseCopyWith(
          ListResponse<T> value, $Res Function(ListResponse<T>) then) =
      _$ListResponseCopyWithImpl<T, $Res, ListResponse<T>>;
  @useResult
  $Res call(
      {@JsonKey(name: 'content') List<T>? content,
      @JsonKey(name: 'totalPages') int? totalPages,
      @JsonKey(name: 'last') bool? last,
      @JsonKey(name: 'totalElements') int? totalElements,
      @JsonKey(name: 'first') bool? first,
      @JsonKey(name: 'numberOfElements') int? numberOfElements,
      @JsonKey(name: 'number') int? number,
      @JsonKey(name: 'size') int? size,
      @JsonKey(name: 'empty') bool? empty,
      @JsonKey(name: 'sort') SortResponse? sort,
      @JsonKey(name: 'pageable') PageableResponse? pageable});

  $SortResponseCopyWith<$Res>? get sort;
  $PageableResponseCopyWith<$Res>? get pageable;
}

/// @nodoc
class _$ListResponseCopyWithImpl<T, $Res, $Val extends ListResponse<T>>
    implements $ListResponseCopyWith<T, $Res> {
  _$ListResponseCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? content = freezed,
    Object? totalPages = freezed,
    Object? last = freezed,
    Object? totalElements = freezed,
    Object? first = freezed,
    Object? numberOfElements = freezed,
    Object? number = freezed,
    Object? size = freezed,
    Object? empty = freezed,
    Object? sort = freezed,
    Object? pageable = freezed,
  }) {
    return _then(_value.copyWith(
      content: freezed == content
          ? _value.content
          : content // ignore: cast_nullable_to_non_nullable
              as List<T>?,
      totalPages: freezed == totalPages
          ? _value.totalPages
          : totalPages // ignore: cast_nullable_to_non_nullable
              as int?,
      last: freezed == last
          ? _value.last
          : last // ignore: cast_nullable_to_non_nullable
              as bool?,
      totalElements: freezed == totalElements
          ? _value.totalElements
          : totalElements // ignore: cast_nullable_to_non_nullable
              as int?,
      first: freezed == first
          ? _value.first
          : first // ignore: cast_nullable_to_non_nullable
              as bool?,
      numberOfElements: freezed == numberOfElements
          ? _value.numberOfElements
          : numberOfElements // ignore: cast_nullable_to_non_nullable
              as int?,
      number: freezed == number
          ? _value.number
          : number // ignore: cast_nullable_to_non_nullable
              as int?,
      size: freezed == size
          ? _value.size
          : size // ignore: cast_nullable_to_non_nullable
              as int?,
      empty: freezed == empty
          ? _value.empty
          : empty // ignore: cast_nullable_to_non_nullable
              as bool?,
      sort: freezed == sort
          ? _value.sort
          : sort // ignore: cast_nullable_to_non_nullable
              as SortResponse?,
      pageable: freezed == pageable
          ? _value.pageable
          : pageable // ignore: cast_nullable_to_non_nullable
              as PageableResponse?,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $SortResponseCopyWith<$Res>? get sort {
    if (_value.sort == null) {
      return null;
    }

    return $SortResponseCopyWith<$Res>(_value.sort!, (value) {
      return _then(_value.copyWith(sort: value) as $Val);
    });
  }

  @override
  @pragma('vm:prefer-inline')
  $PageableResponseCopyWith<$Res>? get pageable {
    if (_value.pageable == null) {
      return null;
    }

    return $PageableResponseCopyWith<$Res>(_value.pageable!, (value) {
      return _then(_value.copyWith(pageable: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$ListResponseImplCopyWith<T, $Res>
    implements $ListResponseCopyWith<T, $Res> {
  factory _$$ListResponseImplCopyWith(_$ListResponseImpl<T> value,
          $Res Function(_$ListResponseImpl<T>) then) =
      __$$ListResponseImplCopyWithImpl<T, $Res>;
  @override
  @useResult
  $Res call(
      {@JsonKey(name: 'content') List<T>? content,
      @JsonKey(name: 'totalPages') int? totalPages,
      @JsonKey(name: 'last') bool? last,
      @JsonKey(name: 'totalElements') int? totalElements,
      @JsonKey(name: 'first') bool? first,
      @JsonKey(name: 'numberOfElements') int? numberOfElements,
      @JsonKey(name: 'number') int? number,
      @JsonKey(name: 'size') int? size,
      @JsonKey(name: 'empty') bool? empty,
      @JsonKey(name: 'sort') SortResponse? sort,
      @JsonKey(name: 'pageable') PageableResponse? pageable});

  @override
  $SortResponseCopyWith<$Res>? get sort;
  @override
  $PageableResponseCopyWith<$Res>? get pageable;
}

/// @nodoc
class __$$ListResponseImplCopyWithImpl<T, $Res>
    extends _$ListResponseCopyWithImpl<T, $Res, _$ListResponseImpl<T>>
    implements _$$ListResponseImplCopyWith<T, $Res> {
  __$$ListResponseImplCopyWithImpl(
      _$ListResponseImpl<T> _value, $Res Function(_$ListResponseImpl<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? content = freezed,
    Object? totalPages = freezed,
    Object? last = freezed,
    Object? totalElements = freezed,
    Object? first = freezed,
    Object? numberOfElements = freezed,
    Object? number = freezed,
    Object? size = freezed,
    Object? empty = freezed,
    Object? sort = freezed,
    Object? pageable = freezed,
  }) {
    return _then(_$ListResponseImpl<T>(
      content: freezed == content
          ? _value._content
          : content // ignore: cast_nullable_to_non_nullable
              as List<T>?,
      totalPages: freezed == totalPages
          ? _value.totalPages
          : totalPages // ignore: cast_nullable_to_non_nullable
              as int?,
      last: freezed == last
          ? _value.last
          : last // ignore: cast_nullable_to_non_nullable
              as bool?,
      totalElements: freezed == totalElements
          ? _value.totalElements
          : totalElements // ignore: cast_nullable_to_non_nullable
              as int?,
      first: freezed == first
          ? _value.first
          : first // ignore: cast_nullable_to_non_nullable
              as bool?,
      numberOfElements: freezed == numberOfElements
          ? _value.numberOfElements
          : numberOfElements // ignore: cast_nullable_to_non_nullable
              as int?,
      number: freezed == number
          ? _value.number
          : number // ignore: cast_nullable_to_non_nullable
              as int?,
      size: freezed == size
          ? _value.size
          : size // ignore: cast_nullable_to_non_nullable
              as int?,
      empty: freezed == empty
          ? _value.empty
          : empty // ignore: cast_nullable_to_non_nullable
              as bool?,
      sort: freezed == sort
          ? _value.sort
          : sort // ignore: cast_nullable_to_non_nullable
              as SortResponse?,
      pageable: freezed == pageable
          ? _value.pageable
          : pageable // ignore: cast_nullable_to_non_nullable
              as PageableResponse?,
    ));
  }
}

/// @nodoc
@JsonSerializable(genericArgumentFactories: true)
class _$ListResponseImpl<T> extends _ListResponse<T> {
  const _$ListResponseImpl(
      {@JsonKey(name: 'content') final List<T>? content,
      @JsonKey(name: 'totalPages') this.totalPages,
      @JsonKey(name: 'last') this.last,
      @JsonKey(name: 'totalElements') this.totalElements,
      @JsonKey(name: 'first') this.first,
      @JsonKey(name: 'numberOfElements') this.numberOfElements,
      @JsonKey(name: 'number') this.number,
      @JsonKey(name: 'size') this.size,
      @JsonKey(name: 'empty') this.empty,
      @JsonKey(name: 'sort') this.sort,
      @JsonKey(name: 'pageable') this.pageable})
      : _content = content,
        super._();

  factory _$ListResponseImpl.fromJson(
          Map<String, dynamic> json, T Function(Object?) fromJsonT) =>
      _$$ListResponseImplFromJson(json, fromJsonT);

  final List<T>? _content;
  @override
  @JsonKey(name: 'content')
  List<T>? get content {
    final value = _content;
    if (value == null) return null;
    if (_content is EqualUnmodifiableListView) return _content;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  @JsonKey(name: 'totalPages')
  final int? totalPages;
  @override
  @JsonKey(name: 'last')
  final bool? last;
  @override
  @JsonKey(name: 'totalElements')
  final int? totalElements;
  @override
  @JsonKey(name: 'first')
  final bool? first;
  @override
  @JsonKey(name: 'numberOfElements')
  final int? numberOfElements;
  @override
  @JsonKey(name: 'number')
  final int? number;
  @override
  @JsonKey(name: 'size')
  final int? size;
  @override
  @JsonKey(name: 'empty')
  final bool? empty;
  @override
  @JsonKey(name: 'sort')
  final SortResponse? sort;
  @override
  @JsonKey(name: 'pageable')
  final PageableResponse? pageable;

  @override
  String toString() {
    return 'ListResponse<$T>(content: $content, totalPages: $totalPages, last: $last, totalElements: $totalElements, first: $first, numberOfElements: $numberOfElements, number: $number, size: $size, empty: $empty, sort: $sort, pageable: $pageable)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ListResponseImpl<T> &&
            const DeepCollectionEquality().equals(other._content, _content) &&
            (identical(other.totalPages, totalPages) ||
                other.totalPages == totalPages) &&
            (identical(other.last, last) || other.last == last) &&
            (identical(other.totalElements, totalElements) ||
                other.totalElements == totalElements) &&
            (identical(other.first, first) || other.first == first) &&
            (identical(other.numberOfElements, numberOfElements) ||
                other.numberOfElements == numberOfElements) &&
            (identical(other.number, number) || other.number == number) &&
            (identical(other.size, size) || other.size == size) &&
            (identical(other.empty, empty) || other.empty == empty) &&
            (identical(other.sort, sort) || other.sort == sort) &&
            (identical(other.pageable, pageable) ||
                other.pageable == pageable));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_content),
      totalPages,
      last,
      totalElements,
      first,
      numberOfElements,
      number,
      size,
      empty,
      sort,
      pageable);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ListResponseImplCopyWith<T, _$ListResponseImpl<T>> get copyWith =>
      __$$ListResponseImplCopyWithImpl<T, _$ListResponseImpl<T>>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson(Object? Function(T) toJsonT) {
    return _$$ListResponseImplToJson<T>(this, toJsonT);
  }
}

abstract class _ListResponse<T> extends ListResponse<T> {
  const factory _ListResponse(
          {@JsonKey(name: 'content') final List<T>? content,
          @JsonKey(name: 'totalPages') final int? totalPages,
          @JsonKey(name: 'last') final bool? last,
          @JsonKey(name: 'totalElements') final int? totalElements,
          @JsonKey(name: 'first') final bool? first,
          @JsonKey(name: 'numberOfElements') final int? numberOfElements,
          @JsonKey(name: 'number') final int? number,
          @JsonKey(name: 'size') final int? size,
          @JsonKey(name: 'empty') final bool? empty,
          @JsonKey(name: 'sort') final SortResponse? sort,
          @JsonKey(name: 'pageable') final PageableResponse? pageable}) =
      _$ListResponseImpl<T>;
  const _ListResponse._() : super._();

  factory _ListResponse.fromJson(
          Map<String, dynamic> json, T Function(Object?) fromJsonT) =
      _$ListResponseImpl<T>.fromJson;

  @override
  @JsonKey(name: 'content')
  List<T>? get content;
  @override
  @JsonKey(name: 'totalPages')
  int? get totalPages;
  @override
  @JsonKey(name: 'last')
  bool? get last;
  @override
  @JsonKey(name: 'totalElements')
  int? get totalElements;
  @override
  @JsonKey(name: 'first')
  bool? get first;
  @override
  @JsonKey(name: 'numberOfElements')
  int? get numberOfElements;
  @override
  @JsonKey(name: 'number')
  int? get number;
  @override
  @JsonKey(name: 'size')
  int? get size;
  @override
  @JsonKey(name: 'empty')
  bool? get empty;
  @override
  @JsonKey(name: 'sort')
  SortResponse? get sort;
  @override
  @JsonKey(name: 'pageable')
  PageableResponse? get pageable;
  @override
  @JsonKey(ignore: true)
  _$$ListResponseImplCopyWith<T, _$ListResponseImpl<T>> get copyWith =>
      throw _privateConstructorUsedError;
}
