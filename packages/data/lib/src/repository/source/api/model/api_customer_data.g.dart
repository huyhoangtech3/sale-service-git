// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'api_customer_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$ApiCustomerDataImpl _$$ApiCustomerDataImplFromJson(
        Map<String, dynamic> json) =>
    _$ApiCustomerDataImpl(
      customerCode: json['customerCode'] as String?,
      address: json['address'] as String?,
      branchCode: json['branchCode'] as String?,
      businessRegistrationNumber: json['businessRegistrationNumber'] as String?,
      customerName: json['customerName'] as String?,
      classification: json['classification'] as String?,
      customerType: json['customerType'] as String?,
      dateOfBirth: json['dateOfBirth'] as String?,
      district: json['district'] as String?,
      gender: json['gender'] as String?,
      idCard: json['idCard'] as String?,
      industry: json['industry'] as String?,
      nationCode: json['nationCode'] as String?,
      province: json['province'] as String?,
      sector: json['sector'] as String?,
      segment: json['segment'] as String?,
      status: json['status'] as String?,
      swiftCode: json['swiftCode'] as String?,
      taxCode: json['taxCode'] as String?,
      transactionFrequency: json['transactionFrequency'] as String?,
      customerTypeSector: json['customerTypeSector'] as String?,
      customerTypeMerge: json['customerTypeMerge'] as String?,
      phoneT24: json['phoneT24'] as String?,
      phoneEmb: json['phoneEmb'] as String?,
      phoneSms: json['phoneSms'] as String?,
      phoneWay4: json['phoneWay4'] as String?,
      emailT24: json['emailT24'] as String?,
      emailEmb: json['emailEmb'] as String?,
      emailWay4: json['emailWay4'] as String?,
      accountOfficer: json['accountOfficer'] as String?,
      createdBy: json['createdBy'] as String?,
      updatedBy: json['updatedBy'] as String?,
    );

Map<String, dynamic> _$$ApiCustomerDataImplToJson(
        _$ApiCustomerDataImpl instance) =>
    <String, dynamic>{
      'customerCode': instance.customerCode,
      'address': instance.address,
      'branchCode': instance.branchCode,
      'businessRegistrationNumber': instance.businessRegistrationNumber,
      'customerName': instance.customerName,
      'classification': instance.classification,
      'customerType': instance.customerType,
      'dateOfBirth': instance.dateOfBirth,
      'district': instance.district,
      'gender': instance.gender,
      'idCard': instance.idCard,
      'industry': instance.industry,
      'nationCode': instance.nationCode,
      'province': instance.province,
      'sector': instance.sector,
      'segment': instance.segment,
      'status': instance.status,
      'swiftCode': instance.swiftCode,
      'taxCode': instance.taxCode,
      'transactionFrequency': instance.transactionFrequency,
      'customerTypeSector': instance.customerTypeSector,
      'customerTypeMerge': instance.customerTypeMerge,
      'phoneT24': instance.phoneT24,
      'phoneEmb': instance.phoneEmb,
      'phoneSms': instance.phoneSms,
      'phoneWay4': instance.phoneWay4,
      'emailT24': instance.emailT24,
      'emailEmb': instance.emailEmb,
      'emailWay4': instance.emailWay4,
      'accountOfficer': instance.accountOfficer,
      'createdBy': instance.createdBy,
      'updatedBy': instance.updatedBy,
    };
