import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:data/data.dart';

part 'api_customer_data.freezed.dart';
part 'api_customer_data.g.dart';

@freezed
class ApiCustomerData with _$ApiCustomerData {
  const ApiCustomerData._();

  const factory ApiCustomerData({
    @JsonKey(name: 'customerCode') String? customerCode,
    @JsonKey(name: 'address') String? address,
    @JsonKey(name: 'branchCode') String? branchCode,
    @JsonKey(name: 'businessRegistrationNumber') String? businessRegistrationNumber,
    @JsonKey(name: 'customerName') String? customerName,
    @JsonKey(name: 'classification') String? classification,
    @JsonKey(name: 'customerType') String? customerType,
    @JsonKey(name: 'dateOfBirth') String? dateOfBirth,
    @JsonKey(name: 'district') String? district,
    @JsonKey(name: 'gender') String? gender,
    @JsonKey(name: 'idCard') String? idCard,
    @JsonKey(name: 'industry') String? industry,
    @JsonKey(name: 'nationCode') String? nationCode,
    @JsonKey(name: 'province') String? province,
    @JsonKey(name: 'sector') String? sector,
    @JsonKey(name: 'segment') String? segment,
    @JsonKey(name: 'status') String? status,
    @JsonKey(name: 'swiftCode') String? swiftCode,
    @JsonKey(name: 'taxCode') String? taxCode,
    @JsonKey(name: 'transactionFrequency') String? transactionFrequency,
    @JsonKey(name: 'customerTypeSector') String? customerTypeSector,
    @JsonKey(name: 'customerTypeMerge') String? customerTypeMerge,
    @JsonKey(name: 'phoneT24') String? phoneT24,
    @JsonKey(name: 'phoneEmb') String? phoneEmb,
    @JsonKey(name: 'phoneSms') String? phoneSms,
    @JsonKey(name: 'phoneWay4') String? phoneWay4,
    @JsonKey(name: 'emailT24') String? emailT24,
    @JsonKey(name: 'emailEmb') String? emailEmb,
    @JsonKey(name: 'emailWay4') String? emailWay4,
    @JsonKey(name: 'accountOfficer') String? accountOfficer,
    @JsonKey(name: 'createdBy') String? createdBy,
    @JsonKey(name: 'updatedBy') String? updatedBy,
  }) = _ApiCustomerData;

  factory ApiCustomerData.fromJson(Map<String, dynamic> json) => _$ApiCustomerDataFromJson(json);
}
