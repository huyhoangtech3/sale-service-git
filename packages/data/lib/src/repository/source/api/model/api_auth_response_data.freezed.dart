// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'api_auth_response_data.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

ApiAuthResponseData _$ApiAuthResponseDataFromJson(Map<String, dynamic> json) {
  return _ApiAuthResponseData.fromJson(json);
}

/// @nodoc
mixin _$ApiAuthResponseData {
  @JsonKey(name: 'access_token')
  String? get accessToken => throw _privateConstructorUsedError;
  @JsonKey(name: 'expires_in')
  int? get expiresIn => throw _privateConstructorUsedError;
  @JsonKey(name: 'refresh_token')
  String? get refreshToken => throw _privateConstructorUsedError;
  @JsonKey(name: 'refresh_expires_in')
  int? get refreshExpiresIn => throw _privateConstructorUsedError;
  @JsonKey(name: 'token_type')
  String? get tokenType => throw _privateConstructorUsedError;
  @JsonKey(name: 'scope')
  String? get scope => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ApiAuthResponseDataCopyWith<ApiAuthResponseData> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ApiAuthResponseDataCopyWith<$Res> {
  factory $ApiAuthResponseDataCopyWith(
          ApiAuthResponseData value, $Res Function(ApiAuthResponseData) then) =
      _$ApiAuthResponseDataCopyWithImpl<$Res, ApiAuthResponseData>;
  @useResult
  $Res call(
      {@JsonKey(name: 'access_token') String? accessToken,
      @JsonKey(name: 'expires_in') int? expiresIn,
      @JsonKey(name: 'refresh_token') String? refreshToken,
      @JsonKey(name: 'refresh_expires_in') int? refreshExpiresIn,
      @JsonKey(name: 'token_type') String? tokenType,
      @JsonKey(name: 'scope') String? scope});
}

/// @nodoc
class _$ApiAuthResponseDataCopyWithImpl<$Res, $Val extends ApiAuthResponseData>
    implements $ApiAuthResponseDataCopyWith<$Res> {
  _$ApiAuthResponseDataCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? accessToken = freezed,
    Object? expiresIn = freezed,
    Object? refreshToken = freezed,
    Object? refreshExpiresIn = freezed,
    Object? tokenType = freezed,
    Object? scope = freezed,
  }) {
    return _then(_value.copyWith(
      accessToken: freezed == accessToken
          ? _value.accessToken
          : accessToken // ignore: cast_nullable_to_non_nullable
              as String?,
      expiresIn: freezed == expiresIn
          ? _value.expiresIn
          : expiresIn // ignore: cast_nullable_to_non_nullable
              as int?,
      refreshToken: freezed == refreshToken
          ? _value.refreshToken
          : refreshToken // ignore: cast_nullable_to_non_nullable
              as String?,
      refreshExpiresIn: freezed == refreshExpiresIn
          ? _value.refreshExpiresIn
          : refreshExpiresIn // ignore: cast_nullable_to_non_nullable
              as int?,
      tokenType: freezed == tokenType
          ? _value.tokenType
          : tokenType // ignore: cast_nullable_to_non_nullable
              as String?,
      scope: freezed == scope
          ? _value.scope
          : scope // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ApiAuthResponseDataImplCopyWith<$Res>
    implements $ApiAuthResponseDataCopyWith<$Res> {
  factory _$$ApiAuthResponseDataImplCopyWith(_$ApiAuthResponseDataImpl value,
          $Res Function(_$ApiAuthResponseDataImpl) then) =
      __$$ApiAuthResponseDataImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@JsonKey(name: 'access_token') String? accessToken,
      @JsonKey(name: 'expires_in') int? expiresIn,
      @JsonKey(name: 'refresh_token') String? refreshToken,
      @JsonKey(name: 'refresh_expires_in') int? refreshExpiresIn,
      @JsonKey(name: 'token_type') String? tokenType,
      @JsonKey(name: 'scope') String? scope});
}

/// @nodoc
class __$$ApiAuthResponseDataImplCopyWithImpl<$Res>
    extends _$ApiAuthResponseDataCopyWithImpl<$Res, _$ApiAuthResponseDataImpl>
    implements _$$ApiAuthResponseDataImplCopyWith<$Res> {
  __$$ApiAuthResponseDataImplCopyWithImpl(_$ApiAuthResponseDataImpl _value,
      $Res Function(_$ApiAuthResponseDataImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? accessToken = freezed,
    Object? expiresIn = freezed,
    Object? refreshToken = freezed,
    Object? refreshExpiresIn = freezed,
    Object? tokenType = freezed,
    Object? scope = freezed,
  }) {
    return _then(_$ApiAuthResponseDataImpl(
      accessToken: freezed == accessToken
          ? _value.accessToken
          : accessToken // ignore: cast_nullable_to_non_nullable
              as String?,
      expiresIn: freezed == expiresIn
          ? _value.expiresIn
          : expiresIn // ignore: cast_nullable_to_non_nullable
              as int?,
      refreshToken: freezed == refreshToken
          ? _value.refreshToken
          : refreshToken // ignore: cast_nullable_to_non_nullable
              as String?,
      refreshExpiresIn: freezed == refreshExpiresIn
          ? _value.refreshExpiresIn
          : refreshExpiresIn // ignore: cast_nullable_to_non_nullable
              as int?,
      tokenType: freezed == tokenType
          ? _value.tokenType
          : tokenType // ignore: cast_nullable_to_non_nullable
              as String?,
      scope: freezed == scope
          ? _value.scope
          : scope // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$ApiAuthResponseDataImpl implements _ApiAuthResponseData {
  const _$ApiAuthResponseDataImpl(
      {@JsonKey(name: 'access_token') this.accessToken,
      @JsonKey(name: 'expires_in') this.expiresIn,
      @JsonKey(name: 'refresh_token') this.refreshToken,
      @JsonKey(name: 'refresh_expires_in') this.refreshExpiresIn,
      @JsonKey(name: 'token_type') this.tokenType,
      @JsonKey(name: 'scope') this.scope});

  factory _$ApiAuthResponseDataImpl.fromJson(Map<String, dynamic> json) =>
      _$$ApiAuthResponseDataImplFromJson(json);

  @override
  @JsonKey(name: 'access_token')
  final String? accessToken;
  @override
  @JsonKey(name: 'expires_in')
  final int? expiresIn;
  @override
  @JsonKey(name: 'refresh_token')
  final String? refreshToken;
  @override
  @JsonKey(name: 'refresh_expires_in')
  final int? refreshExpiresIn;
  @override
  @JsonKey(name: 'token_type')
  final String? tokenType;
  @override
  @JsonKey(name: 'scope')
  final String? scope;

  @override
  String toString() {
    return 'ApiAuthResponseData(accessToken: $accessToken, expiresIn: $expiresIn, refreshToken: $refreshToken, refreshExpiresIn: $refreshExpiresIn, tokenType: $tokenType, scope: $scope)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ApiAuthResponseDataImpl &&
            (identical(other.accessToken, accessToken) ||
                other.accessToken == accessToken) &&
            (identical(other.expiresIn, expiresIn) ||
                other.expiresIn == expiresIn) &&
            (identical(other.refreshToken, refreshToken) ||
                other.refreshToken == refreshToken) &&
            (identical(other.refreshExpiresIn, refreshExpiresIn) ||
                other.refreshExpiresIn == refreshExpiresIn) &&
            (identical(other.tokenType, tokenType) ||
                other.tokenType == tokenType) &&
            (identical(other.scope, scope) || other.scope == scope));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, accessToken, expiresIn,
      refreshToken, refreshExpiresIn, tokenType, scope);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ApiAuthResponseDataImplCopyWith<_$ApiAuthResponseDataImpl> get copyWith =>
      __$$ApiAuthResponseDataImplCopyWithImpl<_$ApiAuthResponseDataImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$ApiAuthResponseDataImplToJson(
      this,
    );
  }
}

abstract class _ApiAuthResponseData implements ApiAuthResponseData {
  const factory _ApiAuthResponseData(
      {@JsonKey(name: 'access_token') final String? accessToken,
      @JsonKey(name: 'expires_in') final int? expiresIn,
      @JsonKey(name: 'refresh_token') final String? refreshToken,
      @JsonKey(name: 'refresh_expires_in') final int? refreshExpiresIn,
      @JsonKey(name: 'token_type') final String? tokenType,
      @JsonKey(name: 'scope') final String? scope}) = _$ApiAuthResponseDataImpl;

  factory _ApiAuthResponseData.fromJson(Map<String, dynamic> json) =
      _$ApiAuthResponseDataImpl.fromJson;

  @override
  @JsonKey(name: 'access_token')
  String? get accessToken;
  @override
  @JsonKey(name: 'expires_in')
  int? get expiresIn;
  @override
  @JsonKey(name: 'refresh_token')
  String? get refreshToken;
  @override
  @JsonKey(name: 'refresh_expires_in')
  int? get refreshExpiresIn;
  @override
  @JsonKey(name: 'token_type')
  String? get tokenType;
  @override
  @JsonKey(name: 'scope')
  String? get scope;
  @override
  @JsonKey(ignore: true)
  _$$ApiAuthResponseDataImplCopyWith<_$ApiAuthResponseDataImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
