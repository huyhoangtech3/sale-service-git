import 'package:freezed_annotation/freezed_annotation.dart';

part 'api_basic_user_Info_data.freezed.dart';
part 'api_basic_user_Info_data.g.dart';

@freezed
class ApiBasicUserInfoData with _$ApiBasicUserInfoData {
  const ApiBasicUserInfoData._();
  const factory ApiBasicUserInfoData({
    @JsonKey(name: 'id') String? id,
    @JsonKey(name: 'code') String? code,
    @JsonKey(name: 'hrsCode') String? hrsCode,
    @JsonKey(name: 'username') String? username,
    @JsonKey(name: 'fullName') String? fullName,
    @JsonKey(name: 'branch') String? branch,
    @JsonKey(name: 'phoneNumber') String? phoneNumber,
    @JsonKey(name: 'email') String? email,
    @JsonKey(name: 'note') String? note,
    @JsonKey(name: 'image') String? image,
    @JsonKey(name: 'branchName') String? branchName,
    @JsonKey(name: 'phoneDevice') String? phoneDevice,
    @JsonKey(name: 'reason') String? reason,
    @JsonKey(name: 'gender') String? gender,
    @JsonKey(name: 'unLock') bool? unLock,
    @JsonKey(name: 'statusWork') String? statusWork,
    @JsonKey(name: 'blockRmManager') List<String>? blockRmManager,
    @JsonKey(name: 'domainAll') bool? domainAll,
    @JsonKey(name: 'adminRole') bool? adminRole,
    @JsonKey(name: 'active') bool? active,
  }) = _ApiBasicUserInfoData;

  factory ApiBasicUserInfoData.fromJson(Map<String, dynamic> json) =>
      _$ApiBasicUserInfoDataFromJson(json);
}