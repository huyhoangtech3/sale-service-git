// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pageable_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$PageableResponseImpl _$$PageableResponseImplFromJson(
        Map<String, dynamic> json) =>
    _$PageableResponseImpl(
      sort: json['sort'] == null
          ? null
          : SortResponse.fromJson(json['sort'] as Map<String, dynamic>),
      pageNumber: (json['pageNumber'] as num?)?.toInt(),
      pageSize: (json['pageSize'] as num?)?.toInt(),
      offset: (json['offset'] as num?)?.toInt(),
      paged: json['paged'] as bool?,
      unpaged: json['unpaged'] as bool?,
    );

Map<String, dynamic> _$$PageableResponseImplToJson(
        _$PageableResponseImpl instance) =>
    <String, dynamic>{
      'sort': instance.sort,
      'pageNumber': instance.pageNumber,
      'pageSize': instance.pageSize,
      'offset': instance.offset,
      'paged': instance.paged,
      'unpaged': instance.unpaged,
    };
