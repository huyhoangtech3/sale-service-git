import 'package:freezed_annotation/freezed_annotation.dart';

part 'sort_response.freezed.dart';
part 'sort_response.g.dart';

@freezed
class SortResponse with _$SortResponse {
  const SortResponse._();

  const factory SortResponse({
    @JsonKey(name: 'unsorted') bool? unsorted,
    @JsonKey(name: 'sorted') bool? sorted,
    @JsonKey(name: 'empty') bool? empty,
  }) = _SortResponse;

  factory SortResponse.fromJson(Map<String, dynamic> json) =>
      _$SortResponseFromJson(json);
}
