// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'pageable_response.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

PageableResponse _$PageableResponseFromJson(Map<String, dynamic> json) {
  return _PageableResponse.fromJson(json);
}

/// @nodoc
mixin _$PageableResponse {
  @JsonKey(name: 'sort')
  SortResponse? get sort => throw _privateConstructorUsedError;
  @JsonKey(name: 'pageNumber')
  int? get pageNumber => throw _privateConstructorUsedError;
  @JsonKey(name: 'pageSize')
  int? get pageSize => throw _privateConstructorUsedError;
  @JsonKey(name: 'offset')
  int? get offset => throw _privateConstructorUsedError;
  @JsonKey(name: 'paged')
  bool? get paged => throw _privateConstructorUsedError;
  @JsonKey(name: 'unpaged')
  bool? get unpaged => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PageableResponseCopyWith<PageableResponse> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PageableResponseCopyWith<$Res> {
  factory $PageableResponseCopyWith(
          PageableResponse value, $Res Function(PageableResponse) then) =
      _$PageableResponseCopyWithImpl<$Res, PageableResponse>;
  @useResult
  $Res call(
      {@JsonKey(name: 'sort') SortResponse? sort,
      @JsonKey(name: 'pageNumber') int? pageNumber,
      @JsonKey(name: 'pageSize') int? pageSize,
      @JsonKey(name: 'offset') int? offset,
      @JsonKey(name: 'paged') bool? paged,
      @JsonKey(name: 'unpaged') bool? unpaged});

  $SortResponseCopyWith<$Res>? get sort;
}

/// @nodoc
class _$PageableResponseCopyWithImpl<$Res, $Val extends PageableResponse>
    implements $PageableResponseCopyWith<$Res> {
  _$PageableResponseCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? sort = freezed,
    Object? pageNumber = freezed,
    Object? pageSize = freezed,
    Object? offset = freezed,
    Object? paged = freezed,
    Object? unpaged = freezed,
  }) {
    return _then(_value.copyWith(
      sort: freezed == sort
          ? _value.sort
          : sort // ignore: cast_nullable_to_non_nullable
              as SortResponse?,
      pageNumber: freezed == pageNumber
          ? _value.pageNumber
          : pageNumber // ignore: cast_nullable_to_non_nullable
              as int?,
      pageSize: freezed == pageSize
          ? _value.pageSize
          : pageSize // ignore: cast_nullable_to_non_nullable
              as int?,
      offset: freezed == offset
          ? _value.offset
          : offset // ignore: cast_nullable_to_non_nullable
              as int?,
      paged: freezed == paged
          ? _value.paged
          : paged // ignore: cast_nullable_to_non_nullable
              as bool?,
      unpaged: freezed == unpaged
          ? _value.unpaged
          : unpaged // ignore: cast_nullable_to_non_nullable
              as bool?,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $SortResponseCopyWith<$Res>? get sort {
    if (_value.sort == null) {
      return null;
    }

    return $SortResponseCopyWith<$Res>(_value.sort!, (value) {
      return _then(_value.copyWith(sort: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$PageableResponseImplCopyWith<$Res>
    implements $PageableResponseCopyWith<$Res> {
  factory _$$PageableResponseImplCopyWith(_$PageableResponseImpl value,
          $Res Function(_$PageableResponseImpl) then) =
      __$$PageableResponseImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@JsonKey(name: 'sort') SortResponse? sort,
      @JsonKey(name: 'pageNumber') int? pageNumber,
      @JsonKey(name: 'pageSize') int? pageSize,
      @JsonKey(name: 'offset') int? offset,
      @JsonKey(name: 'paged') bool? paged,
      @JsonKey(name: 'unpaged') bool? unpaged});

  @override
  $SortResponseCopyWith<$Res>? get sort;
}

/// @nodoc
class __$$PageableResponseImplCopyWithImpl<$Res>
    extends _$PageableResponseCopyWithImpl<$Res, _$PageableResponseImpl>
    implements _$$PageableResponseImplCopyWith<$Res> {
  __$$PageableResponseImplCopyWithImpl(_$PageableResponseImpl _value,
      $Res Function(_$PageableResponseImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? sort = freezed,
    Object? pageNumber = freezed,
    Object? pageSize = freezed,
    Object? offset = freezed,
    Object? paged = freezed,
    Object? unpaged = freezed,
  }) {
    return _then(_$PageableResponseImpl(
      sort: freezed == sort
          ? _value.sort
          : sort // ignore: cast_nullable_to_non_nullable
              as SortResponse?,
      pageNumber: freezed == pageNumber
          ? _value.pageNumber
          : pageNumber // ignore: cast_nullable_to_non_nullable
              as int?,
      pageSize: freezed == pageSize
          ? _value.pageSize
          : pageSize // ignore: cast_nullable_to_non_nullable
              as int?,
      offset: freezed == offset
          ? _value.offset
          : offset // ignore: cast_nullable_to_non_nullable
              as int?,
      paged: freezed == paged
          ? _value.paged
          : paged // ignore: cast_nullable_to_non_nullable
              as bool?,
      unpaged: freezed == unpaged
          ? _value.unpaged
          : unpaged // ignore: cast_nullable_to_non_nullable
              as bool?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$PageableResponseImpl extends _PageableResponse {
  const _$PageableResponseImpl(
      {@JsonKey(name: 'sort') this.sort,
      @JsonKey(name: 'pageNumber') this.pageNumber,
      @JsonKey(name: 'pageSize') this.pageSize,
      @JsonKey(name: 'offset') this.offset,
      @JsonKey(name: 'paged') this.paged,
      @JsonKey(name: 'unpaged') this.unpaged})
      : super._();

  factory _$PageableResponseImpl.fromJson(Map<String, dynamic> json) =>
      _$$PageableResponseImplFromJson(json);

  @override
  @JsonKey(name: 'sort')
  final SortResponse? sort;
  @override
  @JsonKey(name: 'pageNumber')
  final int? pageNumber;
  @override
  @JsonKey(name: 'pageSize')
  final int? pageSize;
  @override
  @JsonKey(name: 'offset')
  final int? offset;
  @override
  @JsonKey(name: 'paged')
  final bool? paged;
  @override
  @JsonKey(name: 'unpaged')
  final bool? unpaged;

  @override
  String toString() {
    return 'PageableResponse(sort: $sort, pageNumber: $pageNumber, pageSize: $pageSize, offset: $offset, paged: $paged, unpaged: $unpaged)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$PageableResponseImpl &&
            (identical(other.sort, sort) || other.sort == sort) &&
            (identical(other.pageNumber, pageNumber) ||
                other.pageNumber == pageNumber) &&
            (identical(other.pageSize, pageSize) ||
                other.pageSize == pageSize) &&
            (identical(other.offset, offset) || other.offset == offset) &&
            (identical(other.paged, paged) || other.paged == paged) &&
            (identical(other.unpaged, unpaged) || other.unpaged == unpaged));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType, sort, pageNumber, pageSize, offset, paged, unpaged);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$PageableResponseImplCopyWith<_$PageableResponseImpl> get copyWith =>
      __$$PageableResponseImplCopyWithImpl<_$PageableResponseImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$PageableResponseImplToJson(
      this,
    );
  }
}

abstract class _PageableResponse extends PageableResponse {
  const factory _PageableResponse(
      {@JsonKey(name: 'sort') final SortResponse? sort,
      @JsonKey(name: 'pageNumber') final int? pageNumber,
      @JsonKey(name: 'pageSize') final int? pageSize,
      @JsonKey(name: 'offset') final int? offset,
      @JsonKey(name: 'paged') final bool? paged,
      @JsonKey(name: 'unpaged') final bool? unpaged}) = _$PageableResponseImpl;
  const _PageableResponse._() : super._();

  factory _PageableResponse.fromJson(Map<String, dynamic> json) =
      _$PageableResponseImpl.fromJson;

  @override
  @JsonKey(name: 'sort')
  SortResponse? get sort;
  @override
  @JsonKey(name: 'pageNumber')
  int? get pageNumber;
  @override
  @JsonKey(name: 'pageSize')
  int? get pageSize;
  @override
  @JsonKey(name: 'offset')
  int? get offset;
  @override
  @JsonKey(name: 'paged')
  bool? get paged;
  @override
  @JsonKey(name: 'unpaged')
  bool? get unpaged;
  @override
  @JsonKey(ignore: true)
  _$$PageableResponseImplCopyWith<_$PageableResponseImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
