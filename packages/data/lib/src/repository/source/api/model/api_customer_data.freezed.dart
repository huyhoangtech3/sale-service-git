// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'api_customer_data.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

ApiCustomerData _$ApiCustomerDataFromJson(Map<String, dynamic> json) {
  return _ApiCustomerData.fromJson(json);
}

/// @nodoc
mixin _$ApiCustomerData {
  @JsonKey(name: 'customerCode')
  String? get customerCode => throw _privateConstructorUsedError;
  @JsonKey(name: 'address')
  String? get address => throw _privateConstructorUsedError;
  @JsonKey(name: 'branchCode')
  String? get branchCode => throw _privateConstructorUsedError;
  @JsonKey(name: 'businessRegistrationNumber')
  String? get businessRegistrationNumber => throw _privateConstructorUsedError;
  @JsonKey(name: 'customerName')
  String? get customerName => throw _privateConstructorUsedError;
  @JsonKey(name: 'classification')
  String? get classification => throw _privateConstructorUsedError;
  @JsonKey(name: 'customerType')
  String? get customerType => throw _privateConstructorUsedError;
  @JsonKey(name: 'dateOfBirth')
  String? get dateOfBirth => throw _privateConstructorUsedError;
  @JsonKey(name: 'district')
  String? get district => throw _privateConstructorUsedError;
  @JsonKey(name: 'gender')
  String? get gender => throw _privateConstructorUsedError;
  @JsonKey(name: 'idCard')
  String? get idCard => throw _privateConstructorUsedError;
  @JsonKey(name: 'industry')
  String? get industry => throw _privateConstructorUsedError;
  @JsonKey(name: 'nationCode')
  String? get nationCode => throw _privateConstructorUsedError;
  @JsonKey(name: 'province')
  String? get province => throw _privateConstructorUsedError;
  @JsonKey(name: 'sector')
  String? get sector => throw _privateConstructorUsedError;
  @JsonKey(name: 'segment')
  String? get segment => throw _privateConstructorUsedError;
  @JsonKey(name: 'status')
  String? get status => throw _privateConstructorUsedError;
  @JsonKey(name: 'swiftCode')
  String? get swiftCode => throw _privateConstructorUsedError;
  @JsonKey(name: 'taxCode')
  String? get taxCode => throw _privateConstructorUsedError;
  @JsonKey(name: 'transactionFrequency')
  String? get transactionFrequency => throw _privateConstructorUsedError;
  @JsonKey(name: 'customerTypeSector')
  String? get customerTypeSector => throw _privateConstructorUsedError;
  @JsonKey(name: 'customerTypeMerge')
  String? get customerTypeMerge => throw _privateConstructorUsedError;
  @JsonKey(name: 'phoneT24')
  String? get phoneT24 => throw _privateConstructorUsedError;
  @JsonKey(name: 'phoneEmb')
  String? get phoneEmb => throw _privateConstructorUsedError;
  @JsonKey(name: 'phoneSms')
  String? get phoneSms => throw _privateConstructorUsedError;
  @JsonKey(name: 'phoneWay4')
  String? get phoneWay4 => throw _privateConstructorUsedError;
  @JsonKey(name: 'emailT24')
  String? get emailT24 => throw _privateConstructorUsedError;
  @JsonKey(name: 'emailEmb')
  String? get emailEmb => throw _privateConstructorUsedError;
  @JsonKey(name: 'emailWay4')
  String? get emailWay4 => throw _privateConstructorUsedError;
  @JsonKey(name: 'accountOfficer')
  String? get accountOfficer => throw _privateConstructorUsedError;
  @JsonKey(name: 'createdBy')
  String? get createdBy => throw _privateConstructorUsedError;
  @JsonKey(name: 'updatedBy')
  String? get updatedBy => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ApiCustomerDataCopyWith<ApiCustomerData> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ApiCustomerDataCopyWith<$Res> {
  factory $ApiCustomerDataCopyWith(
          ApiCustomerData value, $Res Function(ApiCustomerData) then) =
      _$ApiCustomerDataCopyWithImpl<$Res, ApiCustomerData>;
  @useResult
  $Res call(
      {@JsonKey(name: 'customerCode') String? customerCode,
      @JsonKey(name: 'address') String? address,
      @JsonKey(name: 'branchCode') String? branchCode,
      @JsonKey(name: 'businessRegistrationNumber')
      String? businessRegistrationNumber,
      @JsonKey(name: 'customerName') String? customerName,
      @JsonKey(name: 'classification') String? classification,
      @JsonKey(name: 'customerType') String? customerType,
      @JsonKey(name: 'dateOfBirth') String? dateOfBirth,
      @JsonKey(name: 'district') String? district,
      @JsonKey(name: 'gender') String? gender,
      @JsonKey(name: 'idCard') String? idCard,
      @JsonKey(name: 'industry') String? industry,
      @JsonKey(name: 'nationCode') String? nationCode,
      @JsonKey(name: 'province') String? province,
      @JsonKey(name: 'sector') String? sector,
      @JsonKey(name: 'segment') String? segment,
      @JsonKey(name: 'status') String? status,
      @JsonKey(name: 'swiftCode') String? swiftCode,
      @JsonKey(name: 'taxCode') String? taxCode,
      @JsonKey(name: 'transactionFrequency') String? transactionFrequency,
      @JsonKey(name: 'customerTypeSector') String? customerTypeSector,
      @JsonKey(name: 'customerTypeMerge') String? customerTypeMerge,
      @JsonKey(name: 'phoneT24') String? phoneT24,
      @JsonKey(name: 'phoneEmb') String? phoneEmb,
      @JsonKey(name: 'phoneSms') String? phoneSms,
      @JsonKey(name: 'phoneWay4') String? phoneWay4,
      @JsonKey(name: 'emailT24') String? emailT24,
      @JsonKey(name: 'emailEmb') String? emailEmb,
      @JsonKey(name: 'emailWay4') String? emailWay4,
      @JsonKey(name: 'accountOfficer') String? accountOfficer,
      @JsonKey(name: 'createdBy') String? createdBy,
      @JsonKey(name: 'updatedBy') String? updatedBy});
}

/// @nodoc
class _$ApiCustomerDataCopyWithImpl<$Res, $Val extends ApiCustomerData>
    implements $ApiCustomerDataCopyWith<$Res> {
  _$ApiCustomerDataCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? customerCode = freezed,
    Object? address = freezed,
    Object? branchCode = freezed,
    Object? businessRegistrationNumber = freezed,
    Object? customerName = freezed,
    Object? classification = freezed,
    Object? customerType = freezed,
    Object? dateOfBirth = freezed,
    Object? district = freezed,
    Object? gender = freezed,
    Object? idCard = freezed,
    Object? industry = freezed,
    Object? nationCode = freezed,
    Object? province = freezed,
    Object? sector = freezed,
    Object? segment = freezed,
    Object? status = freezed,
    Object? swiftCode = freezed,
    Object? taxCode = freezed,
    Object? transactionFrequency = freezed,
    Object? customerTypeSector = freezed,
    Object? customerTypeMerge = freezed,
    Object? phoneT24 = freezed,
    Object? phoneEmb = freezed,
    Object? phoneSms = freezed,
    Object? phoneWay4 = freezed,
    Object? emailT24 = freezed,
    Object? emailEmb = freezed,
    Object? emailWay4 = freezed,
    Object? accountOfficer = freezed,
    Object? createdBy = freezed,
    Object? updatedBy = freezed,
  }) {
    return _then(_value.copyWith(
      customerCode: freezed == customerCode
          ? _value.customerCode
          : customerCode // ignore: cast_nullable_to_non_nullable
              as String?,
      address: freezed == address
          ? _value.address
          : address // ignore: cast_nullable_to_non_nullable
              as String?,
      branchCode: freezed == branchCode
          ? _value.branchCode
          : branchCode // ignore: cast_nullable_to_non_nullable
              as String?,
      businessRegistrationNumber: freezed == businessRegistrationNumber
          ? _value.businessRegistrationNumber
          : businessRegistrationNumber // ignore: cast_nullable_to_non_nullable
              as String?,
      customerName: freezed == customerName
          ? _value.customerName
          : customerName // ignore: cast_nullable_to_non_nullable
              as String?,
      classification: freezed == classification
          ? _value.classification
          : classification // ignore: cast_nullable_to_non_nullable
              as String?,
      customerType: freezed == customerType
          ? _value.customerType
          : customerType // ignore: cast_nullable_to_non_nullable
              as String?,
      dateOfBirth: freezed == dateOfBirth
          ? _value.dateOfBirth
          : dateOfBirth // ignore: cast_nullable_to_non_nullable
              as String?,
      district: freezed == district
          ? _value.district
          : district // ignore: cast_nullable_to_non_nullable
              as String?,
      gender: freezed == gender
          ? _value.gender
          : gender // ignore: cast_nullable_to_non_nullable
              as String?,
      idCard: freezed == idCard
          ? _value.idCard
          : idCard // ignore: cast_nullable_to_non_nullable
              as String?,
      industry: freezed == industry
          ? _value.industry
          : industry // ignore: cast_nullable_to_non_nullable
              as String?,
      nationCode: freezed == nationCode
          ? _value.nationCode
          : nationCode // ignore: cast_nullable_to_non_nullable
              as String?,
      province: freezed == province
          ? _value.province
          : province // ignore: cast_nullable_to_non_nullable
              as String?,
      sector: freezed == sector
          ? _value.sector
          : sector // ignore: cast_nullable_to_non_nullable
              as String?,
      segment: freezed == segment
          ? _value.segment
          : segment // ignore: cast_nullable_to_non_nullable
              as String?,
      status: freezed == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as String?,
      swiftCode: freezed == swiftCode
          ? _value.swiftCode
          : swiftCode // ignore: cast_nullable_to_non_nullable
              as String?,
      taxCode: freezed == taxCode
          ? _value.taxCode
          : taxCode // ignore: cast_nullable_to_non_nullable
              as String?,
      transactionFrequency: freezed == transactionFrequency
          ? _value.transactionFrequency
          : transactionFrequency // ignore: cast_nullable_to_non_nullable
              as String?,
      customerTypeSector: freezed == customerTypeSector
          ? _value.customerTypeSector
          : customerTypeSector // ignore: cast_nullable_to_non_nullable
              as String?,
      customerTypeMerge: freezed == customerTypeMerge
          ? _value.customerTypeMerge
          : customerTypeMerge // ignore: cast_nullable_to_non_nullable
              as String?,
      phoneT24: freezed == phoneT24
          ? _value.phoneT24
          : phoneT24 // ignore: cast_nullable_to_non_nullable
              as String?,
      phoneEmb: freezed == phoneEmb
          ? _value.phoneEmb
          : phoneEmb // ignore: cast_nullable_to_non_nullable
              as String?,
      phoneSms: freezed == phoneSms
          ? _value.phoneSms
          : phoneSms // ignore: cast_nullable_to_non_nullable
              as String?,
      phoneWay4: freezed == phoneWay4
          ? _value.phoneWay4
          : phoneWay4 // ignore: cast_nullable_to_non_nullable
              as String?,
      emailT24: freezed == emailT24
          ? _value.emailT24
          : emailT24 // ignore: cast_nullable_to_non_nullable
              as String?,
      emailEmb: freezed == emailEmb
          ? _value.emailEmb
          : emailEmb // ignore: cast_nullable_to_non_nullable
              as String?,
      emailWay4: freezed == emailWay4
          ? _value.emailWay4
          : emailWay4 // ignore: cast_nullable_to_non_nullable
              as String?,
      accountOfficer: freezed == accountOfficer
          ? _value.accountOfficer
          : accountOfficer // ignore: cast_nullable_to_non_nullable
              as String?,
      createdBy: freezed == createdBy
          ? _value.createdBy
          : createdBy // ignore: cast_nullable_to_non_nullable
              as String?,
      updatedBy: freezed == updatedBy
          ? _value.updatedBy
          : updatedBy // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ApiCustomerDataImplCopyWith<$Res>
    implements $ApiCustomerDataCopyWith<$Res> {
  factory _$$ApiCustomerDataImplCopyWith(_$ApiCustomerDataImpl value,
          $Res Function(_$ApiCustomerDataImpl) then) =
      __$$ApiCustomerDataImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@JsonKey(name: 'customerCode') String? customerCode,
      @JsonKey(name: 'address') String? address,
      @JsonKey(name: 'branchCode') String? branchCode,
      @JsonKey(name: 'businessRegistrationNumber')
      String? businessRegistrationNumber,
      @JsonKey(name: 'customerName') String? customerName,
      @JsonKey(name: 'classification') String? classification,
      @JsonKey(name: 'customerType') String? customerType,
      @JsonKey(name: 'dateOfBirth') String? dateOfBirth,
      @JsonKey(name: 'district') String? district,
      @JsonKey(name: 'gender') String? gender,
      @JsonKey(name: 'idCard') String? idCard,
      @JsonKey(name: 'industry') String? industry,
      @JsonKey(name: 'nationCode') String? nationCode,
      @JsonKey(name: 'province') String? province,
      @JsonKey(name: 'sector') String? sector,
      @JsonKey(name: 'segment') String? segment,
      @JsonKey(name: 'status') String? status,
      @JsonKey(name: 'swiftCode') String? swiftCode,
      @JsonKey(name: 'taxCode') String? taxCode,
      @JsonKey(name: 'transactionFrequency') String? transactionFrequency,
      @JsonKey(name: 'customerTypeSector') String? customerTypeSector,
      @JsonKey(name: 'customerTypeMerge') String? customerTypeMerge,
      @JsonKey(name: 'phoneT24') String? phoneT24,
      @JsonKey(name: 'phoneEmb') String? phoneEmb,
      @JsonKey(name: 'phoneSms') String? phoneSms,
      @JsonKey(name: 'phoneWay4') String? phoneWay4,
      @JsonKey(name: 'emailT24') String? emailT24,
      @JsonKey(name: 'emailEmb') String? emailEmb,
      @JsonKey(name: 'emailWay4') String? emailWay4,
      @JsonKey(name: 'accountOfficer') String? accountOfficer,
      @JsonKey(name: 'createdBy') String? createdBy,
      @JsonKey(name: 'updatedBy') String? updatedBy});
}

/// @nodoc
class __$$ApiCustomerDataImplCopyWithImpl<$Res>
    extends _$ApiCustomerDataCopyWithImpl<$Res, _$ApiCustomerDataImpl>
    implements _$$ApiCustomerDataImplCopyWith<$Res> {
  __$$ApiCustomerDataImplCopyWithImpl(
      _$ApiCustomerDataImpl _value, $Res Function(_$ApiCustomerDataImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? customerCode = freezed,
    Object? address = freezed,
    Object? branchCode = freezed,
    Object? businessRegistrationNumber = freezed,
    Object? customerName = freezed,
    Object? classification = freezed,
    Object? customerType = freezed,
    Object? dateOfBirth = freezed,
    Object? district = freezed,
    Object? gender = freezed,
    Object? idCard = freezed,
    Object? industry = freezed,
    Object? nationCode = freezed,
    Object? province = freezed,
    Object? sector = freezed,
    Object? segment = freezed,
    Object? status = freezed,
    Object? swiftCode = freezed,
    Object? taxCode = freezed,
    Object? transactionFrequency = freezed,
    Object? customerTypeSector = freezed,
    Object? customerTypeMerge = freezed,
    Object? phoneT24 = freezed,
    Object? phoneEmb = freezed,
    Object? phoneSms = freezed,
    Object? phoneWay4 = freezed,
    Object? emailT24 = freezed,
    Object? emailEmb = freezed,
    Object? emailWay4 = freezed,
    Object? accountOfficer = freezed,
    Object? createdBy = freezed,
    Object? updatedBy = freezed,
  }) {
    return _then(_$ApiCustomerDataImpl(
      customerCode: freezed == customerCode
          ? _value.customerCode
          : customerCode // ignore: cast_nullable_to_non_nullable
              as String?,
      address: freezed == address
          ? _value.address
          : address // ignore: cast_nullable_to_non_nullable
              as String?,
      branchCode: freezed == branchCode
          ? _value.branchCode
          : branchCode // ignore: cast_nullable_to_non_nullable
              as String?,
      businessRegistrationNumber: freezed == businessRegistrationNumber
          ? _value.businessRegistrationNumber
          : businessRegistrationNumber // ignore: cast_nullable_to_non_nullable
              as String?,
      customerName: freezed == customerName
          ? _value.customerName
          : customerName // ignore: cast_nullable_to_non_nullable
              as String?,
      classification: freezed == classification
          ? _value.classification
          : classification // ignore: cast_nullable_to_non_nullable
              as String?,
      customerType: freezed == customerType
          ? _value.customerType
          : customerType // ignore: cast_nullable_to_non_nullable
              as String?,
      dateOfBirth: freezed == dateOfBirth
          ? _value.dateOfBirth
          : dateOfBirth // ignore: cast_nullable_to_non_nullable
              as String?,
      district: freezed == district
          ? _value.district
          : district // ignore: cast_nullable_to_non_nullable
              as String?,
      gender: freezed == gender
          ? _value.gender
          : gender // ignore: cast_nullable_to_non_nullable
              as String?,
      idCard: freezed == idCard
          ? _value.idCard
          : idCard // ignore: cast_nullable_to_non_nullable
              as String?,
      industry: freezed == industry
          ? _value.industry
          : industry // ignore: cast_nullable_to_non_nullable
              as String?,
      nationCode: freezed == nationCode
          ? _value.nationCode
          : nationCode // ignore: cast_nullable_to_non_nullable
              as String?,
      province: freezed == province
          ? _value.province
          : province // ignore: cast_nullable_to_non_nullable
              as String?,
      sector: freezed == sector
          ? _value.sector
          : sector // ignore: cast_nullable_to_non_nullable
              as String?,
      segment: freezed == segment
          ? _value.segment
          : segment // ignore: cast_nullable_to_non_nullable
              as String?,
      status: freezed == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as String?,
      swiftCode: freezed == swiftCode
          ? _value.swiftCode
          : swiftCode // ignore: cast_nullable_to_non_nullable
              as String?,
      taxCode: freezed == taxCode
          ? _value.taxCode
          : taxCode // ignore: cast_nullable_to_non_nullable
              as String?,
      transactionFrequency: freezed == transactionFrequency
          ? _value.transactionFrequency
          : transactionFrequency // ignore: cast_nullable_to_non_nullable
              as String?,
      customerTypeSector: freezed == customerTypeSector
          ? _value.customerTypeSector
          : customerTypeSector // ignore: cast_nullable_to_non_nullable
              as String?,
      customerTypeMerge: freezed == customerTypeMerge
          ? _value.customerTypeMerge
          : customerTypeMerge // ignore: cast_nullable_to_non_nullable
              as String?,
      phoneT24: freezed == phoneT24
          ? _value.phoneT24
          : phoneT24 // ignore: cast_nullable_to_non_nullable
              as String?,
      phoneEmb: freezed == phoneEmb
          ? _value.phoneEmb
          : phoneEmb // ignore: cast_nullable_to_non_nullable
              as String?,
      phoneSms: freezed == phoneSms
          ? _value.phoneSms
          : phoneSms // ignore: cast_nullable_to_non_nullable
              as String?,
      phoneWay4: freezed == phoneWay4
          ? _value.phoneWay4
          : phoneWay4 // ignore: cast_nullable_to_non_nullable
              as String?,
      emailT24: freezed == emailT24
          ? _value.emailT24
          : emailT24 // ignore: cast_nullable_to_non_nullable
              as String?,
      emailEmb: freezed == emailEmb
          ? _value.emailEmb
          : emailEmb // ignore: cast_nullable_to_non_nullable
              as String?,
      emailWay4: freezed == emailWay4
          ? _value.emailWay4
          : emailWay4 // ignore: cast_nullable_to_non_nullable
              as String?,
      accountOfficer: freezed == accountOfficer
          ? _value.accountOfficer
          : accountOfficer // ignore: cast_nullable_to_non_nullable
              as String?,
      createdBy: freezed == createdBy
          ? _value.createdBy
          : createdBy // ignore: cast_nullable_to_non_nullable
              as String?,
      updatedBy: freezed == updatedBy
          ? _value.updatedBy
          : updatedBy // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$ApiCustomerDataImpl extends _ApiCustomerData {
  const _$ApiCustomerDataImpl(
      {@JsonKey(name: 'customerCode') this.customerCode,
      @JsonKey(name: 'address') this.address,
      @JsonKey(name: 'branchCode') this.branchCode,
      @JsonKey(name: 'businessRegistrationNumber')
      this.businessRegistrationNumber,
      @JsonKey(name: 'customerName') this.customerName,
      @JsonKey(name: 'classification') this.classification,
      @JsonKey(name: 'customerType') this.customerType,
      @JsonKey(name: 'dateOfBirth') this.dateOfBirth,
      @JsonKey(name: 'district') this.district,
      @JsonKey(name: 'gender') this.gender,
      @JsonKey(name: 'idCard') this.idCard,
      @JsonKey(name: 'industry') this.industry,
      @JsonKey(name: 'nationCode') this.nationCode,
      @JsonKey(name: 'province') this.province,
      @JsonKey(name: 'sector') this.sector,
      @JsonKey(name: 'segment') this.segment,
      @JsonKey(name: 'status') this.status,
      @JsonKey(name: 'swiftCode') this.swiftCode,
      @JsonKey(name: 'taxCode') this.taxCode,
      @JsonKey(name: 'transactionFrequency') this.transactionFrequency,
      @JsonKey(name: 'customerTypeSector') this.customerTypeSector,
      @JsonKey(name: 'customerTypeMerge') this.customerTypeMerge,
      @JsonKey(name: 'phoneT24') this.phoneT24,
      @JsonKey(name: 'phoneEmb') this.phoneEmb,
      @JsonKey(name: 'phoneSms') this.phoneSms,
      @JsonKey(name: 'phoneWay4') this.phoneWay4,
      @JsonKey(name: 'emailT24') this.emailT24,
      @JsonKey(name: 'emailEmb') this.emailEmb,
      @JsonKey(name: 'emailWay4') this.emailWay4,
      @JsonKey(name: 'accountOfficer') this.accountOfficer,
      @JsonKey(name: 'createdBy') this.createdBy,
      @JsonKey(name: 'updatedBy') this.updatedBy})
      : super._();

  factory _$ApiCustomerDataImpl.fromJson(Map<String, dynamic> json) =>
      _$$ApiCustomerDataImplFromJson(json);

  @override
  @JsonKey(name: 'customerCode')
  final String? customerCode;
  @override
  @JsonKey(name: 'address')
  final String? address;
  @override
  @JsonKey(name: 'branchCode')
  final String? branchCode;
  @override
  @JsonKey(name: 'businessRegistrationNumber')
  final String? businessRegistrationNumber;
  @override
  @JsonKey(name: 'customerName')
  final String? customerName;
  @override
  @JsonKey(name: 'classification')
  final String? classification;
  @override
  @JsonKey(name: 'customerType')
  final String? customerType;
  @override
  @JsonKey(name: 'dateOfBirth')
  final String? dateOfBirth;
  @override
  @JsonKey(name: 'district')
  final String? district;
  @override
  @JsonKey(name: 'gender')
  final String? gender;
  @override
  @JsonKey(name: 'idCard')
  final String? idCard;
  @override
  @JsonKey(name: 'industry')
  final String? industry;
  @override
  @JsonKey(name: 'nationCode')
  final String? nationCode;
  @override
  @JsonKey(name: 'province')
  final String? province;
  @override
  @JsonKey(name: 'sector')
  final String? sector;
  @override
  @JsonKey(name: 'segment')
  final String? segment;
  @override
  @JsonKey(name: 'status')
  final String? status;
  @override
  @JsonKey(name: 'swiftCode')
  final String? swiftCode;
  @override
  @JsonKey(name: 'taxCode')
  final String? taxCode;
  @override
  @JsonKey(name: 'transactionFrequency')
  final String? transactionFrequency;
  @override
  @JsonKey(name: 'customerTypeSector')
  final String? customerTypeSector;
  @override
  @JsonKey(name: 'customerTypeMerge')
  final String? customerTypeMerge;
  @override
  @JsonKey(name: 'phoneT24')
  final String? phoneT24;
  @override
  @JsonKey(name: 'phoneEmb')
  final String? phoneEmb;
  @override
  @JsonKey(name: 'phoneSms')
  final String? phoneSms;
  @override
  @JsonKey(name: 'phoneWay4')
  final String? phoneWay4;
  @override
  @JsonKey(name: 'emailT24')
  final String? emailT24;
  @override
  @JsonKey(name: 'emailEmb')
  final String? emailEmb;
  @override
  @JsonKey(name: 'emailWay4')
  final String? emailWay4;
  @override
  @JsonKey(name: 'accountOfficer')
  final String? accountOfficer;
  @override
  @JsonKey(name: 'createdBy')
  final String? createdBy;
  @override
  @JsonKey(name: 'updatedBy')
  final String? updatedBy;

  @override
  String toString() {
    return 'ApiCustomerData(customerCode: $customerCode, address: $address, branchCode: $branchCode, businessRegistrationNumber: $businessRegistrationNumber, customerName: $customerName, classification: $classification, customerType: $customerType, dateOfBirth: $dateOfBirth, district: $district, gender: $gender, idCard: $idCard, industry: $industry, nationCode: $nationCode, province: $province, sector: $sector, segment: $segment, status: $status, swiftCode: $swiftCode, taxCode: $taxCode, transactionFrequency: $transactionFrequency, customerTypeSector: $customerTypeSector, customerTypeMerge: $customerTypeMerge, phoneT24: $phoneT24, phoneEmb: $phoneEmb, phoneSms: $phoneSms, phoneWay4: $phoneWay4, emailT24: $emailT24, emailEmb: $emailEmb, emailWay4: $emailWay4, accountOfficer: $accountOfficer, createdBy: $createdBy, updatedBy: $updatedBy)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ApiCustomerDataImpl &&
            (identical(other.customerCode, customerCode) ||
                other.customerCode == customerCode) &&
            (identical(other.address, address) || other.address == address) &&
            (identical(other.branchCode, branchCode) ||
                other.branchCode == branchCode) &&
            (identical(other.businessRegistrationNumber,
                    businessRegistrationNumber) ||
                other.businessRegistrationNumber ==
                    businessRegistrationNumber) &&
            (identical(other.customerName, customerName) ||
                other.customerName == customerName) &&
            (identical(other.classification, classification) ||
                other.classification == classification) &&
            (identical(other.customerType, customerType) ||
                other.customerType == customerType) &&
            (identical(other.dateOfBirth, dateOfBirth) ||
                other.dateOfBirth == dateOfBirth) &&
            (identical(other.district, district) ||
                other.district == district) &&
            (identical(other.gender, gender) || other.gender == gender) &&
            (identical(other.idCard, idCard) || other.idCard == idCard) &&
            (identical(other.industry, industry) ||
                other.industry == industry) &&
            (identical(other.nationCode, nationCode) ||
                other.nationCode == nationCode) &&
            (identical(other.province, province) ||
                other.province == province) &&
            (identical(other.sector, sector) || other.sector == sector) &&
            (identical(other.segment, segment) || other.segment == segment) &&
            (identical(other.status, status) || other.status == status) &&
            (identical(other.swiftCode, swiftCode) ||
                other.swiftCode == swiftCode) &&
            (identical(other.taxCode, taxCode) || other.taxCode == taxCode) &&
            (identical(other.transactionFrequency, transactionFrequency) ||
                other.transactionFrequency == transactionFrequency) &&
            (identical(other.customerTypeSector, customerTypeSector) ||
                other.customerTypeSector == customerTypeSector) &&
            (identical(other.customerTypeMerge, customerTypeMerge) ||
                other.customerTypeMerge == customerTypeMerge) &&
            (identical(other.phoneT24, phoneT24) ||
                other.phoneT24 == phoneT24) &&
            (identical(other.phoneEmb, phoneEmb) ||
                other.phoneEmb == phoneEmb) &&
            (identical(other.phoneSms, phoneSms) ||
                other.phoneSms == phoneSms) &&
            (identical(other.phoneWay4, phoneWay4) ||
                other.phoneWay4 == phoneWay4) &&
            (identical(other.emailT24, emailT24) ||
                other.emailT24 == emailT24) &&
            (identical(other.emailEmb, emailEmb) ||
                other.emailEmb == emailEmb) &&
            (identical(other.emailWay4, emailWay4) ||
                other.emailWay4 == emailWay4) &&
            (identical(other.accountOfficer, accountOfficer) ||
                other.accountOfficer == accountOfficer) &&
            (identical(other.createdBy, createdBy) ||
                other.createdBy == createdBy) &&
            (identical(other.updatedBy, updatedBy) ||
                other.updatedBy == updatedBy));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        customerCode,
        address,
        branchCode,
        businessRegistrationNumber,
        customerName,
        classification,
        customerType,
        dateOfBirth,
        district,
        gender,
        idCard,
        industry,
        nationCode,
        province,
        sector,
        segment,
        status,
        swiftCode,
        taxCode,
        transactionFrequency,
        customerTypeSector,
        customerTypeMerge,
        phoneT24,
        phoneEmb,
        phoneSms,
        phoneWay4,
        emailT24,
        emailEmb,
        emailWay4,
        accountOfficer,
        createdBy,
        updatedBy
      ]);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ApiCustomerDataImplCopyWith<_$ApiCustomerDataImpl> get copyWith =>
      __$$ApiCustomerDataImplCopyWithImpl<_$ApiCustomerDataImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$ApiCustomerDataImplToJson(
      this,
    );
  }
}

abstract class _ApiCustomerData extends ApiCustomerData {
  const factory _ApiCustomerData(
      {@JsonKey(name: 'customerCode') final String? customerCode,
      @JsonKey(name: 'address') final String? address,
      @JsonKey(name: 'branchCode') final String? branchCode,
      @JsonKey(name: 'businessRegistrationNumber')
      final String? businessRegistrationNumber,
      @JsonKey(name: 'customerName') final String? customerName,
      @JsonKey(name: 'classification') final String? classification,
      @JsonKey(name: 'customerType') final String? customerType,
      @JsonKey(name: 'dateOfBirth') final String? dateOfBirth,
      @JsonKey(name: 'district') final String? district,
      @JsonKey(name: 'gender') final String? gender,
      @JsonKey(name: 'idCard') final String? idCard,
      @JsonKey(name: 'industry') final String? industry,
      @JsonKey(name: 'nationCode') final String? nationCode,
      @JsonKey(name: 'province') final String? province,
      @JsonKey(name: 'sector') final String? sector,
      @JsonKey(name: 'segment') final String? segment,
      @JsonKey(name: 'status') final String? status,
      @JsonKey(name: 'swiftCode') final String? swiftCode,
      @JsonKey(name: 'taxCode') final String? taxCode,
      @JsonKey(name: 'transactionFrequency') final String? transactionFrequency,
      @JsonKey(name: 'customerTypeSector') final String? customerTypeSector,
      @JsonKey(name: 'customerTypeMerge') final String? customerTypeMerge,
      @JsonKey(name: 'phoneT24') final String? phoneT24,
      @JsonKey(name: 'phoneEmb') final String? phoneEmb,
      @JsonKey(name: 'phoneSms') final String? phoneSms,
      @JsonKey(name: 'phoneWay4') final String? phoneWay4,
      @JsonKey(name: 'emailT24') final String? emailT24,
      @JsonKey(name: 'emailEmb') final String? emailEmb,
      @JsonKey(name: 'emailWay4') final String? emailWay4,
      @JsonKey(name: 'accountOfficer') final String? accountOfficer,
      @JsonKey(name: 'createdBy') final String? createdBy,
      @JsonKey(name: 'updatedBy')
      final String? updatedBy}) = _$ApiCustomerDataImpl;
  const _ApiCustomerData._() : super._();

  factory _ApiCustomerData.fromJson(Map<String, dynamic> json) =
      _$ApiCustomerDataImpl.fromJson;

  @override
  @JsonKey(name: 'customerCode')
  String? get customerCode;
  @override
  @JsonKey(name: 'address')
  String? get address;
  @override
  @JsonKey(name: 'branchCode')
  String? get branchCode;
  @override
  @JsonKey(name: 'businessRegistrationNumber')
  String? get businessRegistrationNumber;
  @override
  @JsonKey(name: 'customerName')
  String? get customerName;
  @override
  @JsonKey(name: 'classification')
  String? get classification;
  @override
  @JsonKey(name: 'customerType')
  String? get customerType;
  @override
  @JsonKey(name: 'dateOfBirth')
  String? get dateOfBirth;
  @override
  @JsonKey(name: 'district')
  String? get district;
  @override
  @JsonKey(name: 'gender')
  String? get gender;
  @override
  @JsonKey(name: 'idCard')
  String? get idCard;
  @override
  @JsonKey(name: 'industry')
  String? get industry;
  @override
  @JsonKey(name: 'nationCode')
  String? get nationCode;
  @override
  @JsonKey(name: 'province')
  String? get province;
  @override
  @JsonKey(name: 'sector')
  String? get sector;
  @override
  @JsonKey(name: 'segment')
  String? get segment;
  @override
  @JsonKey(name: 'status')
  String? get status;
  @override
  @JsonKey(name: 'swiftCode')
  String? get swiftCode;
  @override
  @JsonKey(name: 'taxCode')
  String? get taxCode;
  @override
  @JsonKey(name: 'transactionFrequency')
  String? get transactionFrequency;
  @override
  @JsonKey(name: 'customerTypeSector')
  String? get customerTypeSector;
  @override
  @JsonKey(name: 'customerTypeMerge')
  String? get customerTypeMerge;
  @override
  @JsonKey(name: 'phoneT24')
  String? get phoneT24;
  @override
  @JsonKey(name: 'phoneEmb')
  String? get phoneEmb;
  @override
  @JsonKey(name: 'phoneSms')
  String? get phoneSms;
  @override
  @JsonKey(name: 'phoneWay4')
  String? get phoneWay4;
  @override
  @JsonKey(name: 'emailT24')
  String? get emailT24;
  @override
  @JsonKey(name: 'emailEmb')
  String? get emailEmb;
  @override
  @JsonKey(name: 'emailWay4')
  String? get emailWay4;
  @override
  @JsonKey(name: 'accountOfficer')
  String? get accountOfficer;
  @override
  @JsonKey(name: 'createdBy')
  String? get createdBy;
  @override
  @JsonKey(name: 'updatedBy')
  String? get updatedBy;
  @override
  @JsonKey(ignore: true)
  _$$ApiCustomerDataImplCopyWith<_$ApiCustomerDataImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
