// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'api_basic_user_Info_data.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

ApiBasicUserInfoData _$ApiBasicUserInfoDataFromJson(Map<String, dynamic> json) {
  return _ApiBasicUserInfoData.fromJson(json);
}

/// @nodoc
mixin _$ApiBasicUserInfoData {
  @JsonKey(name: 'id')
  String? get id => throw _privateConstructorUsedError;
  @JsonKey(name: 'code')
  String? get code => throw _privateConstructorUsedError;
  @JsonKey(name: 'hrsCode')
  String? get hrsCode => throw _privateConstructorUsedError;
  @JsonKey(name: 'username')
  String? get username => throw _privateConstructorUsedError;
  @JsonKey(name: 'fullName')
  String? get fullName => throw _privateConstructorUsedError;
  @JsonKey(name: 'branch')
  String? get branch => throw _privateConstructorUsedError;
  @JsonKey(name: 'phoneNumber')
  String? get phoneNumber => throw _privateConstructorUsedError;
  @JsonKey(name: 'email')
  String? get email => throw _privateConstructorUsedError;
  @JsonKey(name: 'note')
  String? get note => throw _privateConstructorUsedError;
  @JsonKey(name: 'image')
  String? get image => throw _privateConstructorUsedError;
  @JsonKey(name: 'branchName')
  String? get branchName => throw _privateConstructorUsedError;
  @JsonKey(name: 'phoneDevice')
  String? get phoneDevice => throw _privateConstructorUsedError;
  @JsonKey(name: 'reason')
  String? get reason => throw _privateConstructorUsedError;
  @JsonKey(name: 'gender')
  String? get gender => throw _privateConstructorUsedError;
  @JsonKey(name: 'unLock')
  bool? get unLock => throw _privateConstructorUsedError;
  @JsonKey(name: 'statusWork')
  String? get statusWork => throw _privateConstructorUsedError;
  @JsonKey(name: 'blockRmManager')
  List<String>? get blockRmManager => throw _privateConstructorUsedError;
  @JsonKey(name: 'domainAll')
  bool? get domainAll => throw _privateConstructorUsedError;
  @JsonKey(name: 'adminRole')
  bool? get adminRole => throw _privateConstructorUsedError;
  @JsonKey(name: 'active')
  bool? get active => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ApiBasicUserInfoDataCopyWith<ApiBasicUserInfoData> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ApiBasicUserInfoDataCopyWith<$Res> {
  factory $ApiBasicUserInfoDataCopyWith(ApiBasicUserInfoData value,
          $Res Function(ApiBasicUserInfoData) then) =
      _$ApiBasicUserInfoDataCopyWithImpl<$Res, ApiBasicUserInfoData>;
  @useResult
  $Res call(
      {@JsonKey(name: 'id') String? id,
      @JsonKey(name: 'code') String? code,
      @JsonKey(name: 'hrsCode') String? hrsCode,
      @JsonKey(name: 'username') String? username,
      @JsonKey(name: 'fullName') String? fullName,
      @JsonKey(name: 'branch') String? branch,
      @JsonKey(name: 'phoneNumber') String? phoneNumber,
      @JsonKey(name: 'email') String? email,
      @JsonKey(name: 'note') String? note,
      @JsonKey(name: 'image') String? image,
      @JsonKey(name: 'branchName') String? branchName,
      @JsonKey(name: 'phoneDevice') String? phoneDevice,
      @JsonKey(name: 'reason') String? reason,
      @JsonKey(name: 'gender') String? gender,
      @JsonKey(name: 'unLock') bool? unLock,
      @JsonKey(name: 'statusWork') String? statusWork,
      @JsonKey(name: 'blockRmManager') List<String>? blockRmManager,
      @JsonKey(name: 'domainAll') bool? domainAll,
      @JsonKey(name: 'adminRole') bool? adminRole,
      @JsonKey(name: 'active') bool? active});
}

/// @nodoc
class _$ApiBasicUserInfoDataCopyWithImpl<$Res,
        $Val extends ApiBasicUserInfoData>
    implements $ApiBasicUserInfoDataCopyWith<$Res> {
  _$ApiBasicUserInfoDataCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? code = freezed,
    Object? hrsCode = freezed,
    Object? username = freezed,
    Object? fullName = freezed,
    Object? branch = freezed,
    Object? phoneNumber = freezed,
    Object? email = freezed,
    Object? note = freezed,
    Object? image = freezed,
    Object? branchName = freezed,
    Object? phoneDevice = freezed,
    Object? reason = freezed,
    Object? gender = freezed,
    Object? unLock = freezed,
    Object? statusWork = freezed,
    Object? blockRmManager = freezed,
    Object? domainAll = freezed,
    Object? adminRole = freezed,
    Object? active = freezed,
  }) {
    return _then(_value.copyWith(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      code: freezed == code
          ? _value.code
          : code // ignore: cast_nullable_to_non_nullable
              as String?,
      hrsCode: freezed == hrsCode
          ? _value.hrsCode
          : hrsCode // ignore: cast_nullable_to_non_nullable
              as String?,
      username: freezed == username
          ? _value.username
          : username // ignore: cast_nullable_to_non_nullable
              as String?,
      fullName: freezed == fullName
          ? _value.fullName
          : fullName // ignore: cast_nullable_to_non_nullable
              as String?,
      branch: freezed == branch
          ? _value.branch
          : branch // ignore: cast_nullable_to_non_nullable
              as String?,
      phoneNumber: freezed == phoneNumber
          ? _value.phoneNumber
          : phoneNumber // ignore: cast_nullable_to_non_nullable
              as String?,
      email: freezed == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      note: freezed == note
          ? _value.note
          : note // ignore: cast_nullable_to_non_nullable
              as String?,
      image: freezed == image
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as String?,
      branchName: freezed == branchName
          ? _value.branchName
          : branchName // ignore: cast_nullable_to_non_nullable
              as String?,
      phoneDevice: freezed == phoneDevice
          ? _value.phoneDevice
          : phoneDevice // ignore: cast_nullable_to_non_nullable
              as String?,
      reason: freezed == reason
          ? _value.reason
          : reason // ignore: cast_nullable_to_non_nullable
              as String?,
      gender: freezed == gender
          ? _value.gender
          : gender // ignore: cast_nullable_to_non_nullable
              as String?,
      unLock: freezed == unLock
          ? _value.unLock
          : unLock // ignore: cast_nullable_to_non_nullable
              as bool?,
      statusWork: freezed == statusWork
          ? _value.statusWork
          : statusWork // ignore: cast_nullable_to_non_nullable
              as String?,
      blockRmManager: freezed == blockRmManager
          ? _value.blockRmManager
          : blockRmManager // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      domainAll: freezed == domainAll
          ? _value.domainAll
          : domainAll // ignore: cast_nullable_to_non_nullable
              as bool?,
      adminRole: freezed == adminRole
          ? _value.adminRole
          : adminRole // ignore: cast_nullable_to_non_nullable
              as bool?,
      active: freezed == active
          ? _value.active
          : active // ignore: cast_nullable_to_non_nullable
              as bool?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ApiBasicUserInfoDataImplCopyWith<$Res>
    implements $ApiBasicUserInfoDataCopyWith<$Res> {
  factory _$$ApiBasicUserInfoDataImplCopyWith(_$ApiBasicUserInfoDataImpl value,
          $Res Function(_$ApiBasicUserInfoDataImpl) then) =
      __$$ApiBasicUserInfoDataImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@JsonKey(name: 'id') String? id,
      @JsonKey(name: 'code') String? code,
      @JsonKey(name: 'hrsCode') String? hrsCode,
      @JsonKey(name: 'username') String? username,
      @JsonKey(name: 'fullName') String? fullName,
      @JsonKey(name: 'branch') String? branch,
      @JsonKey(name: 'phoneNumber') String? phoneNumber,
      @JsonKey(name: 'email') String? email,
      @JsonKey(name: 'note') String? note,
      @JsonKey(name: 'image') String? image,
      @JsonKey(name: 'branchName') String? branchName,
      @JsonKey(name: 'phoneDevice') String? phoneDevice,
      @JsonKey(name: 'reason') String? reason,
      @JsonKey(name: 'gender') String? gender,
      @JsonKey(name: 'unLock') bool? unLock,
      @JsonKey(name: 'statusWork') String? statusWork,
      @JsonKey(name: 'blockRmManager') List<String>? blockRmManager,
      @JsonKey(name: 'domainAll') bool? domainAll,
      @JsonKey(name: 'adminRole') bool? adminRole,
      @JsonKey(name: 'active') bool? active});
}

/// @nodoc
class __$$ApiBasicUserInfoDataImplCopyWithImpl<$Res>
    extends _$ApiBasicUserInfoDataCopyWithImpl<$Res, _$ApiBasicUserInfoDataImpl>
    implements _$$ApiBasicUserInfoDataImplCopyWith<$Res> {
  __$$ApiBasicUserInfoDataImplCopyWithImpl(_$ApiBasicUserInfoDataImpl _value,
      $Res Function(_$ApiBasicUserInfoDataImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? code = freezed,
    Object? hrsCode = freezed,
    Object? username = freezed,
    Object? fullName = freezed,
    Object? branch = freezed,
    Object? phoneNumber = freezed,
    Object? email = freezed,
    Object? note = freezed,
    Object? image = freezed,
    Object? branchName = freezed,
    Object? phoneDevice = freezed,
    Object? reason = freezed,
    Object? gender = freezed,
    Object? unLock = freezed,
    Object? statusWork = freezed,
    Object? blockRmManager = freezed,
    Object? domainAll = freezed,
    Object? adminRole = freezed,
    Object? active = freezed,
  }) {
    return _then(_$ApiBasicUserInfoDataImpl(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      code: freezed == code
          ? _value.code
          : code // ignore: cast_nullable_to_non_nullable
              as String?,
      hrsCode: freezed == hrsCode
          ? _value.hrsCode
          : hrsCode // ignore: cast_nullable_to_non_nullable
              as String?,
      username: freezed == username
          ? _value.username
          : username // ignore: cast_nullable_to_non_nullable
              as String?,
      fullName: freezed == fullName
          ? _value.fullName
          : fullName // ignore: cast_nullable_to_non_nullable
              as String?,
      branch: freezed == branch
          ? _value.branch
          : branch // ignore: cast_nullable_to_non_nullable
              as String?,
      phoneNumber: freezed == phoneNumber
          ? _value.phoneNumber
          : phoneNumber // ignore: cast_nullable_to_non_nullable
              as String?,
      email: freezed == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      note: freezed == note
          ? _value.note
          : note // ignore: cast_nullable_to_non_nullable
              as String?,
      image: freezed == image
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as String?,
      branchName: freezed == branchName
          ? _value.branchName
          : branchName // ignore: cast_nullable_to_non_nullable
              as String?,
      phoneDevice: freezed == phoneDevice
          ? _value.phoneDevice
          : phoneDevice // ignore: cast_nullable_to_non_nullable
              as String?,
      reason: freezed == reason
          ? _value.reason
          : reason // ignore: cast_nullable_to_non_nullable
              as String?,
      gender: freezed == gender
          ? _value.gender
          : gender // ignore: cast_nullable_to_non_nullable
              as String?,
      unLock: freezed == unLock
          ? _value.unLock
          : unLock // ignore: cast_nullable_to_non_nullable
              as bool?,
      statusWork: freezed == statusWork
          ? _value.statusWork
          : statusWork // ignore: cast_nullable_to_non_nullable
              as String?,
      blockRmManager: freezed == blockRmManager
          ? _value._blockRmManager
          : blockRmManager // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      domainAll: freezed == domainAll
          ? _value.domainAll
          : domainAll // ignore: cast_nullable_to_non_nullable
              as bool?,
      adminRole: freezed == adminRole
          ? _value.adminRole
          : adminRole // ignore: cast_nullable_to_non_nullable
              as bool?,
      active: freezed == active
          ? _value.active
          : active // ignore: cast_nullable_to_non_nullable
              as bool?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$ApiBasicUserInfoDataImpl extends _ApiBasicUserInfoData {
  const _$ApiBasicUserInfoDataImpl(
      {@JsonKey(name: 'id') this.id,
      @JsonKey(name: 'code') this.code,
      @JsonKey(name: 'hrsCode') this.hrsCode,
      @JsonKey(name: 'username') this.username,
      @JsonKey(name: 'fullName') this.fullName,
      @JsonKey(name: 'branch') this.branch,
      @JsonKey(name: 'phoneNumber') this.phoneNumber,
      @JsonKey(name: 'email') this.email,
      @JsonKey(name: 'note') this.note,
      @JsonKey(name: 'image') this.image,
      @JsonKey(name: 'branchName') this.branchName,
      @JsonKey(name: 'phoneDevice') this.phoneDevice,
      @JsonKey(name: 'reason') this.reason,
      @JsonKey(name: 'gender') this.gender,
      @JsonKey(name: 'unLock') this.unLock,
      @JsonKey(name: 'statusWork') this.statusWork,
      @JsonKey(name: 'blockRmManager') final List<String>? blockRmManager,
      @JsonKey(name: 'domainAll') this.domainAll,
      @JsonKey(name: 'adminRole') this.adminRole,
      @JsonKey(name: 'active') this.active})
      : _blockRmManager = blockRmManager,
        super._();

  factory _$ApiBasicUserInfoDataImpl.fromJson(Map<String, dynamic> json) =>
      _$$ApiBasicUserInfoDataImplFromJson(json);

  @override
  @JsonKey(name: 'id')
  final String? id;
  @override
  @JsonKey(name: 'code')
  final String? code;
  @override
  @JsonKey(name: 'hrsCode')
  final String? hrsCode;
  @override
  @JsonKey(name: 'username')
  final String? username;
  @override
  @JsonKey(name: 'fullName')
  final String? fullName;
  @override
  @JsonKey(name: 'branch')
  final String? branch;
  @override
  @JsonKey(name: 'phoneNumber')
  final String? phoneNumber;
  @override
  @JsonKey(name: 'email')
  final String? email;
  @override
  @JsonKey(name: 'note')
  final String? note;
  @override
  @JsonKey(name: 'image')
  final String? image;
  @override
  @JsonKey(name: 'branchName')
  final String? branchName;
  @override
  @JsonKey(name: 'phoneDevice')
  final String? phoneDevice;
  @override
  @JsonKey(name: 'reason')
  final String? reason;
  @override
  @JsonKey(name: 'gender')
  final String? gender;
  @override
  @JsonKey(name: 'unLock')
  final bool? unLock;
  @override
  @JsonKey(name: 'statusWork')
  final String? statusWork;
  final List<String>? _blockRmManager;
  @override
  @JsonKey(name: 'blockRmManager')
  List<String>? get blockRmManager {
    final value = _blockRmManager;
    if (value == null) return null;
    if (_blockRmManager is EqualUnmodifiableListView) return _blockRmManager;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  @JsonKey(name: 'domainAll')
  final bool? domainAll;
  @override
  @JsonKey(name: 'adminRole')
  final bool? adminRole;
  @override
  @JsonKey(name: 'active')
  final bool? active;

  @override
  String toString() {
    return 'ApiBasicUserInfoData(id: $id, code: $code, hrsCode: $hrsCode, username: $username, fullName: $fullName, branch: $branch, phoneNumber: $phoneNumber, email: $email, note: $note, image: $image, branchName: $branchName, phoneDevice: $phoneDevice, reason: $reason, gender: $gender, unLock: $unLock, statusWork: $statusWork, blockRmManager: $blockRmManager, domainAll: $domainAll, adminRole: $adminRole, active: $active)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ApiBasicUserInfoDataImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.code, code) || other.code == code) &&
            (identical(other.hrsCode, hrsCode) || other.hrsCode == hrsCode) &&
            (identical(other.username, username) ||
                other.username == username) &&
            (identical(other.fullName, fullName) ||
                other.fullName == fullName) &&
            (identical(other.branch, branch) || other.branch == branch) &&
            (identical(other.phoneNumber, phoneNumber) ||
                other.phoneNumber == phoneNumber) &&
            (identical(other.email, email) || other.email == email) &&
            (identical(other.note, note) || other.note == note) &&
            (identical(other.image, image) || other.image == image) &&
            (identical(other.branchName, branchName) ||
                other.branchName == branchName) &&
            (identical(other.phoneDevice, phoneDevice) ||
                other.phoneDevice == phoneDevice) &&
            (identical(other.reason, reason) || other.reason == reason) &&
            (identical(other.gender, gender) || other.gender == gender) &&
            (identical(other.unLock, unLock) || other.unLock == unLock) &&
            (identical(other.statusWork, statusWork) ||
                other.statusWork == statusWork) &&
            const DeepCollectionEquality()
                .equals(other._blockRmManager, _blockRmManager) &&
            (identical(other.domainAll, domainAll) ||
                other.domainAll == domainAll) &&
            (identical(other.adminRole, adminRole) ||
                other.adminRole == adminRole) &&
            (identical(other.active, active) || other.active == active));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        id,
        code,
        hrsCode,
        username,
        fullName,
        branch,
        phoneNumber,
        email,
        note,
        image,
        branchName,
        phoneDevice,
        reason,
        gender,
        unLock,
        statusWork,
        const DeepCollectionEquality().hash(_blockRmManager),
        domainAll,
        adminRole,
        active
      ]);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ApiBasicUserInfoDataImplCopyWith<_$ApiBasicUserInfoDataImpl>
      get copyWith =>
          __$$ApiBasicUserInfoDataImplCopyWithImpl<_$ApiBasicUserInfoDataImpl>(
              this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$ApiBasicUserInfoDataImplToJson(
      this,
    );
  }
}

abstract class _ApiBasicUserInfoData extends ApiBasicUserInfoData {
  const factory _ApiBasicUserInfoData(
          {@JsonKey(name: 'id') final String? id,
          @JsonKey(name: 'code') final String? code,
          @JsonKey(name: 'hrsCode') final String? hrsCode,
          @JsonKey(name: 'username') final String? username,
          @JsonKey(name: 'fullName') final String? fullName,
          @JsonKey(name: 'branch') final String? branch,
          @JsonKey(name: 'phoneNumber') final String? phoneNumber,
          @JsonKey(name: 'email') final String? email,
          @JsonKey(name: 'note') final String? note,
          @JsonKey(name: 'image') final String? image,
          @JsonKey(name: 'branchName') final String? branchName,
          @JsonKey(name: 'phoneDevice') final String? phoneDevice,
          @JsonKey(name: 'reason') final String? reason,
          @JsonKey(name: 'gender') final String? gender,
          @JsonKey(name: 'unLock') final bool? unLock,
          @JsonKey(name: 'statusWork') final String? statusWork,
          @JsonKey(name: 'blockRmManager') final List<String>? blockRmManager,
          @JsonKey(name: 'domainAll') final bool? domainAll,
          @JsonKey(name: 'adminRole') final bool? adminRole,
          @JsonKey(name: 'active') final bool? active}) =
      _$ApiBasicUserInfoDataImpl;
  const _ApiBasicUserInfoData._() : super._();

  factory _ApiBasicUserInfoData.fromJson(Map<String, dynamic> json) =
      _$ApiBasicUserInfoDataImpl.fromJson;

  @override
  @JsonKey(name: 'id')
  String? get id;
  @override
  @JsonKey(name: 'code')
  String? get code;
  @override
  @JsonKey(name: 'hrsCode')
  String? get hrsCode;
  @override
  @JsonKey(name: 'username')
  String? get username;
  @override
  @JsonKey(name: 'fullName')
  String? get fullName;
  @override
  @JsonKey(name: 'branch')
  String? get branch;
  @override
  @JsonKey(name: 'phoneNumber')
  String? get phoneNumber;
  @override
  @JsonKey(name: 'email')
  String? get email;
  @override
  @JsonKey(name: 'note')
  String? get note;
  @override
  @JsonKey(name: 'image')
  String? get image;
  @override
  @JsonKey(name: 'branchName')
  String? get branchName;
  @override
  @JsonKey(name: 'phoneDevice')
  String? get phoneDevice;
  @override
  @JsonKey(name: 'reason')
  String? get reason;
  @override
  @JsonKey(name: 'gender')
  String? get gender;
  @override
  @JsonKey(name: 'unLock')
  bool? get unLock;
  @override
  @JsonKey(name: 'statusWork')
  String? get statusWork;
  @override
  @JsonKey(name: 'blockRmManager')
  List<String>? get blockRmManager;
  @override
  @JsonKey(name: 'domainAll')
  bool? get domainAll;
  @override
  @JsonKey(name: 'adminRole')
  bool? get adminRole;
  @override
  @JsonKey(name: 'active')
  bool? get active;
  @override
  @JsonKey(ignore: true)
  _$$ApiBasicUserInfoDataImplCopyWith<_$ApiBasicUserInfoDataImpl>
      get copyWith => throw _privateConstructorUsedError;
}
