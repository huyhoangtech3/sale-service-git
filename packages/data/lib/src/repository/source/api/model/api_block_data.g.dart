// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'api_block_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$ApiBlockDataImpl _$$ApiBlockDataImplFromJson(Map<String, dynamic> json) =>
    _$ApiBlockDataImpl(
      id: json['id'] as String?,
      code: json['code'] as String?,
      name: json['name'] as String?,
      isActive: json['isActive'] as bool?,
      customerType: json['customerType'] as String?,
      createdDate: json['createdDate'] as String?,
      createdBy: json['createdBy'] as String?,
      updatedDate: json['updatedDate'] as String?,
      updatedBy: json['updatedBy'] as String?,
    );

Map<String, dynamic> _$$ApiBlockDataImplToJson(_$ApiBlockDataImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'code': instance.code,
      'name': instance.name,
      'isActive': instance.isActive,
      'customerType': instance.customerType,
      'createdDate': instance.createdDate,
      'createdBy': instance.createdBy,
      'updatedDate': instance.updatedDate,
      'updatedBy': instance.updatedBy,
    };
