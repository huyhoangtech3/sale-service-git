// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sort_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$SortResponseImpl _$$SortResponseImplFromJson(Map<String, dynamic> json) =>
    _$SortResponseImpl(
      unsorted: json['unsorted'] as bool?,
      sorted: json['sorted'] as bool?,
      empty: json['empty'] as bool?,
    );

Map<String, dynamic> _$$SortResponseImplToJson(_$SortResponseImpl instance) =>
    <String, dynamic>{
      'unsorted': instance.unsorted,
      'sorted': instance.sorted,
      'empty': instance.empty,
    };
