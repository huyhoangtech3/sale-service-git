import 'package:freezed_annotation/freezed_annotation.dart';

part 'api_branch_data.freezed.dart';
part 'api_branch_data.g.dart';

@freezed
class ApiBranchData with _$ApiBranchData {
  const ApiBranchData._();
  const factory ApiBranchData({
    @JsonKey(name: 'code') String? code,
    @JsonKey(name: 'name') String? name,
    @JsonKey(name: 'region') String? region,
    @JsonKey(name: 'description') String? description,
  }) = _ApiBranchData;

  factory ApiBranchData.fromJson(Map<String, dynamic> json) =>
      _$ApiBranchDataFromJson(json);
}