// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'api_branch_data.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

ApiBranchData _$ApiBranchDataFromJson(Map<String, dynamic> json) {
  return _ApiBranchData.fromJson(json);
}

/// @nodoc
mixin _$ApiBranchData {
  @JsonKey(name: 'code')
  String? get code => throw _privateConstructorUsedError;
  @JsonKey(name: 'name')
  String? get name => throw _privateConstructorUsedError;
  @JsonKey(name: 'region')
  String? get region => throw _privateConstructorUsedError;
  @JsonKey(name: 'description')
  String? get description => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ApiBranchDataCopyWith<ApiBranchData> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ApiBranchDataCopyWith<$Res> {
  factory $ApiBranchDataCopyWith(
          ApiBranchData value, $Res Function(ApiBranchData) then) =
      _$ApiBranchDataCopyWithImpl<$Res, ApiBranchData>;
  @useResult
  $Res call(
      {@JsonKey(name: 'code') String? code,
      @JsonKey(name: 'name') String? name,
      @JsonKey(name: 'region') String? region,
      @JsonKey(name: 'description') String? description});
}

/// @nodoc
class _$ApiBranchDataCopyWithImpl<$Res, $Val extends ApiBranchData>
    implements $ApiBranchDataCopyWith<$Res> {
  _$ApiBranchDataCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? code = freezed,
    Object? name = freezed,
    Object? region = freezed,
    Object? description = freezed,
  }) {
    return _then(_value.copyWith(
      code: freezed == code
          ? _value.code
          : code // ignore: cast_nullable_to_non_nullable
              as String?,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      region: freezed == region
          ? _value.region
          : region // ignore: cast_nullable_to_non_nullable
              as String?,
      description: freezed == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ApiBranchDataImplCopyWith<$Res>
    implements $ApiBranchDataCopyWith<$Res> {
  factory _$$ApiBranchDataImplCopyWith(
          _$ApiBranchDataImpl value, $Res Function(_$ApiBranchDataImpl) then) =
      __$$ApiBranchDataImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@JsonKey(name: 'code') String? code,
      @JsonKey(name: 'name') String? name,
      @JsonKey(name: 'region') String? region,
      @JsonKey(name: 'description') String? description});
}

/// @nodoc
class __$$ApiBranchDataImplCopyWithImpl<$Res>
    extends _$ApiBranchDataCopyWithImpl<$Res, _$ApiBranchDataImpl>
    implements _$$ApiBranchDataImplCopyWith<$Res> {
  __$$ApiBranchDataImplCopyWithImpl(
      _$ApiBranchDataImpl _value, $Res Function(_$ApiBranchDataImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? code = freezed,
    Object? name = freezed,
    Object? region = freezed,
    Object? description = freezed,
  }) {
    return _then(_$ApiBranchDataImpl(
      code: freezed == code
          ? _value.code
          : code // ignore: cast_nullable_to_non_nullable
              as String?,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      region: freezed == region
          ? _value.region
          : region // ignore: cast_nullable_to_non_nullable
              as String?,
      description: freezed == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$ApiBranchDataImpl extends _ApiBranchData {
  const _$ApiBranchDataImpl(
      {@JsonKey(name: 'code') this.code,
      @JsonKey(name: 'name') this.name,
      @JsonKey(name: 'region') this.region,
      @JsonKey(name: 'description') this.description})
      : super._();

  factory _$ApiBranchDataImpl.fromJson(Map<String, dynamic> json) =>
      _$$ApiBranchDataImplFromJson(json);

  @override
  @JsonKey(name: 'code')
  final String? code;
  @override
  @JsonKey(name: 'name')
  final String? name;
  @override
  @JsonKey(name: 'region')
  final String? region;
  @override
  @JsonKey(name: 'description')
  final String? description;

  @override
  String toString() {
    return 'ApiBranchData(code: $code, name: $name, region: $region, description: $description)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ApiBranchDataImpl &&
            (identical(other.code, code) || other.code == code) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.region, region) || other.region == region) &&
            (identical(other.description, description) ||
                other.description == description));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, code, name, region, description);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ApiBranchDataImplCopyWith<_$ApiBranchDataImpl> get copyWith =>
      __$$ApiBranchDataImplCopyWithImpl<_$ApiBranchDataImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$ApiBranchDataImplToJson(
      this,
    );
  }
}

abstract class _ApiBranchData extends ApiBranchData {
  const factory _ApiBranchData(
          {@JsonKey(name: 'code') final String? code,
          @JsonKey(name: 'name') final String? name,
          @JsonKey(name: 'region') final String? region,
          @JsonKey(name: 'description') final String? description}) =
      _$ApiBranchDataImpl;
  const _ApiBranchData._() : super._();

  factory _ApiBranchData.fromJson(Map<String, dynamic> json) =
      _$ApiBranchDataImpl.fromJson;

  @override
  @JsonKey(name: 'code')
  String? get code;
  @override
  @JsonKey(name: 'name')
  String? get name;
  @override
  @JsonKey(name: 'region')
  String? get region;
  @override
  @JsonKey(name: 'description')
  String? get description;
  @override
  @JsonKey(ignore: true)
  _$$ApiBranchDataImplCopyWith<_$ApiBranchDataImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
