// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'list_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$ListResponseImpl<T> _$$ListResponseImplFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    _$ListResponseImpl<T>(
      content: (json['content'] as List<dynamic>?)?.map(fromJsonT).toList(),
      totalPages: (json['totalPages'] as num?)?.toInt(),
      last: json['last'] as bool?,
      totalElements: (json['totalElements'] as num?)?.toInt(),
      first: json['first'] as bool?,
      numberOfElements: (json['numberOfElements'] as num?)?.toInt(),
      number: (json['number'] as num?)?.toInt(),
      size: (json['size'] as num?)?.toInt(),
      empty: json['empty'] as bool?,
      sort: json['sort'] == null
          ? null
          : SortResponse.fromJson(json['sort'] as Map<String, dynamic>),
      pageable: json['pageable'] == null
          ? null
          : PageableResponse.fromJson(json['pageable'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$ListResponseImplToJson<T>(
  _$ListResponseImpl<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'content': instance.content?.map(toJsonT).toList(),
      'totalPages': instance.totalPages,
      'last': instance.last,
      'totalElements': instance.totalElements,
      'first': instance.first,
      'numberOfElements': instance.numberOfElements,
      'number': instance.number,
      'size': instance.size,
      'empty': instance.empty,
      'sort': instance.sort,
      'pageable': instance.pageable,
    };
