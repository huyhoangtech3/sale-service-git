import 'package:freezed_annotation/freezed_annotation.dart';

import 'package:data/data.dart';

part 'pageable_response.freezed.dart';
part 'pageable_response.g.dart';

@freezed
class PageableResponse with _$PageableResponse {
  const PageableResponse._();

  const factory PageableResponse({
    @JsonKey(name: 'sort') SortResponse? sort,
    @JsonKey(name: 'pageNumber') int? pageNumber,
    @JsonKey(name: 'pageSize') int? pageSize,
    @JsonKey(name: 'offset') int? offset,
    @JsonKey(name: 'paged') bool? paged,
    @JsonKey(name: 'unpaged') bool? unpaged,
  }) = _PageableResponse;

  factory PageableResponse.fromJson(Map<String, dynamic> json) =>
      _$PageableResponseFromJson(json);
}
