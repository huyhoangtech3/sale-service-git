// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'api_basic_user_Info_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$ApiBasicUserInfoDataImpl _$$ApiBasicUserInfoDataImplFromJson(
        Map<String, dynamic> json) =>
    _$ApiBasicUserInfoDataImpl(
      id: json['id'] as String?,
      code: json['code'] as String?,
      hrsCode: json['hrsCode'] as String?,
      username: json['username'] as String?,
      fullName: json['fullName'] as String?,
      branch: json['branch'] as String?,
      phoneNumber: json['phoneNumber'] as String?,
      email: json['email'] as String?,
      note: json['note'] as String?,
      image: json['image'] as String?,
      branchName: json['branchName'] as String?,
      phoneDevice: json['phoneDevice'] as String?,
      reason: json['reason'] as String?,
      gender: json['gender'] as String?,
      unLock: json['unLock'] as bool?,
      statusWork: json['statusWork'] as String?,
      blockRmManager: (json['blockRmManager'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      domainAll: json['domainAll'] as bool?,
      adminRole: json['adminRole'] as bool?,
      active: json['active'] as bool?,
    );

Map<String, dynamic> _$$ApiBasicUserInfoDataImplToJson(
        _$ApiBasicUserInfoDataImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'code': instance.code,
      'hrsCode': instance.hrsCode,
      'username': instance.username,
      'fullName': instance.fullName,
      'branch': instance.branch,
      'phoneNumber': instance.phoneNumber,
      'email': instance.email,
      'note': instance.note,
      'image': instance.image,
      'branchName': instance.branchName,
      'phoneDevice': instance.phoneDevice,
      'reason': instance.reason,
      'gender': instance.gender,
      'unLock': instance.unLock,
      'statusWork': instance.statusWork,
      'blockRmManager': instance.blockRmManager,
      'domainAll': instance.domainAll,
      'adminRole': instance.adminRole,
      'active': instance.active,
    };
