// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'api_branch_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$ApiBranchDataImpl _$$ApiBranchDataImplFromJson(Map<String, dynamic> json) =>
    _$ApiBranchDataImpl(
      code: json['code'] as String?,
      name: json['name'] as String?,
      region: json['region'] as String?,
      description: json['description'] as String?,
    );

Map<String, dynamic> _$$ApiBranchDataImplToJson(_$ApiBranchDataImpl instance) =>
    <String, dynamic>{
      'code': instance.code,
      'name': instance.name,
      'region': instance.region,
      'description': instance.description,
    };
