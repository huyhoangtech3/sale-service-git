// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'api_auth_response_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$ApiAuthResponseDataImpl _$$ApiAuthResponseDataImplFromJson(
        Map<String, dynamic> json) =>
    _$ApiAuthResponseDataImpl(
      accessToken: json['access_token'] as String?,
      expiresIn: (json['expires_in'] as num?)?.toInt(),
      refreshToken: json['refresh_token'] as String?,
      refreshExpiresIn: (json['refresh_expires_in'] as num?)?.toInt(),
      tokenType: json['token_type'] as String?,
      scope: json['scope'] as String?,
    );

Map<String, dynamic> _$$ApiAuthResponseDataImplToJson(
        _$ApiAuthResponseDataImpl instance) =>
    <String, dynamic>{
      'access_token': instance.accessToken,
      'expires_in': instance.expiresIn,
      'refresh_token': instance.refreshToken,
      'refresh_expires_in': instance.refreshExpiresIn,
      'token_type': instance.tokenType,
      'scope': instance.scope,
    };
