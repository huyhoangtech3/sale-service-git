// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'api_block_data.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

ApiBlockData _$ApiBlockDataFromJson(Map<String, dynamic> json) {
  return _ApiBlockData.fromJson(json);
}

/// @nodoc
mixin _$ApiBlockData {
  @JsonKey(name: 'id')
  String? get id => throw _privateConstructorUsedError;
  @JsonKey(name: 'code')
  String? get code => throw _privateConstructorUsedError;
  @JsonKey(name: 'name')
  String? get name => throw _privateConstructorUsedError;
  @JsonKey(name: 'isActive')
  bool? get isActive => throw _privateConstructorUsedError;
  @JsonKey(name: 'customerType')
  String? get customerType => throw _privateConstructorUsedError;
  @JsonKey(name: 'createdDate')
  String? get createdDate => throw _privateConstructorUsedError;
  @JsonKey(name: 'createdBy')
  String? get createdBy => throw _privateConstructorUsedError;
  @JsonKey(name: 'updatedDate')
  String? get updatedDate => throw _privateConstructorUsedError;
  @JsonKey(name: 'updatedBy')
  String? get updatedBy => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ApiBlockDataCopyWith<ApiBlockData> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ApiBlockDataCopyWith<$Res> {
  factory $ApiBlockDataCopyWith(
          ApiBlockData value, $Res Function(ApiBlockData) then) =
      _$ApiBlockDataCopyWithImpl<$Res, ApiBlockData>;
  @useResult
  $Res call(
      {@JsonKey(name: 'id') String? id,
      @JsonKey(name: 'code') String? code,
      @JsonKey(name: 'name') String? name,
      @JsonKey(name: 'isActive') bool? isActive,
      @JsonKey(name: 'customerType') String? customerType,
      @JsonKey(name: 'createdDate') String? createdDate,
      @JsonKey(name: 'createdBy') String? createdBy,
      @JsonKey(name: 'updatedDate') String? updatedDate,
      @JsonKey(name: 'updatedBy') String? updatedBy});
}

/// @nodoc
class _$ApiBlockDataCopyWithImpl<$Res, $Val extends ApiBlockData>
    implements $ApiBlockDataCopyWith<$Res> {
  _$ApiBlockDataCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? code = freezed,
    Object? name = freezed,
    Object? isActive = freezed,
    Object? customerType = freezed,
    Object? createdDate = freezed,
    Object? createdBy = freezed,
    Object? updatedDate = freezed,
    Object? updatedBy = freezed,
  }) {
    return _then(_value.copyWith(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      code: freezed == code
          ? _value.code
          : code // ignore: cast_nullable_to_non_nullable
              as String?,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      isActive: freezed == isActive
          ? _value.isActive
          : isActive // ignore: cast_nullable_to_non_nullable
              as bool?,
      customerType: freezed == customerType
          ? _value.customerType
          : customerType // ignore: cast_nullable_to_non_nullable
              as String?,
      createdDate: freezed == createdDate
          ? _value.createdDate
          : createdDate // ignore: cast_nullable_to_non_nullable
              as String?,
      createdBy: freezed == createdBy
          ? _value.createdBy
          : createdBy // ignore: cast_nullable_to_non_nullable
              as String?,
      updatedDate: freezed == updatedDate
          ? _value.updatedDate
          : updatedDate // ignore: cast_nullable_to_non_nullable
              as String?,
      updatedBy: freezed == updatedBy
          ? _value.updatedBy
          : updatedBy // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ApiBlockDataImplCopyWith<$Res>
    implements $ApiBlockDataCopyWith<$Res> {
  factory _$$ApiBlockDataImplCopyWith(
          _$ApiBlockDataImpl value, $Res Function(_$ApiBlockDataImpl) then) =
      __$$ApiBlockDataImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@JsonKey(name: 'id') String? id,
      @JsonKey(name: 'code') String? code,
      @JsonKey(name: 'name') String? name,
      @JsonKey(name: 'isActive') bool? isActive,
      @JsonKey(name: 'customerType') String? customerType,
      @JsonKey(name: 'createdDate') String? createdDate,
      @JsonKey(name: 'createdBy') String? createdBy,
      @JsonKey(name: 'updatedDate') String? updatedDate,
      @JsonKey(name: 'updatedBy') String? updatedBy});
}

/// @nodoc
class __$$ApiBlockDataImplCopyWithImpl<$Res>
    extends _$ApiBlockDataCopyWithImpl<$Res, _$ApiBlockDataImpl>
    implements _$$ApiBlockDataImplCopyWith<$Res> {
  __$$ApiBlockDataImplCopyWithImpl(
      _$ApiBlockDataImpl _value, $Res Function(_$ApiBlockDataImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? code = freezed,
    Object? name = freezed,
    Object? isActive = freezed,
    Object? customerType = freezed,
    Object? createdDate = freezed,
    Object? createdBy = freezed,
    Object? updatedDate = freezed,
    Object? updatedBy = freezed,
  }) {
    return _then(_$ApiBlockDataImpl(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      code: freezed == code
          ? _value.code
          : code // ignore: cast_nullable_to_non_nullable
              as String?,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      isActive: freezed == isActive
          ? _value.isActive
          : isActive // ignore: cast_nullable_to_non_nullable
              as bool?,
      customerType: freezed == customerType
          ? _value.customerType
          : customerType // ignore: cast_nullable_to_non_nullable
              as String?,
      createdDate: freezed == createdDate
          ? _value.createdDate
          : createdDate // ignore: cast_nullable_to_non_nullable
              as String?,
      createdBy: freezed == createdBy
          ? _value.createdBy
          : createdBy // ignore: cast_nullable_to_non_nullable
              as String?,
      updatedDate: freezed == updatedDate
          ? _value.updatedDate
          : updatedDate // ignore: cast_nullable_to_non_nullable
              as String?,
      updatedBy: freezed == updatedBy
          ? _value.updatedBy
          : updatedBy // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$ApiBlockDataImpl extends _ApiBlockData {
  const _$ApiBlockDataImpl(
      {@JsonKey(name: 'id') this.id,
      @JsonKey(name: 'code') this.code,
      @JsonKey(name: 'name') this.name,
      @JsonKey(name: 'isActive') this.isActive,
      @JsonKey(name: 'customerType') this.customerType,
      @JsonKey(name: 'createdDate') this.createdDate,
      @JsonKey(name: 'createdBy') this.createdBy,
      @JsonKey(name: 'updatedDate') this.updatedDate,
      @JsonKey(name: 'updatedBy') this.updatedBy})
      : super._();

  factory _$ApiBlockDataImpl.fromJson(Map<String, dynamic> json) =>
      _$$ApiBlockDataImplFromJson(json);

  @override
  @JsonKey(name: 'id')
  final String? id;
  @override
  @JsonKey(name: 'code')
  final String? code;
  @override
  @JsonKey(name: 'name')
  final String? name;
  @override
  @JsonKey(name: 'isActive')
  final bool? isActive;
  @override
  @JsonKey(name: 'customerType')
  final String? customerType;
  @override
  @JsonKey(name: 'createdDate')
  final String? createdDate;
  @override
  @JsonKey(name: 'createdBy')
  final String? createdBy;
  @override
  @JsonKey(name: 'updatedDate')
  final String? updatedDate;
  @override
  @JsonKey(name: 'updatedBy')
  final String? updatedBy;

  @override
  String toString() {
    return 'ApiBlockData(id: $id, code: $code, name: $name, isActive: $isActive, customerType: $customerType, createdDate: $createdDate, createdBy: $createdBy, updatedDate: $updatedDate, updatedBy: $updatedBy)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ApiBlockDataImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.code, code) || other.code == code) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.isActive, isActive) ||
                other.isActive == isActive) &&
            (identical(other.customerType, customerType) ||
                other.customerType == customerType) &&
            (identical(other.createdDate, createdDate) ||
                other.createdDate == createdDate) &&
            (identical(other.createdBy, createdBy) ||
                other.createdBy == createdBy) &&
            (identical(other.updatedDate, updatedDate) ||
                other.updatedDate == updatedDate) &&
            (identical(other.updatedBy, updatedBy) ||
                other.updatedBy == updatedBy));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, id, code, name, isActive,
      customerType, createdDate, createdBy, updatedDate, updatedBy);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ApiBlockDataImplCopyWith<_$ApiBlockDataImpl> get copyWith =>
      __$$ApiBlockDataImplCopyWithImpl<_$ApiBlockDataImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$ApiBlockDataImplToJson(
      this,
    );
  }
}

abstract class _ApiBlockData extends ApiBlockData {
  const factory _ApiBlockData(
          {@JsonKey(name: 'id') final String? id,
          @JsonKey(name: 'code') final String? code,
          @JsonKey(name: 'name') final String? name,
          @JsonKey(name: 'isActive') final bool? isActive,
          @JsonKey(name: 'customerType') final String? customerType,
          @JsonKey(name: 'createdDate') final String? createdDate,
          @JsonKey(name: 'createdBy') final String? createdBy,
          @JsonKey(name: 'updatedDate') final String? updatedDate,
          @JsonKey(name: 'updatedBy') final String? updatedBy}) =
      _$ApiBlockDataImpl;
  const _ApiBlockData._() : super._();

  factory _ApiBlockData.fromJson(Map<String, dynamic> json) =
      _$ApiBlockDataImpl.fromJson;

  @override
  @JsonKey(name: 'id')
  String? get id;
  @override
  @JsonKey(name: 'code')
  String? get code;
  @override
  @JsonKey(name: 'name')
  String? get name;
  @override
  @JsonKey(name: 'isActive')
  bool? get isActive;
  @override
  @JsonKey(name: 'customerType')
  String? get customerType;
  @override
  @JsonKey(name: 'createdDate')
  String? get createdDate;
  @override
  @JsonKey(name: 'createdBy')
  String? get createdBy;
  @override
  @JsonKey(name: 'updatedDate')
  String? get updatedDate;
  @override
  @JsonKey(name: 'updatedBy')
  String? get updatedBy;
  @override
  @JsonKey(ignore: true)
  _$$ApiBlockDataImplCopyWith<_$ApiBlockDataImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
