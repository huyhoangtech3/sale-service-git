import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';

import 'package:data/data.dart';
import 'package:shared/shared.dart';

@LazySingleton()
class AppApiService {
  AppApiService(
    this._noneAuthAppServerApiClient,
    this._authAppServerApiClient,
    this._randomUserApiClient,
  );
  final NoneAuthAppServerApiClient _noneAuthAppServerApiClient;
  final AuthAppServerApiClient _authAppServerApiClient;
  final RandomUserApiClient _randomUserApiClient;

  Future<ApiAuthResponseData?> login({
    required String username,
    required String password,
  }) async {
    return _noneAuthAppServerApiClient.request(
      method: RestMethod.post,
      path: '${UrlConstants.auth}/token',
      body: {
        'username': username,
        'password': password,
        'client_id': KeyCloakConstants.clientId,
        'client_secret': KeyCloakConstants.clientSecret,
        'grant_type': KeyCloakConstants.grantType,
      },
      options: Options(
        headers: {
          Headers.contentTypeHeader: Headers.formUrlEncodedContentType,
        },
      ),
      successResponseMapperType: SuccessResponseMapperType.jsonObject,
      errorResponseMapperType: ErrorResponseMapperType.jsonObject,
      decoder: (json) => ApiAuthResponseData.fromJson(json as Map<String, dynamic>),
    );
  }

  Future<List<ApiBlockData>?> findBlockByRM() {
    return _authAppServerApiClient.request(
      method: RestMethod.get,
      path: '${UrlConstants.category}/blocks/findBlockByRM',
      successResponseMapperType: SuccessResponseMapperType.jsonArray,
      errorResponseMapperType: ErrorResponseMapperType.jsonObject,
      decoder: (json) => ApiBlockData.fromJson(json as Map<String, dynamic>),
    );
  }

  Future<ApiBasicUserInfoData?> getUserInfo({
    required String username,
    required int externalData,
  }) async {
    return _authAppServerApiClient.request(
      method: RestMethod.get,
      path: '${UrlConstants.rmProfile}/employees/username/$username',
      queryParameters: {
        'externalData': externalData,
      },
      successResponseMapperType: SuccessResponseMapperType.jsonObject,
      errorResponseMapperType: ErrorResponseMapperType.jsonObject,
      decoder: (json) => ApiBasicUserInfoData.fromJson(json as Map<String, dynamic>),
    );
  }

  Future<ListResponse<ApiBasicUserInfoData>?> getDetailUserInfoByHrsCode({
    required String hrsCode,
  }) {
    return _authAppServerApiClient.request(
      method: RestMethod.get,
      path: '${UrlConstants.rmProfile}/employees',
      queryParameters: {
        'hrsCode': hrsCode,
      },
      successResponseMapperType: SuccessResponseMapperType.listJsonObject,
      errorResponseMapperType: ErrorResponseMapperType.jsonObject,
      decoder: (json) => ApiBasicUserInfoData.fromJson(json as Map<String, dynamic>),
    );
  }

  Future<List<ApiBranchData>?> getBranchByUser({
    required String rsId,
  }) {
    return _authAppServerApiClient.request(
      method: RestMethod.get,
      path: '${UrlConstants.category}/branches/branchesByUser',
      queryParameters: {
        'rsId': rsId,
      },
      successResponseMapperType: SuccessResponseMapperType.jsonArray,
      errorResponseMapperType: ErrorResponseMapperType.jsonObject,
      decoder: (json) => ApiBranchData.fromJson(json as Map<String, dynamic>),
    );
  }

  Future<List<ApiCustomerData>?> getCustomers({
    required int pageNumber,
    required int pageSize,
    required String rsId,
    required String scope,
    required String search,
    required String searchFastType,
    required String customerType,
  }) {
    return _authAppServerApiClient.request(
      method: RestMethod.post,
      path: '${UrlConstants.customer}/customers/findAllFetch',
      body: {
        'pageNumber': pageNumber,
        'pageSize': pageSize,
        'rsId': rsId,
        'scope': scope,
        'search': search,
        'searchFastType': searchFastType,
        'customerType': customerType,
      },
      successResponseMapperType: SuccessResponseMapperType.jsonArray,
      errorResponseMapperType: ErrorResponseMapperType.jsonObject,
      decoder: (json) => ApiCustomerData.fromJson(json as Map<String, dynamic>),
    );
  }

  Future<void> logout({
    required String refreshToken,
  }) async {
    await _noneAuthAppServerApiClient.request(
      method: RestMethod.post,
      path: '${UrlConstants.auth}/logout',
      body: {
        'client_id': KeyCloakConstants.clientId,
        'client_secret': KeyCloakConstants.clientSecret,
        'refresh_token': refreshToken,
      },
      options: Options(
        headers: {
          Headers.contentTypeHeader: Headers.formUrlEncodedContentType,
        },
      ),

      decoder: (json) => ApiAuthResponseData.fromJson(json as Map<String, dynamic>),
    );
  }


  //TODO: Test
  Future<ListResponse<ApiBlockData>?> getBlocks({
    required int page,
    required int size,
  }) {
    return _authAppServerApiClient.request(
      method: RestMethod.get,
      path: '${UrlConstants.category}/blocks',
      queryParameters: {
        'page': page,
        'size': size,
      },
      successResponseMapperType: SuccessResponseMapperType.listJsonObject,
      errorResponseMapperType: ErrorResponseMapperType.jsonObject,
      decoder: (json) => ApiBlockData.fromJson(json as Map<String, dynamic>),
    );
  }

  Future<ApiUserData?> getMe() async {
    return _authAppServerApiClient.request(
      method: RestMethod.get,
      path: '/v1/me',
      successResponseMapperType: SuccessResponseMapperType.jsonObject,
      decoder: (json) => ApiUserData.fromJson(json as Map<String, dynamic>),
    );
  }

  Future<ResultsListResponse<ApiUserData>?> getUsers({
    required int page,
    required int? limit,
  }) {
    return _randomUserApiClient.request(
      method: RestMethod.get,
      path: '',
      queryParameters: {
        'page': page,
        'results': limit,
      },
      successResponseMapperType: SuccessResponseMapperType.resultsJsonArray,
      decoder: (json) => ApiUserData.fromJson(json as Map<String, dynamic>),
    );
  }
}
