// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:data/data.dart' as _i29;
import 'package:data/src/di/di.dart' as _i42;
import 'package:data/src/repository/repository_impl.dart' as _i41;
import 'package:data/src/repository/source/api/app_api_service.dart' as _i39;
import 'package:data/src/repository/source/api/client/auth_app_server_api_client.dart'
    as _i38;
import 'package:data/src/repository/source/api/client/none_auth_app_server_api_client.dart'
    as _i34;
import 'package:data/src/repository/source/api/client/random_user_api_client.dart'
    as _i25;
import 'package:data/src/repository/source/api/client/refresh_token_api_client.dart'
    as _i35;
import 'package:data/src/repository/source/api/mapper/api_block_data_mapper.dart'
    as _i18;
import 'package:data/src/repository/source/api/mapper/api_branch_data_mapper.dart'
    as _i22;
import 'package:data/src/repository/source/api/mapper/api_customer_mapper.dart'
    as _i24;
import 'package:data/src/repository/source/api/mapper/api_image_url_data_mapper.dart'
    as _i5;
import 'package:data/src/repository/source/api/mapper/api_pageable_user_info_data_mapper.dart'
    as _i20;
import 'package:data/src/repository/source/api/mapper/api_token_data_mapper.dart'
    as _i6;
import 'package:data/src/repository/source/api/mapper/api_user_data_mapper.dart'
    as _i32;
import 'package:data/src/repository/source/api/mapper/api_user_info_data_mapper.dart'
    as _i19;
import 'package:data/src/repository/source/api/mapper/base/base_error_response_mapper/firebase_storage_error_response_mapper.dart'
    as _i7;
import 'package:data/src/repository/source/api/mapper/base/base_error_response_mapper/goong_error_response_mapper.dart'
    as _i8;
import 'package:data/src/repository/source/api/mapper/base/base_error_response_mapper/json_array_error_response_mapper.dart'
    as _i9;
import 'package:data/src/repository/source/api/mapper/base/base_error_response_mapper/json_object_error_response_mapper.dart'
    as _i10;
import 'package:data/src/repository/source/api/mapper/base/base_error_response_mapper/line_error_response_mapper.dart'
    as _i11;
import 'package:data/src/repository/source/api/mapper/base/base_error_response_mapper/twitter_error_response_mapper.dart'
    as _i12;
import 'package:data/src/repository/source/api/middleware/access_token_interceptor.dart'
    as _i33;
import 'package:data/src/repository/source/api/middleware/connectivity_interceptor.dart'
    as _i13;
import 'package:data/src/repository/source/api/middleware/header_interceptor.dart'
    as _i30;
import 'package:data/src/repository/source/api/middleware/refresh_token_interceptor.dart'
    as _i37;
import 'package:data/src/repository/source/api/refresh_token_api_service.dart'
    as _i36;
import 'package:data/src/repository/source/database/app_database.dart' as _i26;
import 'package:data/src/repository/source/database/mapper/branch_data_mapper.dart'
    as _i23;
import 'package:data/src/repository/source/database/mapper/local_image_url_data_mapper.dart'
    as _i14;
import 'package:data/src/repository/source/database/mapper/local_user_data_mapper.dart'
    as _i28;
import 'package:data/src/repository/source/database/mapper/user_info_data_mapper.dart'
    as _i21;
import 'package:data/src/repository/source/preference/app_preferences.dart'
    as _i27;
import 'package:data/src/repository/source/preference/mapper/preference_user_data_mapper.dart'
    as _i15;
import 'package:data/src/repository/source/shared/mapper/gender_data_mapper.dart'
    as _i16;
import 'package:data/src/repository/source/shared/mapper/language_code_data_mapper.dart'
    as _i17;
import 'package:domain/domain.dart' as _i40;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;
import 'package:objectbox/objectbox.dart' as _i4;
import 'package:shared/shared.dart' as _i31;
import 'package:shared_preferences/shared_preferences.dart' as _i3;

extension GetItInjectableX on _i1.GetIt {
// initializes the registration of main-scope dependencies inside of GetIt
  Future<_i1.GetIt> init({
    String? environment,
    _i2.EnvironmentFilter? environmentFilter,
  }) async {
    final gh = _i2.GetItHelper(
      this,
      environment,
      environmentFilter,
    );
    final serviceModule = _$ServiceModule();
    await gh.factoryAsync<_i3.SharedPreferences>(
      () => serviceModule.prefs,
      preResolve: true,
    );
    await gh.factoryAsync<_i4.Store>(
      () => serviceModule.getStore(),
      preResolve: true,
    );
    gh.factory<_i5.ApiImageUrlDataMapper>(() => _i5.ApiImageUrlDataMapper());
    gh.factory<_i6.ApiTokenDataMapper>(() => _i6.ApiTokenDataMapper());
    gh.factory<_i7.FirebaseStorageErrorResponseMapper>(
        () => _i7.FirebaseStorageErrorResponseMapper());
    gh.factory<_i8.GoongErrorResponseMapper>(
        () => _i8.GoongErrorResponseMapper());
    gh.factory<_i9.JsonArrayErrorResponseMapper>(
        () => _i9.JsonArrayErrorResponseMapper());
    gh.factory<_i10.JsonObjectErrorResponseMapper>(
        () => _i10.JsonObjectErrorResponseMapper());
    gh.factory<_i11.LineErrorResponseMapper>(
        () => _i11.LineErrorResponseMapper());
    gh.factory<_i12.TwitterErrorResponseMapper>(
        () => _i12.TwitterErrorResponseMapper());
    gh.factory<_i13.ConnectivityInterceptor>(
        () => _i13.ConnectivityInterceptor());
    gh.factory<_i14.LocalImageUrlDataMapper>(
        () => _i14.LocalImageUrlDataMapper());
    gh.factory<_i15.PreferenceUserDataMapper>(
        () => _i15.PreferenceUserDataMapper());
    gh.factory<_i16.GenderDataMapper>(() => _i16.GenderDataMapper());
    gh.factory<_i17.LanguageCodeDataMapper>(
        () => _i17.LanguageCodeDataMapper());
    gh.factory<_i18.ApiBlockDataMapper>(() => _i18.ApiBlockDataMapper());
    gh.factory<_i19.ApiUserInfoDataMapper>(() => _i19.ApiUserInfoDataMapper());
    gh.factory<_i20.ApiPageableUserInfoDataMapper>(
        () => _i20.ApiPageableUserInfoDataMapper());
    gh.factory<_i21.UserInfoDataMapper>(() => _i21.UserInfoDataMapper());
    gh.factory<_i22.ApiBranchDataMapper>(() => _i22.ApiBranchDataMapper());
    gh.factory<_i23.BranchDataMapper>(() => _i23.BranchDataMapper());
    gh.factory<_i24.ApiCustomerDataMapper>(() => _i24.ApiCustomerDataMapper());
    gh.lazySingleton<_i25.RandomUserApiClient>(
        () => _i25.RandomUserApiClient());
    gh.lazySingleton<_i26.AppDatabase>(() => _i26.AppDatabase(gh<_i4.Store>()));
    gh.lazySingleton<_i27.AppPreferences>(
        () => _i27.AppPreferences(gh<_i3.SharedPreferences>()));
    gh.factory<_i28.LocalUserDataMapper>(() => _i28.LocalUserDataMapper(
          gh<_i29.GenderDataMapper>(),
          gh<_i29.LocalImageUrlDataMapper>(),
        ));
    gh.factory<_i30.HeaderInterceptor>(
        () => _i30.HeaderInterceptor(gh<_i31.AppInfo>()));
    gh.factory<_i32.ApiUserDataMapper>(() => _i32.ApiUserDataMapper(
          gh<_i29.GenderDataMapper>(),
          gh<_i29.ApiImageUrlDataMapper>(),
        ));
    gh.factory<_i33.AccessTokenInterceptor>(
        () => _i33.AccessTokenInterceptor(gh<_i29.AppPreferences>()));
    gh.lazySingleton<_i34.NoneAuthAppServerApiClient>(
        () => _i34.NoneAuthAppServerApiClient(gh<_i29.HeaderInterceptor>()));
    gh.lazySingleton<_i35.RefreshTokenApiClient>(
        () => _i35.RefreshTokenApiClient(
              gh<_i29.HeaderInterceptor>(),
              gh<_i29.AccessTokenInterceptor>(),
            ));
    gh.lazySingleton<_i36.RefreshTokenApiService>(
        () => _i36.RefreshTokenApiService(gh<_i29.RefreshTokenApiClient>()));
    gh.factory<_i37.RefreshTokenInterceptor>(() => _i37.RefreshTokenInterceptor(
          gh<_i29.AppPreferences>(),
          gh<_i29.RefreshTokenApiService>(),
          gh<_i29.NoneAuthAppServerApiClient>(),
        ));
    gh.lazySingleton<_i38.AuthAppServerApiClient>(
        () => _i38.AuthAppServerApiClient(
              gh<_i29.HeaderInterceptor>(),
              gh<_i29.AccessTokenInterceptor>(),
              gh<_i29.RefreshTokenInterceptor>(),
            ));
    gh.lazySingleton<_i39.AppApiService>(() => _i39.AppApiService(
          gh<_i29.NoneAuthAppServerApiClient>(),
          gh<_i29.AuthAppServerApiClient>(),
          gh<_i29.RandomUserApiClient>(),
        ));
    gh.lazySingleton<_i40.Repository>(() => _i41.RepositoryImpl(
          gh<_i29.AppApiService>(),
          gh<_i29.AppPreferences>(),
          gh<_i29.AppDatabase>(),
          gh<_i29.PreferenceUserDataMapper>(),
          gh<_i29.ApiUserDataMapper>(),
          gh<_i29.LanguageCodeDataMapper>(),
          gh<_i29.GenderDataMapper>(),
          gh<_i29.LocalUserDataMapper>(),
          gh<_i29.ApiBlockDataMapper>(),
          gh<_i29.ApiUserInfoDataMapper>(),
          gh<_i29.ApiPageableUserInfoDataMapper>(),
          gh<_i29.UserInfoDataMapper>(),
          gh<_i29.ApiBranchDataMapper>(),
          gh<_i29.BranchDataMapper>(),
          gh<_i29.ApiCustomerDataMapper>(),
        ));
    return this;
  }
}

class _$ServiceModule extends _i42.ServiceModule {}
