/// GENERATED CODE - DO NOT MODIFY BY HAND
/// *****************************************************
///  FlutterGen
/// *****************************************************

// coverage:ignore-file
// ignore_for_file: type=lint
// ignore_for_file: directives_ordering,unnecessary_import,implicit_dynamic_list_literal,deprecated_member_use

import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/services.dart';

class $AssetsImagesGen {
  const $AssetsImagesGen();

  /// File path: assets/images/accountant.png
  AssetGenImage get accountant =>
      const AssetGenImage('assets/images/accountant.png');

  /// File path: assets/images/app_banner.png
  AssetGenImage get appBanner =>
      const AssetGenImage('assets/images/app_banner.png');

  /// File path: assets/images/app_logo.svg
  SvgGenImage get appLogo => const SvgGenImage('assets/images/app_logo.svg');

  /// File path: assets/images/ava_man.png
  AssetGenImage get avaMan => const AssetGenImage('assets/images/ava_man.png');

  /// File path: assets/images/ava_woman.png
  AssetGenImage get avaWoman =>
      const AssetGenImage('assets/images/ava_woman.png');

  /// File path: assets/images/bg_login.png
  AssetGenImage get bgLoginPng =>
      const AssetGenImage('assets/images/bg_login.png');

  /// File path: assets/images/bg_login.svg
  SvgGenImage get bgLoginSvg => const SvgGenImage('assets/images/bg_login.svg');

  /// File path: assets/images/camera.svg
  SvgGenImage get camera => const SvgGenImage('assets/images/camera.svg');

  /// File path: assets/images/chief_acc.png
  AssetGenImage get chiefAcc =>
      const AssetGenImage('assets/images/chief_acc.png');

  /// File path: assets/images/circle_close.svg
  SvgGenImage get circleClose =>
      const SvgGenImage('assets/images/circle_close.svg');

  /// File path: assets/images/close.png
  AssetGenImage get close => const AssetGenImage('assets/images/close.png');

  /// File path: assets/images/crm_nextgen.svg
  SvgGenImage get crmNextgen =>
      const SvgGenImage('assets/images/crm_nextgen.svg');

  /// File path: assets/images/error.svg
  SvgGenImage get error => const SvgGenImage('assets/images/error.svg');

  /// File path: assets/images/exist.png
  AssetGenImage get exist => const AssetGenImage('assets/images/exist.png');

  /// File path: assets/images/exist_t24.png
  AssetGenImage get existT24 =>
      const AssetGenImage('assets/images/exist_t24.png');

  /// File path: assets/images/ic_apk.png
  AssetGenImage get icApk => const AssetGenImage('assets/images/ic_apk.png');

  /// File path: assets/images/ic_bell.png
  AssetGenImage get icBell => const AssetGenImage('assets/images/ic_bell.png');

  /// File path: assets/images/ic_birthday.png
  AssetGenImage get icBirthday =>
      const AssetGenImage('assets/images/ic_birthday.png');

  /// File path: assets/images/ic_bmp.png
  AssetGenImage get icBmp => const AssetGenImage('assets/images/ic_bmp.png');

  /// File path: assets/images/ic_customer_360.svg
  SvgGenImage get icCustomer360 =>
      const SvgGenImage('assets/images/ic_customer_360.svg');

  /// File path: assets/images/ic_doc.png
  AssetGenImage get icDoc => const AssetGenImage('assets/images/ic_doc.png');

  /// File path: assets/images/ic_logo.png
  AssetGenImage get icLogoPng =>
      const AssetGenImage('assets/images/ic_logo.png');

  /// File path: assets/images/ic_logo.svg
  SvgGenImage get icLogoSvg => const SvgGenImage('assets/images/ic_logo.svg');

  /// File path: assets/images/ic_man.png
  AssetGenImage get icMan => const AssetGenImage('assets/images/ic_man.png');

  /// File path: assets/images/ic_new_birthday.png
  AssetGenImage get icNewBirthday =>
      const AssetGenImage('assets/images/ic_new_birthday.png');

  /// File path: assets/images/ic_pdf.png
  AssetGenImage get icPdf => const AssetGenImage('assets/images/ic_pdf.png');

  /// File path: assets/images/ic_potential_customer.png
  AssetGenImage get icPotentialCustomer =>
      const AssetGenImage('assets/images/ic_potential_customer.png');

  /// File path: assets/images/ic_tab_customer.png
  AssetGenImage get icTabCustomer =>
      const AssetGenImage('assets/images/ic_tab_customer.png');

  /// File path: assets/images/ic_tab_dashboard.png
  AssetGenImage get icTabDashboard =>
      const AssetGenImage('assets/images/ic_tab_dashboard.png');

  /// File path: assets/images/ic_tab_sale_manager.png
  AssetGenImage get icTabSaleManager =>
      const AssetGenImage('assets/images/ic_tab_sale_manager.png');

  /// File path: assets/images/ic_tab_salekit.png
  AssetGenImage get icTabSalekit =>
      const AssetGenImage('assets/images/ic_tab_salekit.png');

  /// File path: assets/images/ic_tab_sales.png
  AssetGenImage get icTabSales =>
      const AssetGenImage('assets/images/ic_tab_sales.png');

  /// File path: assets/images/ic_tab_warning.png
  AssetGenImage get icTabWarning =>
      const AssetGenImage('assets/images/ic_tab_warning.png');

  /// File path: assets/images/ic_tool_interbankrate.png
  AssetGenImage get icToolInterbankrate =>
      const AssetGenImage('assets/images/ic_tool_interbankrate.png');

  /// File path: assets/images/ic_tool_interest.png
  AssetGenImage get icToolInterest =>
      const AssetGenImage('assets/images/ic_tool_interest.png');

  /// File path: assets/images/ic_tool_listedrate.png
  AssetGenImage get icToolListedrate =>
      const AssetGenImage('assets/images/ic_tool_listedrate.png');

  /// File path: assets/images/ic_tool_loanplan.png
  AssetGenImage get icToolLoanplan =>
      const AssetGenImage('assets/images/ic_tool_loanplan.png');

  /// File path: assets/images/ic_tool_open.png
  AssetGenImage get icToolOpen =>
      const AssetGenImage('assets/images/ic_tool_open.png');

  /// File path: assets/images/ic_tool_savemoney.png
  AssetGenImage get icToolSavemoney =>
      const AssetGenImage('assets/images/ic_tool_savemoney.png');

  /// File path: assets/images/ic_tool_tariff.png
  AssetGenImage get icToolTariff =>
      const AssetGenImage('assets/images/ic_tool_tariff.png');

  /// File path: assets/images/ic_txt.png
  AssetGenImage get icTxt => const AssetGenImage('assets/images/ic_txt.png');

  /// File path: assets/images/ic_woman.png
  AssetGenImage get icWoman =>
      const AssetGenImage('assets/images/ic_woman.png');

  /// File path: assets/images/ic_xls.png
  AssetGenImage get icXls => const AssetGenImage('assets/images/ic_xls.png');

  /// File path: assets/images/ic_zip.png
  AssetGenImage get icZip => const AssetGenImage('assets/images/ic_zip.png');

  /// File path: assets/images/icon_back.svg
  SvgGenImage get iconBack => const SvgGenImage('assets/images/icon_back.svg');

  /// File path: assets/images/icon_close.svg
  SvgGenImage get iconClose =>
      const SvgGenImage('assets/images/icon_close.svg');

  /// File path: assets/images/ios_app_icon.png
  AssetGenImage get iosAppIcon =>
      const AssetGenImage('assets/images/ios_app_icon.png');

  /// File path: assets/images/legal_rep.png
  AssetGenImage get legalRep =>
      const AssetGenImage('assets/images/legal_rep.png');

  /// File path: assets/images/logo.svg
  SvgGenImage get logo => const SvgGenImage('assets/images/logo.svg');

  /// File path: assets/images/overlay.svg
  SvgGenImage get overlay => const SvgGenImage('assets/images/overlay.svg');

  /// List of all assets
  List<dynamic> get values => [
        accountant,
        appBanner,
        appLogo,
        avaMan,
        avaWoman,
        bgLoginPng,
        bgLoginSvg,
        camera,
        chiefAcc,
        circleClose,
        close,
        crmNextgen,
        error,
        exist,
        existT24,
        icApk,
        icBell,
        icBirthday,
        icBmp,
        icCustomer360,
        icDoc,
        icLogoPng,
        icLogoSvg,
        icMan,
        icNewBirthday,
        icPdf,
        icPotentialCustomer,
        icTabCustomer,
        icTabDashboard,
        icTabSaleManager,
        icTabSalekit,
        icTabSales,
        icTabWarning,
        icToolInterbankrate,
        icToolInterest,
        icToolListedrate,
        icToolLoanplan,
        icToolOpen,
        icToolSavemoney,
        icToolTariff,
        icTxt,
        icWoman,
        icXls,
        icZip,
        iconBack,
        iconClose,
        iosAppIcon,
        legalRep,
        logo,
        overlay
      ];
}

class Assets {
  Assets._();

  static const String australia = 'assets/australia.json';
  static const $AssetsImagesGen images = $AssetsImagesGen();
  static const String vietnam = 'assets/vietnam.json';
  static const String worldMap = 'assets/world_map.json';

  /// List of all assets
  static List<String> get values => [australia, vietnam, worldMap];
}

class AssetGenImage {
  const AssetGenImage(this._assetName);

  final String _assetName;

  Image image({
    Key? key,
    AssetBundle? bundle,
    ImageFrameBuilder? frameBuilder,
    ImageErrorWidgetBuilder? errorBuilder,
    String? semanticLabel,
    bool excludeFromSemantics = false,
    double? scale,
    double? width,
    double? height,
    Color? color,
    Animation<double>? opacity,
    BlendMode? colorBlendMode,
    BoxFit? fit,
    AlignmentGeometry alignment = Alignment.center,
    ImageRepeat repeat = ImageRepeat.noRepeat,
    Rect? centerSlice,
    bool matchTextDirection = false,
    bool gaplessPlayback = false,
    bool isAntiAlias = false,
    String? package,
    FilterQuality filterQuality = FilterQuality.low,
    int? cacheWidth,
    int? cacheHeight,
  }) {
    return Image.asset(
      _assetName,
      key: key,
      bundle: bundle,
      frameBuilder: frameBuilder,
      errorBuilder: errorBuilder,
      semanticLabel: semanticLabel,
      excludeFromSemantics: excludeFromSemantics,
      scale: scale,
      width: width,
      height: height,
      color: color,
      opacity: opacity,
      colorBlendMode: colorBlendMode,
      fit: fit,
      alignment: alignment,
      repeat: repeat,
      centerSlice: centerSlice,
      matchTextDirection: matchTextDirection,
      gaplessPlayback: gaplessPlayback,
      isAntiAlias: isAntiAlias,
      package: package,
      filterQuality: filterQuality,
      cacheWidth: cacheWidth,
      cacheHeight: cacheHeight,
    );
  }

  ImageProvider provider({
    AssetBundle? bundle,
    String? package,
  }) {
    return AssetImage(
      _assetName,
      bundle: bundle,
      package: package,
    );
  }

  String get path => _assetName;

  String get keyName => _assetName;
}

class SvgGenImage {
  const SvgGenImage(this._assetName);

  final String _assetName;

  SvgPicture svg({
    Key? key,
    bool matchTextDirection = false,
    AssetBundle? bundle,
    String? package,
    double? width,
    double? height,
    BoxFit fit = BoxFit.contain,
    AlignmentGeometry alignment = Alignment.center,
    bool allowDrawingOutsideViewBox = false,
    WidgetBuilder? placeholderBuilder,
    String? semanticsLabel,
    bool excludeFromSemantics = false,
    SvgTheme theme = const SvgTheme(),
    ColorFilter? colorFilter,
    Clip clipBehavior = Clip.hardEdge,
    @deprecated Color? color,
    @deprecated BlendMode colorBlendMode = BlendMode.srcIn,
    @deprecated bool cacheColorFilter = false,
  }) {
    return SvgPicture.asset(
      _assetName,
      key: key,
      matchTextDirection: matchTextDirection,
      bundle: bundle,
      package: package,
      width: width,
      height: height,
      fit: fit,
      alignment: alignment,
      allowDrawingOutsideViewBox: allowDrawingOutsideViewBox,
      placeholderBuilder: placeholderBuilder,
      semanticsLabel: semanticsLabel,
      excludeFromSemantics: excludeFromSemantics,
      theme: theme,
      colorFilter: colorFilter,
      color: color,
      colorBlendMode: colorBlendMode,
      clipBehavior: clipBehavior,
      cacheColorFilter: cacheColorFilter,
    );
  }

  String get path => _assetName;

  String get keyName => _assetName;
}
