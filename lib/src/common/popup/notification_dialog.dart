import 'dart:math';

import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:resources/resources.dart';
import 'package:base_architecture_flutter/src/app.dart';
import 'package:shared/shared.dart';

class NotificationDialog extends StatefulWidget {
  const NotificationDialog({super.key});

  @override
  State<StatefulWidget> createState() {
    return _NotificationDialogState();
  }
}

class _NotificationDialogState extends BasePageState<NotificationDialog, InterbankBloc> {
  List<NotificationEmployee> employees = <NotificationEmployee>[];
  late NotificationDataSource employeeDataSource;

  @override
  void initState() {
    employees = getEmployeeData();
    employeeDataSource = NotificationDataSource(employeeData: employees);
    employeeDataSource.addColumnGroup(ColumnGroup(name: '1', sortGroupRows: false));
  }

  @override
  Widget buildPage(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(Dimens.d0.responsive()),
      margin: EdgeInsets.fromLTRB(0.0, 0.0, Dimens.d780.responsive(), 0.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(0),
        color: Colors.white,
      ),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(Dimens.d4.responsive()),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('0 Chưa đọc', style: AppTextStyles.s12w400Tertiary()),
                Text('Đánh dấu đã đọc toàn bộ', style: AppTextStyles.s12w400Secondary()),
              ],
            ),
          ),
          SizedBox(
            height: Dimens.d1.responsive(),
            child: Container(
              decoration: BoxDecoration(
                color: Colors.orangeAccent,
              ),
            ),
          ),
          SizedBox(
            width: Dimens.d740.responsive(),
            height: Dimens.d520.responsive(),
            child: SfDataGridTheme(
              data: SfDataGridThemeData(headerColor: Colors.orange.shade50, indentColumnWidth: 0),
              child: SfDataGrid(
                onQueryRowHeight: (details) {
                  return details.rowIndex == 0 ? 0.0 : details.rowIndex == 1 ? 0.0 : Dimens.d84.responsive();
                },
                allowSwiping: true,
                swipeMaxOffset: 100.0,
                selectionMode: SelectionMode.multiple,
                source: employeeDataSource,
                columnWidthMode: ColumnWidthMode.fill,
                allowPullToRefresh: true,
                loadMoreViewBuilder: (BuildContext context, LoadMoreRows loadMoreRows) {
                  Future<String> loadRows() async {
                    await loadMoreRows();
                    return Future<String>.value('Completed');
                  }
                  return FutureBuilder<String>(
                      initialData: 'loading',
                      future: loadRows(),
                      builder: (context, snapShot) {
                        if (snapShot.data == 'loading') {
                          return Container(
                              height: 60.0,
                              width: double.infinity,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: BorderDirectional(
                                      top: BorderSide(
                                          width: 1.0,
                                          color: Color.fromRGBO(0, 0, 0, 0.26)))),
                              alignment: Alignment.center,
                              child: CircularProgressIndicator(
                                  valueColor: AlwaysStoppedAnimation(Colors.deepPurple)));
                        } else {
                          return SizedBox.fromSize(size: Size.zero);
                        }
                      });
                },
                startSwipeActionsBuilder: (BuildContext context, DataGridRow row, int rowIndex) {
                  return GestureDetector(
                      onTap: () {
                        employeeDataSource._employeeData.insert(rowIndex, DataGridRow(cells: [
                          DataGridCell<NotificationEmployee>(
                            columnName: '1',
                            value: NotificationEmployee('1', 'Today', NotificationCustomers(23, '1084115', 'Nguyễn Huy Hoàng', '19910401', 'VUTHITHUTRANG@MBBANK.COM.VN', 'FEMALE', '0969435919', 'PRIVATE')),),
                        ]));
                        employeeDataSource.updateDataGridSource();
                      },
                      child: Container(
                          color: Colors.greenAccent,
                          child: Center(
                            child: Icon(Icons.add),
                          )));
                },
                endSwipeActionsBuilder: (BuildContext context, DataGridRow row, int rowIndex) {
                  return GestureDetector(
                      onTap: () {
                        employeeDataSource._employeeData.removeAt(rowIndex);
                        employeeDataSource.updateDataGridSource();
                      },
                      child: Container(
                          color: Colors.redAccent,
                          child: Center(
                            child: Icon(Icons.delete),
                          )));
                },
                columns: employeeDataSource.columns,
              ),
            ),
          ),
        ],
      ),
    );
  }

  List<NotificationEmployee> getEmployeeData() {
    return [
      NotificationEmployee('1', 'Today', NotificationCustomers(23, '1084115', 'Nguyễn Huy Hoàng', '19910401', '21:32 09/09/2024', 'FEMALE', '0969435919', 'Bạn vừa nhận được danh sách cảnh báo khi truy cập vào module cảnh báo.')),
      NotificationEmployee('2', 'Tomorrow', NotificationCustomers(24, '2084115', 'Nguyễn Huy Hoàng A', '19930401', '21:32 09/09/2024', 'MALE', '0969435919', 'Bạn đã được phân giao 1 khách hàng của chiến dịch: Chiến dịch Landing page. Hãy truy cập vào module chiến dịch để nhận thông tin và tiếp cận SLA')),
      NotificationEmployee('3', 'This Week', NotificationCustomers(25, '3084115', 'Nguyễn Huy Hoàng B', '19900401', '21:32 09/09/2024', 'MALE', '0969435919', 'Cập nhật sản phẩm tiếp nhận độc quyền')),
      NotificationEmployee('4', '09/2024', NotificationCustomers(26, '4084115', 'Nguyễn Huy Hoàng C', '19990401', '21:32 09/09/2024', 'FEMALE', '0969435919', 'Bạn bị xếp hạng Bscore nhóm D. Vui lòng thao tác tại module Cảnh báo để xem danh sách chi tiết.')),
      NotificationEmployee('1', 'Today', NotificationCustomers(23, '1084115', 'Nguyễn Huy Hoàng', '19910401', '21:32 09/09/2024', 'FEMALE', '0969435919', 'Bạn vừa nhận được danh sách cảnh báo khi truy cập vào module cảnh báo.')),
      NotificationEmployee('2', 'Tomorrow', NotificationCustomers(24, '2084115', 'Nguyễn Huy Hoàng A', '19930401', '21:32 09/09/2024', 'MALE', '0969435919', 'Bạn đã được phân giao 1 khách hàng của chiến dịch: Chiến dịch Landing page. Hãy truy cập vào module chiến dịch để nhận thông tin và tiếp cận SLA')),
      NotificationEmployee('3', 'This Week', NotificationCustomers(25, '3084115', 'Nguyễn Huy Hoàng B', '19900401', '21:32 09/09/2024', 'MALE', '0969435919', 'Cập nhật sản phẩm tiếp nhận độc quyền')),
      NotificationEmployee('4', '10/2024', NotificationCustomers(26, '4084115', 'Nguyễn Huy Hoàng C', '19990401', '21:32 09/09/2024', 'FEMALE', '0969435919', 'Bạn bị xếp hạng Bscore nhóm D. Vui lòng thao tác tại module Cảnh báo để xem danh sách chi tiết.')),
    ];
  }
}

class NotificationCustomers {
  NotificationCustomers(this.age, this.customerCode, this.customerName, this.dateOfBirth, this.email, this.gender, this.phone, this.segment);
  final int age;
  final String customerCode;
  final String customerName;
  final String dateOfBirth;
  final String email;
  final String gender;
  final String phone;
  final String segment;
}

class NotificationEmployee {
  NotificationEmployee(this.groupId, this.groupName, this.customers);
  final String groupId;
  final String groupName;
  final NotificationCustomers customers;
}

class NotificationDataSource extends DataGridSource {
  NotificationDataSource({
    required List<NotificationEmployee> employeeData,
  }) {
    _employeeData = employeeData.map<DataGridRow>((e) => DataGridRow(cells: [
      DataGridCell<NotificationEmployee>(columnName: '1', value: e),
    ])).toList();
  }

  List<GridColumn> columns = [
    GridColumn(
        columnName: '1',
        label: Container(
            alignment: Alignment.center,
            child: Text('Customers'))),
  ];
  List<DataGridRow> _employeeData = [];
  List<NotificationEmployee> employeeData = [
    NotificationEmployee('1', 'Today', NotificationCustomers(23, '1084115', 'Nguyễn Huy Hoàng', '19910401', 'VUTHITHUTRANG@MBBANK.COM.VN', 'FEMALE', '0969435919', 'PRIVATE')),
    NotificationEmployee('2', 'Tomorrow', NotificationCustomers(24, '2084115', 'Nguyễn Huy Hoàng A', '19930401', 'VUTHITHUTRANG@MBBANK.COM.VN', 'MALE', '0969435919', 'PRIORITY')),
    NotificationEmployee('3', 'This Week', NotificationCustomers(25, '3084115', 'Nguyễn Huy Hoàng B', '19900401', 'VUTHITHUTRANG@MBBANK.COM.VN', 'MALE', '0969435919', 'PRIVATE')),
    NotificationEmployee('4', '09/2024', NotificationCustomers(26, '4084115', 'Nguyễn Huy Hoàng C', '19990401', 'VUTHITHUTRANG@MBBANK.COM.VN', 'FEMALE', '0969435919', 'PRIORITY')),
    NotificationEmployee('1', 'Today', NotificationCustomers(23, '1084115', 'Nguyễn Huy Hoàng', '19910401', 'VUTHITHUTRANG@MBBANK.COM.VN', 'FEMALE', '0969435919', 'PRIVATE')),
    NotificationEmployee('2', 'Tomorrow', NotificationCustomers(24, '2084115', 'Nguyễn Huy Hoàng A', '19930401', 'VUTHITHUTRANG@MBBANK.COM.VN', 'MALE', '0969435919', 'PRIORITY')),
    NotificationEmployee('3', 'This Week', NotificationCustomers(25, '3084115', 'Nguyễn Huy Hoàng B', '19900401', 'VUTHITHUTRANG@MBBANK.COM.VN', 'MALE', '0969435919', 'PRIVATE')),
    NotificationEmployee('4', '10/2024', NotificationCustomers(26, '4084115', 'Nguyễn Huy Hoàng C', '19990401', 'VUTHITHUTRANG@MBBANK.COM.VN', 'FEMALE', '0969435919', 'PRIORITY')),
  ];

  @override
  List<DataGridRow> get rows => _employeeData;

  @override
  DataGridRowAdapter buildRow(DataGridRow row) {
    return DataGridRowAdapter(
        cells: row.getCells().map<Widget>((e) {
          NotificationEmployee c = e.value as NotificationEmployee;
          return Container(
            padding: EdgeInsets.all(Dimens.d6.responsive()),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Align(
                  alignment: Alignment.topLeft,
                  child: Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: c.customers.gender == Constants.male ? Assets.images.avaMan.provider() : Assets.images.avaWoman.provider(),
                        fit: BoxFit.fitHeight,
                      ),
                    ),
                    child: SizedBox(
                      width: Dimens.d28.responsive(), height: Dimens.d28.responsive(),
                    ),
                  ),
                ),
                SizedBox(width: Dimens.d4.responsive()),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        SizedBox(
                          width: Dimens.d200.responsive(),
                          child: Text(
                            c.customers.segment,
                            style: AppTextStyles.s12w400Tertiary(),
                            maxLines: 4,
                            overflow: TextOverflow.ellipsis,
                            textAlign: TextAlign.justify,
                            softWrap: true,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: Dimens.d2.responsive()),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Icon(
                          Icons.date_range_outlined,
                          color: Colors.blueGrey,
                          size: Dimens.d8.responsive(),
                        ),
                        SizedBox(width: Dimens.d4.responsive()),
                        Text(
                          c.customers.email,
                          style: AppTextStyles.s10w300Tertiary(),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          );
        }).toList());
  }

  @override
  Future<void> handleRefresh() async {
    await Future.delayed(const Duration(seconds: 1));
    notifyListeners();
  }

  @override
  Future<void> handleLoadMoreRows() async {
    await Future.delayed(const Duration(seconds: 2));
    _addMoreRows(employeeData, 15);
    buildDataGridRows();
    notifyListeners();
  }

  void buildDataGridRows() {
    _employeeData = employeeData.map<DataGridRow>((e) {
      return DataGridRow(
          cells: columns.map<DataGridCell>((column) {
            dynamic cellValue;
            switch (column.columnName) {
              case '1':
                cellValue = e.customers;
                break;
              default:
                cellValue = null; // Handle any unexpected column names
            }
            return DataGridCell(
              columnName: column.columnName,
              value: cellValue,
            );
          }).toList());
    }).toList();
  }

  void _addMoreRows(List<NotificationEmployee> employeeData, int count) {
    final startIndex = employeeData.isNotEmpty ? employeeData.length : 0, endIndex = startIndex + count;
    for (int i = startIndex; i < endIndex; i++) {
      _employeeData.add(NotificationEmployee(
        employeeData[i].groupId + i.toString(),
        employeeData[i].groupName + i.toString(),
        employeeData[i].customers,
      ) as DataGridRow);
    }
  }

  void refreshDataGrid() {
    notifyListeners();
  }

  void updateDataGridSource() {
    notifyListeners();
  }
}

