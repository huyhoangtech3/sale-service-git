import 'dart:math';

import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:resources/resources.dart';
import 'package:base_architecture_flutter/src/app.dart';
import 'package:shared/shared.dart';

class BirthDayDialog extends StatefulWidget {
  const BirthDayDialog({super.key});

  @override
  State<StatefulWidget> createState() {
    return _BirthDayDialogState();
  }
}

class _BirthDayDialogState extends BasePageState<BirthDayDialog, InterbankBloc> {
  List<Employee> employees = <Employee>[];
  late EmployeeDataSource employeeDataSource;

  @override
  void initState() {
    employees = getEmployeeData();
    employeeDataSource = EmployeeDataSource(employeeData: employees);
    employeeDataSource.addColumnGroup(ColumnGroup(name: '1', sortGroupRows: false));
  }

  @override
  Widget buildPage(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(Dimens.d0.responsive()),
      margin: EdgeInsets.fromLTRB(0.0, 0.0, Dimens.d780.responsive(), 0.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(0),
        color: Colors.white,
      ),
      child: SizedBox(
        width: Dimens.d740.responsive(),
        height: Dimens.d550.responsive(),
        child: SfDataGridTheme(
          data: SfDataGridThemeData(headerColor: Colors.orange.shade50, indentColumnWidth: 0),
          child: SfDataGrid(
            onQueryRowHeight: (details) {
              return details.rowIndex == 0 ? 0.0 : Dimens.d52.responsive();
            },
            groupCaptionTitleFormat: '{Key}',
            allowExpandCollapseGroup: true,
            allowSwiping: true,
            swipeMaxOffset: 100.0,
            selectionMode: SelectionMode.multiple,
            source: employeeDataSource,
            columnWidthMode: ColumnWidthMode.fill,
            allowPullToRefresh: true,
            loadMoreViewBuilder: (BuildContext context, LoadMoreRows loadMoreRows) {
              Future<String> loadRows() async {
                await loadMoreRows();
                return Future<String>.value('Completed');
              }
              return FutureBuilder<String>(
                  initialData: 'loading',
                  future: loadRows(),
                  builder: (context, snapShot) {
                    if (snapShot.data == 'loading') {
                      return Container(
                          height: 60.0,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              border: BorderDirectional(
                                  top: BorderSide(
                                      width: 1.0,
                                      color: Color.fromRGBO(0, 0, 0, 0.26)))),
                          alignment: Alignment.center,
                          child: CircularProgressIndicator(
                              valueColor: AlwaysStoppedAnimation(Colors.deepPurple)));
                    } else {
                      return SizedBox.fromSize(size: Size.zero);
                    }
                  });
            },
            startSwipeActionsBuilder: (BuildContext context, DataGridRow row, int rowIndex) {
              return GestureDetector(
                  onTap: () {
                    employeeDataSource._employeeData.insert(rowIndex, DataGridRow(cells: [
                      DataGridCell<NotificationEmployee>(
                        columnName: '1',
                        value: NotificationEmployee('1', 'Today', NotificationCustomers(23, '1084115', 'Nguyễn Huy Hoàng', '19910401', 'VUTHITHUTRANG@MBBANK.COM.VN', 'FEMALE', '0969435919', 'PRIVATE')),),
                    ]));
                    employeeDataSource.updateDataGridSource();
                  },
                  child: Container(
                      color: Colors.greenAccent,
                      child: Center(
                        child: Icon(Icons.add),
                      )));
            },
            endSwipeActionsBuilder: (BuildContext context, DataGridRow row, int rowIndex) {
              return GestureDetector(
                  onTap: () {
                    employeeDataSource._employeeData.removeAt(rowIndex);
                    employeeDataSource.updateDataGridSource();
                  },
                  child: Container(
                      color: Colors.redAccent,
                      child: Center(
                        child: Icon(Icons.delete),
                      )));
            },
            columns: employeeDataSource.columns,
          ),
        ),
      ),
    );
  }

  List<Employee> getEmployeeData() {
    return [
      Employee('1', 'Today', Customers(23, '1084115', 'Nguyễn Huy Hoàng', '19910401', 'VUTHITHUTRANG@MBBANK.COM.VN', 'FEMALE', '0969435919', 'PRIVATE')),
      Employee('2', 'Tomorrow', Customers(24, '2084115', 'Nguyễn Huy Hoàng A', '19930401', 'VUTHITHUTRANG@MBBANK.COM.VN', 'MALE', '0969435919', 'PRIORITY')),
      Employee('3', 'This Week', Customers(25, '3084115', 'Nguyễn Huy Hoàng B', '19900401', 'VUTHITHUTRANG@MBBANK.COM.VN', 'MALE', '0969435919', 'PRIVATE')),
      Employee('4', '09/2024', Customers(26, '4084115', 'Nguyễn Huy Hoàng C', '19990401', 'VUTHITHUTRANG@MBBANK.COM.VN', 'FEMALE', '0969435919', 'PRIORITY')),
      Employee('1', 'Today', Customers(23, '1084115', 'Nguyễn Huy Hoàng', '19910401', 'VUTHITHUTRANG@MBBANK.COM.VN', 'FEMALE', '0969435919', 'PRIVATE')),
      Employee('2', 'Tomorrow', Customers(24, '2084115', 'Nguyễn Huy Hoàng A', '19930401', 'VUTHITHUTRANG@MBBANK.COM.VN', 'MALE', '0969435919', 'PRIORITY')),
      Employee('3', 'This Week', Customers(25, '3084115', 'Nguyễn Huy Hoàng B', '19900401', 'VUTHITHUTRANG@MBBANK.COM.VN', 'MALE', '0969435919', 'PRIVATE')),
      Employee('4', '10/2024', Customers(26, '4084115', 'Nguyễn Huy Hoàng C', '19990401', 'VUTHITHUTRANG@MBBANK.COM.VN', 'FEMALE', '0969435919', 'PRIORITY')),
    ];
  }
}

class Customers {
  Customers(this.age, this.customerCode, this.customerName, this.dateOfBirth, this.email, this.gender, this.phone, this.segment);
  final int age;
  final String customerCode;
  final String customerName;
  final String dateOfBirth;
  final String email;
  final String gender;
  final String phone;
  final String segment;
}

class Employee {
  Employee(this.groupId, this.groupName, this.customers);
  final String groupId;
  final String groupName;
  final Customers customers;
}

class EmployeeDataSource extends DataGridSource {
  EmployeeDataSource({
    required List<Employee> employeeData,
  }) {
    _employeeData = employeeData.map<DataGridRow>((e) => DataGridRow(cells: [
      DataGridCell<Employee>(columnName: '1', value: e),
    ])).toList();
  }

  List<GridColumn> columns = [
    GridColumn(
        columnName: '1',
        label: Container(
          alignment: Alignment.center,
          child: Text('Customers'))),
  ];
  List<DataGridRow> _employeeData = [];
  List<Employee> employeeData = [
    Employee('1', 'Today', Customers(23, '1084115', 'Nguyễn Huy Hoàng', '19910401', 'VUTHITHUTRANG@MBBANK.COM.VN', 'FEMALE', '0969435919', 'PRIVATE')),
    Employee('2', 'Tomorrow', Customers(24, '2084115', 'Nguyễn Huy Hoàng A', '19930401', 'VUTHITHUTRANG@MBBANK.COM.VN', 'MALE', '0969435919', 'PRIORITY')),
    Employee('3', 'This Week', Customers(25, '3084115', 'Nguyễn Huy Hoàng B', '19900401', 'VUTHITHUTRANG@MBBANK.COM.VN', 'MALE', '0969435919', 'PRIVATE')),
    Employee('4', '09/2024', Customers(26, '4084115', 'Nguyễn Huy Hoàng C', '19990401', 'VUTHITHUTRANG@MBBANK.COM.VN', 'FEMALE', '0969435919', 'PRIORITY')),
    Employee('1', 'Today', Customers(23, '1084115', 'Nguyễn Huy Hoàng', '19910401', 'VUTHITHUTRANG@MBBANK.COM.VN', 'FEMALE', '0969435919', 'PRIVATE')),
    Employee('2', 'Tomorrow', Customers(24, '2084115', 'Nguyễn Huy Hoàng A', '19930401', 'VUTHITHUTRANG@MBBANK.COM.VN', 'MALE', '0969435919', 'PRIORITY')),
    Employee('3', 'This Week', Customers(25, '3084115', 'Nguyễn Huy Hoàng B', '19900401', 'VUTHITHUTRANG@MBBANK.COM.VN', 'MALE', '0969435919', 'PRIVATE')),
    Employee('4', '10/2024', Customers(26, '4084115', 'Nguyễn Huy Hoàng C', '19990401', 'VUTHITHUTRANG@MBBANK.COM.VN', 'FEMALE', '0969435919', 'PRIORITY')),
  ];

  @override
  List<DataGridRow> get rows => _employeeData;

  @override
  DataGridRowAdapter buildRow(DataGridRow row) {
    return DataGridRowAdapter(
        cells: row.getCells().map<Widget>((e) {
          Employee c = e.value as Employee;
          return Container(
            padding: EdgeInsets.all(Dimens.d6.responsive()),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: c.customers.gender == Constants.male ? Assets.images.avaMan.provider() : Assets.images.avaWoman.provider(),
                        fit: BoxFit.fitHeight,
                      ),
                    ),
                    child: SizedBox(
                      width: Dimens.d28.responsive(), height: Dimens.d28.responsive(),
                    ),
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        RichText(
                          text: TextSpan(
                            text: '${c.customers.customerCode} - ',
                            style: TextStyle(color: Colors.orange, fontSize: Dimens.d10.responsive(), fontWeight: FontWeight.bold),
                            children: [
                              TextSpan(
                                text: c.customers.customerName,
                                style: AppTextStyles.s10w500Tertiary(),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: Dimens.d2.responsive()),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Icon(
                          Icons.cake_outlined,
                          color: Colors.blueGrey,
                          size: Dimens.d8.responsive(),
                        ),
                        SizedBox(width: Dimens.d4.responsive()),
                        Text(
                          c.customers.dateOfBirth,
                          style: AppTextStyles.s10w300Tertiary(),
                        ),
                      ],
                    ),
                    SizedBox(height: Dimens.d2.responsive()),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Icon(
                          Icons.local_phone_outlined,
                          color: Colors.blueGrey,
                          size: Dimens.d8.responsive(),
                        ),
                        SizedBox(width: Dimens.d4.responsive()),
                        Text(
                          c.customers.phone,
                          style: AppTextStyles.s10w300Tertiary(),
                        ),
                      ],
                    ),
                  ],
                ),
                Container(
                  padding: EdgeInsets.all(Dimens.d2.responsive()),
                  color: Colors.orange.shade50,
                  child: Text(
                      c.customers.segment,
                      style: TextStyle(color: Colors.orange, fontSize: Dimens.d8.responsive(), fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
          );
        }).toList());
  }

  @override
  Future<void> handleRefresh() async {
    await Future.delayed(const Duration(seconds: 1));
    notifyListeners();
  }

  @override
  Future<void> handleLoadMoreRows() async {
    await Future.delayed(const Duration(seconds: 2));
    _addMoreRows(employeeData, 15);
    buildDataGridRows();
    notifyListeners();
  }

  void buildDataGridRows() {
    _employeeData = employeeData.map<DataGridRow>((e) {
      return DataGridRow(
          cells: columns.map<DataGridCell>((column) {
            dynamic cellValue;
            switch (column.columnName) {
              case '1':
                cellValue = e.customers;
                break;
              default:
                cellValue = null; // Handle any unexpected column names
            }
            return DataGridCell(
              columnName: column.columnName,
              value: cellValue,
            );
          }).toList());
    }).toList();
  }

  void _addMoreRows(List<Employee> employeeData, int count) {
    final startIndex = employeeData.isNotEmpty ? employeeData.length : 0, endIndex = startIndex + count;
    for (int i = startIndex; i < endIndex; i++) {
      _employeeData.add(Employee(
        employeeData[i].groupId + i.toString(),
        employeeData[i].groupName + i.toString(),
        employeeData[i].customers,
      ) as DataGridRow);
    }
  }

  void refreshDataGrid() {
    notifyListeners();
  }

  void updateDataGridSource() {
    notifyListeners();
  }

  @override
  Widget? buildGroupCaptionCellWidget(RowColumnIndex rowColumnIndex, String summaryValue) {
    return Container(
      padding: EdgeInsets.all(Dimens.d14.responsive()),
      color: Colors.white,
      child: Text(summaryValue));
  }

  @override
  String performGrouping(String columnName, DataGridRow row) {
    if (columnName == '1') {
      Employee e = (row
          .getCells()
          .firstWhere((DataGridCell cell) => cell.columnName == columnName)!
          .value) as Employee;
      final String date = e.groupName;
      if (date == 'Today') {
        return 'Today';
      } else if (date == 'Tomorrow') {
        return 'Tomorrow';
      } else if (date == 'This Week') {
        return 'This Week';
      } else {
        return date;
      }
    }
    return super.performGrouping(columnName, row);
  }
}

