import 'package:base_architecture_flutter/src/app.dart';
import 'package:flutter/material.dart';
import 'package:resources/resources.dart';

class ToolsDialog extends StatelessWidget {
  ToolsDialog({
    super.key,
    this.alignment,
    this.padding,
    this.color,
    this.decoration,
    this.foregroundDecoration,
    double? width,
    double? height,
    BoxConstraints? constraints,
    this.margin,
    this.transform,
    this.transformAlignment,
    this.child,
    this.clipBehavior = Clip.none,
    this.onPressedOpenCalculateSavingDialog,
    this.onPressedOpenExchangeRateDialog,
    this.onPressedOpenInterbankDialog,
  }) : assert(margin == null || margin.isNonNegative),
        assert(padding == null || padding.isNonNegative),
        assert(decoration == null || decoration.debugAssertIsValid()),
        assert(constraints == null || constraints.debugAssertIsValid()),
        assert(decoration != null || clipBehavior == Clip.none),
        assert(color == null || decoration == null),
        constraints = (width != null || height != null)
            ? constraints?.tighten(width: width, height: height)
            ?? BoxConstraints.tightFor(width: width, height: height)
            : constraints;

  final Widget? child;
  final AlignmentGeometry? alignment;
  final EdgeInsetsGeometry? padding;
  final Color? color;
  final Decoration? decoration;
  final Decoration? foregroundDecoration;
  final BoxConstraints? constraints;
  final EdgeInsetsGeometry? margin;
  final Matrix4? transform;
  final AlignmentGeometry? transformAlignment;
  final Clip clipBehavior;
  final GestureTapCallback? onPressedOpenCalculateSavingDialog;
  final GestureTapCallback? onPressedOpenExchangeRateDialog;
  final GestureTapCallback? onPressedOpenInterbankDialog;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(Dimens.d12.responsive()),
      margin: EdgeInsets.fromLTRB(0.0, 0.0, Dimens.d780.responsive(), 0.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(0),
        color: Colors.white,
      ),
      child: SingleChildScrollView(
        child: Column(
          children: [
            Align(
              alignment: Alignment.topLeft,
              child: Text(
                S.current.toolsAndUtils,
                style: AppTextStyles.s14w500Tertiary(),
              ),
            ),
            SizedBox(height: Dimens.d24.responsive()),
            Align(
              alignment: Alignment.topLeft,
              child: Material(
                color: Colors.transparent,
                child: InkWell(
                  borderRadius: BorderRadius.circular(20),
                  onTap: onPressedOpenCalculateSavingDialog,
                  child: Row(
                    children: [
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Container(
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: Assets.images.icToolSavemoney.provider(),
                              fit: BoxFit.fitHeight,
                            ),
                          ),
                          child: SizedBox(
                            width: Dimens.d28.responsive(), height: Dimens.d28.responsive(),
                          ),
                        ),
                      ),
                      SizedBox(width: Dimens.d4.responsive()),
                      Text(
                        S.current.calculateSaving,
                        style: AppTextStyles.s14w400Tertiary(),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            SizedBox(height: Dimens.d24.responsive()),
            Align(
              alignment: Alignment.topLeft,
              child: Text(
                S.current.bank,
                style: AppTextStyles.s14w500Tertiary(),
              ),
            ),
            SizedBox(height: Dimens.d24.responsive()),
            Align(
              alignment: Alignment.topLeft,
              child: Material(
                color: Colors.transparent,
                child: InkWell(
                  borderRadius: BorderRadius.circular(20),
                  onTap: (){

                  },
                  child: Row(
                    children: [
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Container(
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: Assets.images.icToolInterest.provider(),
                              fit: BoxFit.fitHeight,
                            ),
                          ),
                          child: SizedBox(
                            width: Dimens.d28.responsive(), height: Dimens.d28.responsive(),
                          ),
                        ),
                      ),
                      SizedBox(width: Dimens.d4.responsive()),
                      Text(
                        S.current.interestRate,
                        style: AppTextStyles.s14w400Tertiary(),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            SizedBox(height: Dimens.d24.responsive()),
            Align(
              alignment: Alignment.topLeft,
              child: Material(
                color: Colors.transparent,
                child: InkWell(
                  borderRadius: BorderRadius.circular(20),
                  onTap: onPressedOpenExchangeRateDialog,
                  child: Row(
                    children: [
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Container(
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: Assets.images.icToolListedrate.provider(),
                              fit: BoxFit.fitHeight,
                            ),
                          ),
                          child: SizedBox(
                            width: Dimens.d28.responsive(), height: Dimens.d28.responsive(),
                          ),
                        ),
                      ),
                      SizedBox(width: Dimens.d4.responsive()),
                      Text(
                        S.current.exchangeRate,
                        style: AppTextStyles.s14w400Tertiary(),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            SizedBox(height: Dimens.d24.responsive()),
            Align(
              alignment: Alignment.topLeft,
              child: Material(
                color: Colors.transparent,
                child: InkWell(
                  borderRadius: BorderRadius.circular(20),
                  onTap: onPressedOpenInterbankDialog,
                  child: Row(
                    children: [
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Container(
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: Assets.images.icToolInterbankrate.provider(),
                              fit: BoxFit.fitHeight,
                            ),
                          ),
                          child: SizedBox(
                            width: Dimens.d28.responsive(), height: Dimens.d28.responsive(),
                          ),
                        ),
                      ),
                      SizedBox(width: Dimens.d4.responsive()),
                      Text(
                        S.current.interbankRate,
                        style: AppTextStyles.s14w400Tertiary(),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            SizedBox(height: Dimens.d24.responsive()),
          ],
        ),
      ),
    );
  }
}