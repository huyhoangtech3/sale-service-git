import 'package:flutter/material.dart';
import 'package:resources/resources.dart';
import 'package:base_architecture_flutter/src/app.dart';

class TextLeftIcon extends StatelessWidget {
  const TextLeftIcon({
    required this.icon,
    required this.text,
    this.color = Colors.transparent,
    super.key,
  });

  final IconData icon;
  final Color color;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Align(
          alignment: Alignment.centerLeft,
          child: Icon(
            icon,
            color: color,
            size: Dimens.d14.responsive(),
          ),
        ),
        SizedBox(width: Dimens.d4.responsive()),
        Text(
          text,
          style: AppTextStyles.s12w400Tertiary(),
        ),
      ],
    );
  }
}