import 'package:flutter/material.dart';
import 'package:base_architecture_flutter/src/app.dart';

class TextIconRightBorder extends StatelessWidget {
  const TextIconRightBorder({
    required this.child,
    this.splashColor = Colors.grey,
    this.onTap,
    super.key,
  });

  final Color? splashColor;
  final TextRightIcon child;
  final GestureTapCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return Material(
      child: InkWell(
        borderRadius: BorderRadius.all(Radius.circular(Dimens.d6.responsive())),
        splashColor: Colors.orangeAccent,
        onTap: onTap,
        child: Container(
          padding: EdgeInsets.all(Dimens.d10.responsive()),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.grey),
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.all(Radius.circular(Dimens.d6.responsive())),
          ),
          child: child,
        ),
      ),
    );
  }
}