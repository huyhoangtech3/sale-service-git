import 'package:flutter/material.dart';

import 'package:base_architecture_flutter/src/app.dart';

class IconButtonCustom extends StatelessWidget {
  const IconButtonCustom({
    this.isSelected,
    required this.icon,
    this.selectedIcon,
    required this.onPressed,
    this.color = Colors.orangeAccent,
    this.highlightColor = Colors.orange,
    super.key,
  });

  final bool? isSelected;
  final Widget icon;
  final Widget? selectedIcon;
  final VoidCallback? onPressed;
  final Color color;
  final Color highlightColor;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: Dimens.d30.responsive(),
      height: Dimens.d30.responsive(),
      child: Material(
        borderRadius: BorderRadius.circular(6.0),
        color: color,
        type: MaterialType.button,
        child: IconButton(
          iconSize: Dimens.d16.responsive(),
          color: Colors.white,
          highlightColor: highlightColor,
          isSelected: isSelected,
          icon: icon,
          selectedIcon: selectedIcon,
          onPressed: onPressed,
        ),
      ),
    );
  }
}
