import 'package:flutter/material.dart';

import 'package:base_architecture_flutter/src/app.dart';
import 'package:flutter/services.dart';

class AppTextFormField extends StatelessWidget {
  const AppTextFormField({
    required this.label,
    this.hintText = '',
    this.suffixIcon,
    this.controller,
    this.onChanged,
    this.onTap,
    this.keyboardType = TextInputType.text,
    this.svgGenImage,
    this.textInputAction,
    this.onFieldSubmitted,
    this.focusNode,
    this.textCapitalization = TextCapitalization.none,
    this.autofocus = false,
    this.hintStyle = const TextStyle(color: Colors.grey),
    this.style = const TextStyle(color: Colors.white),
    this.onTapOutside,
    this.labelStyle = const TextStyle(color: Colors.orangeAccent),
    this.contentPadding,
    this.enabledBorder = const OutlineInputBorder(
      borderSide: BorderSide(color: Colors.grey),
      borderRadius: BorderRadius.all(Radius.circular(8.0)),
    ),
    this.focusedBorder = const OutlineInputBorder(
      borderSide: BorderSide(color: Color.fromARGB(255, 201, 139, 17), width: 1),
      borderRadius: BorderRadius.all(Radius.circular(8.0)),
    ),
    this.errorBorder = const OutlineInputBorder(
      borderSide: BorderSide(color: Colors.red, width: 1),
      borderRadius: BorderRadius.all(Radius.circular(8.0)),
    ),
    this.inputFormatters,
    this.onEditingComplete,
    this.maxLines,
    super.key,
  });

  final String label;
  final String hintText;
  final SvgGenImage? svgGenImage;
  final void Function(String)? onChanged;
  final VoidCallback? onTap;
  final TextInputType keyboardType;
  final TextEditingController? controller;
  final IconButton? suffixIcon;
  final TextInputAction? textInputAction;
  final ValueChanged<String>? onFieldSubmitted;
  final FocusNode? focusNode;
  final TextCapitalization textCapitalization;
  final bool autofocus;
  final TextStyle hintStyle;
  final TextStyle style;
  final TapRegionCallback? onTapOutside;
  final OutlineInputBorder enabledBorder;
  final OutlineInputBorder focusedBorder;
  final OutlineInputBorder errorBorder;
  final TextStyle labelStyle;
  final List<TextInputFormatter>? inputFormatters;
  final EdgeInsetsGeometry? contentPadding;
  final VoidCallback? onEditingComplete;
  final int? maxLines;

  @override
  Widget build(BuildContext context) {
    return Column(
        children: [
          TextFormField(
            onTapOutside: onTapOutside,
            onTap: onTap,
            onChanged: onChanged,
            controller: controller,
            decoration: InputDecoration(
              label: Text(label),
              labelStyle: labelStyle,
              hintText: hintText,
              hintStyle: hintStyle,
              suffixIcon: suffixIcon,
              enabledBorder: enabledBorder,
              focusedBorder: focusedBorder,
              errorBorder: errorBorder,
              contentPadding: contentPadding,
              alignLabelWithHint: true,
            ),
            keyboardType: keyboardType,
            maxLines: maxLines,
            style: style,
            textInputAction: textInputAction,
            onFieldSubmitted: onFieldSubmitted,
            focusNode: focusNode,
            textCapitalization: textCapitalization,
            autofocus: autofocus,
            inputFormatters: inputFormatters,
            onEditingComplete: onEditingComplete,
          ),
        ]);
  }
}
