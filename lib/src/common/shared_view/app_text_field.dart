import 'package:flutter/material.dart';

import 'package:base_architecture_flutter/src/app.dart';

class AppTextField extends StatelessWidget {
  const AppTextField({
    required this.title,
    this.hintText = '',
    this.suffixIcon,
    this.controller,
    this.onChanged,
    this.onTap,
    this.keyboardType = TextInputType.text,
    this.svgGenImage,
    this.textInputAction,
    this.onSubmitted,
    this.focusNode,
    this.textCapitalization = TextCapitalization.none,
    this.autofocus = false,
    this.hintStyle = const TextStyle(color: Colors.grey),
    this.style = const TextStyle(color: Colors.white),
    this.headStyle = const TextStyle(color: Colors.orangeAccent),
    this.onTapOutside,
    super.key,
  });

  final String title;
  final String hintText;
  final SvgGenImage? svgGenImage;
  final void Function(String)? onChanged;
  final VoidCallback? onTap;
  final TextInputType keyboardType;
  final TextEditingController? controller;
  final IconButton? suffixIcon;
  final TextInputAction? textInputAction;
  final ValueChanged<String>? onSubmitted;
  final FocusNode? focusNode;
  final TextCapitalization textCapitalization;
  final bool autofocus;
  final TextStyle hintStyle;
  final TextStyle style;
  final TextStyle headStyle;
  final TapRegionCallback? onTapOutside;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Align(
          alignment: Alignment.centerLeft,
          child: Text(
            title,
            style: headStyle,
          ),
        ),
        SizedBox(height: Dimens.d0.responsive()),
        TextField(
          onTapOutside: onTapOutside,
          onTap: onTap,
          onChanged: onChanged,
          controller: controller,
          decoration: InputDecoration(
            hintText: hintText,
            hintStyle: hintStyle,
            suffixIcon: suffixIcon,
          ),
          keyboardType: keyboardType,
          style: style,
          textInputAction: textInputAction,
          onSubmitted: onSubmitted,
          focusNode: focusNode,
          textCapitalization: textCapitalization,
          autofocus: autofocus,
        ),
      ]);
  }
}
