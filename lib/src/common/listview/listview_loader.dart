import 'package:flutter/material.dart';
import 'package:shared/shared.dart';
import 'package:base_architecture_flutter/src/app.dart';

/// Because [PagedListView] does not expose the [itemCount] property, itemCount = 0 on the first load and thus the Shimmer loading effect can not work. We need to create a fake ListView for the first load.
class ListViewLoader extends StatelessWidget {
  const ListViewLoader({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: UiConstants.shimmerItemCount,
      itemBuilder: (context, index) => Padding(
        padding: EdgeInsets.symmetric(
          horizontal: Dimens.d8.responsive(),
          vertical: Dimens.d4.responsive(),
        ),
        child: const ShimmerLoading(
          loadingWidget: LoadingItem(),
          isLoading: true,
          child: LoadingItem(),
        ),
      ),
    );
  }
}