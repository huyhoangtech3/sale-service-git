import 'package:flutter/material.dart';
import 'package:base_architecture_flutter/src/app.dart';
import 'package:resources/resources.dart';

class CommonDropdownMenu extends StatelessWidget {
  const CommonDropdownMenu({
    required this.list,
    this.controller,
    this.onSelected,
    this.index = 0,
    required this.width,
    super.key,
  });

  final List<String> list;
  final TextEditingController? controller;
  final ValueChanged? onSelected;
  final int index;
  final double width;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return DropdownMenu<String>(
        controller: controller,
        width: width,
        textStyle: AppTextStyles.s12w400Tertiary(),
        inputDecorationTheme: InputDecorationTheme(
          isDense: true,
          contentPadding: EdgeInsets.symmetric(horizontal: Dimens.d8.responsive()),
          constraints: BoxConstraints.tight(Size.fromHeight(Dimens.d34.responsive())),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8),
          ),
        ),
        initialSelection: list.elementAt(index),
        onSelected: onSelected,
        dropdownMenuEntries: list.map<DropdownMenuEntry<String>>((String value) {
          return DropdownMenuEntry<String>(
            value: value,
            label: value,
            labelWidget: Text(
              value,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: AppTextStyles.s12w400Tertiary(),
            ),
            style: ButtonStyle(
              textStyle: MaterialStateTextStyle.resolveWith(
                    (states) => AppTextStyles.s12w400Tertiary(),
              ),
            ),
          );
        }).toList(),
      );
    });
  }
}