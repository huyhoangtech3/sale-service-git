import 'package:base_architecture_flutter/resource/generated/assets.gen.dart';
import 'package:base_architecture_flutter/src/resource/dimens/dimens.dart';
import 'package:domain/domain.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:resources/resources.dart';
import 'package:shared/shared.dart';
import 'package:badges/badges.dart' as badges;
import 'package:base_architecture_flutter/src/app.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  CustomAppBar({
    super.key,
    this.text,
    this.onLeadingPressed,
    this.onTitlePressed,
    this.leadingIcon,
    this.titleType = AppBarTitle.text,
    this.centerTitle = false,
    this.elevation,
    this.actions,
    this.height,
    this.automaticallyImplyLeading = true,
    this.flexibleSpace,
    this.bottom,
    this.shadowColor,
    this.shape,
    this.backgroundColor,
    this.foregroundColor,
    this.iconTheme,
    this.actionsIconTheme,
    this.primary = true,
    this.excludeHeaderSemantics = false,
    this.titleSpacing,
    this.toolbarOpacity = 1.0,
    this.bottomOpacity = 1.0,
    this.leadingWidth,
    this.titleTextStyle,
    this.systemOverlayStyle,
    this.leadingIconColor,
    this.scrolledUnderElevation = 0,
    this.tabUserInfo,
    this.tabToolsAndUtilities,
    this.tabBirthDay,
    this.tabNotification,
  }) : preferredSize = Size.fromHeight(
    height ?? Dimens.d56.responsive(),
  );

  final String? text;
  final VoidCallback? onLeadingPressed;
  final VoidCallback? onTitlePressed;
  final LeadingIcon? leadingIcon;
  final AppBarTitle titleType;
  final bool centerTitle;
  final double? elevation;
  final List<Widget>? actions;
  final double? height;
  final bool automaticallyImplyLeading;
  final Widget? flexibleSpace;
  final PreferredSizeWidget? bottom;
  final Color? shadowColor;
  final ShapeBorder? shape;
  final Color? backgroundColor;
  final Color? foregroundColor;
  final IconThemeData? iconTheme;
  final IconThemeData? actionsIconTheme;
  final bool primary;
  final bool excludeHeaderSemantics;
  final double? titleSpacing;
  final double toolbarOpacity;
  final double bottomOpacity;
  final double? leadingWidth;
  final TextStyle? titleTextStyle;
  final Color? leadingIconColor;
  final SystemUiOverlayStyle? systemOverlayStyle;
  final double scrolledUnderElevation;
  final GestureTapCallback? tabUserInfo;
  final GestureTapCallback? tabToolsAndUtilities;
  final GestureTapCallback? tabBirthDay;
  final GestureTapCallback? tabNotification;

  @override
  final Size preferredSize;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      toolbarHeight: preferredSize.height,
      automaticallyImplyLeading: automaticallyImplyLeading,
      flexibleSpace: flexibleSpace,
      bottom: const PreferredSize(
        preferredSize: Size.fromHeight(1),
        child: Divider(height: 1),
      ),
      shadowColor: shadowColor,
      shape: shape,
      backgroundColor: backgroundColor,
      foregroundColor: foregroundColor,
      iconTheme: iconTheme,
      actionsIconTheme: actionsIconTheme,
      primary: primary,
      excludeHeaderSemantics: excludeHeaderSemantics,
      titleSpacing: titleSpacing,
      toolbarOpacity: toolbarOpacity,
      bottomOpacity: bottomOpacity,
      leadingWidth: 80,
      systemOverlayStyle: systemOverlayStyle,
      leading: Align(
        alignment: Alignment.center,
        child: Assets.images.appLogo.svg(
            width: Dimens.d32.responsive(),
            height: Dimens.d32.responsive()),
      ),
      centerTitle: centerTitle,
      title: Row(
        children: [
          Expanded(
            flex: 1,
            child: Row(
              children: [
                Align(
                  alignment: Alignment.centerLeft,
                  child: InkWell(
                    onTap: tabUserInfo,
                    child: Row(
                      children: [
                        BlocBuilder<MainBloc, MainState>(
                          buildWhen: (previous, current) => previous.id != current.id,
                          builder: (context, state) {
                            return Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                    image: state.gender == Constants.man ? Assets.images.avaMan.provider() : Assets.images.avaWoman.provider(),
                                    fit: BoxFit.fitHeight,
                                  ),
                                ),
                                child: SizedBox(
                                  width: Dimens.d28.responsive(), height: Dimens.d28.responsive(),
                                ),
                              ),
                            );
                          },
                        ),
                        SizedBox(width: Dimens.d16.responsive()),
                        BlocBuilder<MainBloc, MainState>(
                          buildWhen: (previous, current) => previous.id != current.id,
                          builder: (context, state) {
                            return Align(
                              alignment: Alignment.centerLeft,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(state.fullName, style: AppTextStyles.s12w500Tertiary()),
                                  Text(state.userName, style: AppTextStyles.s10w400Tertiary()),
                                ],
                              ),
                            );
                          },
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(width: Dimens.d14.responsive()),
                Align(
                  alignment: Alignment.centerLeft,
                  child: InkWell(
                    onTap: tabBirthDay,
                    child: badges.Badge(
                      badgeContent: Text(
                        '9+',
                        style: AppTextStyles.s8w500Primary(),
                      ),
                      badgeStyle: const badges.BadgeStyle(
                        badgeColor: Colors.orangeAccent,
                      ),
                      child: Container(
                        margin: EdgeInsets.all(Dimens.d2.responsive()),
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: Assets.images.icBirthday.provider(),
                            fit: BoxFit.fitHeight,
                          ),
                        ),
                        child: SizedBox(
                          width: Dimens.d24.responsive(), height: Dimens.d24.responsive(),
                        ),
                      ),
                    )
                  ),
                ),
                SizedBox(width: Dimens.d14.responsive()),
                Align(
                  alignment: Alignment.centerLeft,
                  child: InkWell(
                    onTap: tabNotification,
                    child: badges.Badge(
                      badgeContent: Text(
                        '9+',
                        style: AppTextStyles.s8w500Primary(),
                      ),
                      badgeStyle: const badges.BadgeStyle(
                        badgeColor: Colors.orangeAccent,
                      ),
                      child: Container(
                        margin: EdgeInsets.all(Dimens.d2.responsive()),
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: Assets.images.icBell.provider(),
                            fit: BoxFit.fitHeight,
                            colorFilter: const ColorFilter.mode(Colors.orangeAccent, BlendMode.srcIn),
                          ),
                        ),
                        child: SizedBox(
                          width: Dimens.d24.responsive(), height: Dimens.d24.responsive(),
                        ),
                      ),
                    )
                  ),
                ),
                SizedBox(width: Dimens.d14.responsive()),
                Align(
                  alignment: Alignment.centerLeft,
                  child: InkWell(
                    onTap:tabToolsAndUtilities,
                    child: Container(
                      margin: EdgeInsets.all(Dimens.d2.responsive()),
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: Assets.images.icToolOpen.provider(),
                          fit: BoxFit.fitHeight,
                        ),
                      ),
                      child: SizedBox(
                        width: Dimens.d24.responsive(), height: Dimens.d24.responsive(),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            flex: 2,
            child: Column(
              children: [
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    height: preferredSize.height,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: Assets.images.appBanner.provider(),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
      actions: actions,
      elevation: elevation,
      scrolledUnderElevation: scrolledUnderElevation,
    );
  }

  Widget _buildIcon(SvgGenImage svg) {
    return svg.svg(
      colorFilter: leadingIconColor?.let((it) => ColorFilter.mode(it, BlendMode.srcIn)),
      width: Dimens.d24.responsive(),
      height: Dimens.d24.responsive(),
    );
  }
}
