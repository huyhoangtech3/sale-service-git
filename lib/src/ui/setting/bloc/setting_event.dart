import 'package:freezed_annotation/freezed_annotation.dart';

import 'package:base_architecture_flutter/src/base/bloc/base_bloc_event.dart';

part 'setting_event.freezed.dart';

abstract class SettingEvent extends BaseBlocEvent {
  const SettingEvent();
}

@freezed
class SettingPageInitiated extends SettingEvent with _$SettingPageInitiated {
  const factory SettingPageInitiated({
    required int id,
  }) = _SettingPageInitiated;
}

@freezed
class LogoutButtonPressed extends SettingEvent with _$LogoutButtonPressed {
  const factory LogoutButtonPressed() = _LogoutButtonPressed;
}
