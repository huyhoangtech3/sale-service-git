// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'setting_event.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$SettingPageInitiated {
  int get id => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $SettingPageInitiatedCopyWith<SettingPageInitiated> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SettingPageInitiatedCopyWith<$Res> {
  factory $SettingPageInitiatedCopyWith(SettingPageInitiated value,
          $Res Function(SettingPageInitiated) then) =
      _$SettingPageInitiatedCopyWithImpl<$Res, SettingPageInitiated>;
  @useResult
  $Res call({int id});
}

/// @nodoc
class _$SettingPageInitiatedCopyWithImpl<$Res,
        $Val extends SettingPageInitiated>
    implements $SettingPageInitiatedCopyWith<$Res> {
  _$SettingPageInitiatedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$SettingPageInitiatedImplCopyWith<$Res>
    implements $SettingPageInitiatedCopyWith<$Res> {
  factory _$$SettingPageInitiatedImplCopyWith(_$SettingPageInitiatedImpl value,
          $Res Function(_$SettingPageInitiatedImpl) then) =
      __$$SettingPageInitiatedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int id});
}

/// @nodoc
class __$$SettingPageInitiatedImplCopyWithImpl<$Res>
    extends _$SettingPageInitiatedCopyWithImpl<$Res, _$SettingPageInitiatedImpl>
    implements _$$SettingPageInitiatedImplCopyWith<$Res> {
  __$$SettingPageInitiatedImplCopyWithImpl(_$SettingPageInitiatedImpl _value,
      $Res Function(_$SettingPageInitiatedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
  }) {
    return _then(_$SettingPageInitiatedImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$SettingPageInitiatedImpl implements _SettingPageInitiated {
  const _$SettingPageInitiatedImpl({required this.id});

  @override
  final int id;

  @override
  String toString() {
    return 'SettingPageInitiated(id: $id)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SettingPageInitiatedImpl &&
            (identical(other.id, id) || other.id == id));
  }

  @override
  int get hashCode => Object.hash(runtimeType, id);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$SettingPageInitiatedImplCopyWith<_$SettingPageInitiatedImpl>
      get copyWith =>
          __$$SettingPageInitiatedImplCopyWithImpl<_$SettingPageInitiatedImpl>(
              this, _$identity);
}

abstract class _SettingPageInitiated implements SettingPageInitiated {
  const factory _SettingPageInitiated({required final int id}) =
      _$SettingPageInitiatedImpl;

  @override
  int get id;
  @override
  @JsonKey(ignore: true)
  _$$SettingPageInitiatedImplCopyWith<_$SettingPageInitiatedImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$LogoutButtonPressed {}

/// @nodoc
abstract class $LogoutButtonPressedCopyWith<$Res> {
  factory $LogoutButtonPressedCopyWith(
          LogoutButtonPressed value, $Res Function(LogoutButtonPressed) then) =
      _$LogoutButtonPressedCopyWithImpl<$Res, LogoutButtonPressed>;
}

/// @nodoc
class _$LogoutButtonPressedCopyWithImpl<$Res, $Val extends LogoutButtonPressed>
    implements $LogoutButtonPressedCopyWith<$Res> {
  _$LogoutButtonPressedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$LogoutButtonPressedImplCopyWith<$Res> {
  factory _$$LogoutButtonPressedImplCopyWith(_$LogoutButtonPressedImpl value,
          $Res Function(_$LogoutButtonPressedImpl) then) =
      __$$LogoutButtonPressedImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$LogoutButtonPressedImplCopyWithImpl<$Res>
    extends _$LogoutButtonPressedCopyWithImpl<$Res, _$LogoutButtonPressedImpl>
    implements _$$LogoutButtonPressedImplCopyWith<$Res> {
  __$$LogoutButtonPressedImplCopyWithImpl(_$LogoutButtonPressedImpl _value,
      $Res Function(_$LogoutButtonPressedImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$LogoutButtonPressedImpl implements _LogoutButtonPressed {
  const _$LogoutButtonPressedImpl();

  @override
  String toString() {
    return 'LogoutButtonPressed()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LogoutButtonPressedImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;
}

abstract class _LogoutButtonPressed implements LogoutButtonPressed {
  const factory _LogoutButtonPressed() = _$LogoutButtonPressedImpl;
}
