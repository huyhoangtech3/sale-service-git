import 'package:freezed_annotation/freezed_annotation.dart';

import 'package:base_architecture_flutter/src/base/bloc/base_bloc_state.dart';

part 'rm_state.freezed.dart';

@freezed
class RMState extends BaseBlocState with _$RMState {
  const factory RMState({
    @Default('') String id,
  }) = _RMState;
}
