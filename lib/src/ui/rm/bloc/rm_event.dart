import 'package:freezed_annotation/freezed_annotation.dart';

import 'package:base_architecture_flutter/src/base/bloc/base_bloc_event.dart';

part 'rm_event.freezed.dart';

abstract class RMEvent extends BaseBlocEvent {
  const RMEvent();
}

@freezed
class RMPageInitiated extends RMEvent with _$RMPageInitiated {
  const factory RMPageInitiated({
    required String id,
  }) = _RMPageInitiated;
}
