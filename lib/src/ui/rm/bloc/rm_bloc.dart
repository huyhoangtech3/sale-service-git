import 'dart:async';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:base_architecture_flutter/src/app.dart';

@Injectable()
class RMBloc extends BaseBloc<RMEvent, RMState> {
  RMBloc() : super(const RMState()) {
    on<RMPageInitiated>(
      _onRMPageInitiated,
      transformer: log(),
    );
  }

  FutureOr<void> _onRMPageInitiated(RMPageInitiated event, Emitter<RMState> emit) {}
}
