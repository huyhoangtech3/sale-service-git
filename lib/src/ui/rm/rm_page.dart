import 'package:auto_route/auto_route.dart';
import 'package:domain/domain.dart';
import 'package:flutter/material.dart';
import 'package:resources/resources.dart';
import 'package:base_architecture_flutter/src/app.dart';

@RoutePage()
class RMPage extends StatefulWidget {
  const RMPage({super.key});

  @override
  State<StatefulWidget> createState() {
    return _RMPageState();
  }
}

class _RMPageState extends BasePageState<RMPage, RMBloc> {
  @override
  Widget buildPage(BuildContext context) {
    return CommonScaffold(
      body: Center(
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(AppColors.current.primaryColor),
          ),
          onPressed: () {
            navigator.push(const AppRouteInfo.login());
          },
          child: Text(
            S.current.rm360,
            style: AppTextStyles.s14w400Primary(),
          ),
        ),
      ),
    );
  }
}
