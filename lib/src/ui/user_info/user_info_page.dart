import 'package:auto_route/auto_route.dart';
import 'package:domain/domain.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:resources/resources.dart';

import 'package:base_architecture_flutter/src/app.dart';
import 'package:shared/shared.dart';

@RoutePage()
class UserInfoPage extends StatefulWidget {
  const UserInfoPage({super.key});

  @override
  State<StatefulWidget> createState() {
    return _UserInfoPageState();
  }
}

class _UserInfoPageState extends BasePageState<UserInfoPage, UserInfoBloc> {
  @override
  void initState() {
    super.initState();
    bloc.add(const UserInfoPageInitiated(id: 0));
  }

  @override
  Widget buildPage(BuildContext context) {
    return CommonScaffold(
      body: SafeArea(
        child: Container(
          color: Colors.grey[50],
          padding: EdgeInsets.all(Dimens.d14.responsive()),
          child: Column(
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  S.current.userInfo,
                  style: AppTextStyles.s16w700Tertiary(),
                ),
              ),
              SizedBox(height: Dimens.d24.responsive()),
              Align(
                alignment: Alignment.centerLeft,
                child: Container(
                  padding: EdgeInsets.all(Dimens.d8.responsive()),
                  color: Colors.white,
                  child: Column(
                    children: [
                      Row(
                        children: [
                          BlocBuilder<UserInfoBloc, UserInfoState>(
                            buildWhen: (previous, current) => previous.hrsCode != current.hrsCode,
                            builder: (context, state) {
                              return Align(
                                alignment: Alignment.centerLeft,
                                child: Container(
                                  width: Dimens.d100.responsive(),
                                  height: Dimens.d100.responsive(),
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                      image: state.gender == Constants.man ? Assets.images.avaMan.provider() : Assets.images.avaWoman.provider(),
                                      fit: BoxFit.fitHeight,
                                    ),
                                  ),
                                  child: SizedBox(
                                    width: Dimens.d28.responsive(), height: Dimens.d28.responsive(),
                                  ),
                                ),
                              );
                            },
                          ),
                          SizedBox(width: Dimens.d24.responsive()),
                          BlocBuilder<UserInfoBloc, UserInfoState>(
                            buildWhen: (previous, current) => previous.hrsCode != current.hrsCode,
                            builder: (context, state) {
                              return Align(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      state.fullName,
                                      style: AppTextStyles.s20w700Secondary(),
                                    ),
                                    Text(
                                      S.current.empCode + Constants.colon + Constants.space + state.hrsCode,
                                      style: AppTextStyles.s14w400Tertiary(),
                                    ),
                                    Text(
                                      S.current.rmCode + Constants.colon + Constants.space + state.code,
                                      style: AppTextStyles.s14w400Tertiary(),
                                    ),
                                    Text(
                                      S.current.branchCode + Constants.colon + Constants.space + state.branch,
                                      style: AppTextStyles.s14w400Tertiary(),
                                    ),
                                    Text(
                                      S.current.branchName + Constants.colon + Constants.space + state.branchName,
                                      style: AppTextStyles.s14w400Tertiary(),
                                    ),
                                  ],
                                ),
                              );
                            },
                          ),
                        ],
                      ),
                      SizedBox(height: Dimens.d24.responsive()),
                      Align(
                        alignment: Alignment.bottomLeft,
                        child: Column(
                          children: [
                            ElevatedButton(
                              onPressed: () => bloc.add(const LogoutPressed()),
                              style: ButtonStyle(
                                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(8.0),
                                    )
                                ),
                                backgroundColor: MaterialStateProperty.all(AppColors.current.primaryColor),
                              ),
                              child: Text(
                                S.current.logout,
                                style: AppTextStyles.s14w400Primary(),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
