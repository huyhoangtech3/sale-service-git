import 'package:freezed_annotation/freezed_annotation.dart';

import 'package:base_architecture_flutter/src/base/bloc/base_bloc_state.dart';

part 'user_info_state.freezed.dart';

@freezed
class UserInfoState extends BaseBlocState with _$UserInfoState {
  const UserInfoState._();
  const factory UserInfoState({
    @Default('') String gender,
    @Default('') String fullName,
    @Default('') String hrsCode,
    @Default('') String code,
    @Default('') String branch,
    @Default('') String branchName,
  }) = _UserInfoState;
}
