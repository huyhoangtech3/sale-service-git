import 'dart:async';

import 'package:data/data.dart';
import 'package:domain/domain.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:base_architecture_flutter/src/app.dart';

@Injectable()
class UserInfoBloc extends BaseBloc<UserInfoEvent, UserInfoState> {
  UserInfoBloc(this._logoutUseCase, this._userInfoPageUseCase) : super(const UserInfoState()) {
    on<UserInfoPageInitiated>(
      _onUserInfoPageInitiated,
      transformer: log(),
    );

    on<LogoutPressed>(
      _onLogoutPressed,
      transformer: log(),
    );
  }

  final UserInfoPageUseCase _userInfoPageUseCase;
  final LogoutUseCase _logoutUseCase;

  FutureOr<void> _onUserInfoPageInitiated(UserInfoPageInitiated event, Emitter<UserInfoState> emit) async {
    await runBlocCatching(
      action: () async {
        final output = _userInfoPageUseCase.execute(const UserInfoPageDataInput());
        emit(state.copyWith(
          gender: output.gender,
          fullName: output.fullName,
          hrsCode: output.hrsCode,
          code: output.code,
          branch: output.branch,
          branchName: output.branchName,
        ));
      },
    );
  }

  FutureOr<void> _onLogoutPressed(LogoutPressed event, Emitter<UserInfoState> emit) async {
    return runBlocCatching(
      action: () async {
        await _logoutUseCase.execute(const LogoutInput());
      },
    );
  }
}
