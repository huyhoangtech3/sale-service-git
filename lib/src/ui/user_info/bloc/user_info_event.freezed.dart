// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'user_info_event.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$UserInfoPageInitiated {
  int get id => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $UserInfoPageInitiatedCopyWith<UserInfoPageInitiated> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserInfoPageInitiatedCopyWith<$Res> {
  factory $UserInfoPageInitiatedCopyWith(UserInfoPageInitiated value,
          $Res Function(UserInfoPageInitiated) then) =
      _$UserInfoPageInitiatedCopyWithImpl<$Res, UserInfoPageInitiated>;
  @useResult
  $Res call({int id});
}

/// @nodoc
class _$UserInfoPageInitiatedCopyWithImpl<$Res,
        $Val extends UserInfoPageInitiated>
    implements $UserInfoPageInitiatedCopyWith<$Res> {
  _$UserInfoPageInitiatedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$UserInfoPageInitiatedImplCopyWith<$Res>
    implements $UserInfoPageInitiatedCopyWith<$Res> {
  factory _$$UserInfoPageInitiatedImplCopyWith(
          _$UserInfoPageInitiatedImpl value,
          $Res Function(_$UserInfoPageInitiatedImpl) then) =
      __$$UserInfoPageInitiatedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int id});
}

/// @nodoc
class __$$UserInfoPageInitiatedImplCopyWithImpl<$Res>
    extends _$UserInfoPageInitiatedCopyWithImpl<$Res,
        _$UserInfoPageInitiatedImpl>
    implements _$$UserInfoPageInitiatedImplCopyWith<$Res> {
  __$$UserInfoPageInitiatedImplCopyWithImpl(_$UserInfoPageInitiatedImpl _value,
      $Res Function(_$UserInfoPageInitiatedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
  }) {
    return _then(_$UserInfoPageInitiatedImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$UserInfoPageInitiatedImpl implements _UserInfoPageInitiated {
  const _$UserInfoPageInitiatedImpl({required this.id});

  @override
  final int id;

  @override
  String toString() {
    return 'UserInfoPageInitiated(id: $id)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UserInfoPageInitiatedImpl &&
            (identical(other.id, id) || other.id == id));
  }

  @override
  int get hashCode => Object.hash(runtimeType, id);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$UserInfoPageInitiatedImplCopyWith<_$UserInfoPageInitiatedImpl>
      get copyWith => __$$UserInfoPageInitiatedImplCopyWithImpl<
          _$UserInfoPageInitiatedImpl>(this, _$identity);
}

abstract class _UserInfoPageInitiated implements UserInfoPageInitiated {
  const factory _UserInfoPageInitiated({required final int id}) =
      _$UserInfoPageInitiatedImpl;

  @override
  int get id;
  @override
  @JsonKey(ignore: true)
  _$$UserInfoPageInitiatedImplCopyWith<_$UserInfoPageInitiatedImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$LogoutPressed {}

/// @nodoc
abstract class $LogoutPressedCopyWith<$Res> {
  factory $LogoutPressedCopyWith(
          LogoutPressed value, $Res Function(LogoutPressed) then) =
      _$LogoutPressedCopyWithImpl<$Res, LogoutPressed>;
}

/// @nodoc
class _$LogoutPressedCopyWithImpl<$Res, $Val extends LogoutPressed>
    implements $LogoutPressedCopyWith<$Res> {
  _$LogoutPressedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$LogoutPressedImplCopyWith<$Res> {
  factory _$$LogoutPressedImplCopyWith(
          _$LogoutPressedImpl value, $Res Function(_$LogoutPressedImpl) then) =
      __$$LogoutPressedImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$LogoutPressedImplCopyWithImpl<$Res>
    extends _$LogoutPressedCopyWithImpl<$Res, _$LogoutPressedImpl>
    implements _$$LogoutPressedImplCopyWith<$Res> {
  __$$LogoutPressedImplCopyWithImpl(
      _$LogoutPressedImpl _value, $Res Function(_$LogoutPressedImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$LogoutPressedImpl implements _LogoutPressed {
  const _$LogoutPressedImpl();

  @override
  String toString() {
    return 'LogoutPressed()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$LogoutPressedImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;
}

abstract class _LogoutPressed implements LogoutPressed {
  const factory _LogoutPressed() = _$LogoutPressedImpl;
}
