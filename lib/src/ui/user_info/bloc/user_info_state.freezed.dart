// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'user_info_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$UserInfoState {
  String get gender => throw _privateConstructorUsedError;
  String get fullName => throw _privateConstructorUsedError;
  String get hrsCode => throw _privateConstructorUsedError;
  String get code => throw _privateConstructorUsedError;
  String get branch => throw _privateConstructorUsedError;
  String get branchName => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $UserInfoStateCopyWith<UserInfoState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserInfoStateCopyWith<$Res> {
  factory $UserInfoStateCopyWith(
          UserInfoState value, $Res Function(UserInfoState) then) =
      _$UserInfoStateCopyWithImpl<$Res, UserInfoState>;
  @useResult
  $Res call(
      {String gender,
      String fullName,
      String hrsCode,
      String code,
      String branch,
      String branchName});
}

/// @nodoc
class _$UserInfoStateCopyWithImpl<$Res, $Val extends UserInfoState>
    implements $UserInfoStateCopyWith<$Res> {
  _$UserInfoStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? gender = null,
    Object? fullName = null,
    Object? hrsCode = null,
    Object? code = null,
    Object? branch = null,
    Object? branchName = null,
  }) {
    return _then(_value.copyWith(
      gender: null == gender
          ? _value.gender
          : gender // ignore: cast_nullable_to_non_nullable
              as String,
      fullName: null == fullName
          ? _value.fullName
          : fullName // ignore: cast_nullable_to_non_nullable
              as String,
      hrsCode: null == hrsCode
          ? _value.hrsCode
          : hrsCode // ignore: cast_nullable_to_non_nullable
              as String,
      code: null == code
          ? _value.code
          : code // ignore: cast_nullable_to_non_nullable
              as String,
      branch: null == branch
          ? _value.branch
          : branch // ignore: cast_nullable_to_non_nullable
              as String,
      branchName: null == branchName
          ? _value.branchName
          : branchName // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$UserInfoStateImplCopyWith<$Res>
    implements $UserInfoStateCopyWith<$Res> {
  factory _$$UserInfoStateImplCopyWith(
          _$UserInfoStateImpl value, $Res Function(_$UserInfoStateImpl) then) =
      __$$UserInfoStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String gender,
      String fullName,
      String hrsCode,
      String code,
      String branch,
      String branchName});
}

/// @nodoc
class __$$UserInfoStateImplCopyWithImpl<$Res>
    extends _$UserInfoStateCopyWithImpl<$Res, _$UserInfoStateImpl>
    implements _$$UserInfoStateImplCopyWith<$Res> {
  __$$UserInfoStateImplCopyWithImpl(
      _$UserInfoStateImpl _value, $Res Function(_$UserInfoStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? gender = null,
    Object? fullName = null,
    Object? hrsCode = null,
    Object? code = null,
    Object? branch = null,
    Object? branchName = null,
  }) {
    return _then(_$UserInfoStateImpl(
      gender: null == gender
          ? _value.gender
          : gender // ignore: cast_nullable_to_non_nullable
              as String,
      fullName: null == fullName
          ? _value.fullName
          : fullName // ignore: cast_nullable_to_non_nullable
              as String,
      hrsCode: null == hrsCode
          ? _value.hrsCode
          : hrsCode // ignore: cast_nullable_to_non_nullable
              as String,
      code: null == code
          ? _value.code
          : code // ignore: cast_nullable_to_non_nullable
              as String,
      branch: null == branch
          ? _value.branch
          : branch // ignore: cast_nullable_to_non_nullable
              as String,
      branchName: null == branchName
          ? _value.branchName
          : branchName // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$UserInfoStateImpl extends _UserInfoState {
  const _$UserInfoStateImpl(
      {this.gender = '',
      this.fullName = '',
      this.hrsCode = '',
      this.code = '',
      this.branch = '',
      this.branchName = ''})
      : super._();

  @override
  @JsonKey()
  final String gender;
  @override
  @JsonKey()
  final String fullName;
  @override
  @JsonKey()
  final String hrsCode;
  @override
  @JsonKey()
  final String code;
  @override
  @JsonKey()
  final String branch;
  @override
  @JsonKey()
  final String branchName;

  @override
  String toString() {
    return 'UserInfoState(gender: $gender, fullName: $fullName, hrsCode: $hrsCode, code: $code, branch: $branch, branchName: $branchName)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UserInfoStateImpl &&
            (identical(other.gender, gender) || other.gender == gender) &&
            (identical(other.fullName, fullName) ||
                other.fullName == fullName) &&
            (identical(other.hrsCode, hrsCode) || other.hrsCode == hrsCode) &&
            (identical(other.code, code) || other.code == code) &&
            (identical(other.branch, branch) || other.branch == branch) &&
            (identical(other.branchName, branchName) ||
                other.branchName == branchName));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, gender, fullName, hrsCode, code, branch, branchName);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$UserInfoStateImplCopyWith<_$UserInfoStateImpl> get copyWith =>
      __$$UserInfoStateImplCopyWithImpl<_$UserInfoStateImpl>(this, _$identity);
}

abstract class _UserInfoState extends UserInfoState {
  const factory _UserInfoState(
      {final String gender,
      final String fullName,
      final String hrsCode,
      final String code,
      final String branch,
      final String branchName}) = _$UserInfoStateImpl;
  const _UserInfoState._() : super._();

  @override
  String get gender;
  @override
  String get fullName;
  @override
  String get hrsCode;
  @override
  String get code;
  @override
  String get branch;
  @override
  String get branchName;
  @override
  @JsonKey(ignore: true)
  _$$UserInfoStateImplCopyWith<_$UserInfoStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
