import 'package:freezed_annotation/freezed_annotation.dart';

import 'package:base_architecture_flutter/src/base/bloc/base_bloc_event.dart';

part 'user_info_event.freezed.dart';

abstract class UserInfoEvent extends BaseBlocEvent {
  const UserInfoEvent();
}

@freezed
class UserInfoPageInitiated extends UserInfoEvent with _$UserInfoPageInitiated {
  const factory UserInfoPageInitiated({
    required int id,
  }) = _UserInfoPageInitiated;
}

@freezed
class LogoutPressed extends UserInfoEvent with _$LogoutPressed {
  const factory LogoutPressed() = _LogoutPressed;
}
