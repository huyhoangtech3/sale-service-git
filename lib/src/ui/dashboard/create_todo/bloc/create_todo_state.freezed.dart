// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'create_todo_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$CreateTodoState {
  String get todoTitle => throw _privateConstructorUsedError;
  String get fromDate => throw _privateConstructorUsedError;
  String get toDate => throw _privateConstructorUsedError;
  int get priority => throw _privateConstructorUsedError;
  bool get isCreateButtonEnabled => throw _privateConstructorUsedError;
  bool get isReEntryButtonEnabled => throw _privateConstructorUsedError;
  String get todoContent => throw _privateConstructorUsedError;
  String get todoLocation => throw _privateConstructorUsedError;
  String get selectedDate => throw _privateConstructorUsedError;
  String get dateCount => throw _privateConstructorUsedError;
  String get range => throw _privateConstructorUsedError;
  String get rangeCount => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CreateTodoStateCopyWith<CreateTodoState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CreateTodoStateCopyWith<$Res> {
  factory $CreateTodoStateCopyWith(
          CreateTodoState value, $Res Function(CreateTodoState) then) =
      _$CreateTodoStateCopyWithImpl<$Res, CreateTodoState>;
  @useResult
  $Res call(
      {String todoTitle,
      String fromDate,
      String toDate,
      int priority,
      bool isCreateButtonEnabled,
      bool isReEntryButtonEnabled,
      String todoContent,
      String todoLocation,
      String selectedDate,
      String dateCount,
      String range,
      String rangeCount});
}

/// @nodoc
class _$CreateTodoStateCopyWithImpl<$Res, $Val extends CreateTodoState>
    implements $CreateTodoStateCopyWith<$Res> {
  _$CreateTodoStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? todoTitle = null,
    Object? fromDate = null,
    Object? toDate = null,
    Object? priority = null,
    Object? isCreateButtonEnabled = null,
    Object? isReEntryButtonEnabled = null,
    Object? todoContent = null,
    Object? todoLocation = null,
    Object? selectedDate = null,
    Object? dateCount = null,
    Object? range = null,
    Object? rangeCount = null,
  }) {
    return _then(_value.copyWith(
      todoTitle: null == todoTitle
          ? _value.todoTitle
          : todoTitle // ignore: cast_nullable_to_non_nullable
              as String,
      fromDate: null == fromDate
          ? _value.fromDate
          : fromDate // ignore: cast_nullable_to_non_nullable
              as String,
      toDate: null == toDate
          ? _value.toDate
          : toDate // ignore: cast_nullable_to_non_nullable
              as String,
      priority: null == priority
          ? _value.priority
          : priority // ignore: cast_nullable_to_non_nullable
              as int,
      isCreateButtonEnabled: null == isCreateButtonEnabled
          ? _value.isCreateButtonEnabled
          : isCreateButtonEnabled // ignore: cast_nullable_to_non_nullable
              as bool,
      isReEntryButtonEnabled: null == isReEntryButtonEnabled
          ? _value.isReEntryButtonEnabled
          : isReEntryButtonEnabled // ignore: cast_nullable_to_non_nullable
              as bool,
      todoContent: null == todoContent
          ? _value.todoContent
          : todoContent // ignore: cast_nullable_to_non_nullable
              as String,
      todoLocation: null == todoLocation
          ? _value.todoLocation
          : todoLocation // ignore: cast_nullable_to_non_nullable
              as String,
      selectedDate: null == selectedDate
          ? _value.selectedDate
          : selectedDate // ignore: cast_nullable_to_non_nullable
              as String,
      dateCount: null == dateCount
          ? _value.dateCount
          : dateCount // ignore: cast_nullable_to_non_nullable
              as String,
      range: null == range
          ? _value.range
          : range // ignore: cast_nullable_to_non_nullable
              as String,
      rangeCount: null == rangeCount
          ? _value.rangeCount
          : rangeCount // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$CreateTodoStateImplCopyWith<$Res>
    implements $CreateTodoStateCopyWith<$Res> {
  factory _$$CreateTodoStateImplCopyWith(_$CreateTodoStateImpl value,
          $Res Function(_$CreateTodoStateImpl) then) =
      __$$CreateTodoStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String todoTitle,
      String fromDate,
      String toDate,
      int priority,
      bool isCreateButtonEnabled,
      bool isReEntryButtonEnabled,
      String todoContent,
      String todoLocation,
      String selectedDate,
      String dateCount,
      String range,
      String rangeCount});
}

/// @nodoc
class __$$CreateTodoStateImplCopyWithImpl<$Res>
    extends _$CreateTodoStateCopyWithImpl<$Res, _$CreateTodoStateImpl>
    implements _$$CreateTodoStateImplCopyWith<$Res> {
  __$$CreateTodoStateImplCopyWithImpl(
      _$CreateTodoStateImpl _value, $Res Function(_$CreateTodoStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? todoTitle = null,
    Object? fromDate = null,
    Object? toDate = null,
    Object? priority = null,
    Object? isCreateButtonEnabled = null,
    Object? isReEntryButtonEnabled = null,
    Object? todoContent = null,
    Object? todoLocation = null,
    Object? selectedDate = null,
    Object? dateCount = null,
    Object? range = null,
    Object? rangeCount = null,
  }) {
    return _then(_$CreateTodoStateImpl(
      todoTitle: null == todoTitle
          ? _value.todoTitle
          : todoTitle // ignore: cast_nullable_to_non_nullable
              as String,
      fromDate: null == fromDate
          ? _value.fromDate
          : fromDate // ignore: cast_nullable_to_non_nullable
              as String,
      toDate: null == toDate
          ? _value.toDate
          : toDate // ignore: cast_nullable_to_non_nullable
              as String,
      priority: null == priority
          ? _value.priority
          : priority // ignore: cast_nullable_to_non_nullable
              as int,
      isCreateButtonEnabled: null == isCreateButtonEnabled
          ? _value.isCreateButtonEnabled
          : isCreateButtonEnabled // ignore: cast_nullable_to_non_nullable
              as bool,
      isReEntryButtonEnabled: null == isReEntryButtonEnabled
          ? _value.isReEntryButtonEnabled
          : isReEntryButtonEnabled // ignore: cast_nullable_to_non_nullable
              as bool,
      todoContent: null == todoContent
          ? _value.todoContent
          : todoContent // ignore: cast_nullable_to_non_nullable
              as String,
      todoLocation: null == todoLocation
          ? _value.todoLocation
          : todoLocation // ignore: cast_nullable_to_non_nullable
              as String,
      selectedDate: null == selectedDate
          ? _value.selectedDate
          : selectedDate // ignore: cast_nullable_to_non_nullable
              as String,
      dateCount: null == dateCount
          ? _value.dateCount
          : dateCount // ignore: cast_nullable_to_non_nullable
              as String,
      range: null == range
          ? _value.range
          : range // ignore: cast_nullable_to_non_nullable
              as String,
      rangeCount: null == rangeCount
          ? _value.rangeCount
          : rangeCount // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$CreateTodoStateImpl implements _CreateTodoState {
  const _$CreateTodoStateImpl(
      {this.todoTitle = '',
      this.fromDate = '',
      this.toDate = '',
      this.priority = 0,
      this.isCreateButtonEnabled = false,
      this.isReEntryButtonEnabled = false,
      this.todoContent = '',
      this.todoLocation = '',
      this.selectedDate = '',
      this.dateCount = '',
      this.range = '',
      this.rangeCount = ''});

  @override
  @JsonKey()
  final String todoTitle;
  @override
  @JsonKey()
  final String fromDate;
  @override
  @JsonKey()
  final String toDate;
  @override
  @JsonKey()
  final int priority;
  @override
  @JsonKey()
  final bool isCreateButtonEnabled;
  @override
  @JsonKey()
  final bool isReEntryButtonEnabled;
  @override
  @JsonKey()
  final String todoContent;
  @override
  @JsonKey()
  final String todoLocation;
  @override
  @JsonKey()
  final String selectedDate;
  @override
  @JsonKey()
  final String dateCount;
  @override
  @JsonKey()
  final String range;
  @override
  @JsonKey()
  final String rangeCount;

  @override
  String toString() {
    return 'CreateTodoState(todoTitle: $todoTitle, fromDate: $fromDate, toDate: $toDate, priority: $priority, isCreateButtonEnabled: $isCreateButtonEnabled, isReEntryButtonEnabled: $isReEntryButtonEnabled, todoContent: $todoContent, todoLocation: $todoLocation, selectedDate: $selectedDate, dateCount: $dateCount, range: $range, rangeCount: $rangeCount)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CreateTodoStateImpl &&
            (identical(other.todoTitle, todoTitle) ||
                other.todoTitle == todoTitle) &&
            (identical(other.fromDate, fromDate) ||
                other.fromDate == fromDate) &&
            (identical(other.toDate, toDate) || other.toDate == toDate) &&
            (identical(other.priority, priority) ||
                other.priority == priority) &&
            (identical(other.isCreateButtonEnabled, isCreateButtonEnabled) ||
                other.isCreateButtonEnabled == isCreateButtonEnabled) &&
            (identical(other.isReEntryButtonEnabled, isReEntryButtonEnabled) ||
                other.isReEntryButtonEnabled == isReEntryButtonEnabled) &&
            (identical(other.todoContent, todoContent) ||
                other.todoContent == todoContent) &&
            (identical(other.todoLocation, todoLocation) ||
                other.todoLocation == todoLocation) &&
            (identical(other.selectedDate, selectedDate) ||
                other.selectedDate == selectedDate) &&
            (identical(other.dateCount, dateCount) ||
                other.dateCount == dateCount) &&
            (identical(other.range, range) || other.range == range) &&
            (identical(other.rangeCount, rangeCount) ||
                other.rangeCount == rangeCount));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      todoTitle,
      fromDate,
      toDate,
      priority,
      isCreateButtonEnabled,
      isReEntryButtonEnabled,
      todoContent,
      todoLocation,
      selectedDate,
      dateCount,
      range,
      rangeCount);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CreateTodoStateImplCopyWith<_$CreateTodoStateImpl> get copyWith =>
      __$$CreateTodoStateImplCopyWithImpl<_$CreateTodoStateImpl>(
          this, _$identity);
}

abstract class _CreateTodoState implements CreateTodoState {
  const factory _CreateTodoState(
      {final String todoTitle,
      final String fromDate,
      final String toDate,
      final int priority,
      final bool isCreateButtonEnabled,
      final bool isReEntryButtonEnabled,
      final String todoContent,
      final String todoLocation,
      final String selectedDate,
      final String dateCount,
      final String range,
      final String rangeCount}) = _$CreateTodoStateImpl;

  @override
  String get todoTitle;
  @override
  String get fromDate;
  @override
  String get toDate;
  @override
  int get priority;
  @override
  bool get isCreateButtonEnabled;
  @override
  bool get isReEntryButtonEnabled;
  @override
  String get todoContent;
  @override
  String get todoLocation;
  @override
  String get selectedDate;
  @override
  String get dateCount;
  @override
  String get range;
  @override
  String get rangeCount;
  @override
  @JsonKey(ignore: true)
  _$$CreateTodoStateImplCopyWith<_$CreateTodoStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
