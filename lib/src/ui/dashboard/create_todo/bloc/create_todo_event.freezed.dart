// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'create_todo_event.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$TodoTitleTextFieldChanged {
  String get todoTitle => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $TodoTitleTextFieldChangedCopyWith<TodoTitleTextFieldChanged> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TodoTitleTextFieldChangedCopyWith<$Res> {
  factory $TodoTitleTextFieldChangedCopyWith(TodoTitleTextFieldChanged value,
          $Res Function(TodoTitleTextFieldChanged) then) =
      _$TodoTitleTextFieldChangedCopyWithImpl<$Res, TodoTitleTextFieldChanged>;
  @useResult
  $Res call({String todoTitle});
}

/// @nodoc
class _$TodoTitleTextFieldChangedCopyWithImpl<$Res,
        $Val extends TodoTitleTextFieldChanged>
    implements $TodoTitleTextFieldChangedCopyWith<$Res> {
  _$TodoTitleTextFieldChangedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? todoTitle = null,
  }) {
    return _then(_value.copyWith(
      todoTitle: null == todoTitle
          ? _value.todoTitle
          : todoTitle // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$TodoTitleTextFieldChangedImplCopyWith<$Res>
    implements $TodoTitleTextFieldChangedCopyWith<$Res> {
  factory _$$TodoTitleTextFieldChangedImplCopyWith(
          _$TodoTitleTextFieldChangedImpl value,
          $Res Function(_$TodoTitleTextFieldChangedImpl) then) =
      __$$TodoTitleTextFieldChangedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String todoTitle});
}

/// @nodoc
class __$$TodoTitleTextFieldChangedImplCopyWithImpl<$Res>
    extends _$TodoTitleTextFieldChangedCopyWithImpl<$Res,
        _$TodoTitleTextFieldChangedImpl>
    implements _$$TodoTitleTextFieldChangedImplCopyWith<$Res> {
  __$$TodoTitleTextFieldChangedImplCopyWithImpl(
      _$TodoTitleTextFieldChangedImpl _value,
      $Res Function(_$TodoTitleTextFieldChangedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? todoTitle = null,
  }) {
    return _then(_$TodoTitleTextFieldChangedImpl(
      todoTitle: null == todoTitle
          ? _value.todoTitle
          : todoTitle // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$TodoTitleTextFieldChangedImpl implements _TodoTitleTextFieldChanged {
  const _$TodoTitleTextFieldChangedImpl({required this.todoTitle});

  @override
  final String todoTitle;

  @override
  String toString() {
    return 'TodoTitleTextFieldChanged(todoTitle: $todoTitle)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$TodoTitleTextFieldChangedImpl &&
            (identical(other.todoTitle, todoTitle) ||
                other.todoTitle == todoTitle));
  }

  @override
  int get hashCode => Object.hash(runtimeType, todoTitle);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$TodoTitleTextFieldChangedImplCopyWith<_$TodoTitleTextFieldChangedImpl>
      get copyWith => __$$TodoTitleTextFieldChangedImplCopyWithImpl<
          _$TodoTitleTextFieldChangedImpl>(this, _$identity);
}

abstract class _TodoTitleTextFieldChanged implements TodoTitleTextFieldChanged {
  const factory _TodoTitleTextFieldChanged({required final String todoTitle}) =
      _$TodoTitleTextFieldChangedImpl;

  @override
  String get todoTitle;
  @override
  @JsonKey(ignore: true)
  _$$TodoTitleTextFieldChangedImplCopyWith<_$TodoTitleTextFieldChangedImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$TodoPriorityCheckBoxChanged {
  String get priority => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $TodoPriorityCheckBoxChangedCopyWith<TodoPriorityCheckBoxChanged>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TodoPriorityCheckBoxChangedCopyWith<$Res> {
  factory $TodoPriorityCheckBoxChangedCopyWith(
          TodoPriorityCheckBoxChanged value,
          $Res Function(TodoPriorityCheckBoxChanged) then) =
      _$TodoPriorityCheckBoxChangedCopyWithImpl<$Res,
          TodoPriorityCheckBoxChanged>;
  @useResult
  $Res call({String priority});
}

/// @nodoc
class _$TodoPriorityCheckBoxChangedCopyWithImpl<$Res,
        $Val extends TodoPriorityCheckBoxChanged>
    implements $TodoPriorityCheckBoxChangedCopyWith<$Res> {
  _$TodoPriorityCheckBoxChangedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? priority = null,
  }) {
    return _then(_value.copyWith(
      priority: null == priority
          ? _value.priority
          : priority // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$TodoPriorityCheckBoxChangedImplCopyWith<$Res>
    implements $TodoPriorityCheckBoxChangedCopyWith<$Res> {
  factory _$$TodoPriorityCheckBoxChangedImplCopyWith(
          _$TodoPriorityCheckBoxChangedImpl value,
          $Res Function(_$TodoPriorityCheckBoxChangedImpl) then) =
      __$$TodoPriorityCheckBoxChangedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String priority});
}

/// @nodoc
class __$$TodoPriorityCheckBoxChangedImplCopyWithImpl<$Res>
    extends _$TodoPriorityCheckBoxChangedCopyWithImpl<$Res,
        _$TodoPriorityCheckBoxChangedImpl>
    implements _$$TodoPriorityCheckBoxChangedImplCopyWith<$Res> {
  __$$TodoPriorityCheckBoxChangedImplCopyWithImpl(
      _$TodoPriorityCheckBoxChangedImpl _value,
      $Res Function(_$TodoPriorityCheckBoxChangedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? priority = null,
  }) {
    return _then(_$TodoPriorityCheckBoxChangedImpl(
      priority: null == priority
          ? _value.priority
          : priority // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$TodoPriorityCheckBoxChangedImpl
    implements _TodoPriorityCheckBoxChanged {
  const _$TodoPriorityCheckBoxChangedImpl({required this.priority});

  @override
  final String priority;

  @override
  String toString() {
    return 'TodoPriorityCheckBoxChanged(priority: $priority)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$TodoPriorityCheckBoxChangedImpl &&
            (identical(other.priority, priority) ||
                other.priority == priority));
  }

  @override
  int get hashCode => Object.hash(runtimeType, priority);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$TodoPriorityCheckBoxChangedImplCopyWith<_$TodoPriorityCheckBoxChangedImpl>
      get copyWith => __$$TodoPriorityCheckBoxChangedImplCopyWithImpl<
          _$TodoPriorityCheckBoxChangedImpl>(this, _$identity);
}

abstract class _TodoPriorityCheckBoxChanged
    implements TodoPriorityCheckBoxChanged {
  const factory _TodoPriorityCheckBoxChanged({required final String priority}) =
      _$TodoPriorityCheckBoxChangedImpl;

  @override
  String get priority;
  @override
  @JsonKey(ignore: true)
  _$$TodoPriorityCheckBoxChangedImplCopyWith<_$TodoPriorityCheckBoxChangedImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$TodoFromDatePickerChanged {
  String get fromDatePicker => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $TodoFromDatePickerChangedCopyWith<TodoFromDatePickerChanged> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TodoFromDatePickerChangedCopyWith<$Res> {
  factory $TodoFromDatePickerChangedCopyWith(TodoFromDatePickerChanged value,
          $Res Function(TodoFromDatePickerChanged) then) =
      _$TodoFromDatePickerChangedCopyWithImpl<$Res, TodoFromDatePickerChanged>;
  @useResult
  $Res call({String fromDatePicker});
}

/// @nodoc
class _$TodoFromDatePickerChangedCopyWithImpl<$Res,
        $Val extends TodoFromDatePickerChanged>
    implements $TodoFromDatePickerChangedCopyWith<$Res> {
  _$TodoFromDatePickerChangedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? fromDatePicker = null,
  }) {
    return _then(_value.copyWith(
      fromDatePicker: null == fromDatePicker
          ? _value.fromDatePicker
          : fromDatePicker // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$TodoFromDatePickerChangedImplCopyWith<$Res>
    implements $TodoFromDatePickerChangedCopyWith<$Res> {
  factory _$$TodoFromDatePickerChangedImplCopyWith(
          _$TodoFromDatePickerChangedImpl value,
          $Res Function(_$TodoFromDatePickerChangedImpl) then) =
      __$$TodoFromDatePickerChangedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String fromDatePicker});
}

/// @nodoc
class __$$TodoFromDatePickerChangedImplCopyWithImpl<$Res>
    extends _$TodoFromDatePickerChangedCopyWithImpl<$Res,
        _$TodoFromDatePickerChangedImpl>
    implements _$$TodoFromDatePickerChangedImplCopyWith<$Res> {
  __$$TodoFromDatePickerChangedImplCopyWithImpl(
      _$TodoFromDatePickerChangedImpl _value,
      $Res Function(_$TodoFromDatePickerChangedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? fromDatePicker = null,
  }) {
    return _then(_$TodoFromDatePickerChangedImpl(
      fromDatePicker: null == fromDatePicker
          ? _value.fromDatePicker
          : fromDatePicker // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$TodoFromDatePickerChangedImpl implements _TodoFromDatePickerChanged {
  const _$TodoFromDatePickerChangedImpl({required this.fromDatePicker});

  @override
  final String fromDatePicker;

  @override
  String toString() {
    return 'TodoFromDatePickerChanged(fromDatePicker: $fromDatePicker)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$TodoFromDatePickerChangedImpl &&
            (identical(other.fromDatePicker, fromDatePicker) ||
                other.fromDatePicker == fromDatePicker));
  }

  @override
  int get hashCode => Object.hash(runtimeType, fromDatePicker);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$TodoFromDatePickerChangedImplCopyWith<_$TodoFromDatePickerChangedImpl>
      get copyWith => __$$TodoFromDatePickerChangedImplCopyWithImpl<
          _$TodoFromDatePickerChangedImpl>(this, _$identity);
}

abstract class _TodoFromDatePickerChanged implements TodoFromDatePickerChanged {
  const factory _TodoFromDatePickerChanged(
      {required final String fromDatePicker}) = _$TodoFromDatePickerChangedImpl;

  @override
  String get fromDatePicker;
  @override
  @JsonKey(ignore: true)
  _$$TodoFromDatePickerChangedImplCopyWith<_$TodoFromDatePickerChangedImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$TodoToDatePickerChanged {
  String get toDatePicker => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $TodoToDatePickerChangedCopyWith<TodoToDatePickerChanged> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TodoToDatePickerChangedCopyWith<$Res> {
  factory $TodoToDatePickerChangedCopyWith(TodoToDatePickerChanged value,
          $Res Function(TodoToDatePickerChanged) then) =
      _$TodoToDatePickerChangedCopyWithImpl<$Res, TodoToDatePickerChanged>;
  @useResult
  $Res call({String toDatePicker});
}

/// @nodoc
class _$TodoToDatePickerChangedCopyWithImpl<$Res,
        $Val extends TodoToDatePickerChanged>
    implements $TodoToDatePickerChangedCopyWith<$Res> {
  _$TodoToDatePickerChangedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? toDatePicker = null,
  }) {
    return _then(_value.copyWith(
      toDatePicker: null == toDatePicker
          ? _value.toDatePicker
          : toDatePicker // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$TodoToDatePickerChangedImplCopyWith<$Res>
    implements $TodoToDatePickerChangedCopyWith<$Res> {
  factory _$$TodoToDatePickerChangedImplCopyWith(
          _$TodoToDatePickerChangedImpl value,
          $Res Function(_$TodoToDatePickerChangedImpl) then) =
      __$$TodoToDatePickerChangedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String toDatePicker});
}

/// @nodoc
class __$$TodoToDatePickerChangedImplCopyWithImpl<$Res>
    extends _$TodoToDatePickerChangedCopyWithImpl<$Res,
        _$TodoToDatePickerChangedImpl>
    implements _$$TodoToDatePickerChangedImplCopyWith<$Res> {
  __$$TodoToDatePickerChangedImplCopyWithImpl(
      _$TodoToDatePickerChangedImpl _value,
      $Res Function(_$TodoToDatePickerChangedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? toDatePicker = null,
  }) {
    return _then(_$TodoToDatePickerChangedImpl(
      toDatePicker: null == toDatePicker
          ? _value.toDatePicker
          : toDatePicker // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$TodoToDatePickerChangedImpl implements _TodoToDatePickerChanged {
  const _$TodoToDatePickerChangedImpl({required this.toDatePicker});

  @override
  final String toDatePicker;

  @override
  String toString() {
    return 'TodoToDatePickerChanged(toDatePicker: $toDatePicker)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$TodoToDatePickerChangedImpl &&
            (identical(other.toDatePicker, toDatePicker) ||
                other.toDatePicker == toDatePicker));
  }

  @override
  int get hashCode => Object.hash(runtimeType, toDatePicker);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$TodoToDatePickerChangedImplCopyWith<_$TodoToDatePickerChangedImpl>
      get copyWith => __$$TodoToDatePickerChangedImplCopyWithImpl<
          _$TodoToDatePickerChangedImpl>(this, _$identity);
}

abstract class _TodoToDatePickerChanged implements TodoToDatePickerChanged {
  const factory _TodoToDatePickerChanged({required final String toDatePicker}) =
      _$TodoToDatePickerChangedImpl;

  @override
  String get toDatePicker;
  @override
  @JsonKey(ignore: true)
  _$$TodoToDatePickerChangedImplCopyWith<_$TodoToDatePickerChangedImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$TodoContentChanged {
  String get todoContent => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $TodoContentChangedCopyWith<TodoContentChanged> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TodoContentChangedCopyWith<$Res> {
  factory $TodoContentChangedCopyWith(
          TodoContentChanged value, $Res Function(TodoContentChanged) then) =
      _$TodoContentChangedCopyWithImpl<$Res, TodoContentChanged>;
  @useResult
  $Res call({String todoContent});
}

/// @nodoc
class _$TodoContentChangedCopyWithImpl<$Res, $Val extends TodoContentChanged>
    implements $TodoContentChangedCopyWith<$Res> {
  _$TodoContentChangedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? todoContent = null,
  }) {
    return _then(_value.copyWith(
      todoContent: null == todoContent
          ? _value.todoContent
          : todoContent // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$TodoContentTodoChangedImplCopyWith<$Res>
    implements $TodoContentChangedCopyWith<$Res> {
  factory _$$TodoContentTodoChangedImplCopyWith(
          _$TodoContentTodoChangedImpl value,
          $Res Function(_$TodoContentTodoChangedImpl) then) =
      __$$TodoContentTodoChangedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String todoContent});
}

/// @nodoc
class __$$TodoContentTodoChangedImplCopyWithImpl<$Res>
    extends _$TodoContentChangedCopyWithImpl<$Res, _$TodoContentTodoChangedImpl>
    implements _$$TodoContentTodoChangedImplCopyWith<$Res> {
  __$$TodoContentTodoChangedImplCopyWithImpl(
      _$TodoContentTodoChangedImpl _value,
      $Res Function(_$TodoContentTodoChangedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? todoContent = null,
  }) {
    return _then(_$TodoContentTodoChangedImpl(
      todoContent: null == todoContent
          ? _value.todoContent
          : todoContent // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$TodoContentTodoChangedImpl implements _TodoContentTodoChanged {
  const _$TodoContentTodoChangedImpl({required this.todoContent});

  @override
  final String todoContent;

  @override
  String toString() {
    return 'TodoContentChanged(todoContent: $todoContent)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$TodoContentTodoChangedImpl &&
            (identical(other.todoContent, todoContent) ||
                other.todoContent == todoContent));
  }

  @override
  int get hashCode => Object.hash(runtimeType, todoContent);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$TodoContentTodoChangedImplCopyWith<_$TodoContentTodoChangedImpl>
      get copyWith => __$$TodoContentTodoChangedImplCopyWithImpl<
          _$TodoContentTodoChangedImpl>(this, _$identity);
}

abstract class _TodoContentTodoChanged implements TodoContentChanged {
  const factory _TodoContentTodoChanged({required final String todoContent}) =
      _$TodoContentTodoChangedImpl;

  @override
  String get todoContent;
  @override
  @JsonKey(ignore: true)
  _$$TodoContentTodoChangedImplCopyWith<_$TodoContentTodoChangedImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$ReEntryCreateTodoButtonPressed {}

/// @nodoc
abstract class $ReEntryCreateTodoButtonPressedCopyWith<$Res> {
  factory $ReEntryCreateTodoButtonPressedCopyWith(
          ReEntryCreateTodoButtonPressed value,
          $Res Function(ReEntryCreateTodoButtonPressed) then) =
      _$ReEntryCreateTodoButtonPressedCopyWithImpl<$Res,
          ReEntryCreateTodoButtonPressed>;
}

/// @nodoc
class _$ReEntryCreateTodoButtonPressedCopyWithImpl<$Res,
        $Val extends ReEntryCreateTodoButtonPressed>
    implements $ReEntryCreateTodoButtonPressedCopyWith<$Res> {
  _$ReEntryCreateTodoButtonPressedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$ReEntryCreateTodoButtonPressedImplCopyWith<$Res> {
  factory _$$ReEntryCreateTodoButtonPressedImplCopyWith(
          _$ReEntryCreateTodoButtonPressedImpl value,
          $Res Function(_$ReEntryCreateTodoButtonPressedImpl) then) =
      __$$ReEntryCreateTodoButtonPressedImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ReEntryCreateTodoButtonPressedImplCopyWithImpl<$Res>
    extends _$ReEntryCreateTodoButtonPressedCopyWithImpl<$Res,
        _$ReEntryCreateTodoButtonPressedImpl>
    implements _$$ReEntryCreateTodoButtonPressedImplCopyWith<$Res> {
  __$$ReEntryCreateTodoButtonPressedImplCopyWithImpl(
      _$ReEntryCreateTodoButtonPressedImpl _value,
      $Res Function(_$ReEntryCreateTodoButtonPressedImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ReEntryCreateTodoButtonPressedImpl
    implements _ReEntryCreateTodoButtonPressed {
  const _$ReEntryCreateTodoButtonPressedImpl();

  @override
  String toString() {
    return 'ReEntryCreateTodoButtonPressed()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ReEntryCreateTodoButtonPressedImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;
}

abstract class _ReEntryCreateTodoButtonPressed
    implements ReEntryCreateTodoButtonPressed {
  const factory _ReEntryCreateTodoButtonPressed() =
      _$ReEntryCreateTodoButtonPressedImpl;
}

/// @nodoc
mixin _$CreateTodoButtonPressed {
  String get todoTitle => throw _privateConstructorUsedError;
  String get priority => throw _privateConstructorUsedError;
  String get fromDatePicker => throw _privateConstructorUsedError;
  String get toDatePicker => throw _privateConstructorUsedError;
  String get todoContent => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CreateTodoButtonPressedCopyWith<CreateTodoButtonPressed> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CreateTodoButtonPressedCopyWith<$Res> {
  factory $CreateTodoButtonPressedCopyWith(CreateTodoButtonPressed value,
          $Res Function(CreateTodoButtonPressed) then) =
      _$CreateTodoButtonPressedCopyWithImpl<$Res, CreateTodoButtonPressed>;
  @useResult
  $Res call(
      {String todoTitle,
      String priority,
      String fromDatePicker,
      String toDatePicker,
      String todoContent});
}

/// @nodoc
class _$CreateTodoButtonPressedCopyWithImpl<$Res,
        $Val extends CreateTodoButtonPressed>
    implements $CreateTodoButtonPressedCopyWith<$Res> {
  _$CreateTodoButtonPressedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? todoTitle = null,
    Object? priority = null,
    Object? fromDatePicker = null,
    Object? toDatePicker = null,
    Object? todoContent = null,
  }) {
    return _then(_value.copyWith(
      todoTitle: null == todoTitle
          ? _value.todoTitle
          : todoTitle // ignore: cast_nullable_to_non_nullable
              as String,
      priority: null == priority
          ? _value.priority
          : priority // ignore: cast_nullable_to_non_nullable
              as String,
      fromDatePicker: null == fromDatePicker
          ? _value.fromDatePicker
          : fromDatePicker // ignore: cast_nullable_to_non_nullable
              as String,
      toDatePicker: null == toDatePicker
          ? _value.toDatePicker
          : toDatePicker // ignore: cast_nullable_to_non_nullable
              as String,
      todoContent: null == todoContent
          ? _value.todoContent
          : todoContent // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$CreateTodoButtonPressedImplCopyWith<$Res>
    implements $CreateTodoButtonPressedCopyWith<$Res> {
  factory _$$CreateTodoButtonPressedImplCopyWith(
          _$CreateTodoButtonPressedImpl value,
          $Res Function(_$CreateTodoButtonPressedImpl) then) =
      __$$CreateTodoButtonPressedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String todoTitle,
      String priority,
      String fromDatePicker,
      String toDatePicker,
      String todoContent});
}

/// @nodoc
class __$$CreateTodoButtonPressedImplCopyWithImpl<$Res>
    extends _$CreateTodoButtonPressedCopyWithImpl<$Res,
        _$CreateTodoButtonPressedImpl>
    implements _$$CreateTodoButtonPressedImplCopyWith<$Res> {
  __$$CreateTodoButtonPressedImplCopyWithImpl(
      _$CreateTodoButtonPressedImpl _value,
      $Res Function(_$CreateTodoButtonPressedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? todoTitle = null,
    Object? priority = null,
    Object? fromDatePicker = null,
    Object? toDatePicker = null,
    Object? todoContent = null,
  }) {
    return _then(_$CreateTodoButtonPressedImpl(
      todoTitle: null == todoTitle
          ? _value.todoTitle
          : todoTitle // ignore: cast_nullable_to_non_nullable
              as String,
      priority: null == priority
          ? _value.priority
          : priority // ignore: cast_nullable_to_non_nullable
              as String,
      fromDatePicker: null == fromDatePicker
          ? _value.fromDatePicker
          : fromDatePicker // ignore: cast_nullable_to_non_nullable
              as String,
      toDatePicker: null == toDatePicker
          ? _value.toDatePicker
          : toDatePicker // ignore: cast_nullable_to_non_nullable
              as String,
      todoContent: null == todoContent
          ? _value.todoContent
          : todoContent // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$CreateTodoButtonPressedImpl implements _CreateTodoButtonPressed {
  const _$CreateTodoButtonPressedImpl(
      {required this.todoTitle,
      required this.priority,
      required this.fromDatePicker,
      required this.toDatePicker,
      required this.todoContent});

  @override
  final String todoTitle;
  @override
  final String priority;
  @override
  final String fromDatePicker;
  @override
  final String toDatePicker;
  @override
  final String todoContent;

  @override
  String toString() {
    return 'CreateTodoButtonPressed(todoTitle: $todoTitle, priority: $priority, fromDatePicker: $fromDatePicker, toDatePicker: $toDatePicker, todoContent: $todoContent)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CreateTodoButtonPressedImpl &&
            (identical(other.todoTitle, todoTitle) ||
                other.todoTitle == todoTitle) &&
            (identical(other.priority, priority) ||
                other.priority == priority) &&
            (identical(other.fromDatePicker, fromDatePicker) ||
                other.fromDatePicker == fromDatePicker) &&
            (identical(other.toDatePicker, toDatePicker) ||
                other.toDatePicker == toDatePicker) &&
            (identical(other.todoContent, todoContent) ||
                other.todoContent == todoContent));
  }

  @override
  int get hashCode => Object.hash(runtimeType, todoTitle, priority,
      fromDatePicker, toDatePicker, todoContent);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CreateTodoButtonPressedImplCopyWith<_$CreateTodoButtonPressedImpl>
      get copyWith => __$$CreateTodoButtonPressedImplCopyWithImpl<
          _$CreateTodoButtonPressedImpl>(this, _$identity);
}

abstract class _CreateTodoButtonPressed implements CreateTodoButtonPressed {
  const factory _CreateTodoButtonPressed(
      {required final String todoTitle,
      required final String priority,
      required final String fromDatePicker,
      required final String toDatePicker,
      required final String todoContent}) = _$CreateTodoButtonPressedImpl;

  @override
  String get todoTitle;
  @override
  String get priority;
  @override
  String get fromDatePicker;
  @override
  String get toDatePicker;
  @override
  String get todoContent;
  @override
  @JsonKey(ignore: true)
  _$$CreateTodoButtonPressedImplCopyWith<_$CreateTodoButtonPressedImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$PickerDateRangeTodoButtonPressed {
  String get selectedDate => throw _privateConstructorUsedError;
  String get dateCount => throw _privateConstructorUsedError;
  String get range => throw _privateConstructorUsedError;
  String get rangeCount => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $PickerDateRangeTodoButtonPressedCopyWith<PickerDateRangeTodoButtonPressed>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PickerDateRangeTodoButtonPressedCopyWith<$Res> {
  factory $PickerDateRangeTodoButtonPressedCopyWith(
          PickerDateRangeTodoButtonPressed value,
          $Res Function(PickerDateRangeTodoButtonPressed) then) =
      _$PickerDateRangeTodoButtonPressedCopyWithImpl<$Res,
          PickerDateRangeTodoButtonPressed>;
  @useResult
  $Res call(
      {String selectedDate, String dateCount, String range, String rangeCount});
}

/// @nodoc
class _$PickerDateRangeTodoButtonPressedCopyWithImpl<$Res,
        $Val extends PickerDateRangeTodoButtonPressed>
    implements $PickerDateRangeTodoButtonPressedCopyWith<$Res> {
  _$PickerDateRangeTodoButtonPressedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? selectedDate = null,
    Object? dateCount = null,
    Object? range = null,
    Object? rangeCount = null,
  }) {
    return _then(_value.copyWith(
      selectedDate: null == selectedDate
          ? _value.selectedDate
          : selectedDate // ignore: cast_nullable_to_non_nullable
              as String,
      dateCount: null == dateCount
          ? _value.dateCount
          : dateCount // ignore: cast_nullable_to_non_nullable
              as String,
      range: null == range
          ? _value.range
          : range // ignore: cast_nullable_to_non_nullable
              as String,
      rangeCount: null == rangeCount
          ? _value.rangeCount
          : rangeCount // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$PickerDateRangeTodoButtonPressedImplCopyWith<$Res>
    implements $PickerDateRangeTodoButtonPressedCopyWith<$Res> {
  factory _$$PickerDateRangeTodoButtonPressedImplCopyWith(
          _$PickerDateRangeTodoButtonPressedImpl value,
          $Res Function(_$PickerDateRangeTodoButtonPressedImpl) then) =
      __$$PickerDateRangeTodoButtonPressedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String selectedDate, String dateCount, String range, String rangeCount});
}

/// @nodoc
class __$$PickerDateRangeTodoButtonPressedImplCopyWithImpl<$Res>
    extends _$PickerDateRangeTodoButtonPressedCopyWithImpl<$Res,
        _$PickerDateRangeTodoButtonPressedImpl>
    implements _$$PickerDateRangeTodoButtonPressedImplCopyWith<$Res> {
  __$$PickerDateRangeTodoButtonPressedImplCopyWithImpl(
      _$PickerDateRangeTodoButtonPressedImpl _value,
      $Res Function(_$PickerDateRangeTodoButtonPressedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? selectedDate = null,
    Object? dateCount = null,
    Object? range = null,
    Object? rangeCount = null,
  }) {
    return _then(_$PickerDateRangeTodoButtonPressedImpl(
      selectedDate: null == selectedDate
          ? _value.selectedDate
          : selectedDate // ignore: cast_nullable_to_non_nullable
              as String,
      dateCount: null == dateCount
          ? _value.dateCount
          : dateCount // ignore: cast_nullable_to_non_nullable
              as String,
      range: null == range
          ? _value.range
          : range // ignore: cast_nullable_to_non_nullable
              as String,
      rangeCount: null == rangeCount
          ? _value.rangeCount
          : rangeCount // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$PickerDateRangeTodoButtonPressedImpl
    implements _PickerDateRangeTodoButtonPressed {
  const _$PickerDateRangeTodoButtonPressedImpl(
      {required this.selectedDate,
      required this.dateCount,
      required this.range,
      required this.rangeCount});

  @override
  final String selectedDate;
  @override
  final String dateCount;
  @override
  final String range;
  @override
  final String rangeCount;

  @override
  String toString() {
    return 'PickerDateRangeTodoButtonPressed(selectedDate: $selectedDate, dateCount: $dateCount, range: $range, rangeCount: $rangeCount)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$PickerDateRangeTodoButtonPressedImpl &&
            (identical(other.selectedDate, selectedDate) ||
                other.selectedDate == selectedDate) &&
            (identical(other.dateCount, dateCount) ||
                other.dateCount == dateCount) &&
            (identical(other.range, range) || other.range == range) &&
            (identical(other.rangeCount, rangeCount) ||
                other.rangeCount == rangeCount));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, selectedDate, dateCount, range, rangeCount);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$PickerDateRangeTodoButtonPressedImplCopyWith<
          _$PickerDateRangeTodoButtonPressedImpl>
      get copyWith => __$$PickerDateRangeTodoButtonPressedImplCopyWithImpl<
          _$PickerDateRangeTodoButtonPressedImpl>(this, _$identity);
}

abstract class _PickerDateRangeTodoButtonPressed
    implements PickerDateRangeTodoButtonPressed {
  const factory _PickerDateRangeTodoButtonPressed(
          {required final String selectedDate,
          required final String dateCount,
          required final String range,
          required final String rangeCount}) =
      _$PickerDateRangeTodoButtonPressedImpl;

  @override
  String get selectedDate;
  @override
  String get dateCount;
  @override
  String get range;
  @override
  String get rangeCount;
  @override
  @JsonKey(ignore: true)
  _$$PickerDateRangeTodoButtonPressedImplCopyWith<
          _$PickerDateRangeTodoButtonPressedImpl>
      get copyWith => throw _privateConstructorUsedError;
}
