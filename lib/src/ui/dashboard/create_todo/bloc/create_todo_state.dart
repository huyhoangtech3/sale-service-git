import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:base_architecture_flutter/src/base/bloc/base_bloc_state.dart';
part 'create_todo_state.freezed.dart';

@freezed
class CreateTodoState extends BaseBlocState with _$CreateTodoState {
  const factory CreateTodoState({
    @Default('') String todoTitle,
    @Default('') String fromDate,
    @Default('') String toDate,
    @Default(0) int priority,
    @Default(false) bool isCreateButtonEnabled,
    @Default(false) bool isReEntryButtonEnabled,
    @Default('') String todoContent,
    @Default('') String todoLocation,
    @Default('') String selectedDate,
    @Default('') String dateCount,
    @Default('') String range,
    @Default('') String rangeCount,
  }) = _CreateTodoState;
}
