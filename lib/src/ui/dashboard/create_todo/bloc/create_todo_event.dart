import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:base_architecture_flutter/src/base/bloc/base_bloc_event.dart';
part 'create_todo_event.freezed.dart';

abstract class CreateTodoEvent extends BaseBlocEvent {
  const CreateTodoEvent();
}

@freezed
class TodoTitleTextFieldChanged extends CreateTodoEvent with _$TodoTitleTextFieldChanged {
  const factory TodoTitleTextFieldChanged({
    required String todoTitle,
  }) = _TodoTitleTextFieldChanged;
}

@freezed
class TodoPriorityCheckBoxChanged extends CreateTodoEvent with _$TodoPriorityCheckBoxChanged {
  const factory TodoPriorityCheckBoxChanged({
    required String priority,
  }) = _TodoPriorityCheckBoxChanged;
}

@freezed
class TodoFromDatePickerChanged extends CreateTodoEvent with _$TodoFromDatePickerChanged {
  const factory TodoFromDatePickerChanged({
    required String fromDatePicker,
  }) = _TodoFromDatePickerChanged;
}

@freezed
class TodoToDatePickerChanged extends CreateTodoEvent with _$TodoToDatePickerChanged {
  const factory TodoToDatePickerChanged({
    required String toDatePicker,
  }) = _TodoToDatePickerChanged;
}

@freezed
class TodoContentChanged extends CreateTodoEvent with _$TodoContentChanged {
  const factory TodoContentChanged({
    required String todoContent,
  }) = _TodoContentTodoChanged;
}

@freezed
class ReEntryCreateTodoButtonPressed extends CreateTodoEvent with _$ReEntryCreateTodoButtonPressed {
  const factory ReEntryCreateTodoButtonPressed() = _ReEntryCreateTodoButtonPressed;
}

@freezed
class CreateTodoButtonPressed extends CreateTodoEvent with _$CreateTodoButtonPressed {
  const factory CreateTodoButtonPressed({
    required String todoTitle,
    required String priority,
    required String fromDatePicker,
    required String toDatePicker,
    required String todoContent,
  }) = _CreateTodoButtonPressed;
}

@freezed
class PickerDateRangeTodoButtonPressed extends CreateTodoEvent with _$PickerDateRangeTodoButtonPressed {
  const factory PickerDateRangeTodoButtonPressed({
    required String selectedDate,
    required String dateCount,
    required String range,
    required String rangeCount,
  }) = _PickerDateRangeTodoButtonPressed;
}