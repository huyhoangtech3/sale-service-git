import 'dart:async';
import 'package:domain/domain.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:base_architecture_flutter/src/app.dart';
import 'package:shared/shared.dart';

@Injectable()
class CreateTodoBloc extends BaseBloc<CreateTodoEvent, CreateTodoState> {
  CreateTodoBloc(this._createTodoUseCase) : super(const CreateTodoState()) {
    on<TodoTitleTextFieldChanged>(
      _onTodoTitleTextFieldChanged,
      transformer: distinct(),
    );

    on<TodoPriorityCheckBoxChanged>(
      _onTodoPriorityCheckBoxChanged,
      transformer: distinct(),
    );

    on<TodoFromDatePickerChanged>(
      _onTodoFromDatePickerChanged,
      transformer: distinct(),
    );

    on<TodoToDatePickerChanged>(
      _onTodoToDatePickerChanged,
      transformer: distinct(),
    );

    on<TodoContentChanged>(
      _onTodoContentChanged,
      transformer: distinct(),
    );

    on<ReEntryCreateTodoButtonPressed>(
      _onReEntryCreateTodoButtonPressed,
      transformer: log(),
    );

    on<CreateTodoButtonPressed>(
      _onCreateTodoButtonPressed,
      transformer: distinct(),
    );

    on<PickerDateRangeTodoButtonPressed>(
      _onPickerDateRangeTodoButtonPressed,
      transformer: distinct(),
    );
  }

  final CreateTodoUseCase _createTodoUseCase;

  bool _isCreateJobButtonEnabled(String todoTitle, String todoContent, String fromDate, String toDate) {
    return todoTitle.isNotEmpty && todoContent.isNotEmpty && fromDate.isNotEmpty && toDate.isNotEmpty;
  }

  bool _isReEntryButtonEnabled(String todoTitle, String todoContent, String fromDate, String toDate) {
    return todoTitle.isNotEmpty && todoContent.isNotEmpty && fromDate.isNotEmpty && toDate.isNotEmpty;
  }

  void _onTodoTitleTextFieldChanged(TodoTitleTextFieldChanged event, Emitter<CreateTodoState> emit) {
    emit(state.copyWith(
      // depositAmount: event.depositAmount,
      // isReEntryButtonEnabled: _isReEntryButtonEnabled(event.depositAmount, state.depositTerm, state.interestRatePerYear),
      // isCalculateButtonEnabled: _isCalculateButtonEnabled(event.depositAmount, state.depositTerm, state.interestRatePerYear),
    ));
  }

  void _onTodoPriorityCheckBoxChanged(TodoPriorityCheckBoxChanged event, Emitter<CreateTodoState> emit) {
    emit(state.copyWith(
      // depositTerm: event.depositTerm,
      // isReEntryButtonEnabled: _isReEntryButtonEnabled(state.depositAmount, event.depositTerm, state.interestRatePerYear),
      // isCalculateButtonEnabled: _isCalculateButtonEnabled(state.depositAmount, event.depositTerm, state.interestRatePerYear),
    ));
  }

  void _onTodoFromDatePickerChanged(TodoFromDatePickerChanged event, Emitter<CreateTodoState> emit) {
    emit(state.copyWith(
      // interestRatePerYear: event.interestRatePerYear,
      // isReEntryButtonEnabled: _isReEntryButtonEnabled(state.depositAmount, state.depositTerm, event.interestRatePerYear),
      // isCalculateButtonEnabled: _isCalculateButtonEnabled(state.depositAmount, state.depositTerm, event.interestRatePerYear),
    ));
  }

  void _onTodoToDatePickerChanged(TodoToDatePickerChanged event, Emitter<CreateTodoState> emit) {
    emit(state.copyWith(
      // dateUnit: event.dateUnit,
    ));
  }

  void _onTodoContentChanged(TodoContentChanged event, Emitter<CreateTodoState> emit) {
    emit(state.copyWith(
      // dateUnit: event.dateUnit,
    ));
  }

  FutureOr<void> _onCreateTodoButtonPressed(CreateTodoButtonPressed event, Emitter<CreateTodoState> emit) {
    return runBlocCatching(
      action: () async {
        // final output = _calculateSavingUseCase.execute(
        //     CalculateSavingInput(
        //       depositAmount: state.depositAmount,
        //       depositTerm: state.depositTerm,
        //       interestRatePerYear: state.interestRatePerYear,
        //       dateUnit: state.dateUnit,
        //     )
        // );
        // emit(state.copyWith(
        //   totalInterestEndPeriod: output.totalInterestEndPeriod,
        //   totalInterestPrincipalEndPeriod: output.totalInterestPrincipalEndPeriod,
        // ));
      },
    );
  }

  FutureOr<void> _onReEntryCreateTodoButtonPressed(ReEntryCreateTodoButtonPressed event, Emitter<CreateTodoState> emit) {
    emit(state.copyWith(
      todoTitle: '',
      fromDate: '',
      toDate: '',
      priority: 0,
      isReEntryButtonEnabled: false,
      isCreateButtonEnabled: false,
      todoContent: '',
      todoLocation: '',
    ));
  }

  void _onPickerDateRangeTodoButtonPressed(PickerDateRangeTodoButtonPressed event, Emitter<CreateTodoState> emit) {
    emit(state.copyWith(
      selectedDate: event.selectedDate,
      dateCount: event.dateCount,
      range: event.range,
      rangeCount: event.rangeCount,
    ));
  }
}
