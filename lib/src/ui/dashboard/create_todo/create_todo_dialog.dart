import 'package:domain/domain.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:resources/resources.dart';
import 'package:base_architecture_flutter/src/app.dart';
import 'package:shared/shared.dart';
import 'package:intl/intl.dart';

class CreateTodoDialog extends StatefulWidget {
  const CreateTodoDialog({super.key});

  @override
  State<StatefulWidget> createState() {
    return _CreateTodoDialogDialogState();
  }
}

class _CreateTodoDialogDialogState extends BasePageState<CreateTodoDialog, CreateTodoBloc> {
  final TextEditingController ctrFromDate = TextEditingController();
  final TextEditingController ctrToDate = TextEditingController();
  final TextEditingController ctrInterestRatePerYear = TextEditingController();
  final TextEditingController ctrDate = TextEditingController();
  FocusNode focusNode = FocusNode();
  bool isSelectedHigh = false;
  bool isSelectedMedium = false;
  bool isSelectedLow = false;
  /// Calendar
  String _fromDate = '';
  String _toDate = '';
  String _selectedDate = '';
  String _dateCount = '';
  String _range = '';
  String _rangeCount = '';
  bool _isCalendarVisible = false;

  void _setCalendarVisible(visible) {
    setState(() {
      _isCalendarVisible = visible;
    });
  }

  /// The method for [DateRangePickerSelectionChanged] callback, which will be
  /// called whenever a selection changed on the date picker widget.
  void _onSelectionChanged(DateRangePickerSelectionChangedArgs args) {
    setState(() {
      if (args.value is PickerDateRange) {
        _range = '${DateFormat('dd/MM/yyyy').format(args.value.startDate)} -'
            ' ${DateFormat('dd/MM/yyyy').format(args.value.endDate ?? args.value.startDate)}';
        _fromDate = DateFormat('dd/MM/yyyy').format(args.value.startDate);
        _toDate = DateFormat('dd/MM/yyyy').format(args.value.endDate ?? args.value.startDate);
      } else if (args.value is DateTime) {
        _selectedDate = args.value.toString();
      } else if (args.value is List<DateTime>) {
        _dateCount = args.value.length.toString();
      } else {
        _rangeCount = args.value.length.toString();
      }
    });
  }

  @override
  void initState() {
    super.initState();
    focusNode = FocusNode();
  }

  @override
  void dispose() {
    focusNode.dispose();
    super.dispose();
  }

  @override
  Widget buildPage(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Container(
        alignment: Alignment.topCenter,
        padding: EdgeInsets.all(Dimens.d12.responsive()),
        margin: EdgeInsets.fromLTRB(
          Dimens.d180.responsive(),
          Dimens.d20.responsive(),
          Dimens.d180.responsive(),
          Dimens.d20.responsive(),
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: Colors.white,
        ),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      S.current.createTodo,
                      style: AppTextStyles.s14w600Tertiary(),
                    ),
                    InkWell(
                      onTap: () => navigator.pop(),
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Container(
                          margin: EdgeInsets.all(Dimens.d4.responsive()),
                          alignment: Alignment.centerRight,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: Assets.images.close.provider(),
                              fit: BoxFit.fitHeight,
                            ),
                          ),
                          child: SizedBox(
                            width: Dimens.d24.responsive(), height: Dimens.d24.responsive(),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                color: Colors.blueGrey.shade50,
                child: SizedBox(height: Dimens.d1.responsive()),
              ),
              SizedBox(height: Dimens.d16.responsive()),
              BlocBuilder<CreateTodoBloc, CreateTodoState>(
                builder: (context, state) {
                  return Material(
                    color: Colors.transparent,
                    child: AppTextFormField(
                      // controller: ctrDepositAmount,
                      label: S.current.toDoTitle,
                      labelStyle: AppTextStyles.s14w300Tertiary(),
                      style: AppTextStyles.s14w400Tertiary(),
                      hintStyle: AppTextStyles.s12w200Tertiary(),
                      hintText: S.current.toDoTitle,
                      onChanged: (depositAmount) {
                        // depositAmount = depositAmount.removeDots();
                        // bloc.add(DepositAmountTextFieldChanged(depositAmount: depositAmount));
                      },
                      textInputAction: TextInputAction.next,
                      onFieldSubmitted: (depositAmount){
                        focusNode.requestFocus();
                      },
                      onTapOutside: (event) {
                        FocusScope.of(context).unfocus();
                      },
                    ),
                  );
                },
              ),
              SizedBox(height: Dimens.d16.responsive()),
              Stack(
                children: [
                  Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              BlocBuilder<CreateTodoBloc, CreateTodoState>(
                                builder: (context, state) {
                                  return TextIconRightBorder(
                                    splashColor: Colors.orangeAccent,
                                    onTap: (){
                                      _setCalendarVisible(true);
                                    },
                                    child: TextRightIcon(
                                      color: Colors.orange,
                                      icon: Icons.calendar_month,
                                      text: _fromDate == '' ? DateFormat('dd/MM/yyyy').format(DateTime.now()) : _fromDate,
                                    ),
                                  );
                                },
                              ),
                              SizedBox(
                                width: Dimens.d16.responsive(),
                              ),
                              Text(
                                  S.current.toDate,
                                  style: AppTextStyles.s12w400Tertiary()
                              ),
                              SizedBox(
                                width: Dimens.d16.responsive(),
                              ),
                              TextIconRightBorder(
                                splashColor: Colors.orangeAccent,
                                onTap: (){
                                  _setCalendarVisible(true);
                                },
                                child: TextRightIcon(
                                  color: Colors.orange,
                                  icon: Icons.calendar_month,
                                  text: _toDate == '' ? DateFormat('dd/MM/yyyy').format(DateTime.now()) : _toDate,
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Text(
                                  '${S.current.priority}:',
                                  style: AppTextStyles.s12w400Tertiary()
                              ),
                              SizedBox(
                                width: Dimens.d16.responsive(),
                              ),
                              Column(
                                children: [
                                  IconButtonCustom(
                                    isSelected: isSelectedHigh,
                                    icon: const Icon(null),
                                    selectedIcon: const Icon(Icons.done_rounded),
                                    color: Colors.red,
                                    highlightColor: Colors.redAccent,
                                    onPressed: (){
                                      setState(() {
                                        isSelectedHigh = !isSelectedHigh;
                                      });
                                      if(isSelectedHigh == true){
                                        isSelectedMedium = false;
                                        isSelectedLow = false;
                                      }
                                    },
                                  ),
                                  Text(
                                    S.current.high, style: AppTextStyles.s10w400Tertiary(),
                                  ),
                                ],
                              ),
                              SizedBox(
                                width: Dimens.d8.responsive(),
                              ),
                              Column(
                                children: [
                                  IconButtonCustom(
                                    isSelected: isSelectedMedium,
                                    icon: const Icon(null),
                                    selectedIcon: const Icon(Icons.done_rounded),
                                    color: Colors.orange,
                                    highlightColor: Colors.orangeAccent,
                                    onPressed: (){
                                      setState(() {
                                        isSelectedMedium = !isSelectedMedium;
                                      });
                                      if(isSelectedMedium == true){
                                        isSelectedHigh = false;
                                        isSelectedLow = false;
                                      }
                                    },
                                  ),
                                  Text(
                                    S.current.medium, style: AppTextStyles.s10w400Tertiary(),
                                  ),
                                ],
                              ),
                              SizedBox(
                                width: Dimens.d8.responsive(),
                              ),
                              Column(
                                children: [
                                  IconButtonCustom(
                                    isSelected: isSelectedLow,
                                    icon: const Icon(null),
                                    selectedIcon: const Icon(Icons.done_rounded),
                                    color: Colors.green,
                                    highlightColor: Colors.greenAccent,
                                    onPressed: (){
                                      setState(() {
                                        isSelectedLow = !isSelectedLow;
                                      });
                                      if(isSelectedLow == true){
                                        isSelectedHigh = false;
                                        isSelectedMedium = false;
                                      }
                                    },
                                  ),
                                  Text(
                                    S.current.low, style: AppTextStyles.s10w400Tertiary(),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                      SizedBox(height: Dimens.d16.responsive()),
                      BlocBuilder<CreateTodoBloc, CreateTodoState>(
                        builder: (context, state) {
                          return Material(
                            color: Colors.transparent,
                            child: AppTextFormField(
                              controller: ctrInterestRatePerYear,
                              label: S.current.toDoContent,
                              labelStyle: AppTextStyles.s14w300Tertiary(),
                              style: AppTextStyles.s14w400Tertiary(),
                              hintStyle: AppTextStyles.s12w200Tertiary(),
                              hintText: S.current.toDoContent,
                              onChanged: (interestRatePerYear) {
                                // interestRatePerYear = interestRatePerYear.removePercent();
                                // bloc.add(InterestRatePerYearTextFieldChanged(interestRatePerYear: interestRatePerYear));
                              },
                              keyboardType: TextInputType.multiline,
                              maxLines: 9,
                              textInputAction: TextInputAction.newline,
                              onFieldSubmitted: (_) {
                                focusNode.requestFocus();
                              },
                              onTapOutside: (event) {
                                FocusScope.of(context).unfocus();
                              },
                            ),
                          );
                        },
                      ),
                      SizedBox(height: Dimens.d16.responsive()),
                      Container(
                        margin: EdgeInsets.only(top: Dimens.d20.responsive()),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            BlocBuilder<CreateTodoBloc, CreateTodoState>(
                              buildWhen: (previous, current) =>
                              previous.isReEntryButtonEnabled != current.isReEntryButtonEnabled,
                              builder: (context, state) {
                                return ElevatedButton(
                                  onPressed: () {
                                    if (state.isReEntryButtonEnabled) {
                                      // bloc.add(const ReEntryButtonPressed());
                                      // ctrDepositAmount.clear();
                                      // ctrDepositTerm.clear();
                                      // ctrInterestRatePerYear.clear();
                                      // ctrDate.text = S.current.day;
                                    }
                                  },
                                  style: ButtonStyle(
                                    backgroundColor: MaterialStateProperty.all(AppColors.current.primaryColor
                                        .withOpacity(state.isReEntryButtonEnabled ? 1 : 0.5)),
                                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(6.0),
                                      ),
                                    ),
                                    padding: MaterialStateProperty.all<EdgeInsets>(
                                      EdgeInsets.fromLTRB(
                                        Dimens.d60.responsive(),
                                        Dimens.d4.responsive(),
                                        Dimens.d60.responsive(),
                                        Dimens.d4.responsive(),
                                      ),
                                    ),
                                  ),
                                  child: Text(
                                    S.current.retype,
                                    style: AppTextStyles.s14w400Primary(),
                                  ),
                                );
                              },
                            ),
                            BlocBuilder<CreateTodoBloc, CreateTodoState>(
                              buildWhen: (previous, current) =>
                              previous.isCreateButtonEnabled != current.isCreateButtonEnabled,
                              builder: (context, state) {
                                return ElevatedButton(
                                  onPressed: () {
                                    if (state.isCreateButtonEnabled) {
                                      // bloc.add(CalculateButtonPressed(
                                      //   depositAmount: ctrDepositAmount.value.text,
                                      //   depositTerm: ctrDepositTerm.value.text,
                                      //   interestRatePerYear: ctrInterestRatePerYear.value.text,
                                      //   dateUnit: ctrDate.value.text,
                                      // ));
                                    }
                                  },
                                  style: ButtonStyle(
                                    backgroundColor: MaterialStateProperty.all(AppColors.current.primaryColor
                                        .withOpacity(state.isCreateButtonEnabled ? 1 : 0.5)),
                                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(6.0),
                                      ),
                                    ),
                                    padding: MaterialStateProperty.all<EdgeInsets>(
                                      EdgeInsets.fromLTRB(
                                        Dimens.d60.responsive(),
                                        Dimens.d4.responsive(),
                                        Dimens.d60.responsive(),
                                        Dimens.d4.responsive(),
                                      ),
                                    ),
                                  ),
                                  child: Text(
                                    S.current.createTodo,
                                    style: AppTextStyles.s14w400Primary(),
                                  ),
                                );
                              },
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Visibility(
                    visible: _isCalendarVisible,
                    child:  Material(
                      color: Colors.transparent,
                      child: Container(
                        color: Colors.white,
                        margin: EdgeInsets.fromLTRB(
                          Dimens.d0.responsive(),
                          Dimens.d42.responsive(),
                          Dimens.d440.responsive(),
                          Dimens.d0.responsive(),
                        ),
                        child: Column(
                          children: [
                            SfDateRangePicker(
                              onSelectionChanged: _onSelectionChanged,
                              selectionMode: DateRangePickerSelectionMode.range,
                              initialSelectedRange: PickerDateRange(
                                  DateTime.now().subtract(const Duration(days: 4)),
                                  DateTime.now().add(const Duration(days: 3))),
                              rangeSelectionColor: Colors.orangeAccent,
                              startRangeSelectionColor: Colors.orange,
                              endRangeSelectionColor: Colors.orange,
                              backgroundColor: Colors.orange.shade50,
                              todayHighlightColor: Colors.orange,
                              headerStyle: DateRangePickerHeaderStyle(
                                backgroundColor: Colors.orange.shade50,
                              ),
                            ),
                            SizedBox(height: Dimens.d4.responsive()),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                ElevatedButton(
                                  onPressed: () {
                                    _setCalendarVisible(false);
                                  },
                                  style: ButtonStyle(
                                    backgroundColor: MaterialStateProperty.all(AppColors.current.primaryColor),
                                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(6.0),
                                      ),
                                    ),
                                    padding: MaterialStateProperty.all<EdgeInsets>(
                                      EdgeInsets.fromLTRB(
                                        Dimens.d34.responsive(),
                                        Dimens.d4.responsive(),
                                        Dimens.d34.responsive(),
                                        Dimens.d4.responsive(),
                                      ),
                                    ),
                                  ),
                                  child: Text(
                                    S.current.cancel,
                                    style: AppTextStyles.s14w400Primary(),
                                  ),
                                ),
                                BlocBuilder<CreateTodoBloc, CreateTodoState>(
                                  builder: (context, state) {
                                    return ElevatedButton(
                                      onPressed: () {
                                        _setCalendarVisible(false);
                                      },
                                      style: ButtonStyle(
                                        backgroundColor: MaterialStateProperty.all(AppColors.current.primaryColor),
                                        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(6.0),
                                          ),
                                        ),
                                        padding: MaterialStateProperty.all<EdgeInsets>(
                                          EdgeInsets.fromLTRB(
                                            Dimens.d20.responsive(),
                                            Dimens.d4.responsive(),
                                            Dimens.d20.responsive(),
                                            Dimens.d4.responsive(),
                                          ),
                                        ),
                                      ),
                                      child: Text(
                                        S.current.confirm,
                                        style: AppTextStyles.s14w400Primary(),
                                      ),
                                    );
                                  },
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
