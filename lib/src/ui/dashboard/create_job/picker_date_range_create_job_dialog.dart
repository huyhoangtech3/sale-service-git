import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:resources/resources.dart';
import 'package:base_architecture_flutter/src/app.dart';
import 'package:intl/intl.dart';
import 'package:shared/shared.dart';

class PickerDateRangeDialog extends StatefulWidget {
  const PickerDateRangeDialog({super.key});

  @override
  State<StatefulWidget> createState() {
    return _PickerDateRangeDialogState();
  }
}

class _PickerDateRangeDialogState extends BasePageState<PickerDateRangeDialog, CreateJobBloc> {
  String _selectedDate = '';
  String _dateCount = '';
  String _range = '';
  String _rangeCount = '';
  String _fromDate = '';
  String _toDate = '';

  /// The method for [DateRangePickerSelectionChanged] callback, which will be
  /// called whenever a selection changed on the date picker widget.
  void _onSelectionChanged(DateRangePickerSelectionChangedArgs args) {
    setState(() {
      if (args.value is PickerDateRange) {
        _range = '${DateFormat('dd/MM/yyyy').format(args.value.startDate)} -'
            ' ${DateFormat('dd/MM/yyyy').format(args.value.endDate ?? args.value.startDate)}';
        _fromDate = DateFormat('dd/MM/yyyy').format(args.value.startDate);
        _toDate = DateFormat('dd/MM/yyyy').format(args.value.endDate ?? args.value.startDate);
      } else if (args.value is DateTime) {
        _selectedDate = args.value.toString();
      } else if (args.value is List<DateTime>) {
        _dateCount = args.value.length.toString();
      } else {
        _rangeCount = args.value.length.toString();
      }
    });
  }

  @override
  Widget buildPage(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Container(
        color: Colors.white,
        margin: EdgeInsets.fromLTRB(
          Dimens.d192.responsive(),
          Dimens.d254.responsive(),
          Dimens.d630.responsive(),
          Dimens.d74.responsive(),
        ),
        child: Column(
          children: [
            SfDateRangePicker(
              onSelectionChanged: _onSelectionChanged,
              selectionMode: DateRangePickerSelectionMode.range,
              initialSelectedRange: PickerDateRange(
                  DateTime.now().subtract(const Duration(days: 4)),
                  DateTime.now().add(const Duration(days: 3))),
            ),
            SizedBox(height: Dimens.d4.responsive()),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ElevatedButton(
                  onPressed: () {
                    navigator.pop();
                  },
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(AppColors.current.primaryColor),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(6.0),
                      ),
                    ),
                    padding: MaterialStateProperty.all<EdgeInsets>(
                      EdgeInsets.fromLTRB(
                        Dimens.d34.responsive(),
                        Dimens.d4.responsive(),
                        Dimens.d34.responsive(),
                        Dimens.d4.responsive(),
                      ),
                    ),
                  ),
                  child: Text(
                    S.current.cancel,
                    style: AppTextStyles.s14w400Primary(),
                  ),
                ),
                BlocBuilder<CreateJobBloc, CreateJobState>(
                  builder: (context, state) {
                    return ElevatedButton(
                      onPressed: () {
                        bloc.add(PickerDateRangeButtonPressed(
                          selectedDate: _selectedDate,
                          dateCount: _dateCount,
                          range: _range,
                          rangeCount: _rangeCount,
                        ));
                        navigator.pop();
                      },
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(AppColors.current.primaryColor),
                        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(6.0),
                          ),
                        ),
                        padding: MaterialStateProperty.all<EdgeInsets>(
                          EdgeInsets.fromLTRB(
                            Dimens.d20.responsive(),
                            Dimens.d4.responsive(),
                            Dimens.d20.responsive(),
                            Dimens.d4.responsive(),
                          ),
                        ),
                      ),
                      child: Text(
                        S.current.confirm,
                        style: AppTextStyles.s14w400Primary(),
                      ),
                    );
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
