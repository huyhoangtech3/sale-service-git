import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:base_architecture_flutter/src/base/bloc/base_bloc_state.dart';
part 'create_job_state.freezed.dart';

@freezed
class CreateJobState extends BaseBlocState with _$CreateJobState {
  const factory CreateJobState({
    @Default('') String jobTitle,
    @Default('') String fromDate,
    @Default('') String toDate,
    @Default(0) int priority,
    @Default(false) bool isCreateJobButtonEnabled,
    @Default(false) bool isReEntryButtonEnabled,
    @Default('') String jobContent,
    @Default('') String jobLocation,
    @Default('') String selectedDate,
    @Default('') String dateCount,
    @Default('') String range,
    @Default('') String rangeCount,
  }) = _CreateJobState;
}
