// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'create_job_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$CreateJobState {
  String get jobTitle => throw _privateConstructorUsedError;
  String get fromDate => throw _privateConstructorUsedError;
  String get toDate => throw _privateConstructorUsedError;
  int get priority => throw _privateConstructorUsedError;
  bool get isCreateJobButtonEnabled => throw _privateConstructorUsedError;
  bool get isReEntryButtonEnabled => throw _privateConstructorUsedError;
  String get jobContent => throw _privateConstructorUsedError;
  String get jobLocation => throw _privateConstructorUsedError;
  String get selectedDate => throw _privateConstructorUsedError;
  String get dateCount => throw _privateConstructorUsedError;
  String get range => throw _privateConstructorUsedError;
  String get rangeCount => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CreateJobStateCopyWith<CreateJobState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CreateJobStateCopyWith<$Res> {
  factory $CreateJobStateCopyWith(
          CreateJobState value, $Res Function(CreateJobState) then) =
      _$CreateJobStateCopyWithImpl<$Res, CreateJobState>;
  @useResult
  $Res call(
      {String jobTitle,
      String fromDate,
      String toDate,
      int priority,
      bool isCreateJobButtonEnabled,
      bool isReEntryButtonEnabled,
      String jobContent,
      String jobLocation,
      String selectedDate,
      String dateCount,
      String range,
      String rangeCount});
}

/// @nodoc
class _$CreateJobStateCopyWithImpl<$Res, $Val extends CreateJobState>
    implements $CreateJobStateCopyWith<$Res> {
  _$CreateJobStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? jobTitle = null,
    Object? fromDate = null,
    Object? toDate = null,
    Object? priority = null,
    Object? isCreateJobButtonEnabled = null,
    Object? isReEntryButtonEnabled = null,
    Object? jobContent = null,
    Object? jobLocation = null,
    Object? selectedDate = null,
    Object? dateCount = null,
    Object? range = null,
    Object? rangeCount = null,
  }) {
    return _then(_value.copyWith(
      jobTitle: null == jobTitle
          ? _value.jobTitle
          : jobTitle // ignore: cast_nullable_to_non_nullable
              as String,
      fromDate: null == fromDate
          ? _value.fromDate
          : fromDate // ignore: cast_nullable_to_non_nullable
              as String,
      toDate: null == toDate
          ? _value.toDate
          : toDate // ignore: cast_nullable_to_non_nullable
              as String,
      priority: null == priority
          ? _value.priority
          : priority // ignore: cast_nullable_to_non_nullable
              as int,
      isCreateJobButtonEnabled: null == isCreateJobButtonEnabled
          ? _value.isCreateJobButtonEnabled
          : isCreateJobButtonEnabled // ignore: cast_nullable_to_non_nullable
              as bool,
      isReEntryButtonEnabled: null == isReEntryButtonEnabled
          ? _value.isReEntryButtonEnabled
          : isReEntryButtonEnabled // ignore: cast_nullable_to_non_nullable
              as bool,
      jobContent: null == jobContent
          ? _value.jobContent
          : jobContent // ignore: cast_nullable_to_non_nullable
              as String,
      jobLocation: null == jobLocation
          ? _value.jobLocation
          : jobLocation // ignore: cast_nullable_to_non_nullable
              as String,
      selectedDate: null == selectedDate
          ? _value.selectedDate
          : selectedDate // ignore: cast_nullable_to_non_nullable
              as String,
      dateCount: null == dateCount
          ? _value.dateCount
          : dateCount // ignore: cast_nullable_to_non_nullable
              as String,
      range: null == range
          ? _value.range
          : range // ignore: cast_nullable_to_non_nullable
              as String,
      rangeCount: null == rangeCount
          ? _value.rangeCount
          : rangeCount // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$CreateJobStateImplCopyWith<$Res>
    implements $CreateJobStateCopyWith<$Res> {
  factory _$$CreateJobStateImplCopyWith(_$CreateJobStateImpl value,
          $Res Function(_$CreateJobStateImpl) then) =
      __$$CreateJobStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String jobTitle,
      String fromDate,
      String toDate,
      int priority,
      bool isCreateJobButtonEnabled,
      bool isReEntryButtonEnabled,
      String jobContent,
      String jobLocation,
      String selectedDate,
      String dateCount,
      String range,
      String rangeCount});
}

/// @nodoc
class __$$CreateJobStateImplCopyWithImpl<$Res>
    extends _$CreateJobStateCopyWithImpl<$Res, _$CreateJobStateImpl>
    implements _$$CreateJobStateImplCopyWith<$Res> {
  __$$CreateJobStateImplCopyWithImpl(
      _$CreateJobStateImpl _value, $Res Function(_$CreateJobStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? jobTitle = null,
    Object? fromDate = null,
    Object? toDate = null,
    Object? priority = null,
    Object? isCreateJobButtonEnabled = null,
    Object? isReEntryButtonEnabled = null,
    Object? jobContent = null,
    Object? jobLocation = null,
    Object? selectedDate = null,
    Object? dateCount = null,
    Object? range = null,
    Object? rangeCount = null,
  }) {
    return _then(_$CreateJobStateImpl(
      jobTitle: null == jobTitle
          ? _value.jobTitle
          : jobTitle // ignore: cast_nullable_to_non_nullable
              as String,
      fromDate: null == fromDate
          ? _value.fromDate
          : fromDate // ignore: cast_nullable_to_non_nullable
              as String,
      toDate: null == toDate
          ? _value.toDate
          : toDate // ignore: cast_nullable_to_non_nullable
              as String,
      priority: null == priority
          ? _value.priority
          : priority // ignore: cast_nullable_to_non_nullable
              as int,
      isCreateJobButtonEnabled: null == isCreateJobButtonEnabled
          ? _value.isCreateJobButtonEnabled
          : isCreateJobButtonEnabled // ignore: cast_nullable_to_non_nullable
              as bool,
      isReEntryButtonEnabled: null == isReEntryButtonEnabled
          ? _value.isReEntryButtonEnabled
          : isReEntryButtonEnabled // ignore: cast_nullable_to_non_nullable
              as bool,
      jobContent: null == jobContent
          ? _value.jobContent
          : jobContent // ignore: cast_nullable_to_non_nullable
              as String,
      jobLocation: null == jobLocation
          ? _value.jobLocation
          : jobLocation // ignore: cast_nullable_to_non_nullable
              as String,
      selectedDate: null == selectedDate
          ? _value.selectedDate
          : selectedDate // ignore: cast_nullable_to_non_nullable
              as String,
      dateCount: null == dateCount
          ? _value.dateCount
          : dateCount // ignore: cast_nullable_to_non_nullable
              as String,
      range: null == range
          ? _value.range
          : range // ignore: cast_nullable_to_non_nullable
              as String,
      rangeCount: null == rangeCount
          ? _value.rangeCount
          : rangeCount // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$CreateJobStateImpl implements _CreateJobState {
  const _$CreateJobStateImpl(
      {this.jobTitle = '',
      this.fromDate = '',
      this.toDate = '',
      this.priority = 0,
      this.isCreateJobButtonEnabled = false,
      this.isReEntryButtonEnabled = false,
      this.jobContent = '',
      this.jobLocation = '',
      this.selectedDate = '',
      this.dateCount = '',
      this.range = '',
      this.rangeCount = ''});

  @override
  @JsonKey()
  final String jobTitle;
  @override
  @JsonKey()
  final String fromDate;
  @override
  @JsonKey()
  final String toDate;
  @override
  @JsonKey()
  final int priority;
  @override
  @JsonKey()
  final bool isCreateJobButtonEnabled;
  @override
  @JsonKey()
  final bool isReEntryButtonEnabled;
  @override
  @JsonKey()
  final String jobContent;
  @override
  @JsonKey()
  final String jobLocation;
  @override
  @JsonKey()
  final String selectedDate;
  @override
  @JsonKey()
  final String dateCount;
  @override
  @JsonKey()
  final String range;
  @override
  @JsonKey()
  final String rangeCount;

  @override
  String toString() {
    return 'CreateJobState(jobTitle: $jobTitle, fromDate: $fromDate, toDate: $toDate, priority: $priority, isCreateJobButtonEnabled: $isCreateJobButtonEnabled, isReEntryButtonEnabled: $isReEntryButtonEnabled, jobContent: $jobContent, jobLocation: $jobLocation, selectedDate: $selectedDate, dateCount: $dateCount, range: $range, rangeCount: $rangeCount)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CreateJobStateImpl &&
            (identical(other.jobTitle, jobTitle) ||
                other.jobTitle == jobTitle) &&
            (identical(other.fromDate, fromDate) ||
                other.fromDate == fromDate) &&
            (identical(other.toDate, toDate) || other.toDate == toDate) &&
            (identical(other.priority, priority) ||
                other.priority == priority) &&
            (identical(
                    other.isCreateJobButtonEnabled, isCreateJobButtonEnabled) ||
                other.isCreateJobButtonEnabled == isCreateJobButtonEnabled) &&
            (identical(other.isReEntryButtonEnabled, isReEntryButtonEnabled) ||
                other.isReEntryButtonEnabled == isReEntryButtonEnabled) &&
            (identical(other.jobContent, jobContent) ||
                other.jobContent == jobContent) &&
            (identical(other.jobLocation, jobLocation) ||
                other.jobLocation == jobLocation) &&
            (identical(other.selectedDate, selectedDate) ||
                other.selectedDate == selectedDate) &&
            (identical(other.dateCount, dateCount) ||
                other.dateCount == dateCount) &&
            (identical(other.range, range) || other.range == range) &&
            (identical(other.rangeCount, rangeCount) ||
                other.rangeCount == rangeCount));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      jobTitle,
      fromDate,
      toDate,
      priority,
      isCreateJobButtonEnabled,
      isReEntryButtonEnabled,
      jobContent,
      jobLocation,
      selectedDate,
      dateCount,
      range,
      rangeCount);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CreateJobStateImplCopyWith<_$CreateJobStateImpl> get copyWith =>
      __$$CreateJobStateImplCopyWithImpl<_$CreateJobStateImpl>(
          this, _$identity);
}

abstract class _CreateJobState implements CreateJobState {
  const factory _CreateJobState(
      {final String jobTitle,
      final String fromDate,
      final String toDate,
      final int priority,
      final bool isCreateJobButtonEnabled,
      final bool isReEntryButtonEnabled,
      final String jobContent,
      final String jobLocation,
      final String selectedDate,
      final String dateCount,
      final String range,
      final String rangeCount}) = _$CreateJobStateImpl;

  @override
  String get jobTitle;
  @override
  String get fromDate;
  @override
  String get toDate;
  @override
  int get priority;
  @override
  bool get isCreateJobButtonEnabled;
  @override
  bool get isReEntryButtonEnabled;
  @override
  String get jobContent;
  @override
  String get jobLocation;
  @override
  String get selectedDate;
  @override
  String get dateCount;
  @override
  String get range;
  @override
  String get rangeCount;
  @override
  @JsonKey(ignore: true)
  _$$CreateJobStateImplCopyWith<_$CreateJobStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
