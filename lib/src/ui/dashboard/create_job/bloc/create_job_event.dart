import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:base_architecture_flutter/src/base/bloc/base_bloc_event.dart';
part 'create_job_event.freezed.dart';

abstract class CreateJobEvent extends BaseBlocEvent {
  const CreateJobEvent();
}

@freezed
class JobTitleTextFieldChanged extends CreateJobEvent with _$JobTitleTextFieldChanged {
  const factory JobTitleTextFieldChanged({
    required String jobTitle,
  }) = _JobTitleTextFieldChanged;
}

@freezed
class PriorityCheckBoxChanged extends CreateJobEvent with _$PriorityCheckBoxChanged {
  const factory PriorityCheckBoxChanged({
    required String priority,
  }) = _PriorityCheckBoxChanged;
}

@freezed
class FromDatePickerChanged extends CreateJobEvent with _$FromDatePickerChanged {
  const factory FromDatePickerChanged({
    required String fromDatePicker,
  }) = _FromDatePickerChanged;
}

@freezed
class ToDatePickerChanged extends CreateJobEvent with _$ToDatePickerChanged {
  const factory ToDatePickerChanged({
    required String toDatePicker,
  }) = _ToDatePickerChanged;
}

@freezed
class JobContentChanged extends CreateJobEvent with _$JobContentChanged {
  const factory JobContentChanged({
    required String jobContent,
  }) = _JobContentChanged;
}

@freezed
class ReEntryCreateJobButtonPressed extends CreateJobEvent with _$ReEntryCreateJobButtonPressed {
  const factory ReEntryCreateJobButtonPressed() = _ReEntryCreateJobButtonPressed;
}

@freezed
class CreateJobButtonPressed extends CreateJobEvent with _$CreateJobButtonPressed {
  const factory CreateJobButtonPressed({
    required String jobTitle,
    required String priority,
    required String fromDatePicker,
    required String toDatePicker,
    required String jobContent,
  }) = _CreateJobButtonPressed;
}

@freezed
class PickerDateRangeButtonPressed extends CreateJobEvent with _$PickerDateRangeButtonPressed {
  const factory PickerDateRangeButtonPressed({
    required String selectedDate,
    required String dateCount,
    required String range,
    required String rangeCount,
  }) = _PickerDateRangeButtonPressed;
}