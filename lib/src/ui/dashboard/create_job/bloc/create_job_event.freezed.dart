// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'create_job_event.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$JobTitleTextFieldChanged {
  String get jobTitle => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $JobTitleTextFieldChangedCopyWith<JobTitleTextFieldChanged> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $JobTitleTextFieldChangedCopyWith<$Res> {
  factory $JobTitleTextFieldChangedCopyWith(JobTitleTextFieldChanged value,
          $Res Function(JobTitleTextFieldChanged) then) =
      _$JobTitleTextFieldChangedCopyWithImpl<$Res, JobTitleTextFieldChanged>;
  @useResult
  $Res call({String jobTitle});
}

/// @nodoc
class _$JobTitleTextFieldChangedCopyWithImpl<$Res,
        $Val extends JobTitleTextFieldChanged>
    implements $JobTitleTextFieldChangedCopyWith<$Res> {
  _$JobTitleTextFieldChangedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? jobTitle = null,
  }) {
    return _then(_value.copyWith(
      jobTitle: null == jobTitle
          ? _value.jobTitle
          : jobTitle // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$JobTitleTextFieldChangedImplCopyWith<$Res>
    implements $JobTitleTextFieldChangedCopyWith<$Res> {
  factory _$$JobTitleTextFieldChangedImplCopyWith(
          _$JobTitleTextFieldChangedImpl value,
          $Res Function(_$JobTitleTextFieldChangedImpl) then) =
      __$$JobTitleTextFieldChangedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String jobTitle});
}

/// @nodoc
class __$$JobTitleTextFieldChangedImplCopyWithImpl<$Res>
    extends _$JobTitleTextFieldChangedCopyWithImpl<$Res,
        _$JobTitleTextFieldChangedImpl>
    implements _$$JobTitleTextFieldChangedImplCopyWith<$Res> {
  __$$JobTitleTextFieldChangedImplCopyWithImpl(
      _$JobTitleTextFieldChangedImpl _value,
      $Res Function(_$JobTitleTextFieldChangedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? jobTitle = null,
  }) {
    return _then(_$JobTitleTextFieldChangedImpl(
      jobTitle: null == jobTitle
          ? _value.jobTitle
          : jobTitle // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$JobTitleTextFieldChangedImpl implements _JobTitleTextFieldChanged {
  const _$JobTitleTextFieldChangedImpl({required this.jobTitle});

  @override
  final String jobTitle;

  @override
  String toString() {
    return 'JobTitleTextFieldChanged(jobTitle: $jobTitle)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$JobTitleTextFieldChangedImpl &&
            (identical(other.jobTitle, jobTitle) ||
                other.jobTitle == jobTitle));
  }

  @override
  int get hashCode => Object.hash(runtimeType, jobTitle);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$JobTitleTextFieldChangedImplCopyWith<_$JobTitleTextFieldChangedImpl>
      get copyWith => __$$JobTitleTextFieldChangedImplCopyWithImpl<
          _$JobTitleTextFieldChangedImpl>(this, _$identity);
}

abstract class _JobTitleTextFieldChanged implements JobTitleTextFieldChanged {
  const factory _JobTitleTextFieldChanged({required final String jobTitle}) =
      _$JobTitleTextFieldChangedImpl;

  @override
  String get jobTitle;
  @override
  @JsonKey(ignore: true)
  _$$JobTitleTextFieldChangedImplCopyWith<_$JobTitleTextFieldChangedImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$PriorityCheckBoxChanged {
  String get priority => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $PriorityCheckBoxChangedCopyWith<PriorityCheckBoxChanged> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PriorityCheckBoxChangedCopyWith<$Res> {
  factory $PriorityCheckBoxChangedCopyWith(PriorityCheckBoxChanged value,
          $Res Function(PriorityCheckBoxChanged) then) =
      _$PriorityCheckBoxChangedCopyWithImpl<$Res, PriorityCheckBoxChanged>;
  @useResult
  $Res call({String priority});
}

/// @nodoc
class _$PriorityCheckBoxChangedCopyWithImpl<$Res,
        $Val extends PriorityCheckBoxChanged>
    implements $PriorityCheckBoxChangedCopyWith<$Res> {
  _$PriorityCheckBoxChangedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? priority = null,
  }) {
    return _then(_value.copyWith(
      priority: null == priority
          ? _value.priority
          : priority // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$PriorityCheckBoxChangedImplCopyWith<$Res>
    implements $PriorityCheckBoxChangedCopyWith<$Res> {
  factory _$$PriorityCheckBoxChangedImplCopyWith(
          _$PriorityCheckBoxChangedImpl value,
          $Res Function(_$PriorityCheckBoxChangedImpl) then) =
      __$$PriorityCheckBoxChangedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String priority});
}

/// @nodoc
class __$$PriorityCheckBoxChangedImplCopyWithImpl<$Res>
    extends _$PriorityCheckBoxChangedCopyWithImpl<$Res,
        _$PriorityCheckBoxChangedImpl>
    implements _$$PriorityCheckBoxChangedImplCopyWith<$Res> {
  __$$PriorityCheckBoxChangedImplCopyWithImpl(
      _$PriorityCheckBoxChangedImpl _value,
      $Res Function(_$PriorityCheckBoxChangedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? priority = null,
  }) {
    return _then(_$PriorityCheckBoxChangedImpl(
      priority: null == priority
          ? _value.priority
          : priority // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$PriorityCheckBoxChangedImpl implements _PriorityCheckBoxChanged {
  const _$PriorityCheckBoxChangedImpl({required this.priority});

  @override
  final String priority;

  @override
  String toString() {
    return 'PriorityCheckBoxChanged(priority: $priority)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$PriorityCheckBoxChangedImpl &&
            (identical(other.priority, priority) ||
                other.priority == priority));
  }

  @override
  int get hashCode => Object.hash(runtimeType, priority);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$PriorityCheckBoxChangedImplCopyWith<_$PriorityCheckBoxChangedImpl>
      get copyWith => __$$PriorityCheckBoxChangedImplCopyWithImpl<
          _$PriorityCheckBoxChangedImpl>(this, _$identity);
}

abstract class _PriorityCheckBoxChanged implements PriorityCheckBoxChanged {
  const factory _PriorityCheckBoxChanged({required final String priority}) =
      _$PriorityCheckBoxChangedImpl;

  @override
  String get priority;
  @override
  @JsonKey(ignore: true)
  _$$PriorityCheckBoxChangedImplCopyWith<_$PriorityCheckBoxChangedImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$FromDatePickerChanged {
  String get fromDatePicker => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $FromDatePickerChangedCopyWith<FromDatePickerChanged> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FromDatePickerChangedCopyWith<$Res> {
  factory $FromDatePickerChangedCopyWith(FromDatePickerChanged value,
          $Res Function(FromDatePickerChanged) then) =
      _$FromDatePickerChangedCopyWithImpl<$Res, FromDatePickerChanged>;
  @useResult
  $Res call({String fromDatePicker});
}

/// @nodoc
class _$FromDatePickerChangedCopyWithImpl<$Res,
        $Val extends FromDatePickerChanged>
    implements $FromDatePickerChangedCopyWith<$Res> {
  _$FromDatePickerChangedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? fromDatePicker = null,
  }) {
    return _then(_value.copyWith(
      fromDatePicker: null == fromDatePicker
          ? _value.fromDatePicker
          : fromDatePicker // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$FromDatePickerChangedImplCopyWith<$Res>
    implements $FromDatePickerChangedCopyWith<$Res> {
  factory _$$FromDatePickerChangedImplCopyWith(
          _$FromDatePickerChangedImpl value,
          $Res Function(_$FromDatePickerChangedImpl) then) =
      __$$FromDatePickerChangedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String fromDatePicker});
}

/// @nodoc
class __$$FromDatePickerChangedImplCopyWithImpl<$Res>
    extends _$FromDatePickerChangedCopyWithImpl<$Res,
        _$FromDatePickerChangedImpl>
    implements _$$FromDatePickerChangedImplCopyWith<$Res> {
  __$$FromDatePickerChangedImplCopyWithImpl(_$FromDatePickerChangedImpl _value,
      $Res Function(_$FromDatePickerChangedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? fromDatePicker = null,
  }) {
    return _then(_$FromDatePickerChangedImpl(
      fromDatePicker: null == fromDatePicker
          ? _value.fromDatePicker
          : fromDatePicker // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$FromDatePickerChangedImpl implements _FromDatePickerChanged {
  const _$FromDatePickerChangedImpl({required this.fromDatePicker});

  @override
  final String fromDatePicker;

  @override
  String toString() {
    return 'FromDatePickerChanged(fromDatePicker: $fromDatePicker)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$FromDatePickerChangedImpl &&
            (identical(other.fromDatePicker, fromDatePicker) ||
                other.fromDatePicker == fromDatePicker));
  }

  @override
  int get hashCode => Object.hash(runtimeType, fromDatePicker);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$FromDatePickerChangedImplCopyWith<_$FromDatePickerChangedImpl>
      get copyWith => __$$FromDatePickerChangedImplCopyWithImpl<
          _$FromDatePickerChangedImpl>(this, _$identity);
}

abstract class _FromDatePickerChanged implements FromDatePickerChanged {
  const factory _FromDatePickerChanged({required final String fromDatePicker}) =
      _$FromDatePickerChangedImpl;

  @override
  String get fromDatePicker;
  @override
  @JsonKey(ignore: true)
  _$$FromDatePickerChangedImplCopyWith<_$FromDatePickerChangedImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$ToDatePickerChanged {
  String get toDatePicker => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ToDatePickerChangedCopyWith<ToDatePickerChanged> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ToDatePickerChangedCopyWith<$Res> {
  factory $ToDatePickerChangedCopyWith(
          ToDatePickerChanged value, $Res Function(ToDatePickerChanged) then) =
      _$ToDatePickerChangedCopyWithImpl<$Res, ToDatePickerChanged>;
  @useResult
  $Res call({String toDatePicker});
}

/// @nodoc
class _$ToDatePickerChangedCopyWithImpl<$Res, $Val extends ToDatePickerChanged>
    implements $ToDatePickerChangedCopyWith<$Res> {
  _$ToDatePickerChangedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? toDatePicker = null,
  }) {
    return _then(_value.copyWith(
      toDatePicker: null == toDatePicker
          ? _value.toDatePicker
          : toDatePicker // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ToDatePickerChangedImplCopyWith<$Res>
    implements $ToDatePickerChangedCopyWith<$Res> {
  factory _$$ToDatePickerChangedImplCopyWith(_$ToDatePickerChangedImpl value,
          $Res Function(_$ToDatePickerChangedImpl) then) =
      __$$ToDatePickerChangedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String toDatePicker});
}

/// @nodoc
class __$$ToDatePickerChangedImplCopyWithImpl<$Res>
    extends _$ToDatePickerChangedCopyWithImpl<$Res, _$ToDatePickerChangedImpl>
    implements _$$ToDatePickerChangedImplCopyWith<$Res> {
  __$$ToDatePickerChangedImplCopyWithImpl(_$ToDatePickerChangedImpl _value,
      $Res Function(_$ToDatePickerChangedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? toDatePicker = null,
  }) {
    return _then(_$ToDatePickerChangedImpl(
      toDatePicker: null == toDatePicker
          ? _value.toDatePicker
          : toDatePicker // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$ToDatePickerChangedImpl implements _ToDatePickerChanged {
  const _$ToDatePickerChangedImpl({required this.toDatePicker});

  @override
  final String toDatePicker;

  @override
  String toString() {
    return 'ToDatePickerChanged(toDatePicker: $toDatePicker)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ToDatePickerChangedImpl &&
            (identical(other.toDatePicker, toDatePicker) ||
                other.toDatePicker == toDatePicker));
  }

  @override
  int get hashCode => Object.hash(runtimeType, toDatePicker);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ToDatePickerChangedImplCopyWith<_$ToDatePickerChangedImpl> get copyWith =>
      __$$ToDatePickerChangedImplCopyWithImpl<_$ToDatePickerChangedImpl>(
          this, _$identity);
}

abstract class _ToDatePickerChanged implements ToDatePickerChanged {
  const factory _ToDatePickerChanged({required final String toDatePicker}) =
      _$ToDatePickerChangedImpl;

  @override
  String get toDatePicker;
  @override
  @JsonKey(ignore: true)
  _$$ToDatePickerChangedImplCopyWith<_$ToDatePickerChangedImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$JobContentChanged {
  String get jobContent => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $JobContentChangedCopyWith<JobContentChanged> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $JobContentChangedCopyWith<$Res> {
  factory $JobContentChangedCopyWith(
          JobContentChanged value, $Res Function(JobContentChanged) then) =
      _$JobContentChangedCopyWithImpl<$Res, JobContentChanged>;
  @useResult
  $Res call({String jobContent});
}

/// @nodoc
class _$JobContentChangedCopyWithImpl<$Res, $Val extends JobContentChanged>
    implements $JobContentChangedCopyWith<$Res> {
  _$JobContentChangedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? jobContent = null,
  }) {
    return _then(_value.copyWith(
      jobContent: null == jobContent
          ? _value.jobContent
          : jobContent // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$JobContentChangedImplCopyWith<$Res>
    implements $JobContentChangedCopyWith<$Res> {
  factory _$$JobContentChangedImplCopyWith(_$JobContentChangedImpl value,
          $Res Function(_$JobContentChangedImpl) then) =
      __$$JobContentChangedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String jobContent});
}

/// @nodoc
class __$$JobContentChangedImplCopyWithImpl<$Res>
    extends _$JobContentChangedCopyWithImpl<$Res, _$JobContentChangedImpl>
    implements _$$JobContentChangedImplCopyWith<$Res> {
  __$$JobContentChangedImplCopyWithImpl(_$JobContentChangedImpl _value,
      $Res Function(_$JobContentChangedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? jobContent = null,
  }) {
    return _then(_$JobContentChangedImpl(
      jobContent: null == jobContent
          ? _value.jobContent
          : jobContent // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$JobContentChangedImpl implements _JobContentChanged {
  const _$JobContentChangedImpl({required this.jobContent});

  @override
  final String jobContent;

  @override
  String toString() {
    return 'JobContentChanged(jobContent: $jobContent)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$JobContentChangedImpl &&
            (identical(other.jobContent, jobContent) ||
                other.jobContent == jobContent));
  }

  @override
  int get hashCode => Object.hash(runtimeType, jobContent);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$JobContentChangedImplCopyWith<_$JobContentChangedImpl> get copyWith =>
      __$$JobContentChangedImplCopyWithImpl<_$JobContentChangedImpl>(
          this, _$identity);
}

abstract class _JobContentChanged implements JobContentChanged {
  const factory _JobContentChanged({required final String jobContent}) =
      _$JobContentChangedImpl;

  @override
  String get jobContent;
  @override
  @JsonKey(ignore: true)
  _$$JobContentChangedImplCopyWith<_$JobContentChangedImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$ReEntryCreateJobButtonPressed {}

/// @nodoc
abstract class $ReEntryCreateJobButtonPressedCopyWith<$Res> {
  factory $ReEntryCreateJobButtonPressedCopyWith(
          ReEntryCreateJobButtonPressed value,
          $Res Function(ReEntryCreateJobButtonPressed) then) =
      _$ReEntryCreateJobButtonPressedCopyWithImpl<$Res,
          ReEntryCreateJobButtonPressed>;
}

/// @nodoc
class _$ReEntryCreateJobButtonPressedCopyWithImpl<$Res,
        $Val extends ReEntryCreateJobButtonPressed>
    implements $ReEntryCreateJobButtonPressedCopyWith<$Res> {
  _$ReEntryCreateJobButtonPressedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$ReEntryCreateJobButtonPressedImplCopyWith<$Res> {
  factory _$$ReEntryCreateJobButtonPressedImplCopyWith(
          _$ReEntryCreateJobButtonPressedImpl value,
          $Res Function(_$ReEntryCreateJobButtonPressedImpl) then) =
      __$$ReEntryCreateJobButtonPressedImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ReEntryCreateJobButtonPressedImplCopyWithImpl<$Res>
    extends _$ReEntryCreateJobButtonPressedCopyWithImpl<$Res,
        _$ReEntryCreateJobButtonPressedImpl>
    implements _$$ReEntryCreateJobButtonPressedImplCopyWith<$Res> {
  __$$ReEntryCreateJobButtonPressedImplCopyWithImpl(
      _$ReEntryCreateJobButtonPressedImpl _value,
      $Res Function(_$ReEntryCreateJobButtonPressedImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ReEntryCreateJobButtonPressedImpl
    implements _ReEntryCreateJobButtonPressed {
  const _$ReEntryCreateJobButtonPressedImpl();

  @override
  String toString() {
    return 'ReEntryCreateJobButtonPressed()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ReEntryCreateJobButtonPressedImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;
}

abstract class _ReEntryCreateJobButtonPressed
    implements ReEntryCreateJobButtonPressed {
  const factory _ReEntryCreateJobButtonPressed() =
      _$ReEntryCreateJobButtonPressedImpl;
}

/// @nodoc
mixin _$CreateJobButtonPressed {
  String get jobTitle => throw _privateConstructorUsedError;
  String get priority => throw _privateConstructorUsedError;
  String get fromDatePicker => throw _privateConstructorUsedError;
  String get toDatePicker => throw _privateConstructorUsedError;
  String get jobContent => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CreateJobButtonPressedCopyWith<CreateJobButtonPressed> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CreateJobButtonPressedCopyWith<$Res> {
  factory $CreateJobButtonPressedCopyWith(CreateJobButtonPressed value,
          $Res Function(CreateJobButtonPressed) then) =
      _$CreateJobButtonPressedCopyWithImpl<$Res, CreateJobButtonPressed>;
  @useResult
  $Res call(
      {String jobTitle,
      String priority,
      String fromDatePicker,
      String toDatePicker,
      String jobContent});
}

/// @nodoc
class _$CreateJobButtonPressedCopyWithImpl<$Res,
        $Val extends CreateJobButtonPressed>
    implements $CreateJobButtonPressedCopyWith<$Res> {
  _$CreateJobButtonPressedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? jobTitle = null,
    Object? priority = null,
    Object? fromDatePicker = null,
    Object? toDatePicker = null,
    Object? jobContent = null,
  }) {
    return _then(_value.copyWith(
      jobTitle: null == jobTitle
          ? _value.jobTitle
          : jobTitle // ignore: cast_nullable_to_non_nullable
              as String,
      priority: null == priority
          ? _value.priority
          : priority // ignore: cast_nullable_to_non_nullable
              as String,
      fromDatePicker: null == fromDatePicker
          ? _value.fromDatePicker
          : fromDatePicker // ignore: cast_nullable_to_non_nullable
              as String,
      toDatePicker: null == toDatePicker
          ? _value.toDatePicker
          : toDatePicker // ignore: cast_nullable_to_non_nullable
              as String,
      jobContent: null == jobContent
          ? _value.jobContent
          : jobContent // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$CreateJobButtonPressedImplCopyWith<$Res>
    implements $CreateJobButtonPressedCopyWith<$Res> {
  factory _$$CreateJobButtonPressedImplCopyWith(
          _$CreateJobButtonPressedImpl value,
          $Res Function(_$CreateJobButtonPressedImpl) then) =
      __$$CreateJobButtonPressedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String jobTitle,
      String priority,
      String fromDatePicker,
      String toDatePicker,
      String jobContent});
}

/// @nodoc
class __$$CreateJobButtonPressedImplCopyWithImpl<$Res>
    extends _$CreateJobButtonPressedCopyWithImpl<$Res,
        _$CreateJobButtonPressedImpl>
    implements _$$CreateJobButtonPressedImplCopyWith<$Res> {
  __$$CreateJobButtonPressedImplCopyWithImpl(
      _$CreateJobButtonPressedImpl _value,
      $Res Function(_$CreateJobButtonPressedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? jobTitle = null,
    Object? priority = null,
    Object? fromDatePicker = null,
    Object? toDatePicker = null,
    Object? jobContent = null,
  }) {
    return _then(_$CreateJobButtonPressedImpl(
      jobTitle: null == jobTitle
          ? _value.jobTitle
          : jobTitle // ignore: cast_nullable_to_non_nullable
              as String,
      priority: null == priority
          ? _value.priority
          : priority // ignore: cast_nullable_to_non_nullable
              as String,
      fromDatePicker: null == fromDatePicker
          ? _value.fromDatePicker
          : fromDatePicker // ignore: cast_nullable_to_non_nullable
              as String,
      toDatePicker: null == toDatePicker
          ? _value.toDatePicker
          : toDatePicker // ignore: cast_nullable_to_non_nullable
              as String,
      jobContent: null == jobContent
          ? _value.jobContent
          : jobContent // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$CreateJobButtonPressedImpl implements _CreateJobButtonPressed {
  const _$CreateJobButtonPressedImpl(
      {required this.jobTitle,
      required this.priority,
      required this.fromDatePicker,
      required this.toDatePicker,
      required this.jobContent});

  @override
  final String jobTitle;
  @override
  final String priority;
  @override
  final String fromDatePicker;
  @override
  final String toDatePicker;
  @override
  final String jobContent;

  @override
  String toString() {
    return 'CreateJobButtonPressed(jobTitle: $jobTitle, priority: $priority, fromDatePicker: $fromDatePicker, toDatePicker: $toDatePicker, jobContent: $jobContent)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CreateJobButtonPressedImpl &&
            (identical(other.jobTitle, jobTitle) ||
                other.jobTitle == jobTitle) &&
            (identical(other.priority, priority) ||
                other.priority == priority) &&
            (identical(other.fromDatePicker, fromDatePicker) ||
                other.fromDatePicker == fromDatePicker) &&
            (identical(other.toDatePicker, toDatePicker) ||
                other.toDatePicker == toDatePicker) &&
            (identical(other.jobContent, jobContent) ||
                other.jobContent == jobContent));
  }

  @override
  int get hashCode => Object.hash(runtimeType, jobTitle, priority,
      fromDatePicker, toDatePicker, jobContent);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CreateJobButtonPressedImplCopyWith<_$CreateJobButtonPressedImpl>
      get copyWith => __$$CreateJobButtonPressedImplCopyWithImpl<
          _$CreateJobButtonPressedImpl>(this, _$identity);
}

abstract class _CreateJobButtonPressed implements CreateJobButtonPressed {
  const factory _CreateJobButtonPressed(
      {required final String jobTitle,
      required final String priority,
      required final String fromDatePicker,
      required final String toDatePicker,
      required final String jobContent}) = _$CreateJobButtonPressedImpl;

  @override
  String get jobTitle;
  @override
  String get priority;
  @override
  String get fromDatePicker;
  @override
  String get toDatePicker;
  @override
  String get jobContent;
  @override
  @JsonKey(ignore: true)
  _$$CreateJobButtonPressedImplCopyWith<_$CreateJobButtonPressedImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$PickerDateRangeButtonPressed {
  String get selectedDate => throw _privateConstructorUsedError;
  String get dateCount => throw _privateConstructorUsedError;
  String get range => throw _privateConstructorUsedError;
  String get rangeCount => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $PickerDateRangeButtonPressedCopyWith<PickerDateRangeButtonPressed>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PickerDateRangeButtonPressedCopyWith<$Res> {
  factory $PickerDateRangeButtonPressedCopyWith(
          PickerDateRangeButtonPressed value,
          $Res Function(PickerDateRangeButtonPressed) then) =
      _$PickerDateRangeButtonPressedCopyWithImpl<$Res,
          PickerDateRangeButtonPressed>;
  @useResult
  $Res call(
      {String selectedDate, String dateCount, String range, String rangeCount});
}

/// @nodoc
class _$PickerDateRangeButtonPressedCopyWithImpl<$Res,
        $Val extends PickerDateRangeButtonPressed>
    implements $PickerDateRangeButtonPressedCopyWith<$Res> {
  _$PickerDateRangeButtonPressedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? selectedDate = null,
    Object? dateCount = null,
    Object? range = null,
    Object? rangeCount = null,
  }) {
    return _then(_value.copyWith(
      selectedDate: null == selectedDate
          ? _value.selectedDate
          : selectedDate // ignore: cast_nullable_to_non_nullable
              as String,
      dateCount: null == dateCount
          ? _value.dateCount
          : dateCount // ignore: cast_nullable_to_non_nullable
              as String,
      range: null == range
          ? _value.range
          : range // ignore: cast_nullable_to_non_nullable
              as String,
      rangeCount: null == rangeCount
          ? _value.rangeCount
          : rangeCount // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$PickerDateRangeButtonPressedImplCopyWith<$Res>
    implements $PickerDateRangeButtonPressedCopyWith<$Res> {
  factory _$$PickerDateRangeButtonPressedImplCopyWith(
          _$PickerDateRangeButtonPressedImpl value,
          $Res Function(_$PickerDateRangeButtonPressedImpl) then) =
      __$$PickerDateRangeButtonPressedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String selectedDate, String dateCount, String range, String rangeCount});
}

/// @nodoc
class __$$PickerDateRangeButtonPressedImplCopyWithImpl<$Res>
    extends _$PickerDateRangeButtonPressedCopyWithImpl<$Res,
        _$PickerDateRangeButtonPressedImpl>
    implements _$$PickerDateRangeButtonPressedImplCopyWith<$Res> {
  __$$PickerDateRangeButtonPressedImplCopyWithImpl(
      _$PickerDateRangeButtonPressedImpl _value,
      $Res Function(_$PickerDateRangeButtonPressedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? selectedDate = null,
    Object? dateCount = null,
    Object? range = null,
    Object? rangeCount = null,
  }) {
    return _then(_$PickerDateRangeButtonPressedImpl(
      selectedDate: null == selectedDate
          ? _value.selectedDate
          : selectedDate // ignore: cast_nullable_to_non_nullable
              as String,
      dateCount: null == dateCount
          ? _value.dateCount
          : dateCount // ignore: cast_nullable_to_non_nullable
              as String,
      range: null == range
          ? _value.range
          : range // ignore: cast_nullable_to_non_nullable
              as String,
      rangeCount: null == rangeCount
          ? _value.rangeCount
          : rangeCount // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$PickerDateRangeButtonPressedImpl
    implements _PickerDateRangeButtonPressed {
  const _$PickerDateRangeButtonPressedImpl(
      {required this.selectedDate,
      required this.dateCount,
      required this.range,
      required this.rangeCount});

  @override
  final String selectedDate;
  @override
  final String dateCount;
  @override
  final String range;
  @override
  final String rangeCount;

  @override
  String toString() {
    return 'PickerDateRangeButtonPressed(selectedDate: $selectedDate, dateCount: $dateCount, range: $range, rangeCount: $rangeCount)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$PickerDateRangeButtonPressedImpl &&
            (identical(other.selectedDate, selectedDate) ||
                other.selectedDate == selectedDate) &&
            (identical(other.dateCount, dateCount) ||
                other.dateCount == dateCount) &&
            (identical(other.range, range) || other.range == range) &&
            (identical(other.rangeCount, rangeCount) ||
                other.rangeCount == rangeCount));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, selectedDate, dateCount, range, rangeCount);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$PickerDateRangeButtonPressedImplCopyWith<
          _$PickerDateRangeButtonPressedImpl>
      get copyWith => __$$PickerDateRangeButtonPressedImplCopyWithImpl<
          _$PickerDateRangeButtonPressedImpl>(this, _$identity);
}

abstract class _PickerDateRangeButtonPressed
    implements PickerDateRangeButtonPressed {
  const factory _PickerDateRangeButtonPressed(
      {required final String selectedDate,
      required final String dateCount,
      required final String range,
      required final String rangeCount}) = _$PickerDateRangeButtonPressedImpl;

  @override
  String get selectedDate;
  @override
  String get dateCount;
  @override
  String get range;
  @override
  String get rangeCount;
  @override
  @JsonKey(ignore: true)
  _$$PickerDateRangeButtonPressedImplCopyWith<
          _$PickerDateRangeButtonPressedImpl>
      get copyWith => throw _privateConstructorUsedError;
}
