import 'dart:async';
import 'package:domain/domain.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:base_architecture_flutter/src/app.dart';
import 'package:shared/shared.dart';

@Injectable()
class CreateJobBloc extends BaseBloc<CreateJobEvent, CreateJobState> {
  CreateJobBloc(this._createJobUseCase) : super(const CreateJobState()) {
    on<JobTitleTextFieldChanged>(
      _onJobTitleTextFieldChanged,
      transformer: distinct(),
    );

    on<PriorityCheckBoxChanged>(
      _onPriorityCheckBoxChanged,
      transformer: distinct(),
    );

    on<FromDatePickerChanged>(
      _onFromDatePickerChanged,
      transformer: distinct(),
    );

    on<ToDatePickerChanged>(
      _onToDatePickerChanged,
      transformer: distinct(),
    );

    on<JobContentChanged>(
      _onJobContentChanged,
      transformer: distinct(),
    );

    on<ReEntryCreateJobButtonPressed>(
      _onReEntryCreateJobButtonPressed,
      transformer: log(),
    );

    on<CreateJobButtonPressed>(
      _onCreateJobButtonPressed,
      transformer: distinct(),
    );

    on<PickerDateRangeButtonPressed>(
      _onPickerDateRangeButtonPressed,
      transformer: distinct(),
    );
  }

  final CreateJobUseCase _createJobUseCase;

  bool _isCreateJobButtonEnabled(String jobTitle, String jobContent, String fromDate, String toDate) {
    return jobTitle.isNotEmpty && jobContent.isNotEmpty && fromDate.isNotEmpty && toDate.isNotEmpty;
  }

  bool _isReEntryButtonEnabled(String jobTitle, String jobContent, String fromDate, String toDate) {
    return jobTitle.isNotEmpty && jobContent.isNotEmpty && fromDate.isNotEmpty && toDate.isNotEmpty;
  }

  void _onJobTitleTextFieldChanged(JobTitleTextFieldChanged event, Emitter<CreateJobState> emit) {
    emit(state.copyWith(
      // depositAmount: event.depositAmount,
      // isReEntryButtonEnabled: _isReEntryButtonEnabled(event.depositAmount, state.depositTerm, state.interestRatePerYear),
      // isCalculateButtonEnabled: _isCalculateButtonEnabled(event.depositAmount, state.depositTerm, state.interestRatePerYear),
    ));
  }

  void _onPriorityCheckBoxChanged(PriorityCheckBoxChanged event, Emitter<CreateJobState> emit) {
    emit(state.copyWith(
      // depositTerm: event.depositTerm,
      // isReEntryButtonEnabled: _isReEntryButtonEnabled(state.depositAmount, event.depositTerm, state.interestRatePerYear),
      // isCalculateButtonEnabled: _isCalculateButtonEnabled(state.depositAmount, event.depositTerm, state.interestRatePerYear),
    ));
  }

  void _onFromDatePickerChanged(FromDatePickerChanged event, Emitter<CreateJobState> emit) {
    emit(state.copyWith(
      // interestRatePerYear: event.interestRatePerYear,
      // isReEntryButtonEnabled: _isReEntryButtonEnabled(state.depositAmount, state.depositTerm, event.interestRatePerYear),
      // isCalculateButtonEnabled: _isCalculateButtonEnabled(state.depositAmount, state.depositTerm, event.interestRatePerYear),
    ));
  }

  void _onToDatePickerChanged(ToDatePickerChanged event, Emitter<CreateJobState> emit) {
    emit(state.copyWith(
      // dateUnit: event.dateUnit,
    ));
  }

  void _onJobContentChanged(JobContentChanged event, Emitter<CreateJobState> emit) {
    emit(state.copyWith(
      // dateUnit: event.dateUnit,
    ));
  }

  FutureOr<void> _onCreateJobButtonPressed(CreateJobButtonPressed event, Emitter<CreateJobState> emit) {
    return runBlocCatching(
      action: () async {
        // final output = _calculateSavingUseCase.execute(
        //     CalculateSavingInput(
        //       depositAmount: state.depositAmount,
        //       depositTerm: state.depositTerm,
        //       interestRatePerYear: state.interestRatePerYear,
        //       dateUnit: state.dateUnit,
        //     )
        // );
        // emit(state.copyWith(
        //   totalInterestEndPeriod: output.totalInterestEndPeriod,
        //   totalInterestPrincipalEndPeriod: output.totalInterestPrincipalEndPeriod,
        // ));
      },
    );
  }

  FutureOr<void> _onReEntryCreateJobButtonPressed(ReEntryCreateJobButtonPressed event, Emitter<CreateJobState> emit) {
    emit(state.copyWith(
      jobTitle: '',
      fromDate: '',
      toDate: '',
      priority: 0,
      isReEntryButtonEnabled: false,
      isCreateJobButtonEnabled: false,
      jobContent: '',
      jobLocation: '',
    ));
  }

  void _onPickerDateRangeButtonPressed(PickerDateRangeButtonPressed event, Emitter<CreateJobState> emit) {
    emit(state.copyWith(
      selectedDate: event.selectedDate,
      dateCount: event.dateCount,
      range: event.range,
      rangeCount: event.rangeCount,
    ));
  }
}
