import 'dart:async';

import 'package:auto_route/auto_route.dart';
import 'package:domain/domain.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:resources/resources.dart';
import 'package:base_architecture_flutter/src/app.dart';
import 'package:shared/shared.dart';
import 'package:resources/src/chart/lib/src/charts/common/legend.dart';

@RoutePage()
class DashBoardPage extends StatefulWidget {
  const DashBoardPage({super.key});

  @override
  State<StatefulWidget> createState() {
    return _DashBoardPageState();
  }
}

class _DashBoardPageState extends BasePageState<DashBoardPage, DashBoardBloc> {
  static List<String> listDateTime = <String>[S.current.day, S.current.week, S.current.month];
  static List<String> listPriority = <String>[S.current.all, S.current.high, S.current.medium, S.current.low];
  bool isSelectedCreateCalendarEvent = false;
  bool isSelectedCreateTodoEvent = false;
  final CalendarController ctrCalendar = CalendarController();
  final TextEditingController ctrViewStyle = TextEditingController();
  late final _pagingController = CommonPagingController<Customer>()..disposeBy(disposeBag);
  /// Charts
  List<_SalesData> data = [
    _SalesData('Jan', 35),
    _SalesData('Feb', 28),
    _SalesData('Mar', 34),
    _SalesData('Apr', 32),
    _SalesData('May', 40)
  ];
  List<_SalesData> data1 = [
    _SalesData('Jan', 45),
    _SalesData('Feb', 20),
    _SalesData('Mar', 31),
    _SalesData('Apr', 10),
    _SalesData('May', 20)
  ];
  List<_PieData> pieData = [
    _PieData('Jan', 56,),
    _PieData('Feb', 28),
    _PieData('Mar', 34),
    _PieData('Apr', 32),
    _PieData('May', 40)
  ];

  @override
  void initState() {
    super.initState();
    bloc.add(const DashBoardSearch(search: ''));
    bloc.add(const DashBoardPageInitiated());
    _pagingController.listen(
      onLoadMore: () => bloc.add(const DashBoardLoadMore()),
    );
  }

  @override
  Widget buildPageListeners({required Widget child}) {
    return MultiBlocListener(
      listeners: [
        BlocListener<DashBoardBloc, DashBoardState>(
          listenWhen: (previous, current) => previous.customers != current.customers,
          listener: (context, state) {
            _pagingController.appendLoadMoreOutput(state.customers);
          },
        ),
        BlocListener<DashBoardBloc, DashBoardState>(
          listenWhen: (previous, current) =>
          previous.loadCustomersException != current.loadCustomersException,
          listener: (context, state) {
            _pagingController.error = state.loadCustomersException;
          },
        ),
      ],
      child: child,
    );
  }

  @override
  Widget buildPage(BuildContext context) {
    final List<ChartData> chartData = [
      ChartData('0 sp', 10, Colors.lightGreen),
      ChartData('1 sp', 11, Colors.orangeAccent),
      ChartData('2 sp', 9, Colors.lightBlueAccent),
      ChartData('3-5 sp', 10, Colors.lime),
      ChartData('>5 sp', 10, Colors.indigoAccent)
    ];
    return CommonScaffold(
      body: SafeArea(
        child: Container(
          color: Colors.blueGrey.shade50,
          padding: EdgeInsets.all(Dimens.d8.responsive()),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Row(
                  children: [
                    Expanded(
                      flex: 2,
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.all(Radius.circular(Dimens.d6.responsive())),
                        ),
                        padding: EdgeInsets.all(Dimens.d0.responsive()),
                        child: Column(
                          children: [
                            Container(
                              padding: EdgeInsets.fromLTRB(
                                  Dimens.d10.responsive(),
                                  Dimens.d8.responsive(),
                                  Dimens.d10.responsive(),
                                  Dimens.d8.responsive(),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    S.current.workSchedule, style: AppTextStyles.s14w500Tertiary(),
                                  ),
                                  TextLeftIcon(
                                    icon: Icons.circle,
                                    text: S.current.highPriority,
                                    color: Colors.redAccent,
                                  ),
                                  TextLeftIcon(
                                    icon: Icons.circle,
                                    text: S.current.mediumPriority,
                                    color: Colors.orangeAccent,
                                  ),
                                  TextLeftIcon(
                                    icon: Icons.circle,
                                    text: S.current.lowPriority,
                                    color: Colors.greenAccent,
                                  ),
                                  CommonDropdownMenu(
                                    list: listDateTime,
                                    width: Dimens.d90.responsive(),
                                    controller: ctrViewStyle,
                                    onSelected: (value) {
                                      if(value == S.current.day){
                                        ctrCalendar.view = CalendarView.day;
                                      } else if(value == S.current.week){
                                        ctrCalendar.view = CalendarView.week;
                                      } else if(value == S.current.month){
                                        ctrCalendar.view = CalendarView.month;
                                      } else{
                                        ctrCalendar.view = CalendarView.week;
                                      }
                                      // bloc.add(DateUnitTextFieldChanged(dateUnit: value!));
                                    },
                                    index: 1,
                                  ),
                                  CommonDropdownMenu(
                                    list: listPriority,
                                    width: Dimens.d90.responsive(),
                                    // controller: ctrViewStyle,
                                    onSelected: (value) {
                                      // bloc.add(DateUnitTextFieldChanged(dateUnit: value!));
                                    },
                                  ),
                                  IconButtonCustom(
                                    isSelected: isSelectedCreateCalendarEvent,
                                    icon: const Icon(Icons.add_circle_outline),
                                    selectedIcon: const Icon(Icons.add_circle),
                                    onPressed: () {
                                      setState(() {
                                        isSelectedCreateCalendarEvent = !isSelectedCreateCalendarEvent;
                                      });
                                      navigator.showModalSheet(
                                        const AppPopupInfo.createJobDialog(),
                                        barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
                                        barrierDismissible: false,
                                        useRootNavigator: true,
                                      );
                                    },
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: Dimens.d400.responsive(),
                              child: SfCalendar(
                                controller: ctrCalendar,
                                view: CalendarView.week,
                                allowDragAndDrop: true,
                                allowAppointmentResize: true,
                                dataSource: getCalendarDataSource(),
                                onTap: calendarTapped,
                              ),
                            ),
                          ],
                        )
                      ),
                    ),
                    SizedBox(width: Dimens.d4.responsive()),
                    Expanded(
                      flex: 1,
                      child: Container(
                        margin: EdgeInsets.only(left: Dimens.d2.responsive()),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.all(Radius.circular(Dimens.d6.responsive())),
                        ),
                        padding: EdgeInsets.all(Dimens.d10.responsive()),
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  S.current.thingsToDo,
                                  style: AppTextStyles.s14w500Tertiary(),
                                ),
                                IconButtonCustom(
                                  isSelected: isSelectedCreateTodoEvent,
                                  icon: const Icon(Icons.add_circle_outline),
                                  selectedIcon: const Icon(Icons.add_circle),
                                  onPressed: () {
                                    setState(() {
                                      isSelectedCreateTodoEvent = !isSelectedCreateTodoEvent;
                                    });
                                    navigator.showModalSheet(
                                      const AppPopupInfo.createTodoDialog(),
                                      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
                                      barrierDismissible: false,
                                      useRootNavigator: false,
                                    );
                                  },
                                ),
                              ],
                            ),
                            SizedBox(height: Dimens.d8.responsive()),
                            SizedBox(
                              height: Dimens.d1.responsive(),
                              child: Container(color: Colors.blueGrey.shade50),
                            ),
                            SizedBox(
                              height: Dimens.d390.responsive(),
                              child: BlocBuilder<DashBoardBloc, DashBoardState>(
                                buildWhen: (previous, current) =>
                                previous.customers != current.customers || previous.isShimmerLoading != current.isShimmerLoading,
                                builder: (context, state) {
                                  return RefreshIndicator(
                                    onRefresh: () {
                                      final completer = Completer<void>();
                                      bloc.add(DashBoardPageRefreshed(completer: completer));
                                      return completer.future;
                                    },
                                    child: state.isShimmerLoading && state.customers.data.isEmpty
                                        ? const ListViewLoader()
                                        : CommonPagedListView<Customer>(
                                      scrollDirection: Axis.vertical,
                                      shrinkWrap: true,
                                      physics: const ScrollPhysics(),
                                      pagingController: _pagingController,
                                      itemBuilder: (context, customer, index) {
                                        return Padding(
                                          padding: EdgeInsets.symmetric(
                                            horizontal: Dimens.d0.responsive(),
                                            vertical: Dimens.d4.responsive(),
                                          ),
                                          child: ShimmerLoading(
                                            isLoading: state.isShimmerLoading,
                                            loadingWidget: const LoadingItem(),
                                            child: GestureDetector(
                                              onTap: () async {
                                                // await navigator.push(AppRouteInfo.customerDetail(customer));
                                              },
                                              child: Container(
                                                alignment: Alignment.centerLeft,
                                                padding: EdgeInsets.all(Dimens.d8.responsive()),
                                                decoration: BoxDecoration(
                                                  color: Colors.grey.shade50,
                                                  borderRadius: BorderRadius.circular(Dimens.d8.responsive()),
                                                ),
                                                width: double.infinity,
                                                height: Dimens.d52.responsive(),
                                                child: Column(
                                                  children: [
                                                    Row(
                                                      children: [
                                                        Expanded(
                                                          flex: 1,
                                                          child: Align(
                                                            alignment: Alignment.centerLeft,
                                                            child: Row(
                                                              children: [
                                                                Align(
                                                                  alignment: Alignment.centerLeft,
                                                                  child: Container(
                                                                    decoration: BoxDecoration(
                                                                      image: DecorationImage(
                                                                        image: customer.gender == Constants.male ? Assets.images.avaMan.provider() : Assets.images.avaWoman.provider(),
                                                                        fit: BoxFit.fitHeight,
                                                                      ),
                                                                    ),
                                                                    child: SizedBox(
                                                                      width: Dimens.d28.responsive(), height: Dimens.d28.responsive(),
                                                                    ),
                                                                  ),
                                                                ),
                                                                SizedBox(width: Dimens.d16.responsive()),
                                                                Align(
                                                                  alignment: Alignment.centerLeft,
                                                                  child: Column(
                                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                                    children: [
                                                                      Text(customer.customerName, style: AppTextStyles.s12w400Tertiary()),
                                                                      Text('${S.current.idCard}: ${customer.customerCode}', style: AppTextStyles.s12w400Tertiary()),
                                                                    ],
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                        );
                                      },
                                    ),
                                  );
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: Dimens.d6.responsive()),
                Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.all(Radius.circular(Dimens.d6.responsive())),
                        ),
                        padding: EdgeInsets.all(Dimens.d4.responsive()),
                        child:SfCartesianChart(
                            primaryXAxis: CategoryAxis(),
                            title: ChartTitle(text: S.current.segment),
                            legend: Legend(isVisible: true),
                            tooltipBehavior: TooltipBehavior(enable: true),
                            series: <CartesianSeries<_SalesData, String>>[
                              LineSeries<_SalesData, String>(
                                dataSource: data,
                                xValueMapper: (_SalesData sales, _) => sales.year,
                                yValueMapper: (_SalesData sales, _) => sales.sales,
                                name: S.current.private,
                                dataLabelSettings: DataLabelSettings(isVisible: true),
                                animationDuration: 2000,
                              ),
                              LineSeries<_SalesData, String>(
                                dataSource: data1,
                                xValueMapper: (_SalesData sales, _) => sales.year,
                                yValueMapper: (_SalesData sales, _) => sales.sales,
                                name: S.current.priority,
                                dataLabelSettings: DataLabelSettings(isVisible: true),
                                animationDuration: 2000,
                              ),
                            ]),
                      ),
                    ),
                    SizedBox(width: Dimens.d6.responsive()),
                    Expanded(
                      flex: 1,
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.all(Radius.circular(Dimens.d6.responsive())),
                        ),
                        padding: EdgeInsets.all(Dimens.d4.responsive()),
                        child: SfCircularChart(
                            title: ChartTitle(text: S.current.privatePriority),
                            legend: Legend(isVisible: true),
                            series: <PieSeries<_PieData, String>>[
                              PieSeries<_PieData, String>(
                                explode: true,
                                explodeIndex: 0,
                                dataSource: pieData,
                                xValueMapper: (_PieData data, _) => data.xData,
                                yValueMapper: (_PieData data, _) => data.yData,
                                dataLabelMapper: (_PieData data, _) => data.text,
                                dataLabelSettings: DataLabelSettings(isVisible: true),
                                animationDuration: 2000,
                              ),
                            ]
                        )
                      ),
                    ),
                    SizedBox(width: Dimens.d6.responsive()),
                    Expanded(
                      flex: 1,
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.all(Radius.circular(Dimens.d6.responsive())),
                        ),
                        padding: EdgeInsets.all(Dimens.d4.responsive()),
                        child: SfCartesianChart(
                            primaryXAxis: CategoryAxis(),
                            title: ChartTitle(text: S.current.numberUsedProductsServices),
                            palette: <Color>[
                              Colors.teal,
                              Colors.orange,
                              Colors.brown,
                              Colors.redAccent,
                              Colors.lightBlue
                            ],
                            series: <CartesianSeries<ChartData, String>>[
                              ColumnSeries<ChartData, String>(
                                  dataSource: chartData,
                                  xValueMapper: (ChartData data, _) => data.x,
                                  yValueMapper: (ChartData data, _) => data.y,
                                  animationDuration: 2000,
                                  pointColorMapper: (ChartData data, _) => data.color
                              ),
                            ]
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void calendarTapped(CalendarTapDetails calendarTapDetails) {
    if (ctrCalendar.view == CalendarView.month &&
        calendarTapDetails.targetElement == CalendarElement.calendarCell) {
      ctrCalendar.view = CalendarView.day;
      ctrViewStyle.text = S.current.day;
    } else if ((ctrCalendar.view == CalendarView.week ||
        ctrCalendar.view == CalendarView.workWeek) &&
        calendarTapDetails.targetElement == CalendarElement.viewHeader) {
      ctrCalendar.view = CalendarView.day;
      ctrViewStyle.text = S.current.day;
    }
  }

  _DataSource getCalendarDataSource() {
    final List<Appointment> appointments = <Appointment>[];
    appointments.add(Appointment(
      startTime: DateTime.now(),
      endTime: DateTime.now().add(const Duration(hours: 1)),
      subject: 'Meeting',
      color: Colors.redAccent,
    ));
    appointments.add(Appointment(
      startTime: DateTime.now().add(const Duration(hours: 4)),
      endTime: DateTime.now().add(const Duration(hours: 5)),
      subject: 'Release Meeting',
      color: Colors.lightBlueAccent,
    ));
    appointments.add(Appointment(
      startTime: DateTime.now().add(const Duration(hours: 6)),
      endTime: DateTime.now().add(const Duration(hours: 7)),
      subject: 'Performance check',
      color: Colors.orangeAccent,
    ));
    appointments.add(Appointment(
      startTime: DateTime(2024, 8, 14, 1, 0, 0),
      endTime: DateTime(2024, 8, 14, 3, 0, 0),
      subject: 'Support',
      color: Colors.greenAccent,
    ));
    appointments.add(Appointment(
      startTime: DateTime(2024, 8, 15, 3, 0, 0),
      endTime: DateTime(2024, 8, 15, 4, 0, 0),
      subject: 'Retrospective',
      color: Colors.orangeAccent,
    ));

    return _DataSource(appointments);
  }
}

class _DataSource extends CalendarDataSource {
  _DataSource(this.source);

  List<Appointment> source;

  @override
  List<dynamic> get appointments => source;
}

class _SalesData {
  _SalesData(this.year, this.sales);

  final String year;
  final double sales;
}

class _PieData {
  _PieData(this.xData, this.yData, [this.text]);
  final String xData;
  final num yData;
  String? text;
}

class ChartData{
  ChartData(this.x, this.y, this.color);
  final String x;
  final double y;
  final Color? color;
}
