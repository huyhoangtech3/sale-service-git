// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'dashboard_event.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$DashBoardPageInitiated {}

/// @nodoc
abstract class $DashBoardPageInitiatedCopyWith<$Res> {
  factory $DashBoardPageInitiatedCopyWith(DashBoardPageInitiated value,
          $Res Function(DashBoardPageInitiated) then) =
      _$DashBoardPageInitiatedCopyWithImpl<$Res, DashBoardPageInitiated>;
}

/// @nodoc
class _$DashBoardPageInitiatedCopyWithImpl<$Res,
        $Val extends DashBoardPageInitiated>
    implements $DashBoardPageInitiatedCopyWith<$Res> {
  _$DashBoardPageInitiatedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$DashBoardPageInitiatedImplCopyWith<$Res> {
  factory _$$DashBoardPageInitiatedImplCopyWith(
          _$DashBoardPageInitiatedImpl value,
          $Res Function(_$DashBoardPageInitiatedImpl) then) =
      __$$DashBoardPageInitiatedImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$DashBoardPageInitiatedImplCopyWithImpl<$Res>
    extends _$DashBoardPageInitiatedCopyWithImpl<$Res,
        _$DashBoardPageInitiatedImpl>
    implements _$$DashBoardPageInitiatedImplCopyWith<$Res> {
  __$$DashBoardPageInitiatedImplCopyWithImpl(
      _$DashBoardPageInitiatedImpl _value,
      $Res Function(_$DashBoardPageInitiatedImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$DashBoardPageInitiatedImpl implements _DashBoardPageInitiated {
  const _$DashBoardPageInitiatedImpl();

  @override
  String toString() {
    return 'DashBoardPageInitiated()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$DashBoardPageInitiatedImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;
}

abstract class _DashBoardPageInitiated implements DashBoardPageInitiated {
  const factory _DashBoardPageInitiated() = _$DashBoardPageInitiatedImpl;
}

/// @nodoc
mixin _$DashBoardPageRefreshed {
  Completer<void> get completer => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $DashBoardPageRefreshedCopyWith<DashBoardPageRefreshed> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DashBoardPageRefreshedCopyWith<$Res> {
  factory $DashBoardPageRefreshedCopyWith(DashBoardPageRefreshed value,
          $Res Function(DashBoardPageRefreshed) then) =
      _$DashBoardPageRefreshedCopyWithImpl<$Res, DashBoardPageRefreshed>;
  @useResult
  $Res call({Completer<void> completer});
}

/// @nodoc
class _$DashBoardPageRefreshedCopyWithImpl<$Res,
        $Val extends DashBoardPageRefreshed>
    implements $DashBoardPageRefreshedCopyWith<$Res> {
  _$DashBoardPageRefreshedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? completer = null,
  }) {
    return _then(_value.copyWith(
      completer: null == completer
          ? _value.completer
          : completer // ignore: cast_nullable_to_non_nullable
              as Completer<void>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$DashBoardPageRefreshedImplCopyWith<$Res>
    implements $DashBoardPageRefreshedCopyWith<$Res> {
  factory _$$DashBoardPageRefreshedImplCopyWith(
          _$DashBoardPageRefreshedImpl value,
          $Res Function(_$DashBoardPageRefreshedImpl) then) =
      __$$DashBoardPageRefreshedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({Completer<void> completer});
}

/// @nodoc
class __$$DashBoardPageRefreshedImplCopyWithImpl<$Res>
    extends _$DashBoardPageRefreshedCopyWithImpl<$Res,
        _$DashBoardPageRefreshedImpl>
    implements _$$DashBoardPageRefreshedImplCopyWith<$Res> {
  __$$DashBoardPageRefreshedImplCopyWithImpl(
      _$DashBoardPageRefreshedImpl _value,
      $Res Function(_$DashBoardPageRefreshedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? completer = null,
  }) {
    return _then(_$DashBoardPageRefreshedImpl(
      completer: null == completer
          ? _value.completer
          : completer // ignore: cast_nullable_to_non_nullable
              as Completer<void>,
    ));
  }
}

/// @nodoc

class _$DashBoardPageRefreshedImpl implements _DashBoardPageRefreshed {
  const _$DashBoardPageRefreshedImpl({required this.completer});

  @override
  final Completer<void> completer;

  @override
  String toString() {
    return 'DashBoardPageRefreshed(completer: $completer)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$DashBoardPageRefreshedImpl &&
            (identical(other.completer, completer) ||
                other.completer == completer));
  }

  @override
  int get hashCode => Object.hash(runtimeType, completer);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$DashBoardPageRefreshedImplCopyWith<_$DashBoardPageRefreshedImpl>
      get copyWith => __$$DashBoardPageRefreshedImplCopyWithImpl<
          _$DashBoardPageRefreshedImpl>(this, _$identity);
}

abstract class _DashBoardPageRefreshed implements DashBoardPageRefreshed {
  const factory _DashBoardPageRefreshed(
          {required final Completer<void> completer}) =
      _$DashBoardPageRefreshedImpl;

  @override
  Completer<void> get completer;
  @override
  @JsonKey(ignore: true)
  _$$DashBoardPageRefreshedImplCopyWith<_$DashBoardPageRefreshedImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$DashBoardLoadMore {}

/// @nodoc
abstract class $DashBoardLoadMoreCopyWith<$Res> {
  factory $DashBoardLoadMoreCopyWith(
          DashBoardLoadMore value, $Res Function(DashBoardLoadMore) then) =
      _$DashBoardLoadMoreCopyWithImpl<$Res, DashBoardLoadMore>;
}

/// @nodoc
class _$DashBoardLoadMoreCopyWithImpl<$Res, $Val extends DashBoardLoadMore>
    implements $DashBoardLoadMoreCopyWith<$Res> {
  _$DashBoardLoadMoreCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$DashBoardLoadMoreImplCopyWith<$Res> {
  factory _$$DashBoardLoadMoreImplCopyWith(_$DashBoardLoadMoreImpl value,
          $Res Function(_$DashBoardLoadMoreImpl) then) =
      __$$DashBoardLoadMoreImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$DashBoardLoadMoreImplCopyWithImpl<$Res>
    extends _$DashBoardLoadMoreCopyWithImpl<$Res, _$DashBoardLoadMoreImpl>
    implements _$$DashBoardLoadMoreImplCopyWith<$Res> {
  __$$DashBoardLoadMoreImplCopyWithImpl(_$DashBoardLoadMoreImpl _value,
      $Res Function(_$DashBoardLoadMoreImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$DashBoardLoadMoreImpl implements _DashBoardLoadMore {
  const _$DashBoardLoadMoreImpl();

  @override
  String toString() {
    return 'DashBoardLoadMore()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$DashBoardLoadMoreImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;
}

abstract class _DashBoardLoadMore implements DashBoardLoadMore {
  const factory _DashBoardLoadMore() = _$DashBoardLoadMoreImpl;
}

/// @nodoc
mixin _$DashBoardSearch {
  String get search => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $DashBoardSearchCopyWith<DashBoardSearch> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DashBoardSearchCopyWith<$Res> {
  factory $DashBoardSearchCopyWith(
          DashBoardSearch value, $Res Function(DashBoardSearch) then) =
      _$DashBoardSearchCopyWithImpl<$Res, DashBoardSearch>;
  @useResult
  $Res call({String search});
}

/// @nodoc
class _$DashBoardSearchCopyWithImpl<$Res, $Val extends DashBoardSearch>
    implements $DashBoardSearchCopyWith<$Res> {
  _$DashBoardSearchCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? search = null,
  }) {
    return _then(_value.copyWith(
      search: null == search
          ? _value.search
          : search // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$DashBoardSearchImplCopyWith<$Res>
    implements $DashBoardSearchCopyWith<$Res> {
  factory _$$DashBoardSearchImplCopyWith(_$DashBoardSearchImpl value,
          $Res Function(_$DashBoardSearchImpl) then) =
      __$$DashBoardSearchImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String search});
}

/// @nodoc
class __$$DashBoardSearchImplCopyWithImpl<$Res>
    extends _$DashBoardSearchCopyWithImpl<$Res, _$DashBoardSearchImpl>
    implements _$$DashBoardSearchImplCopyWith<$Res> {
  __$$DashBoardSearchImplCopyWithImpl(
      _$DashBoardSearchImpl _value, $Res Function(_$DashBoardSearchImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? search = null,
  }) {
    return _then(_$DashBoardSearchImpl(
      search: null == search
          ? _value.search
          : search // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$DashBoardSearchImpl implements _DashBoardSearch {
  const _$DashBoardSearchImpl({required this.search});

  @override
  final String search;

  @override
  String toString() {
    return 'DashBoardSearch(search: $search)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$DashBoardSearchImpl &&
            (identical(other.search, search) || other.search == search));
  }

  @override
  int get hashCode => Object.hash(runtimeType, search);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$DashBoardSearchImplCopyWith<_$DashBoardSearchImpl> get copyWith =>
      __$$DashBoardSearchImplCopyWithImpl<_$DashBoardSearchImpl>(
          this, _$identity);
}

abstract class _DashBoardSearch implements DashBoardSearch {
  const factory _DashBoardSearch({required final String search}) =
      _$DashBoardSearchImpl;

  @override
  String get search;
  @override
  @JsonKey(ignore: true)
  _$$DashBoardSearchImplCopyWith<_$DashBoardSearchImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
