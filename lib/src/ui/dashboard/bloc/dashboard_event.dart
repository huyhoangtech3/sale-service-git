import 'dart:async';

import 'package:freezed_annotation/freezed_annotation.dart';

import 'package:base_architecture_flutter/src/base/bloc/base_bloc_event.dart';

part 'dashboard_event.freezed.dart';

abstract class DashBoardEvent extends BaseBlocEvent {
  const DashBoardEvent();
}

@freezed
class DashBoardPageInitiated extends DashBoardEvent with _$DashBoardPageInitiated {
  const factory DashBoardPageInitiated() = _DashBoardPageInitiated;
}

@freezed
class DashBoardPageRefreshed extends DashBoardEvent with _$DashBoardPageRefreshed {
  const factory DashBoardPageRefreshed({
    required Completer<void> completer,
  }) = _DashBoardPageRefreshed;
}

@freezed
class DashBoardLoadMore extends DashBoardEvent with _$DashBoardLoadMore {
  const factory DashBoardLoadMore() = _DashBoardLoadMore;
}

@freezed
class DashBoardSearch extends DashBoardEvent with _$DashBoardSearch {
  const factory DashBoardSearch({
    required String search,
  }) = _DashBoardSearch;
}
