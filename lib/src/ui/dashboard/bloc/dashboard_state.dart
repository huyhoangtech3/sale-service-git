import 'package:domain/domain.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import 'package:base_architecture_flutter/src/base/bloc/base_bloc_state.dart';
import 'package:shared/shared.dart';

part 'dashboard_state.freezed.dart';

@freezed
class DashBoardState extends BaseBlocState with _$DashBoardState {
  factory DashBoardState({
    @Default('') String search,
    @Default('') String searchFastType,
    @Default(0) int record,
    @Default(LoadMoreOutput<Customer>(data: <Customer>[])) LoadMoreOutput<Customer> customers,
    @Default(false) bool isShimmerLoading,
    AppException? loadCustomersException,
  }) = _DashBoardState;
}
