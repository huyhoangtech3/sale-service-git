import 'dart:async';
import 'package:domain/domain.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:base_architecture_flutter/src/app.dart';
import 'package:shared/shared.dart';

@Injectable()
class DashBoardBloc extends BaseBloc<DashBoardEvent, DashBoardState> {
  DashBoardBloc(this._customerUseCase) : super(DashBoardState()) {
    on<DashBoardPageInitiated>(
      _onDashBoardPageInitiated,
      transformer: log(),
    );

    on<DashBoardLoadMore>(
      _onDashBoardLoadMore,
      transformer: log(),
    );

    on<DashBoardPageRefreshed>(
      _onDashBoardPageRefreshed,
      transformer: log(),
    );

    on<DashBoardSearch>(
      _onDashBoardSearch,
      transformer: distinct(),
    );
  }

  final CustomerUseCase _customerUseCase;

  FutureOr<void> _onDashBoardPageInitiated(DashBoardPageInitiated event, Emitter<DashBoardState> emit) async {
    await _getCustomers(
      emit: emit,
      isInitialLoad: true,
      doOnSubscribe: () async {
        emit(state.copyWith(isShimmerLoading: true));
        emit(state.copyWith(record: state.customers.data.length));
      },
      doOnSuccessOrError: () async => emit(state.copyWith(isShimmerLoading: false)),
    );
  }

  FutureOr<void> _onDashBoardLoadMore(DashBoardLoadMore event, Emitter<DashBoardState> emit) async {
    await _getCustomers(
      emit: emit,
      isInitialLoad: false,
      doOnSubscribe: () async {
        emit(state.copyWith(record: state.record + PagingConstants.itemsPerPage));
      },
    );
  }

  FutureOr<void> _onDashBoardPageRefreshed(DashBoardPageRefreshed event, Emitter<DashBoardState> emit) async {
    await _getCustomers(
      emit: emit,
      isInitialLoad: true,
      doOnSubscribe: () async {
        emit(state.copyWith(isShimmerLoading: true));
        emit(state.copyWith(record: state.customers.data.length));
      },
      doOnSuccessOrError: () async {
        emit(state.copyWith(isShimmerLoading: false));
        if (!event.completer.isCompleted) {
          event.completer.complete();
        }
      },
    );
  }

  Future<void> _getCustomers({
    required Emitter<DashBoardState> emit,
    required bool isInitialLoad,
    Future<void> Function()? doOnSubscribe,
    Future<void> Function()? doOnSuccessOrError,
  }) async {
    return runBlocCatching(
      action: () async {
        emit(state.copyWith(loadCustomersException: null));
        final output = await _customerUseCase.execute(GetCustomerInput(
            search: state.search,
            searchFastType: state.searchFastType
        ), isInitialLoad);
        emit(state.copyWith(customers: output));
      },
      doOnError: (e) async {
        emit(state.copyWith(loadCustomersException: e));
      },
      doOnSubscribe: doOnSubscribe,
      doOnSuccessOrError: doOnSuccessOrError,
      handleLoading: false,
      maxRetries: 3,
    );
  }

  void _onDashBoardSearch(DashBoardSearch event, Emitter<DashBoardState> emit) {
    emit(state.copyWith(
      search: event.search,
    ));
  }
}
