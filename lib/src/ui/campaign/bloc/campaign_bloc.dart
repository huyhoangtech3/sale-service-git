import 'dart:async';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:base_architecture_flutter/src/app.dart';

@Injectable()
class CampaignBloc extends BaseBloc<CampaignEvent, CampaignState> {
  CampaignBloc() : super(const CampaignState()) {
    on<CampaignPageInitiated>(
      _onCampaignPageInitiated,
      transformer: log(),
    );
  }

  FutureOr<void> _onCampaignPageInitiated(CampaignPageInitiated event, Emitter<CampaignState> emit) {}
}
