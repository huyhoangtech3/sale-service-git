import 'package:freezed_annotation/freezed_annotation.dart';

import 'package:base_architecture_flutter/src/base/bloc/base_bloc_event.dart';

part 'campaign_event.freezed.dart';

abstract class CampaignEvent extends BaseBlocEvent {
  const CampaignEvent();
}

@freezed
class CampaignPageInitiated extends CampaignEvent with _$CampaignPageInitiated {
  const factory CampaignPageInitiated({
    required String id,
  }) = _CampaignPageInitiated;
}
