// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'campaign_event.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$CampaignPageInitiated {
  String get id => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CampaignPageInitiatedCopyWith<CampaignPageInitiated> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CampaignPageInitiatedCopyWith<$Res> {
  factory $CampaignPageInitiatedCopyWith(CampaignPageInitiated value,
          $Res Function(CampaignPageInitiated) then) =
      _$CampaignPageInitiatedCopyWithImpl<$Res, CampaignPageInitiated>;
  @useResult
  $Res call({String id});
}

/// @nodoc
class _$CampaignPageInitiatedCopyWithImpl<$Res,
        $Val extends CampaignPageInitiated>
    implements $CampaignPageInitiatedCopyWith<$Res> {
  _$CampaignPageInitiatedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$CampaignPageInitiatedImplCopyWith<$Res>
    implements $CampaignPageInitiatedCopyWith<$Res> {
  factory _$$CampaignPageInitiatedImplCopyWith(
          _$CampaignPageInitiatedImpl value,
          $Res Function(_$CampaignPageInitiatedImpl) then) =
      __$$CampaignPageInitiatedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String id});
}

/// @nodoc
class __$$CampaignPageInitiatedImplCopyWithImpl<$Res>
    extends _$CampaignPageInitiatedCopyWithImpl<$Res,
        _$CampaignPageInitiatedImpl>
    implements _$$CampaignPageInitiatedImplCopyWith<$Res> {
  __$$CampaignPageInitiatedImplCopyWithImpl(_$CampaignPageInitiatedImpl _value,
      $Res Function(_$CampaignPageInitiatedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
  }) {
    return _then(_$CampaignPageInitiatedImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$CampaignPageInitiatedImpl implements _CampaignPageInitiated {
  const _$CampaignPageInitiatedImpl({required this.id});

  @override
  final String id;

  @override
  String toString() {
    return 'CampaignPageInitiated(id: $id)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CampaignPageInitiatedImpl &&
            (identical(other.id, id) || other.id == id));
  }

  @override
  int get hashCode => Object.hash(runtimeType, id);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CampaignPageInitiatedImplCopyWith<_$CampaignPageInitiatedImpl>
      get copyWith => __$$CampaignPageInitiatedImplCopyWithImpl<
          _$CampaignPageInitiatedImpl>(this, _$identity);
}

abstract class _CampaignPageInitiated implements CampaignPageInitiated {
  const factory _CampaignPageInitiated({required final String id}) =
      _$CampaignPageInitiatedImpl;

  @override
  String get id;
  @override
  @JsonKey(ignore: true)
  _$$CampaignPageInitiatedImplCopyWith<_$CampaignPageInitiatedImpl>
      get copyWith => throw _privateConstructorUsedError;
}
