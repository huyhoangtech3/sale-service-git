import 'package:freezed_annotation/freezed_annotation.dart';

import 'package:base_architecture_flutter/src/base/bloc/base_bloc_state.dart';

part 'campaign_state.freezed.dart';

@freezed
class CampaignState extends BaseBlocState with _$CampaignState {
  const factory CampaignState({
    @Default('') String id,
  }) = _CampaignState;
}
