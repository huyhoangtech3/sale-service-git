import 'package:freezed_annotation/freezed_annotation.dart';

import 'package:base_architecture_flutter/src/base/bloc/base_bloc_state.dart';

part 'item_detail_state.freezed.dart';

@freezed
class ItemDetailState extends BaseBlocState with _$ItemDetailState {
  const factory ItemDetailState({
    @Default('') String id,
  }) = _ItemDetailState;
}
