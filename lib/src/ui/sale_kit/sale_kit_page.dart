import 'package:auto_route/auto_route.dart';
import 'package:domain/domain.dart';
import 'package:flutter/material.dart';
import 'package:resources/resources.dart';
import 'package:base_architecture_flutter/src/app.dart';

@RoutePage()
class SaleKitPage extends StatefulWidget {
  const SaleKitPage({super.key});

  @override
  State<StatefulWidget> createState() {
    return _SaleKitPageState();
  }
}

class _SaleKitPageState extends BasePageState<SaleKitPage, SaleKitBloc> {
  @override
  Widget buildPage(BuildContext context) {
    return CommonScaffold(
      body: Center(
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(AppColors.current.primaryColor),
          ),
          onPressed: () {
            navigator.push(const AppRouteInfo.login());
          },
          child: Text(
            S.current.saleKit,
            style: AppTextStyles.s14w400Primary(),
          ),
        ),
      ),
    );
  }
}
