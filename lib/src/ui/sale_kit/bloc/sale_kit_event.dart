import 'package:freezed_annotation/freezed_annotation.dart';

import 'package:base_architecture_flutter/src/base/bloc/base_bloc_event.dart';

part 'sale_kit_event.freezed.dart';

abstract class SaleKitEvent extends BaseBlocEvent {
  const SaleKitEvent();
}

@freezed
class SaleKitPageInitiated extends SaleKitEvent with _$SaleKitPageInitiated {
  const factory SaleKitPageInitiated({
    required String id,
  }) = _SaleKitPageInitiated;
}
