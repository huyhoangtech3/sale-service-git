// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'sale_kit_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$SaleKitState {
  String get id => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $SaleKitStateCopyWith<SaleKitState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SaleKitStateCopyWith<$Res> {
  factory $SaleKitStateCopyWith(
          SaleKitState value, $Res Function(SaleKitState) then) =
      _$SaleKitStateCopyWithImpl<$Res, SaleKitState>;
  @useResult
  $Res call({String id});
}

/// @nodoc
class _$SaleKitStateCopyWithImpl<$Res, $Val extends SaleKitState>
    implements $SaleKitStateCopyWith<$Res> {
  _$SaleKitStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$SaleKitStateImplCopyWith<$Res>
    implements $SaleKitStateCopyWith<$Res> {
  factory _$$SaleKitStateImplCopyWith(
          _$SaleKitStateImpl value, $Res Function(_$SaleKitStateImpl) then) =
      __$$SaleKitStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String id});
}

/// @nodoc
class __$$SaleKitStateImplCopyWithImpl<$Res>
    extends _$SaleKitStateCopyWithImpl<$Res, _$SaleKitStateImpl>
    implements _$$SaleKitStateImplCopyWith<$Res> {
  __$$SaleKitStateImplCopyWithImpl(
      _$SaleKitStateImpl _value, $Res Function(_$SaleKitStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
  }) {
    return _then(_$SaleKitStateImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$SaleKitStateImpl implements _SaleKitState {
  const _$SaleKitStateImpl({this.id = ''});

  @override
  @JsonKey()
  final String id;

  @override
  String toString() {
    return 'SaleKitState(id: $id)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SaleKitStateImpl &&
            (identical(other.id, id) || other.id == id));
  }

  @override
  int get hashCode => Object.hash(runtimeType, id);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$SaleKitStateImplCopyWith<_$SaleKitStateImpl> get copyWith =>
      __$$SaleKitStateImplCopyWithImpl<_$SaleKitStateImpl>(this, _$identity);
}

abstract class _SaleKitState implements SaleKitState {
  const factory _SaleKitState({final String id}) = _$SaleKitStateImpl;

  @override
  String get id;
  @override
  @JsonKey(ignore: true)
  _$$SaleKitStateImplCopyWith<_$SaleKitStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
