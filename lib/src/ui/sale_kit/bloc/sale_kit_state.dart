import 'package:freezed_annotation/freezed_annotation.dart';

import 'package:base_architecture_flutter/src/base/bloc/base_bloc_state.dart';

part 'sale_kit_state.freezed.dart';

@freezed
class SaleKitState extends BaseBlocState with _$SaleKitState {
  const factory SaleKitState({
    @Default('') String id,
  }) = _SaleKitState;
}
