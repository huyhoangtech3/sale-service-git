// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'sale_kit_event.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$SaleKitPageInitiated {
  String get id => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $SaleKitPageInitiatedCopyWith<SaleKitPageInitiated> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SaleKitPageInitiatedCopyWith<$Res> {
  factory $SaleKitPageInitiatedCopyWith(SaleKitPageInitiated value,
          $Res Function(SaleKitPageInitiated) then) =
      _$SaleKitPageInitiatedCopyWithImpl<$Res, SaleKitPageInitiated>;
  @useResult
  $Res call({String id});
}

/// @nodoc
class _$SaleKitPageInitiatedCopyWithImpl<$Res,
        $Val extends SaleKitPageInitiated>
    implements $SaleKitPageInitiatedCopyWith<$Res> {
  _$SaleKitPageInitiatedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$SaleKitPageInitiatedImplCopyWith<$Res>
    implements $SaleKitPageInitiatedCopyWith<$Res> {
  factory _$$SaleKitPageInitiatedImplCopyWith(_$SaleKitPageInitiatedImpl value,
          $Res Function(_$SaleKitPageInitiatedImpl) then) =
      __$$SaleKitPageInitiatedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String id});
}

/// @nodoc
class __$$SaleKitPageInitiatedImplCopyWithImpl<$Res>
    extends _$SaleKitPageInitiatedCopyWithImpl<$Res, _$SaleKitPageInitiatedImpl>
    implements _$$SaleKitPageInitiatedImplCopyWith<$Res> {
  __$$SaleKitPageInitiatedImplCopyWithImpl(_$SaleKitPageInitiatedImpl _value,
      $Res Function(_$SaleKitPageInitiatedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
  }) {
    return _then(_$SaleKitPageInitiatedImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$SaleKitPageInitiatedImpl implements _SaleKitPageInitiated {
  const _$SaleKitPageInitiatedImpl({required this.id});

  @override
  final String id;

  @override
  String toString() {
    return 'SaleKitPageInitiated(id: $id)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SaleKitPageInitiatedImpl &&
            (identical(other.id, id) || other.id == id));
  }

  @override
  int get hashCode => Object.hash(runtimeType, id);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$SaleKitPageInitiatedImplCopyWith<_$SaleKitPageInitiatedImpl>
      get copyWith =>
          __$$SaleKitPageInitiatedImplCopyWithImpl<_$SaleKitPageInitiatedImpl>(
              this, _$identity);
}

abstract class _SaleKitPageInitiated implements SaleKitPageInitiated {
  const factory _SaleKitPageInitiated({required final String id}) =
      _$SaleKitPageInitiatedImpl;

  @override
  String get id;
  @override
  @JsonKey(ignore: true)
  _$$SaleKitPageInitiatedImplCopyWith<_$SaleKitPageInitiatedImpl>
      get copyWith => throw _privateConstructorUsedError;
}
