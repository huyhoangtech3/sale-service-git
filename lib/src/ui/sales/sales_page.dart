import 'package:auto_route/auto_route.dart';
import 'package:domain/domain.dart';
import 'package:flutter/material.dart';
import 'package:resources/resources.dart';
import 'package:base_architecture_flutter/src/app.dart';

@RoutePage()
class SalesPage extends StatefulWidget {
  const SalesPage({super.key});

  @override
  State<StatefulWidget> createState() {
    return _SalesPageState();
  }
}

class _SalesPageState extends BasePageState<SalesPage, SalesBloc> {
  @override
  Widget buildPage(BuildContext context) {
    return CommonScaffold(
      body: Center(
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(AppColors.current.primaryColor),
          ),
          onPressed: () {
            navigator.push(const AppRouteInfo.login());
          },
          child: Text(
            S.current.sales,
            style: AppTextStyles.s14w400Primary(),
          ),
        ),
      ),
    );
  }
}
