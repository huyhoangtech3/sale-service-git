import 'dart:async';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:base_architecture_flutter/src/app.dart';

@Injectable()
class SalesBloc extends BaseBloc<SalesEvent, SalesState> {
  SalesBloc() : super(const SalesState()) {
    on<SalesPageInitiated>(
      _onSalesPageInitiated,
      transformer: log(),
    );
  }

  FutureOr<void> _onSalesPageInitiated(SalesPageInitiated event, Emitter<SalesState> emit) {}
}
