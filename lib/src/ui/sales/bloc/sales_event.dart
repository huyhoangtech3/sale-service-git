import 'package:freezed_annotation/freezed_annotation.dart';

import 'package:base_architecture_flutter/src/base/bloc/base_bloc_event.dart';

part 'sales_event.freezed.dart';

abstract class SalesEvent extends BaseBlocEvent {
  const SalesEvent();
}

@freezed
class SalesPageInitiated extends SalesEvent with _$SalesPageInitiated {
  const factory SalesPageInitiated({
    required String id,
  }) = _SalesPageInitiated;
}
