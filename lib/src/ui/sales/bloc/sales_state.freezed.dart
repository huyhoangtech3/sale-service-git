// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'sales_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$SalesState {
  String get id => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $SalesStateCopyWith<SalesState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SalesStateCopyWith<$Res> {
  factory $SalesStateCopyWith(
          SalesState value, $Res Function(SalesState) then) =
      _$SalesStateCopyWithImpl<$Res, SalesState>;
  @useResult
  $Res call({String id});
}

/// @nodoc
class _$SalesStateCopyWithImpl<$Res, $Val extends SalesState>
    implements $SalesStateCopyWith<$Res> {
  _$SalesStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$SalesStateImplCopyWith<$Res>
    implements $SalesStateCopyWith<$Res> {
  factory _$$SalesStateImplCopyWith(
          _$SalesStateImpl value, $Res Function(_$SalesStateImpl) then) =
      __$$SalesStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String id});
}

/// @nodoc
class __$$SalesStateImplCopyWithImpl<$Res>
    extends _$SalesStateCopyWithImpl<$Res, _$SalesStateImpl>
    implements _$$SalesStateImplCopyWith<$Res> {
  __$$SalesStateImplCopyWithImpl(
      _$SalesStateImpl _value, $Res Function(_$SalesStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
  }) {
    return _then(_$SalesStateImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$SalesStateImpl implements _SalesState {
  const _$SalesStateImpl({this.id = ''});

  @override
  @JsonKey()
  final String id;

  @override
  String toString() {
    return 'SalesState(id: $id)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SalesStateImpl &&
            (identical(other.id, id) || other.id == id));
  }

  @override
  int get hashCode => Object.hash(runtimeType, id);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$SalesStateImplCopyWith<_$SalesStateImpl> get copyWith =>
      __$$SalesStateImplCopyWithImpl<_$SalesStateImpl>(this, _$identity);
}

abstract class _SalesState implements SalesState {
  const factory _SalesState({final String id}) = _$SalesStateImpl;

  @override
  String get id;
  @override
  @JsonKey(ignore: true)
  _$$SalesStateImplCopyWith<_$SalesStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
