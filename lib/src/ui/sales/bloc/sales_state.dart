import 'package:freezed_annotation/freezed_annotation.dart';

import 'package:base_architecture_flutter/src/base/bloc/base_bloc_state.dart';

part 'sales_state.freezed.dart';

@freezed
class SalesState extends BaseBlocState with _$SalesState {
  const factory SalesState({
    @Default('') String id,
  }) = _SalesState;
}
