import 'package:auto_route/auto_route.dart';
import 'package:domain/domain.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:base_architecture_flutter/src/app.dart';
import 'package:flutter/services.dart';
import 'package:resources/resources.dart';
import 'package:shared/shared.dart';
import 'package:tab_indicator_styler/tab_indicator_styler.dart';

@RoutePage()
class CustomerDetailPage extends StatefulWidget {
  const CustomerDetailPage({
    required this.customer,
    super.key,
  });

  final Customer customer;

  @override
  State<StatefulWidget> createState() {
    return _CustomerDetailPageState();
  }
}

class _CustomerDetailPageState extends BasePageState<CustomerDetailPage, CustomerDetailBloc> with TickerProviderStateMixin{
  bool callSelected = false;
  bool smsSelected = false;
  bool emailSelected = false;
  bool calendarSelected = false;
  bool standardSelected = false;
  late final TabController _tabController;
  int _activeIndex = 0;
  bool isExpanded = false;
  bool isExpandedTwo = false;
  bool isExpandedThree = false;
  bool isSelectedEdit = false;
  final ExpansionTileController customerInformationController = ExpansionTileController();
  final ExpansionTileController customerInformationControllerTwo = ExpansionTileController();
  final ExpansionTileController customerInformationControllerThree = ExpansionTileController();
  final List<String> _tabs = [S.current.generalInformation, S.current.serviceProduct, S.current.transactionHistory, S.current.othersInformation];

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 4, vsync: this);
    _tabController.addListener(() {
      setState(() {
        _activeIndex = _tabController.index;
      });
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget buildPage(BuildContext context) {
    return CommonScaffold(
      backgroundColor: Colors.blueGrey.shade50,
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return [
            SliverOverlapAbsorber(
              handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
              sliver: SliverAppBar(
                backgroundColor: Colors.blueGrey.shade50,
                toolbarHeight: Dimens.d400.responsive(),
                automaticallyImplyLeading: false,
                floating: true,
                pinned: true,
                snap: false,
                primary: false,
                expandedHeight: 160,
                forceElevated: innerBoxIsScrolled,
                title: Container(
                  color: Colors.transparent,
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Material(
                            color: Colors.transparent,
                            child: InkWell(
                              onTap: () => navigator.pop(),
                              child: Container(
                                padding: EdgeInsets.fromLTRB(0, 0, Dimens.d8.responsive(), 0),
                                child: Align(
                                  alignment: Alignment.topLeft,
                                  child: Assets.images.iconBack.svg(
                                      colorFilter: AppColors.current.primaryColor.let((it) => ColorFilter.mode(it, BlendMode.srcIn)),
                                      width: Dimens.d20.responsive(),
                                      height: Dimens.d20.responsive()),
                                ),
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.topLeft,
                            child: Text(
                              S.current.customerDetailTitle,
                              style: AppTextStyles.s14w500Tertiary(),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: Dimens.d48.responsive()),
                    ],
                  ),
                ),
                flexibleSpace: FlexibleSpaceBar(
                  expandedTitleScale: 1,
                  titlePadding: EdgeInsets.fromLTRB(Dimens.d12.responsive(), Dimens.d40.responsive(), Dimens.d12.responsive(), Dimens.d48.responsive()),
                  centerTitle: true,
                  title: Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.all(Radius.circular(Dimens.d6.responsive())),
                    ),
                    padding: EdgeInsets.all(Dimens.d8.responsive()),
                    child: Row(
                      children: [
                        Expanded(
                          flex: 1,
                          child: Row(
                            children: [
                              Align(
                                alignment: Alignment.centerLeft,
                                child: Container(
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                      image: widget.customer.gender == Constants.male ? Assets.images.avaMan.provider() : Assets.images.avaWoman.provider(),
                                      fit: BoxFit.fitHeight,
                                    ),
                                  ),
                                  child: SizedBox(
                                    width: Dimens.d32.responsive(), height: Dimens.d32.responsive(),
                                  ),
                                ),
                              ),
                              SizedBox(width: Dimens.d12.responsive()),
                              Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  widget.customer.customerName,
                                  style: AppTextStyles.s14w500Tertiary(),
                                ),
                              ),
                              SizedBox(width: Dimens.d48.responsive()),
                              Align(
                                alignment: Alignment.centerLeft,
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Colors.orange.shade50,
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.all(Radius.circular(Dimens.d6.responsive())),
                                  ),
                                  padding: EdgeInsets.all(Dimens.d4.responsive()),
                                  child: Text(
                                    widget.customer.customerType,
                                    style: AppTextStyles.s12w400Tertiary(),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              IconButtonCustom(
                                isSelected: callSelected,
                                icon: const Icon(Icons.call_outlined),
                                selectedIcon: const Icon(Icons.call),
                                onPressed: () {
                                  setState(() {
                                    callSelected = !callSelected;
                                    smsSelected = false;
                                    emailSelected = false;
                                    calendarSelected = false;
                                    standardSelected = false;
                                  });
                                  IntentUtils.call(uri: 'tel:${widget.customer.phoneT24}');
                                },
                              ),
                              SizedBox(width: Dimens.d8.responsive()),
                              IconButtonCustom(
                                isSelected: smsSelected,
                                icon: const Icon(Icons.sms_outlined),
                                selectedIcon: const Icon(Icons.sms),
                                onPressed: () {
                                  setState(() {
                                    smsSelected = !smsSelected;
                                    callSelected = false;
                                    emailSelected = false;
                                    calendarSelected = false;
                                    standardSelected = false;
                                  });
                                  IntentUtils.sms(uri: 'sms:${widget.customer.phoneT24}');
                                },
                              ),
                              SizedBox(width: Dimens.d8.responsive()),
                              IconButtonCustom(
                                isSelected: emailSelected,
                                icon: const Icon(Icons.email_outlined),
                                selectedIcon: const Icon(Icons.email),
                                onPressed: () {
                                  setState(() {
                                    emailSelected = !emailSelected;
                                    callSelected = false;
                                    smsSelected = false;
                                    calendarSelected = false;
                                    standardSelected = false;
                                  });
                                  IntentUtils.email(
                                      to: widget.customer.emailT24,
                                      subject: '[VinaLiving]',
                                      body: 'Hello world!'
                                  );
                                },
                              ),
                              SizedBox(width: Dimens.d8.responsive()),
                              IconButtonCustom(
                                isSelected: calendarSelected,
                                icon: const Icon(Icons.calendar_month_outlined),
                                selectedIcon: const Icon(Icons.calendar_month),
                                onPressed: () {
                                  setState(() {
                                    calendarSelected = !calendarSelected;
                                    callSelected = false;
                                    smsSelected = false;
                                    emailSelected = false;
                                    standardSelected = false;
                                  });
                                },
                              ),
                              SizedBox(width: Dimens.d8.responsive()),
                              IconButtonCustom(
                                isSelected: standardSelected,
                                icon: const Icon(Icons.shopping_bag_outlined),
                                selectedIcon: const Icon(Icons.shopping_bag),
                                onPressed: () {
                                  setState(() {
                                    standardSelected = !standardSelected;
                                    callSelected = false;
                                    smsSelected = false;
                                    emailSelected = false;
                                    calendarSelected = false;
                                  });
                                },
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                bottom: PreferredSize(
                  preferredSize: const Size(double.infinity, 50),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.all(Radius.circular(Dimens.d6.responsive())),
                    ),
                    margin: EdgeInsets.fromLTRB(Dimens.d12.responsive(), 0, Dimens.d12.responsive(), Dimens.d1.responsive()),
                    child: TabBar(
                      controller: _tabController,
                      overlayColor: MaterialStateProperty.resolveWith<Color?>(
                            (Set<MaterialState> states) {
                          if (states.contains(MaterialState.pressed)) {
                            return Colors.orangeAccent.withOpacity(0.5);
                          }
                          if (states.contains(MaterialState.hovered)) {
                            return Colors.orangeAccent.withOpacity(0.5);
                          }
                          if (states.contains(MaterialState.focused)) {
                            return Colors.orangeAccent.withOpacity(0.5);
                          }
                          return Colors.white.withOpacity(0.5);
                        },
                      ),
                      dividerColor: Colors.orangeAccent,
                      labelColor: Colors.white,
                      tabs: [
                        Tab(
                          child: Text(
                            S.current.generalInformation,
                            style: _activeIndex == 0 ? AppTextStyles.s12w500Secondary() : AppTextStyles.s12w500Tertiary(),
                          ),
                        ),
                        Tab(
                          child: Text(
                            S.current.serviceProduct,
                            style: _activeIndex == 1 ? AppTextStyles.s12w500Secondary() : AppTextStyles.s12w500Tertiary(),
                          ),
                        ),
                        Tab(
                          child: Text(
                            S.current.transactionHistory,
                            style: _activeIndex == 2 ? AppTextStyles.s12w500Secondary() : AppTextStyles.s12w500Tertiary(),
                          ),
                        ),
                        Tab(
                          child: Text(
                            S.current.otherInformation,
                            style: _activeIndex == 3 ? AppTextStyles.s12w500Secondary() : AppTextStyles.s12w500Tertiary(),
                          ),
                        ),
                      ],
                      indicator: MaterialIndicator(
                          height: 4,
                          color: Colors.orangeAccent,
                          bottomLeftRadius: 10,
                          bottomRightRadius: 10,
                          topLeftRadius: 10,
                          topRightRadius: 10,
                          tabPosition: TabPosition.bottom
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ];
        },
        body: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.all(Radius.circular(Dimens.d6.responsive())),
          ),
          margin: EdgeInsets.fromLTRB(Dimens.d12.responsive(), Dimens.d0.responsive(), Dimens.d12.responsive(), Dimens.d12.responsive()),
          padding: EdgeInsets.all(Dimens.d0.responsive()),
          child: TabBarView(
            controller: _tabController,
            children: [
              SafeArea(
                top: false,
                bottom: false,
                child: Builder(
                  builder: (BuildContext context) {
                    return CustomScrollView(
                      slivers: [
                        SliverOverlapInjector(
                          handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
                        ),
                        SliverPadding(
                          padding: EdgeInsets.fromLTRB(Dimens.d8.responsive(), Dimens.d4.responsive(), Dimens.d8.responsive(), Dimens.d4.responsive()),
                          sliver: SliverFixedExtentList(
                            itemExtent: Dimens.d540.responsive(),
                            delegate: SliverChildBuilderDelegate(
                              (BuildContext context, int index) {
                                return CustomerDetailInformation(
                                  controller: customerInformationController,
                                  controllerTwo: customerInformationControllerTwo,
                                  controllerThree: customerInformationControllerThree,
                                  isSelectedEdit: isSelectedEdit,
                                  isExpanded: isExpanded,
                                  isExpandedTwo: isExpandedTwo,
                                  isExpandedThree: isExpandedThree,
                                  onPressed: () {
                                    setState(() {
                                      isSelectedEdit = !isSelectedEdit;
                                    });
                                  },
                                  onExpansionChanged: (value) {
                                    setState(() {
                                      isExpanded = value;
                                    });
                                  },
                                  onExpansionChangedTwo: (value) {
                                    setState(() {
                                      isExpandedTwo = value;
                                    });
                                  },
                                  onExpansionChangedThree: (value) {
                                    setState(() {
                                      isExpandedThree = value;
                                    });
                                  },
                                  customer: widget.customer,
                                );
                              },
                              childCount: 1,
                            ),
                          ),
                        ),
                      ],
                    );
                  },
                ),
              ),
              SafeArea(
                top: false,
                bottom: false,
                child: Builder(
                  builder: (BuildContext context) {
                    return CustomScrollView(
                      slivers: [
                        SliverOverlapInjector(
                          handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
                        ),
                        SliverPadding(
                          padding: EdgeInsets.fromLTRB(Dimens.d8.responsive(), Dimens.d4.responsive(), Dimens.d8.responsive(), Dimens.d4.responsive()),
                          sliver: SliverFixedExtentList(
                            itemExtent: Dimens.d540.responsive(),
                            delegate: SliverChildBuilderDelegate(
                                  (BuildContext context, int index) {
                                return Text(_activeIndex.toString());
                              },
                              childCount: 1,
                            ),
                          ),
                        ),
                      ],
                    );
                  },
                ),
              ),
              SafeArea(
                top: false,
                bottom: false,
                child: Builder(
                  builder: (BuildContext context) {
                    return CustomScrollView(
                      slivers: [
                        SliverOverlapInjector(
                          handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
                        ),
                        SliverPadding(
                          padding: EdgeInsets.fromLTRB(Dimens.d8.responsive(), Dimens.d4.responsive(), Dimens.d8.responsive(), Dimens.d4.responsive()),
                          sliver: SliverFixedExtentList(
                            itemExtent: Dimens.d540.responsive(),
                            delegate: SliverChildBuilderDelegate(
                                  (BuildContext context, int index) {
                                return Text(_activeIndex.toString());
                              },
                              childCount: 1,
                            ),
                          ),
                        ),
                      ],
                    );
                  },
                ),
              ),
              SafeArea(
                top: false,
                bottom: false,
                child: Builder(
                  builder: (BuildContext context) {
                    return CustomScrollView(
                      slivers: [
                        SliverOverlapInjector(
                          handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
                        ),
                        SliverPadding(
                          padding: EdgeInsets.fromLTRB(Dimens.d8.responsive(), Dimens.d4.responsive(), Dimens.d8.responsive(), Dimens.d4.responsive()),
                          sliver: SliverFixedExtentList(
                            itemExtent: Dimens.d540.responsive(),
                            delegate: SliverChildBuilderDelegate(
                                  (BuildContext context, int index) {
                                return Text(_activeIndex.toString());
                              },
                              childCount: 1,
                            ),
                          ),
                        ),
                      ],
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
