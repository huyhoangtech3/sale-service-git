// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'customer_detail_event.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$CustomerDetailPageInitiated {
  int get id => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CustomerDetailPageInitiatedCopyWith<CustomerDetailPageInitiated>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CustomerDetailPageInitiatedCopyWith<$Res> {
  factory $CustomerDetailPageInitiatedCopyWith(
          CustomerDetailPageInitiated value,
          $Res Function(CustomerDetailPageInitiated) then) =
      _$CustomerDetailPageInitiatedCopyWithImpl<$Res,
          CustomerDetailPageInitiated>;
  @useResult
  $Res call({int id});
}

/// @nodoc
class _$CustomerDetailPageInitiatedCopyWithImpl<$Res,
        $Val extends CustomerDetailPageInitiated>
    implements $CustomerDetailPageInitiatedCopyWith<$Res> {
  _$CustomerDetailPageInitiatedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$CustomerDetailPageInitiatedImplCopyWith<$Res>
    implements $CustomerDetailPageInitiatedCopyWith<$Res> {
  factory _$$CustomerDetailPageInitiatedImplCopyWith(
          _$CustomerDetailPageInitiatedImpl value,
          $Res Function(_$CustomerDetailPageInitiatedImpl) then) =
      __$$CustomerDetailPageInitiatedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int id});
}

/// @nodoc
class __$$CustomerDetailPageInitiatedImplCopyWithImpl<$Res>
    extends _$CustomerDetailPageInitiatedCopyWithImpl<$Res,
        _$CustomerDetailPageInitiatedImpl>
    implements _$$CustomerDetailPageInitiatedImplCopyWith<$Res> {
  __$$CustomerDetailPageInitiatedImplCopyWithImpl(
      _$CustomerDetailPageInitiatedImpl _value,
      $Res Function(_$CustomerDetailPageInitiatedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
  }) {
    return _then(_$CustomerDetailPageInitiatedImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$CustomerDetailPageInitiatedImpl
    implements _CustomerDetailPageInitiated {
  const _$CustomerDetailPageInitiatedImpl({required this.id});

  @override
  final int id;

  @override
  String toString() {
    return 'CustomerDetailPageInitiated(id: $id)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CustomerDetailPageInitiatedImpl &&
            (identical(other.id, id) || other.id == id));
  }

  @override
  int get hashCode => Object.hash(runtimeType, id);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CustomerDetailPageInitiatedImplCopyWith<_$CustomerDetailPageInitiatedImpl>
      get copyWith => __$$CustomerDetailPageInitiatedImplCopyWithImpl<
          _$CustomerDetailPageInitiatedImpl>(this, _$identity);
}

abstract class _CustomerDetailPageInitiated
    implements CustomerDetailPageInitiated {
  const factory _CustomerDetailPageInitiated({required final int id}) =
      _$CustomerDetailPageInitiatedImpl;

  @override
  int get id;
  @override
  @JsonKey(ignore: true)
  _$$CustomerDetailPageInitiatedImplCopyWith<_$CustomerDetailPageInitiatedImpl>
      get copyWith => throw _privateConstructorUsedError;
}
