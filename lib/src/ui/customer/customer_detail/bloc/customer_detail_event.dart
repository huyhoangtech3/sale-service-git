import 'package:freezed_annotation/freezed_annotation.dart';

import 'package:base_architecture_flutter/src/base/bloc/base_bloc_event.dart';

part 'customer_detail_event.freezed.dart';

abstract class CustomerDetailEvent extends BaseBlocEvent {
  const CustomerDetailEvent();
}

@freezed
class CustomerDetailPageInitiated extends CustomerDetailEvent with _$CustomerDetailPageInitiated {
  const factory CustomerDetailPageInitiated({
    required int id,
  }) = _CustomerDetailPageInitiated;
}
