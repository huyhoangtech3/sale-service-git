// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'customer_detail_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$CustomerDetailState {
  String get id => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CustomerDetailStateCopyWith<CustomerDetailState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CustomerDetailStateCopyWith<$Res> {
  factory $CustomerDetailStateCopyWith(
          CustomerDetailState value, $Res Function(CustomerDetailState) then) =
      _$CustomerDetailStateCopyWithImpl<$Res, CustomerDetailState>;
  @useResult
  $Res call({String id});
}

/// @nodoc
class _$CustomerDetailStateCopyWithImpl<$Res, $Val extends CustomerDetailState>
    implements $CustomerDetailStateCopyWith<$Res> {
  _$CustomerDetailStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$CustomerDetailStateImplCopyWith<$Res>
    implements $CustomerDetailStateCopyWith<$Res> {
  factory _$$CustomerDetailStateImplCopyWith(_$CustomerDetailStateImpl value,
          $Res Function(_$CustomerDetailStateImpl) then) =
      __$$CustomerDetailStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String id});
}

/// @nodoc
class __$$CustomerDetailStateImplCopyWithImpl<$Res>
    extends _$CustomerDetailStateCopyWithImpl<$Res, _$CustomerDetailStateImpl>
    implements _$$CustomerDetailStateImplCopyWith<$Res> {
  __$$CustomerDetailStateImplCopyWithImpl(_$CustomerDetailStateImpl _value,
      $Res Function(_$CustomerDetailStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
  }) {
    return _then(_$CustomerDetailStateImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$CustomerDetailStateImpl implements _CustomerDetailState {
  const _$CustomerDetailStateImpl({this.id = ''});

  @override
  @JsonKey()
  final String id;

  @override
  String toString() {
    return 'CustomerDetailState(id: $id)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CustomerDetailStateImpl &&
            (identical(other.id, id) || other.id == id));
  }

  @override
  int get hashCode => Object.hash(runtimeType, id);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CustomerDetailStateImplCopyWith<_$CustomerDetailStateImpl> get copyWith =>
      __$$CustomerDetailStateImplCopyWithImpl<_$CustomerDetailStateImpl>(
          this, _$identity);
}

abstract class _CustomerDetailState implements CustomerDetailState {
  const factory _CustomerDetailState({final String id}) =
      _$CustomerDetailStateImpl;

  @override
  String get id;
  @override
  @JsonKey(ignore: true)
  _$$CustomerDetailStateImplCopyWith<_$CustomerDetailStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
