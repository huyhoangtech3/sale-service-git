import 'package:freezed_annotation/freezed_annotation.dart';

import 'package:base_architecture_flutter/src/base/bloc/base_bloc_state.dart';

part 'customer_detail_state.freezed.dart';

@freezed
class CustomerDetailState extends BaseBlocState with _$CustomerDetailState {
  const factory CustomerDetailState({
    @Default('') String id,
  }) = _CustomerDetailState;
}
