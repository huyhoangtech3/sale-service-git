import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

import 'package:base_architecture_flutter/src/app.dart';

@Injectable()
class CustomerDetailBloc extends BaseBloc<CustomerDetailEvent, CustomerDetailState> {
  CustomerDetailBloc() : super(const CustomerDetailState()) {
    on<CustomerDetailPageInitiated>(
      _onCustomerDetailPageInitiated,
      transformer: log(),
    );
  }

  FutureOr<void> _onCustomerDetailPageInitiated(
      CustomerDetailPageInitiated event,
      Emitter<CustomerDetailState> emit,
      ) {}
}
