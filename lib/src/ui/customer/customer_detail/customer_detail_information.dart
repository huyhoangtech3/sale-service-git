import 'package:domain/domain.dart';
import 'package:flutter/material.dart';

import 'package:base_architecture_flutter/src/app.dart';
import 'package:resources/resources.dart';

class CustomerDetailInformation extends StatelessWidget {
  const CustomerDetailInformation({
    required this.controller,
    required this.controllerTwo,
    required this.controllerThree,
    this.isSelectedEdit,
    this.isExpanded,
    this.isExpandedTwo,
    this.isExpandedThree,
    this.onPressed,
    this.onExpansionChanged,
    this.onExpansionChangedTwo,
    this.onExpansionChangedThree,
    required this.customer,
    super.key,
  });

  final ExpansionTileController controller, controllerTwo, controllerThree;
  final bool? isSelectedEdit;
  final bool? isExpanded, isExpandedTwo, isExpandedThree;
  final VoidCallback? onPressed;
  final ValueChanged<bool>? onExpansionChanged, onExpansionChangedTwo, onExpansionChangedThree;
  final Customer customer;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ExpansionTile(
          visualDensity: const VisualDensity(horizontal: -3),
          dense: true,
          tilePadding: EdgeInsets.zero,
          controller: controller,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                S.current.customerInformation,
                style: AppTextStyles.s14w500Tertiary(),
              ),
              IconButtonCustom(
                isSelected: isSelectedEdit,
                icon: const Icon(Icons.edit_outlined),
                selectedIcon: const Icon(Icons.edit),
                onPressed: onPressed,
              ),
            ],
          ),
          onExpansionChanged: onExpansionChanged,
          trailing: IconButtonCustom(
            isSelected: isExpanded,
            icon: const Icon(Icons.keyboard_arrow_up),
            selectedIcon: const Icon(Icons.keyboard_arrow_down),
            onPressed: () {
              controller.isExpanded ?
              controller.collapse() :
              controller.expand();
            },
          ),
          initiallyExpanded: true,
          collapsedShape: const RoundedRectangleBorder(
            side: BorderSide.none,
          ),
          shape: const RoundedRectangleBorder(
            side: BorderSide.none,
          ),
          children: [
            SizedBox(height: Dimens.d4.responsive()),
            SizedBox(
              height: Dimens.d1.responsive(),
              child: Container(
                color: Colors.blueGrey.shade50,
              ),
            ),
            SizedBox(height: Dimens.d8.responsive()),
            Row(
              children: [
                Expanded(
                  flex: 1,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        S.current.customerName,
                        style: AppTextStyles.s12w300Tertiary(),
                      ),
                      SizedBox(height: Dimens.d8.responsive()),
                      Text(
                        S.current.phone,
                        style: AppTextStyles.s12w300Tertiary(),
                      ),
                      SizedBox(height: Dimens.d8.responsive()),
                      Text(
                        S.current.email,
                        style: AppTextStyles.s12w300Tertiary(),
                      ),
                      SizedBox(height: Dimens.d8.responsive()),
                      Text(
                        S.current.identification,
                        style: AppTextStyles.s12w300Tertiary(),
                      ),
                      SizedBox(height: Dimens.d8.responsive()),
                      Text(
                        S.current.block,
                        style: AppTextStyles.s12w300Tertiary(),
                      ),
                      SizedBox(height: Dimens.d8.responsive()),
                      Text(
                        S.current.sector,
                        style: AppTextStyles.s12w300Tertiary(),
                      ),
                      SizedBox(height: Dimens.d8.responsive()),
                      SizedBox(
                        height: Dimens.d48.responsive(),
                        child: Text(
                          S.current.status,
                          style: AppTextStyles.s12w300Tertiary(),
                        ),
                      ),
                      SizedBox(height: Dimens.d8.responsive()),
                    ],
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(customer.customerName, style: AppTextStyles.s12w400Tertiary()),
                      SizedBox(height: Dimens.d8.responsive()),
                      Text(customer.phoneT24, style: AppTextStyles.s12w400Tertiary()),
                      SizedBox(height: Dimens.d8.responsive()),
                      Text(customer.emailT24, style: AppTextStyles.s12w400Tertiary()),
                      SizedBox(height: Dimens.d8.responsive()),
                      Text(customer.idCard, style: AppTextStyles.s12w400Tertiary()),
                      SizedBox(height: Dimens.d8.responsive()),
                      Text(customer.customerTypeSector, style: AppTextStyles.s12w400Tertiary()),
                      SizedBox(height: Dimens.d8.responsive()),
                      Text(customer.sector, style: AppTextStyles.s12w400Tertiary()),
                      SizedBox(height: Dimens.d8.responsive()),
                      SizedBox(
                        height: Dimens.d48.responsive(),
                        child: Text(customer.status, style: AppTextStyles.s12w300Tertiary()),
                      ),
                      SizedBox(height: Dimens.d8.responsive()),
                    ],
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        S.current.sex,
                        style: AppTextStyles.s12w300Tertiary(),
                      ),
                      SizedBox(height: Dimens.d8.responsive()),
                      Text(
                        S.current.maritalStatus,
                        style: AppTextStyles.s12w300Tertiary(),
                      ),
                      SizedBox(height: Dimens.d8.responsive()),
                      Text(
                        S.current.dateOfBirth,
                        style: AppTextStyles.s12w300Tertiary(),
                      ),
                      SizedBox(height: Dimens.d8.responsive()),
                      Text(
                        S.current.ageGroup,
                        style: AppTextStyles.s12w300Tertiary(),
                      ),
                      SizedBox(height: Dimens.d8.responsive()),
                      Text(
                        S.current.nationality,
                        style: AppTextStyles.s12w300Tertiary(),
                      ),
                      SizedBox(height: Dimens.d8.responsive()),
                      Text(
                        S.current.branchManagement,
                        style: AppTextStyles.s12w300Tertiary(),
                      ),
                      SizedBox(height: Dimens.d8.responsive()),
                      SizedBox(
                        height: Dimens.d48.responsive(),
                        child: Text(
                          S.current.address,
                          style: AppTextStyles.s12w300Tertiary(),
                        ),
                      ),
                      SizedBox(height: Dimens.d8.responsive()),
                    ],
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(customer.gender, style: AppTextStyles.s12w400Tertiary()),
                      SizedBox(height: Dimens.d8.responsive()),
                      Text(customer.status, style: AppTextStyles.s12w400Tertiary()),
                      SizedBox(height: Dimens.d8.responsive()),
                      Text(customer.dateOfBirth, style: AppTextStyles.s12w400Tertiary()),
                      SizedBox(height: Dimens.d8.responsive()),
                      Text(customer.district, style: AppTextStyles.s12w400Tertiary()),
                      SizedBox(height: Dimens.d8.responsive()),
                      Text(customer.nationCode, style: AppTextStyles.s12w400Tertiary()),
                      SizedBox(height: Dimens.d8.responsive()),
                      Text(customer.branchCode, style: AppTextStyles.s12w400Tertiary()),
                      SizedBox(height: Dimens.d8.responsive()),
                      SizedBox(
                        height: Dimens.d48.responsive(),
                        child: Text(customer.address, style: AppTextStyles.s12w400Tertiary()),
                      ),
                      SizedBox(height: Dimens.d8.responsive()),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
        SizedBox(
          height: Dimens.d1.responsive(),
          child: Container(
            color: Colors.orangeAccent,
          ),
        ),
        SizedBox(height: Dimens.d4.responsive()),
        ExpansionTile(
          visualDensity: const VisualDensity(horizontal: -3),
          dense: true,
          tilePadding: EdgeInsets.zero,
          controller: controllerTwo,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                S.current.incomeAndJob,
                style: AppTextStyles.s14w500Tertiary(),
              ),
            ],
          ),
          onExpansionChanged: onExpansionChangedTwo,
          trailing: IconButtonCustom(
            isSelected: isExpandedTwo,
            icon: const Icon(Icons.keyboard_arrow_up),
            selectedIcon: const Icon(Icons.keyboard_arrow_down),
            onPressed: () {
              controllerTwo.isExpanded ?
              controllerTwo.collapse() :
              controllerTwo.expand();
            },
          ),
          initiallyExpanded: false,
          collapsedShape: const RoundedRectangleBorder(
            side: BorderSide.none,
          ),
          shape: const RoundedRectangleBorder(
            side: BorderSide.none,
          ),
          children: [
            SizedBox(height: Dimens.d4.responsive()),
            SizedBox(
              height: Dimens.d1.responsive(),
              child: Container(
                color: Colors.blueGrey.shade50,
              ),
            ),
            SizedBox(height: Dimens.d8.responsive()),
            Row(
              children: [
                Expanded(
                  flex: 1,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        S.current.job,
                        style: AppTextStyles.s12w300Tertiary(),
                      ),
                      SizedBox(height: Dimens.d8.responsive()),
                      Text(
                        S.current.incomeVnd,
                        style: AppTextStyles.s12w300Tertiary(),
                      ),
                      SizedBox(height: Dimens.d8.responsive()),
                      Text(
                        S.current.averageSalaryThreeMonths,
                        style: AppTextStyles.s12w300Tertiary(),
                      ),
                      SizedBox(height: Dimens.d8.responsive()),
                    ],
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(customer.customerName, style: AppTextStyles.s12w400Tertiary()),
                      SizedBox(height: Dimens.d8.responsive()),
                      Text(customer.phoneT24, style: AppTextStyles.s12w400Tertiary()),
                      SizedBox(height: Dimens.d8.responsive()),
                      Text(customer.emailT24, style: AppTextStyles.s12w400Tertiary()),
                      SizedBox(height: Dimens.d8.responsive()),
                    ],
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        S.current.companyPaysSalary,
                        style: AppTextStyles.s12w300Tertiary(),
                      ),
                      SizedBox(height: Dimens.d8.responsive()),
                      Text(
                        S.current.formality,
                        style: AppTextStyles.s12w300Tertiary(),
                      ),
                      SizedBox(height: Dimens.d36.responsive()),
                    ],
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(customer.gender, style: AppTextStyles.s12w400Tertiary()),
                      SizedBox(height: Dimens.d8.responsive()),
                      Text(customer.gender, style: AppTextStyles.s12w400Tertiary()),
                      SizedBox(height: Dimens.d36.responsive()),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
        SizedBox(
          height: Dimens.d1.responsive(),
          child: Container(
            color: Colors.orangeAccent,
          ),
        ),
        SizedBox(height: Dimens.d4.responsive()),
        ExpansionTile(
          visualDensity: const VisualDensity(horizontal: -3),
          dense: true,
          tilePadding: EdgeInsets.zero,
          controller: controllerThree,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                S.current.othersInformation,
                style: AppTextStyles.s14w500Tertiary(),
              ),
            ],
          ),
          onExpansionChanged: onExpansionChangedThree,
          trailing: IconButtonCustom(
            isSelected: isExpandedThree,
            icon: const Icon(Icons.keyboard_arrow_up),
            selectedIcon: const Icon(Icons.keyboard_arrow_down),
            onPressed: () {
              controllerThree.isExpanded ?
              controllerThree.collapse() :
              controllerThree.expand();
            },
          ),
          initiallyExpanded: false,
          collapsedShape: const RoundedRectangleBorder(
            side: BorderSide.none,
          ),
          shape: const RoundedRectangleBorder(
            side: BorderSide.none,
          ),
          children: [
            SizedBox(height: Dimens.d4.responsive()),
            SizedBox(
              height: Dimens.d1.responsive(),
              child: Container(
                color: Colors.blueGrey.shade50,
              ),
            ),
            SizedBox(height: Dimens.d8.responsive()),
            Row(
              children: [
                Expanded(
                  flex: 1,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        S.current.codeOpenDate,
                        style: AppTextStyles.s12w300Tertiary(),
                      ),
                      SizedBox(height: Dimens.d8.responsive()),
                      Text(
                        S.current.currentDeptMB,
                        style: AppTextStyles.s12w300Tertiary(),
                      ),
                      SizedBox(height: Dimens.d8.responsive()),
                      Text(
                        S.current.payroll,
                        style: AppTextStyles.s12w300Tertiary(),
                      ),
                      SizedBox(height: Dimens.d8.responsive()),
                    ],
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(customer.customerName, style: AppTextStyles.s12w400Tertiary()),
                      SizedBox(height: Dimens.d8.responsive()),
                      Text(customer.phoneT24, style: AppTextStyles.s12w400Tertiary()),
                      SizedBox(height: Dimens.d8.responsive()),
                      Text(customer.emailT24, style: AppTextStyles.s12w400Tertiary()),
                      SizedBox(height: Dimens.d8.responsive()),
                    ],
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        S.current.timeRelationshipMB,
                        style: AppTextStyles.s12w300Tertiary(),
                      ),
                      SizedBox(height: Dimens.d8.responsive()),
                      Text(
                        S.current.deptMB,
                        style: AppTextStyles.s12w300Tertiary(),
                      ),
                      SizedBox(height: Dimens.d36.responsive()),
                    ],
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(customer.gender, style: AppTextStyles.s12w400Tertiary()),
                      SizedBox(height: Dimens.d8.responsive()),
                      Text(customer.status, style: AppTextStyles.s12w400Tertiary()),
                      SizedBox(height: Dimens.d36.responsive()),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ],
    );
  }
}
