import 'dart:async';

import 'package:auto_route/auto_route.dart';
import 'package:domain/domain.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:resources/resources.dart';
import 'package:base_architecture_flutter/src/app.dart';
import 'package:shared/shared.dart';

@RoutePage()
class CustomerPage extends StatefulWidget {
  const CustomerPage({super.key});

  @override
  State<StatefulWidget> createState() {
    return _CustomerPageState();
  }
}

class _CustomerPageState extends BasePageState<CustomerPage, CustomerBloc> {
  static List<String> listType = <String>[S.current.private, S.current.public];
  static List<String> listStatus = <String>[S.current.active, S.current.inactive];
  late final _pagingController = CommonPagingController<Customer>()..disposeBy(disposeBag);
  FocusNode focusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    focusNode = FocusNode();
    bloc.add(const CustomerSearch(search: ''));
    bloc.add(const CustomerPageInitiated());
    _pagingController.listen(
      onLoadMore: () => bloc.add(const CustomerLoadMore()),
    );
  }

  @override
  Widget buildPageListeners({required Widget child}) {
    return MultiBlocListener(
      listeners: [
        BlocListener<CustomerBloc, CustomerState>(
          listenWhen: (previous, current) => previous.customers != current.customers,
          listener: (context, state) {
            _pagingController.appendLoadMoreOutput(state.customers);
          },
        ),
        BlocListener<CustomerBloc, CustomerState>(
          listenWhen: (previous, current) =>
          previous.loadCustomersException != current.loadCustomersException,
          listener: (context, state) {
            _pagingController.error = state.loadCustomersException;
          },
        ),
      ],
      child: child,
    );
  }

  @override
  Widget buildPage(BuildContext context) {
    return CommonScaffold(
      body: SafeArea(
        child: Container(
          color: Colors.white,
          child: Container(
            padding: EdgeInsets.all(Dimens.d12.responsive()),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(height: Dimens.d3.responsive()),
                  BlocBuilder<CustomerBloc, CustomerState>(
                    buildWhen: (previous, current) =>
                    previous.customers != current.customers ||
                    previous.isShimmerLoading != current.isShimmerLoading,
                    builder: (context, state) {
                      return AppTextFormField(
                        contentPadding: EdgeInsets.symmetric(horizontal: Dimens.d8.responsive()),
                        label: S.current.search,
                        labelStyle: AppTextStyles.s12w300Tertiary(),
                        style: AppTextStyles.s12w400Tertiary(),
                        hintStyle: AppTextStyles.s12w200Tertiary(),
                        hintText: S.current.customer,
                        onChanged: (value) {
                          bloc.add(CustomerSearch(search: value));
                        },
                        keyboardType: TextInputType.text,
                        textInputAction: TextInputAction.done,
                        focusNode: focusNode,
                        onFieldSubmitted: (value){
                          bloc.add(CustomerSearch(search: value));
                          bloc.add(const CustomerPageInitiated());
                        },
                        onTapOutside: (event) {
                          FocusScope.of(context).unfocus();
                        },
                      );
                    }
                  ),
                  SizedBox(height: Dimens.d8.responsive()),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      LayoutBuilder(builder: (context, constraints) {
                        return DropdownMenu<String>(
                          // controller: ctrDate,
                          textStyle: AppTextStyles.s12w400Tertiary(),
                          inputDecorationTheme: InputDecorationTheme(
                            isDense: true,
                            contentPadding: EdgeInsets.symmetric(horizontal: Dimens.d8.responsive()),
                            constraints: BoxConstraints.tight(Size.fromHeight(Dimens.d40.responsive())),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8),
                            ),
                          ),
                          initialSelection: listType.first,
                          onSelected: (value) {
                            // bloc.add(DateUnitTextFieldChanged(dateUnit: value!));
                          },
                          dropdownMenuEntries: listType.map<DropdownMenuEntry<String>>((String value) {
                            return DropdownMenuEntry<String>(
                              value: value,
                              label: value,
                              labelWidget: Text(
                                value,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: AppTextStyles.s12w400Tertiary(),
                              ),
                              style: ButtonStyle(
                                textStyle: MaterialStateTextStyle.resolveWith(
                                      (states) => AppTextStyles.s12w400Tertiary(),
                                ),
                              ),
                            );
                          }).toList(),
                        );
                      }),
                      SizedBox(width: Dimens.d8.responsive()),
                      LayoutBuilder(builder: (context, constraints) {
                        return DropdownMenu<String>(
                          // controller: ctrDate,
                          textStyle: AppTextStyles.s12w400Tertiary(),
                          inputDecorationTheme: InputDecorationTheme(
                            isDense: true,
                            contentPadding: EdgeInsets.symmetric(horizontal: Dimens.d8.responsive()),
                            constraints: BoxConstraints.tight(Size.fromHeight(Dimens.d40.responsive())),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8),
                            ),
                          ),
                          initialSelection: listStatus.first,
                          onSelected: (value) {
                            // bloc.add(DateUnitTextFieldChanged(dateUnit: value!));
                          },
                          dropdownMenuEntries: listStatus.map<DropdownMenuEntry<String>>((String value) {
                            return DropdownMenuEntry<String>(
                              value: value,
                              label: value,
                              labelWidget: Text(
                                value,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: AppTextStyles.s12w400Tertiary(),
                              ),
                              style: ButtonStyle(
                                textStyle: MaterialStateTextStyle.resolveWith(
                                      (states) => AppTextStyles.s12w400Tertiary(),
                                ),
                              ),
                            );
                          }).toList(),
                        );
                      }),
                    ],
                  ),
                  SizedBox(height: Dimens.d4.responsive()),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        flex: 1,
                        child: BlocBuilder<CustomerBloc, CustomerState>(
                                buildWhen: (previous, current) =>
                                previous.customers != current.customers ||
                                previous.isShimmerLoading != current.isShimmerLoading,
                                builder: (context, state) {
                                  return ElevatedButton(
                                    onPressed: () {
                                      bloc.add(const CustomerPageInitiated());
                                    },
                                    style: ButtonStyle(
                                      backgroundColor: MaterialStateProperty.all(AppColors.current.primaryColor),
                                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                        RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(6.0),
                                        ),
                                      ),
                                    ),
                                    child: Text(
                                      S.current.search,
                                      style: AppTextStyles.s14w400Primary(),
                                    ),
                                  );
                                }
                              ),
                      ),
                      SizedBox(width: Dimens.d8.responsive()),
                      Expanded(
                        flex: 1,
                        child: ElevatedButton(
                          onPressed: () {

                          },
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all(AppColors.current.secondaryColor),
                            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(6.0),
                              ),
                            ),
                          ),
                          child: Text(
                            S.current.advanceSearch,
                            style: AppTextStyles.s14w400Primary(),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: Dimens.d4.responsive()),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      BlocBuilder<CustomerBloc, CustomerState>(
                        buildWhen: (previous, current) =>
                        previous.customers != current.customers ||
                            previous.isShimmerLoading != current.isShimmerLoading,
                        builder: (context, state) {
                          return Text(
                            ('${state.record} ${S.current.record}'),
                            style: AppTextStyles.s10w400Tertiary(),
                          );
                        }
                      ),
                      InkWell(
                        onTap: () => navigator.pop(),
                        child: DecoratedBox(
                          decoration: BoxDecoration(
                            color: Colors.blueGrey[50],
                            shape: BoxShape.rectangle,
                            borderRadius: const BorderRadius.all(Radius.circular(6.0)),
                          ),
                          child: Container(
                            padding: EdgeInsets.all(Dimens.d4.responsive()),
                            child: Text(
                              S.current.sort,
                              style: AppTextStyles.s10w400Tertiary(),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: Dimens.d4.responsive()),
                  SizedBox(
                    height: Dimens.d360.responsive(),
                    child: BlocBuilder<CustomerBloc, CustomerState>(
                      buildWhen: (previous, current) =>
                      previous.customers != current.customers ||
                          previous.isShimmerLoading != current.isShimmerLoading,
                      builder: (context, state) {
                        return RefreshIndicator(
                          onRefresh: () {
                            final completer = Completer<void>();
                            bloc.add(CustomerPageRefreshed(completer: completer));
                            return completer.future;
                          },
                          child: state.isShimmerLoading && state.customers.data.isEmpty
                              ? const ListViewLoader()
                              : CommonPagedListView<Customer>(
                            scrollDirection: Axis.vertical,
                            shrinkWrap: true,
                            physics: const ScrollPhysics(),
                            pagingController: _pagingController,
                            itemBuilder: (context, customer, index) {
                              return Padding(
                                padding: EdgeInsets.symmetric(
                                  horizontal: Dimens.d8.responsive(),
                                  vertical: Dimens.d4.responsive(),
                                ),
                                child: ShimmerLoading(
                                  isLoading: state.isShimmerLoading,
                                  loadingWidget: const LoadingItem(),
                                  child: GestureDetector(
                                    onTap: () async {
                                      await navigator.push(AppRouteInfo.customerDetail(customer));
                                    },
                                    child: Container(
                                      alignment: Alignment.centerLeft,
                                      padding: EdgeInsets.all(Dimens.d8.responsive()),
                                      decoration: BoxDecoration(
                                        color: Colors.grey.shade50,
                                        borderRadius: BorderRadius.circular(Dimens.d8.responsive()),
                                      ),
                                      width: double.infinity,
                                      height: Dimens.d52.responsive(),
                                      child: Column(
                                        children: [
                                          Row(
                                            children: [
                                              Expanded(
                                                flex: 1,
                                                child: Align(
                                                  alignment: Alignment.centerLeft,
                                                  child: Row(
                                                    children: [
                                                      Align(
                                                        alignment: Alignment.centerLeft,
                                                        child: Container(
                                                          decoration: BoxDecoration(
                                                            image: DecorationImage(
                                                              image: customer.gender == Constants.male ? Assets.images.avaMan.provider() : Assets.images.avaWoman.provider(),
                                                              fit: BoxFit.fitHeight,
                                                            ),
                                                          ),
                                                          child: SizedBox(
                                                            width: Dimens.d28.responsive(), height: Dimens.d28.responsive(),
                                                          ),
                                                        ),
                                                      ),
                                                      SizedBox(width: Dimens.d16.responsive()),
                                                      Align(
                                                        alignment: Alignment.centerLeft,
                                                        child: Column(
                                                          crossAxisAlignment: CrossAxisAlignment.start,
                                                          children: [
                                                            Text(customer.customerName, style: AppTextStyles.s12w400Tertiary()),
                                                            Text('${S.current.idCard}: ${customer.customerCode}', style: AppTextStyles.s12w400Tertiary()),
                                                          ],
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              Expanded(
                                                flex: 1,
                                                child: Align(
                                                  alignment: Alignment.centerLeft,
                                                  child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: [
                                                      Text('${S.current.customerCode}: ${customer.customerCode}', style: AppTextStyles.s12w400Tertiary()),
                                                      Text('${S.current.branchCode}: ${customer.branchCode}', style: AppTextStyles.s12w400Tertiary()),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              Expanded(
                                                flex: 1,
                                                child: Align(
                                                  alignment: Alignment.centerLeft,
                                                  child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: [
                                                      Text('${S.current.email}: ${customer.emailT24}', style: AppTextStyles.s12w400Tertiary(), overflow: TextOverflow.ellipsis),
                                                      Text('${S.current.phone}: ${customer.phoneT24}', style: AppTextStyles.s12w400Tertiary()),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              Expanded(
                                                flex: 1,
                                                child: Align(
                                                  alignment: Alignment.centerLeft,
                                                  child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: [
                                                      Text('${S.current.dateOfBirth}: ${customer.dateOfBirth}', style: AppTextStyles.s12w400Tertiary()),
                                                      Text('${S.current.group}: ${customer.customerType}', style: AppTextStyles.s12w400Tertiary()),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            },
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}