import 'package:freezed_annotation/freezed_annotation.dart';
import 'dart:async';
import 'package:base_architecture_flutter/src/base/bloc/base_bloc_event.dart';

part 'customer_event.freezed.dart';

abstract class CustomerEvent extends BaseBlocEvent {
  const CustomerEvent();
}

@freezed
class CustomerPageInitiated extends CustomerEvent with _$CustomerPageInitiated {
  const factory CustomerPageInitiated() = _CustomerPageInitiated;
}

@freezed
class CustomerPageRefreshed extends CustomerEvent with _$CustomerPageRefreshed {
  const factory CustomerPageRefreshed({
    required Completer<void> completer,
  }) = _CustomerPageRefreshed;
}

@freezed
class CustomerLoadMore extends CustomerEvent with _$CustomerLoadMore {
  const factory CustomerLoadMore() = _CustomerLoadMore;
}

@freezed
class CustomerSearch extends CustomerEvent with _$CustomerSearch {
  const factory CustomerSearch({
    required String search,
  }) = _CustomerSearch;
}