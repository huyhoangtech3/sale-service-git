import 'dart:async';
import 'package:domain/domain.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:base_architecture_flutter/src/app.dart';
import 'package:shared/shared.dart';

@Injectable()
class CustomerBloc extends BaseBloc<CustomerEvent, CustomerState> {
  CustomerBloc(this._customerUseCase) : super(CustomerState()) {
    on<CustomerPageInitiated>(
      _onCustomerPageInitiated,
      transformer: log(),
    );

    on<CustomerLoadMore>(
      _onCustomerLoadMore,
      transformer: log(),
    );

    on<CustomerPageRefreshed>(
      _onCustomerPageRefreshed,
      transformer: log(),
    );

    on<CustomerSearch>(
      _onCustomerSearch,
      transformer: distinct(),
    );
  }

  final CustomerUseCase _customerUseCase;

  FutureOr<void> _onCustomerPageInitiated(CustomerPageInitiated event, Emitter<CustomerState> emit) async {
    await _getCustomers(
      emit: emit,
      isInitialLoad: true,
      doOnSubscribe: () async {
        emit(state.copyWith(isShimmerLoading: true));
        emit(state.copyWith(record: state.customers.data.length));
      },
      doOnSuccessOrError: () async => emit(state.copyWith(isShimmerLoading: false)),
    );
  }

  FutureOr<void> _onCustomerLoadMore(CustomerLoadMore event, Emitter<CustomerState> emit) async {
    await _getCustomers(
      emit: emit,
      isInitialLoad: false,
      doOnSubscribe: () async {
        emit(state.copyWith(record: state.record + PagingConstants.itemsPerPage));
      },
    );
  }

  FutureOr<void> _onCustomerPageRefreshed(CustomerPageRefreshed event, Emitter<CustomerState> emit) async {
    await _getCustomers(
      emit: emit,
      isInitialLoad: true,
      doOnSubscribe: () async {
        emit(state.copyWith(isShimmerLoading: true));
        emit(state.copyWith(record: state.customers.data.length));
      },
      doOnSuccessOrError: () async {
        emit(state.copyWith(isShimmerLoading: false));
        if (!event.completer.isCompleted) {
          event.completer.complete();
        }
      },
    );
  }

  Future<void> _getCustomers({
    required Emitter<CustomerState> emit,
    required bool isInitialLoad,
    Future<void> Function()? doOnSubscribe,
    Future<void> Function()? doOnSuccessOrError,
  }) async {
    return runBlocCatching(
      action: () async {
        emit(state.copyWith(loadCustomersException: null));
        final output = await _customerUseCase.execute(GetCustomerInput(
            search: state.search,
            searchFastType: state.searchFastType
        ), isInitialLoad);
        emit(state.copyWith(customers: output));
      },
      doOnError: (e) async {
        emit(state.copyWith(loadCustomersException: e));
      },
      doOnSubscribe: doOnSubscribe,
      doOnSuccessOrError: doOnSuccessOrError,
      handleLoading: false,
      maxRetries: 3,
    );
  }

  void _onCustomerSearch(CustomerSearch event, Emitter<CustomerState> emit) {
    emit(state.copyWith(
      search: event.search,
    ));
  }
}
