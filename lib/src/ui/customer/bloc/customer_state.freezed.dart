// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'customer_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$CustomerState {
  String get search => throw _privateConstructorUsedError;
  String get searchFastType => throw _privateConstructorUsedError;
  int get record => throw _privateConstructorUsedError;
  LoadMoreOutput<Customer> get customers => throw _privateConstructorUsedError;
  bool get isShimmerLoading => throw _privateConstructorUsedError;
  AppException? get loadCustomersException =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CustomerStateCopyWith<CustomerState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CustomerStateCopyWith<$Res> {
  factory $CustomerStateCopyWith(
          CustomerState value, $Res Function(CustomerState) then) =
      _$CustomerStateCopyWithImpl<$Res, CustomerState>;
  @useResult
  $Res call(
      {String search,
      String searchFastType,
      int record,
      LoadMoreOutput<Customer> customers,
      bool isShimmerLoading,
      AppException? loadCustomersException});

  $LoadMoreOutputCopyWith<Customer, $Res> get customers;
}

/// @nodoc
class _$CustomerStateCopyWithImpl<$Res, $Val extends CustomerState>
    implements $CustomerStateCopyWith<$Res> {
  _$CustomerStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? search = null,
    Object? searchFastType = null,
    Object? record = null,
    Object? customers = null,
    Object? isShimmerLoading = null,
    Object? loadCustomersException = freezed,
  }) {
    return _then(_value.copyWith(
      search: null == search
          ? _value.search
          : search // ignore: cast_nullable_to_non_nullable
              as String,
      searchFastType: null == searchFastType
          ? _value.searchFastType
          : searchFastType // ignore: cast_nullable_to_non_nullable
              as String,
      record: null == record
          ? _value.record
          : record // ignore: cast_nullable_to_non_nullable
              as int,
      customers: null == customers
          ? _value.customers
          : customers // ignore: cast_nullable_to_non_nullable
              as LoadMoreOutput<Customer>,
      isShimmerLoading: null == isShimmerLoading
          ? _value.isShimmerLoading
          : isShimmerLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      loadCustomersException: freezed == loadCustomersException
          ? _value.loadCustomersException
          : loadCustomersException // ignore: cast_nullable_to_non_nullable
              as AppException?,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $LoadMoreOutputCopyWith<Customer, $Res> get customers {
    return $LoadMoreOutputCopyWith<Customer, $Res>(_value.customers, (value) {
      return _then(_value.copyWith(customers: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$CustomerStateImplCopyWith<$Res>
    implements $CustomerStateCopyWith<$Res> {
  factory _$$CustomerStateImplCopyWith(
          _$CustomerStateImpl value, $Res Function(_$CustomerStateImpl) then) =
      __$$CustomerStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String search,
      String searchFastType,
      int record,
      LoadMoreOutput<Customer> customers,
      bool isShimmerLoading,
      AppException? loadCustomersException});

  @override
  $LoadMoreOutputCopyWith<Customer, $Res> get customers;
}

/// @nodoc
class __$$CustomerStateImplCopyWithImpl<$Res>
    extends _$CustomerStateCopyWithImpl<$Res, _$CustomerStateImpl>
    implements _$$CustomerStateImplCopyWith<$Res> {
  __$$CustomerStateImplCopyWithImpl(
      _$CustomerStateImpl _value, $Res Function(_$CustomerStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? search = null,
    Object? searchFastType = null,
    Object? record = null,
    Object? customers = null,
    Object? isShimmerLoading = null,
    Object? loadCustomersException = freezed,
  }) {
    return _then(_$CustomerStateImpl(
      search: null == search
          ? _value.search
          : search // ignore: cast_nullable_to_non_nullable
              as String,
      searchFastType: null == searchFastType
          ? _value.searchFastType
          : searchFastType // ignore: cast_nullable_to_non_nullable
              as String,
      record: null == record
          ? _value.record
          : record // ignore: cast_nullable_to_non_nullable
              as int,
      customers: null == customers
          ? _value.customers
          : customers // ignore: cast_nullable_to_non_nullable
              as LoadMoreOutput<Customer>,
      isShimmerLoading: null == isShimmerLoading
          ? _value.isShimmerLoading
          : isShimmerLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      loadCustomersException: freezed == loadCustomersException
          ? _value.loadCustomersException
          : loadCustomersException // ignore: cast_nullable_to_non_nullable
              as AppException?,
    ));
  }
}

/// @nodoc

class _$CustomerStateImpl implements _CustomerState {
  _$CustomerStateImpl(
      {this.search = '',
      this.searchFastType = '',
      this.record = 0,
      this.customers = const LoadMoreOutput<Customer>(data: <Customer>[]),
      this.isShimmerLoading = false,
      this.loadCustomersException});

  @override
  @JsonKey()
  final String search;
  @override
  @JsonKey()
  final String searchFastType;
  @override
  @JsonKey()
  final int record;
  @override
  @JsonKey()
  final LoadMoreOutput<Customer> customers;
  @override
  @JsonKey()
  final bool isShimmerLoading;
  @override
  final AppException? loadCustomersException;

  @override
  String toString() {
    return 'CustomerState(search: $search, searchFastType: $searchFastType, record: $record, customers: $customers, isShimmerLoading: $isShimmerLoading, loadCustomersException: $loadCustomersException)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CustomerStateImpl &&
            (identical(other.search, search) || other.search == search) &&
            (identical(other.searchFastType, searchFastType) ||
                other.searchFastType == searchFastType) &&
            (identical(other.record, record) || other.record == record) &&
            (identical(other.customers, customers) ||
                other.customers == customers) &&
            (identical(other.isShimmerLoading, isShimmerLoading) ||
                other.isShimmerLoading == isShimmerLoading) &&
            (identical(other.loadCustomersException, loadCustomersException) ||
                other.loadCustomersException == loadCustomersException));
  }

  @override
  int get hashCode => Object.hash(runtimeType, search, searchFastType, record,
      customers, isShimmerLoading, loadCustomersException);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CustomerStateImplCopyWith<_$CustomerStateImpl> get copyWith =>
      __$$CustomerStateImplCopyWithImpl<_$CustomerStateImpl>(this, _$identity);
}

abstract class _CustomerState implements CustomerState {
  factory _CustomerState(
      {final String search,
      final String searchFastType,
      final int record,
      final LoadMoreOutput<Customer> customers,
      final bool isShimmerLoading,
      final AppException? loadCustomersException}) = _$CustomerStateImpl;

  @override
  String get search;
  @override
  String get searchFastType;
  @override
  int get record;
  @override
  LoadMoreOutput<Customer> get customers;
  @override
  bool get isShimmerLoading;
  @override
  AppException? get loadCustomersException;
  @override
  @JsonKey(ignore: true)
  _$$CustomerStateImplCopyWith<_$CustomerStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
