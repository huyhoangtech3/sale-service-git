// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'customer_event.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$CustomerPageInitiated {}

/// @nodoc
abstract class $CustomerPageInitiatedCopyWith<$Res> {
  factory $CustomerPageInitiatedCopyWith(CustomerPageInitiated value,
          $Res Function(CustomerPageInitiated) then) =
      _$CustomerPageInitiatedCopyWithImpl<$Res, CustomerPageInitiated>;
}

/// @nodoc
class _$CustomerPageInitiatedCopyWithImpl<$Res,
        $Val extends CustomerPageInitiated>
    implements $CustomerPageInitiatedCopyWith<$Res> {
  _$CustomerPageInitiatedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$CustomerPageInitiatedImplCopyWith<$Res> {
  factory _$$CustomerPageInitiatedImplCopyWith(
          _$CustomerPageInitiatedImpl value,
          $Res Function(_$CustomerPageInitiatedImpl) then) =
      __$$CustomerPageInitiatedImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$CustomerPageInitiatedImplCopyWithImpl<$Res>
    extends _$CustomerPageInitiatedCopyWithImpl<$Res,
        _$CustomerPageInitiatedImpl>
    implements _$$CustomerPageInitiatedImplCopyWith<$Res> {
  __$$CustomerPageInitiatedImplCopyWithImpl(_$CustomerPageInitiatedImpl _value,
      $Res Function(_$CustomerPageInitiatedImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$CustomerPageInitiatedImpl implements _CustomerPageInitiated {
  const _$CustomerPageInitiatedImpl();

  @override
  String toString() {
    return 'CustomerPageInitiated()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CustomerPageInitiatedImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;
}

abstract class _CustomerPageInitiated implements CustomerPageInitiated {
  const factory _CustomerPageInitiated() = _$CustomerPageInitiatedImpl;
}

/// @nodoc
mixin _$CustomerPageRefreshed {
  Completer<void> get completer => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CustomerPageRefreshedCopyWith<CustomerPageRefreshed> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CustomerPageRefreshedCopyWith<$Res> {
  factory $CustomerPageRefreshedCopyWith(CustomerPageRefreshed value,
          $Res Function(CustomerPageRefreshed) then) =
      _$CustomerPageRefreshedCopyWithImpl<$Res, CustomerPageRefreshed>;
  @useResult
  $Res call({Completer<void> completer});
}

/// @nodoc
class _$CustomerPageRefreshedCopyWithImpl<$Res,
        $Val extends CustomerPageRefreshed>
    implements $CustomerPageRefreshedCopyWith<$Res> {
  _$CustomerPageRefreshedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? completer = null,
  }) {
    return _then(_value.copyWith(
      completer: null == completer
          ? _value.completer
          : completer // ignore: cast_nullable_to_non_nullable
              as Completer<void>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$CustomerPageRefreshedImplCopyWith<$Res>
    implements $CustomerPageRefreshedCopyWith<$Res> {
  factory _$$CustomerPageRefreshedImplCopyWith(
          _$CustomerPageRefreshedImpl value,
          $Res Function(_$CustomerPageRefreshedImpl) then) =
      __$$CustomerPageRefreshedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({Completer<void> completer});
}

/// @nodoc
class __$$CustomerPageRefreshedImplCopyWithImpl<$Res>
    extends _$CustomerPageRefreshedCopyWithImpl<$Res,
        _$CustomerPageRefreshedImpl>
    implements _$$CustomerPageRefreshedImplCopyWith<$Res> {
  __$$CustomerPageRefreshedImplCopyWithImpl(_$CustomerPageRefreshedImpl _value,
      $Res Function(_$CustomerPageRefreshedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? completer = null,
  }) {
    return _then(_$CustomerPageRefreshedImpl(
      completer: null == completer
          ? _value.completer
          : completer // ignore: cast_nullable_to_non_nullable
              as Completer<void>,
    ));
  }
}

/// @nodoc

class _$CustomerPageRefreshedImpl implements _CustomerPageRefreshed {
  const _$CustomerPageRefreshedImpl({required this.completer});

  @override
  final Completer<void> completer;

  @override
  String toString() {
    return 'CustomerPageRefreshed(completer: $completer)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CustomerPageRefreshedImpl &&
            (identical(other.completer, completer) ||
                other.completer == completer));
  }

  @override
  int get hashCode => Object.hash(runtimeType, completer);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CustomerPageRefreshedImplCopyWith<_$CustomerPageRefreshedImpl>
      get copyWith => __$$CustomerPageRefreshedImplCopyWithImpl<
          _$CustomerPageRefreshedImpl>(this, _$identity);
}

abstract class _CustomerPageRefreshed implements CustomerPageRefreshed {
  const factory _CustomerPageRefreshed(
      {required final Completer<void> completer}) = _$CustomerPageRefreshedImpl;

  @override
  Completer<void> get completer;
  @override
  @JsonKey(ignore: true)
  _$$CustomerPageRefreshedImplCopyWith<_$CustomerPageRefreshedImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$CustomerLoadMore {}

/// @nodoc
abstract class $CustomerLoadMoreCopyWith<$Res> {
  factory $CustomerLoadMoreCopyWith(
          CustomerLoadMore value, $Res Function(CustomerLoadMore) then) =
      _$CustomerLoadMoreCopyWithImpl<$Res, CustomerLoadMore>;
}

/// @nodoc
class _$CustomerLoadMoreCopyWithImpl<$Res, $Val extends CustomerLoadMore>
    implements $CustomerLoadMoreCopyWith<$Res> {
  _$CustomerLoadMoreCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$CustomerLoadMoreImplCopyWith<$Res> {
  factory _$$CustomerLoadMoreImplCopyWith(_$CustomerLoadMoreImpl value,
          $Res Function(_$CustomerLoadMoreImpl) then) =
      __$$CustomerLoadMoreImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$CustomerLoadMoreImplCopyWithImpl<$Res>
    extends _$CustomerLoadMoreCopyWithImpl<$Res, _$CustomerLoadMoreImpl>
    implements _$$CustomerLoadMoreImplCopyWith<$Res> {
  __$$CustomerLoadMoreImplCopyWithImpl(_$CustomerLoadMoreImpl _value,
      $Res Function(_$CustomerLoadMoreImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$CustomerLoadMoreImpl implements _CustomerLoadMore {
  const _$CustomerLoadMoreImpl();

  @override
  String toString() {
    return 'CustomerLoadMore()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$CustomerLoadMoreImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;
}

abstract class _CustomerLoadMore implements CustomerLoadMore {
  const factory _CustomerLoadMore() = _$CustomerLoadMoreImpl;
}

/// @nodoc
mixin _$CustomerSearch {
  String get search => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CustomerSearchCopyWith<CustomerSearch> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CustomerSearchCopyWith<$Res> {
  factory $CustomerSearchCopyWith(
          CustomerSearch value, $Res Function(CustomerSearch) then) =
      _$CustomerSearchCopyWithImpl<$Res, CustomerSearch>;
  @useResult
  $Res call({String search});
}

/// @nodoc
class _$CustomerSearchCopyWithImpl<$Res, $Val extends CustomerSearch>
    implements $CustomerSearchCopyWith<$Res> {
  _$CustomerSearchCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? search = null,
  }) {
    return _then(_value.copyWith(
      search: null == search
          ? _value.search
          : search // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$CustomerSearchImplCopyWith<$Res>
    implements $CustomerSearchCopyWith<$Res> {
  factory _$$CustomerSearchImplCopyWith(_$CustomerSearchImpl value,
          $Res Function(_$CustomerSearchImpl) then) =
      __$$CustomerSearchImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String search});
}

/// @nodoc
class __$$CustomerSearchImplCopyWithImpl<$Res>
    extends _$CustomerSearchCopyWithImpl<$Res, _$CustomerSearchImpl>
    implements _$$CustomerSearchImplCopyWith<$Res> {
  __$$CustomerSearchImplCopyWithImpl(
      _$CustomerSearchImpl _value, $Res Function(_$CustomerSearchImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? search = null,
  }) {
    return _then(_$CustomerSearchImpl(
      search: null == search
          ? _value.search
          : search // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$CustomerSearchImpl implements _CustomerSearch {
  const _$CustomerSearchImpl({required this.search});

  @override
  final String search;

  @override
  String toString() {
    return 'CustomerSearch(search: $search)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CustomerSearchImpl &&
            (identical(other.search, search) || other.search == search));
  }

  @override
  int get hashCode => Object.hash(runtimeType, search);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CustomerSearchImplCopyWith<_$CustomerSearchImpl> get copyWith =>
      __$$CustomerSearchImplCopyWithImpl<_$CustomerSearchImpl>(
          this, _$identity);
}

abstract class _CustomerSearch implements CustomerSearch {
  const factory _CustomerSearch({required final String search}) =
      _$CustomerSearchImpl;

  @override
  String get search;
  @override
  @JsonKey(ignore: true)
  _$$CustomerSearchImplCopyWith<_$CustomerSearchImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
