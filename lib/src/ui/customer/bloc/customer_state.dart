import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:domain/domain.dart';
import 'package:shared/shared.dart';
import 'package:base_architecture_flutter/src/base/bloc/base_bloc_state.dart';

part 'customer_state.freezed.dart';

@freezed
class CustomerState extends BaseBlocState with _$CustomerState {
  factory CustomerState({
    @Default('') String search,
    @Default('') String searchFastType,
    @Default(0) int record,
    @Default(LoadMoreOutput<Customer>(data: <Customer>[])) LoadMoreOutput<Customer> customers,
    @Default(false) bool isShimmerLoading,
    AppException? loadCustomersException,
  }) = _CustomerState;
}
