// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'home_event.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$HomePageInitiated {}

/// @nodoc
abstract class $HomePageInitiatedCopyWith<$Res> {
  factory $HomePageInitiatedCopyWith(
          HomePageInitiated value, $Res Function(HomePageInitiated) then) =
      _$HomePageInitiatedCopyWithImpl<$Res, HomePageInitiated>;
}

/// @nodoc
class _$HomePageInitiatedCopyWithImpl<$Res, $Val extends HomePageInitiated>
    implements $HomePageInitiatedCopyWith<$Res> {
  _$HomePageInitiatedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$HomePageInitiatedImplCopyWith<$Res> {
  factory _$$HomePageInitiatedImplCopyWith(_$HomePageInitiatedImpl value,
          $Res Function(_$HomePageInitiatedImpl) then) =
      __$$HomePageInitiatedImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$HomePageInitiatedImplCopyWithImpl<$Res>
    extends _$HomePageInitiatedCopyWithImpl<$Res, _$HomePageInitiatedImpl>
    implements _$$HomePageInitiatedImplCopyWith<$Res> {
  __$$HomePageInitiatedImplCopyWithImpl(_$HomePageInitiatedImpl _value,
      $Res Function(_$HomePageInitiatedImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$HomePageInitiatedImpl implements _HomePageInitiated {
  const _$HomePageInitiatedImpl();

  @override
  String toString() {
    return 'HomePageInitiated()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$HomePageInitiatedImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;
}

abstract class _HomePageInitiated implements HomePageInitiated {
  const factory _HomePageInitiated() = _$HomePageInitiatedImpl;
}

/// @nodoc
mixin _$HomePageRefreshed {
  Completer<void> get completer => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $HomePageRefreshedCopyWith<HomePageRefreshed> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $HomePageRefreshedCopyWith<$Res> {
  factory $HomePageRefreshedCopyWith(
          HomePageRefreshed value, $Res Function(HomePageRefreshed) then) =
      _$HomePageRefreshedCopyWithImpl<$Res, HomePageRefreshed>;
  @useResult
  $Res call({Completer<void> completer});
}

/// @nodoc
class _$HomePageRefreshedCopyWithImpl<$Res, $Val extends HomePageRefreshed>
    implements $HomePageRefreshedCopyWith<$Res> {
  _$HomePageRefreshedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? completer = null,
  }) {
    return _then(_value.copyWith(
      completer: null == completer
          ? _value.completer
          : completer // ignore: cast_nullable_to_non_nullable
              as Completer<void>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$HomePageRefreshedImplCopyWith<$Res>
    implements $HomePageRefreshedCopyWith<$Res> {
  factory _$$HomePageRefreshedImplCopyWith(_$HomePageRefreshedImpl value,
          $Res Function(_$HomePageRefreshedImpl) then) =
      __$$HomePageRefreshedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({Completer<void> completer});
}

/// @nodoc
class __$$HomePageRefreshedImplCopyWithImpl<$Res>
    extends _$HomePageRefreshedCopyWithImpl<$Res, _$HomePageRefreshedImpl>
    implements _$$HomePageRefreshedImplCopyWith<$Res> {
  __$$HomePageRefreshedImplCopyWithImpl(_$HomePageRefreshedImpl _value,
      $Res Function(_$HomePageRefreshedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? completer = null,
  }) {
    return _then(_$HomePageRefreshedImpl(
      completer: null == completer
          ? _value.completer
          : completer // ignore: cast_nullable_to_non_nullable
              as Completer<void>,
    ));
  }
}

/// @nodoc

class _$HomePageRefreshedImpl implements _HomePageRefreshed {
  const _$HomePageRefreshedImpl({required this.completer});

  @override
  final Completer<void> completer;

  @override
  String toString() {
    return 'HomePageRefreshed(completer: $completer)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$HomePageRefreshedImpl &&
            (identical(other.completer, completer) ||
                other.completer == completer));
  }

  @override
  int get hashCode => Object.hash(runtimeType, completer);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$HomePageRefreshedImplCopyWith<_$HomePageRefreshedImpl> get copyWith =>
      __$$HomePageRefreshedImplCopyWithImpl<_$HomePageRefreshedImpl>(
          this, _$identity);
}

abstract class _HomePageRefreshed implements HomePageRefreshed {
  const factory _HomePageRefreshed({required final Completer<void> completer}) =
      _$HomePageRefreshedImpl;

  @override
  Completer<void> get completer;
  @override
  @JsonKey(ignore: true)
  _$$HomePageRefreshedImplCopyWith<_$HomePageRefreshedImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$UserLoadMore {}

/// @nodoc
abstract class $UserLoadMoreCopyWith<$Res> {
  factory $UserLoadMoreCopyWith(
          UserLoadMore value, $Res Function(UserLoadMore) then) =
      _$UserLoadMoreCopyWithImpl<$Res, UserLoadMore>;
}

/// @nodoc
class _$UserLoadMoreCopyWithImpl<$Res, $Val extends UserLoadMore>
    implements $UserLoadMoreCopyWith<$Res> {
  _$UserLoadMoreCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$UserLoadMoreImplCopyWith<$Res> {
  factory _$$UserLoadMoreImplCopyWith(
          _$UserLoadMoreImpl value, $Res Function(_$UserLoadMoreImpl) then) =
      __$$UserLoadMoreImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$UserLoadMoreImplCopyWithImpl<$Res>
    extends _$UserLoadMoreCopyWithImpl<$Res, _$UserLoadMoreImpl>
    implements _$$UserLoadMoreImplCopyWith<$Res> {
  __$$UserLoadMoreImplCopyWithImpl(
      _$UserLoadMoreImpl _value, $Res Function(_$UserLoadMoreImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$UserLoadMoreImpl implements _UserLoadMore {
  const _$UserLoadMoreImpl();

  @override
  String toString() {
    return 'UserLoadMore()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$UserLoadMoreImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;
}

abstract class _UserLoadMore implements UserLoadMore {
  const factory _UserLoadMore() = _$UserLoadMoreImpl;
}
