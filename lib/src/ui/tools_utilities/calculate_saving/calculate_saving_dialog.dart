import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:resources/resources.dart';
import 'package:base_architecture_flutter/src/app.dart';
import 'package:shared/shared.dart';

class CalculateSavingDialog extends StatefulWidget {
  const CalculateSavingDialog({super.key});

  @override
  State<StatefulWidget> createState() {
    return _CalculateSavingDialogState();
  }
}

class _CalculateSavingDialogState extends BasePageState<CalculateSavingDialog, CalculateSavingBloc> {
  final TextEditingController ctrDepositAmount = TextEditingController();
  final TextEditingController ctrDepositTerm = TextEditingController();
  final TextEditingController ctrInterestRatePerYear = TextEditingController();
  final TextEditingController ctrDate = TextEditingController();
  FocusNode focusNode = FocusNode();
  static List<String> list = <String>[S.current.day, S.current.month, S.current.year];
  String formatCurrency = '';

  @override
  void initState() {
    super.initState();
    focusNode = FocusNode();
    bloc.add(DateUnitTextFieldChanged(dateUnit: list.first));
  }

  @override
  void dispose() {
    // Clean up the focus node when the Form is disposed.
    focusNode.dispose();
    super.dispose();
  }

  @override
  Widget buildPage(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(Dimens.d12.responsive()),
      margin: EdgeInsets.fromLTRB(Dimens.d264.responsive(), 0.0, Dimens.d280.responsive(), 0.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(0),
        color: Colors.white,
      ),
      child: SingleChildScrollView(
        child: Column(
          children: [
            Align(
              alignment: Alignment.centerLeft,
              child: Row(
                children: [
                  Align(
                    child: Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: Assets.images.icToolSavemoney.provider(),
                          fit: BoxFit.fitHeight,
                        ),
                      ),
                      child: SizedBox(
                        width: Dimens.d40.responsive(), height: Dimens.d40.responsive(),
                      ),
                    ),
                  ),
                  SizedBox(width: Dimens.d4.responsive()),
                  Expanded(
                    flex: 1,
                    child:  Text(
                      S.current.calculateSaving,
                      style: AppTextStyles.s16w500Tertiary(),
                    ),
                  ),
                  Material(
                    color: Colors.transparent,
                    child: InkWell(
                      onTap: () => navigator.pop(),
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Container(
                          margin: EdgeInsets.all(Dimens.d4.responsive()),
                          alignment: Alignment.centerRight,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: Assets.images.close.provider(),
                              fit: BoxFit.fitHeight,
                            ),
                          ),
                          child: SizedBox(
                            width: Dimens.d24.responsive(), height: Dimens.d24.responsive(),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: Dimens.d24.responsive()),
            BlocBuilder<CalculateSavingBloc, CalculateSavingState>(
              builder: (context, state) {
                return AppTextFormField(
                  controller: ctrDepositAmount,
                  label: S.current.depositAmount,
                  labelStyle: AppTextStyles.s14w300Tertiary(),
                  style: AppTextStyles.s14w400Tertiary(),
                  hintStyle: AppTextStyles.s12w200Tertiary(),
                  hintText: S.current.depositAmount,
                  inputFormatters: [
                    CurrencyFormatter(separator: '#,###'),
                  ],
                  onChanged: (depositAmount) {
                    depositAmount = depositAmount.removeDots();
                    bloc.add(DepositAmountTextFieldChanged(depositAmount: depositAmount));
                  },
                  keyboardType: TextInputType.number,
                  textInputAction: TextInputAction.next,
                  onFieldSubmitted: (depositAmount){
                    focusNode.requestFocus();
                  },
                  onTapOutside: (event) {
                    FocusScope.of(context).unfocus();
                  },
                );
              },
            ),
            SizedBox(height: Dimens.d24.responsive()),
            Row(
              children: [
                Expanded(
                  flex: 1,
                  child: BlocBuilder<CalculateSavingBloc, CalculateSavingState>(
                    builder: (context, state) {
                      return AppTextFormField(
                        controller: ctrDepositTerm,
                        label: S.current.depositTerm,
                        labelStyle: AppTextStyles.s14w300Tertiary(),
                        style: AppTextStyles.s14w400Tertiary(),
                        hintStyle: AppTextStyles.s12w200Tertiary(),
                        hintText: S.current.depositTerm,
                        onChanged: (depositTerm) {
                          bloc.add(DepositTermTextFieldChanged(depositTerm: depositTerm));
                        },
                        keyboardType: TextInputType.number,
                        textInputAction: TextInputAction.next,
                        onFieldSubmitted: (_){
                          focusNode.requestFocus();
                        },
                        onTapOutside: (event) {
                          FocusScope.of(context).unfocus();
                        },
                      );
                    },
                  ),
                ),
                SizedBox(width: Dimens.d30.responsive()),
                Expanded(
                  flex: 1,
                  child: BlocBuilder<CalculateSavingBloc, CalculateSavingState>(
                    builder: (context, state) {
                      // return CommonDropdownMenu(
                      //   list: list,
                      //   width: MediaQuery.of(context).size.width * 0.19,
                      //   controller: ctrDate,
                      //   onSelected: (value) {
                      //     bloc.add(DateUnitTextFieldChanged(dateUnit: value!));
                      //   },
                      // );
                      return DropdownMenu<String>(
                        controller: ctrDate,
                        width: MediaQuery.of(context).size.width * 0.2,
                        initialSelection: list.first,
                        onSelected: (value) {
                          bloc.add(DateUnitTextFieldChanged(dateUnit: value!));
                        },
                        inputDecorationTheme: InputDecorationTheme(
                          isDense: true,
                          contentPadding: EdgeInsets.symmetric(horizontal: Dimens.d8.responsive()),
                          constraints: BoxConstraints.tight(Size.fromHeight(Dimens.d50.responsive())),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8),
                          ),
                        ),
                        dropdownMenuEntries: list.map<DropdownMenuEntry<String>>((String value) {
                          return DropdownMenuEntry<String>(value: value, label: value);
                        }).toList(),
                      );
                    },
                  ),
                ),
              ],
            ),
            SizedBox(height: Dimens.d24.responsive()),
            BlocBuilder<CalculateSavingBloc, CalculateSavingState>(
              builder: (context, state) {
                return AppTextFormField(
                  controller: ctrInterestRatePerYear,
                  label: S.current.interestRatePerYear,
                  labelStyle: AppTextStyles.s14w300Tertiary(),
                  style: AppTextStyles.s14w400Tertiary(),
                  hintStyle: AppTextStyles.s12w200Tertiary(),
                  hintText: S.current.interestRatePerYear,
                  inputFormatters: [
                    SymbolFormatter(symbolStart: '', symbolEnd: '%'),
                  ],
                  onChanged: (interestRatePerYear) {
                    interestRatePerYear = interestRatePerYear.removePercent();
                    bloc.add(InterestRatePerYearTextFieldChanged(interestRatePerYear: interestRatePerYear));
                  },
                  keyboardType: TextInputType.number,
                  textInputAction: TextInputAction.next,
                  onFieldSubmitted: (_){
                    focusNode.requestFocus();
                  },
                  onTapOutside: (event) {
                    FocusScope.of(context).unfocus();
                  },
                );
              },
            ),
            SizedBox(height: Dimens.d24.responsive()),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                BlocBuilder<CalculateSavingBloc, CalculateSavingState>(
                  buildWhen: (previous, current) =>
                  previous.isReEntryButtonEnabled != current.isReEntryButtonEnabled,
                  builder: (context, state) {
                    return ElevatedButton(
                      onPressed: () {
                        if (state.isReEntryButtonEnabled) {
                          bloc.add(const ReEntryButtonPressed());
                          ctrDepositAmount.clear();
                          ctrDepositTerm.clear();
                          ctrInterestRatePerYear.clear();
                          ctrDate.text = S.current.day;
                        }
                      },
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(AppColors.current.primaryColor
                            .withOpacity(state.isReEntryButtonEnabled ? 1 : 0.5)),
                        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(6.0),
                          ),
                        ),
                        padding: MaterialStateProperty.all<EdgeInsets>(
                          EdgeInsets.fromLTRB(
                            Dimens.d88.responsive(),
                            Dimens.d4.responsive(),
                            Dimens.d88.responsive(),
                            Dimens.d4.responsive(),
                          ),
                        ),
                      ),
                      child: Text(
                        S.current.retype,
                        style: AppTextStyles.s14w400Primary(),
                      ),
                    );
                  },
                ),
                BlocBuilder<CalculateSavingBloc, CalculateSavingState>(
                  buildWhen: (previous, current) =>
                  previous.isCalculateButtonEnabled != current.isCalculateButtonEnabled,
                  builder: (context, state) {
                    return ElevatedButton(
                      onPressed: () {
                        if (state.isCalculateButtonEnabled) {
                          bloc.add(CalculateButtonPressed(
                            depositAmount: ctrDepositAmount.value.text,
                            depositTerm: ctrDepositTerm.value.text,
                            interestRatePerYear: ctrInterestRatePerYear.value.text,
                            dateUnit: ctrDate.value.text,
                          ));
                        }
                      },
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(AppColors.current.primaryColor
                            .withOpacity(state.isCalculateButtonEnabled ? 1 : 0.5)),
                        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(6.0),
                          ),
                        ),
                        padding: MaterialStateProperty.all<EdgeInsets>(
                          EdgeInsets.fromLTRB(
                            Dimens.d88.responsive(),
                            Dimens.d4.responsive(),
                            Dimens.d88.responsive(),
                            Dimens.d4.responsive(),
                          ),
                        ),
                      ),
                      child: Text(
                        S.current.calculate,
                        style: AppTextStyles.s14w400Primary(),
                      ),
                    );
                  },
                ),
              ],
            ),
            SizedBox(height: Dimens.d24.responsive()),
            Align(
              alignment: Alignment.topLeft,
              child: Text(
                S.current.estimatedResults,
                style: AppTextStyles.s14w500Tertiary(),
              ),
            ),
            SizedBox(height: Dimens.d24.responsive()),
            BlocBuilder<CalculateSavingBloc, CalculateSavingState>(
              builder: (context, state) {
                return Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    S.current.totalInterestEndPeriod + state.totalInterestEndPeriod,
                    style: AppTextStyles.s14w400Tertiary(),
                  ),
                );
              },
            ),
            SizedBox(height: Dimens.d8.responsive()),
            BlocBuilder<CalculateSavingBloc, CalculateSavingState>(
              builder: (context, state) {
                return Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    S.current.totalInterestPrincipalEndPeriod + state.totalInterestPrincipalEndPeriod,
                    style: AppTextStyles.s14w400Tertiary(),
                  ),
                );
              },
            ),
            SizedBox(height: Dimens.d24.responsive()),
          ],
        ),
      ),
    );
  }
}
