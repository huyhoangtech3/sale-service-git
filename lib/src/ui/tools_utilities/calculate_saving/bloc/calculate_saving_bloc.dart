import 'dart:async';

import 'package:domain/domain.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

import 'package:base_architecture_flutter/src/app.dart';

@Injectable()
class CalculateSavingBloc extends BaseBloc<CalculateSavingEvent, CalculateSavingState> {
  CalculateSavingBloc(this._calculateSavingUseCase) : super(const CalculateSavingState()) {
    on<DepositAmountTextFieldChanged>(
      _onDepositAmountTextFieldChanged,
      transformer: distinct(),
    );

    on<DepositTermTextFieldChanged>(
      _onDepositTermTextFieldChanged,
      transformer: distinct(),
    );

    on<InterestRatePerYearTextFieldChanged>(
      _onInterestRatePerYearTextFieldChanged,
      transformer: distinct(),
    );

    on<DateUnitTextFieldChanged>(
      _onDateUnitTextFieldChanged,
      transformer: distinct(),
    );

    on<ReEntryButtonPressed>(
      _onReEntryButtonPressed,
      transformer: log(),
    );

    on<CalculateButtonPressed>(
      _onCalculateButtonPressed,
      transformer: distinct(),
    );
  }

  final CalculateSavingUseCase _calculateSavingUseCase;

  bool _isCalculateButtonEnabled(String depositAmount, String depositTerm, String interestRatePerYear) {
    return depositAmount.isNotEmpty && depositTerm.isNotEmpty && interestRatePerYear.isNotEmpty;
  }

  bool _isReEntryButtonEnabled(String depositAmount, String depositTerm, String interestRatePerYear) {
    return depositAmount.isNotEmpty && depositTerm.isNotEmpty && interestRatePerYear.isNotEmpty;
  }

  void _onDepositAmountTextFieldChanged(DepositAmountTextFieldChanged event, Emitter<CalculateSavingState> emit) {
    emit(state.copyWith(
      depositAmount: event.depositAmount,
      isReEntryButtonEnabled: _isReEntryButtonEnabled(event.depositAmount, state.depositTerm, state.interestRatePerYear),
      isCalculateButtonEnabled: _isCalculateButtonEnabled(event.depositAmount, state.depositTerm, state.interestRatePerYear),
    ));
  }

  void _onDepositTermTextFieldChanged(DepositTermTextFieldChanged event, Emitter<CalculateSavingState> emit) {
    emit(state.copyWith(
      depositTerm: event.depositTerm,
      isReEntryButtonEnabled: _isReEntryButtonEnabled(state.depositAmount, event.depositTerm, state.interestRatePerYear),
      isCalculateButtonEnabled: _isCalculateButtonEnabled(state.depositAmount, event.depositTerm, state.interestRatePerYear),
    ));
  }

  void _onInterestRatePerYearTextFieldChanged(InterestRatePerYearTextFieldChanged event, Emitter<CalculateSavingState> emit) {
    emit(state.copyWith(
      interestRatePerYear: event.interestRatePerYear,
      isReEntryButtonEnabled: _isReEntryButtonEnabled(state.depositAmount, state.depositTerm, event.interestRatePerYear),
      isCalculateButtonEnabled: _isCalculateButtonEnabled(state.depositAmount, state.depositTerm, event.interestRatePerYear),
    ));
  }

  void _onDateUnitTextFieldChanged(DateUnitTextFieldChanged event, Emitter<CalculateSavingState> emit) {
    emit(state.copyWith(
      dateUnit: event.dateUnit,
    ));
  }

  FutureOr<void> _onCalculateButtonPressed(CalculateButtonPressed event, Emitter<CalculateSavingState> emit) {
    return runBlocCatching(
      action: () async {
        final output = _calculateSavingUseCase.execute(
            CalculateSavingInput(
              depositAmount: state.depositAmount,
              depositTerm: state.depositTerm,
              interestRatePerYear: state.interestRatePerYear,
              dateUnit: state.dateUnit,
            )
        );
        emit(state.copyWith(
          totalInterestEndPeriod: output.totalInterestEndPeriod,
          totalInterestPrincipalEndPeriod: output.totalInterestPrincipalEndPeriod,
        ));
      },
    );
  }

  FutureOr<void> _onReEntryButtonPressed(ReEntryButtonPressed event, Emitter<CalculateSavingState> emit) {
    emit(state.copyWith(
          depositAmount: '',
          depositTerm: '',
          interestRatePerYear: '',
          isReEntryButtonEnabled: false,
          isCalculateButtonEnabled: false,
          totalInterestEndPeriod: '',
          totalInterestPrincipalEndPeriod: '',
      ));
  }
}
