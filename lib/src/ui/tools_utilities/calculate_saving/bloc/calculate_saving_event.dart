import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:base_architecture_flutter/src/base/bloc/base_bloc_event.dart';
part 'calculate_saving_event.freezed.dart';

abstract class CalculateSavingEvent extends BaseBlocEvent {
  const CalculateSavingEvent();
}

@freezed
class DepositAmountTextFieldChanged extends CalculateSavingEvent with _$DepositAmountTextFieldChanged {
  const factory DepositAmountTextFieldChanged({
    required String depositAmount,
  }) = _DepositAmountTextFieldChanged;
}

@freezed
class DepositTermTextFieldChanged extends CalculateSavingEvent with _$DepositTermTextFieldChanged {
  const factory DepositTermTextFieldChanged({
    required String depositTerm,
  }) = _DepositTermTextFieldChanged;
}

@freezed
class InterestRatePerYearTextFieldChanged extends CalculateSavingEvent with _$InterestRatePerYearTextFieldChanged {
  const factory InterestRatePerYearTextFieldChanged({
    required String interestRatePerYear,
  }) = _InterestRatePerYearTextFieldChanged;
}

@freezed
class DateUnitTextFieldChanged extends CalculateSavingEvent with _$DateUnitTextFieldChanged {
  const factory DateUnitTextFieldChanged({
    required String dateUnit,
  }) = _DateUnitTextFieldChanged;
}

@freezed
class ReEntryButtonPressed extends CalculateSavingEvent with _$ReEntryButtonPressed {
  const factory ReEntryButtonPressed() = _ReEntryButtonPressed;
}

@freezed
class CalculateButtonPressed extends CalculateSavingEvent with _$CalculateButtonPressed {
  const factory CalculateButtonPressed({
    required String depositAmount,
    required String depositTerm,
    required String interestRatePerYear,
    required String dateUnit,
  }) = _CalculateButtonPressed;
}
