// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'calculate_saving_event.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$DepositAmountTextFieldChanged {
  String get depositAmount => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $DepositAmountTextFieldChangedCopyWith<DepositAmountTextFieldChanged>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DepositAmountTextFieldChangedCopyWith<$Res> {
  factory $DepositAmountTextFieldChangedCopyWith(
          DepositAmountTextFieldChanged value,
          $Res Function(DepositAmountTextFieldChanged) then) =
      _$DepositAmountTextFieldChangedCopyWithImpl<$Res,
          DepositAmountTextFieldChanged>;
  @useResult
  $Res call({String depositAmount});
}

/// @nodoc
class _$DepositAmountTextFieldChangedCopyWithImpl<$Res,
        $Val extends DepositAmountTextFieldChanged>
    implements $DepositAmountTextFieldChangedCopyWith<$Res> {
  _$DepositAmountTextFieldChangedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? depositAmount = null,
  }) {
    return _then(_value.copyWith(
      depositAmount: null == depositAmount
          ? _value.depositAmount
          : depositAmount // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$DepositAmountTextFieldChangedImplCopyWith<$Res>
    implements $DepositAmountTextFieldChangedCopyWith<$Res> {
  factory _$$DepositAmountTextFieldChangedImplCopyWith(
          _$DepositAmountTextFieldChangedImpl value,
          $Res Function(_$DepositAmountTextFieldChangedImpl) then) =
      __$$DepositAmountTextFieldChangedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String depositAmount});
}

/// @nodoc
class __$$DepositAmountTextFieldChangedImplCopyWithImpl<$Res>
    extends _$DepositAmountTextFieldChangedCopyWithImpl<$Res,
        _$DepositAmountTextFieldChangedImpl>
    implements _$$DepositAmountTextFieldChangedImplCopyWith<$Res> {
  __$$DepositAmountTextFieldChangedImplCopyWithImpl(
      _$DepositAmountTextFieldChangedImpl _value,
      $Res Function(_$DepositAmountTextFieldChangedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? depositAmount = null,
  }) {
    return _then(_$DepositAmountTextFieldChangedImpl(
      depositAmount: null == depositAmount
          ? _value.depositAmount
          : depositAmount // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$DepositAmountTextFieldChangedImpl
    implements _DepositAmountTextFieldChanged {
  const _$DepositAmountTextFieldChangedImpl({required this.depositAmount});

  @override
  final String depositAmount;

  @override
  String toString() {
    return 'DepositAmountTextFieldChanged(depositAmount: $depositAmount)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$DepositAmountTextFieldChangedImpl &&
            (identical(other.depositAmount, depositAmount) ||
                other.depositAmount == depositAmount));
  }

  @override
  int get hashCode => Object.hash(runtimeType, depositAmount);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$DepositAmountTextFieldChangedImplCopyWith<
          _$DepositAmountTextFieldChangedImpl>
      get copyWith => __$$DepositAmountTextFieldChangedImplCopyWithImpl<
          _$DepositAmountTextFieldChangedImpl>(this, _$identity);
}

abstract class _DepositAmountTextFieldChanged
    implements DepositAmountTextFieldChanged {
  const factory _DepositAmountTextFieldChanged(
          {required final String depositAmount}) =
      _$DepositAmountTextFieldChangedImpl;

  @override
  String get depositAmount;
  @override
  @JsonKey(ignore: true)
  _$$DepositAmountTextFieldChangedImplCopyWith<
          _$DepositAmountTextFieldChangedImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$DepositTermTextFieldChanged {
  String get depositTerm => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $DepositTermTextFieldChangedCopyWith<DepositTermTextFieldChanged>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DepositTermTextFieldChangedCopyWith<$Res> {
  factory $DepositTermTextFieldChangedCopyWith(
          DepositTermTextFieldChanged value,
          $Res Function(DepositTermTextFieldChanged) then) =
      _$DepositTermTextFieldChangedCopyWithImpl<$Res,
          DepositTermTextFieldChanged>;
  @useResult
  $Res call({String depositTerm});
}

/// @nodoc
class _$DepositTermTextFieldChangedCopyWithImpl<$Res,
        $Val extends DepositTermTextFieldChanged>
    implements $DepositTermTextFieldChangedCopyWith<$Res> {
  _$DepositTermTextFieldChangedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? depositTerm = null,
  }) {
    return _then(_value.copyWith(
      depositTerm: null == depositTerm
          ? _value.depositTerm
          : depositTerm // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$DepositTermTextFieldChangedImplCopyWith<$Res>
    implements $DepositTermTextFieldChangedCopyWith<$Res> {
  factory _$$DepositTermTextFieldChangedImplCopyWith(
          _$DepositTermTextFieldChangedImpl value,
          $Res Function(_$DepositTermTextFieldChangedImpl) then) =
      __$$DepositTermTextFieldChangedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String depositTerm});
}

/// @nodoc
class __$$DepositTermTextFieldChangedImplCopyWithImpl<$Res>
    extends _$DepositTermTextFieldChangedCopyWithImpl<$Res,
        _$DepositTermTextFieldChangedImpl>
    implements _$$DepositTermTextFieldChangedImplCopyWith<$Res> {
  __$$DepositTermTextFieldChangedImplCopyWithImpl(
      _$DepositTermTextFieldChangedImpl _value,
      $Res Function(_$DepositTermTextFieldChangedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? depositTerm = null,
  }) {
    return _then(_$DepositTermTextFieldChangedImpl(
      depositTerm: null == depositTerm
          ? _value.depositTerm
          : depositTerm // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$DepositTermTextFieldChangedImpl
    implements _DepositTermTextFieldChanged {
  const _$DepositTermTextFieldChangedImpl({required this.depositTerm});

  @override
  final String depositTerm;

  @override
  String toString() {
    return 'DepositTermTextFieldChanged(depositTerm: $depositTerm)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$DepositTermTextFieldChangedImpl &&
            (identical(other.depositTerm, depositTerm) ||
                other.depositTerm == depositTerm));
  }

  @override
  int get hashCode => Object.hash(runtimeType, depositTerm);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$DepositTermTextFieldChangedImplCopyWith<_$DepositTermTextFieldChangedImpl>
      get copyWith => __$$DepositTermTextFieldChangedImplCopyWithImpl<
          _$DepositTermTextFieldChangedImpl>(this, _$identity);
}

abstract class _DepositTermTextFieldChanged
    implements DepositTermTextFieldChanged {
  const factory _DepositTermTextFieldChanged(
      {required final String depositTerm}) = _$DepositTermTextFieldChangedImpl;

  @override
  String get depositTerm;
  @override
  @JsonKey(ignore: true)
  _$$DepositTermTextFieldChangedImplCopyWith<_$DepositTermTextFieldChangedImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$InterestRatePerYearTextFieldChanged {
  String get interestRatePerYear => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $InterestRatePerYearTextFieldChangedCopyWith<
          InterestRatePerYearTextFieldChanged>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $InterestRatePerYearTextFieldChangedCopyWith<$Res> {
  factory $InterestRatePerYearTextFieldChangedCopyWith(
          InterestRatePerYearTextFieldChanged value,
          $Res Function(InterestRatePerYearTextFieldChanged) then) =
      _$InterestRatePerYearTextFieldChangedCopyWithImpl<$Res,
          InterestRatePerYearTextFieldChanged>;
  @useResult
  $Res call({String interestRatePerYear});
}

/// @nodoc
class _$InterestRatePerYearTextFieldChangedCopyWithImpl<$Res,
        $Val extends InterestRatePerYearTextFieldChanged>
    implements $InterestRatePerYearTextFieldChangedCopyWith<$Res> {
  _$InterestRatePerYearTextFieldChangedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? interestRatePerYear = null,
  }) {
    return _then(_value.copyWith(
      interestRatePerYear: null == interestRatePerYear
          ? _value.interestRatePerYear
          : interestRatePerYear // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$InterestRatePerYearTextFieldChangedImplCopyWith<$Res>
    implements $InterestRatePerYearTextFieldChangedCopyWith<$Res> {
  factory _$$InterestRatePerYearTextFieldChangedImplCopyWith(
          _$InterestRatePerYearTextFieldChangedImpl value,
          $Res Function(_$InterestRatePerYearTextFieldChangedImpl) then) =
      __$$InterestRatePerYearTextFieldChangedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String interestRatePerYear});
}

/// @nodoc
class __$$InterestRatePerYearTextFieldChangedImplCopyWithImpl<$Res>
    extends _$InterestRatePerYearTextFieldChangedCopyWithImpl<$Res,
        _$InterestRatePerYearTextFieldChangedImpl>
    implements _$$InterestRatePerYearTextFieldChangedImplCopyWith<$Res> {
  __$$InterestRatePerYearTextFieldChangedImplCopyWithImpl(
      _$InterestRatePerYearTextFieldChangedImpl _value,
      $Res Function(_$InterestRatePerYearTextFieldChangedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? interestRatePerYear = null,
  }) {
    return _then(_$InterestRatePerYearTextFieldChangedImpl(
      interestRatePerYear: null == interestRatePerYear
          ? _value.interestRatePerYear
          : interestRatePerYear // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$InterestRatePerYearTextFieldChangedImpl
    implements _InterestRatePerYearTextFieldChanged {
  const _$InterestRatePerYearTextFieldChangedImpl(
      {required this.interestRatePerYear});

  @override
  final String interestRatePerYear;

  @override
  String toString() {
    return 'InterestRatePerYearTextFieldChanged(interestRatePerYear: $interestRatePerYear)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$InterestRatePerYearTextFieldChangedImpl &&
            (identical(other.interestRatePerYear, interestRatePerYear) ||
                other.interestRatePerYear == interestRatePerYear));
  }

  @override
  int get hashCode => Object.hash(runtimeType, interestRatePerYear);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$InterestRatePerYearTextFieldChangedImplCopyWith<
          _$InterestRatePerYearTextFieldChangedImpl>
      get copyWith => __$$InterestRatePerYearTextFieldChangedImplCopyWithImpl<
          _$InterestRatePerYearTextFieldChangedImpl>(this, _$identity);
}

abstract class _InterestRatePerYearTextFieldChanged
    implements InterestRatePerYearTextFieldChanged {
  const factory _InterestRatePerYearTextFieldChanged(
          {required final String interestRatePerYear}) =
      _$InterestRatePerYearTextFieldChangedImpl;

  @override
  String get interestRatePerYear;
  @override
  @JsonKey(ignore: true)
  _$$InterestRatePerYearTextFieldChangedImplCopyWith<
          _$InterestRatePerYearTextFieldChangedImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$DateUnitTextFieldChanged {
  String get dateUnit => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $DateUnitTextFieldChangedCopyWith<DateUnitTextFieldChanged> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DateUnitTextFieldChangedCopyWith<$Res> {
  factory $DateUnitTextFieldChangedCopyWith(DateUnitTextFieldChanged value,
          $Res Function(DateUnitTextFieldChanged) then) =
      _$DateUnitTextFieldChangedCopyWithImpl<$Res, DateUnitTextFieldChanged>;
  @useResult
  $Res call({String dateUnit});
}

/// @nodoc
class _$DateUnitTextFieldChangedCopyWithImpl<$Res,
        $Val extends DateUnitTextFieldChanged>
    implements $DateUnitTextFieldChangedCopyWith<$Res> {
  _$DateUnitTextFieldChangedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? dateUnit = null,
  }) {
    return _then(_value.copyWith(
      dateUnit: null == dateUnit
          ? _value.dateUnit
          : dateUnit // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$DateUnitTextFieldChangedImplCopyWith<$Res>
    implements $DateUnitTextFieldChangedCopyWith<$Res> {
  factory _$$DateUnitTextFieldChangedImplCopyWith(
          _$DateUnitTextFieldChangedImpl value,
          $Res Function(_$DateUnitTextFieldChangedImpl) then) =
      __$$DateUnitTextFieldChangedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String dateUnit});
}

/// @nodoc
class __$$DateUnitTextFieldChangedImplCopyWithImpl<$Res>
    extends _$DateUnitTextFieldChangedCopyWithImpl<$Res,
        _$DateUnitTextFieldChangedImpl>
    implements _$$DateUnitTextFieldChangedImplCopyWith<$Res> {
  __$$DateUnitTextFieldChangedImplCopyWithImpl(
      _$DateUnitTextFieldChangedImpl _value,
      $Res Function(_$DateUnitTextFieldChangedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? dateUnit = null,
  }) {
    return _then(_$DateUnitTextFieldChangedImpl(
      dateUnit: null == dateUnit
          ? _value.dateUnit
          : dateUnit // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$DateUnitTextFieldChangedImpl implements _DateUnitTextFieldChanged {
  const _$DateUnitTextFieldChangedImpl({required this.dateUnit});

  @override
  final String dateUnit;

  @override
  String toString() {
    return 'DateUnitTextFieldChanged(dateUnit: $dateUnit)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$DateUnitTextFieldChangedImpl &&
            (identical(other.dateUnit, dateUnit) ||
                other.dateUnit == dateUnit));
  }

  @override
  int get hashCode => Object.hash(runtimeType, dateUnit);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$DateUnitTextFieldChangedImplCopyWith<_$DateUnitTextFieldChangedImpl>
      get copyWith => __$$DateUnitTextFieldChangedImplCopyWithImpl<
          _$DateUnitTextFieldChangedImpl>(this, _$identity);
}

abstract class _DateUnitTextFieldChanged implements DateUnitTextFieldChanged {
  const factory _DateUnitTextFieldChanged({required final String dateUnit}) =
      _$DateUnitTextFieldChangedImpl;

  @override
  String get dateUnit;
  @override
  @JsonKey(ignore: true)
  _$$DateUnitTextFieldChangedImplCopyWith<_$DateUnitTextFieldChangedImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$ReEntryButtonPressed {}

/// @nodoc
abstract class $ReEntryButtonPressedCopyWith<$Res> {
  factory $ReEntryButtonPressedCopyWith(ReEntryButtonPressed value,
          $Res Function(ReEntryButtonPressed) then) =
      _$ReEntryButtonPressedCopyWithImpl<$Res, ReEntryButtonPressed>;
}

/// @nodoc
class _$ReEntryButtonPressedCopyWithImpl<$Res,
        $Val extends ReEntryButtonPressed>
    implements $ReEntryButtonPressedCopyWith<$Res> {
  _$ReEntryButtonPressedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$ReEntryButtonPressedImplCopyWith<$Res> {
  factory _$$ReEntryButtonPressedImplCopyWith(_$ReEntryButtonPressedImpl value,
          $Res Function(_$ReEntryButtonPressedImpl) then) =
      __$$ReEntryButtonPressedImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ReEntryButtonPressedImplCopyWithImpl<$Res>
    extends _$ReEntryButtonPressedCopyWithImpl<$Res, _$ReEntryButtonPressedImpl>
    implements _$$ReEntryButtonPressedImplCopyWith<$Res> {
  __$$ReEntryButtonPressedImplCopyWithImpl(_$ReEntryButtonPressedImpl _value,
      $Res Function(_$ReEntryButtonPressedImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ReEntryButtonPressedImpl implements _ReEntryButtonPressed {
  const _$ReEntryButtonPressedImpl();

  @override
  String toString() {
    return 'ReEntryButtonPressed()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ReEntryButtonPressedImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;
}

abstract class _ReEntryButtonPressed implements ReEntryButtonPressed {
  const factory _ReEntryButtonPressed() = _$ReEntryButtonPressedImpl;
}

/// @nodoc
mixin _$CalculateButtonPressed {
  String get depositAmount => throw _privateConstructorUsedError;
  String get depositTerm => throw _privateConstructorUsedError;
  String get interestRatePerYear => throw _privateConstructorUsedError;
  String get dateUnit => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CalculateButtonPressedCopyWith<CalculateButtonPressed> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CalculateButtonPressedCopyWith<$Res> {
  factory $CalculateButtonPressedCopyWith(CalculateButtonPressed value,
          $Res Function(CalculateButtonPressed) then) =
      _$CalculateButtonPressedCopyWithImpl<$Res, CalculateButtonPressed>;
  @useResult
  $Res call(
      {String depositAmount,
      String depositTerm,
      String interestRatePerYear,
      String dateUnit});
}

/// @nodoc
class _$CalculateButtonPressedCopyWithImpl<$Res,
        $Val extends CalculateButtonPressed>
    implements $CalculateButtonPressedCopyWith<$Res> {
  _$CalculateButtonPressedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? depositAmount = null,
    Object? depositTerm = null,
    Object? interestRatePerYear = null,
    Object? dateUnit = null,
  }) {
    return _then(_value.copyWith(
      depositAmount: null == depositAmount
          ? _value.depositAmount
          : depositAmount // ignore: cast_nullable_to_non_nullable
              as String,
      depositTerm: null == depositTerm
          ? _value.depositTerm
          : depositTerm // ignore: cast_nullable_to_non_nullable
              as String,
      interestRatePerYear: null == interestRatePerYear
          ? _value.interestRatePerYear
          : interestRatePerYear // ignore: cast_nullable_to_non_nullable
              as String,
      dateUnit: null == dateUnit
          ? _value.dateUnit
          : dateUnit // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$CalculateButtonPressedImplCopyWith<$Res>
    implements $CalculateButtonPressedCopyWith<$Res> {
  factory _$$CalculateButtonPressedImplCopyWith(
          _$CalculateButtonPressedImpl value,
          $Res Function(_$CalculateButtonPressedImpl) then) =
      __$$CalculateButtonPressedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String depositAmount,
      String depositTerm,
      String interestRatePerYear,
      String dateUnit});
}

/// @nodoc
class __$$CalculateButtonPressedImplCopyWithImpl<$Res>
    extends _$CalculateButtonPressedCopyWithImpl<$Res,
        _$CalculateButtonPressedImpl>
    implements _$$CalculateButtonPressedImplCopyWith<$Res> {
  __$$CalculateButtonPressedImplCopyWithImpl(
      _$CalculateButtonPressedImpl _value,
      $Res Function(_$CalculateButtonPressedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? depositAmount = null,
    Object? depositTerm = null,
    Object? interestRatePerYear = null,
    Object? dateUnit = null,
  }) {
    return _then(_$CalculateButtonPressedImpl(
      depositAmount: null == depositAmount
          ? _value.depositAmount
          : depositAmount // ignore: cast_nullable_to_non_nullable
              as String,
      depositTerm: null == depositTerm
          ? _value.depositTerm
          : depositTerm // ignore: cast_nullable_to_non_nullable
              as String,
      interestRatePerYear: null == interestRatePerYear
          ? _value.interestRatePerYear
          : interestRatePerYear // ignore: cast_nullable_to_non_nullable
              as String,
      dateUnit: null == dateUnit
          ? _value.dateUnit
          : dateUnit // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$CalculateButtonPressedImpl implements _CalculateButtonPressed {
  const _$CalculateButtonPressedImpl(
      {required this.depositAmount,
      required this.depositTerm,
      required this.interestRatePerYear,
      required this.dateUnit});

  @override
  final String depositAmount;
  @override
  final String depositTerm;
  @override
  final String interestRatePerYear;
  @override
  final String dateUnit;

  @override
  String toString() {
    return 'CalculateButtonPressed(depositAmount: $depositAmount, depositTerm: $depositTerm, interestRatePerYear: $interestRatePerYear, dateUnit: $dateUnit)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CalculateButtonPressedImpl &&
            (identical(other.depositAmount, depositAmount) ||
                other.depositAmount == depositAmount) &&
            (identical(other.depositTerm, depositTerm) ||
                other.depositTerm == depositTerm) &&
            (identical(other.interestRatePerYear, interestRatePerYear) ||
                other.interestRatePerYear == interestRatePerYear) &&
            (identical(other.dateUnit, dateUnit) ||
                other.dateUnit == dateUnit));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, depositAmount, depositTerm, interestRatePerYear, dateUnit);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CalculateButtonPressedImplCopyWith<_$CalculateButtonPressedImpl>
      get copyWith => __$$CalculateButtonPressedImplCopyWithImpl<
          _$CalculateButtonPressedImpl>(this, _$identity);
}

abstract class _CalculateButtonPressed implements CalculateButtonPressed {
  const factory _CalculateButtonPressed(
      {required final String depositAmount,
      required final String depositTerm,
      required final String interestRatePerYear,
      required final String dateUnit}) = _$CalculateButtonPressedImpl;

  @override
  String get depositAmount;
  @override
  String get depositTerm;
  @override
  String get interestRatePerYear;
  @override
  String get dateUnit;
  @override
  @JsonKey(ignore: true)
  _$$CalculateButtonPressedImplCopyWith<_$CalculateButtonPressedImpl>
      get copyWith => throw _privateConstructorUsedError;
}
