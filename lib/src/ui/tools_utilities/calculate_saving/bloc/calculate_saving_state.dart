import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:base_architecture_flutter/src/base/bloc/base_bloc_state.dart';
part 'calculate_saving_state.freezed.dart';

@freezed
class CalculateSavingState extends BaseBlocState with _$CalculateSavingState {
  const factory CalculateSavingState({
    @Default('') String depositAmount,
    @Default('') String depositTerm,
    @Default('') String interestRatePerYear,
    @Default('') String dateUnit,
    @Default(false) bool isCalculateButtonEnabled,
    @Default(false) bool isReEntryButtonEnabled,
    @Default('') String totalInterestEndPeriod,
    @Default('') String totalInterestPrincipalEndPeriod,
  }) = _CalculateSavingState;
}
