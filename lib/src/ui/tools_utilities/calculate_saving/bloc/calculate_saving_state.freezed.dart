// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'calculate_saving_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$CalculateSavingState {
  String get depositAmount => throw _privateConstructorUsedError;
  String get depositTerm => throw _privateConstructorUsedError;
  String get interestRatePerYear => throw _privateConstructorUsedError;
  String get dateUnit => throw _privateConstructorUsedError;
  bool get isCalculateButtonEnabled => throw _privateConstructorUsedError;
  bool get isReEntryButtonEnabled => throw _privateConstructorUsedError;
  String get totalInterestEndPeriod => throw _privateConstructorUsedError;
  String get totalInterestPrincipalEndPeriod =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CalculateSavingStateCopyWith<CalculateSavingState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CalculateSavingStateCopyWith<$Res> {
  factory $CalculateSavingStateCopyWith(CalculateSavingState value,
          $Res Function(CalculateSavingState) then) =
      _$CalculateSavingStateCopyWithImpl<$Res, CalculateSavingState>;
  @useResult
  $Res call(
      {String depositAmount,
      String depositTerm,
      String interestRatePerYear,
      String dateUnit,
      bool isCalculateButtonEnabled,
      bool isReEntryButtonEnabled,
      String totalInterestEndPeriod,
      String totalInterestPrincipalEndPeriod});
}

/// @nodoc
class _$CalculateSavingStateCopyWithImpl<$Res,
        $Val extends CalculateSavingState>
    implements $CalculateSavingStateCopyWith<$Res> {
  _$CalculateSavingStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? depositAmount = null,
    Object? depositTerm = null,
    Object? interestRatePerYear = null,
    Object? dateUnit = null,
    Object? isCalculateButtonEnabled = null,
    Object? isReEntryButtonEnabled = null,
    Object? totalInterestEndPeriod = null,
    Object? totalInterestPrincipalEndPeriod = null,
  }) {
    return _then(_value.copyWith(
      depositAmount: null == depositAmount
          ? _value.depositAmount
          : depositAmount // ignore: cast_nullable_to_non_nullable
              as String,
      depositTerm: null == depositTerm
          ? _value.depositTerm
          : depositTerm // ignore: cast_nullable_to_non_nullable
              as String,
      interestRatePerYear: null == interestRatePerYear
          ? _value.interestRatePerYear
          : interestRatePerYear // ignore: cast_nullable_to_non_nullable
              as String,
      dateUnit: null == dateUnit
          ? _value.dateUnit
          : dateUnit // ignore: cast_nullable_to_non_nullable
              as String,
      isCalculateButtonEnabled: null == isCalculateButtonEnabled
          ? _value.isCalculateButtonEnabled
          : isCalculateButtonEnabled // ignore: cast_nullable_to_non_nullable
              as bool,
      isReEntryButtonEnabled: null == isReEntryButtonEnabled
          ? _value.isReEntryButtonEnabled
          : isReEntryButtonEnabled // ignore: cast_nullable_to_non_nullable
              as bool,
      totalInterestEndPeriod: null == totalInterestEndPeriod
          ? _value.totalInterestEndPeriod
          : totalInterestEndPeriod // ignore: cast_nullable_to_non_nullable
              as String,
      totalInterestPrincipalEndPeriod: null == totalInterestPrincipalEndPeriod
          ? _value.totalInterestPrincipalEndPeriod
          : totalInterestPrincipalEndPeriod // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$CalculateSavingStateImplCopyWith<$Res>
    implements $CalculateSavingStateCopyWith<$Res> {
  factory _$$CalculateSavingStateImplCopyWith(_$CalculateSavingStateImpl value,
          $Res Function(_$CalculateSavingStateImpl) then) =
      __$$CalculateSavingStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String depositAmount,
      String depositTerm,
      String interestRatePerYear,
      String dateUnit,
      bool isCalculateButtonEnabled,
      bool isReEntryButtonEnabled,
      String totalInterestEndPeriod,
      String totalInterestPrincipalEndPeriod});
}

/// @nodoc
class __$$CalculateSavingStateImplCopyWithImpl<$Res>
    extends _$CalculateSavingStateCopyWithImpl<$Res, _$CalculateSavingStateImpl>
    implements _$$CalculateSavingStateImplCopyWith<$Res> {
  __$$CalculateSavingStateImplCopyWithImpl(_$CalculateSavingStateImpl _value,
      $Res Function(_$CalculateSavingStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? depositAmount = null,
    Object? depositTerm = null,
    Object? interestRatePerYear = null,
    Object? dateUnit = null,
    Object? isCalculateButtonEnabled = null,
    Object? isReEntryButtonEnabled = null,
    Object? totalInterestEndPeriod = null,
    Object? totalInterestPrincipalEndPeriod = null,
  }) {
    return _then(_$CalculateSavingStateImpl(
      depositAmount: null == depositAmount
          ? _value.depositAmount
          : depositAmount // ignore: cast_nullable_to_non_nullable
              as String,
      depositTerm: null == depositTerm
          ? _value.depositTerm
          : depositTerm // ignore: cast_nullable_to_non_nullable
              as String,
      interestRatePerYear: null == interestRatePerYear
          ? _value.interestRatePerYear
          : interestRatePerYear // ignore: cast_nullable_to_non_nullable
              as String,
      dateUnit: null == dateUnit
          ? _value.dateUnit
          : dateUnit // ignore: cast_nullable_to_non_nullable
              as String,
      isCalculateButtonEnabled: null == isCalculateButtonEnabled
          ? _value.isCalculateButtonEnabled
          : isCalculateButtonEnabled // ignore: cast_nullable_to_non_nullable
              as bool,
      isReEntryButtonEnabled: null == isReEntryButtonEnabled
          ? _value.isReEntryButtonEnabled
          : isReEntryButtonEnabled // ignore: cast_nullable_to_non_nullable
              as bool,
      totalInterestEndPeriod: null == totalInterestEndPeriod
          ? _value.totalInterestEndPeriod
          : totalInterestEndPeriod // ignore: cast_nullable_to_non_nullable
              as String,
      totalInterestPrincipalEndPeriod: null == totalInterestPrincipalEndPeriod
          ? _value.totalInterestPrincipalEndPeriod
          : totalInterestPrincipalEndPeriod // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$CalculateSavingStateImpl implements _CalculateSavingState {
  const _$CalculateSavingStateImpl(
      {this.depositAmount = '',
      this.depositTerm = '',
      this.interestRatePerYear = '',
      this.dateUnit = '',
      this.isCalculateButtonEnabled = false,
      this.isReEntryButtonEnabled = false,
      this.totalInterestEndPeriod = '',
      this.totalInterestPrincipalEndPeriod = ''});

  @override
  @JsonKey()
  final String depositAmount;
  @override
  @JsonKey()
  final String depositTerm;
  @override
  @JsonKey()
  final String interestRatePerYear;
  @override
  @JsonKey()
  final String dateUnit;
  @override
  @JsonKey()
  final bool isCalculateButtonEnabled;
  @override
  @JsonKey()
  final bool isReEntryButtonEnabled;
  @override
  @JsonKey()
  final String totalInterestEndPeriod;
  @override
  @JsonKey()
  final String totalInterestPrincipalEndPeriod;

  @override
  String toString() {
    return 'CalculateSavingState(depositAmount: $depositAmount, depositTerm: $depositTerm, interestRatePerYear: $interestRatePerYear, dateUnit: $dateUnit, isCalculateButtonEnabled: $isCalculateButtonEnabled, isReEntryButtonEnabled: $isReEntryButtonEnabled, totalInterestEndPeriod: $totalInterestEndPeriod, totalInterestPrincipalEndPeriod: $totalInterestPrincipalEndPeriod)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CalculateSavingStateImpl &&
            (identical(other.depositAmount, depositAmount) ||
                other.depositAmount == depositAmount) &&
            (identical(other.depositTerm, depositTerm) ||
                other.depositTerm == depositTerm) &&
            (identical(other.interestRatePerYear, interestRatePerYear) ||
                other.interestRatePerYear == interestRatePerYear) &&
            (identical(other.dateUnit, dateUnit) ||
                other.dateUnit == dateUnit) &&
            (identical(
                    other.isCalculateButtonEnabled, isCalculateButtonEnabled) ||
                other.isCalculateButtonEnabled == isCalculateButtonEnabled) &&
            (identical(other.isReEntryButtonEnabled, isReEntryButtonEnabled) ||
                other.isReEntryButtonEnabled == isReEntryButtonEnabled) &&
            (identical(other.totalInterestEndPeriod, totalInterestEndPeriod) ||
                other.totalInterestEndPeriod == totalInterestEndPeriod) &&
            (identical(other.totalInterestPrincipalEndPeriod,
                    totalInterestPrincipalEndPeriod) ||
                other.totalInterestPrincipalEndPeriod ==
                    totalInterestPrincipalEndPeriod));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      depositAmount,
      depositTerm,
      interestRatePerYear,
      dateUnit,
      isCalculateButtonEnabled,
      isReEntryButtonEnabled,
      totalInterestEndPeriod,
      totalInterestPrincipalEndPeriod);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CalculateSavingStateImplCopyWith<_$CalculateSavingStateImpl>
      get copyWith =>
          __$$CalculateSavingStateImplCopyWithImpl<_$CalculateSavingStateImpl>(
              this, _$identity);
}

abstract class _CalculateSavingState implements CalculateSavingState {
  const factory _CalculateSavingState(
          {final String depositAmount,
          final String depositTerm,
          final String interestRatePerYear,
          final String dateUnit,
          final bool isCalculateButtonEnabled,
          final bool isReEntryButtonEnabled,
          final String totalInterestEndPeriod,
          final String totalInterestPrincipalEndPeriod}) =
      _$CalculateSavingStateImpl;

  @override
  String get depositAmount;
  @override
  String get depositTerm;
  @override
  String get interestRatePerYear;
  @override
  String get dateUnit;
  @override
  bool get isCalculateButtonEnabled;
  @override
  bool get isReEntryButtonEnabled;
  @override
  String get totalInterestEndPeriod;
  @override
  String get totalInterestPrincipalEndPeriod;
  @override
  @JsonKey(ignore: true)
  _$$CalculateSavingStateImplCopyWith<_$CalculateSavingStateImpl>
      get copyWith => throw _privateConstructorUsedError;
}
