// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'exchange_rate_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$ExchangeRateState {
  String get dailyExchangeRate => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ExchangeRateStateCopyWith<ExchangeRateState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ExchangeRateStateCopyWith<$Res> {
  factory $ExchangeRateStateCopyWith(
          ExchangeRateState value, $Res Function(ExchangeRateState) then) =
      _$ExchangeRateStateCopyWithImpl<$Res, ExchangeRateState>;
  @useResult
  $Res call({String dailyExchangeRate});
}

/// @nodoc
class _$ExchangeRateStateCopyWithImpl<$Res, $Val extends ExchangeRateState>
    implements $ExchangeRateStateCopyWith<$Res> {
  _$ExchangeRateStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? dailyExchangeRate = null,
  }) {
    return _then(_value.copyWith(
      dailyExchangeRate: null == dailyExchangeRate
          ? _value.dailyExchangeRate
          : dailyExchangeRate // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ExchangeRateStateImplCopyWith<$Res>
    implements $ExchangeRateStateCopyWith<$Res> {
  factory _$$ExchangeRateStateImplCopyWith(_$ExchangeRateStateImpl value,
          $Res Function(_$ExchangeRateStateImpl) then) =
      __$$ExchangeRateStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String dailyExchangeRate});
}

/// @nodoc
class __$$ExchangeRateStateImplCopyWithImpl<$Res>
    extends _$ExchangeRateStateCopyWithImpl<$Res, _$ExchangeRateStateImpl>
    implements _$$ExchangeRateStateImplCopyWith<$Res> {
  __$$ExchangeRateStateImplCopyWithImpl(_$ExchangeRateStateImpl _value,
      $Res Function(_$ExchangeRateStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? dailyExchangeRate = null,
  }) {
    return _then(_$ExchangeRateStateImpl(
      dailyExchangeRate: null == dailyExchangeRate
          ? _value.dailyExchangeRate
          : dailyExchangeRate // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$ExchangeRateStateImpl implements _ExchangeRateState {
  const _$ExchangeRateStateImpl({this.dailyExchangeRate = ''});

  @override
  @JsonKey()
  final String dailyExchangeRate;

  @override
  String toString() {
    return 'ExchangeRateState(dailyExchangeRate: $dailyExchangeRate)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ExchangeRateStateImpl &&
            (identical(other.dailyExchangeRate, dailyExchangeRate) ||
                other.dailyExchangeRate == dailyExchangeRate));
  }

  @override
  int get hashCode => Object.hash(runtimeType, dailyExchangeRate);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ExchangeRateStateImplCopyWith<_$ExchangeRateStateImpl> get copyWith =>
      __$$ExchangeRateStateImplCopyWithImpl<_$ExchangeRateStateImpl>(
          this, _$identity);
}

abstract class _ExchangeRateState implements ExchangeRateState {
  const factory _ExchangeRateState({final String dailyExchangeRate}) =
      _$ExchangeRateStateImpl;

  @override
  String get dailyExchangeRate;
  @override
  @JsonKey(ignore: true)
  _$$ExchangeRateStateImplCopyWith<_$ExchangeRateStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
