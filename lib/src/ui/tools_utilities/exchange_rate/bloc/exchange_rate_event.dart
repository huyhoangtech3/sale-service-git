import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:base_architecture_flutter/src/base/bloc/base_bloc_event.dart';
part 'exchange_rate_event.freezed.dart';

abstract class ExchangeRateEvent extends BaseBlocEvent {
  const ExchangeRateEvent();
}

@freezed
class ExchangeRateTextFieldChanged extends ExchangeRateEvent with _$ExchangeRateTextFieldChanged {
  const factory ExchangeRateTextFieldChanged({
    required String dailyExchangeRate,
  }) = _ExchangeRateTextFieldChanged;
}
