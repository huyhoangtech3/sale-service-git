import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:base_architecture_flutter/src/base/bloc/base_bloc_state.dart';
part 'exchange_rate_state.freezed.dart';

@freezed
class ExchangeRateState extends BaseBlocState with _$ExchangeRateState {
  const factory ExchangeRateState({
    @Default('') String dailyExchangeRate,
  }) = _ExchangeRateState;
}
