import 'dart:async';

import 'package:domain/domain.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

import 'package:base_architecture_flutter/src/app.dart';

@Injectable()
class ExchangeRateBloc extends BaseBloc<ExchangeRateEvent, ExchangeRateState> {
  ExchangeRateBloc(this._exchangeRateUseCase) : super(const ExchangeRateState()) {
    on<ExchangeRateTextFieldChanged>(
      _onExchangeRateTextFieldChanged,
      transformer: distinct(),
    );
  }

  final ExchangeRateUseCase _exchangeRateUseCase;

  void _onExchangeRateTextFieldChanged(ExchangeRateTextFieldChanged event, Emitter<ExchangeRateState> emit) {
    emit(state.copyWith(
      dailyExchangeRate: event.dailyExchangeRate,
    ));
  }
}
