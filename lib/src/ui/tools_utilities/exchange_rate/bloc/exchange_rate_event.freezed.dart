// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'exchange_rate_event.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$ExchangeRateTextFieldChanged {
  String get dailyExchangeRate => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ExchangeRateTextFieldChangedCopyWith<ExchangeRateTextFieldChanged>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ExchangeRateTextFieldChangedCopyWith<$Res> {
  factory $ExchangeRateTextFieldChangedCopyWith(
          ExchangeRateTextFieldChanged value,
          $Res Function(ExchangeRateTextFieldChanged) then) =
      _$ExchangeRateTextFieldChangedCopyWithImpl<$Res,
          ExchangeRateTextFieldChanged>;
  @useResult
  $Res call({String dailyExchangeRate});
}

/// @nodoc
class _$ExchangeRateTextFieldChangedCopyWithImpl<$Res,
        $Val extends ExchangeRateTextFieldChanged>
    implements $ExchangeRateTextFieldChangedCopyWith<$Res> {
  _$ExchangeRateTextFieldChangedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? dailyExchangeRate = null,
  }) {
    return _then(_value.copyWith(
      dailyExchangeRate: null == dailyExchangeRate
          ? _value.dailyExchangeRate
          : dailyExchangeRate // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ExchangeRateTextFieldChangedImplCopyWith<$Res>
    implements $ExchangeRateTextFieldChangedCopyWith<$Res> {
  factory _$$ExchangeRateTextFieldChangedImplCopyWith(
          _$ExchangeRateTextFieldChangedImpl value,
          $Res Function(_$ExchangeRateTextFieldChangedImpl) then) =
      __$$ExchangeRateTextFieldChangedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String dailyExchangeRate});
}

/// @nodoc
class __$$ExchangeRateTextFieldChangedImplCopyWithImpl<$Res>
    extends _$ExchangeRateTextFieldChangedCopyWithImpl<$Res,
        _$ExchangeRateTextFieldChangedImpl>
    implements _$$ExchangeRateTextFieldChangedImplCopyWith<$Res> {
  __$$ExchangeRateTextFieldChangedImplCopyWithImpl(
      _$ExchangeRateTextFieldChangedImpl _value,
      $Res Function(_$ExchangeRateTextFieldChangedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? dailyExchangeRate = null,
  }) {
    return _then(_$ExchangeRateTextFieldChangedImpl(
      dailyExchangeRate: null == dailyExchangeRate
          ? _value.dailyExchangeRate
          : dailyExchangeRate // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$ExchangeRateTextFieldChangedImpl
    implements _ExchangeRateTextFieldChanged {
  const _$ExchangeRateTextFieldChangedImpl({required this.dailyExchangeRate});

  @override
  final String dailyExchangeRate;

  @override
  String toString() {
    return 'ExchangeRateTextFieldChanged(dailyExchangeRate: $dailyExchangeRate)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ExchangeRateTextFieldChangedImpl &&
            (identical(other.dailyExchangeRate, dailyExchangeRate) ||
                other.dailyExchangeRate == dailyExchangeRate));
  }

  @override
  int get hashCode => Object.hash(runtimeType, dailyExchangeRate);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ExchangeRateTextFieldChangedImplCopyWith<
          _$ExchangeRateTextFieldChangedImpl>
      get copyWith => __$$ExchangeRateTextFieldChangedImplCopyWithImpl<
          _$ExchangeRateTextFieldChangedImpl>(this, _$identity);
}

abstract class _ExchangeRateTextFieldChanged
    implements ExchangeRateTextFieldChanged {
  const factory _ExchangeRateTextFieldChanged(
          {required final String dailyExchangeRate}) =
      _$ExchangeRateTextFieldChangedImpl;

  @override
  String get dailyExchangeRate;
  @override
  @JsonKey(ignore: true)
  _$$ExchangeRateTextFieldChangedImplCopyWith<
          _$ExchangeRateTextFieldChangedImpl>
      get copyWith => throw _privateConstructorUsedError;
}
