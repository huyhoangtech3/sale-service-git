import 'dart:math';

import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:resources/resources.dart';
import 'package:base_architecture_flutter/src/app.dart';
import 'package:shared/shared.dart';

class ExchangeRateDialog extends StatefulWidget {
  const ExchangeRateDialog({super.key});

  @override
  State<StatefulWidget> createState() {
    return _ExchangeRateDialogState();
  }
}

class _ExchangeRateDialogState extends BasePageState<ExchangeRateDialog, ExchangeRateBloc> {
  List<ExchangeRate> exchangeRate = <ExchangeRate>[];
  late ExchangeRateDataSource exchangeRateDataSource;

  @override
  void initState() {
    bloc.add(ExchangeRateTextFieldChanged(dailyExchangeRate: '11:30 27/08/2024'));
    exchangeRate = getExchangeRateData();
    exchangeRateDataSource = ExchangeRateDataSource(exchangeRateData: exchangeRate);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget buildPage(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(Dimens.d12.responsive()),
      margin: EdgeInsets.fromLTRB(Dimens.d264.responsive(), 0.0, 0.0, 0.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(0),
        color: Colors.white,
      ),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: Assets.images.icToolListedrate.provider(),
                    fit: BoxFit.fitHeight,
                  ),
                ),
                child: SizedBox(
                  width: Dimens.d40.responsive(), height: Dimens.d40.responsive(),
                ),
              ),
              Text(
                S.current.exchangeRate,
                style: AppTextStyles.s16w500Tertiary(),
              ),
              Material(
                color: Colors.transparent,
                child: InkWell(
                  onTap: () => navigator.pop(),
                  child: Container(
                    margin: EdgeInsets.all(Dimens.d4.responsive()),
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: Assets.images.close.provider(),
                        fit: BoxFit.fitHeight,
                      ),
                    ),
                    child: SizedBox(
                      width: Dimens.d24.responsive(), height: Dimens.d24.responsive(),
                    ),
                  ),
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                S.current.dailyExchangeRate,
                style: AppTextStyles.s12w200Tertiary(),
              ),
              Text(
                ' 11:30 27/08/2024',
                style: AppTextStyles.s12w400Tertiary(),
              ),
            ],
          ),
          SizedBox(height: Dimens.d8.responsive()),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                width: Dimens.d740.responsive(),
                height: Dimens.d440.responsive(),
                child: SfDataGridTheme(
                  data: SfDataGridThemeData(headerColor: Colors.orange.shade50),
                  child: SfDataGrid(
                    allowSwiping: true,
                    swipeMaxOffset: 100.0,
                    showCheckboxColumn: true,
                    selectionMode: SelectionMode.multiple,
                    source: exchangeRateDataSource,
                    columnWidthMode: ColumnWidthMode.fill,
                    allowPullToRefresh: true,
                    loadMoreViewBuilder: (BuildContext context, LoadMoreRows loadMoreRows) {
                      Future<String> loadRows() async {
                        await loadMoreRows();
                        return Future<String>.value('Completed');
                      }

                      return FutureBuilder<String>(
                          initialData: 'loading',
                          future: loadRows(),
                          builder: (context, snapShot) {
                            if (snapShot.data == 'loading') {
                              return Container(
                                  height: 60.0,
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: BorderDirectional(
                                          top: BorderSide(
                                              width: 1.0,
                                              color: Color.fromRGBO(0, 0, 0, 0.26)))),
                                  alignment: Alignment.center,
                                  child: CircularProgressIndicator(
                                      valueColor: AlwaysStoppedAnimation(Colors.deepPurple)));
                            } else {
                              return SizedBox.fromSize(size: Size.zero);
                            }
                          });
                    },
                    allowColumnsDragging: true,
                    onColumnDragging: (DataGridColumnDragDetails details) {
                      if (details.action == DataGridColumnDragAction.dropped && details.to != null) {
                        final GridColumn rearrangeColumn = exchangeRateDataSource.columns[details.from];
                        exchangeRateDataSource.columns.removeAt(details.from);
                        exchangeRateDataSource.columns.insert(details.to!, rearrangeColumn);
                        exchangeRateDataSource.buildDataGridRows();
                        exchangeRateDataSource.refreshDataGrid();
                      }
                      return true;
                    },
                    startSwipeActionsBuilder: (BuildContext context, DataGridRow row, int rowIndex) {
                      return GestureDetector(
                          onTap: () {
                            exchangeRateDataSource._exchangeRateData.insert(rowIndex, DataGridRow(cells: [
                                  DataGridCell<String>(columnName: '1', value: 'USD'),
                                  DataGridCell<int>(columnName: '2', value: 32910),
                                  DataGridCell<int>(columnName: '3', value: 42910),
                                  DataGridCell<int>(columnName: '4', value: 52910),
                                ]));
                            exchangeRateDataSource.updateDataGridSource();
                          },
                          child: Container(
                              color: Colors.greenAccent,
                              child: Center(
                                child: Icon(Icons.add),
                              )));
                    },
                    endSwipeActionsBuilder: (BuildContext context, DataGridRow row, int rowIndex) {
                      return GestureDetector(
                          onTap: () {
                            exchangeRateDataSource._exchangeRateData.removeAt(rowIndex);
                            exchangeRateDataSource.updateDataGridSource();
                          },
                          child: Container(
                              color: Colors.redAccent,
                              child: Center(
                                child: Icon(Icons.delete),
                              )));
                    },
                    columns: exchangeRateDataSource.columns,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );

  }

  List<ExchangeRate> getExchangeRateData() {
    return [
      ExchangeRate('USD', 22910, 22920, 23120),
      ExchangeRate('EUR', 22910, 22920, 23120),
      ExchangeRate('GBP', 22910, 22920, 23120),
      ExchangeRate('JPY', 22910, 22920, 23120),
      ExchangeRate('AUD', 22910, 22920, 23120),
      ExchangeRate('SGD', 22910, 22920, 23120),
      ExchangeRate('THB', 22910, 22920, 23120),
      ExchangeRate('CAD', 22910, 22920, 23120),
      ExchangeRate('CHF', 22910, 22920, 23120),
      ExchangeRate('HKD', 22910, 22920, 23120),
      ExchangeRate('CNY', 22910, 22920, 23120),
      ExchangeRate('DKK', 22910, 22920, 23120),
      ExchangeRate('INR', 22910, 22920, 23120),
      ExchangeRate('KRW', 22910, 22920, 23120),
      ExchangeRate('KWD', 22910, 22920, 23120),
      ExchangeRate('MYR', 22910, 22920, 23120),
      ExchangeRate('NOK', 22910, 22920, 23120),
      ExchangeRate('RUB', 22910, 22920, 23120),
      ExchangeRate('SAR', 22910, 22920, 23120),
      ExchangeRate('SEK', 22910, 22920, 23120)
    ];
  }
}

class ExchangeRate {
  ExchangeRate(this.foreignCurrency, this.cashBuy, this.transferBuy, this.transferSale);
  final String foreignCurrency;
  final int cashBuy;
  final int transferBuy;
  final int transferSale;
}

class ExchangeRateDataSource extends DataGridSource {
  ExchangeRateDataSource({
    required List<ExchangeRate> exchangeRateData,
  }) {
    _exchangeRateData = exchangeRateData.map<DataGridRow>((e) => DataGridRow(cells: [
      DataGridCell<String>(columnName: '1', value: e.foreignCurrency),
      DataGridCell<int>(columnName: '2', value: e.cashBuy),
      DataGridCell<int>(columnName: '3', value: e.transferBuy),
      DataGridCell<int>(columnName: '4', value: e.transferSale),
    ])).toList();
  }

  List<GridColumn> columns = [
    GridColumn(
      columnName: '1',
      label: Container(
          padding: EdgeInsets.all(Dimens.d16.responsive()),
          alignment: Alignment.centerLeft,
          child: Text(
            S.current.foreignCurrency,
            style: AppTextStyles.s12w500Tertiary(),
          ))),
    GridColumn(
        columnName: '2',
        label: Container(
            padding: EdgeInsets.all(Dimens.d16.responsive()),
            alignment: Alignment.centerRight,
            child: Text(
              S.current.cashBuy,
              style: AppTextStyles.s12w500Tertiary(),
            ))),
    GridColumn(
        columnName: '3',
        label: Container(
            padding: EdgeInsets.all(Dimens.d16.responsive()),
            alignment: Alignment.centerRight,
            child: Text(
              S.current.transferBuy,
              style: AppTextStyles.s12w500Tertiary(),
            ))),
    GridColumn(
        columnName: '4',
        label: Container(
            padding: EdgeInsets.all(Dimens.d16.responsive()),
            alignment: Alignment.centerRight,
            child: Text(
              S.current.transferSale,
              style: AppTextStyles.s12w500Tertiary(),
            ))),
  ];
  List<DataGridRow> _exchangeRateData = [];
  List<ExchangeRate> exchangeRateData = [
    ExchangeRate('USD', 22910, 22920, 23120),
    ExchangeRate('EUR', 22910, 22920, 23120),
    ExchangeRate('GBP', 22910, 22920, 23120),
    ExchangeRate('JPY', 22910, 22920, 23120),
    ExchangeRate('AUD', 22910, 22920, 23120),
    ExchangeRate('SGD', 22910, 22920, 23120),
    ExchangeRate('THB', 22910, 22920, 23120),
    ExchangeRate('CAD', 22910, 22920, 23120),
    ExchangeRate('CHF', 22910, 22920, 23120),
    ExchangeRate('CHF', 22910, 22920, 23120),
    ExchangeRate('HKD', 22910, 22920, 23120),
    ExchangeRate('CNY', 22910, 22920, 23120),
    ExchangeRate('DKK', 22910, 22920, 23120),
    ExchangeRate('INR', 22910, 22920, 23120),
    ExchangeRate('KRW', 22910, 22920, 23120),
    ExchangeRate('KWD', 22910, 22920, 23120),
    ExchangeRate('MYR', 22910, 22920, 23120),
    ExchangeRate('NOK', 22910, 22920, 23120),
    ExchangeRate('RUB', 22910, 22920, 23120),
    ExchangeRate('SAR', 22910, 22920, 23120),
    ExchangeRate('SEK', 22910, 22920, 23120)
  ];

  @override
  List<DataGridRow> get rows => _exchangeRateData;

  @override
  DataGridRowAdapter buildRow(DataGridRow row) {
    return DataGridRowAdapter(
        cells: row.getCells().map<Widget>((e) {
          return Container(
            alignment: (e.columnName == '1' ? Alignment.centerLeft : Alignment.centerRight),
            padding: EdgeInsets.fromLTRB(Dimens.d16.responsive(), 0, Dimens.d16.responsive(), 0),
            child: Text(e.value.toString()),
          );
        }).toList());
  }

  @override
  Future<void> handleRefresh() async {
    await Future.delayed(const Duration(seconds: 1));
    notifyListeners();
  }

  @override
  Future<void> handleLoadMoreRows() async {
    await Future.delayed(const Duration(seconds: 2));
    _addMoreRows(exchangeRateData, 15);
    buildDataGridRows();
    notifyListeners();
  }

  void buildDataGridRows() {
    _exchangeRateData = exchangeRateData.map<DataGridRow>((e) {
      return DataGridRow(
          cells: columns.map<DataGridCell>((column) {
            dynamic cellValue;
            switch (column.columnName) {
              case '1':
                cellValue = e.foreignCurrency;
                break;
              case '2':
                cellValue = e.cashBuy;
                break;
              case '3':
                cellValue = e.transferBuy;
                break;
              case '4':
                cellValue = e.transferSale;
                break;
              default:
                cellValue = null; // Handle any unexpected column names
            }
            return DataGridCell(
              columnName: column.columnName,
              value: cellValue,
            );
          }).toList());
    }).toList();
  }

  void _addMoreRows(List<ExchangeRate> exchangeRateData, int count) {
    final startIndex = exchangeRateData.isNotEmpty ? exchangeRateData.length : 0, endIndex = startIndex + count;
    for (int i = startIndex; i < endIndex; i++) {
      _exchangeRateData.add(ExchangeRate(
        exchangeRateData[i].foreignCurrency + i.toString(),
        exchangeRateData[i].cashBuy + i,
        exchangeRateData[i].transferBuy + i,
        exchangeRateData[i].transferSale + i,
      ) as DataGridRow);
    }
  }

  void refreshDataGrid() {
    notifyListeners();
  }

  void updateDataGridSource() {
    notifyListeners();
  }
}
