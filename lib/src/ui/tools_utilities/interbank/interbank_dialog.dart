import 'dart:math';

import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:resources/resources.dart';
import 'package:base_architecture_flutter/src/app.dart';
import 'package:shared/shared.dart';

class InterbankDialog extends StatefulWidget {
  const InterbankDialog({super.key});

  @override
  State<StatefulWidget> createState() {
    return _InterbankDialogState();
  }
}

class _InterbankDialogState extends BasePageState<InterbankDialog, InterbankBloc> {
  List<Currency> currency = <Currency>[];
  List<Interbank> interbank = <Interbank>[];
  late InterbankDataSource interbankDataSource;
  late CurrencyDataSource currencyDataSource;

  @override
  void initState() {
    bloc.add(InterbankTextFieldChanged(interbankRate: '11:30 27/08/2024'));
    interbank = getInterbankData();
    interbankDataSource = InterbankDataSource(interbankData: interbank);
    currency = getCurrencyData();
    currencyDataSource = CurrencyDataSource(currencyData: currency);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget buildPage(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(Dimens.d12.responsive()),
      margin: EdgeInsets.fromLTRB(Dimens.d264.responsive(), 0.0, 0.0, 0.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(0),
        color: Colors.white,
      ),
      child: SingleChildScrollView(
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: Assets.images.icToolInterbankrate.provider(),
                      fit: BoxFit.fitHeight,
                    ),
                  ),
                  child: SizedBox(
                    width: Dimens.d40.responsive(), height: Dimens.d40.responsive(),
                  ),
                ),
                Text(
                  S.current.exchangeRate,
                  style: AppTextStyles.s16w500Tertiary(),
                ),
                Material(
                  color: Colors.transparent,
                  child: InkWell(
                    onTap: () => navigator.pop(),
                    child: Container(
                      margin: EdgeInsets.all(Dimens.d4.responsive()),
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: Assets.images.close.provider(),
                          fit: BoxFit.fitHeight,
                        ),
                      ),
                      child: SizedBox(
                        width: Dimens.d24.responsive(), height: Dimens.d24.responsive(),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  S.current.dailyExchangeRate,
                  style: AppTextStyles.s12w200Tertiary(),
                ),
                Text(
                  ' 11:30 27/08/2024',
                  style: AppTextStyles.s12w400Tertiary(),
                ),
              ],
            ),
            SizedBox(height: Dimens.d8.responsive()),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: Dimens.d740.responsive(),
                  height: Dimens.d140.responsive(),
                  child: SfDataGridTheme(
                    data: SfDataGridThemeData(headerColor: Colors.orange.shade50),
                    child: SfDataGrid(
                      selectionMode: SelectionMode.multiple,
                      source: currencyDataSource,
                      columnWidthMode: ColumnWidthMode.fill,
                      allowPullToRefresh: true,
                      loadMoreViewBuilder: (BuildContext context, LoadMoreRows loadMoreRows) {
                        Future<String> loadRows() async {
                          await loadMoreRows();
                          return Future<String>.value('Completed');
                        }
                        return FutureBuilder<String>(
                            initialData: 'loading',
                            future: loadRows(),
                            builder: (context, snapShot) {
                              if (snapShot.data == 'loading') {
                                return Container(
                                    height: 60.0,
                                    width: double.infinity,
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        border: BorderDirectional(
                                            top: BorderSide(
                                                width: 1.0,
                                                color: Color.fromRGBO(0, 0, 0, 0.26)))),
                                    alignment: Alignment.center,
                                    child: CircularProgressIndicator(
                                        valueColor: AlwaysStoppedAnimation(Colors.deepPurple)));
                              } else {
                                return SizedBox.fromSize(size: Size.zero);
                              }
                            });
                      },
                      allowColumnsDragging: true,
                      onColumnDragging: (DataGridColumnDragDetails details) {
                        if (details.action == DataGridColumnDragAction.dropped && details.to != null) {
                          final GridColumn rearrangeColumn = currencyDataSource.columns[details.from];
                          currencyDataSource.columns.removeAt(details.from);
                          currencyDataSource.columns.insert(details.to!, rearrangeColumn);
                          currencyDataSource.buildDataGridRows();
                          currencyDataSource.refreshDataGrid();
                        }
                        return true;
                      },
                      columns: currencyDataSource.columns,
                    ),
                  ),
                ),
              ],
            ),
            Container(
              padding: EdgeInsets.all(Dimens.d12.responsive()),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        '- Tỷ giá Interbank là giá vốn của ngân hàng.',
                        style: AppTextStyles.s12w400TertiaryItalic(),
                      ),
                    ],
                  ),
                  SizedBox(height: Dimens.d8.responsive()),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        '- Biên định hướng là các biên bản tham khảo và RMs căn cứ vào biên định hướng để ra quyết định chào giá tới khách hàng.',
                        style: AppTextStyles.s12w400TertiaryItalic(),
                      ),
                    ],
                  ),
                  SizedBox(height: Dimens.d8.responsive()),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        '- Tỷ giá mua bán chào khách hàng = tỷ giá Interbank +/- biên giao dịch.',
                        style: AppTextStyles.s12w400TertiaryItalic(),
                      ),
                    ],
                  ),
                  SizedBox(height: Dimens.d8.responsive()),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        '- Biên mua bán càng cao thì lợi nhuận càng lớn (lợi nhuận giao dịch = biên giao dịch * doanh số giao dịch).',
                        style: AppTextStyles.s12w400TertiaryItalic(),
                      ),
                    ],
                  ),
                  SizedBox(height: Dimens.d16.responsive()),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        'BIÊN ĐỊNH HƯỚNG MUA BÁN NGOẠI TỆ THÁNG 2 VỚI KHÁCH HÀNG',
                        style: AppTextStyles.s12w500Tertiary(),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: Dimens.d740.responsive(),
                  height: Dimens.d380.responsive(),
                  child: SfDataGridTheme(
                    data: SfDataGridThemeData(headerColor: Colors.orange.shade50),
                    child: SfDataGrid(
                      selectionMode: SelectionMode.multiple,
                      source: interbankDataSource,
                      columnWidthMode: ColumnWidthMode.fill,
                      allowPullToRefresh: true,
                      loadMoreViewBuilder: (BuildContext context, LoadMoreRows loadMoreRows) {
                        Future<String> loadRows() async {
                          await loadMoreRows();
                          return Future<String>.value('Completed');
                        }

                        return FutureBuilder<String>(
                            initialData: 'loading',
                            future: loadRows(),
                            builder: (context, snapShot) {
                              if (snapShot.data == 'loading') {
                                return Container(
                                    height: 60.0,
                                    width: double.infinity,
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        border: BorderDirectional(
                                            top: BorderSide(
                                                width: 1.0,
                                                color: Color.fromRGBO(0, 0, 0, 0.26)))),
                                    alignment: Alignment.center,
                                    child: CircularProgressIndicator(
                                        valueColor: AlwaysStoppedAnimation(Colors.deepPurple)));
                              } else {
                                return SizedBox.fromSize(size: Size.zero);
                              }
                            });
                      },
                      allowColumnsDragging: true,
                      onColumnDragging: (DataGridColumnDragDetails details) {
                        if (details.action == DataGridColumnDragAction.dropped && details.to != null) {
                          final GridColumn rearrangeColumn = interbankDataSource.columns[details.from];
                          interbankDataSource.columns.removeAt(details.from);
                          interbankDataSource.columns.insert(details.to!, rearrangeColumn);
                          interbankDataSource.buildDataGridRows();
                          interbankDataSource.refreshDataGrid();
                        }
                        return true;
                      },
                      columns: interbankDataSource.columns,
                      // gridLinesVisibility: GridLinesVisibility.both,
                      headerGridLinesVisibility: GridLinesVisibility.both,
                      stackedHeaderRows: <StackedHeaderRow>[
                        StackedHeaderRow(cells: [
                          StackedHeaderCell(
                              columnNames: ['2', '3'],
                              child: Container(
                                  color: Colors.orange.shade50,
                                  child: Center(child: Text(S.current.sme, style: AppTextStyles.s12w500Tertiary())))),
                          StackedHeaderCell(
                              columnNames: ['4', '5'],
                              child: Container(
                                  color: Colors.orange.shade50,
                                  child: Center(child: Text(S.current.indiv, style: AppTextStyles.s12w500Tertiary()))))
                        ])
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Container(
              padding: EdgeInsets.all(Dimens.d12.responsive()),
              child: Column(
                children: [
                  SizedBox(height: Dimens.d8.responsive()),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        'VÍ DỤ MINH HỌA',
                        style: AppTextStyles.s12w500Tertiary(),
                      ),
                    ],
                  ),
                  SizedBox(height: Dimens.d16.responsive()),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        '1. Giao dịch mua ngoại tệ của khách hàng',
                        style: AppTextStyles.s12w500Tertiary(),
                      ),
                    ],
                  ),
                  SizedBox(height: Dimens.d8.responsive()),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      RichText(
                        text: TextSpan(
                          text: '- Tỷ giá Interbank mua USD/VND',
                          style: AppTextStyles.s12w400Tertiary(),
                          children: [
                            TextSpan(
                              text: ' 23.175',
                              style: TextStyle(color: Colors.orange, fontSize: Dimens.d12.responsive(), fontWeight: FontWeight.bold)),
                          ],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: Dimens.d8.responsive()),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      RichText(
                        text: TextSpan(
                          text: '- Biên định hướng',
                          style: AppTextStyles.s12w400Tertiary(),
                          children: [
                            TextSpan(
                                text: ' 50 điểm',
                                style: TextStyle(color: Colors.orange, fontSize: Dimens.d12.responsive(), fontWeight: FontWeight.bold)),
                          ],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: Dimens.d8.responsive()),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        '- Căn cứ vào Biên định hướng và cạnh tranh, RM có thể chọn biên giao dịch phù hợp',
                        style: AppTextStyles.s12w400Tertiary(),
                      ),
                    ],
                  ),
                  SizedBox(height: Dimens.d8.responsive()),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        '* Ví dụ: Biên giao dịch = Biên định hướng + 10 điểm = 50 + 10 = 60 điểm >>> Tỷ giá mua USD/VND của khách hàng = 23175 - 60 = 23105',
                        style: AppTextStyles.s12w400Tertiary(),
                      ),
                    ],
                  ),
                  SizedBox(height: Dimens.d8.responsive()),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        '2. Giao dịch bán ngoại tệ của khách hàng',
                        style: AppTextStyles.s12w500Tertiary(),
                      ),
                    ],
                  ),
                  SizedBox(height: Dimens.d8.responsive()),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      RichText(
                        text: TextSpan(
                          text: '- Tỷ giá Interbank mua USD/VND',
                          style: AppTextStyles.s12w400Tertiary(),
                          children: [
                            TextSpan(
                                text: ' 23.180',
                                style: TextStyle(color: Colors.orange, fontSize: Dimens.d12.responsive(), fontWeight: FontWeight.bold)),
                          ],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: Dimens.d8.responsive()),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      RichText(
                        text: TextSpan(
                          text: '- Biên định hướng',
                          style: AppTextStyles.s12w400Tertiary(),
                          children: [
                            TextSpan(
                                text: ' 50 điểm',
                                style: TextStyle(color: Colors.orange, fontSize: Dimens.d12.responsive(), fontWeight: FontWeight.bold)),
                          ],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: Dimens.d8.responsive()),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        '- Căn cứ vào Biên định hướng và cạnh tranh, RM có thể chọn biên giao dịch phù hợp',
                        style: AppTextStyles.s12w400Tertiary(),
                      ),
                    ],
                  ),
                  SizedBox(height: Dimens.d8.responsive()),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        '* Ví dụ: Biên giao dịch = Biên định hướng + 10 điểm = 50 + 10 = 60 điểm >>> Tỷ giá mua USD/VND của khách hàng = 23180 + 40 = 23220',
                        style: AppTextStyles.s12w400Tertiary(),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  List<Currency> getCurrencyData() {
    return [
      Currency('USD/VND', 22910, 23920),
      Currency('EUR/VND', 25910, 26920),
    ];
  }

  List<Interbank> getInterbankData() {
    return [
      Interbank('Hà Nội 1', 50, 50, 50, 50),
      Interbank('Hà Nội 2', 50, 50, 50, 50),
      Interbank('Đông Bắc Bộ', 50, 50, 50, 50),
      Interbank('Tây Bắc Bộ', 50, 50, 50, 50),
      Interbank('Tây Nam Bộ', 50, 50, 50, 50),
      Interbank('Miền Trung', 50, 50, 50, 50),
      Interbank('Hồ Chí Minh 1', 50, 50, 50, 50),
      Interbank('Hồ Chí Minh 2', 50, 50, 50, 50),
      Interbank('Đông Nam Bộ', 50, 50, 50, 50),
      Interbank('Tây Nam Bộ', 50, 50, 50, 50),
    ];
  }
}

class Currency {
  Currency(this.currency, this.itbBuy, this.itbSell);
  final String currency;
  final int itbBuy;
  final int itbSell;
}

class Interbank {
  Interbank(this.area, this.directionalBoundaryBuySME, this.directionalBoundarySellSME, this.directionalBoundaryBuyINDIV, this.directionalBoundarySellINDIV);
  final String area;
  final int directionalBoundaryBuySME;
  final int directionalBoundarySellSME;
  final int directionalBoundaryBuyINDIV;
  final int directionalBoundarySellINDIV;
}

class CurrencyDataSource extends DataGridSource {
  CurrencyDataSource({
    required List<Currency> currencyData,
  }) {
    _currencyData = currencyData.map<DataGridRow>((e) => DataGridRow(cells: [
      DataGridCell<String>(columnName: '1', value: e.currency),
      DataGridCell<int>(columnName: '2', value: e.itbBuy),
      DataGridCell<int>(columnName: '3', value: e.itbSell),
    ])).toList();
  }

  List<GridColumn> columns = [
    GridColumn(
        columnName: '1',
        label: Container(
            padding: EdgeInsets.all(Dimens.d16.responsive()),
            alignment: Alignment.centerLeft,
            child: Text(
              S.current.currency,
              style: AppTextStyles.s12w500Tertiary(),
            ))),
    GridColumn(
        columnName: '2',
        label: Container(
            padding: EdgeInsets.all(Dimens.d16.responsive()),
            alignment: Alignment.center,
            child: Text(
              S.current.itbPurchasePrice,
              style: AppTextStyles.s12w500Tertiary(),
            ))),
    GridColumn(
        columnName: '3',
        label: Container(
            padding: EdgeInsets.all(Dimens.d16.responsive()),
            alignment: Alignment.centerRight,
            child: Text(
              S.current.itpSellingPrice,
              style: AppTextStyles.s12w500Tertiary(),
            ))),
  ];
  List<DataGridRow> _currencyData = [];
  List<Currency> currencyData = [
    Currency('USD/VND', 22910, 23920),
    Currency('EUR/VND', 25910, 26920),
  ];

  @override
  List<DataGridRow> get rows => _currencyData;

  @override
  DataGridRowAdapter buildRow(DataGridRow row) {
    return DataGridRowAdapter(
        cells: row.getCells().map<Widget>((e) {
          return Container(
            alignment: e.columnName == '1'
                ? Alignment.centerLeft
                : e.columnName == '2'
                ? Alignment.center
                : Alignment.centerRight,
            padding: EdgeInsets.fromLTRB(Dimens.d16.responsive(), 0, Dimens.d16.responsive(), 0),
            child: Text(e.value.toString()),
          );
        }).toList());
  }

  @override
  Future<void> handleRefresh() async {
    await Future.delayed(const Duration(seconds: 1));
    notifyListeners();
  }

  @override
  Future<void> handleLoadMoreRows() async {
    await Future.delayed(const Duration(seconds: 2));
    _addMoreRows(currencyData, 15);
    buildDataGridRows();
    notifyListeners();
  }

  void buildDataGridRows() {
    _currencyData = currencyData.map<DataGridRow>((e) {
      return DataGridRow(
          cells: columns.map<DataGridCell>((column) {
            dynamic cellValue;
            switch (column.columnName) {
              case '1':
                cellValue = e.currency;
                break;
              case '2':
                cellValue = e.itbBuy;
                break;
              case '3':
                cellValue = e.itbSell;
                break;
              default:
                cellValue = null; // Handle any unexpected column names
            }
            return DataGridCell(
              columnName: column.columnName,
              value: cellValue,
            );
          }).toList());
    }).toList();
  }

  void _addMoreRows(List<Currency> currencyData, int count) {
    final startIndex = currencyData.isNotEmpty ? currencyData.length : 0, endIndex = startIndex + count;
    for (int i = startIndex; i < endIndex; i++) {
      _currencyData.add(Currency(
        currencyData[i].currency + i.toString(),
        currencyData[i].itbBuy + i,
        currencyData[i].itbSell + i,
      ) as DataGridRow);
    }
  }

  void refreshDataGrid() {
    notifyListeners();
  }

  void updateDataGridSource() {
    notifyListeners();
  }
}

class InterbankDataSource extends DataGridSource {
  InterbankDataSource({
    required List<Interbank> interbankData,
  }) {
    _interbankData = interbankData.map<DataGridRow>((e) => DataGridRow(cells: [
      DataGridCell<String>(columnName: '1', value: e.area),
      DataGridCell<int>(columnName: '2', value: e.directionalBoundaryBuySME),
      DataGridCell<int>(columnName: '3', value: e.directionalBoundarySellSME),
      DataGridCell<int>(columnName: '4', value: e.directionalBoundaryBuyINDIV),
      DataGridCell<int>(columnName: '5', value: e.directionalBoundarySellINDIV),
    ])).toList();
  }

  List<GridColumn> columns = [
    GridColumn(
        columnName: '1',
        label: Container(
            padding: EdgeInsets.all(Dimens.d16.responsive()),
            alignment: Alignment.centerLeft,
            child: Text(
              S.current.foreignCurrency,
              style: AppTextStyles.s12w500Tertiary(),
            ))),
    GridColumn(
        columnName: '2',
        label: Container(
            padding: EdgeInsets.all(Dimens.d16.responsive()),
            alignment: Alignment.center,
            child: Text(
              S.current.cashBuy,
              style: AppTextStyles.s12w400Tertiary(),
            ))),
    GridColumn(
        columnName: '3',
        label: Container(
            padding: EdgeInsets.all(Dimens.d16.responsive()),
            alignment: Alignment.center,
            child: Text(
              S.current.transferBuy,
              style: AppTextStyles.s12w400Tertiary(),
            ))),
    GridColumn(
        columnName: '4',
        label: Container(
            padding: EdgeInsets.all(Dimens.d16.responsive()),
            alignment: Alignment.center,
            child: Text(
              S.current.transferSale,
              style: AppTextStyles.s12w400Tertiary(),
            ))),
    GridColumn(
        columnName: '5',
        label: Container(
            padding: EdgeInsets.all(Dimens.d16.responsive()),
            alignment: Alignment.center,
            child: Text(
              S.current.transferSale,
              style: AppTextStyles.s12w400Tertiary(),
            ))),
  ];
  List<DataGridRow> _interbankData = [];
  List<Interbank> interbankData = [
    Interbank('Hà Nội 1', 50, 50, 50, 50),
    Interbank('Hà Nội 2', 50, 50, 50, 50),
    Interbank('Đông Bắc Bộ', 50, 50, 50, 50),
    Interbank('Tây Bắc Bộ', 50, 50, 50, 50),
    Interbank('Tây Nam Bộ', 50, 50, 50, 50),
    Interbank('Miền Trung', 50, 50, 50, 50),
    Interbank('Hồ Chí Minh 1', 50, 50, 50, 50),
    Interbank('Hồ Chí Minh 2', 50, 50, 50, 50),
    Interbank('Đông Nam Bộ', 50, 50, 50, 50),
    Interbank('Tây Nam Bộ', 50, 50, 50, 50),
    Interbank('Hà Nội 1', 50, 50, 50, 50),
    Interbank('Hà Nội 2', 50, 50, 50, 50),
    Interbank('Đông Bắc Bộ', 50, 50, 50, 50),
    Interbank('Tây Bắc Bộ', 50, 50, 50, 50),
    Interbank('Tây Nam Bộ', 50, 50, 50, 50),
    Interbank('Miền Trung', 50, 50, 50, 50),
    Interbank('Hồ Chí Minh 1', 50, 50, 50, 50),
    Interbank('Hồ Chí Minh 2', 50, 50, 50, 50),
    Interbank('Đông Nam Bộ', 50, 50, 50, 50),
    Interbank('Tây Nam Bộ', 50, 50, 50, 50),
  ];

  @override
  List<DataGridRow> get rows => _interbankData;

  @override
  DataGridRowAdapter buildRow(DataGridRow row) {
    return DataGridRowAdapter(
        cells: row.getCells().map<Widget>((e) {
          return Container(
            alignment: (e.columnName == '1' ? Alignment.centerLeft : Alignment.center),
            padding: EdgeInsets.fromLTRB(Dimens.d16.responsive(), 0, Dimens.d16.responsive(), 0),
            child: Text(e.value.toString()),
          );
        }).toList());
  }

  @override
  Future<void> handleRefresh() async {
    await Future.delayed(const Duration(seconds: 1));
    notifyListeners();
  }

  @override
  Future<void> handleLoadMoreRows() async {
    await Future.delayed(const Duration(seconds: 2));
    _addMoreRows(interbankData, 15);
    buildDataGridRows();
    notifyListeners();
  }

  void buildDataGridRows() {
    _interbankData = interbankData.map<DataGridRow>((e) {
      return DataGridRow(
          cells: columns.map<DataGridCell>((column) {
            dynamic cellValue;
            switch (column.columnName) {
              case '1':
                cellValue = e.area;
                break;
              case '2':
                cellValue = e.directionalBoundarySellSME;
                break;
              case '3':
                cellValue = e.directionalBoundarySellSME;
                break;
              case '4':
                cellValue = e.directionalBoundaryBuyINDIV;
                break;
              case '5':
                cellValue = e.directionalBoundarySellINDIV;
                break;
              default:
                cellValue = null; // Handle any unexpected column names
            }
            return DataGridCell(
              columnName: column.columnName,
              value: cellValue,
            );
          }).toList());
    }).toList();
  }

  void _addMoreRows(List<Interbank> interbankData, int count) {
    final startIndex = interbankData.isNotEmpty ? interbankData.length : 0, endIndex = startIndex + count;
    for (int i = startIndex; i < endIndex; i++) {
      _interbankData.add(Interbank(
        interbankData[i].area + i.toString(),
        interbankData[i].directionalBoundaryBuySME + i,
        interbankData[i].directionalBoundarySellSME + i,
        interbankData[i].directionalBoundaryBuyINDIV + i,
        interbankData[i].directionalBoundarySellINDIV + i,
      ) as DataGridRow);
    }
  }

  void refreshDataGrid() {
    notifyListeners();
  }

  void updateDataGridSource() {
    notifyListeners();
  }
}
