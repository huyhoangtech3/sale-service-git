import 'dart:async';

import 'package:domain/domain.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

import 'package:base_architecture_flutter/src/app.dart';

@Injectable()
class InterbankBloc extends BaseBloc<InterbankEvent, InterbankState> {
  InterbankBloc(this._interbankUseCase) : super(const InterbankState()) {
    on<InterbankTextFieldChanged>(
      _onInterbankTextFieldChanged,
      transformer: distinct(),
    );
  }

  final InterbankUseCase _interbankUseCase;

  void _onInterbankTextFieldChanged(InterbankTextFieldChanged event, Emitter<InterbankState> emit) {
    emit(state.copyWith(
      interbankRate: event.interbankRate,
    ));
  }
}
