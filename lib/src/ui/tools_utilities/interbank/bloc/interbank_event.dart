import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:base_architecture_flutter/src/base/bloc/base_bloc_event.dart';
part 'interbank_event.freezed.dart';

abstract class InterbankEvent extends BaseBlocEvent {
  const InterbankEvent();
}

@freezed
class InterbankTextFieldChanged extends InterbankEvent with _$InterbankTextFieldChanged {
  const factory InterbankTextFieldChanged({
    required String interbankRate,
  }) = _InterbankTextFieldChanged;
}
