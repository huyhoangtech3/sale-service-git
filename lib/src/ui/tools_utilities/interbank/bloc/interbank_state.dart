import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:base_architecture_flutter/src/base/bloc/base_bloc_state.dart';
part 'interbank_state.freezed.dart';

@freezed
class InterbankState extends BaseBlocState with _$InterbankState {
  const factory InterbankState({
    @Default('') String interbankRate,
  }) = _InterbankState;
}
