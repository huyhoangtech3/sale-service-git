// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'login_event.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$EmailTextFieldChanged {
  String get email => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $EmailTextFieldChangedCopyWith<EmailTextFieldChanged> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $EmailTextFieldChangedCopyWith<$Res> {
  factory $EmailTextFieldChangedCopyWith(EmailTextFieldChanged value,
          $Res Function(EmailTextFieldChanged) then) =
      _$EmailTextFieldChangedCopyWithImpl<$Res, EmailTextFieldChanged>;
  @useResult
  $Res call({String email});
}

/// @nodoc
class _$EmailTextFieldChangedCopyWithImpl<$Res,
        $Val extends EmailTextFieldChanged>
    implements $EmailTextFieldChangedCopyWith<$Res> {
  _$EmailTextFieldChangedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? email = null,
  }) {
    return _then(_value.copyWith(
      email: null == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$EmailTextFieldChangedImplCopyWith<$Res>
    implements $EmailTextFieldChangedCopyWith<$Res> {
  factory _$$EmailTextFieldChangedImplCopyWith(
          _$EmailTextFieldChangedImpl value,
          $Res Function(_$EmailTextFieldChangedImpl) then) =
      __$$EmailTextFieldChangedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String email});
}

/// @nodoc
class __$$EmailTextFieldChangedImplCopyWithImpl<$Res>
    extends _$EmailTextFieldChangedCopyWithImpl<$Res,
        _$EmailTextFieldChangedImpl>
    implements _$$EmailTextFieldChangedImplCopyWith<$Res> {
  __$$EmailTextFieldChangedImplCopyWithImpl(_$EmailTextFieldChangedImpl _value,
      $Res Function(_$EmailTextFieldChangedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? email = null,
  }) {
    return _then(_$EmailTextFieldChangedImpl(
      email: null == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$EmailTextFieldChangedImpl implements _EmailTextFieldChanged {
  const _$EmailTextFieldChangedImpl({required this.email});

  @override
  final String email;

  @override
  String toString() {
    return 'EmailTextFieldChanged(email: $email)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$EmailTextFieldChangedImpl &&
            (identical(other.email, email) || other.email == email));
  }

  @override
  int get hashCode => Object.hash(runtimeType, email);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$EmailTextFieldChangedImplCopyWith<_$EmailTextFieldChangedImpl>
      get copyWith => __$$EmailTextFieldChangedImplCopyWithImpl<
          _$EmailTextFieldChangedImpl>(this, _$identity);
}

abstract class _EmailTextFieldChanged implements EmailTextFieldChanged {
  const factory _EmailTextFieldChanged({required final String email}) =
      _$EmailTextFieldChangedImpl;

  @override
  String get email;
  @override
  @JsonKey(ignore: true)
  _$$EmailTextFieldChangedImplCopyWith<_$EmailTextFieldChangedImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$PasswordTextFieldChanged {
  String get password => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $PasswordTextFieldChangedCopyWith<PasswordTextFieldChanged> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PasswordTextFieldChangedCopyWith<$Res> {
  factory $PasswordTextFieldChangedCopyWith(PasswordTextFieldChanged value,
          $Res Function(PasswordTextFieldChanged) then) =
      _$PasswordTextFieldChangedCopyWithImpl<$Res, PasswordTextFieldChanged>;
  @useResult
  $Res call({String password});
}

/// @nodoc
class _$PasswordTextFieldChangedCopyWithImpl<$Res,
        $Val extends PasswordTextFieldChanged>
    implements $PasswordTextFieldChangedCopyWith<$Res> {
  _$PasswordTextFieldChangedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? password = null,
  }) {
    return _then(_value.copyWith(
      password: null == password
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$PasswordTextFieldChangedImplCopyWith<$Res>
    implements $PasswordTextFieldChangedCopyWith<$Res> {
  factory _$$PasswordTextFieldChangedImplCopyWith(
          _$PasswordTextFieldChangedImpl value,
          $Res Function(_$PasswordTextFieldChangedImpl) then) =
      __$$PasswordTextFieldChangedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String password});
}

/// @nodoc
class __$$PasswordTextFieldChangedImplCopyWithImpl<$Res>
    extends _$PasswordTextFieldChangedCopyWithImpl<$Res,
        _$PasswordTextFieldChangedImpl>
    implements _$$PasswordTextFieldChangedImplCopyWith<$Res> {
  __$$PasswordTextFieldChangedImplCopyWithImpl(
      _$PasswordTextFieldChangedImpl _value,
      $Res Function(_$PasswordTextFieldChangedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? password = null,
  }) {
    return _then(_$PasswordTextFieldChangedImpl(
      password: null == password
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$PasswordTextFieldChangedImpl implements _PasswordTextFieldChanged {
  const _$PasswordTextFieldChangedImpl({required this.password});

  @override
  final String password;

  @override
  String toString() {
    return 'PasswordTextFieldChanged(password: $password)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$PasswordTextFieldChangedImpl &&
            (identical(other.password, password) ||
                other.password == password));
  }

  @override
  int get hashCode => Object.hash(runtimeType, password);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$PasswordTextFieldChangedImplCopyWith<_$PasswordTextFieldChangedImpl>
      get copyWith => __$$PasswordTextFieldChangedImplCopyWithImpl<
          _$PasswordTextFieldChangedImpl>(this, _$identity);
}

abstract class _PasswordTextFieldChanged implements PasswordTextFieldChanged {
  const factory _PasswordTextFieldChanged({required final String password}) =
      _$PasswordTextFieldChangedImpl;

  @override
  String get password;
  @override
  @JsonKey(ignore: true)
  _$$PasswordTextFieldChangedImplCopyWith<_$PasswordTextFieldChangedImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$EyeIconPressed {}

/// @nodoc
abstract class $EyeIconPressedCopyWith<$Res> {
  factory $EyeIconPressedCopyWith(
          EyeIconPressed value, $Res Function(EyeIconPressed) then) =
      _$EyeIconPressedCopyWithImpl<$Res, EyeIconPressed>;
}

/// @nodoc
class _$EyeIconPressedCopyWithImpl<$Res, $Val extends EyeIconPressed>
    implements $EyeIconPressedCopyWith<$Res> {
  _$EyeIconPressedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$EyeIconPressedImplCopyWith<$Res> {
  factory _$$EyeIconPressedImplCopyWith(_$EyeIconPressedImpl value,
          $Res Function(_$EyeIconPressedImpl) then) =
      __$$EyeIconPressedImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$EyeIconPressedImplCopyWithImpl<$Res>
    extends _$EyeIconPressedCopyWithImpl<$Res, _$EyeIconPressedImpl>
    implements _$$EyeIconPressedImplCopyWith<$Res> {
  __$$EyeIconPressedImplCopyWithImpl(
      _$EyeIconPressedImpl _value, $Res Function(_$EyeIconPressedImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$EyeIconPressedImpl implements _EyeIconPressed {
  const _$EyeIconPressedImpl();

  @override
  String toString() {
    return 'EyeIconPressed()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$EyeIconPressedImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;
}

abstract class _EyeIconPressed implements EyeIconPressed {
  const factory _EyeIconPressed() = _$EyeIconPressedImpl;
}

/// @nodoc
mixin _$LoginButtonPressed {}

/// @nodoc
abstract class $LoginButtonPressedCopyWith<$Res> {
  factory $LoginButtonPressedCopyWith(
          LoginButtonPressed value, $Res Function(LoginButtonPressed) then) =
      _$LoginButtonPressedCopyWithImpl<$Res, LoginButtonPressed>;
}

/// @nodoc
class _$LoginButtonPressedCopyWithImpl<$Res, $Val extends LoginButtonPressed>
    implements $LoginButtonPressedCopyWith<$Res> {
  _$LoginButtonPressedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$LoginButtonPressedImplCopyWith<$Res> {
  factory _$$LoginButtonPressedImplCopyWith(_$LoginButtonPressedImpl value,
          $Res Function(_$LoginButtonPressedImpl) then) =
      __$$LoginButtonPressedImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$LoginButtonPressedImplCopyWithImpl<$Res>
    extends _$LoginButtonPressedCopyWithImpl<$Res, _$LoginButtonPressedImpl>
    implements _$$LoginButtonPressedImplCopyWith<$Res> {
  __$$LoginButtonPressedImplCopyWithImpl(_$LoginButtonPressedImpl _value,
      $Res Function(_$LoginButtonPressedImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$LoginButtonPressedImpl implements _LoginButtonPressed {
  const _$LoginButtonPressedImpl();

  @override
  String toString() {
    return 'LoginButtonPressed()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$LoginButtonPressedImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;
}

abstract class _LoginButtonPressed implements LoginButtonPressed {
  const factory _LoginButtonPressed() = _$LoginButtonPressedImpl;
}
