import 'dart:async';

import 'package:domain/domain.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

import 'package:base_architecture_flutter/src/app.dart';
import 'login.dart';

@Injectable()
class LoginBloc extends BaseBloc<LoginEvent, LoginState> {
  LoginBloc(this._loginUseCase, this._userInfoUseCase, this._userInfoHrsCodeUseCase,
      this._blockUseCase, this._blockListUseCase, this._branchUseCase) : super(const LoginState()) {
    on<EmailTextFieldChanged>(
      _onEmailTextFieldChanged,
      transformer: distinct(),
    );

    on<PasswordTextFieldChanged>(
      _onPasswordTextFieldChanged,
      transformer: distinct(),
    );

    on<LoginButtonPressed>(
      _onLoginButtonPressed,
      transformer: log(),
    );

    on<EyeIconPressed>(
      _onEyeIconPressed,
      transformer: log(),
    );
  }

  final LoginUseCase _loginUseCase;
  final UserInfoUseCase _userInfoUseCase;
  final UserInfoHrsCodeUseCase _userInfoHrsCodeUseCase;
  final BlockUseCase _blockUseCase;
  final BlockListUseCase _blockListUseCase;
  final BranchUseCase _branchUseCase;

  bool _isLoginButtonEnabled(String email, String password) {
    return email.isNotEmpty && password.isNotEmpty;
  }

  void _onEmailTextFieldChanged(EmailTextFieldChanged event, Emitter<LoginState> emit) {
    emit(state.copyWith(
      username: event.email,
      isLoginButtonEnabled: _isLoginButtonEnabled(event.email, state.password),
      onPageError: '',
    ));
  }

  void _onPasswordTextFieldChanged(PasswordTextFieldChanged event, Emitter<LoginState> emit) {
    emit(state.copyWith(
      password: event.password,
      isLoginButtonEnabled: _isLoginButtonEnabled(state.username, event.password),
      onPageError: '',
    ));
  }

  FutureOr<void> _onLoginButtonPressed(LoginButtonPressed event, Emitter<LoginState> emit) {
    return runBlocCatching(
      action: () async {
        /// 1. Call API lấy token
        await _loginUseCase.execute(LoginInput(username: state.username, password: state.password));
        /// 2. Call API lấy User Info
        await _userInfoUseCase.execute(UserInfoInput(username: state.username, externalData: 1));
        /// 3. Call API lấy Thông tin chi tiết User theo hrsCode
        await _userInfoHrsCodeUseCase.execute(const UserInfoHrsCodeInput());
        /// 4. Call API tìm kiếm Block theo RM
        await _blockUseCase.execute(const BlockInput());
        // /// 5. Call API lấy danh sách Khối
        // await _blockListUseCase.execute(const BlockListInput(page: 0, size: 20));
        /// 5. Call API lấy Branch code
        await _branchUseCase.execute(const BranchInput());
        /// 0. login thành công, điều hướng sang màn hình chính
        await navigator.replace(const AppRouteInfo.main());
      },
      handleError: false,
      doOnError: (e) async {
        emit(state.copyWith(onPageError: exceptionMessageMapper.map(e)));
      },
    );
  }

  void _onEyeIconPressed(EyeIconPressed event, Emitter<LoginState> emit) {
    emit(state.copyWith(obscureText: !state.obscureText));
  }
}
