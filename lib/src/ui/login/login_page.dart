import 'package:auto_route/auto_route.dart';
import 'package:domain/domain.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:resources/resources.dart';
import 'package:flutter/foundation.dart';
import 'package:base_architecture_flutter/src/app.dart';
import 'package:shared/shared.dart';
import 'bloc/login.dart';
import 'package:flutter_nfc_kit/flutter_nfc_kit.dart';
import 'package:ndef/ndef.dart' as ndef;
import 'package:ndef/utilities.dart';
import 'dart:io' show Platform, sleep;
import 'package:flutter/services.dart';

@RoutePage()
class LoginPage extends StatefulWidget {
  const LoginPage({super.key});


  @override
  State<StatefulWidget> createState() {
    return _LoginPageState();
  }
}

class _LoginPageState extends BasePageState<LoginPage, LoginBloc> {
  FocusNode focusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    focusNode = FocusNode();
  }

  @override
  void dispose() {
    // Clean up the focus node when the Form is disposed.
    focusNode.dispose();
    super.dispose();
  }

  @override
  Widget buildPage(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: Assets.images.bgLoginPng.provider(),
              fit: BoxFit.cover,
            ),
          ),
        ),
        CommonScaffold(
          backgroundColor: Colors.transparent,
          hideKeyboardWhenTouchOutside: true,
          // appBar: CommonAppBar(
          //   leadingIcon: navigator.canPopSelfOrChildren ? LeadingIcon.close : LeadingIcon.none,
          //   leadingIconColor: AppColors.current.secondaryColor,
          //   titleType: AppBarTitle.text,
          //   centerTitle: true,
          //   text: S.current.crmNextGen,
          //   backgroundColor: AppColors.current.primaryColor,
          //   titleTextStyle: AppTextStyles.s14w400Primary(),
          //   bottom: TabBar(
          //     tabs: const <Widget>[
          //       Tab(text: 'Read'),
          //       Tab(text: 'Write'),
          //     ],
          //     controller: _tabController,
          //   )
          // ),
          body: SafeArea(
            child: SingleChildScrollView(
              padding: EdgeInsets.fromLTRB(
                Dimens.d80.responsive(),
                Dimens.d16.responsive(),
                Dimens.d16.responsive(),
                Dimens.d40.responsive(),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                    flex: 2,
                    child: Column(
                      children: [
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Assets.images.icLogoSvg.svg(
                              width: Dimens.d56.responsive(),
                              height: Dimens.d56.responsive()
                          ),
                        ),
                        SizedBox(height: Dimens.d24.responsive()),
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            S.current.welcomeLoginPage,
                            style: AppTextStyles.s14w400Primary(),
                          ),
                        ),
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            S.current.crmNextGen,
                            style: AppTextStyles.s20w700Secondary(),
                          ),
                        ),
                        SizedBox(height: Dimens.d24.responsive()),
                        BlocBuilder<AppBloc, AppState>(
                          builder: (context, state) {
                            bloc.add(EmailTextFieldChanged(email: state.userName));
                            return AppTextField(
                              headStyle: AppTextStyles.s12w200Secondary(),
                              controller: TextEditingController(text: state.userName),
                              title: S.current.userName,
                              hintText: S.current.hintUserName,
                              onChanged: (email) {
                                bloc.add(EmailTextFieldChanged(email: email));
                              },
                              keyboardType: TextInputType.emailAddress,
                              suffixIcon: IconButton(
                                icon: const Icon(Icons.mail, color: Colors.grey),
                                onPressed: () {  },
                              ),
                              textInputAction: TextInputAction.next,
                              onSubmitted: (_){
                                focusNode.requestFocus();
                              },
                            );
                          },
                        ),
                        SizedBox(height: Dimens.d24.responsive()),
                        BlocBuilder<LoginBloc, LoginState>(
                          builder: (context, state) {
                            return AppTextField(
                                headStyle: AppTextStyles.s12w200Secondary(),
                                textCapitalization: TextCapitalization.words,
                                focusNode: focusNode,
                                title: S.current.password,
                                hintText: S.current.hintPassword,
                                onChanged: (pass) => bloc.add(PasswordTextFieldChanged(password: pass)),
                                keyboardType: TextInputType.visiblePassword,
                                suffixIcon: IconButton(
                                  icon: const Icon(Icons.lock, color: Colors.grey),
                                  onPressed: () {  },
                                ),
                                textInputAction: TextInputAction.done,
                                onSubmitted: (_){
                                  bloc.add(const LoginButtonPressed());
                                  appBloc.add(UserNameChanged(userName: state.username));
                                }
                            );
                          },
                        ),
                        BlocBuilder<LoginBloc, LoginState>(
                          buildWhen: (previous, current) => previous.onPageError != current.onPageError,
                          builder: (_, state) => Text(
                            state.onPageError,
                            style: AppTextStyles.s14w400Secondary(),
                          ),
                        ),
                        BlocBuilder<LoginBloc, LoginState>(
                          buildWhen: (previous, current) =>
                          previous.isLoginButtonEnabled != current.isLoginButtonEnabled,
                          builder: (context, state) {
                            return ElevatedButton(
                              onPressed: () {
                                if (state.isLoginButtonEnabled) {
                                  bloc.add(const LoginButtonPressed());
                                  appBloc.add(UserNameChanged(userName: state.username));
                                }
                              },
                              style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all(AppColors.current.primaryColor
                                    .withOpacity(state.isLoginButtonEnabled ? 1 : 0.5)),
                                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(6.0),
                                  ),
                                ),
                                padding: MaterialStateProperty.all<EdgeInsets>(
                                    EdgeInsets.fromLTRB(
                                      Dimens.d78.responsive(),
                                      Dimens.d4.responsive(),
                                      Dimens.d78.responsive(),
                                      Dimens.d4.responsive(),
                                    ),
                                ),
                              ), 
                              child: Text(
                                S.current.login,
                                style: AppTextStyles.s14w400Primary(),
                              ),
                            );
                          },
                        ),
                        SizedBox(height: Dimens.d8.responsive()),
                        SizedBox(
                          height: Dimens.d16.responsive(),
                          child: Row(
                            children: [
                              Expanded(
                                flex: 1,
                                child: Column(
                                  children: [
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: InkWell(
                                        onTap: (){

                                        },
                                        child: Text(
                                          S.current.support,
                                          style: AppTextStyles.s10w100Secondary(),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: Column(
                                  children: [
                                    Align(
                                      alignment: Alignment.centerRight,
                                      child: InkWell(
                                        onTap: (){

                                        },
                                        child: Text(
                                          S.current.guideLine,
                                          style: AppTextStyles.s10w100Secondary(),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: Dimens.d16.responsive()),
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            S.current.version,
                            style: AppTextStyles.s10w100Primary(),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 7,
                    child: Stack(
                      children: [
                        Container(
                          alignment: Alignment.center,
                          child: Assets.images.overlay.svg(
                            // colorFilter: AppColors.current.primaryColor.let((it) => ColorFilter.mode(it, BlendMode.srcIn)),
                              width: Dimens.d520.responsive(),
                              height: Dimens.d520.responsive()),
                        ),
                        Column(
                          children: [
                            Align(
                              alignment: Alignment.topRight,
                              child: Assets.images.crmNextgen.svg(
                                // colorFilter: AppColors.current.primaryColor.let((it) => ColorFilter.mode(it, BlendMode.srcIn)),
                                  width: Dimens.d40.responsive(),
                                  height: Dimens.d40.responsive()),
                            ),
                          ]),
                      ]),
                  ),
                ]),
            ),
          ),
        ),
      ]);
  }
}
