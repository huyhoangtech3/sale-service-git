import 'package:auto_route/auto_route.dart';
import 'package:dartx/dartx.dart';
import 'package:domain/domain.dart';
import 'package:flutter/material.dart';
import 'package:resources/resources.dart';
import 'package:base_architecture_flutter/src/app.dart';
import 'package:shared/shared.dart';
import 'package:tab_indicator_styler/tab_indicator_styler.dart';

@RoutePage()
class WarningPage extends StatefulWidget {
  const WarningPage({super.key});

  @override
  State<StatefulWidget> createState() {
    return _WarningPageState();
  }
}

class _WarningPageState extends BasePageState<WarningPage, WarningBloc> with TickerProviderStateMixin{
  bool? isChecked = true;
  static List<String> listPriority = <String>[S.current.all, S.current.high, S.current.medium, S.current.low];
  late final TabController _tabController;
  int _activeIndex = 0;
  List<Warning> warning = <Warning>[];
  List<WarningBScore> warningBScore = <WarningBScore>[];
  late WarningDataSource warningAllDataSource;
  late WarningDataSource warningCreditDataSource;
  late WarningDataSource warningSavingDataSource;
  late WarningBScoreDataSource warningBScoreDataSource;
  late WarningBScoreDataSource warningHighPriorityDataSource;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 5, vsync: this);
    _tabController.addListener(() {
      setState(() {
        _activeIndex = _tabController.index;
      });
    });
    warning = getWarningData();
    warningAllDataSource = WarningDataSource(warningData: warning);

    warningCreditDataSource = WarningDataSource(warningData: warning);
    warningCreditDataSource.addColumnGroup(ColumnGroup(name: '1', sortGroupRows: false));

    warningSavingDataSource = WarningDataSource(warningData: warning);
    warningSavingDataSource.addColumnGroup(ColumnGroup(name: '1', sortGroupRows: false));
    warningSavingDataSource.addColumnGroup(ColumnGroup(name: '2', sortGroupRows: false));

    warningBScore = getWarningBScoreData();
    warningBScoreDataSource = WarningBScoreDataSource(warningData: warningBScore);
    warningHighPriorityDataSource = WarningBScoreDataSource(warningData: warningBScore);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget buildPage(BuildContext context) {
    return CommonScaffold(
      body: SafeArea(
        child: Container(
          color: Colors.blueGrey.shade50,
          padding: EdgeInsets.all(Dimens.d8.responsive()),
          child: PreferredSize(
            preferredSize: const Size(double.infinity, 50),
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.all(Radius.circular(Dimens.d6.responsive())),
              ),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(width: Dimens.d10.responsive()),
                          Text(S.current.warning, style: AppTextStyles.s12w500Tertiary()),
                          SizedBox(width: Dimens.d10.responsive()),
                          SizedBox(
                            width: Dimens.d500.responsive(),
                            child: TabBar(
                              controller: _tabController,
                              overlayColor: MaterialStateProperty.resolveWith<Color?>(
                                    (Set<MaterialState> states) {
                                  if (states.contains(MaterialState.pressed)) {
                                    return Colors.orangeAccent.withOpacity(0.5);
                                  }
                                  if (states.contains(MaterialState.hovered)) {
                                    return Colors.orangeAccent.withOpacity(0.5);
                                  }
                                  if (states.contains(MaterialState.focused)) {
                                    return Colors.orangeAccent.withOpacity(0.5);
                                  }
                                  return Colors.white.withOpacity(0.5);
                                },
                              ),
                              dividerColor: Colors.orangeAccent,
                              labelColor: Colors.white,
                              tabs: [
                                Tab(
                                  child: Text(
                                    S.current.all,
                                    style: _activeIndex == 0 ? AppTextStyles.s12w500Secondary() : AppTextStyles.s12w500Tertiary(),
                                  ),
                                ),
                                Tab(
                                  child: Text(
                                    S.current.credit,
                                    style: _activeIndex == 1 ? AppTextStyles.s12w500Secondary() : AppTextStyles.s12w500Tertiary(),
                                  ),
                                ),
                                Tab(
                                  child: Text(
                                    S.current.savings,
                                    style: _activeIndex == 2 ? AppTextStyles.s12w500Secondary() : AppTextStyles.s12w500Tertiary(),
                                  ),
                                ),
                                Tab(
                                  child: Text(
                                    S.current.bscore,
                                    style: _activeIndex == 3 ? AppTextStyles.s12w500Secondary() : AppTextStyles.s12w500Tertiary(),
                                  ),
                                ),
                                Tab(
                                  child: Text(
                                    S.current.highPriority,
                                    style: _activeIndex == 4 ? AppTextStyles.s12w500Secondary() : AppTextStyles.s12w500Tertiary(),
                                  ),
                                ),
                              ],
                              indicator: MaterialIndicator(
                                  height: 4,
                                  color: Colors.orangeAccent,
                                  bottomLeftRadius: 10,
                                  bottomRightRadius: 10,
                                  topLeftRadius: 10,
                                  topRightRadius: 10,
                                  tabPosition: TabPosition.bottom
                              ),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Checkbox(
                            tristate: false,
                            value: isChecked,
                            onChanged: (bool? value) {
                              setState(() {
                                isChecked = value;
                              });
                            },
                          ),
                          Text(S.current.customerGroup, style: AppTextStyles.s12w400Tertiary()),
                          SizedBox(width: Dimens.d40.responsive()),
                          CommonDropdownMenu(
                            list: listPriority,
                            width: Dimens.d100.responsive(),
                            // controller: ctrViewStyle,
                            onSelected: (value) {
                              // bloc.add(DateUnitTextFieldChanged(dateUnit: value!));
                            },
                          ),
                          SizedBox(width: Dimens.d10.responsive()),
                        ],
                      ),
                    ],
                  ),
                  Expanded(
                    child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.all(Radius.circular(Dimens.d6.responsive())),
                        ),
                        margin: EdgeInsets.fromLTRB(Dimens.d10.responsive(), Dimens.d0.responsive(), Dimens.d10.responsive(), Dimens.d10.responsive()),
                        padding: EdgeInsets.all(Dimens.d0.responsive()),
                        child: TabBarView(
                          controller: _tabController,
                          children: [
                            SfDataGridTheme(
                              data: SfDataGridThemeData(
                                headerColor: Colors.orange.shade50,
                                indentColumnWidth: Dimens.d24.responsive(),
                                indentColumnColor: Colors.white,
                              ),
                              child: SfDataGrid(
                                horizontalScrollPhysics: const NeverScrollableScrollPhysics(),
                                groupCaptionTitleFormat: '{Key}',
                                allowExpandCollapseGroup: true,
                                allowSwiping: false,
                                swipeMaxOffset: 100.0,
                                selectionMode: SelectionMode.multiple,
                                source: warningAllDataSource,
                                columnWidthMode: ColumnWidthMode.fill,
                                allowPullToRefresh: true,
                                loadMoreViewBuilder: (BuildContext context, LoadMoreRows loadMoreRows) {
                                  Future<String> loadRows() async {
                                    await loadMoreRows();
                                    return Future<String>.value('Completed');
                                  }
                                  return FutureBuilder<String>(
                                      initialData: 'loading',
                                      future: loadRows(),
                                      builder: (context, snapShot) {
                                        if (snapShot.data == 'loading') {
                                          return Container(
                                              height: 60.0,
                                              width: double.infinity,
                                              decoration: BoxDecoration(
                                                  color: Colors.white,
                                                  border: BorderDirectional(
                                                      top: BorderSide(
                                                          width: 1.0,
                                                          color: Color.fromRGBO(0, 0, 0, 0.26)))),
                                              alignment: Alignment.center,
                                              child: CircularProgressIndicator(
                                                  valueColor: AlwaysStoppedAnimation(Colors.deepPurple)));
                                        } else {
                                          return SizedBox.fromSize(size: Size.zero);
                                        }
                                      });
                                },
                                startSwipeActionsBuilder: (BuildContext context, DataGridRow row, int rowIndex) {
                                  return GestureDetector(
                                      onTap: () {
                                        warningAllDataSource.rows.insert(rowIndex, DataGridRow(cells: [
                                          DataGridCell<Warning>(
                                            columnName: '2',
                                            value: Warning('605dc0ba13076e2e04b12642', 'Today', '4665540', 'CHIC-LAND JSC', 'LD1931707564', 'Cho vay dau tu du an thong thuong', '64000000', '10388553', 'WARNING_CREDIT', '20/05/2020', 'INDIV', 'VN0010005', 'B306', '123@gmail.com', '0974477522', '1111', 'MALE'),
                                          ),
                                        ]));
                                        warningAllDataSource.updateDataGridSource();
                                      },
                                      child: Container(
                                          color: Colors.greenAccent,
                                          child: Center(
                                            child: Icon(Icons.add),
                                          )));
                                },
                                endSwipeActionsBuilder: (BuildContext context, DataGridRow row, int rowIndex) {
                                  return GestureDetector(
                                      onTap: () {
                                        warningAllDataSource.rows.removeAt(rowIndex);
                                        warningAllDataSource.updateDataGridSource();
                                      },
                                      child: Container(
                                          color: Colors.redAccent,
                                          child: Center(
                                            child: Icon(Icons.delete),
                                          )));
                                },
                                columns: warningAllDataSource.columns,
                              ),
                            ),
                            SfDataGridTheme(
                              data: SfDataGridThemeData(
                                headerColor: Colors.orange.shade50,
                                indentColumnWidth: Dimens.d0.responsive(),
                                indentColumnColor: Colors.white,
                              ),
                              child: SfDataGrid(
                                horizontalScrollPhysics: const NeverScrollableScrollPhysics(),
                                groupCaptionTitleFormat: '{Key}',
                                allowExpandCollapseGroup: true,
                                allowSwiping: false,
                                swipeMaxOffset: 100.0,
                                selectionMode: SelectionMode.multiple,
                                source: warningCreditDataSource,
                                columnWidthMode: ColumnWidthMode.fill,
                                allowPullToRefresh: true,
                                loadMoreViewBuilder: (BuildContext context, LoadMoreRows loadMoreRows) {
                                  Future<String> loadRows() async {
                                    await loadMoreRows();
                                    return Future<String>.value('Completed');
                                  }
                                  return FutureBuilder<String>(
                                      initialData: 'loading',
                                      future: loadRows(),
                                      builder: (context, snapShot) {
                                        if (snapShot.data == 'loading') {
                                          return Container(
                                              height: 60.0,
                                              width: double.infinity,
                                              decoration: BoxDecoration(
                                                  color: Colors.white,
                                                  border: BorderDirectional(
                                                      top: BorderSide(
                                                          width: 1.0,
                                                          color: Color.fromRGBO(0, 0, 0, 0.26)))),
                                              alignment: Alignment.center,
                                              child: CircularProgressIndicator(
                                                  valueColor: AlwaysStoppedAnimation(Colors.deepPurple)));
                                        } else {
                                          return SizedBox.fromSize(size: Size.zero);
                                        }
                                      });
                                },
                                startSwipeActionsBuilder: (BuildContext context, DataGridRow row, int rowIndex) {
                                  return GestureDetector(
                                      onTap: () {
                                        warningCreditDataSource.rows.insert(rowIndex, DataGridRow(cells: [
                                          DataGridCell<Warning>(
                                            columnName: '2',
                                            value: Warning('605dc0ba13076e2e04b12642', 'Today', '4665540', 'CHIC-LAND JSC', 'LD1931707564', 'Cho vay dau tu du an thong thuong', '64000000', '10388553', 'WARNING_CREDIT', '20/05/2020', 'INDIV', 'VN0010005', 'B306', '123@gmail.com', '0974477522', '1111', 'MALE'),
                                          ),
                                        ]));
                                        warningCreditDataSource.updateDataGridSource();
                                      },
                                      child: Container(
                                          color: Colors.greenAccent,
                                          child: Center(
                                            child: Icon(Icons.add),
                                          )));
                                },
                                endSwipeActionsBuilder: (BuildContext context, DataGridRow row, int rowIndex) {
                                  return GestureDetector(
                                      onTap: () {
                                        warningCreditDataSource.rows.removeAt(rowIndex);
                                        warningCreditDataSource.updateDataGridSource();
                                      },
                                      child: Container(
                                          color: Colors.redAccent,
                                          child: Center(
                                            child: Icon(Icons.delete),
                                          )));
                                },
                                columns: warningCreditDataSource.columns,
                              ),
                            ),
                            SfDataGridTheme(
                              data: SfDataGridThemeData(
                                headerColor: Colors.orange.shade50,
                                indentColumnWidth: Dimens.d24.responsive(),
                                indentColumnColor: Colors.white,
                              ),
                              child: SfDataGrid(
                                horizontalScrollPhysics: const NeverScrollableScrollPhysics(),
                                groupCaptionTitleFormat: '{Key}',
                                allowExpandCollapseGroup: true,
                                allowSwiping: false,
                                swipeMaxOffset: 100.0,
                                selectionMode: SelectionMode.multiple,
                                source: warningSavingDataSource,
                                columnWidthMode: ColumnWidthMode.fill,
                                allowPullToRefresh: true,
                                loadMoreViewBuilder: (BuildContext context, LoadMoreRows loadMoreRows) {
                                  Future<String> loadRows() async {
                                    await loadMoreRows();
                                    return Future<String>.value('Completed');
                                  }
                                  return FutureBuilder<String>(
                                      initialData: 'loading',
                                      future: loadRows(),
                                      builder: (context, snapShot) {
                                        if (snapShot.data == 'loading') {
                                          return Container(
                                              height: 60.0,
                                              width: double.infinity,
                                              decoration: BoxDecoration(
                                                  color: Colors.white,
                                                  border: BorderDirectional(
                                                      top: BorderSide(
                                                          width: 1.0,
                                                          color: Color.fromRGBO(0, 0, 0, 0.26)))),
                                              alignment: Alignment.center,
                                              child: CircularProgressIndicator(
                                                  valueColor: AlwaysStoppedAnimation(Colors.deepPurple)));
                                        } else {
                                          return SizedBox.fromSize(size: Size.zero);
                                        }
                                      });
                                },
                                startSwipeActionsBuilder: (BuildContext context, DataGridRow row, int rowIndex) {
                                  return GestureDetector(
                                      onTap: () {
                                        warningSavingDataSource.rows.insert(rowIndex, DataGridRow(cells: [
                                          DataGridCell<Warning>(
                                            columnName: '2',
                                            value: Warning('605dc0ba13076e2e04b12642', 'Today', '4665540', 'CHIC-LAND JSC', 'LD1931707564', 'Cho vay dau tu du an thong thuong', '64000000', '10388553', 'WARNING_CREDIT', '20/05/2020', 'INDIV', 'VN0010005', 'B306', '123@gmail.com', '0974477522', '1111', 'MALE'),
                                          ),
                                        ]));
                                        warningSavingDataSource.updateDataGridSource();
                                      },
                                      child: Container(
                                          color: Colors.greenAccent,
                                          child: Center(
                                            child: Icon(Icons.add),
                                          )));
                                },
                                endSwipeActionsBuilder: (BuildContext context, DataGridRow row, int rowIndex) {
                                  return GestureDetector(
                                      onTap: () {
                                        warningSavingDataSource.rows.removeAt(rowIndex);
                                        warningSavingDataSource.updateDataGridSource();
                                      },
                                      child: Container(
                                          color: Colors.redAccent,
                                          child: Center(
                                            child: Icon(Icons.delete),
                                          )));
                                },
                                columns: warningSavingDataSource.columns,
                              ),
                            ),
                            SfDataGridTheme(
                              data: SfDataGridThemeData(
                                headerColor: Colors.orange.shade50,
                              ),
                              child: SfDataGrid(
                                horizontalScrollPhysics: const NeverScrollableScrollPhysics(),
                                selectionMode: SelectionMode.multiple,
                                source: warningBScoreDataSource,
                                columnWidthMode: ColumnWidthMode.fill,
                                allowPullToRefresh: true,
                                loadMoreViewBuilder: (BuildContext context, LoadMoreRows loadMoreRows) {
                                  Future<String> loadRows() async {
                                    await loadMoreRows();
                                    return Future<String>.value('Completed');
                                  }
                                  return FutureBuilder<String>(
                                      initialData: 'loading',
                                      future: loadRows(),
                                      builder: (context, snapShot) {
                                        if (snapShot.data == 'loading') {
                                          return Container(
                                              height: 60.0,
                                              width: double.infinity,
                                              decoration: BoxDecoration(
                                                  color: Colors.white,
                                                  border: BorderDirectional(
                                                      top: BorderSide(
                                                          width: 1.0,
                                                          color: Color.fromRGBO(0, 0, 0, 0.26)))),
                                              alignment: Alignment.center,
                                              child: CircularProgressIndicator(
                                                  valueColor: AlwaysStoppedAnimation(Colors.deepPurple)));
                                        } else {
                                          return SizedBox.fromSize(size: Size.zero);
                                        }
                                      });
                                },
                                columns: warningBScoreDataSource.columns,
                              ),
                            ),
                            SfDataGridTheme(
                              data: SfDataGridThemeData(
                                headerColor: Colors.orange.shade50,
                                frozenPaneElevation: 7.0,
                              ),
                              child: SfDataGrid(
                                horizontalScrollPhysics: const NeverScrollableScrollPhysics(),
                                selectionMode: SelectionMode.multiple,
                                source: warningHighPriorityDataSource,
                                columnWidthMode: ColumnWidthMode.fill,
                                allowPullToRefresh: true,
                                loadMoreViewBuilder: (BuildContext context, LoadMoreRows loadMoreRows) {
                                  Future<String> loadRows() async {
                                    await loadMoreRows();
                                    return Future<String>.value('Completed');
                                  }
                                  return FutureBuilder<String>(
                                      initialData: 'loading',
                                      future: loadRows(),
                                      builder: (context, snapShot) {
                                        if (snapShot.data == 'loading') {
                                          return Container(
                                              height: 60.0,
                                              width: double.infinity,
                                              decoration: BoxDecoration(
                                                  color: Colors.white,
                                                  border: BorderDirectional(
                                                      top: BorderSide(
                                                          width: 1.0,
                                                          color: Color.fromRGBO(0, 0, 0, 0.26)))),
                                              alignment: Alignment.center,
                                              child: CircularProgressIndicator(
                                                  valueColor: AlwaysStoppedAnimation(Colors.deepPurple)));
                                        } else {
                                          return SizedBox.fromSize(size: Size.zero);
                                        }
                                      });
                                },
                                columns: warningHighPriorityDataSource.columns,
                                footerFrozenRowsCount: 1,
                                frozenColumnsCount: 1,
                                footer: Container(
                                    color: Colors.orange.shade50,
                                    child: Center(
                                        child: Text(
                                          '* Xem chi tiết hướng dẫn cách xử lý đối với các KH bị điểm BScore D',
                                          style: AppTextStyles.s12w500Tertiary(),
                                        ))),
                              ),
                            ),
                          ],
                        ),
                      ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  List<Warning> getWarningData() {
    return [
      Warning('605dc0ba13076e2e04b12642', 'Today', '4665540', 'Nguyễn Hoàng Duy', 'LD1931707564', 'Cho vay dau tu du an thong thuong', '64000000', '10388553', 'WARNING_CREDIT', '20/05/2020', 'INDIV', 'VN0010005', 'B306', '123@gmail.com', '0974477522', '1111', 'MALE'),
      Warning('605dc0ba13076e2e04b12645', 'Tomorrow', '321123', 'Hương', 'HD20210326', 'Cho vay dau tu du an thong thuong', '100000', '12988', 'WARNING_SAVING', '26/03/2021', 'INDIV', 'VN0010005', 'B306', '123@gmail.com', '0974477522', '1111', 'FEMALE'),
      Warning('60617c23ec608726205ce5d0', 'This Week', '1069768', 'Nguyễn Đắc Nhân Tâm', 'HD1', 'Hợp đồng sinh trắc vân tay trọn gói test theo KH DXSKL OMR PMXSKL.', '64000000', '10388553', 'WARNING_SAVING', '01/03/2021', 'INDIV', 'VN0010005', 'B306', '123@gmail.com', '0974477522', '1111', 'MALE'),
      Warning('60617dc0ec608726205ce5d1', '09/2024', '1030951', 'Nguyễn Huy Hoàng', 'HD2', 'Hợp đồng cùng mã khách hàng', '64000000', '10388553', 'WARNING_CREDIT', '31/03/2021', 'INDIV', 'VN0010005', 'B306', '123@gmail.com', '0974477522', '1111', 'FEMALE'),
      Warning('605dc0ba13076e2e04b12642', 'Today', '4665540', 'Nguyễn Hoàng Duy', 'LD1931707564', 'Cho vay dau tu du an thong thuong', '64000000', '10388553', 'WARNING_CREDIT', '20/05/2020', 'INDIV', 'VN0010005', 'B306', '123@gmail.com', '0974477522', '1111', 'MALE'),
      Warning('605dc0ba13076e2e04b12645', 'Tomorrow', '321123', 'Hương', 'HD20210326', 'Cho vay dau tu du an thong thuong', '100000', '12988', 'WARNING_SAVING', '26/03/2021', 'INDIV', 'VN0010005', 'B306', '123@gmail.com', '0974477522', '1111', 'FEMALE'),
      Warning('60617c23ec608726205ce5d0', 'This Week', '1069768', 'Nguyễn Đắc Nhân Tâm', 'HD1', 'Hợp đồng sinh trắc vân tay trọn gói test theo KH DXSKL OMR PMXSKL.', '64000000', '10388553', 'WARNING_SAVING', '01/03/2021', 'INDIV', 'VN0010005', 'B306', '123@gmail.com', '0974477522', '1111', 'MALE'),
      Warning('60617dc0ec608726205ce5d1', '09/2024', '1030951', 'Nguyễn Huy Hoàng', 'HD2', 'Hợp đồng cùng mã khách hàng', '64000000', '10388553', 'WARNING_CREDIT', '31/03/2021', 'INDIV', 'VN0010005', 'B306', '123@gmail.com', '0974477522', '1111', 'FEMALE'),
    ];
  }

  List<WarningBScore> getWarningBScoreData() {
    return [
      WarningBScore('1', 'Today', '4665540', 'Nguyễn Hoàng Duy', 'LD1931707564', 'Cho vay dau tu du an thong thuong', '64000000', '10388553', 'WARNING_CREDIT', '20/05/2020', 'INDIV', 'VN0010005', 'B306', 'A', 'B', 'C', 'MALE'),
      WarningBScore('2', 'Tomorrow', '321123', 'Hương', 'HD20210326', 'Cho vay dau tu du an thong thuong', '100000', '12988', 'WARNING_SAVING', '26/03/2021', 'INDIV', 'VN0010005', 'B306', 'C', 'B', 'A', 'FEMALE'),
      WarningBScore('3', 'This Week', '1069768', 'Nguyễn Đắc Nhân Tâm', 'HD1', 'Hợp đồng sinh trắc vân tay trọn gói test theo KH DXSKL OMR PMXSKL.', '64000000', '10388553', 'WARNING_SAVING', '01/03/2021', 'INDIV', 'VN0010005', 'B306', 'B', 'B', 'A', 'MALE'),
      WarningBScore('4', '09/2024', '1030951', 'Nguyễn Huy Hoàng', 'HD2', 'Hợp đồng cùng mã khách hàng', '64000000', '10388553', 'WARNING_CREDIT', '31/03/2021', 'INDIV', 'VN0010005', 'B306', 'A', 'C', 'C', 'FEMALE'),
      WarningBScore('5', 'Today', '4665540', 'Nguyễn Hoàng Duy', 'LD1931707564', 'Cho vay dau tu du an thong thuong', '64000000', '10388553', 'WARNING_CREDIT', '20/05/2020', 'INDIV', 'VN0010005', 'B306', 'C', 'A', 'B', 'MALE'),
      WarningBScore('6', 'Tomorrow', '321123', 'Hương', 'HD20210326', 'Cho vay dau tu du an thong thuong', '100000', '12988', 'WARNING_SAVING', '26/03/2021', 'INDIV', 'VN0010005', 'B306', 'A', 'A', 'A', 'FEMALE'),
      WarningBScore('7', 'This Week', '1069768', 'Nguyễn Đắc Nhân Tâm', 'HD1', 'Hợp đồng sinh trắc vân tay trọn gói test theo KH DXSKL OMR PMXSKL.', '64000000', '10388553', 'WARNING_SAVING', '01/03/2021', 'INDIV', 'VN0010005', 'B306', 'B', 'B', 'B', 'MALE'),
      WarningBScore('8', '09/2024', '1030951', 'Nguyễn Huy Hoàng', 'HD2', 'Hợp đồng cùng mã khách hàng', '64000000', '10388553', 'WARNING_CREDIT', '31/03/2021', 'INDIV', 'VN0010005', 'B306', 'C', 'C', 'C', 'FEMALE'),
      WarningBScore('9', 'Today', '4665540', 'Nguyễn Hoàng Duy', 'LD1931707564', 'Cho vay dau tu du an thong thuong', '64000000', '10388553', 'WARNING_CREDIT', '20/05/2020', 'INDIV', 'VN0010005', 'B306', 'A', 'B', 'C', 'MALE'),
      WarningBScore('10', 'Tomorrow', '321123', 'Hương', 'HD20210326', 'Cho vay dau tu du an thong thuong', '100000', '12988', 'WARNING_SAVING', '26/03/2021', 'INDIV', 'VN0010005', 'B306', 'C', 'B', 'A', 'FEMALE'),
      WarningBScore('11', 'This Week', '1069768', 'Nguyễn Đắc Nhân Tâm', 'HD1', 'Hợp đồng sinh trắc vân tay trọn gói test theo KH DXSKL OMR PMXSKL.', '64000000', '10388553', 'WARNING_SAVING', '01/03/2021', 'INDIV', 'VN0010005', 'B306', 'B', 'B', 'A', 'MALE'),
      WarningBScore('12', '09/2024', '1030951', 'Nguyễn Huy Hoàng', 'HD2', 'Hợp đồng cùng mã khách hàng', '64000000', '10388553', 'WARNING_CREDIT', '31/03/2021', 'INDIV', 'VN0010005', 'B306', 'A', 'C', 'C', 'FEMALE'),
      WarningBScore('13', 'Today', '4665540', 'Nguyễn Hoàng Duy', 'LD1931707564', 'Cho vay dau tu du an thong thuong', '64000000', '10388553', 'WARNING_CREDIT', '20/05/2020', 'INDIV', 'VN0010005', 'B306', 'C', 'A', 'B', 'MALE'),
      WarningBScore('14', 'Tomorrow', '321123', 'Hương', 'HD20210326', 'Cho vay dau tu du an thong thuong', '100000', '12988', 'WARNING_SAVING', '26/03/2021', 'INDIV', 'VN0010005', 'B306', 'A', 'A', 'A', 'FEMALE'),
      WarningBScore('15', 'This Week', '1069768', 'Nguyễn Đắc Nhân Tâm', 'HD1', 'Hợp đồng sinh trắc vân tay trọn gói test theo KH DXSKL OMR PMXSKL.', '64000000', '10388553', 'WARNING_SAVING', '01/03/2021', 'INDIV', 'VN0010005', 'B306', 'B', 'B', 'B', 'MALE'),
      WarningBScore('16', '09/2024', '1030951', 'Nguyễn Huy Hoàng', 'HD2', 'Hợp đồng cùng mã khách hàng', '64000000', '10388553', 'WARNING_CREDIT', '31/03/2021', 'INDIV', 'VN0010005', 'B306', 'C', 'C', 'C', 'FEMALE'),
      WarningBScore('17', 'Today', '4665540', 'Nguyễn Hoàng Duy', 'LD1931707564', 'Cho vay dau tu du an thong thuong', '64000000', '10388553', 'WARNING_CREDIT', '20/05/2020', 'INDIV', 'VN0010005', 'B306', 'A', 'B', 'C', 'MALE'),
      WarningBScore('18', 'Tomorrow', '321123', 'Hương', 'HD20210326', 'Cho vay dau tu du an thong thuong', '100000', '12988', 'WARNING_SAVING', '26/03/2021', 'INDIV', 'VN0010005', 'B306', 'C', 'B', 'A', 'FEMALE'),
      WarningBScore('19', 'This Week', '1069768', 'Nguyễn Đắc Nhân Tâm', 'HD1', 'Hợp đồng sinh trắc vân tay trọn gói test theo KH DXSKL OMR PMXSKL.', '64000000', '10388553', 'WARNING_SAVING', '01/03/2021', 'INDIV', 'VN0010005', 'B306', 'B', 'B', 'A', 'MALE'),
      WarningBScore('20', '09/2024', '1030951', 'Nguyễn Huy Hoàng', 'HD2', 'Hợp đồng cùng mã khách hàng', '64000000', '10388553', 'WARNING_CREDIT', '31/03/2021', 'INDIV', 'VN0010005', 'B306', 'A', 'C', 'C', 'FEMALE'),
      WarningBScore('21', 'Today', '4665540', 'Nguyễn Hoàng Duy', 'LD1931707564', 'Cho vay dau tu du an thong thuong', '64000000', '10388553', 'WARNING_CREDIT', '20/05/2020', 'INDIV', 'VN0010005', 'B306', 'C', 'A', 'B', 'MALE'),
      WarningBScore('22', 'Tomorrow', '321123', 'Hương', 'HD20210326', 'Cho vay dau tu du an thong thuong', '100000', '12988', 'WARNING_SAVING', '26/03/2021', 'INDIV', 'VN0010005', 'B306', 'A', 'A', 'A', 'FEMALE'),
      WarningBScore('23', 'This Week', '1069768', 'Nguyễn Đắc Nhân Tâm', 'HD1', 'Hợp đồng sinh trắc vân tay trọn gói test theo KH DXSKL OMR PMXSKL.', '64000000', '10388553', 'WARNING_SAVING', '01/03/2021', 'INDIV', 'VN0010005', 'B306', 'B', 'B', 'B', 'MALE'),
      WarningBScore('24', '09/2024', '1030951', 'Nguyễn Huy Hoàng', 'HD2', 'Hợp đồng cùng mã khách hàng', '64000000', '10388553', 'WARNING_CREDIT', '31/03/2021', 'INDIV', 'VN0010005', 'B306', 'C', 'C', 'C', 'FEMALE'),
      WarningBScore('25', 'Today', '4665540', 'Nguyễn Hoàng Duy', 'LD1931707564', 'Cho vay dau tu du an thong thuong', '64000000', '10388553', 'WARNING_CREDIT', '20/05/2020', 'INDIV', 'VN0010005', 'B306', 'A', 'B', 'C', 'MALE'),
      WarningBScore('26', 'Tomorrow', '321123', 'Hương', 'HD20210326', 'Cho vay dau tu du an thong thuong', '100000', '12988', 'WARNING_SAVING', '26/03/2021', 'INDIV', 'VN0010005', 'B306', 'C', 'B', 'A', 'FEMALE'),
      WarningBScore('27', 'This Week', '1069768', 'Nguyễn Đắc Nhân Tâm', 'HD1', 'Hợp đồng sinh trắc vân tay trọn gói test theo KH DXSKL OMR PMXSKL.', '64000000', '10388553', 'WARNING_SAVING', '01/03/2021', 'INDIV', 'VN0010005', 'B306', 'B', 'B', 'A', 'MALE'),
      WarningBScore('28', '09/2024', '1030951', 'Nguyễn Huy Hoàng', 'HD2', 'Hợp đồng cùng mã khách hàng', '64000000', '10388553', 'WARNING_CREDIT', '31/03/2021', 'INDIV', 'VN0010005', 'B306', 'A', 'C', 'C', 'FEMALE'),
      WarningBScore('29', 'Today', '4665540', 'Nguyễn Hoàng Duy', 'LD1931707564', 'Cho vay dau tu du an thong thuong', '64000000', '10388553', 'WARNING_CREDIT', '20/05/2020', 'INDIV', 'VN0010005', 'B306', 'C', 'A', 'B', 'MALE'),
      WarningBScore('30', 'Tomorrow', '321123', 'Hương', 'HD20210326', 'Cho vay dau tu du an thong thuong', '100000', '12988', 'WARNING_SAVING', '26/03/2021', 'INDIV', 'VN0010005', 'B306', 'A', 'A', 'A', 'FEMALE'),
      WarningBScore('31', 'This Week', '1069768', 'Nguyễn Đắc Nhân Tâm', 'HD1', 'Hợp đồng sinh trắc vân tay trọn gói test theo KH DXSKL OMR PMXSKL.', '64000000', '10388553', 'WARNING_SAVING', '01/03/2021', 'INDIV', 'VN0010005', 'B306', 'B', 'B', 'B', 'MALE'),
      WarningBScore('32', '09/2024', '1030951', 'Nguyễn Huy Hoàng', 'HD2', 'Hợp đồng cùng mã khách hàng', '64000000', '10388553', 'WARNING_CREDIT', '31/03/2021', 'INDIV', 'VN0010005', 'B306', 'C', 'C', 'C', 'FEMALE'),
    ];
  }
}
