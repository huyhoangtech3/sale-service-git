import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:resources/resources.dart';
import 'package:base_architecture_flutter/src/app.dart';
import 'package:shared/shared.dart';

class Warning {
  Warning(this.id, this.groupDate, this.customerCode, this.customerName, this.contractCode, this.product, this.principal, this.interest, this.type,
      this.expiredDate, this.customerType, this.branch, this.rmCode, this.email, this.phone, this.productCode, this.gender);
  final String id;
  final String groupDate;
  final String customerCode;
  final String customerName;
  final String contractCode;
  final String product;
  final String principal;
  final String interest;
  final String type;
  final String expiredDate;
  final String customerType;
  final String branch;
  final String rmCode;
  final String email;
  final String phone;
  final String productCode;
  final String gender;
}

class WarningDataSource extends DataGridSource {
  WarningDataSource({
    required List<Warning> warningData,
  }) {
    _warningData = warningData.map<DataGridRow>((e) => DataGridRow(cells: [
      DataGridCell<String>(columnName: '1', value: '${e.groupDate}|${e.gender}'),
      DataGridCell<String>(columnName: '2', value: '${e.contractCode}|${e.customerName}'),
      DataGridCell<String>(columnName: '3', value: e.product),
      DataGridCell<String>(columnName: '4', value: e.principal),
      DataGridCell<String>(columnName: '5', value: e.interest),
      DataGridCell<String>(columnName: '6', value: (e.principal.toInt() + e.interest.toInt()).toString()),
      DataGridCell<String>(columnName: '7', value: e.type),
      DataGridCell<String>(columnName: '8', value: e.expiredDate),
    ])).toList();
  }

  List<GridColumn> columns = [
    GridColumn(
        columnName: '1',
        label: Container(
            padding: EdgeInsets.only(left: Dimens.d14.responsive()),
            alignment: Alignment.centerLeft,
            child: Text(''))),
    GridColumn(
        columnName: '2',
        label: Container(
            alignment: Alignment.center,
            child: Text('Mã hợp đồng'))),
    GridColumn(
        columnName: '3',
        label: Container(
            alignment: Alignment.center,
            child: Text('Sản phẩm'))),
    GridColumn(
        columnName: '4',
        label: Container(
            alignment: Alignment.center,
            child: Text('Gốc (đ)'))),
    GridColumn(
        columnName: '5',
        label: Container(
            alignment: Alignment.center,
            child: Text('Lãi (đ)'))),
    GridColumn(
        columnName: '6',
        label: Container(
            alignment: Alignment.center,
            child: Text('Tổng (đ)'))),
    GridColumn(
        columnName: '7',
        label: Container(
            alignment: Alignment.center,
            child: Text('Loại'))),
    GridColumn(
        columnName: '8',
        label: Container(
            alignment: Alignment.center,
            child: Text('Ngày đến hạn'))),
  ];
  List<DataGridRow> _warningData = [];
  List<Warning> warningData = [
    Warning('605dc0ba13076e2e04b12642', 'Today', '4665540', 'CHIC-LAND JSC', 'LD1931707564', 'Cho vay dau tu du an thong thuong', '64000000', '10388553', 'WARNING_CREDIT', '20/05/2020', 'INDIV', 'VN0010005', 'B306', '123@gmail.com', '0974477522', '1111', 'MALE'),
    Warning('605dc0ba13076e2e04b12645', 'Tomorrow', '321123', 'Hương Test', 'HD20210326', 'Cho vay dau tu du an thong thuong', '100000', '12988', 'WARNING_SAVING', '26/03/2021', 'INDIV', 'VN0010005', 'B306', '123@gmail.com', '0974477522', '1111', 'FEMALE'),
    Warning('60617c23ec608726205ce5d0', 'This Week', '1069768', 'DXSKL OMR PMXSKL', 'HD1', 'Hợp đồng sinh trắc vân tay trọn gói test theo KH DXSKL OMR PMXSKL.', '64000000', '10388553', 'WARNING_SAVING', '01/03/2021', 'INDIV', 'VN0010005', 'B306', '123@gmail.com', '0974477522', '1111', 'MALE'),
    Warning('60617dc0ec608726205ce5d1', '09/2024', '1030951', 'Nguyễn Huy Hoàng', 'HD2', 'Hợp đồng cùng mã khách hàng', '64000000', '10388553', 'WARNING_CREDIT', '31/03/2021', 'INDIV', 'VN0010005', 'B306', '123@gmail.com', '0974477522', '1111', 'FEMALE'),
    Warning('605dc0ba13076e2e04b12642', 'Today', '4665540', 'CHIC-LAND JSC', 'LD1931707564', 'Cho vay dau tu du an thong thuong', '64000000', '10388553', 'WARNING_CREDIT', '20/05/2020', 'INDIV', 'VN0010005', 'B306', '123@gmail.com', '0974477522', '1111', 'MALE'),
    Warning('605dc0ba13076e2e04b12645', 'Tomorrow', '321123', 'Hương Test', 'HD20210326', 'Cho vay dau tu du an thong thuong', '100000', '12988', 'WARNING_SAVING', '26/03/2021', 'INDIV', 'VN0010005', 'B306', '123@gmail.com', '0974477522', '1111', 'FEMALE'),
    Warning('60617c23ec608726205ce5d0', 'This Week', '1069768', 'DXSKL OMR PMXSKL', 'HD1', 'Hợp đồng sinh trắc vân tay trọn gói test theo KH DXSKL OMR PMXSKL.', '64000000', '10388553', 'WARNING_SAVING', '01/03/2021', 'INDIV', 'VN0010005', 'B306', '123@gmail.com', '0974477522', '1111', 'MALE'),
    Warning('60617dc0ec608726205ce5d1', '09/2024', '1030951', 'Nguyễn Huy Hoàng', 'HD2', 'Hợp đồng cùng mã khách hàng', '64000000', '10388553', 'WARNING_CREDIT', '31/03/2021', 'INDIV', 'VN0010005', 'B306', '123@gmail.com', '0974477522', '1111', 'FEMALE'),
  ];

  @override
  List<DataGridRow> get rows => _warningData;

  @override
  DataGridRowAdapter buildRow(DataGridRow row) {
    return DataGridRowAdapter(
        cells: row.getCells().map<Widget>((e) {
          return Container(
            alignment: Alignment.center,
            padding: EdgeInsets.all(Dimens.d0.responsive()),
            child: e.columnName == '1' ? Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: e.value.split('|')[1] == Constants.male ? Assets.images.avaMan.provider() : Assets.images.avaWoman.provider(),
                  fit: BoxFit.fitHeight,
                ),
              ),
              child: SizedBox(
                width: Dimens.d28.responsive(), height: Dimens.d28.responsive(),
              ),
            ) : e.columnName == '2' ? Text(e.value.split('|')[0], style: AppTextStyles.s12w400Tertiary())
                : Text(e.value, style: AppTextStyles.s12w400Tertiary(), overflow: TextOverflow.ellipsis, maxLines: 2),
          );
        }).toList());
  }

  @override
  Future<void> handleRefresh() async {
    await Future.delayed(const Duration(seconds: 1));
    notifyListeners();
  }

  @override
  Future<void> handleLoadMoreRows() async {
    await Future.delayed(const Duration(seconds: 2));
    _addMoreRows(warningData, 15);
    buildDataGridRows();
    notifyListeners();
  }

  void buildDataGridRows() {
    _warningData = warningData.map<DataGridRow>((e) {
      return DataGridRow(
          cells: columns.map<DataGridCell>((column) {
            dynamic cellValue;
            switch (column.columnName) {
              case '1':
                cellValue = e.groupDate;
                break;
              case '2':
                cellValue = e.contractCode;
                break;
              default:
                cellValue = null; // Handle any unexpected column names
            }
            return DataGridCell(
              columnName: column.columnName,
              value: cellValue,
            );
          }).toList());
    }).toList();
  }

  void _addMoreRows(List<Warning> warningData, int count) {
    final startIndex = warningData.isNotEmpty ? warningData.length : 0, endIndex = startIndex + count;
    for (int i = startIndex; i < endIndex; i++) {
      _warningData.add(Warning(
        warningData[i].id + i.toString(),
        warningData[i].customerCode + i.toString(),
        warningData[i].groupDate + i.toString(),
        warningData[i].id + i.toString(),
        warningData[i].customerCode + i.toString(),
        warningData[i].groupDate + i.toString(),
        warningData[i].id + i.toString(),
        warningData[i].customerCode + i.toString(),
        warningData[i].groupDate + i.toString(),
        warningData[i].id + i.toString(),
        warningData[i].customerCode + i.toString(),
        warningData[i].groupDate + i.toString(),
        warningData[i].id + i.toString(),
        warningData[i].customerCode + i.toString(),
        warningData[i].groupDate + i.toString(),
        warningData[i].id + i.toString(),
        warningData[i].customerCode + i.toString(),
      ) as DataGridRow);
    }
  }

  void refreshDataGrid() {
    notifyListeners();
  }

  void updateDataGridSource() {
    notifyListeners();
  }

  @override
  Widget? buildGroupCaptionCellWidget(RowColumnIndex rowColumnIndex, String summaryValue) {
    return Container(
        padding: EdgeInsets.only(left: Dimens.d14.responsive()),
        color: Colors.white,
        alignment: Alignment.centerLeft,
        child: Text(summaryValue));
  }

  @override
  String performGrouping(String columnName, DataGridRow row) {
    if (columnName == '1') {
      String e = row
          .getCells()
          .firstWhere((DataGridCell cell) => cell.columnName == columnName)!
          .value;
      final String date = e.split('|')[0];
      if (date == 'Today') {
        return 'Hôm nay';
      } else if (date == 'Tomorrow') {
        return 'Ngày mai';
      } else if (date == 'This Week') {
        return 'Tuần này';
      } else {
        return date;
      }
    }
    if (columnName == '2') {
      String e = row
          .getCells()
          .firstWhere((DataGridCell cell) => cell.columnName == columnName)!
          .value;
      final String code = e.split('|')[0];
      final String name = e.split('|')[1];
      return '$code - $name';
    }
    return super.performGrouping(columnName, row);
  }
}