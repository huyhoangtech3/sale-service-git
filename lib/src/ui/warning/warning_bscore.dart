import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:resources/resources.dart';
import 'package:base_architecture_flutter/src/app.dart';
import 'package:shared/shared.dart';

class WarningBScore {
  WarningBScore(this.id, this.groupDate, this.customerCode, this.customerName, this.contractCode, this.product, this.principal, this.interest, this.type,
      this.expiredDate, this.customerType, this.branch, this.rmCode, this.email, this.phone, this.productCode, this.gender);
  final String id;
  final String groupDate;
  final String customerCode;
  final String customerName;
  final String contractCode;
  final String product;
  final String principal;
  final String interest;
  final String type;
  final String expiredDate;
  final String customerType;
  final String branch;
  final String rmCode;
  final String email;
  final String phone;
  final String productCode;
  final String gender;
}

class WarningBScoreDataSource extends DataGridSource {
  WarningBScoreDataSource({
    required List<WarningBScore> warningData,
  }) {
    _warningData = warningData.map<DataGridRow>((e) => DataGridRow(cells: [
      DataGridCell<String>(columnName: '1', value: e.id),
      DataGridCell<String>(columnName: '2', value: '${e.contractCode} - ${e.customerName}'),
      DataGridCell<String>(columnName: '3', value: e.email),
      DataGridCell<String>(columnName: '4', value: e.phone),
      DataGridCell<String>(columnName: '5', value: e.productCode),
    ])).toList();
  }

  List<GridColumn> columns = [
    GridColumn(
        columnName: '1',
        label: Container(
            width: Dimens.d20.responsive(),
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.all(Dimens.d8.responsive()),
            child: Text('STT'))),
    GridColumn(
        columnName: '2',
        label: Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.all(Dimens.d8.responsive()),
            child: Text('Tên'))),
    GridColumn(
        columnName: '3',
        label: Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.all(Dimens.d8.responsive()),
            child: Text('Tháng 5/2024'))),
    GridColumn(
        columnName: '4',
        label: Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.all(Dimens.d8.responsive()),
            child: Text('Tháng 4/2024'))),
    GridColumn(
        columnName: '5',
        label: Container(
            alignment: Alignment.center,
            padding: EdgeInsets.all(Dimens.d8.responsive()),
            child: Text('Tháng 3/2024'))),
  ];
  List<DataGridRow> _warningData = [];
  List<Warning> warningData = [
    Warning('1', 'Today', '4665540', 'CHIC-LAND JSC', 'LD1931707564', 'Cho vay dau tu du an thong thuong', '64000000', '10388553', 'WARNING_CREDIT', '20/05/2020', 'INDIV', 'VN0010005', 'B306', 'A', 'B', 'C', 'MALE'),
    Warning('2', 'Tomorrow', '321123', 'Hương Test', 'HD20210326', 'Cho vay dau tu du an thong thuong', '100000', '12988', 'WARNING_SAVING', '26/03/2021', 'INDIV', 'VN0010005', 'B306', 'C', 'B', 'A', 'FEMALE'),
    Warning('3', 'This Week', '1069768', 'DXSKL OMR PMXSKL', 'HD1', 'Hợp đồng sinh trắc vân tay trọn gói test theo KH DXSKL OMR PMXSKL.', '64000000', '10388553', 'WARNING_SAVING', '01/03/2021', 'INDIV', 'VN0010005', 'B306', 'B', 'B', 'A', 'MALE'),
    Warning('4', '09/2024', '1030951', 'Nguyễn Huy Hoàng', 'HD2', 'Hợp đồng cùng mã khách hàng', '64000000', '10388553', 'WARNING_CREDIT', '31/03/2021', 'INDIV', 'VN0010005', 'B306', 'A', 'C', 'C', 'FEMALE'),
    Warning('5', 'Today', '4665540', 'CHIC-LAND JSC', 'LD1931707564', 'Cho vay dau tu du an thong thuong', '64000000', '10388553', 'WARNING_CREDIT', '20/05/2020', 'INDIV', 'VN0010005', 'B306', 'C', 'A', 'B', 'MALE'),
    Warning('6', 'Tomorrow', '321123', 'Hương Test', 'HD20210326', 'Cho vay dau tu du an thong thuong', '100000', '12988', 'WARNING_SAVING', '26/03/2021', 'INDIV', 'VN0010005', 'B306', 'A', 'A', 'A', 'FEMALE'),
    Warning('7', 'This Week', '1069768', 'DXSKL OMR PMXSKL', 'HD1', 'Hợp đồng sinh trắc vân tay trọn gói test theo KH DXSKL OMR PMXSKL.', '64000000', '10388553', 'WARNING_SAVING', '01/03/2021', 'INDIV', 'VN0010005', 'B306', 'B', 'B', 'B', 'MALE'),
    Warning('8', '09/2024', '1030951', 'Nguyễn Huy Hoàng', 'HD2', 'Hợp đồng cùng mã khách hàng', '64000000', '10388553', 'WARNING_CREDIT', '31/03/2021', 'INDIV', 'VN0010005', 'B306', 'C', 'C', 'C', 'FEMALE'),
  ];

  @override
  List<DataGridRow> get rows => _warningData;

  @override
  DataGridRowAdapter buildRow(DataGridRow row) {
    return DataGridRowAdapter(
        cells: row.getCells().map<Widget>((e) {
          return Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.all(Dimens.d8.responsive()),
            child: Text(e.value, style: AppTextStyles.s12w400Tertiary(), overflow: TextOverflow.ellipsis, maxLines: 2),
          );
        }).toList());
  }

  @override
  Future<void> handleRefresh() async {
    await Future.delayed(const Duration(seconds: 1));
    notifyListeners();
  }

  @override
  Future<void> handleLoadMoreRows() async {
    await Future.delayed(const Duration(seconds: 2));
    _addMoreRows(warningData, 15);
    buildDataGridRows();
    notifyListeners();
  }

  void buildDataGridRows() {
    _warningData = warningData.map<DataGridRow>((e) {
      return DataGridRow(
          cells: columns.map<DataGridCell>((column) {
            dynamic cellValue;
            switch (column.columnName) {
              case '1':
                cellValue = e.groupDate;
                break;
              case '2':
                cellValue = e.contractCode;
                break;
              default:
                cellValue = null; // Handle any unexpected column names
            }
            return DataGridCell(
              columnName: column.columnName,
              value: cellValue,
            );
          }).toList());
    }).toList();
  }

  void _addMoreRows(List<Warning> warningData, int count) {
    final startIndex = warningData.isNotEmpty ? warningData.length : 0, endIndex = startIndex + count;
    for (int i = startIndex; i < endIndex; i++) {
      _warningData.add(Warning(
        warningData[i].id + i.toString(),
        warningData[i].customerCode + i.toString(),
        warningData[i].groupDate + i.toString(),
        warningData[i].id + i.toString(),
        warningData[i].customerCode + i.toString(),
        warningData[i].groupDate + i.toString(),
        warningData[i].id + i.toString(),
        warningData[i].customerCode + i.toString(),
        warningData[i].groupDate + i.toString(),
        warningData[i].id + i.toString(),
        warningData[i].customerCode + i.toString(),
        warningData[i].groupDate + i.toString(),
        warningData[i].id + i.toString(),
        warningData[i].customerCode + i.toString(),
        warningData[i].groupDate + i.toString(),
        warningData[i].id + i.toString(),
        warningData[i].customerCode + i.toString(),
      ) as DataGridRow);
    }
  }

  void refreshDataGrid() {
    notifyListeners();
  }

  void updateDataGridSource() {
    notifyListeners();
  }
}