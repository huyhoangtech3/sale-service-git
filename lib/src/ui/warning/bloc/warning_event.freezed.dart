// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'warning_event.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$WarningPageInitiated {
  String get id => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $WarningPageInitiatedCopyWith<WarningPageInitiated> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $WarningPageInitiatedCopyWith<$Res> {
  factory $WarningPageInitiatedCopyWith(WarningPageInitiated value,
          $Res Function(WarningPageInitiated) then) =
      _$WarningPageInitiatedCopyWithImpl<$Res, WarningPageInitiated>;
  @useResult
  $Res call({String id});
}

/// @nodoc
class _$WarningPageInitiatedCopyWithImpl<$Res,
        $Val extends WarningPageInitiated>
    implements $WarningPageInitiatedCopyWith<$Res> {
  _$WarningPageInitiatedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$WarningPageInitiatedImplCopyWith<$Res>
    implements $WarningPageInitiatedCopyWith<$Res> {
  factory _$$WarningPageInitiatedImplCopyWith(_$WarningPageInitiatedImpl value,
          $Res Function(_$WarningPageInitiatedImpl) then) =
      __$$WarningPageInitiatedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String id});
}

/// @nodoc
class __$$WarningPageInitiatedImplCopyWithImpl<$Res>
    extends _$WarningPageInitiatedCopyWithImpl<$Res, _$WarningPageInitiatedImpl>
    implements _$$WarningPageInitiatedImplCopyWith<$Res> {
  __$$WarningPageInitiatedImplCopyWithImpl(_$WarningPageInitiatedImpl _value,
      $Res Function(_$WarningPageInitiatedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
  }) {
    return _then(_$WarningPageInitiatedImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$WarningPageInitiatedImpl implements _WarningPageInitiated {
  const _$WarningPageInitiatedImpl({required this.id});

  @override
  final String id;

  @override
  String toString() {
    return 'WarningPageInitiated(id: $id)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$WarningPageInitiatedImpl &&
            (identical(other.id, id) || other.id == id));
  }

  @override
  int get hashCode => Object.hash(runtimeType, id);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$WarningPageInitiatedImplCopyWith<_$WarningPageInitiatedImpl>
      get copyWith =>
          __$$WarningPageInitiatedImplCopyWithImpl<_$WarningPageInitiatedImpl>(
              this, _$identity);
}

abstract class _WarningPageInitiated implements WarningPageInitiated {
  const factory _WarningPageInitiated({required final String id}) =
      _$WarningPageInitiatedImpl;

  @override
  String get id;
  @override
  @JsonKey(ignore: true)
  _$$WarningPageInitiatedImplCopyWith<_$WarningPageInitiatedImpl>
      get copyWith => throw _privateConstructorUsedError;
}
