import 'package:freezed_annotation/freezed_annotation.dart';

import 'package:base_architecture_flutter/src/base/bloc/base_bloc_event.dart';

part 'warning_event.freezed.dart';

abstract class WarningEvent extends BaseBlocEvent {
  const WarningEvent();
}

@freezed
class WarningPageInitiated extends WarningEvent with _$WarningPageInitiated {
  const factory WarningPageInitiated({
    required String id,
  }) = _WarningPageInitiated;
}
