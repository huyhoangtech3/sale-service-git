// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'warning_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$WarningState {
  String get id => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $WarningStateCopyWith<WarningState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $WarningStateCopyWith<$Res> {
  factory $WarningStateCopyWith(
          WarningState value, $Res Function(WarningState) then) =
      _$WarningStateCopyWithImpl<$Res, WarningState>;
  @useResult
  $Res call({String id});
}

/// @nodoc
class _$WarningStateCopyWithImpl<$Res, $Val extends WarningState>
    implements $WarningStateCopyWith<$Res> {
  _$WarningStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$WarningStateImplCopyWith<$Res>
    implements $WarningStateCopyWith<$Res> {
  factory _$$WarningStateImplCopyWith(
          _$WarningStateImpl value, $Res Function(_$WarningStateImpl) then) =
      __$$WarningStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String id});
}

/// @nodoc
class __$$WarningStateImplCopyWithImpl<$Res>
    extends _$WarningStateCopyWithImpl<$Res, _$WarningStateImpl>
    implements _$$WarningStateImplCopyWith<$Res> {
  __$$WarningStateImplCopyWithImpl(
      _$WarningStateImpl _value, $Res Function(_$WarningStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
  }) {
    return _then(_$WarningStateImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$WarningStateImpl implements _WarningState {
  const _$WarningStateImpl({this.id = ''});

  @override
  @JsonKey()
  final String id;

  @override
  String toString() {
    return 'WarningState(id: $id)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$WarningStateImpl &&
            (identical(other.id, id) || other.id == id));
  }

  @override
  int get hashCode => Object.hash(runtimeType, id);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$WarningStateImplCopyWith<_$WarningStateImpl> get copyWith =>
      __$$WarningStateImplCopyWithImpl<_$WarningStateImpl>(this, _$identity);
}

abstract class _WarningState implements WarningState {
  const factory _WarningState({final String id}) = _$WarningStateImpl;

  @override
  String get id;
  @override
  @JsonKey(ignore: true)
  _$$WarningStateImplCopyWith<_$WarningStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
