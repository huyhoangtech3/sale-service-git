import 'package:freezed_annotation/freezed_annotation.dart';

import 'package:base_architecture_flutter/src/base/bloc/base_bloc_state.dart';

part 'warning_state.freezed.dart';

@freezed
class WarningState extends BaseBlocState with _$WarningState {
  const factory WarningState({
    @Default('') String id,
  }) = _WarningState;
}
