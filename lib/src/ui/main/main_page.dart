import 'package:auto_route/auto_route.dart';
import 'package:base_architecture_flutter/src/ui/home/home_page.dart';
import 'package:base_architecture_flutter/src/ui/setting/setting_page.dart';
import 'package:base_architecture_flutter/src/ui/customer/customer_page.dart';
import 'package:domain/domain.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:base_architecture_flutter/src/app.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:resources/resources.dart';
import 'package:shared/shared.dart';

@RoutePage()
class MainPage extends StatefulWidget {
  const MainPage({super.key});

  @override
  State<StatefulWidget> createState() {
    return _MainPageState();
  }
}

class _MainPageState extends BasePageState<MainPage, MainBloc> {
  final _bottomBarKey = GlobalKey();
  NavigationRailLabelType labelType = NavigationRailLabelType.all;
  bool showLeading = false;
  bool showTrailing = false;
  double groupAlignment = -1.0;

  @override
  void initState() {
    super.initState();
    bloc.add(const MainPageInitiated(id: ''));
  }

  @override
  Widget buildPage(BuildContext context) {
    return AutoTabsRouter(
      routes: (navigator as AppNavigatorImpl).tabRoutes,
      builder: (context, child) {
        final tabsRouter = AutoTabsRouter.of(context);
        (navigator as AppNavigatorImpl).tabsRouter = tabsRouter;
        return CommonScaffold(
          hideKeyboardWhenTouchOutside: true,
          appBar: CustomAppBar(
            shadowColor: AppColors.current.primaryColor,
            tabUserInfo: () => tabsRouter.setActiveIndex(8),
            tabToolsAndUtilities: () => navigator.showModalSheet(
              const AppPopupInfo.toolsDialog(),
              barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
              barrierDismissible: true,
              useRootNavigator: false,
            ),
            tabBirthDay: () => navigator.showModalSheet(
              const AppPopupInfo.birthDayDialog(),
              barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
              barrierDismissible: true,
              useRootNavigator: false,
            ),
            tabNotification: () => navigator.showModalSheet(
              const AppPopupInfo.notificationDialog(),
              barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
              barrierDismissible: true,
              useRootNavigator: false,
            ),
          ),
          body: SafeArea(
            child: Row(
              children: [
                SingleChildScrollView(
                  physics: const NeverScrollableScrollPhysics(),
                  child: ConstrainedBox(
                    constraints: BoxConstraints(minHeight: MediaQuery.of(context).size.height),
                    child: IntrinsicHeight(
                      child: NavigationRail(
                        indicatorColor: Colors.transparent,
                        elevation: 1,
                        key: _bottomBarKey,
                        selectedIndex: tabsRouter.activeIndex,
                        groupAlignment: groupAlignment,
                        labelType: labelType,
                        selectedIconTheme: IconThemeData(
                          color: AppColors.current.primaryColor,
                        ),
                        selectedLabelTextStyle: AppTextStyles.s10w400Secondary(),
                        unselectedLabelTextStyle: AppTextStyles.s10w400Tertiary(),
                        leading: showLeading? FloatingActionButton(
                          elevation: 0,
                          onPressed: () {
                            // Add your onPressed code here!
                          },
                          child: const Icon(Icons.add),
                        )
                            : const SizedBox(),
                        trailing: showTrailing? IconButton(
                          onPressed: () {
                            // Add your onPressed code here!
                          },
                          icon: const Icon(Icons.more_horiz_rounded),
                        )
                            : const SizedBox(),
                        onDestinationSelected: (index) {
                          tabsRouter.setActiveIndex(index);
                          if (index == tabsRouter.activeIndex) {
                            (navigator as AppNavigatorImpl).popUntilRootOfCurrentBottomTab();
                          }
                        },
                        destinations: BottomTab.values.map((tab) => NavigationRailDestination(
                          label: Text(tab.title),
                          icon: tab.icon,
                          selectedIcon: tab.activeIcon,
                          disabled: tab.title == Constants.space ? true : false,
                        ),
                        ).toList(),
                      ),
                    ),
                  ),
                ),
                const VerticalDivider(thickness: 1, width: 1),
                Expanded(
                  child: child,
                ),
              ],
            ),
          ),
        );
      },
    );
    // return AutoTabsScaffold(
    //   routes: (navigator as AppNavigatorImpl).tabRoutes,
    //   bottomNavigationBuilder: (_, tabsRouter) {
    //     (navigator as AppNavigatorImpl).tabsRouter = tabsRouter;
    //     return BottomNavigationBar(
    //       key: _bottomBarKey,
    //       currentIndex: tabsRouter.activeIndex,
    //       onTap: (index) {
    //         if (index == tabsRouter.activeIndex) {
    //           (navigator as AppNavigatorImpl).popUntilRootOfCurrentBottomTab();
    //         }
    //         tabsRouter.setActiveIndex(index);
    //       },
    //       showSelectedLabels: true,
    //       showUnselectedLabels: true,
    //       // unselectedItemColor: AppColors.current.primaryColor,
    //       // selectedItemColor: AppColors.current.primaryColor,
    //       type: BottomNavigationBarType.fixed,
    //       // backgroundColor: AppColors.current.primaryColor,
    //       items: BottomTab.values
    //           .map(
    //             (tab) => BottomNavigationBarItem(
    //               label: tab.title,
    //               icon: tab.icon,
    //               activeIcon: tab.activeIcon,
    //             ),
    //           )
    //           .toList(),
    //     );
    //   },
    // );
  }
}