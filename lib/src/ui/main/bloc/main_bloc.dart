import 'dart:async';

import 'package:domain/domain.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

import 'package:base_architecture_flutter/src/app.dart';
import 'package:shared/shared.dart';

@Injectable()
class MainBloc extends BaseBloc<MainEvent, MainState> {
  MainBloc(this._getInitialMainDataUseCase) : super(const MainState()) {
    on<MainPageInitiated>(
      _onMainPageInitiated,
      transformer: log(),
    );
  }

  final GetInitialMainDataUseCase _getInitialMainDataUseCase;

  FutureOr<void> _onMainPageInitiated(MainPageInitiated event, Emitter<MainState> emit) async {
    await runBlocCatching(
      action: () async {
        final output = _getInitialMainDataUseCase.execute(const GetInitialMainDataInput());
        emit(state.copyWith(
          id: output.id,
          fullName: output.fullName,
          userName: output.userName,
          gender: output.gender,
        ));
      },
    );
  }
}
