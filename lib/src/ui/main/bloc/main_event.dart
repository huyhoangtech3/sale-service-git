import 'package:freezed_annotation/freezed_annotation.dart';

import 'package:base_architecture_flutter/src/app.dart';

part 'main_event.freezed.dart';

abstract class MainEvent extends BaseBlocEvent {
  const MainEvent();
}

@freezed
class MainPageInitiated extends MainEvent with _$MainPageInitiated {
  const factory MainPageInitiated({
    required String id,
  }) = _MainPageInitiated;
}
