import 'package:domain/domain.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:resources/resources.dart';
import 'package:shared/shared.dart';

import 'package:base_architecture_flutter/src/app.dart';

@LazySingleton(as: BasePopupInfoMapper)
class AppPopupInfoMapper extends BasePopupInfoMapper {
  FocusNode focusNode = FocusNode();
  static const List<String> list = <String>['Ngày', 'Tháng', 'Năm'];
  String dropdownValue = list.first;
  @override
  void initState() {
    super.initState();
    focusNode = FocusNode();
  }

  @override
  void dispose() {
    // Clean up the focus node when the Form is disposed.
    focusNode.dispose();
    super.dispose();
  }

  @override
  Widget buildPage(BuildContext context) {
    // TODO: implement buildPage
    throw UnimplementedError();
  }

  @override
  Widget map(AppPopupInfo appPopupInfo, AppNavigator navigator) {
    return appPopupInfo.when(
      confirmDialog: (message, onPressed) {
        return CommonDialog(
          actions: [
            PopupButton(
              text: S.current.ok,
              onPressed: onPressed ?? Func0(() => navigator.pop()),
            ),
          ],
          message: message,
          title: S.current.message,
        );
      },
      errorWithRetryDialog: (message, onRetryPressed) {
        return CommonDialog(
          actions: [
            PopupButton(
              text: S.current.cancel,
              onPressed: Func0(() => navigator.pop()),
            ),
            PopupButton(
              text: S.current.retry,
              onPressed: onRetryPressed ?? Func0(() => navigator.pop()),
              isDefault: true,
            ),
          ],
          message: message,
          title: S.current.errorOccurred,
        );
      },
      requiredLoginDialog: () => CommonDialog.adaptive(
        title: S.current.login,
        message: S.current.login,
        actions: [
          PopupButton(
            text: S.current.cancel,
            onPressed: Func0(() => navigator.pop()),
          ),
          PopupButton(
            text: S.current.login,
            onPressed: Func0(() async {
              await navigator.pop();
              await navigator.push(const AppRouteInfo.login());
            }),
          ),
        ],
      ),
      toolsDialog: () {
        return ToolsDialog(
            onPressedOpenCalculateSavingDialog: Func0(() async {
              await navigator.showModalSheet(
                const AppPopupInfo.calculateSavingDialog(),
                barrierDismissible: false,
                useRootNavigator: false,
              );
            }).call,
            onPressedOpenExchangeRateDialog: Func0(() async {
              await navigator.showModalSheet(
                const AppPopupInfo.exchangeRateDialog(),
                barrierDismissible: false,
                useRootNavigator: false,
              );
            }).call,
            onPressedOpenInterbankDialog: Func0(() async {
              await navigator.showModalSheet(
                const AppPopupInfo.interbankDialog(),
                barrierDismissible: false,
                useRootNavigator: false,
              );
            }).call,
        );
      },
      birthDayDialog: () {
        return const BirthDayDialog();
      },
      notificationDialog: () {
        return const NotificationDialog();
      },
      calculateSavingDialog: () {
        return const CalculateSavingDialog();
      },
      exchangeRateDialog: () {
        return const ExchangeRateDialog();
      },
      interbankDialog: () {
        return const InterbankDialog();
      },
      createJobDialog: () {
        return const CreateJobDialog();
      },
      pickerDateRangeDialog: () {
        return const PickerDateRangeDialog();
      },
      createTodoDialog: () {
        return const CreateTodoDialog();
      },
    );
  }
}
