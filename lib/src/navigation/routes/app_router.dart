import 'package:auto_route/auto_route.dart';
import 'package:domain/domain.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';

import 'package:base_architecture_flutter/src/app.dart';

// ignore_for_file:prefer-single-widget-per-file
@AutoRouterConfig(
  replaceInRouteName: 'Page,Route',
)
@LazySingleton()
class AppRouter extends $AppRouter {
  @override
  RouteType get defaultRouteType => const RouteType.adaptive();

  @override
  List<AutoRoute> get routes => [
        AutoRoute(page: LoginRoute.page),
        AutoRoute(page: MainRoute.page, children: [
          AutoRoute(
            // page: HomeTab.page,
            page: DashBoardTab.page,
            maintainState: true,
            children: [
              // AutoRoute(page: HomeRoute.page, initial: true),
              // AutoRoute(
              //   page: ItemDetailRoute.page,
              //   guards: [RouteGuard(GetIt.instance.get<IsLoggedInUseCase>())],
              // ),
              AutoRoute(page: DashBoardRoute.page, initial: true),
            ],
          ),
          AutoRoute(
            page: CustomerTab.page,
            maintainState: true,
            children: [
              AutoRoute(page: CustomerRoute.page, initial: true),
              AutoRoute(
                page: CustomerDetailRoute.page,
                guards: [RouteGuard(GetIt.instance.get<IsLoggedInUseCase>())],
              ),
            ],
          ),
          AutoRoute(
            page: RMTab.page,
            maintainState: true,
            children: [
              AutoRoute(page: RMRoute.page, initial: true),
            ],
          ),
          AutoRoute(
            page: CampaignTab.page,
            maintainState: true,
            children: [
              AutoRoute(page: CampaignRoute.page, initial: true),
            ],
          ),
          AutoRoute(
            page: SalesTab.page,
            maintainState: true,
            children: [
              AutoRoute(page: SalesRoute.page, initial: true),
            ],
          ),
          AutoRoute(
            page: SaleKitTab.page,
            maintainState: true,
            children: [
              AutoRoute(page: SaleKitRoute.page, initial: true),
            ],
          ),
          AutoRoute(
            page: WarningTab.page,
            maintainState: true,
            children: [
              AutoRoute(page: WarningRoute.page, initial: true),
            ],
          ),
          AutoRoute(
            page: SettingTab.page,
            maintainState: true,
            children: [
              AutoRoute(page: SettingRoute.page, initial: true),
            ],
          ),
          AutoRoute(
            page: UserInfoTab.page,
            maintainState: true,
            children: [
              AutoRoute(page: UserInfoRoute.page, initial: true),
            ],
          ),
        ]),
      ];
}

// @RoutePage(name: 'HomeTab')
// class HomeTabPage extends AutoRouter {
//   const HomeTabPage({super.key});
// }

@RoutePage(name: 'DashBoardTab')
class DashBoardTabPage extends AutoRouter {
  const DashBoardTabPage({super.key});
}

@RoutePage(name: 'CustomerTab')
class CustomerTabPage extends AutoRouter {
  const CustomerTabPage({super.key});
}

@RoutePage(name: 'RMTab')
class RMTabPage extends AutoRouter {
  const RMTabPage({super.key});
}

@RoutePage(name: 'CampaignTab')
class CampaignTabPage extends AutoRouter {
  const CampaignTabPage({super.key});
}

@RoutePage(name: 'SalesTab')
class SalesTabPage extends AutoRouter {
  const SalesTabPage({super.key});
}

@RoutePage(name: 'SaleKitTab')
class SaleKitTabPage extends AutoRouter {
  const SaleKitTabPage({super.key});
}

@RoutePage(name: 'WarningTab')
class WarningTabPage extends AutoRouter {
  const WarningTabPage({super.key});
}

@RoutePage(name: 'SettingTab')
class SettingTabPage extends AutoRouter {
  const SettingTabPage({super.key});
}

@RoutePage(name: 'UserInfoTab')
class UserInfoTabPage extends AutoRouter {
  const UserInfoTabPage({super.key});
}
