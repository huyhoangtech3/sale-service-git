// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouterGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:auto_route/auto_route.dart' as _i16;
import 'package:base_architecture_flutter/src/navigation/routes/app_router.dart'
    as _i2;
import 'package:base_architecture_flutter/src/ui/campaign/campaign_page.dart'
    as _i1;
import 'package:base_architecture_flutter/src/ui/customer/customer_detail/customer_detail_page.dart'
    as _i3;
import 'package:base_architecture_flutter/src/ui/customer/customer_page.dart'
    as _i4;
import 'package:base_architecture_flutter/src/ui/dashboard/dashboard_page.dart'
    as _i5;
import 'package:base_architecture_flutter/src/ui/home/home_page.dart' as _i6;
import 'package:base_architecture_flutter/src/ui/item_detail/item_detail_page.dart'
    as _i7;
import 'package:base_architecture_flutter/src/ui/login/login_page.dart' as _i8;
import 'package:base_architecture_flutter/src/ui/main/main_page.dart' as _i9;
import 'package:base_architecture_flutter/src/ui/rm/rm_page.dart' as _i10;
import 'package:base_architecture_flutter/src/ui/sale_kit/sale_kit_page.dart'
    as _i11;
import 'package:base_architecture_flutter/src/ui/sales/sales_page.dart' as _i12;
import 'package:base_architecture_flutter/src/ui/setting/setting_page.dart'
    as _i13;
import 'package:base_architecture_flutter/src/ui/user_info/user_info_page.dart'
    as _i14;
import 'package:base_architecture_flutter/src/ui/warning/warning_page.dart'
    as _i15;
import 'package:domain/domain.dart' as _i17;
import 'package:flutter/cupertino.dart' as _i18;
import 'package:flutter/material.dart' as _i19;

abstract class $AppRouter extends _i16.RootStackRouter {
  $AppRouter({super.navigatorKey});

  @override
  final Map<String, _i16.PageFactory> pagesMap = {
    CampaignRoute.name: (routeData) {
      return _i16.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i1.CampaignPage(),
      );
    },
    CampaignTab.name: (routeData) {
      return _i16.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i2.CampaignTabPage(),
      );
    },
    CustomerDetailRoute.name: (routeData) {
      final args = routeData.argsAs<CustomerDetailRouteArgs>();
      return _i16.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: _i3.CustomerDetailPage(
          customer: args.customer,
          key: args.key,
        ),
      );
    },
    CustomerRoute.name: (routeData) {
      return _i16.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i4.CustomerPage(),
      );
    },
    CustomerTab.name: (routeData) {
      return _i16.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i2.CustomerTabPage(),
      );
    },
    DashBoardRoute.name: (routeData) {
      return _i16.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i5.DashBoardPage(),
      );
    },
    DashBoardTab.name: (routeData) {
      return _i16.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i2.DashBoardTabPage(),
      );
    },
    HomeRoute.name: (routeData) {
      return _i16.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i6.HomePage(),
      );
    },
    ItemDetailRoute.name: (routeData) {
      final args = routeData.argsAs<ItemDetailRouteArgs>();
      return _i16.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: _i7.ItemDetailPage(
          user: args.user,
          key: args.key,
        ),
      );
    },
    LoginRoute.name: (routeData) {
      return _i16.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i8.LoginPage(),
      );
    },
    MainRoute.name: (routeData) {
      return _i16.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i9.MainPage(),
      );
    },
    RMRoute.name: (routeData) {
      return _i16.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i10.RMPage(),
      );
    },
    RMTab.name: (routeData) {
      return _i16.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i2.RMTabPage(),
      );
    },
    SaleKitRoute.name: (routeData) {
      return _i16.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i11.SaleKitPage(),
      );
    },
    SaleKitTab.name: (routeData) {
      return _i16.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i2.SaleKitTabPage(),
      );
    },
    SalesRoute.name: (routeData) {
      return _i16.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i12.SalesPage(),
      );
    },
    SalesTab.name: (routeData) {
      return _i16.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i2.SalesTabPage(),
      );
    },
    SettingRoute.name: (routeData) {
      return _i16.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i13.SettingPage(),
      );
    },
    SettingTab.name: (routeData) {
      return _i16.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i2.SettingTabPage(),
      );
    },
    UserInfoRoute.name: (routeData) {
      return _i16.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i14.UserInfoPage(),
      );
    },
    UserInfoTab.name: (routeData) {
      return _i16.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i2.UserInfoTabPage(),
      );
    },
    WarningRoute.name: (routeData) {
      return _i16.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i15.WarningPage(),
      );
    },
    WarningTab.name: (routeData) {
      return _i16.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i2.WarningTabPage(),
      );
    },
  };
}

/// generated route for
/// [_i1.CampaignPage]
class CampaignRoute extends _i16.PageRouteInfo<void> {
  const CampaignRoute({List<_i16.PageRouteInfo>? children})
      : super(
          CampaignRoute.name,
          initialChildren: children,
        );

  static const String name = 'CampaignRoute';

  static const _i16.PageInfo<void> page = _i16.PageInfo<void>(name);
}

/// generated route for
/// [_i2.CampaignTabPage]
class CampaignTab extends _i16.PageRouteInfo<void> {
  const CampaignTab({List<_i16.PageRouteInfo>? children})
      : super(
          CampaignTab.name,
          initialChildren: children,
        );

  static const String name = 'CampaignTab';

  static const _i16.PageInfo<void> page = _i16.PageInfo<void>(name);
}

/// generated route for
/// [_i3.CustomerDetailPage]
class CustomerDetailRoute extends _i16.PageRouteInfo<CustomerDetailRouteArgs> {
  CustomerDetailRoute({
    required _i17.Customer customer,
    _i18.Key? key,
    List<_i16.PageRouteInfo>? children,
  }) : super(
          CustomerDetailRoute.name,
          args: CustomerDetailRouteArgs(
            customer: customer,
            key: key,
          ),
          initialChildren: children,
        );

  static const String name = 'CustomerDetailRoute';

  static const _i16.PageInfo<CustomerDetailRouteArgs> page =
      _i16.PageInfo<CustomerDetailRouteArgs>(name);
}

class CustomerDetailRouteArgs {
  const CustomerDetailRouteArgs({
    required this.customer,
    this.key,
  });

  final _i17.Customer customer;

  final _i18.Key? key;

  @override
  String toString() {
    return 'CustomerDetailRouteArgs{customer: $customer, key: $key}';
  }
}

/// generated route for
/// [_i4.CustomerPage]
class CustomerRoute extends _i16.PageRouteInfo<void> {
  const CustomerRoute({List<_i16.PageRouteInfo>? children})
      : super(
          CustomerRoute.name,
          initialChildren: children,
        );

  static const String name = 'CustomerRoute';

  static const _i16.PageInfo<void> page = _i16.PageInfo<void>(name);
}

/// generated route for
/// [_i2.CustomerTabPage]
class CustomerTab extends _i16.PageRouteInfo<void> {
  const CustomerTab({List<_i16.PageRouteInfo>? children})
      : super(
          CustomerTab.name,
          initialChildren: children,
        );

  static const String name = 'CustomerTab';

  static const _i16.PageInfo<void> page = _i16.PageInfo<void>(name);
}

/// generated route for
/// [_i5.DashBoardPage]
class DashBoardRoute extends _i16.PageRouteInfo<void> {
  const DashBoardRoute({List<_i16.PageRouteInfo>? children})
      : super(
          DashBoardRoute.name,
          initialChildren: children,
        );

  static const String name = 'DashBoardRoute';

  static const _i16.PageInfo<void> page = _i16.PageInfo<void>(name);
}

/// generated route for
/// [_i2.DashBoardTabPage]
class DashBoardTab extends _i16.PageRouteInfo<void> {
  const DashBoardTab({List<_i16.PageRouteInfo>? children})
      : super(
          DashBoardTab.name,
          initialChildren: children,
        );

  static const String name = 'DashBoardTab';

  static const _i16.PageInfo<void> page = _i16.PageInfo<void>(name);
}

/// generated route for
/// [_i6.HomePage]
class HomeRoute extends _i16.PageRouteInfo<void> {
  const HomeRoute({List<_i16.PageRouteInfo>? children})
      : super(
          HomeRoute.name,
          initialChildren: children,
        );

  static const String name = 'HomeRoute';

  static const _i16.PageInfo<void> page = _i16.PageInfo<void>(name);
}

/// generated route for
/// [_i7.ItemDetailPage]
class ItemDetailRoute extends _i16.PageRouteInfo<ItemDetailRouteArgs> {
  ItemDetailRoute({
    required _i17.User user,
    _i19.Key? key,
    List<_i16.PageRouteInfo>? children,
  }) : super(
          ItemDetailRoute.name,
          args: ItemDetailRouteArgs(
            user: user,
            key: key,
          ),
          initialChildren: children,
        );

  static const String name = 'ItemDetailRoute';

  static const _i16.PageInfo<ItemDetailRouteArgs> page =
      _i16.PageInfo<ItemDetailRouteArgs>(name);
}

class ItemDetailRouteArgs {
  const ItemDetailRouteArgs({
    required this.user,
    this.key,
  });

  final _i17.User user;

  final _i19.Key? key;

  @override
  String toString() {
    return 'ItemDetailRouteArgs{user: $user, key: $key}';
  }
}

/// generated route for
/// [_i8.LoginPage]
class LoginRoute extends _i16.PageRouteInfo<void> {
  const LoginRoute({List<_i16.PageRouteInfo>? children})
      : super(
          LoginRoute.name,
          initialChildren: children,
        );

  static const String name = 'LoginRoute';

  static const _i16.PageInfo<void> page = _i16.PageInfo<void>(name);
}

/// generated route for
/// [_i9.MainPage]
class MainRoute extends _i16.PageRouteInfo<void> {
  const MainRoute({List<_i16.PageRouteInfo>? children})
      : super(
          MainRoute.name,
          initialChildren: children,
        );

  static const String name = 'MainRoute';

  static const _i16.PageInfo<void> page = _i16.PageInfo<void>(name);
}

/// generated route for
/// [_i10.RMPage]
class RMRoute extends _i16.PageRouteInfo<void> {
  const RMRoute({List<_i16.PageRouteInfo>? children})
      : super(
          RMRoute.name,
          initialChildren: children,
        );

  static const String name = 'RMRoute';

  static const _i16.PageInfo<void> page = _i16.PageInfo<void>(name);
}

/// generated route for
/// [_i2.RMTabPage]
class RMTab extends _i16.PageRouteInfo<void> {
  const RMTab({List<_i16.PageRouteInfo>? children})
      : super(
          RMTab.name,
          initialChildren: children,
        );

  static const String name = 'RMTab';

  static const _i16.PageInfo<void> page = _i16.PageInfo<void>(name);
}

/// generated route for
/// [_i11.SaleKitPage]
class SaleKitRoute extends _i16.PageRouteInfo<void> {
  const SaleKitRoute({List<_i16.PageRouteInfo>? children})
      : super(
          SaleKitRoute.name,
          initialChildren: children,
        );

  static const String name = 'SaleKitRoute';

  static const _i16.PageInfo<void> page = _i16.PageInfo<void>(name);
}

/// generated route for
/// [_i2.SaleKitTabPage]
class SaleKitTab extends _i16.PageRouteInfo<void> {
  const SaleKitTab({List<_i16.PageRouteInfo>? children})
      : super(
          SaleKitTab.name,
          initialChildren: children,
        );

  static const String name = 'SaleKitTab';

  static const _i16.PageInfo<void> page = _i16.PageInfo<void>(name);
}

/// generated route for
/// [_i12.SalesPage]
class SalesRoute extends _i16.PageRouteInfo<void> {
  const SalesRoute({List<_i16.PageRouteInfo>? children})
      : super(
          SalesRoute.name,
          initialChildren: children,
        );

  static const String name = 'SalesRoute';

  static const _i16.PageInfo<void> page = _i16.PageInfo<void>(name);
}

/// generated route for
/// [_i2.SalesTabPage]
class SalesTab extends _i16.PageRouteInfo<void> {
  const SalesTab({List<_i16.PageRouteInfo>? children})
      : super(
          SalesTab.name,
          initialChildren: children,
        );

  static const String name = 'SalesTab';

  static const _i16.PageInfo<void> page = _i16.PageInfo<void>(name);
}

/// generated route for
/// [_i13.SettingPage]
class SettingRoute extends _i16.PageRouteInfo<void> {
  const SettingRoute({List<_i16.PageRouteInfo>? children})
      : super(
          SettingRoute.name,
          initialChildren: children,
        );

  static const String name = 'SettingRoute';

  static const _i16.PageInfo<void> page = _i16.PageInfo<void>(name);
}

/// generated route for
/// [_i2.SettingTabPage]
class SettingTab extends _i16.PageRouteInfo<void> {
  const SettingTab({List<_i16.PageRouteInfo>? children})
      : super(
          SettingTab.name,
          initialChildren: children,
        );

  static const String name = 'SettingTab';

  static const _i16.PageInfo<void> page = _i16.PageInfo<void>(name);
}

/// generated route for
/// [_i14.UserInfoPage]
class UserInfoRoute extends _i16.PageRouteInfo<void> {
  const UserInfoRoute({List<_i16.PageRouteInfo>? children})
      : super(
          UserInfoRoute.name,
          initialChildren: children,
        );

  static const String name = 'UserInfoRoute';

  static const _i16.PageInfo<void> page = _i16.PageInfo<void>(name);
}

/// generated route for
/// [_i2.UserInfoTabPage]
class UserInfoTab extends _i16.PageRouteInfo<void> {
  const UserInfoTab({List<_i16.PageRouteInfo>? children})
      : super(
          UserInfoTab.name,
          initialChildren: children,
        );

  static const String name = 'UserInfoTab';

  static const _i16.PageInfo<void> page = _i16.PageInfo<void>(name);
}

/// generated route for
/// [_i15.WarningPage]
class WarningRoute extends _i16.PageRouteInfo<void> {
  const WarningRoute({List<_i16.PageRouteInfo>? children})
      : super(
          WarningRoute.name,
          initialChildren: children,
        );

  static const String name = 'WarningRoute';

  static const _i16.PageInfo<void> page = _i16.PageInfo<void>(name);
}

/// generated route for
/// [_i2.WarningTabPage]
class WarningTab extends _i16.PageRouteInfo<void> {
  const WarningTab({List<_i16.PageRouteInfo>? children})
      : super(
          WarningTab.name,
          initialChildren: children,
        );

  static const String name = 'WarningTab';

  static const _i16.PageInfo<void> page = _i16.PageInfo<void>(name);
}
