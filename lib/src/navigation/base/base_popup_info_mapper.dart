import 'package:domain/domain.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:provider/provider.dart';
import 'package:shared/shared.dart';

import 'package:base_architecture_flutter/src/app.dart';

abstract class BasePopupInfoMapper <T extends StatefulWidget, B extends BaseBloc>
    extends BasePageStateDelegate<T, B> with LogMixin {
  Widget map(AppPopupInfo appPopupInfo, AppNavigator navigator);
}
