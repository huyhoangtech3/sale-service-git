// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:base_architecture_flutter/src/app.dart' as _i11;
import 'package:base_architecture_flutter/src/app/bloc/app_bloc.dart' as _i26;
import 'package:base_architecture_flutter/src/base/bloc/common/common_bloc.dart'
    as _i22;
import 'package:base_architecture_flutter/src/navigation/app_navigator_impl.dart'
    as _i30;
import 'package:base_architecture_flutter/src/navigation/mapper/app_popup_info_mapper.dart'
    as _i16;
import 'package:base_architecture_flutter/src/navigation/mapper/app_route_info_mapper.dart'
    as _i12;
import 'package:base_architecture_flutter/src/navigation/middleware/route_guard.dart'
    as _i29;
import 'package:base_architecture_flutter/src/navigation/routes/app_router.dart'
    as _i10;
import 'package:base_architecture_flutter/src/ui/campaign/bloc/campaign_bloc.dart'
    as _i3;
import 'package:base_architecture_flutter/src/ui/customer/bloc/customer_bloc.dart'
    as _i20;
import 'package:base_architecture_flutter/src/ui/customer/customer_detail/bloc/customer_detail_bloc.dart'
    as _i4;
import 'package:base_architecture_flutter/src/ui/dashboard/bloc/dashboard_bloc.dart'
    as _i21;
import 'package:base_architecture_flutter/src/ui/dashboard/create_job/bloc/create_job_bloc.dart'
    as _i17;
import 'package:base_architecture_flutter/src/ui/dashboard/create_todo/bloc/create_todo_bloc.dart'
    as _i31;
import 'package:base_architecture_flutter/src/ui/home/bloc/home_bloc.dart'
    as _i28;
import 'package:base_architecture_flutter/src/ui/item_detail/bloc/item_detail_bloc.dart'
    as _i5;
import 'package:base_architecture_flutter/src/ui/login/bloc/login_bloc.dart'
    as _i13;
import 'package:base_architecture_flutter/src/ui/main/bloc/main_bloc.dart'
    as _i27;
import 'package:base_architecture_flutter/src/ui/rm/bloc/rm_bloc.dart' as _i6;
import 'package:base_architecture_flutter/src/ui/sale_kit/bloc/sale_kit_bloc.dart'
    as _i8;
import 'package:base_architecture_flutter/src/ui/sales/bloc/sales_bloc.dart'
    as _i7;
import 'package:base_architecture_flutter/src/ui/setting/bloc/setting_bloc.dart'
    as _i23;
import 'package:base_architecture_flutter/src/ui/tools_utilities/calculate_saving/bloc/calculate_saving_bloc.dart'
    as _i25;
import 'package:base_architecture_flutter/src/ui/tools_utilities/exchange_rate/bloc/exchange_rate_bloc.dart'
    as _i19;
import 'package:base_architecture_flutter/src/ui/tools_utilities/interbank/bloc/interbank_bloc.dart'
    as _i18;
import 'package:base_architecture_flutter/src/ui/user_info/bloc/user_info_bloc.dart'
    as _i24;
import 'package:base_architecture_flutter/src/ui/warning/bloc/warning_bloc.dart'
    as _i9;
import 'package:domain/domain.dart' as _i14;
import 'package:flutter/material.dart' as _i15;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

extension GetItInjectableX on _i1.GetIt {
// initializes the registration of main-scope dependencies inside of GetIt
  _i1.GetIt init({
    String? environment,
    _i2.EnvironmentFilter? environmentFilter,
  }) {
    final gh = _i2.GetItHelper(
      this,
      environment,
      environmentFilter,
    );
    gh.factory<_i3.CampaignBloc>(() => _i3.CampaignBloc());
    gh.factory<_i4.CustomerDetailBloc>(() => _i4.CustomerDetailBloc());
    gh.factory<_i5.ItemDetailBloc>(() => _i5.ItemDetailBloc());
    gh.factory<_i6.RMBloc>(() => _i6.RMBloc());
    gh.factory<_i7.SalesBloc>(() => _i7.SalesBloc());
    gh.factory<_i8.SaleKitBloc>(() => _i8.SaleKitBloc());
    gh.factory<_i9.WarningBloc>(() => _i9.WarningBloc());
    gh.lazySingleton<_i10.AppRouter>(() => _i10.AppRouter());
    gh.lazySingleton<_i11.BaseRouteInfoMapper>(() => _i12.AppRouteInfoMapper());
    gh.factory<_i13.LoginBloc>(() => _i13.LoginBloc(
          gh<_i14.LoginUseCase>(),
          gh<_i14.UserInfoUseCase>(),
          gh<_i14.UserInfoHrsCodeUseCase>(),
          gh<_i14.BlockUseCase>(),
          gh<_i14.BlockListUseCase>(),
          gh<_i14.BranchUseCase>(),
        ));
    gh.lazySingleton<
            _i11.BasePopupInfoMapper<_i15.StatefulWidget,
                _i11.BaseBloc<_i11.BaseBlocEvent, _i11.BaseBlocState>>>(
        () => _i16.AppPopupInfoMapper());
    gh.factory<_i17.CreateJobBloc>(
        () => _i17.CreateJobBloc(gh<_i14.CreateJobUseCase>()));
    gh.factory<_i18.InterbankBloc>(
        () => _i18.InterbankBloc(gh<_i14.InterbankUseCase>()));
    gh.factory<_i19.ExchangeRateBloc>(
        () => _i19.ExchangeRateBloc(gh<_i14.ExchangeRateUseCase>()));
    gh.factory<_i20.CustomerBloc>(
        () => _i20.CustomerBloc(gh<_i14.CustomerUseCase>()));
    gh.factory<_i21.DashBoardBloc>(
        () => _i21.DashBoardBloc(gh<_i14.CustomerUseCase>()));
    gh.factory<_i22.CommonBloc>(
        () => _i22.CommonBloc(gh<_i14.ClearCurrentUserDataUseCase>()));
    gh.factory<_i23.SettingBloc>(
        () => _i23.SettingBloc(gh<_i14.LogoutUseCase>()));
    gh.factory<_i24.UserInfoBloc>(() => _i24.UserInfoBloc(
          gh<_i14.LogoutUseCase>(),
          gh<_i14.UserInfoPageUseCase>(),
        ));
    gh.factory<_i25.CalculateSavingBloc>(
        () => _i25.CalculateSavingBloc(gh<_i14.CalculateSavingUseCase>()));
    gh.lazySingleton<_i26.AppBloc>(() => _i26.AppBloc(
          gh<_i14.GetInitialAppDataUseCase>(),
          gh<_i14.SaveIsDarkModeUseCase>(),
          gh<_i14.SaveLanguageCodeUseCase>(),
          gh<_i14.SaveUserNameUseCase>(),
        ));
    gh.factory<_i27.MainBloc>(
        () => _i27.MainBloc(gh<_i14.GetInitialMainDataUseCase>()));
    gh.factory<_i28.HomeBloc>(() => _i28.HomeBloc(gh<_i14.GetUsersUseCase>()));
    gh.factory<_i29.RouteGuard>(
        () => _i29.RouteGuard(gh<_i14.IsLoggedInUseCase>()));
    gh.lazySingleton<_i14.AppNavigator>(() => _i30.AppNavigatorImpl(
          gh<_i11.AppRouter>(),
          gh<
              _i11.BasePopupInfoMapper<_i15.StatefulWidget,
                  _i11.BaseBloc<_i11.BaseBlocEvent, _i11.BaseBlocState>>>(),
          gh<_i11.BaseRouteInfoMapper>(),
        ));
    gh.factory<_i31.CreateTodoBloc>(
        () => _i31.CreateTodoBloc(gh<_i14.CreateTodoUseCase>()));
    return this;
  }
}
