// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'app_event.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$IsLoggedInStatusChanged {
  bool get isLoggedIn => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $IsLoggedInStatusChangedCopyWith<IsLoggedInStatusChanged> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $IsLoggedInStatusChangedCopyWith<$Res> {
  factory $IsLoggedInStatusChangedCopyWith(IsLoggedInStatusChanged value,
          $Res Function(IsLoggedInStatusChanged) then) =
      _$IsLoggedInStatusChangedCopyWithImpl<$Res, IsLoggedInStatusChanged>;
  @useResult
  $Res call({bool isLoggedIn});
}

/// @nodoc
class _$IsLoggedInStatusChangedCopyWithImpl<$Res,
        $Val extends IsLoggedInStatusChanged>
    implements $IsLoggedInStatusChangedCopyWith<$Res> {
  _$IsLoggedInStatusChangedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isLoggedIn = null,
  }) {
    return _then(_value.copyWith(
      isLoggedIn: null == isLoggedIn
          ? _value.isLoggedIn
          : isLoggedIn // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$IsLoggedInStatusChangedImplCopyWith<$Res>
    implements $IsLoggedInStatusChangedCopyWith<$Res> {
  factory _$$IsLoggedInStatusChangedImplCopyWith(
          _$IsLoggedInStatusChangedImpl value,
          $Res Function(_$IsLoggedInStatusChangedImpl) then) =
      __$$IsLoggedInStatusChangedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({bool isLoggedIn});
}

/// @nodoc
class __$$IsLoggedInStatusChangedImplCopyWithImpl<$Res>
    extends _$IsLoggedInStatusChangedCopyWithImpl<$Res,
        _$IsLoggedInStatusChangedImpl>
    implements _$$IsLoggedInStatusChangedImplCopyWith<$Res> {
  __$$IsLoggedInStatusChangedImplCopyWithImpl(
      _$IsLoggedInStatusChangedImpl _value,
      $Res Function(_$IsLoggedInStatusChangedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isLoggedIn = null,
  }) {
    return _then(_$IsLoggedInStatusChangedImpl(
      isLoggedIn: null == isLoggedIn
          ? _value.isLoggedIn
          : isLoggedIn // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$IsLoggedInStatusChangedImpl implements _IsLoggedInStatusChanged {
  const _$IsLoggedInStatusChangedImpl({required this.isLoggedIn});

  @override
  final bool isLoggedIn;

  @override
  String toString() {
    return 'IsLoggedInStatusChanged(isLoggedIn: $isLoggedIn)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$IsLoggedInStatusChangedImpl &&
            (identical(other.isLoggedIn, isLoggedIn) ||
                other.isLoggedIn == isLoggedIn));
  }

  @override
  int get hashCode => Object.hash(runtimeType, isLoggedIn);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$IsLoggedInStatusChangedImplCopyWith<_$IsLoggedInStatusChangedImpl>
      get copyWith => __$$IsLoggedInStatusChangedImplCopyWithImpl<
          _$IsLoggedInStatusChangedImpl>(this, _$identity);
}

abstract class _IsLoggedInStatusChanged implements IsLoggedInStatusChanged {
  const factory _IsLoggedInStatusChanged({required final bool isLoggedIn}) =
      _$IsLoggedInStatusChangedImpl;

  @override
  bool get isLoggedIn;
  @override
  @JsonKey(ignore: true)
  _$$IsLoggedInStatusChangedImplCopyWith<_$IsLoggedInStatusChangedImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$AppThemeChanged {
  bool get isDarkTheme => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $AppThemeChangedCopyWith<AppThemeChanged> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AppThemeChangedCopyWith<$Res> {
  factory $AppThemeChangedCopyWith(
          AppThemeChanged value, $Res Function(AppThemeChanged) then) =
      _$AppThemeChangedCopyWithImpl<$Res, AppThemeChanged>;
  @useResult
  $Res call({bool isDarkTheme});
}

/// @nodoc
class _$AppThemeChangedCopyWithImpl<$Res, $Val extends AppThemeChanged>
    implements $AppThemeChangedCopyWith<$Res> {
  _$AppThemeChangedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isDarkTheme = null,
  }) {
    return _then(_value.copyWith(
      isDarkTheme: null == isDarkTheme
          ? _value.isDarkTheme
          : isDarkTheme // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$AppThemeChangedImplCopyWith<$Res>
    implements $AppThemeChangedCopyWith<$Res> {
  factory _$$AppThemeChangedImplCopyWith(_$AppThemeChangedImpl value,
          $Res Function(_$AppThemeChangedImpl) then) =
      __$$AppThemeChangedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({bool isDarkTheme});
}

/// @nodoc
class __$$AppThemeChangedImplCopyWithImpl<$Res>
    extends _$AppThemeChangedCopyWithImpl<$Res, _$AppThemeChangedImpl>
    implements _$$AppThemeChangedImplCopyWith<$Res> {
  __$$AppThemeChangedImplCopyWithImpl(
      _$AppThemeChangedImpl _value, $Res Function(_$AppThemeChangedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isDarkTheme = null,
  }) {
    return _then(_$AppThemeChangedImpl(
      isDarkTheme: null == isDarkTheme
          ? _value.isDarkTheme
          : isDarkTheme // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$AppThemeChangedImpl implements _AppThemeChanged {
  const _$AppThemeChangedImpl({required this.isDarkTheme});

  @override
  final bool isDarkTheme;

  @override
  String toString() {
    return 'AppThemeChanged(isDarkTheme: $isDarkTheme)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AppThemeChangedImpl &&
            (identical(other.isDarkTheme, isDarkTheme) ||
                other.isDarkTheme == isDarkTheme));
  }

  @override
  int get hashCode => Object.hash(runtimeType, isDarkTheme);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$AppThemeChangedImplCopyWith<_$AppThemeChangedImpl> get copyWith =>
      __$$AppThemeChangedImplCopyWithImpl<_$AppThemeChangedImpl>(
          this, _$identity);
}

abstract class _AppThemeChanged implements AppThemeChanged {
  const factory _AppThemeChanged({required final bool isDarkTheme}) =
      _$AppThemeChangedImpl;

  @override
  bool get isDarkTheme;
  @override
  @JsonKey(ignore: true)
  _$$AppThemeChangedImplCopyWith<_$AppThemeChangedImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$AppLanguageChanged {
  LanguageCode get languageCode => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $AppLanguageChangedCopyWith<AppLanguageChanged> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AppLanguageChangedCopyWith<$Res> {
  factory $AppLanguageChangedCopyWith(
          AppLanguageChanged value, $Res Function(AppLanguageChanged) then) =
      _$AppLanguageChangedCopyWithImpl<$Res, AppLanguageChanged>;
  @useResult
  $Res call({LanguageCode languageCode});
}

/// @nodoc
class _$AppLanguageChangedCopyWithImpl<$Res, $Val extends AppLanguageChanged>
    implements $AppLanguageChangedCopyWith<$Res> {
  _$AppLanguageChangedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? languageCode = null,
  }) {
    return _then(_value.copyWith(
      languageCode: null == languageCode
          ? _value.languageCode
          : languageCode // ignore: cast_nullable_to_non_nullable
              as LanguageCode,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$AppLanguageChangedImplCopyWith<$Res>
    implements $AppLanguageChangedCopyWith<$Res> {
  factory _$$AppLanguageChangedImplCopyWith(_$AppLanguageChangedImpl value,
          $Res Function(_$AppLanguageChangedImpl) then) =
      __$$AppLanguageChangedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({LanguageCode languageCode});
}

/// @nodoc
class __$$AppLanguageChangedImplCopyWithImpl<$Res>
    extends _$AppLanguageChangedCopyWithImpl<$Res, _$AppLanguageChangedImpl>
    implements _$$AppLanguageChangedImplCopyWith<$Res> {
  __$$AppLanguageChangedImplCopyWithImpl(_$AppLanguageChangedImpl _value,
      $Res Function(_$AppLanguageChangedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? languageCode = null,
  }) {
    return _then(_$AppLanguageChangedImpl(
      languageCode: null == languageCode
          ? _value.languageCode
          : languageCode // ignore: cast_nullable_to_non_nullable
              as LanguageCode,
    ));
  }
}

/// @nodoc

class _$AppLanguageChangedImpl implements _AppLanguageChanged {
  const _$AppLanguageChangedImpl({required this.languageCode});

  @override
  final LanguageCode languageCode;

  @override
  String toString() {
    return 'AppLanguageChanged(languageCode: $languageCode)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AppLanguageChangedImpl &&
            (identical(other.languageCode, languageCode) ||
                other.languageCode == languageCode));
  }

  @override
  int get hashCode => Object.hash(runtimeType, languageCode);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$AppLanguageChangedImplCopyWith<_$AppLanguageChangedImpl> get copyWith =>
      __$$AppLanguageChangedImplCopyWithImpl<_$AppLanguageChangedImpl>(
          this, _$identity);
}

abstract class _AppLanguageChanged implements AppLanguageChanged {
  const factory _AppLanguageChanged(
      {required final LanguageCode languageCode}) = _$AppLanguageChangedImpl;

  @override
  LanguageCode get languageCode;
  @override
  @JsonKey(ignore: true)
  _$$AppLanguageChangedImplCopyWith<_$AppLanguageChangedImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$UserNameChanged {
  String get userName => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $UserNameChangedCopyWith<UserNameChanged> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserNameChangedCopyWith<$Res> {
  factory $UserNameChangedCopyWith(
          UserNameChanged value, $Res Function(UserNameChanged) then) =
      _$UserNameChangedCopyWithImpl<$Res, UserNameChanged>;
  @useResult
  $Res call({String userName});
}

/// @nodoc
class _$UserNameChangedCopyWithImpl<$Res, $Val extends UserNameChanged>
    implements $UserNameChangedCopyWith<$Res> {
  _$UserNameChangedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? userName = null,
  }) {
    return _then(_value.copyWith(
      userName: null == userName
          ? _value.userName
          : userName // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$UserNameChangedImplCopyWith<$Res>
    implements $UserNameChangedCopyWith<$Res> {
  factory _$$UserNameChangedImplCopyWith(_$UserNameChangedImpl value,
          $Res Function(_$UserNameChangedImpl) then) =
      __$$UserNameChangedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String userName});
}

/// @nodoc
class __$$UserNameChangedImplCopyWithImpl<$Res>
    extends _$UserNameChangedCopyWithImpl<$Res, _$UserNameChangedImpl>
    implements _$$UserNameChangedImplCopyWith<$Res> {
  __$$UserNameChangedImplCopyWithImpl(
      _$UserNameChangedImpl _value, $Res Function(_$UserNameChangedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? userName = null,
  }) {
    return _then(_$UserNameChangedImpl(
      userName: null == userName
          ? _value.userName
          : userName // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$UserNameChangedImpl implements _UserNameChanged {
  const _$UserNameChangedImpl({required this.userName});

  @override
  final String userName;

  @override
  String toString() {
    return 'UserNameChanged(userName: $userName)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UserNameChangedImpl &&
            (identical(other.userName, userName) ||
                other.userName == userName));
  }

  @override
  int get hashCode => Object.hash(runtimeType, userName);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$UserNameChangedImplCopyWith<_$UserNameChangedImpl> get copyWith =>
      __$$UserNameChangedImplCopyWithImpl<_$UserNameChangedImpl>(
          this, _$identity);
}

abstract class _UserNameChanged implements UserNameChanged {
  const factory _UserNameChanged({required final String userName}) =
      _$UserNameChangedImpl;

  @override
  String get userName;
  @override
  @JsonKey(ignore: true)
  _$$UserNameChangedImplCopyWith<_$UserNameChangedImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$AppInitiated {}

/// @nodoc
abstract class $AppInitiatedCopyWith<$Res> {
  factory $AppInitiatedCopyWith(
          AppInitiated value, $Res Function(AppInitiated) then) =
      _$AppInitiatedCopyWithImpl<$Res, AppInitiated>;
}

/// @nodoc
class _$AppInitiatedCopyWithImpl<$Res, $Val extends AppInitiated>
    implements $AppInitiatedCopyWith<$Res> {
  _$AppInitiatedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$AppInitiatedImplCopyWith<$Res> {
  factory _$$AppInitiatedImplCopyWith(
          _$AppInitiatedImpl value, $Res Function(_$AppInitiatedImpl) then) =
      __$$AppInitiatedImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$AppInitiatedImplCopyWithImpl<$Res>
    extends _$AppInitiatedCopyWithImpl<$Res, _$AppInitiatedImpl>
    implements _$$AppInitiatedImplCopyWith<$Res> {
  __$$AppInitiatedImplCopyWithImpl(
      _$AppInitiatedImpl _value, $Res Function(_$AppInitiatedImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$AppInitiatedImpl implements _AppInitiated {
  const _$AppInitiatedImpl();

  @override
  String toString() {
    return 'AppInitiated()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$AppInitiatedImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;
}

abstract class _AppInitiated implements AppInitiated {
  const factory _AppInitiated() = _$AppInitiatedImpl;
}
