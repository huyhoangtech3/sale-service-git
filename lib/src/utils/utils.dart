import 'package:flutter/material.dart';
import 'package:base_architecture_flutter/src/app.dart';
import 'package:shared/shared.dart';

class Utils{
  Widget buildIconSvg(SvgGenImage svg, double width, double height, Color? iconColor) {
    return svg.svg(
      colorFilter: iconColor?.let((it) => ColorFilter.mode(it, BlendMode.srcIn)),
      width: width,
      height: height,
    );
  }
}